// shipitfile.babel.js
const path = require("path");
const deployModule = require("shipit-deploy");

module.exports = (shipit) => {
  deployModule(shipit);
  require("shipit-shared")(shipit);
  shipit.initConfig({
    default: {
      keepReleases: 4,
      repositoryUrl: "git@gitlab.com:uplinestudio/eco-plant/ecoplant-nuxt.git",
    },
    production: {
      deployTo: "~/shipit/production",
      path: "nuxt",
      branch: "master",
      servers: {
        host: "130.193.35.71",
        user: "ecoplant-nuxt",
      },
    },
    ab: {
      deployTo: "~/shipit/ab",
      branch: "feature/grass-page-in-popup",
      path: "nuxt",
      servers: {
        host: "130.193.35.71",
        user: "ecoplant-nuxt",
      },
    },
    ab2: {
      deployTo: "~/shipit/ab2",
      branch: "ab-tests/change-tab-block-order",
      path: "nuxt",
      servers: {
        host: "130.193.35.71",
        user: "ecoplant-nuxt",
      },
    },
    seo: {
      deployTo: "~/shipit/seo",
      branch: "master",
      path: "nuxt",
      servers: {
        host: "130.193.35.71",
        user: "ecoplant-nuxt",
      },
    },
    apollo: {
      deployTo: "~/shipit/apollo",
      branch: "master",
      path: "apollo",
      servers: {
        host: "130.193.35.71",
        user: "ecoplant-nuxt",
      },
      shared: {
        files: ["apollo/.env"],
      },
    },
  });
  shipit.blTask("tilde", function () {
    if (!shipit.config.deployTo.includes("~/")) {
      return;
    }
    return shipit.remote("echo ~/").then(function (servers) {
      const path = servers[0].stdout.trim();
      shipit.config.deployTo = shipit.config.deployTo.replace("~/", path);
    });
  });
  shipit.blTask("build", async function () {
    const currentNodeModules = path.join(
      shipit.currentPath,
      "/" + shipit.config.path,
      "/node_modules"
    );
    const newNodeModules = path.join(
      shipit.releasePath,
      "/" + shipit.config.path,
      "/node_modules"
    );
    await shipit.remote(
      `test -d ${currentNodeModules} && cp -r ${currentNodeModules} ${newNodeModules} || echo "node modules does not exists"`
    );
    const projectPath = path.join(shipit.releasePath, "/" + shipit.config.path);
    await shipit.remote(
      `. ~/.nvm/nvm.sh && cd ${projectPath} && npm i && NODE_ENV=production npm run build`
    );
    await shipit.local('echo "app compiled"');
  });
  shipit.blTask("pm2", function () {
    shipit.log("Launching pm2");

    const cmd = `. ~/.nvm/nvm.sh && cd ${shipit.config.deployTo} && pm2 startOrRestart current/shipit-pm2.config.js --only ${shipit.environment}`;

    return shipit.remote(cmd);
  });
  shipit.on("updated", function () {
    shipit.start("build");
  });

  shipit.on("fetched", function () {
    shipit.start("tilde");
  });

  shipit.on("published", function () {
    return shipit.start("pm2");
  });
};
