declare module 'ymaps' {
  class Ymaps {
    static load(url: string): Promise<any>
  }
  export = Ymaps
}
