import { actionTree, mutationTree } from 'typed-vuex'
import Vue from 'vue'

export type GrassProductPrice = {
  old: number
  current: number
}

export type GrassProductUsage = {
  icon: string
  text: string
}

export type GrassProductAttribute = {
  title: string
  value: string
}
export type GrassProductImages = {
  small: string
  large: string
  full: string
}

export type GrassProduct = {
  id: number
  type: string
  heading: string
  content: string
  size: string
  image: string
  price: GrassProductPrice
  usage: GrassProductUsage[]
  attributes: GrassProductAttribute[]
  images: GrassProductImages[]
}

const getProductImages = (lawnId: number) => {
  const tpl = (size: string, lawnId: number, imageId: number) =>
    `https://ecoplant-pitomnik.ru/image/catalog/landings/grass/${size}/lawn-${lawnId}/${imageId}.jpg`
  const images = []
  for (let i = 1; i <= 3; i++) {
    images.push({
      small: tpl('small', lawnId, i),
      large: tpl('large', lawnId, i),
      full: tpl('full', lawnId, i)
    })
  }
  return images
}

const products = {
  universal: {
    id: 1,
    heading: 'Универсальный газон',
    content:
      'Рекомендуется для городского и загородного озеленения. Отличается высокой степенью устойчивости к вытаптыванию и способностью к быстрому восстановлению.',
    size: '0.4 х 2 метра, площадь 0.8 м',
    image:
      'https://ecoplant-pitomnik.ru/image/catalog/landings/grass/icons/universal.jpg',
    usage: [
      {
        icon: 'factory',
        text: 'Озеленение промышленных и технических зон'
      },
      {
        icon: 'building',
        text: 'Городское озеленение'
      }
    ],
    attributes: [
      {
        title: 'Размеры рулона:',
        value: '2x0.4 м./0.8 м'
      },
      {
        title: 'Вес рулона:',
        value: '15-25 кг в зависимости от кол-ва осадков'
      },
      {
        title: 'Состав травосмеси:',
        value:
          'Мятлик луговой Skye, возможно присутствие: Овсяница луговая, Тимофеевка луговая.'
      },
      {
        title: 'Густота:',
        value:
          'Достаточно плотная зеленой масса достигается за счет показателей проективного покрытия и количества побегов на единицу площади\n'
      }
    ],
    images: getProductImages(1)
  },
  shadow: {
    id: 2,
    heading: 'Теневыносливый газон',
    content:
      'Идеален для территорий, которые большую часть светого дня находятся в тени. Способен спокойно переносить затемнения и повышенную влажность.',
    size: '0.4 х 2 метра, площадь 0.8 м',
    image:
      'https://ecoplant-pitomnik.ru/image/catalog/landings/grass/icons/shadow.jpg',
    usage: [
      {
        icon: 'real-estate',
        text: 'Частные дворовые и дачные территории'
      },
      {
        icon: 'park',
        text: 'Для озеленения парков, скверов, придорожных территорий'
      },
      {
        icon: 'jogging',
        text: 'Территории с интенсивным использованием газона'
      }
    ],
    attributes: [
      {
        title: 'Размеры рулона:',
        value: '2x0.4 м./0.8 м'
      },
      {
        title: 'Вес рулона:',
        value: '15-25 кг в зависимости от кол-ва осадков'
      },
      {
        title: 'Состав травосмеси:',
        value:
          '40% мятлик луговой сорт БАРИРИС; 40% мятлик луговой сорт БАРОНИАЛ; 20% овсяница красная жесткая сорт БАРГРИН.'
      },
      {
        title: 'Густота:',
        value:
          'Обладает насыщенным темно-зеленым цветом, однороден по цвету и фактуре, рекомендуется для проведения ландшафтных работ на участках, которые большую часть времени находятся в тени.'
      }
    ],
    images: getProductImages(2)
  },
  sport: {
    id: 3,
    heading: 'Спортивный газон',
    content:
      'Имеет ярко зеленый насыщенный цвет. Упругий, не боится втаптывания,способный к быстрому восстановлению повреждений.',
    size: '0.4 х 2 метра, площадь 0.8 м',
    image:
      'https://ecoplant-pitomnik.ru/image/catalog/landings/grass/icons/sport.jpg',
    usage: [
      {
        icon: 'real-estate',
        text: 'Частные дворовые и дачные территории'
      },
      {
        icon: 'park',
        text: 'Для озеленения парков, скверов, придорожных территорий'
      }
    ],
    attributes: [
      {
        title: 'Размеры рулона:',
        value: '2x0.4 м./0.8 м'
      },
      {
        title: 'Вес рулона:',
        value: '15-25 кг в зависимости от кол-ва осадков'
      },
      {
        title: 'Состав травосмеси:',
        value:
          '100% мятник луговой, основной сорт «Blueberry Kentucky Bluegrass»'
      },
      {
        title: 'Густота:',
        value:
          'Имеет ярко зеленый насыщенный цвет. Упругий, не боится втаптывания,способный к быстрому восстановлению повреждений.'
      }
    ],
    images: getProductImages(3)
  },
  elite: {
    id: 4,
    heading: 'Элитный газон',
    content:
      'Эксклюзивный газон, насыщенного изумрудного цвета. Статусный и роскошный вид.',
    size: '0.4 х 2 метра, площадь 0.8 м',
    image:
      'https://ecoplant-pitomnik.ru/image/catalog/landings/grass/icons/elite.jpg',
    usage: [
      {
        icon: 'real-estate',
        text: 'Частные дворовые и дачные территории'
      },
      {
        icon: 'park',
        text: 'Для создания видовых, статусных участков'
      },
      {
        icon: 'jogging',
        text: 'Территории с интенсивным использованием газона'
      }
    ],
    attributes: [
      {
        title: 'Размеры рулона:',
        value: '2x0.4 м./0.8 м'
      },
      {
        title: 'Вес рулона:',
        value: '15-25 кг в зависимости от кол-ва осадков'
      },
      {
        title: 'Состав травосмеси:',
        value:
          '100% мятлик (Poa pratensis), 25% Award, 25% Sudden Impact, 25% NuGlade, 25% Full Moon.'
      },
      {
        title: 'Густота:',
        value:
          'Большая плотность дернины и травянного покрытия за счет 4хлетней культивацией на полях перед продажей.'
      }
    ],
    images: getProductImages(4)
  },
  economical: {
    id: 5,
    heading: 'Экономичный газон',
    content:
      'Рекомендуется использовать, в местах, где нет специальных требований к газону.',
    size: '0.4 х 2 метра, площадь 0.8 м',
    image:
      'https://ecoplant-pitomnik.ru/image/catalog/landings/grass/icons/economical.jpg',
    usage: [
      {
        icon: 'real-estate',
        text: 'Частные дворовые и дачные территории'
      },
      {
        icon: 'park',
        text: 'Для озеленения парков, скверов, придорожных территорий'
      }
      // {
      //   icon: 'jogging',
      //   text: 'Территории с интенсивным использованием газона'
      // }
    ],
    attributes: [
      {
        title: 'Размеры рулона:',
        value: '2x0.4 м./0.8 м'
      },
      {
        title: 'Вес рулона:',
        value: '15-25 кг в зависимости от кол-ва осадков'
      },
      {
        title: 'Состав травосмеси:',
        value:
          '40% мятлик луговой сорт БАРИРИС; 40% мятлик луговой сорт БАРОНИАЛ; 20% овсяница красная жесткая сорт БАРГРИН.'
      },
      {
        title: 'Густота:',
        value:
          'Обладает насыщенным темно-зеленым цветом, однороден по цвету и фактуре, рекомендуется использовать, в местах, где нет специальных требований к газону.'
      }
    ],
    images: getProductImages(2)
  }
}

export type AvailableProduct = {
  type: 'universal' | 'shadow' | 'sport' | 'elite' | 'economical'
  price: GrassProductPrice
}

export type GrassProducts = GrassProduct[]

export type GrassService = {
  title: string
  price: number
  info: string
  items: string[]
}

export const state = () => ({
  products: [] as GrassProducts,
  services: [] as GrassService[]
})

export const mutations = mutationTree(state, {
  fillGrassProducts(state, availableProducts: AvailableProduct[]) {
    Vue.set(
      state,
      'products',
      Object.freeze(
        availableProducts.map(({ type, price }) => ({
          ...products[type],
          price
        }))
      )
    )
  },
  fillGrassServices(state, services: GrassService[]) {
    Vue.set(state, 'services', Object.freeze(services))
  }
})

export const actions = actionTree(
  { state, mutations },
  {
    nuxtFillGrass({ commit }, { availableProducts, services }) {
      commit('fillGrassProducts', availableProducts)
      commit('fillGrassServices', services)
    }
  }
)
