import { getAccessorType, actionTree, mutationTree } from 'typed-vuex'
import { Context } from '@nuxt/types'

import * as contacts from '~/store/contacts'
import * as socials from '~/store/socials'
import * as components from '~/store/components'
import * as grass from '~/store/grass'

type Planting = {
  video: string
  poster: string
  buttonText: string
}

export const state = () => ({
  rocketCrmId: '',
  group: '',
  categoryIds: [] as string[],
  meta: {
    homePage: {
      title: '',
      description: ''
    }
  },
  showExpandedInformation: false,
  planting: null as Planting | null
})

export const mutations = mutationTree(state, {
  setRocketCrmId(state, rocketCrmId: string) {
    state.rocketCrmId = rocketCrmId
  },
  setGroup(state, group: string) {
    state.group = group
  },
  setCategoryIds(state, categoryIds: string[]) {
    state.categoryIds = categoryIds
  },
  setHomePageMeta(state, { title, description }) {
    state.meta.homePage.title = title
    state.meta.homePage.description = description
  },
  setShowExpandedInformation(state, flag: boolean) {
    state.showExpandedInformation = flag
  },
  setPlanting(state, planting: Planting) {
    state.planting = planting
  }
})

const modules = {
  contacts,
  socials,
  components,
  grass
}

export const actions = actionTree(
  { state, modules },
  {
    nuxtServerInit(_vuexContext, nuxtContext: Context) {
      const data = nuxtContext.req.data
      if (data) {
        _vuexContext.commit('setRocketCrmId', data.crmCode)
        _vuexContext.commit('setGroup', data.group)
        _vuexContext.commit('setCategoryIds', data.categoryIds)
        _vuexContext.commit('setPlanting', data.planting)
        _vuexContext.commit(
          'setShowExpandedInformation',
          data.showExpandedInformation
        )
        _vuexContext.dispatch('contacts/nuxtFillContacts', data.contacts)
        _vuexContext.commit('setHomePageMeta', data.meta.home)
        _vuexContext.dispatch(
          'components/nuxtFillLayoutComponents',
          data.layoutComponents
        )
        _vuexContext.dispatch(
          'components/nuxtFillGrassComponents',
          data.grassComponents
        )
        _vuexContext.dispatch(
          'components/nuxtFillHomeComponents',
          data.homeComponents
        )
        _vuexContext.dispatch(
          'components/nuxtFillHomeLayoutComponents',
          data.homeLayoutComponents || data.layoutComponents
        )
        _vuexContext.dispatch(
          'components/nuxtFillCategoryComponent',
          data.categoryComponent
        )
        _vuexContext.dispatch('grass/nuxtFillGrass', data.grass)
      }
    }
  }
)

export const accessorType = getAccessorType({
  state,
  actions,
  mutations,
  modules
})
