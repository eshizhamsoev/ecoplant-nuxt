import { actionTree, getterTree, mutationTree } from 'typed-vuex'

export type PhoneData = {
  text: string
  number: string
}

const SPB_GROUP = 'spb'
const REGION_GROUP = 'region'

const defaultMsk = {
  phone: {
    text: '8 (495) 374 57 28',
    number: '+74953745728'
  },
  email: 'info@ecoplant.ru',
  address:
    'г. Москва, 10км от МКАД по Ильинскому шоссе в сторону области, деревня Ильинское-Усово',
  geoPoint: {
    latitude: 55.7619709,
    longitude: 37.227821
  },
  region: 'Москва'
}

const defaultSpb = {
  phone: {
    number: '+78124071598',
    text: '8 (812) 407 15 98'
  },
  email: 'spb@ecoplant.ru',
  support: {
    whatsApp: '79219157746'
  },
  address:
    'г. Санкт-Петербург, 1-й км от КАД по Выборгскому шоссе в сторону области, поселок Осиновая роща, улица Колхозная 3.',
  geoPoint: {
    latitude: 60.105995,
    longitude: 30.249413
  },
  region: 'Санкт-Петербург'
}

export const state = () => ({
  siteName: 'Питомник растений в Москве',
  phone: {
    text: '8 (495) 374 57 28',
    number: '+74953745728'
  },
  whatsApp: '79190200797',
  telegram: 'pitomnikecoplant_bot',
  email: 'info@ecoplant.ru',
  support: {
    whatsApp: '79060610799'
  },
  address:
    'г. Москва, 10км от МКАД по Ильинскому шоссе в сторону области, деревня Ильинское-Усово',
  geoPoint: {
    latitude: 55.7619709,
    longitude: 37.227821
  },
  legal:
    'ОГРН 1175024011997\n' +
    'ИНН 5024174206\n' +
    'ООО «Питомник растений», \n' +
    '1993-2019'
})

export const getters = getterTree(state, {
  additionalContacts(state, _, rootState) {
    return [SPB_GROUP, REGION_GROUP].includes(rootState.group)
      ? defaultMsk
      : defaultSpb
  },
  currentRegion(state, _, rootState) {
    if (rootState.group === REGION_GROUP) {
      return 'Махачкала'
    }
    return rootState.group === SPB_GROUP ? defaultSpb.region : defaultMsk.region
  }
})

export const mutations = mutationTree(state, {
  fillPhone(state, { text, number }) {
    state.phone.text = text
    state.phone.number = number
  },
  fillSiteName(state, siteName) {
    state.siteName = siteName
  },
  fillWhatsApp(state, whatsApp) {
    state.whatsApp = whatsApp
  },
  fillTelegram(state, telegram) {
    state.telegram = telegram
  },
  fillEmail(state, email) {
    state.email = email
  },
  fillAddress(state, address) {
    state.address = address
  },
  fillGeoPoint(state, { latitude, longitude }) {
    state.geoPoint.latitude = latitude
    state.geoPoint.longitude = longitude
  },
  fillLegal(state, legal) {
    state.legal = legal
  }
})

export const actions = actionTree(
  { state, mutations },
  {
    nuxtFillContacts({ commit }, data) {
      commit('fillPhone', data.phone)
      commit('fillSiteName', data.siteName)
      if (data.whatsApp) {
        commit('fillWhatsApp', data.whatsApp)
      }
      if (data.telegram) {
        commit('fillTelegram', data.telegram)
      }
      commit('fillEmail', data.email)
      commit('fillAddress', data.address)
      commit('fillGeoPoint', data.geoPoint)
      commit('fillLegal', data.legal)
    },
    setPhone({ commit }, payload) {
      const data = payload.toString()
      if (data) {
        if (data[0] === '7') {
          const number = '+' + data
          const text =
            '8 (' +
            data.substring(1, 4) +
            ') ' +
            data.substring(4, 7) +
            ' ' +
            data.substring(7, 9) +
            ' ' +
            data.substring(9)
          commit('fillPhone', { text, number })
        }
      }
    }
  }
)
