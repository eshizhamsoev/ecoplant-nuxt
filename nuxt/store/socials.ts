export const state = () => ({
  socials: [
    {
      link: 'https://vk.com/pitomniktuy',
      icon: 'vk',

      number: 60921, // 45651,
      label: 'человек'
    },
    {
      link: 'https://ok.ru/pitomniktu',
      icon: 'od',

      number: 10605, // 8828,
      label: 'человек'
    },
    {
      link: 'https://www.facebook.com/tuivmoskve',
      icon: 'fb',
      number: 3536, // 2595,
      label: 'человек'
    },
    {
      link: 'http://instagram.com/pitomnikecoplant',
      icon: 'ig',

      number: 25410, // 13200,
      label: 'человек'
    },
    {
      link: 'https://t.me/eco_plant',
      icon: 'tg',

      number: 72,
      label: 'человек'
    },
    {
      link: 'https://www.youtube.com/channel/UCQBp8ZyXKSXP_z3K8pcws1Q/videos',
      icon: 'yt',

      number: 25000000,
      label: 'просмотров'
    }
  ]
})
