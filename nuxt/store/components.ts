import { actionTree, mutationTree } from 'typed-vuex'
import Vue from 'vue'
import { CategoryPageType } from '~/data/const/page-types/category'

export type Component = {
  id: string
  name: string
  props: any
  isMobile: boolean
  isDesktop: boolean
}

export const state = () => ({
  categoryComponent: '' as CategoryPageType,
  layoutComponents: [] as Component[],
  homeComponents: [] as Component[],
  grassComponents: [] as Component[],
  homeLayoutComponents: [] as Component[]
})

const fillId = (components: Component[]) => {
  const ids = {} as { [key: string]: number }
  return components.map((component) => {
    if (!ids[component.name]) {
      ids[component.name] = 1
    } else {
      ids[component.name]++
    }
    return {
      ...component,
      id: component.name + ids[component.name]
    }
  })
}

export const mutations = mutationTree(state, {
  fillLayoutComponents(state, components) {
    Vue.set(state, 'layoutComponents', fillId(components))
  },
  fillHomeLayoutComponents(state, components) {
    Vue.set(state, 'homeLayoutComponents', fillId(components))
  },
  fillHomeComponents(state, components) {
    Vue.set(state, 'homeComponents', fillId(components))
  },
  fillGrassComponents(state, components) {
    Vue.set(state, 'grassComponents', fillId(components))
  },
  fillCategoryComponent(state, component) {
    Vue.set(state, 'categoryComponent', component)
  },
  setHeading(state, heading) {
    state.layoutComponents.forEach((item) => {
      if (item.name === 'BlockHeading') {
        item.props.heading = heading
      }
    })
    state.homeLayoutComponents.forEach((item) => {
      if (item.name === 'BlockHeading') {
        item.props.heading = heading
      }
    })
  }
})

export const actions = actionTree(
  { state, mutations },
  {
    nuxtFillLayoutComponents({ commit }, components) {
      commit('fillLayoutComponents', components)
    },
    nuxtFillHomeComponents({ commit }, components) {
      commit('fillHomeComponents', components)
    },
    nuxtFillGrassComponents({ commit }, components) {
      commit('fillGrassComponents', components)
    },
    nuxtFillHomeLayoutComponents({ commit }, components) {
      commit('fillHomeLayoutComponents', components)
    },
    nuxtFillCategoryComponent({ commit }, component) {
      commit('fillCategoryComponent', component)
    }
  }
)
