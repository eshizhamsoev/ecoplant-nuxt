export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

export type AbstractService = {
  id: Scalars['ID'];
  images: Array<ServiceImage>;
  name: Scalars['String'];
  price?: Maybe<Scalars['String']>;
};

export type Award = {
  __typename?: 'Award';
  image: ThumbnailImage;
};

export enum CacheControlScope {
  Private = 'PRIVATE',
  Public = 'PUBLIC'
}

export type CareSeason = {
  __typename?: 'CareSeason';
  blocks: Array<CareSeasonBlock>;
  id: Scalars['ID'];
  title: Scalars['String'];
};

export type CareSeasonBlock = {
  __typename?: 'CareSeasonBlock';
  id: Scalars['ID'];
  images: Array<ImageWithSize>;
  text: Scalars['String'];
  title: Scalars['String'];
};

export type CartPrice = {
  __typename?: 'CartPrice';
  priceId: Scalars['ID'];
  quantity: Scalars['Int'];
};

export type CartProduct = {
  __typename?: 'CartProduct';
  prices: Array<CartPrice>;
  productId: Scalars['ID'];
};

export type Category = {
  id: Scalars['ID'];
  image: Scalars['String'];
  name: Scalars['String'];
};

export type Client = {
  __typename?: 'Client';
  image: ThumbnailImage;
  isState: Scalars['Boolean'];
  title?: Maybe<Scalars['String']>;
};

export type ClientPhoto = {
  __typename?: 'ClientPhoto';
  image: ThumbnailImage;
  name: Scalars['String'];
};

export type Document = {
  __typename?: 'Document';
  image: ThumbnailImage;
};

export type Exclusive = AbstractService & {
  __typename?: 'Exclusive';
  id: Scalars['ID'];
  images: Array<ServiceImage>;
  name: Scalars['String'];
  price?: Maybe<Scalars['String']>;
};

export type ExclusiveCategory = Category & {
  __typename?: 'ExclusiveCategory';
  exclusives: Array<Exclusive>;
  id: Scalars['ID'];
  image: Scalars['String'];
  name: Scalars['String'];
};

export type FileVideo = {
  __typename?: 'FileVideo';
  source: Scalars['String'];
};

export type GalleryItem = {
  __typename?: 'GalleryItem';
  content?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  image: ThumbnailImage;
  title: Scalars['String'];
};

export type ImageWithSize = {
  __typename?: 'ImageWithSize';
  height: Scalars['Int'];
  src: Scalars['String'];
  width: Scalars['Int'];
};

export type Meta = {
  __typename?: 'Meta';
  description?: Maybe<Scalars['String']>;
  h1: Scalars['String'];
  title: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  cartQuantity: Array<CartProduct>;
  clearCart?: Maybe<Scalars['Boolean']>;
};


export type MutationCartQuantityArgs = {
  priceId: Scalars['ID'];
  productId: Scalars['ID'];
  quantity: Scalars['Int'];
};

export type Price = {
  __typename?: 'Price';
  id: Scalars['ID'];
  oldPrice?: Maybe<Scalars['Float']>;
  price?: Maybe<Scalars['Float']>;
  size?: Maybe<Scalars['String']>;
  weekPrice: Scalars['Boolean'];
};

export type Product = {
  __typename?: 'Product';
  category: ProductCategory;
  categoryId: Scalars['ID'];
  description?: Maybe<Scalars['String']>;
  hasAttributes: Scalars['Boolean'];
  id: Scalars['ID'];
  image: Scalars['String'];
  images: Array<ProductImage>;
  isSameSize: Scalars['Boolean'];
  meta: Meta;
  name: Scalars['String'];
  prices: Array<Price>;
  salePrice?: Maybe<Price>;
  showSalePrice: Scalars['Boolean'];
  squaredImage: SquaredProductImage;
  varieties: Array<Variety>;
};


export type ProductPricesArgs = {
  ids?: Maybe<Array<Scalars['ID']>>;
};

export type ProductAttribute = {
  __typename?: 'ProductAttribute';
  name: Scalars['String'];
  text: Scalars['String'];
};

export type ProductCategory = Category & {
  __typename?: 'ProductCategory';
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  image: Scalars['String'];
  meta: Meta;
  name: Scalars['String'];
  products: Array<Product>;
};

export type ProductImage = {
  __typename?: 'ProductImage';
  large: Scalars['String'];
  small: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  awards: Array<Award>;
  careSeason?: Maybe<CareSeason>;
  careSeasons: Array<CareSeason>;
  cartProducts: Array<CartProduct>;
  categories: Array<Category>;
  category?: Maybe<Category>;
  clientPhotos: Array<ClientPhoto>;
  clients: Array<Client>;
  documents: Array<Document>;
  exclusive?: Maybe<Exclusive>;
  featuredProducts: Array<Product>;
  gallery: Array<GalleryItem>;
  galleryItem?: Maybe<GalleryItem>;
  product?: Maybe<Product>;
  productAttributes: Array<ProductAttribute>;
  productCategories: Array<ProductCategory>;
  products: Array<Product>;
  search: Array<Product>;
  service?: Maybe<Service>;
  variety?: Maybe<Variety>;
  videoReviews: Array<VideoReview>;
  villages: Array<Village>;
};


export type QueryCareSeasonArgs = {
  id: Scalars['ID'];
};


export type QueryCategoriesArgs = {
  ids?: Maybe<Array<Scalars['ID']>>;
};


export type QueryCategoryArgs = {
  id: Scalars['ID'];
};


export type QueryExclusiveArgs = {
  id: Scalars['ID'];
};


export type QueryFeaturedProductsArgs = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
};


export type QueryGalleryArgs = {
  id: Scalars['ID'];
};


export type QueryGalleryItemArgs = {
  id: Scalars['ID'];
};


export type QueryProductArgs = {
  id: Scalars['ID'];
};


export type QueryProductAttributesArgs = {
  productId: Scalars['ID'];
};


export type QueryProductCategoriesArgs = {
  ids?: Maybe<Array<Scalars['ID']>>;
};


export type QueryProductsArgs = {
  ids: Array<Scalars['ID']>;
};


export type QuerySearchArgs = {
  query: Scalars['String'];
};


export type QueryServiceArgs = {
  id: Scalars['ID'];
};


export type QueryVarietyArgs = {
  id: Scalars['ID'];
};

export type ResponsiveImageVariants = {
  __typename?: 'ResponsiveImageVariants';
  desktop?: Maybe<ImageWithSize>;
  mobile?: Maybe<ImageWithSize>;
};

export type Service = AbstractService & {
  __typename?: 'Service';
  content: Array<ServiceContent>;
  id: Scalars['ID'];
  images: Array<ServiceImage>;
  name: Scalars['String'];
  price?: Maybe<Scalars['String']>;
};

export type ServiceCategory = Category & {
  __typename?: 'ServiceCategory';
  id: Scalars['ID'];
  image: Scalars['String'];
  name: Scalars['String'];
  services: Array<Service>;
};

export type ServiceContent = ServiceGallery | ServiceText;

export type ServiceGallery = {
  __typename?: 'ServiceGallery';
  images: Array<ServiceImage>;
};

export type ServiceImage = {
  __typename?: 'ServiceImage';
  large: Scalars['String'];
  original: Scalars['String'];
  small: Scalars['String'];
};

export type ServiceText = {
  __typename?: 'ServiceText';
  text: Scalars['String'];
};

export type SquaredProductImage = {
  __typename?: 'SquaredProductImage';
  original: Scalars['String'];
  thumb: Scalars['String'];
};

export type ThumbnailImage = {
  __typename?: 'ThumbnailImage';
  large: ImageWithSize;
  thumb: ImageWithSize;
};

export type Variety = {
  __typename?: 'Variety';
  description: Array<VarietyAttribute>;
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type VarietyAttribute = {
  __typename?: 'VarietyAttribute';
  name: Scalars['String'];
  value: Scalars['String'];
};

export type Video = FileVideo | YoutubeVideo;

export type VideoReview = {
  __typename?: 'VideoReview';
  person?: Maybe<Scalars['String']>;
  poster: ResponsiveImageVariants;
  role?: Maybe<Scalars['String']>;
  video: Video;
};

export type Village = {
  __typename?: 'Village';
  image: ThumbnailImage;
  title: Scalars['String'];
};

export type YoutubeVideo = {
  __typename?: 'YoutubeVideo';
  code: Scalars['String'];
};

export type CartQuantityMutationVariables = Exact<{
  productId: Scalars['ID'];
  priceId: Scalars['ID'];
  quantity: Scalars['Int'];
}>;


export type CartQuantityMutation = { __typename?: 'Mutation', cartQuantity: Array<{ __typename?: 'CartProduct', productId: string, prices: Array<{ __typename?: 'CartPrice', priceId: string, quantity: number }> }> };

export type ClearCartMutationVariables = Exact<{ [key: string]: never; }>;


export type ClearCartMutation = { __typename?: 'Mutation', clearCart?: boolean | null | undefined };

export type AwardsQueryVariables = Exact<{ [key: string]: never; }>;


export type AwardsQuery = { __typename?: 'Query', awards: Array<{ __typename?: 'Award', image: { __typename?: 'ThumbnailImage', thumb: { __typename?: 'ImageWithSize', src: string, width: number, height: number }, large: { __typename?: 'ImageWithSize', src: string, width: number, height: number } } }> };

export type CareSeasonQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type CareSeasonQuery = { __typename?: 'Query', careSeason?: { __typename?: 'CareSeason', id: string, title: string, blocks: Array<{ __typename?: 'CareSeasonBlock', title: string, text: string, images: Array<{ __typename?: 'ImageWithSize', src: string, width: number, height: number }> }> } | null | undefined };

export type CareSeasonsQueryVariables = Exact<{ [key: string]: never; }>;


export type CareSeasonsQuery = { __typename?: 'Query', careSeasons: Array<{ __typename?: 'CareSeason', id: string, title: string }> };

export type CartProductsQueryVariables = Exact<{ [key: string]: never; }>;


export type CartProductsQuery = { __typename?: 'Query', cartProducts: Array<{ __typename?: 'CartProduct', productId: string, prices: Array<{ __typename?: 'CartPrice', priceId: string, quantity: number }> }> };

export type CategoriesQueryVariables = Exact<{
  ids?: Maybe<Array<Scalars['ID']> | Scalars['ID']>;
}>;


export type CategoriesQuery = { __typename?: 'Query', categories: Array<{ __typename?: 'ExclusiveCategory', id: string, name: string, image: string } | { __typename?: 'ProductCategory', id: string, name: string, image: string } | { __typename?: 'ServiceCategory', id: string, name: string, image: string }> };

export type CategoryQueryVariables = Exact<{
  id: Scalars['ID'];
  withMeta?: Maybe<Scalars['Boolean']>;
}>;


export type CategoryQuery = { __typename?: 'Query', category?: { __typename?: 'ExclusiveCategory', id: string, name: string, image: string, exclusives: Array<{ __typename?: 'Exclusive', id: string, price?: string | null | undefined, name: string, images: Array<{ __typename?: 'ServiceImage', small: string, large: string, original: string }> }> } | { __typename?: 'ProductCategory', description?: string | null | undefined, id: string, name: string, image: string, products: Array<{ __typename?: 'Product', id: string, name: string, categoryId: string, image: string }>, meta?: { __typename?: 'Meta', title: string, h1: string, description?: string | null | undefined } } | { __typename?: 'ServiceCategory', id: string, name: string, image: string, services: Array<{ __typename?: 'Service', id: string, price?: string | null | undefined, name: string, images: Array<{ __typename?: 'ServiceImage', small: string, large: string, original: string }> }> } | null | undefined };

export type CategoryNameQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type CategoryNameQuery = { __typename?: 'Query', category?: { __typename?: 'ExclusiveCategory', id: string, name: string } | { __typename?: 'ProductCategory', id: string, name: string } | { __typename?: 'ServiceCategory', id: string, name: string } | null | undefined };

export type ClientPhotosQueryVariables = Exact<{ [key: string]: never; }>;


export type ClientPhotosQuery = { __typename?: 'Query', clientPhotos: Array<{ __typename?: 'ClientPhoto', name: string, image: { __typename?: 'ThumbnailImage', thumb: { __typename?: 'ImageWithSize', src: string, width: number, height: number }, large: { __typename?: 'ImageWithSize', src: string, width: number, height: number } } }> };

export type ClientsQueryVariables = Exact<{ [key: string]: never; }>;


export type ClientsQuery = { __typename?: 'Query', clients: Array<{ __typename?: 'Client', title?: string | null | undefined, isState: boolean, image: { __typename?: 'ThumbnailImage', thumb: { __typename?: 'ImageWithSize', src: string, width: number, height: number }, large: { __typename?: 'ImageWithSize', src: string, width: number, height: number } } }> };

export type DocumentsQueryVariables = Exact<{ [key: string]: never; }>;


export type DocumentsQuery = { __typename?: 'Query', documents: Array<{ __typename?: 'Document', image: { __typename?: 'ThumbnailImage', thumb: { __typename?: 'ImageWithSize', src: string, width: number, height: number }, large: { __typename?: 'ImageWithSize', src: string, width: number, height: number } } }> };

export type ExclusiveQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ExclusiveQuery = { __typename?: 'Query', exclusive?: { __typename?: 'Exclusive', id: string, name: string, price?: string | null | undefined, images: Array<{ __typename?: 'ServiceImage', small: string, large: string, original: string }> } | null | undefined };

export type FeaturedProductsQueryVariables = Exact<{ [key: string]: never; }>;


export type FeaturedProductsQuery = { __typename?: 'Query', featuredProducts: Array<{ __typename?: 'Product', id: string, name: string, categoryId: string, image: string, salePrice?: { __typename?: 'Price', size?: string | null | undefined, price?: number | null | undefined, oldPrice?: number | null | undefined } | null | undefined }> };

export type GalleryQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GalleryQuery = { __typename?: 'Query', gallery: Array<{ __typename?: 'GalleryItem', id: string, title: string, content?: string | null | undefined, image: { __typename?: 'ThumbnailImage', thumb: { __typename?: 'ImageWithSize', src: string, width: number, height: number }, large: { __typename?: 'ImageWithSize', src: string, width: number, height: number } } }> };

export type GalleryItemQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GalleryItemQuery = { __typename?: 'Query', galleryItem?: { __typename?: 'GalleryItem', id: string, title: string, content?: string | null | undefined, image: { __typename?: 'ThumbnailImage', thumb: { __typename?: 'ImageWithSize', src: string, width: number, height: number }, large: { __typename?: 'ImageWithSize', src: string, width: number, height: number } } } | null | undefined };

export type ProductAttributesQueryVariables = Exact<{
  productId: Scalars['ID'];
}>;


export type ProductAttributesQuery = { __typename?: 'Query', productAttributes: Array<{ __typename?: 'ProductAttribute', name: string, text: string }> };

export type ProductInfoQueryVariables = Exact<{
  id: Scalars['ID'];
  withMeta?: Maybe<Scalars['Boolean']>;
}>;


export type ProductInfoQuery = { __typename?: 'Query', product?: { __typename?: 'Product', description?: string | null | undefined, id: string, name: string, image: string, hasAttributes: boolean, isSameSize: boolean, categoryId: string, showSalePrice: boolean, squaredImage: { __typename?: 'SquaredProductImage', thumb: string, original: string }, images: Array<{ __typename?: 'ProductImage', small: string, large: string }>, prices: Array<{ __typename?: 'Price', id: string, size?: string | null | undefined, price?: number | null | undefined, weekPrice: boolean }>, salePrice?: { __typename?: 'Price', price?: number | null | undefined, size?: string | null | undefined, oldPrice?: number | null | undefined } | null | undefined, varieties: Array<{ __typename?: 'Variety', id: string, name: string }>, meta?: { __typename?: 'Meta', title: string, h1: string, description?: string | null | undefined } } | null | undefined };

export type ProductInfoWithCategoryQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ProductInfoWithCategoryQuery = { __typename?: 'Query', product?: { __typename?: 'Product', id: string, name: string, image: string, hasAttributes: boolean, isSameSize: boolean, showSalePrice: boolean, images: Array<{ __typename?: 'ProductImage', small: string, large: string }>, prices: Array<{ __typename?: 'Price', id: string, size?: string | null | undefined, price?: number | null | undefined, weekPrice: boolean }>, salePrice?: { __typename?: 'Price', price?: number | null | undefined, size?: string | null | undefined, oldPrice?: number | null | undefined } | null | undefined, category: { __typename?: 'ProductCategory', id: string, name: string, image: string, products: Array<{ __typename?: 'Product', id: string, name: string, categoryId: string, image: string }> } } | null | undefined };

export type ProductsInCartQueryVariables = Exact<{
  ids: Array<Scalars['ID']> | Scalars['ID'];
  priceIds: Array<Scalars['ID']> | Scalars['ID'];
}>;


export type ProductsInCartQuery = { __typename?: 'Query', products: Array<{ __typename?: 'Product', id: string, name: string, image: string, prices: Array<{ __typename?: 'Price', id: string, size?: string | null | undefined, price?: number | null | undefined }> }> };

export type SearchProductQueryVariables = Exact<{
  search: Scalars['String'];
}>;


export type SearchProductQuery = { __typename?: 'Query', search: Array<{ __typename?: 'Product', id: string, name: string }> };

export type ServiceQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ServiceQuery = { __typename?: 'Query', service?: { __typename?: 'Service', id: string, name: string, price?: string | null | undefined, images: Array<{ __typename?: 'ServiceImage', small: string, large: string, original: string }>, content: Array<{ __typename?: 'ServiceGallery', images: Array<{ __typename?: 'ServiceImage', small: string, large: string, original: string }> } | { __typename?: 'ServiceText', text: string }> } | null | undefined };

export type VarietyInfoQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type VarietyInfoQuery = { __typename?: 'Query', variety?: { __typename?: 'Variety', id: string, name: string, description: Array<{ __typename?: 'VarietyAttribute', name: string, value: string }> } | null | undefined };

export type VideoReviewsQueryVariables = Exact<{ [key: string]: never; }>;


export type VideoReviewsQuery = { __typename?: 'Query', videoReviews: Array<{ __typename?: 'VideoReview', person?: string | null | undefined, video: { __typename?: 'FileVideo', source: string } | { __typename?: 'YoutubeVideo', code: string }, poster: { __typename?: 'ResponsiveImageVariants', mobile?: { __typename?: 'ImageWithSize', src: string, width: number, height: number } | null | undefined, desktop?: { __typename?: 'ImageWithSize', src: string, width: number, height: number } | null | undefined } }> };

export type VillagesQueryVariables = Exact<{ [key: string]: never; }>;


export type VillagesQuery = { __typename?: 'Query', villages: Array<{ __typename?: 'Village', title: string, image: { __typename?: 'ThumbnailImage', thumb: { __typename?: 'ImageWithSize', src: string, width: number, height: number }, large: { __typename?: 'ImageWithSize', src: string, width: number, height: number } } }> };
