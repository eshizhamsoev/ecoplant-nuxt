import fetch from 'node-fetch'
import { ServerMiddleware } from '@nuxt/types'
import { SERVICE_CATEGORY_ID } from '../data/const/servicesCategory'
import { EXCLUSIVE_CATEGORY_ID } from '../data/const/exclusivesCategory'
import { CategoryPageType } from '../data/const/page-types/category'
import { tabsConfigFactory } from './configs/tabs'
import { getGallery } from './configs/galleries'
import { getGrassPrices, getGrassServices } from './configs/grass'

import { ExternalLandingConfig } from '~/common-types/external-data/landing-config/data'
import { LandingGroup } from '~/common-types/common/LandingGroup'
import { ServerLandingConfig } from '~/common-types/server/landing-config'
import { CategoryId } from '~/common-types/catalog'

const landings = new Map()

enum SiteType {
  THUJA = 'thuja',
  LARGE = 'large',
  LEAF = 'leaf',
  BUSH = 'bush',
  GRASS = 'grass',
  INSTRUCTION = 'instruction',
  BARK_BEETLE = 'bark-beetle',
  GUARANTEE = 'guarantee',
  GUARANTEE2 = 'guarantee-2',
  JULY_CARE = 'july-care',
  BLACK_FRIDAY = 'black-friday',
  NEWSLETTER_PRESENT = 'newsletter-present',
  SEO = 'seo',
  OPT = 'opt',
  ECO = 'eco',
  CARE = 'care',
  LANDSCAPE = 'landscape'
}

function getType(id: number): SiteType {
  switch (id) {
    case 6:
    case 7:
    case 8:
      return SiteType.THUJA
    case 9:
    case 10:
    case 11:
      return SiteType.LARGE
    case 12:
    case 13:
    case 14:
      return SiteType.LEAF
    case 15:
    case 16:
    case 17:
      return SiteType.BUSH
    case 62:
    case 63:
      return SiteType.GRASS
    case 65:
    case 66:
      return SiteType.INSTRUCTION
    case 67:
    case 68:
      return SiteType.BARK_BEETLE
    case 69:
      return SiteType.GUARANTEE
    case 70:
    case 71:
      return SiteType.GUARANTEE2
    case 72:
    case 73:
      return SiteType.JULY_CARE
    case 45:
    case 46:
      return SiteType.BLACK_FRIDAY
    case 76:
    case 77:
      return SiteType.NEWSLETTER_PRESENT
    case 78:
      return SiteType.SEO
    case 21:
    case 38:
      return SiteType.OPT
    case 1:
      return SiteType.CARE
    case 80:
    case 81:
      return SiteType.LANDSCAPE
    case 18:
    case 19:
    case 20:
    default:
      return SiteType.ECO
  }
}

function getContacts(data: ExternalLandingConfig) {
  const contacts = data.contacts
  return {
    siteName: data.store.componentData.header.siteName
      .replace('<br>', '\n')
      .replace(/<\/?span>/gi, '\n')
      .replace(/\n\s+/gi, '\n')
      .replace(/^\n/, ''),
    phone: contacts.phone,
    email: contacts.email,
    address: contacts.address.text,
    geoPoint: {
      latitude: +contacts.address.latitude,
      longitude: +contacts.address.longitude
    },
    whatsApp: contacts.whatsapp,
    legal:
      contacts.legal.info.replace('<br>', '\n') +
      '\n' +
      contacts.legal.name.replace('<br>', '\n')
  }
}

const getHeading = (data: ExternalLandingConfig) => ({
  name: 'BlockHeading',
  props: {
    slogan: data.store.componentData.header.siteName
      .replace('<br>', '\n')
      .replace(/<\/?span>/gi, '\n')
      .replace(/\n\s+/gi, '\n')
      .replace(/^\n/, ''),
    heading: data.store.componentData.header.heading.replace('<br>', '\n'),
    leading: data.store.componentData.header.leading.replace('<br>', '\n'),
    background: 'https://ecoplant-pitomnik.ru/' + data.heading.image,
    mobileBackground:
      'https://ecoplant-pitomnik.ru/' + data.heading.mobileImage.src
  },
  isMobile: true,
  isDesktop: true
})

const getCategoryConfigByIds = (
  homeCategories: CategoryId[],
  availableCategories: CategoryId[]
) => {
  const currentCategory = homeCategories[0]
  return [
    {
      name: 'BlockCatalog',
      props: {
        currentCategoryId: currentCategory,
        categoryIds: availableCategories
      },
      isMobile: true,
      isDesktop: false
    },
    ...homeCategories.map((id) => ({
      name: 'BlockCategory',
      props: {
        categoryId: id.toString()
      },
      isMobile: true,
      isDesktop: true
    }))
  ]
}

function getCategoryIds(data: ExternalLandingConfig) {
  let categories: CategoryId[] = []
  data.pages.home.components.forEach((v) => {
    if (v.name === 'category') {
      categories.push(v.properties.categoryId)
    }
  })
  if (!categories.length) {
    categories = ['2', '3', '1', '4', '5']
  }
  return [...categories, EXCLUSIVE_CATEGORY_ID, SERVICE_CATEGORY_ID]
}

const getPlantingConfig = (type: string) => {
  switch (type) {
    case 'large':
      return {
        video: 'https://ecoplant-pitomnik.ru/files/videos/planting/01.mp4',
        poster:
          'https://ecoplant-pitomnik.ru/image/catalog/blocks/planting/videos/new-covers/large.jpg',
        buttonText: 'Посадка\nкрупномеров'
      }
    case 'bush':
      return {
        video: 'https://ecoplant-pitomnik.ru/files/videos/planting/03.mp4',
        poster:
          'https://ecoplant-pitomnik.ru/image/catalog/blocks/planting/videos/new-covers/leaf.jpg',
        buttonText: 'Посадка\nкустарников'
      }
    case 'thuja':
      return {
        video: 'https://ecoplant-pitomnik.ru/files/videos/planting/02.mp4',
        poster:
          'https://ecoplant-pitomnik.ru/image/catalog/blocks/planting/videos/new-covers/thuja.jpg',
        buttonText: 'Посадка\nтуй'
      }
    case 'eco':
    case 'leaf':
    default:
      return {
        video: 'https://ecoplant-pitomnik.ru/files/videos/planting/03.mp4',
        poster:
          'https://ecoplant-pitomnik.ru/image/catalog/blocks/planting/videos/new-covers/leaf.jpg',
        buttonText: 'Посадка\nлиственных'
      }
  }
}

const getClientPhotosComponent = () => {
  return [
    {
      name: 'BlockClients',
      props: {},
      isMobile: true,
      isDesktop: true
    }
  ]
}

const getDefaultLayoutBlocks = (data: ExternalLandingConfig) => [
  tabsConfigFactory({}),
  {
    name: 'BlockFeaturedProducts',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockPhoneButton',
    isMobile: true
  },
  ...getGallery(getType(+data.configId)),
  {
    name: 'BlockMainClient',
    isMobile: true,
    isDesktop: true
  },
  ...getClientPhotosComponent(),
  {
    name: 'BlockPhoneButton',
    isMobile: true
  },
  {
    name: 'BlockSale',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockContacts',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockDirector',
    props: {
      stamp:
        data.group === 'spb'
          ? 'https://ecoplant-pitomnik.ru/image/catalog/blocks/director/stamps/stamp-spb.png'
          : 'https://ecoplant-pitomnik.ru/image/catalog/blocks/director/stamps/stamp-msk.png'
    },
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockLandscapeSecrets',
    isDesktop: true,
    isMobile: true
  },
  {
    name: 'BlockFooter',
    isMobile: true,
    isDesktop: true,
    props: {
      withLinks: getType(+data.configId) === 'seo'
    }
  },
  {
    name: 'BlockCartButton',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockMobileFixedLine',
    isMobile: true,
    isDesktop: false
  }
]

const BlockContent = {
  name: 'BlockContent',
  isMobile: true,
  isDesktop: true
}

const getLayoutComponents = (data: ExternalLandingConfig) => {
  return [
    getHeading(data),
    BlockContent,
    {
      name: 'BlockCategories',
      isMobile: true,
      isDesktop: true,
      props: {
        ids: ['2', '3', '1', '4', '5']
      }
    },
    ...getDefaultLayoutBlocks(data)
  ]
}

const getGrass = (group: LandingGroup, mainPage: boolean) => [
  {
    name: 'BlockHeading',
    isMobile: false,
    isDesktop: true,
    props: {
      heading: `РУЛОННЫЙ ГАЗОН «ПОД КЛЮЧ» ${
        group === 'spb' ? '' : 'ВЫЕЗД И ПРОЕКТ БЕСПЛАТНО'
      }`,
      leading: 'Без посредников и переплаты',
      withSaleButton: false,
      background:
        'https://ecoplant-pitomnik.ru/image/catalog/landings/headings/grass-desktop.jpg'
    }
  },
  {
    name: 'BlockHeading',
    isMobile: true,
    isDesktop: false,
    props: {
      heading: `РУЛОННЫЙ ГАЗОН «ПОД КЛЮЧ» ${
        group === 'spb' ? '' : 'ВЫЕЗД И ПРОЕКТ БЕСПЛАТНО'
      }`,
      leading: 'Без посредников и переплаты',
      background:
        'https://ecoplant-pitomnik.ru/image/catalog/landings/headings/grass.jpg',
      withBottomLine: false,
      withOverlay: false,
      topLineOptions: {
        hideElements: ['water'],
        withNavigation: false
      }
    }
  },
  {
    name: 'BlockGrass',
    isMobile: true,
    isDesktop: true,
    props: {
      withBackButton: !mainPage,
      structure: {
        sizeText:
          group === 'spb'
            ? '0,4 x 2,0 м и 0,4 x 2,5 м'
            : '0,4 x 2,5 м и 0,6 х 1,67 м',
        widthText: group === 'spb' ? '0,40м' : '0,40м (0,60м)',
        lengthText: group === 'spb' ? '2.0м  (2,5м)' : '2.50м  (1,67м)'
      },
      pallet: {
        deliverLabel:
          group === 'spb'
            ? 'В один манипулятор можно загрузить'
            : 'В одну грузовую машину можно загрузить',
        deliverySize: group === 'spb' ? '7 поддонов' : 'около 24-25 поддонов',
        deliveryAdditionalInfo:
          group === 'spb'
            ? '315 m2 в зависимости от погоды'
            : '960-1000 m2 в зависимости от погоды',
        currierSize: null
      }
    }
  },
  {
    name: 'BlockCategories',
    isMobile: true,
    isDesktop: false,
    props: {
      ids: ['2', '3', '1', '4', '5', EXCLUSIVE_CATEGORY_ID, SERVICE_CATEGORY_ID]
    }
  },
  {
    name: 'BlockCategories',
    isMobile: false,
    isDesktop: true,
    props: {
      ids: ['2', '3', '1', '4', '5']
    }
  },
  {
    name: 'BlockFooter',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockMobileFixedLine',
    isMobile: true,
    isDesktop: false
  }
]

const getBarkBeetle = () => [
  {
    name: 'BlockBarkBeetle',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockCategories',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockFooter',
    isMobile: true,
    isDesktop: true,
    props: {
      showBothContacts: false
    }
  }
]

const getInstruction = () => [
  {
    name: 'BlockHeadingOneLine',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockGoogleTableInstructions',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockCategories',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockPlantingCareRecommendation',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockFooter',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockMobileFixedLine',
    isMobile: true,
    isDesktop: false
  }
]

const getGuarantee = () => [
  {
    name: 'BlockHeading',
    isMobile: true,
    isDesktop: true,
    props: {
      heading: '',
      leading:
        'Экоплант начнёт гарантийные выезды с 10 июня в связи с обстановкой в стране. Мы про Вас помним и хорошего Вам дня. Посмотрите что нужно делать с растениями',
      background:
        'https://ecoplant-pitomnik.ru/image/catalog/landings/headings/grass.jpg',
      withBottomLine: false,
      topLineOptions: {
        hideElements: ['email', 'phone']
      }
    }
  },
  {
    name: 'BlockPlantingCareRecommendation',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockFooter',
    isMobile: true,
    isDesktop: true
  }
]

const getLandscape = () => [
  // {
  //   name: 'BlockHeading',
  //   isMobile: true,
  //   isDesktop: true,
  //   props: {
  //     heading: '',
  //     leading:
  //       'Экоплант начнёт гарантийные выезды с 10 июня в связи с обстановкой в стране. Мы про Вас помним и хорошего Вам дня. Посмотрите что нужно делать с растениями',
  //     background:
  //       'https://ecoplant-pitomnik.ru/image/catalog/landings/headings/grass.jpg',
  //     withBottomLine: false,
  //     topLineOptions: {
  //       hideElements: ['email', 'phone']
  //     }
  //   }
  // },
  {
    name: 'BlockLandscapeHeading',
    isDesktop: true,
    isMobile: true
  },
  {
    name: 'BlockLandscapeServices',
    isDesktop: true,
    isMobile: true
  },
  {
    name: 'BlockLandscapeAdvantages',
    isDesktop: true,
    isMobile: true
  },
  {
    name: 'BlockLandscapePartnership',
    isDesktop: true,
    isMobile: true
  },
  {
    name: 'BlockLandscapeCalculate',
    isDesktop: true,
    isMobile: true
  },
  {
    name: 'BlockLandscapePopups',
    isDesktop: true,
    isMobile: true
  },
  {
    name: 'BlockLandscapeExamples',
    isDesktop: true,
    isMobile: true
  },
  {
    name: 'BlockLandscapeDendrologist',
    isDesktop: true,
    isMobile: true
  },
  {
    name: 'BlockLandscapeSecrets',
    isDesktop: true,
    isMobile: true
  },
  {
    name: 'BlockFooter',
    isMobile: true,
    isDesktop: true
  }
]

const getBlackFriday = (data: ExternalLandingConfig) => [
  {
    name: 'BlockBlackFriday',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockDirector',
    props: {
      stamp:
        data.group === 'spb'
          ? 'https://ecoplant-pitomnik.ru/image/catalog/blocks/director/stamps/stamp-spb.png'
          : 'https://ecoplant-pitomnik.ru/image/catalog/blocks/director/stamps/stamp-msk.png'
    },
    isMobile: true,
    isDesktop: true
  },
  ...getGallery(getType(+data.configId)),
  {
    name: 'BlockMainClient',
    isMobile: true,
    isDesktop: true
  },
  ...getClientPhotosComponent(),
  {
    name: 'BlockPhoneButton',
    isMobile: true
  },
  tabsConfigFactory({}),
  {
    name: 'BlockFooter',
    props: {
      withSaleFir: +data.configId === 18
    },
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockMobileFixedLine',
    isMobile: true,
    isDesktop: false,
    props: {
      withCart: false
    }
  }
]

const getJulyCare = (data: ExternalLandingConfig) => [
  {
    name: 'BlockHeading',
    isMobile: true,
    isDesktop: true,
    props: {
      heading: data.store.componentData.header.heading,
      leading: '',
      background: 'https://ecoplant-pitomnik.ru/' + data.heading.image,
      withBottomLine: false
      // topLineOptions: {
      //   hideElements: ['email', 'phone']
      // }
    }
  },
  {
    name: 'BlockVideo',
    props: {
      heading: 'Хвойные (сосны, ели)',
      video: 'https://ecoplant-pitomnik.ru/files/videos/july-care/1.mp4',
      poster: 'https://ecoplant-pitomnik.ru/files/videos/july-care/1.jpg'
    },
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockVideo',
    props: {
      heading: 'Туи',
      video: 'https://ecoplant-pitomnik.ru/files/videos/july-care/2.mp4',
      poster: 'https://ecoplant-pitomnik.ru/files/videos/july-care/2.jpg'
    },
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockVideo',
    props: {
      heading: 'Лиственные',
      video: 'https://ecoplant-pitomnik.ru/files/videos/july-care/3.mp4',
      poster: 'https://ecoplant-pitomnik.ru/files/videos/july-care/3.jpg'
    },
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockVideo',
    props: {
      heading: 'Кустарники',
      video: 'https://ecoplant-pitomnik.ru/files/videos/july-care/4.mp4',
      poster: 'https://ecoplant-pitomnik.ru/files/videos/july-care/4.jpg'
    },
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockCategories',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockPhoneButton',
    isMobile: true
  },
  tabsConfigFactory({}),
  {
    name: 'BlockSale',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockDirector',
    props: {
      stamp:
        data.group === 'spb'
          ? 'https://ecoplant-pitomnik.ru/image/catalog/blocks/director/stamps/stamp-spb.png'
          : 'https://ecoplant-pitomnik.ru/image/catalog/blocks/director/stamps/stamp-msk.png'
    },
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockFooter',
    isMobile: true,
    isDesktop: true
  },
  {
    name: 'BlockMobileFixedLine',
    isMobile: true,
    isDesktop: false,
    props: {
      withCart: false
    }
  }
]

const getNewsletterPresentSite = (data: ExternalLandingConfig) => {
  const headingData = getHeading(data)
  return [
    {
      ...headingData,
      props: {
        ...headingData.props,
        heading: '',
        leading: '',
        withBottomLine: false,
        withScrollDownArrow: false,
        withSaleButton: false,
        topLineOptions: {
          // withNavigation: true
        }
      }
    },
    {
      name: 'BlockPresent',
      isMobile: true,
      isDesktop: true
    },
    ...getLayoutComponents(data).slice(1)
  ]
}

const seoComponents = (data: ExternalLandingConfig) => [
  getHeading(data),
  {
    name: 'BlockCategories',
    isMobile: true,
    isDesktop: false,
    props: {
      showBlockHeading: false,
      ids: ['2', '3', '1', '4', '5', EXCLUSIVE_CATEGORY_ID, SERVICE_CATEGORY_ID]
    }
  },
  {
    name: 'BlockCategories',
    isMobile: false,
    isDesktop: true,
    props: {
      showBlockHeading: false,
      ids: ['2', '3', '1', '4', '5']
    }
  }
]

function convertImage(image: any) {
  if (typeof image === 'string') {
    return image[0] === '/' ? 'https://ecoplant-pitomnik.ru' + image : image
  }
  if (typeof image !== 'object' || !image) {
    return null
  }
  const newObject = { ...image }
  if (newObject.small) {
    newObject.small = convertImage(newObject.small)
  }
  if (newObject.large) {
    newObject.large = convertImage(newObject.large)
  }
  if (newObject.src) {
    newObject.src = convertImage(newObject.src)
  }
  return newObject
}

function getOpt(data: ExternalLandingConfig) {
  const components = data.pages.home.components
    .filter((component) =>
      ['photo-grid', 'photo-slider'].includes(component.name)
    )
    .map((item) => ({
      name:
        item.name === 'photo-slider'
          ? 'BlockSlider'
          : 'UniversalBlockImageGrid',
      isMobile: true,
      isDesktop: true,
      props: {
        ...item.properties,
        label: !!item.properties.label,
        images: item.properties.images.map((image: any) => convertImage(image))
      }
    }))
  const heading = getHeading(data)
  return [
    {
      ...heading,
      props: {
        ...heading.props,
        bottomIconsType: 'opt'
      }
    },
    ...getCategoryConfigByIds(
      ['1', '4', '3', '2', '5'],
      ['1', '4', '3', '2', '5', '6', '7']
    ),
    {
      name: 'BlockCategories',
      isMobile: true,
      isDesktop: true,
      props: {
        ids: ['1', '4', '3', '2', '5', '6', '7']
      }
    },
    tabsConfigFactory({
      hide: ['BlockClientLogos']
    }),
    {
      name: 'BlockPhoneButton',
      isMobile: true
    },
    ...getGallery(getType(+data.configId)),
    {
      name: 'BlockMainClient',
      isMobile: true,
      isDesktop: true
    },
    ...getClientPhotosComponent(),
    {
      name: 'BlockPhoneButton',
      isMobile: true
    },
    {
      name: 'BlockSale',
      isMobile: true,
      isDesktop: true
    },
    ...components,
    {
      name: 'BlockDirector',
      props: {
        stamp:
          data.group === 'spb'
            ? 'https://ecoplant-pitomnik.ru/image/catalog/blocks/director/stamps/stamp-spb.png'
            : 'https://ecoplant-pitomnik.ru/image/catalog/blocks/director/stamps/stamp-msk.png'
      },
      isMobile: true,
      isDesktop: true
    },
    {
      name: 'BlockFooter',
      isMobile: true,
      isDesktop: true,
      props: {
        showBothContacts: false
      }
    },
    {
      name: 'BlockCartButton',
      isMobile: true,
      isDesktop: true
    },
    {
      name: 'BlockMobileFixedLine',
      isMobile: true,
      isDesktop: false
    }
  ]
}

function getCareComponents(data: ExternalLandingConfig) {
  return [
    getHeading(data),
    {
      name: 'BlockCareSeasons',
      isMobile: true,
      isDesktop: true
    },
    {
      name: 'BlockHowToWater',
      isMobile: true,
      isDesktop: true
    },
    {
      name: 'BlockPlantingVideos',
      isMobile: true,
      isDesktop: true
    },
    {
      name: 'BlockCategories',
      isMobile: true,
      isDesktop: true,
      props: {
        ids: ['1', '4', '3', '2', '5', '6', '7']
      }
    },
    tabsConfigFactory({
      hide: ['BlockClientLogos']
    }),
    {
      name: 'BlockPhoneButton',
      isMobile: true
    },
    ...getGallery(getType(+data.configId)),
    {
      name: 'BlockMainClient',
      isMobile: true,
      isDesktop: true
    },
    ...getClientPhotosComponent(),
    {
      name: 'BlockPhoneButton',
      isMobile: true
    },
    {
      name: 'BlockSale',
      isMobile: true,
      isDesktop: true
    },
    {
      name: 'BlockFooter',
      isMobile: true,
      isDesktop: true,
      props: {
        showBothContacts: false
      }
    },
    {
      name: 'BlockCartButton',
      isMobile: true,
      isDesktop: true
    },
    {
      name: 'BlockMobileFixedLine',
      isMobile: true,
      isDesktop: false
    }
  ]
}

const homeLayoutComponents = (data: ExternalLandingConfig, type: string) => {
  const group = data.group
  switch (type) {
    case SiteType.GRASS:
      return getGrass(group, true)
    case SiteType.BARK_BEETLE:
      return getBarkBeetle()
    case SiteType.INSTRUCTION:
      return getInstruction()
    case SiteType.GUARANTEE:
      return getGuarantee()
    case SiteType.LANDSCAPE:
      return getLandscape()
    case SiteType.BLACK_FRIDAY:
      return getBlackFriday(data)
    case SiteType.JULY_CARE:
      return getJulyCare(data)
    case SiteType.CARE:
      return getCareComponents(data)
    case SiteType.NEWSLETTER_PRESENT:
      return getNewsletterPresentSite(data)
    case SiteType.SEO:
      return [BlockContent, ...getDefaultLayoutBlocks(data)]
    case SiteType.OPT:
      return getOpt(data)
    default:
      return null
  }
}

const homeComponents = (data: ExternalLandingConfig, type: string) => {
  const categoryIds = getCategoryIds(data)
  switch (type) {
    case 'seo':
      return seoComponents(data)
    default:
      return getCategoryConfigByIds(categoryIds, categoryIds)
  }
}

const layoutComponents = (data: ExternalLandingConfig, type: string) => {
  switch (type) {
    case 'seo':
      return [
        {
          name: 'BlockHeadingOneLine',
          isMobile: true,
          isDesktop: true
        },
        BlockContent,
        ...getDefaultLayoutBlocks(data)
      ]
    default:
      return getLayoutComponents(data)
  }
}

function categoryComponent(type: SiteType): CategoryPageType {
  switch (type) {
    case SiteType.SEO:
      return CategoryPageType.SEO
    default:
      return CategoryPageType.LANDING
  }
}

function showExpandedInformation(type: SiteType): boolean {
  return type === SiteType.SEO
}

function getHomeMeta(type: SiteType, siteName: string) {
  switch (type) {
    case SiteType.SEO:
      return {
        title:
          'Купить деревья и кустарники в питомнике растений в Москве - официальный сайт Ecoplant',
        description:
          'Саженцы деревьев и кустарников в питомнике растений Ecoplant ' +
          '✔ Привезем и посадим в течение одного дня, Москва и Московская область ' +
          '✔ Смотреть видеоотзыв Дмитрия Нагиева ✔ Звоните ☎ 8 (495) 374 57 28.'
      }
    default:
      return {
        title: siteName || 'Питомник растений',
        description: 'Питомник растений'
      }
  }
}

function convertExternalData(data: ExternalLandingConfig): ServerLandingConfig {
  const configId = +data.configId
  const type = getType(configId)

  const categoryIds = getCategoryIds(data)

  const contacts = getContacts(data)
  return {
    configId,
    type,
    categoryIds,
    mainCategoryId: categoryIds[0],
    contacts,
    layoutComponents: layoutComponents(data, type),
    homeLayoutComponents: homeLayoutComponents(data, type),
    homeComponents: homeComponents(data, type),
    grassComponents: getGrass(data.group, false),
    categoryComponent: categoryComponent(type),
    showExpandedInformation: showExpandedInformation(type),
    crmCode: data.crmCode,
    group: data.group,
    grass: {
      availableProducts: getGrassPrices(data.group),
      services: getGrassServices(data.group)
    },
    planting: getPlantingConfig(type),
    meta: {
      home: getHomeMeta(type, contacts.siteName)
    }
  }
}

const getConfig = async function (domainName: string) {
  // TODO add error handling, update config regularly, update config in bg
  if (landings.has(domainName)) {
    return landings.get(domainName)
  }
  const apiUrl = `http://ecoplant-pitomnik.ru/index.php?route=extension/feed/landing/by_site&site=${domainName}`
  const response = await fetch(apiUrl, {
    headers: { Accept: 'application/json' }
  })
  if (!response.ok) {
    throw new Error('Unknown domain')
  }
  const json = (await response.json()) as ExternalLandingConfig
  const data = convertExternalData(json)
  landings.set(domainName, data)
  return data
}

const landingConfigMiddleware: ServerMiddleware = async function (
  req,
  res,
  next
) {
  if (req.url === '/clear-config-cache') {
    landings.clear()
  }
  try {
    let host = req.headers.host
    if (!host) {
      throw new Error('Invalid host')
    }
    const pos = host?.lastIndexOf(':')
    if (pos !== -1) {
      host = host?.substring(0, pos)
    }
    // @ts-ignore
    req.data = await getConfig(host)
    next()
  } catch (e) {
    next({ statusCode: 404, message: 'not found' })
  }
}

export default landingConfigMiddleware
