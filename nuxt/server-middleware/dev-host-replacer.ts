import { ServerMiddleware } from '@nuxt/types'

const hostReplacer: ServerMiddleware = function (req, res, next) {
  if (process.env.CUSTOM_HOST) {
    req.headers.host = process.env.CUSTOM_HOST
  }
  next()
}

export default hostReplacer
