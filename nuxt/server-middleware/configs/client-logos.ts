export const clientLogos = {
  items: [
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/1-160x110.jpg',
      isState: true,
      title: ''
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/2-160x110.jpg',
      isState: true,
      title: ''
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/3-160x110.jpg',
      isState: true,
      title: ''
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/4-160x110.jpg',
      isState: false,
      title: ''
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/5-160x110.jpg',
      isState: false,
      title: ''
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/6-160x110.jpg',
      isState: false,
      title: ''
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/7-160x110.jpg',
      isState: false,
      title: ''
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/8-160x110.jpg',
      isState: false,
      title: ''
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/9-160x110.jpg',
      isState: false,
      title: ''
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/10-160x110.jpg',
      isState: false,
      title: ''
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/11-160x110.jpg',
      isState: false,
      title: ''
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/12-160x110.jpg',
      isState: false,
      title: ''
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/13-160x110.jpg',
      isState: false,
      title: ''
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/14-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u041A\u043E\u043B\u043E\u043C\u043D\u044B'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/15-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u0410\u0440\u0445\u0430\u043D\u0433\u0435\u043B\u044C\u0441\u043A\u0430'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/16-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u041C\u043E\u0441\u043A\u0432\u044B'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/17-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u0421\u0430\u043D\u043A\u0442-\u041F\u0435\u0442\u0435\u0440\u0431\u0443\u0440\u0433\u0430'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/18-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u041A\u0430\u0437\u0430\u043D\u0438'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/19-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u0422\u0443\u043B\u044B'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/20-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u0423\u0444\u044B'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/21-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u0418\u0432\u0430\u043D\u043E\u0432\u043E'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/22-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u041A\u0440\u0430\u0441\u043D\u043E\u0433\u043E\u0440\u0441\u043A\u0430'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/23-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u041C\u0430\u0433\u043D\u0438\u0442\u043E\u0433\u043E\u0440\u0441\u043A\u0430'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/24-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u0421\u0435\u0440\u0433\u0438\u0435\u0432\u0430 \u041F\u043E\u0441\u0430\u0434\u0430'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/25-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u0413\u0440\u043E\u0437\u043D\u043E\u0433\u043E'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/26-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u0420\u0436\u0435\u0432\u0430'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/27-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u041C\u0443\u0440\u0438\u043D\u043E'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/28-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u041F\u0438\u043A\u0430\u043B\u0451\u0432\u043E'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/29-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u041B\u043E\u0431\u043D\u0438'
    },
    {
      image:
        'https://ecoplant-pitomnik.ru/image/cache/catalog/modals/clients/30-160x110.jpg',
      isState: true,
      title:
        '\u0410\u0434\u043C\u0438\u043D\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F <Br>\u0412\u0441\u0435\u0432\u043E\u043B\u043E\u0436\u0441\u043A\u0430'
    }
  ]
}
