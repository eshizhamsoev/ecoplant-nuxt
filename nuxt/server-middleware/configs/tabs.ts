import { documents } from './documents'

type Settings = {
  hide?: string[]
}

export const tabsConfigFactory = ({ hide }: Settings) => ({
  name: 'BlockTabs',
  props: {
    showBlocks: [
      {
        name: 'BlockShipping',
        title: 'Доставка',
        isMobile: true,
        isDesktop: true
      },
      {
        name: 'BlockPlanting',
        title: 'Посадка',
        isMobile: true,
        isDesktop: true
      },
      {
        name: 'BlockDocuments',
        title: 'Докумены',
        props: documents,
        isMobile: true,
        isDesktop: true
      },
      {
        name: 'BlockClientLogos',
        title: 'Клиенты',
        isMobile: true,
        isDesktop: true
      },
      {
        name: 'BlockVideoReviews',
        title: 'Отзывы',
        props: {},
        isMobile: true,
        isDesktop: true
      },
      {
        name: 'BlockAwards',
        title: 'Награды',
        props: {},
        isMobile: true,
        isDesktop: true
      },
      {
        name: 'BlockCharitableFoundation',
        title: 'Фонд Хабенского',
        isMobile: true,
        isDesktop: true
      }
    ].filter(({ name }) => {
      return !hide || !hide.includes(name)
    })
  },
  isMobile: true,
  isDesktop: true
})
