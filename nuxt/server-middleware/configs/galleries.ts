const getDefaultSlider = (
  images: any,
  isMobile: boolean,
  isDesktop: boolean
) => ({
  name: 'BlockSlider',
  props: {
    heading: 'Выращиваем 9670 видов растений',
    leading: 'на территории питомника в 700 гектар',
    images
  },
  isMobile,
  isDesktop
})

const getSliderImages = (
  path: string,
  quantity: number,
  width: number,
  height: number
) => {
  const items = []
  for (let i = 1; i <= quantity; i++) {
    const image = `${path}${i.toString().padStart(2, '0')}.jpg`
    items.push({
      small: {
        src: image,
        width,
        height
      },
      large: image,
      title: ''
    })
  }
  return items
}

export function getGallery(type: string) {
  switch (type) {
    case 'bush':
      return [
        getDefaultSlider(
          getSliderImages(
            'https://ecoplant-pitomnik.ru/image/catalog/blocks/photo_sliders_with_logo/new/bush-desktop/bush-desktop-',
            9,
            800,
            600
          ),
          false,
          true
        ),
        getDefaultSlider(
          getSliderImages(
            'https://ecoplant-pitomnik.ru/image/catalog/blocks/photo_sliders_with_logo/new/bush-mobile/bush-mobile-',
            16,
            600,
            800
          ),
          true,
          false
        )
      ]
    case 'thuja':
      return [
        getDefaultSlider(
          getSliderImages(
            'https://ecoplant-pitomnik.ru/image/catalog/blocks/photo_sliders_with_logo/new/thuja-desktop/thuja-desktop-',
            12,
            800,
            600
          ),
          false,
          true
        ),
        getDefaultSlider(
          getSliderImages(
            'https://ecoplant-pitomnik.ru/image/catalog/blocks/photo_sliders_with_logo/new/thuja-mobile/thuja-mobile-',
            25,
            600,
            800
          ),
          true,
          false
        )
      ]
    case 'leaf':
      return [
        getDefaultSlider(
          getSliderImages(
            'https://ecoplant-pitomnik.ru/image/catalog/blocks/photo_sliders_with_logo/new/leaf-desktop-1/',
            8,
            800,
            600
          ),
          false,
          true
        ),
        getDefaultSlider(
          getSliderImages(
            'https://ecoplant-pitomnik.ru/image/catalog/blocks/photo_sliders_with_logo/new/leaf-mobile/leaf-mobile-',
            18,
            600,
            800
          ),
          true,
          false
        )
      ]
    case 'large':
      return [
        getDefaultSlider(
          getSliderImages(
            'https://ecoplant-pitomnik.ru/image/catalog/blocks/photo_sliders_with_logo/new/large-desktop/large-desktop-',
            14,
            800,
            600
          ),
          false,
          true
        ),
        getDefaultSlider(
          getSliderImages(
            'https://ecoplant-pitomnik.ru/image/catalog/blocks/photo_sliders_with_logo/new/large-mobile/large-mobile-',
            15,
            800,
            1200
          ),
          true,
          false
        )
      ]
    case 'eco':
    default:
      return [
        getDefaultSlider(
          [
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/1-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/1-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/2-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/2-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/3-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/3-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/4-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/4-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/5-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/5-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/6-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/6-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/7-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/7-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/8-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/8-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/9-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/9-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/10-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/10-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/11-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/11-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/12-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/12-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/13-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/13-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/14-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/14-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/15-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/15-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/16-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/16-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/17-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/17-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/18-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/18-0x0.jpg',
              title: ''
            },
            {
              small: {
                src: 'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/19-0x600h.jpg',
                width: 977,
                height: 600
              },
              large:
                'https://ecoplant-pitomnik.ru/image/cache/catalog/blocks/photo_sliders/our_plants/19-0x0.jpg',
              title: ''
            }
          ],
          false,
          true
        ),
        getDefaultSlider(
          getSliderImages(
            'https://ecoplant-pitomnik.ru/image/catalog/blocks/photo_sliders_with_logo/new/eco-mobile/main-mobile-',
            15,
            600,
            800
          ),
          true,
          false
        )
      ]
  }
}
