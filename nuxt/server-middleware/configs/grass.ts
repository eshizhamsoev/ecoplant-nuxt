import { LandingGroup } from '~/common-types/common/LandingGroup'

export const getGrassPrices = (group: LandingGroup) => {
  return group === 'spb'
    ? [
        {
          type: 'economical',
          price: {
            old: 180,
            current: 160
          }
        },
        {
          type: 'universal',
          price: {
            old: 200,
            current: 180
          }
        },
        {
          type: 'elite',
          price: {
            old: 240,
            current: 220
          }
        }
      ]
    : [
        {
          type: 'universal',
          price: {
            old: 120,
            current: 105
          }
        },
        {
          type: 'shadow',

          price: {
            old: 135,
            current: 120
          }
        },
        {
          type: 'sport',
          price: {
            old: 150,
            current: 120
          }
        },
        {
          type: 'elite',
          price: {
            old: 160,
            current: 120
          }
        }
      ]
}

export const getGrassServices = (group: LandingGroup) => [
  {
    title: 'Эконом',
    price: group === 'spb' ? 120 : 90,
    info: 'Без подготовки поверхности',
    items: [
      'анализ почвы',
      'укладка рулонного газона',
      'прикатывание газона',
      'вырезание приствольных кругов',
      'рекомендации по уходу и эксплуатации рулонного газона'
    ]
  },
  {
    title: 'Стандарт+',
    price: 250,
    info: 'Подготовка поверхности\nс планированием грунта',
    items: [
      'анализ почвы',
      ...(group === 'spb' ? [] : ['грунтовые работы']),
      'удаление сорняков',
      'планировка грунта на участке',
      'выведение уклонов',
      'подготовка поверхности под укладку',
      'чистовая планировка',
      'прикатывание грунта',
      'внесение удобрений',
      'укладка рулонного газона',
      'прикатывание газона',
      'вырезание приствольных кругов',
      'первый полив',
      'уборка территории'
    ]
  },
  {
    title: 'Элит',
    price: 550,
    info: 'С монтажом автополива',
    items: [
      'монтаж системы автоматического полива',
      'анализ почвы',
      'снятие и вывоз существующего грунта',
      'удаление сорняков',
      'планировка грунта',
      'выведение уклонов',
      'подготовка поверхности под укладку',
      'чистовая планировка',
      'внесение удобрений',
      'укладка рулонного газона',
      'прикатывание газона',
      'вырезание приствольных кругов',
      'первый полив',
      'уборка территории',
      'рекомендации по уходу и эксплуатации рулонного газона'
    ]
  }
]
