import Vue from 'vue'
import { accessorType } from '~/store'
import { ServerLandingConfig } from '~/common-types/server/landing-config'
import { Device } from '~/plugins/device'

declare module 'vue/types/vue' {
  interface Vue {
    $modal: any
    $accessor: typeof accessorType
    $gtm: {
      push(props: any): void
    }
    $device: Device
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $accessor: typeof accessorType
    $gtm: {
      push(props: any): void
    }
    $device: Device
  }
  interface Context {
    $accessor: typeof accessorType
    $gtm: {
      push(props: any): void
    }
    $device: Device
    $modal: any
  }
}

declare module 'connect' {
  interface IncomingMessage {
    data: ServerLandingConfig
  }
}

declare module '@nuxtjs/composition-api' {
  export type ComponentInstance = Vue
}
