import { GraphQLFieldResolver } from 'graphql'
import {
  InMemoryCache,
  IntrospectionFragmentMatcher
} from 'apollo-cache-inmemory'
import { Context } from '@nuxt/types'
import schema from '../graphql/local/schema.graphql'
import { CartProducts } from '../graphql/queries/CartProducts.graphql'
import {
  CartPrice,
  CartProduct,
  CartProductsQuery,
  CartQuantityMutationVariables,
  ClearCartMutationVariables,
  Price
} from '~/_generated/types'
// @ts-ignore
import schemaIntrospection from '~/graphql.schema.json'

const LOCAL_STORAGE_GQL_CART = 'gql-cart-products'

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: schemaIntrospection
})

function writeCart(data: any): void {
  if (!process.client || !window || !window.localStorage) {
    return
  }
  window.localStorage.setItem(LOCAL_STORAGE_GQL_CART, JSON.stringify(data))
}

function getCart(): any {
  if (!process.client || !window || !window.localStorage) {
    return
  }
  const data = window.localStorage.getItem(LOCAL_STORAGE_GQL_CART)
  if (!data) {
    return
  }
  return JSON.parse(data)
}

const inCartResolver: GraphQLFieldResolver<Price, any, any> = (
  price,
  _args,
  { cache }
) => {
  const queryResult = cache.readQuery({
    query: CartProducts
  })
  const { cartProducts } = queryResult
  for (const cartProduct of cartProducts) {
    for (const cartPrice of cartProduct.prices) {
      if (cartPrice.priceId === price.id) {
        return cartPrice.quantity
      }
    }
  }
  return 0
}

function validate(data: any) {
  if (!Array.isArray(data)) {
    return false
  }
  return data.every((item: any) => {
    if (!item || typeof item !== 'object') {
      return false
    }
    if (
      item.__typename !== 'CartProduct' ||
      !item.productId ||
      !Array.isArray(item.prices)
    ) {
      return false
    }
    return item.prices.every((item: any) => {
      if (!item || typeof item !== 'object') {
        return false
      }
      return (
        item.__typename === 'CartPrice' &&
        item.priceId &&
        Number.isInteger(item.quantity)
      )
    })
  })
}

const cartQuantityMutationResolver: GraphQLFieldResolver<
  any,
  { cache: InMemoryCache },
  CartQuantityMutationVariables
> = (root, { productId, priceId, quantity }, { cache }) => {
  const queryResult = cache.readQuery<CartProductsQuery>({
    query: CartProducts
  })
  // price.
  if (queryResult) {
    const { cartProducts } = queryResult
    const cartProduct = cartProducts.find(
      (cartProduct) => cartProduct.productId === productId
    )
    if (cartProduct) {
      const cartPrice = cartProduct.prices.find(
        (cartPrice) => cartPrice.priceId === priceId
      )
      if (cartPrice) {
        cartPrice.quantity = quantity
      } else {
        const cartPrice: CartPrice = {
          __typename: 'CartPrice',
          priceId,
          quantity
        }
        cartProduct.prices.push(cartPrice)
      }
    } else {
      const cartPrice: CartPrice = {
        __typename: 'CartPrice',
        priceId,
        quantity
      }
      const cartProduct: CartProduct = {
        __typename: 'CartProduct',
        productId,
        prices: [cartPrice]
      }
      cartProducts.push(cartProduct)
    }

    const data = {
      cartProducts: [...cartProducts]
    }
    // console.log(data)
    cache.writeQuery({ query: CartProducts, data })
    writeCart(cartProducts)
    return data.cartProducts
  }
  return []
}

const clearCartMutationResolver: GraphQLFieldResolver<
  any,
  { cache: InMemoryCache },
  ClearCartMutationVariables
> = (root, args, { cache }) => {
  const data = {
    cartProducts: []
  }
  cache.writeQuery({ query: CartProducts, data })
  writeCart([])
  return true
}

export default function (ctx: Context) {
  return {
    cache: new InMemoryCache({
      fragmentMatcher
    }),
    httpLinkOptions: {
      headers: {
        host: ctx.req?.headers.host
      }
    },
    httpEndpoint: process.server ? 'http://localhost:4000/' : '/gql',
    typeDefs: schema,
    resolvers: {
      Price: {
        inCart: inCartResolver
      },
      Mutation: {
        cartQuantity: cartQuantityMutationResolver,
        clearCart: clearCartMutationResolver
      }
    },
    onCacheInit: (cache: InMemoryCache) => {
      const cartData = getCart()
      const cartProducts = validate(cartData) ? cartData : []
      cache.writeQuery({ query: CartProducts, data: { cartProducts } })
    }
  }
}
