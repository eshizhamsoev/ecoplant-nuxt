import Vue from 'vue'
import VModal from 'vue-js-modal/dist/ssr.nocss'
import 'vue-js-modal/dist/styles.css'
import VueYoutube from 'vue-youtube'

Vue.use(VModal, {
  dynamic: true,
  dynamicDefaults: {
    adaptive: true,
    scrollable: true,
    height: 'auto'
  }
})

Vue.use(VueYoutube)

export default function (_, inject) {
  inject('modal', Vue.prototype.$modal)
}
