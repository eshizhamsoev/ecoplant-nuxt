import Parser from 'ua-parser-js'
import { Context, Plugin } from '@nuxt/types'
import { ssrRef } from '@nuxtjs/composition-api'

export type Device = {
  isMobile: boolean
  isDesktop: boolean
  isMobileOrTablet: boolean
}

function getDeviceType(ctx: Context) {
  if (ctx.req && ctx.req.headers['x-device-type']) {
    return ctx.req.headers['x-device-type']
  }
  return null
}

enum NGINX_TYPES {
  MOBILE = 'mobile',
  DESKTOP = 'desktop',
  TABLET = 'tablet'
}

const plugin: Plugin = function (ctx: Context, inject) {
  const data = ssrRef<Device | null>(null)
  if (data.value) {
    inject('device', data.value)
    return
  }
  const deviceType = getDeviceType(ctx)
  let mobile = false
  let desktop = true
  if (deviceType) {
    mobile = deviceType === NGINX_TYPES.MOBILE
    desktop = deviceType === NGINX_TYPES.DESKTOP
  } else if (ctx.req.headers && ctx.req.headers['user-agent']) {
    const ua = new Parser(ctx.req.headers['user-agent'])
    const type = ua.getDevice().type
    mobile = type === 'mobile'
    desktop = !type || !['mobile', 'tablet', 'wearable'].includes(type)
  }
  data.value = {
    isMobile: mobile,
    isMobileOrTablet: !desktop,
    isDesktop: desktop
  }
  inject('device', data.value)
}
export default plugin
