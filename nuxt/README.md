# front-nuxt

```bash
npx shipit (ab|ab2|production) deploy # for deploy 
```

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

Shim для работы ts
```ts
export declare type ComponentRenderProxy<P = {}, S = {}, PublicProps = P> = {
    $data: S;
    $props: PublicProps | {vShow?: boolean, style?: any, class?: ElementClass|ElementClass[], id?:string, key?: string|number, props?: PublicProps, onClose?: Function, onClick?: Function, onChange?: Function, onInput?: Function, scopedSlots?: any};
    $attrs: Data;
    $refs: Data;
    $slots: Data;
    $root: ComponentInstance | null;
    $parent: ComponentInstance | null;
    $emit: (event: string, ...args: unknown[]) => void;
} & P & S;
```
