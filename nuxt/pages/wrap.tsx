import { h } from '@vue/composition-api'
import { defineComponent } from '@nuxtjs/composition-api'
import { DesktopBlockShipping } from '~/components/blocks/block-shipping/desktop'

export default defineComponent({
  name: 'ProductPage',
  layout: () => {
    return 'product'
  },
  setup: () => {
    return () => h(DesktopBlockShipping)
  }
})
