import { Context } from '@nuxt/types'
import { h } from '@vue/composition-api'
import { computed, defineComponent } from '@nuxtjs/composition-api'
import { renderBlockComponents } from '~/components/blocks/block-components'
import { BlockCarantinVideo } from '~/components/blocks/block-carantin-video'

export default defineComponent({
  layout: (ctx: Context) => {
    return ctx.$device.isMobileOrTablet ? 'mobile/index' : 'desktop/index'
  },
  setup(props, { root }) {
    const homeComponents = computed(
      () => root.$accessor.components.homeComponents
    )
    const componentsFactory = renderBlockComponents(
      root.$device.isMobileOrTablet
    )

    return () => (
      <div>
        <BlockCarantinVideo />
        {h(componentsFactory, {
          props: {
            prefix: 'home-',
            components: homeComponents.value
          }
        })}
      </div>
    )
  },
  head() {
    return {
      title: this.$accessor.contacts.siteName
    }
  }
})
