import { h } from '@vue/composition-api'
import { computed, defineComponent } from '@nuxtjs/composition-api'
import { Context } from '@nuxt/types'
import { renderBlockComponents } from '~/components/blocks/block-components'

export default defineComponent({
  layout(ctx: Context) {
    return ctx.$device.isMobileOrTablet ? 'grass/mobile' : 'only-content'
  },
  setup(props, { root }) {
    const grassComponents = computed(() =>
      root.$accessor.components.grassComponents.filter(
        (item) =>
          (item.isMobile && root.$device.isMobileOrTablet) ||
          (item.isDesktop && !root.$device.isMobileOrTablet)
      )
    )
    const componentsFactory = renderBlockComponents(
      root.$device.isMobileOrTablet
    )

    return () => (
      <div>
        {h(componentsFactory, {
          props: {
            prefix: 'grass-',
            components: grassComponents.value
          }
        })}
      </div>
    )
  },
  head() {
    return {
      title: 'Газоны'
    }
  }
})
