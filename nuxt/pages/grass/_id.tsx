import { defineComponent } from '@nuxtjs/composition-api'
import { GrassItemPage } from '~/components/pages/grass-item-page/grass-item-page'
import { GrassProduct } from '~/store/grass'

export default defineComponent({
  layout: 'only-content',
  validate(ctx: any) {
    if (!/^\d+$/.test(ctx.params.id)) {
      return false
    }
    const index = +ctx.params.id as number
    return ctx.store.state.grass.products.some(
      (item: GrassProduct) => item.id === index
    )
  },
  setup(props, { root }) {
    const index = +root.$route.params.id
    const product = root.$accessor.grass.products.find(
      (item: GrassProduct) => item.id === index
    ) as GrassProduct

    return () => <GrassItemPage product={product} />
  },
  head() {
    // @ts-ignore
    const index = +this.$route.params.id as number
    const product = this.$store.state.grass.products.find(
      (item: GrassProduct) => item.id === index
    )
    return {
      title: product.heading
    }
  }
})
