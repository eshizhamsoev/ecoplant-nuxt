import { h } from '@vue/composition-api'
import { defineComponent, useContext, useMeta } from '@nuxtjs/composition-api'
import { Context } from '@nuxt/types'
import { useRouteParam } from '~/support/hooks/useRouteParam'
import { getServiceById } from '~/queries/graphql/get-service-by-id'

const MobileBlockService = () =>
  import('~/components/blocks/block-service/mobile').then(
    (v) => v.MobileBlockService
  )
const DesktopBlockService = () =>
  import('~/components/blocks/block-service/desktop').then(
    (v) => v.DesktopBlockService
  )

export default defineComponent({
  name: 'ServicePage',
  layout: (ctx: Context) => {
    return ctx.$device.isMobileOrTablet ? 'only-content' : 'desktop/index'
  },
  setup: (props, { root }) => {
    const serviceId = useRouteParam('id')
    const Component = root.$device.isMobileOrTablet
      ? MobileBlockService
      : DesktopBlockService

    const meta = useMeta({})

    const { service, onResult } = getServiceById(serviceId)
    const { redirect } = useContext()

    onResult(() => {
      if (service.value) {
        meta.title.value = service.value.name
      } else {
        redirect('/')
      }
    })

    return () =>
      service.value &&
      h(Component, {
        props: { service: service.value }
      })
  },
  head: {}
})
