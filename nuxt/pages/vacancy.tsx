import { h } from '@vue/composition-api'
import { defineComponent } from '@nuxtjs/composition-api'
import { VacancyContentFactory } from '~/components/pages/vacancy-content'

export default defineComponent({
  layout: 'only-content',
  setup(props, { root }) {
    const content = VacancyContentFactory(root)
    return () => h(content)
  },
  head() {
    return {
      title: 'Вакансии компании Экоплант'
    }
  }
})
