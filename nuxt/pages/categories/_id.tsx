import { h } from '@vue/composition-api'
import { defineComponent, useMeta, useContext } from '@nuxtjs/composition-api'
import { Context } from '@nuxt/types'
import { getComponent } from '~/components/pages/category'
import { getCategoryById } from '~/queries/graphql/get-category-by-id'
import { useRouteParam } from '~/support/hooks/useRouteParam'

export default defineComponent({
  name: 'CategoryPage',
  layout: (ctx: Context) => {
    return ctx.$device.isMobileOrTablet ? 'mobile/index' : 'desktop/index'
  },
  scrollToTop: false,
  setup: (_, { root }) => {
    const categoryId = useRouteParam('id')

    const { category, onResult } = getCategoryById(
      categoryId,
      root.$accessor.showExpandedInformation
    )
    const meta = useMeta({})
    const { error } = useContext()

    onResult(() => {
      if (category.value) {
        meta.title.value = category.value?.name
      } else {
        error({ statusCode: 404, message: 'Not Found' })
      }
    })

    const component = getComponent(root.$accessor.components.categoryComponent)

    return () => (
      <div id="category-page">
        {h(component, {
          props: {
            categoryId: categoryId.value
          }
        })}
      </div>
    )
  },
  head: {}
})
