import { defineComponent, useContext, useMeta } from '@nuxtjs/composition-api'
import { Context } from '@nuxt/types'
import { useRouteParam } from '~/support/hooks/useRouteParam'
import { getProductById } from '~/queries/graphql/get-product-by-id'
import { BlockProduct } from '~/components/blocks/block-product'

export default defineComponent({
  name: 'ProductPage',
  layout: (ctx: Context) => {
    return ctx.$device.isMobileOrTablet ? 'only-content' : 'desktop/index'
  },
  setup: (props, { root }) => {
    const { redirect } = useContext()

    const productId = useRouteParam('id')

    const meta = useMeta({})

    const { product, onResult } = getProductById(
      productId,
      root.$accessor.showExpandedInformation
    )

    onResult(() => {
      if (!product.value) {
        redirect('/')
        return
      }
      if (product.value.meta) {
        meta.title.value = product.value.meta.title
        if (product.value.meta.description) {
          meta.meta.value = [
            {
              hid: 'description',
              name: 'description',
              content: product.value.meta.description
            }
          ]
        }
      }
    })

    return () => product.value && <BlockProduct product={product.value} />
  },
  head: {}
})
