import {
  defineComponent,
  useMeta,
  useContext,
  computed
} from '@nuxtjs/composition-api'
import { Context } from '@nuxt/types'
import { useRouteParam } from '~/support/hooks/useRouteParam'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { careSeasonQuery } from '~/graphql/queries'
import BlockCareSeason from '~/components/blocks/block-care-season/block-care-season.vue'

export default defineComponent({
  name: 'CareSeason',
  layout: (ctx: Context) => {
    return ctx.$device.isMobileOrTablet ? 'mobile/index' : 'desktop/index'
  },
  scrollToTop: false,
  setup: () => {
    const id = useRouteParam('id')

    const params = computed(() => ({
      id: id.value
    }))
    const { result, onResult } = useGqlQuery(careSeasonQuery, params)
    const meta = useMeta({})
    const { error } = useContext()
    const season = computed(() => result.value?.careSeason || null)

    onResult(() => {
      if (season.value) {
        meta.title.value = season.value.title
      } else {
        error({ statusCode: 404, message: 'Not Found' })
      }
    })

    return () =>
      season.value && (
        <div id="season-page">
          <BlockCareSeason season={season.value} />
        </div>
      )
  },
  head: {}
})
