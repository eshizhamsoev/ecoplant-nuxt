import { h } from '@vue/composition-api'
import { defineComponent } from '@nuxtjs/composition-api'
const MobileBlock = () =>
  import('@/components/pages/checkout/mobile').then((v) => v.MobileCheckoutPage)
const DesktopBlock = () =>
  import('@/components/pages/checkout/desktop').then(
    (v) => v.DesktopCheckoutPage
  )

export default defineComponent({
  name: 'CheckoutPage',
  layout: () => {
    return 'only-content'
  },
  setup: (props, { root }) => {
    const Component = root.$device.isMobileOrTablet ? MobileBlock : DesktopBlock

    return () => h(Component)
  },
  head() {
    return {
      title: 'Оформление заказа'
    }
  }
})
