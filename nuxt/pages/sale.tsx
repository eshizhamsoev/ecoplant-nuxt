import { defineComponent } from '@nuxtjs/composition-api'
import { MobileSaleContent } from '~/components/pages/sale-content/mobile'

export default defineComponent({
  layout: 'only-content',
  setup() {
    return () => <MobileSaleContent />
  },
  head() {
    return {
      title: 'Акции компании Экоплант'
    }
  }
})
