import { h } from '@vue/composition-api'
import { defineComponent, useContext } from '@nuxtjs/composition-api'
import { Context } from '@nuxt/types'
import { useRouteParam } from '~/support/hooks/useRouteParam'
import { getProductById } from '~/queries/graphql/get-product-by-id'

const MobileBlock = () =>
  import('@/components/blocks/block-product/mobile').then(
    (v) => v.MobileBlockProduct
  )
const DesktopBlock = () =>
  import('~/components/blocks/block-product/desktop').then(
    (v) => v.DesktopBlockProduct
  )

export default defineComponent({
  name: 'LpProductPage',
  layout: (ctx: Context) => {
    return ctx.$device.isMobileOrTablet ? 'only-content' : 'desktop/index'
  },
  // validate(ctx: any) {
  //   if (!/^\d+$/.test(ctx.params.id)) {
  //     return false
  //   }
  //   const productId = +ctx.params.id as number
  //   const product = ctx.store.$accessor.products.products[productId]
  //   return !!product
  // },
  setup: (props, { root }) => {
    const productId = useRouteParam('id')
    const { product, onResult } = getProductById(productId)
    const { redirect } = useContext()

    onResult(() => {
      if (!product.value) {
        redirect('/')
      }
    })

    const Component = root.$device.isMobileOrTablet ? MobileBlock : DesktopBlock

    return () =>
      product.value &&
      h(Component, {
        props: { product: product.value }
      })
  }
  // head() {
  //   // @ts-ignore
  //   const productId = +this.$route.params.id as number
  //   // @ts-ignore
  //   const product = this.$accessor.products.products[productId]
  //   return {
  //     title: product.name
  //   }
  // }
})
