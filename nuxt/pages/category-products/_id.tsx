import {
  computed,
  ComputedRef,
  defineComponent,
  useContext,
  useMeta
} from '@nuxtjs/composition-api'
import { h } from '@vue/composition-api'
import { Context } from '@nuxt/types'
import chunk from 'lodash/chunk'
import { ProductsPanel } from '~/components/blocks/block-category/products-panel'
import { CategoryInfo } from '~/components/blocks/block-category/category-info'
import { getProductById } from '~/queries/graphql/get-product-by-id'
import { useRouteParam } from '~/support/hooks/useRouteParam'
import { Product } from '~/_generated/types'
import { getProductWithCategoriesById } from '~/queries/graphql/get-product-with-categories-by-id'

const MobileBlock = () =>
  import('@/components/blocks/block-product/mobile').then(
    (v) => v.MobileBlockProduct
  )

export default defineComponent({
  name: 'CategoryProductPage',
  layout: (ctx: Context) => {
    return ctx.$device.isMobileOrTablet ? 'only-content' : 'desktop/index'
  },
  setup: (props, { root }) => {
    const productId = useRouteParam('id')

    const meta = useMeta({})

    const { redirect } = useContext()

    if (root.$device.isMobileOrTablet) {
      const { product, onResult } = getProductById(productId)
      onResult(() => {
        if (product.value) {
          meta.title.value = product.value?.name
        } else {
          redirect('/')
        }
      })
      return () =>
        h(MobileBlock, {
          props: { product: product.value }
        })
    } else {
      const { product, onResult, loading } =
        getProductWithCategoriesById(productId)
      onResult(() => {
        if (product.value) {
          meta.title.value = product.value?.name
        } else {
          redirect('/')
        }
      })

      const products: ComputedRef<Pick<Product, 'id' | 'name'>[]> = computed(
        () => {
          if (!product.value) {
            return []
          }
          const id = product.value.id
          const products = product.value.category.products
          const index = products.findIndex((item) => item.id === id)
          if (index !== -1) {
            const firstProduct = products[index]
            products.splice(index, 1)
            products.unshift(firstProduct)
          }
          return products
        }
      )

      const productGroups = computed(() => chunk(products.value, 18))
      return () => (
        <div
          style={{ marginTop: '50px', opacity: loading.value ? 0.5 : 1 }}
          id="category-products"
        >
          {productGroups.value.map((products, index) => (
            <ProductsPanel products={products}>
              {index === 0 && product.value && (
                <CategoryInfo category={product.value.category} />
              )}
            </ProductsPanel>
          ))}
        </div>
      )
    }
  },
  head: {}
})
