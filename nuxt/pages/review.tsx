import { defineComponent } from '@nuxtjs/composition-api'
import { ReviewContent } from '~/components/content/review-content'

export default defineComponent({
  name: 'Review',
  layout: 'only-content',
  props: {},
  setup: (props, { root }) => {
    const close = () => root.$router.push('/')
    return () => <ReviewContent onClose={close} />
  }
})
