import { defineComponent } from '@nuxtjs/composition-api'
import { BlockHeadingOneLine } from '~/components/blocks/block-heading-one-line'
import { PrivacyPolicyContent } from '~/components/content/privacy-policy-content'
import { BlockFooterOneLine } from '~/components/blocks/block-footer-one-line'

export default defineComponent({
  name: 'PrivacyPolicy',
  layout: 'only-content',
  setup: () => {
    return () => (
      <div>
        <BlockHeadingOneLine />
        <PrivacyPolicyContent />
        <BlockFooterOneLine />
      </div>
    )
  }
})
