import { h } from '@vue/composition-api'
import { defineComponent, useContext, useMeta } from '@nuxtjs/composition-api'
import { Context } from '@nuxt/types'
import { useRouteParam } from '~/support/hooks/useRouteParam'
import { getExclusive } from '~/queries/graphql/get-exclusive-by-id'

const MobileBlockService = () =>
  import('~/components/blocks/block-service/mobile').then(
    (v) => v.MobileBlockService
  )
const DesktopBlockService = () =>
  import('~/components/blocks/block-service/desktop').then(
    (v) => v.DesktopBlockService
  )

export default defineComponent({
  name: 'ServicePage',
  layout: (ctx: Context) => {
    return ctx.$device.isMobileOrTablet ? 'only-content' : 'desktop/index'
  },
  setup: (props, { root }) => {
    const exclusiveId = useRouteParam('id')
    const Component = root.$device.isMobileOrTablet
      ? MobileBlockService
      : DesktopBlockService

    const meta = useMeta({})

    const { exclusive, onResult } = getExclusive(exclusiveId)
    const { redirect } = useContext()

    onResult(() => {
      if (exclusive.value) {
        meta.title.value = exclusive.value.name
      } else {
        redirect('/')
      }
    })

    return () =>
      exclusive.value &&
      h(Component, {
        props: { service: exclusive.value }
      })
  },
  head: {}
})
