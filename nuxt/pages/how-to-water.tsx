import { defineComponent } from '@nuxtjs/composition-api'
import { HowToWaterContent } from '~/components/pages/how-to-water-content'

export default defineComponent({
  layout: 'only-content',
  setup() {
    return () => <HowToWaterContent />
  },
  head() {
    return {
      title: 'Как поливать растения'
    }
  }
})
