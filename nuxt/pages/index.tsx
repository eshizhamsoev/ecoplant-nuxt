import { Context } from '@nuxt/types'
import { h } from '@vue/composition-api'
import { computed, defineComponent, useMeta } from '@nuxtjs/composition-api'
import { renderBlockComponents } from '~/components/blocks/block-components'

export default defineComponent({
  layout: (ctx: Context) => {
    return ctx.$device.isMobileOrTablet ? 'mobile/index' : 'desktop/index'
  },
  setup(props, { root }) {
    const homeComponents = computed(() =>
      root.$accessor.components.homeComponents.filter(
        ({ isMobile, isDesktop }) =>
          root.$device.isMobileOrTablet ? isMobile : isDesktop
      )
    )
    const componentsFactory = renderBlockComponents(
      root.$device.isMobileOrTablet
    )
    useMeta({
      title: root.$accessor.meta.homePage.title,
      meta: [
        {
          hid: 'description',
          name: 'description',
          content: root.$accessor.meta.homePage.description
        }
      ]
    })

    return () => (
      <div>
        {h(componentsFactory, {
          props: {
            prefix: 'home-',
            components: homeComponents.value
          }
        })}
      </div>
    )
  },
  head: {}
})
