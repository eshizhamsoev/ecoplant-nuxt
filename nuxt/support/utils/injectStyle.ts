import Vue from 'vue/types'

export type Style = {
  __inject__(context?: Vue): void
}

export type StyleModule = Record<string, string> & Style

type BeforedefineComponent = {
  beforeCreate?(this: Vue): void
}
export function injectStyles<T>(
  style: StyleModule,
  object: T & BeforedefineComponent
): T & { class: string } {
  const beforeCreate = object.beforeCreate
  object.beforeCreate = function () {
    style.__inject__ && style.__inject__(this.$ssrContext)
    if (typeof beforeCreate === 'function') {
      return beforeCreate.call(this)
    }
  }
  return object as T & { class: string }
}
