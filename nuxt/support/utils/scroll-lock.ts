import {
  disableBodyScroll,
  clearAllBodyScrollLocks,
  enableBodyScroll
} from 'body-scroll-lock'

const bodyScrollOptions = {
  reserveScrollBarGap: true
}

export const lockScroll = (el: Element) => {
  disableBodyScroll(el, bodyScrollOptions)
}

export const unlockScroll = (el: Element) => {
  enableBodyScroll(el)
}

export const clearAllScrollLocks = () => {
  clearAllBodyScrollLocks()
}
