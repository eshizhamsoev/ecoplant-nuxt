export function preloadImage(src: string) {
  const image = new Image()
  image.src = src
}
