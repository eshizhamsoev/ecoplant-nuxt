import { ClassNameFormatter, withNaming } from '@bem-react/classname'

type StyleType = { [key: string]: string }

const cn = withNaming({ e: '__', m: '_', v: '_' })

const useClassNames = (blockName: string, style: StyleType) => {
  const block = cn(blockName)
  return function () {
    const data = block(...arguments)
    return data
      .split(' ')
      .map((className) => style[className])
      .join(' ')
  } as ClassNameFormatter
}

export { cn, useClassNames }
