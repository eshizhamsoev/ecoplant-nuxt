export function parseQueryParams(queryString: string) {
  // This function is anonymous, is executed immediately and
  // the return value is assigned to QueryString!
  const queryParams: { [key: string]: string | string[] } = {}
  if (queryString) {
    const query = queryString.substring(1)
    const vars = query.split('&')
    for (let i = 0; i < vars.length; i++) {
      const [key, value] = vars[i].split('=')
      // If first entry with this name
      const previous = queryParams[key]
      if (typeof previous === 'undefined') {
        queryParams[key] = decodeURIComponent(value)
        // If second entry with this name
      } else if (typeof previous === 'string') {
        queryParams[key] = [previous, decodeURIComponent(value)]
        // If third or later entry with this name
      } else if (Array.isArray(previous)) {
        previous.push(decodeURIComponent(value))
      }
    }
  }
  return queryParams
}
