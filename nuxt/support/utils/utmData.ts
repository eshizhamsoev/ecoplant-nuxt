// @ts-ignore
import { get, set } from 'js-cookie'
import { parseQueryParams } from './parseQueryParams'

const UTM_FIELDS = [
  'utm_campaign',
  'utm_source',
  'utm_medium',
  'utm_term',
  'utm_content'
]

const PREFIX = 'uls_'
const cookieName = (name: string) => `${PREFIX}${name}`

export type UTMData = Record<string, string | null>

function extractUtm(url: string): UTMData {
  const params = parseQueryParams(url)
  const keys = [...Object.values(UTM_FIELDS)]
  const entries = keys.map((key) => {
    const value = params[key]
    if (value === null) {
      return [key, null]
    }
    return [key, Array.isArray(value) ? value.join(',') : value]
  })
  return Object.fromEntries(entries)
}

export function saveUtm(location: Location) {
  const utm = extractUtm(location.search)
  Object.entries(utm).forEach(([key, value]) => {
    if (value) {
      set(cookieName(key), value, { expires: 60 })
    }
  })
}

export function getUTM(): UTMData {
  return Object.fromEntries(
    UTM_FIELDS.map((key) => {
      return [key, get(cookieName(key)) || null]
    })
  )
}
