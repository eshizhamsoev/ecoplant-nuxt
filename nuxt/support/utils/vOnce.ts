import type { VNode } from 'vue'

type NodeType = VNode | null

export const vOnceFabric = (renderFunction: () => NodeType) => {
  let html: NodeType = null
  return () => {
    if (html) {
      return html
    } else {
      html = renderFunction()
      return html
    }
  }
}
