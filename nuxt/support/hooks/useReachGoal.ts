import { useContext } from '@nuxtjs/composition-api'

export enum GTM_EVENTS {
  whatsApp = 'whats-app',
  telegram = 'telegram',
  phoneClick = 'phone-click',
  emailClick = 'email-click',
  openChat = 'open-chat',
  callBack = 'call-back',
  mailBack = 'mail-back',
  writeToDirector = 'write-to-director',
  videoCall = 'video-call',
  crmNotAvailable = 'crm-not-available',
  vacancySend = 'vacancy-send',
  vacancyCallBack = 'vacancy-callback',
  order = 'order',
  review = 'review',
  addToCart = 'addToCart',
  removeFromCart = 'removeFromCart',
  unsubscribe = 'unsubscribe',
  bookForm = 'book-form',
  calculateServicePrice = 'calculate-service-price'
}

export const useReachGoal = () => {
  const { $gtm } = useContext()
  return (goal: GTM_EVENTS, props?: any) => {
    $gtm.push({ event: goal, ...props })
  }
}
