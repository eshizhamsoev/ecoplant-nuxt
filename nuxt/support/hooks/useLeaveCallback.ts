export const useLeaveCallback = (
  callback: Function,
  startMilliseconds: number,
  endMilliseconds = 0
) => {
  let start = false
  let end = false
  let shown = false

  setTimeout(function () {
    start = true
  }, startMilliseconds)

  if (endMilliseconds) {
    setTimeout(function () {
      end = true
    }, endMilliseconds)
  }

  document.addEventListener('mouseleave', function () {
    if (start && !end && !shown) {
      callback()
      shown = true
    }
  })
}
