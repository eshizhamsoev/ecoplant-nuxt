import { onMounted, onUnmounted, ref } from '@nuxtjs/composition-api'

export const useIntersectedOnce = (getRef: () => Element) => {
  const intersected = ref(false)
  let observer: IntersectionObserver
  onMounted(() => {
    observer = new IntersectionObserver(
      (entries) => {
        if (entries[0].isIntersecting) {
          intersected.value = true
          observer.disconnect()
        }
      },
      {
        threshold: [0, 0.2],
        root: null,
        rootMargin: '0px 0px 0px 0px'
      }
    )
    const el = getRef()
    if (el) {
      observer.observe(el)
    }
  })
  onUnmounted(() => {
    if (observer && observer.disconnect) {
      observer.disconnect()
    }
  })
  return intersected
}
