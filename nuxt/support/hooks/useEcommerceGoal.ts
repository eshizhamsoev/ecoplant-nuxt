import { GTM_EVENTS, useReachGoal } from '~/support/hooks/useReachGoal'
import { Price, Product } from '~/_generated/types'

type EcommerceProduct = Pick<Product, 'id' | 'name'>
type EcommercePrice = Pick<Price, 'id' | 'size' | 'price'>

export const useEcommerceGoal = () => {
  const reachGoal = useReachGoal()

  function getProductInfo(product: EcommerceProduct, price: EcommercePrice) {
    return {
      id: product.id + '-' + price.id,
      name: product.name,
      variant: price.size,
      price: price.price
    }
  }

  return {
    add: (product: EcommerceProduct, price: EcommercePrice, quantity = 1) => {
      reachGoal(GTM_EVENTS.addToCart, {
        ecommerce: {
          add: {
            products: [
              {
                ...getProductInfo(product, price),
                quantity
              }
            ]
          }
        }
      })
    },
    remove: (
      product: EcommerceProduct,
      price: EcommercePrice,
      quantity = 1
    ) => {
      reachGoal(GTM_EVENTS.removeFromCart, {
        ecommerce: {
          remove: {
            products: [
              {
                ...getProductInfo(product, price),
                quantity
              }
            ]
          }
        }
      })
    }
  }
}
