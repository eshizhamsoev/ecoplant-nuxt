import { useContext, computed } from '@nuxtjs/composition-api'

export function useRouteParam(paramName: string) {
  const ctx = useContext()
  return computed(() => ctx.params && ctx.params.value[paramName])
}
