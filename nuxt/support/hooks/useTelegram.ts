import { useContext } from '@nuxtjs/composition-api'
import { GTM_EVENTS, useReachGoal } from '~/support/hooks/useReachGoal'

const getTelegramLink = (account: string) => {
  return `tg://resolve?domain=${account}`
}

export const useOnTelegramClick = () => {
  const reachGoal = useReachGoal()
  const { store, $device } = useContext()

  return () => {
    reachGoal(GTM_EVENTS.telegram)
    const link = getTelegramLink(store.state.contacts.telegram)
    if ($device.isMobileOrTablet) {
      window.location.assign(link)
    } else {
      window.open(link)
    }
  }
}
