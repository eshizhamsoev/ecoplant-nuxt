import { useContext } from '@nuxtjs/composition-api'
import { GTM_EVENTS, useReachGoal } from '~/support/hooks/useReachGoal'

const getWhatsAppLink = (number: string) => {
  return `https://wa.me/${number}`
}

export const useOnWhatsAppClick = () => {
  const { store, $device } = useContext()
  const reachGoal = useReachGoal()
  return () => {
    reachGoal(GTM_EVENTS.whatsApp)
    const link = getWhatsAppLink(store.state.contacts.whatsApp)
    if ($device.isMobileOrTablet) {
      window.location.assign(link)
    } else {
      window.open(link)
    }
  }
}
