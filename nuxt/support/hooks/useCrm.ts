import axios from 'axios'
import type { ComponentInstance } from '@nuxtjs/composition-api'
import { GTM_EVENTS, useReachGoal } from '~/support/hooks/useReachGoal'
import { OldProductType } from '~/support/hooks/useForm'

type CrmData = {
  name?: string
  email?: string
  phone?: string
  source?: string
  address?: string
  products?: OldProductType[]
  total?: number
  planting?: boolean
  ground?: boolean
  fertilizer?: boolean
  shippingDate?: string
}

type PriceLabel = [number, string]

function getPriceLabels(group: string): PriceLabel[] {
  return group === 'spb'
    ? [
        [500000, 'очень важная'],
        [100000, 'высокобюджетная'],
        [30000, 'среднебюджетная'],
        [0, 'малобюджетная']
      ]
    : [
        [500000, 'очень важная'],
        [150000, 'высокобюджетная'],
        [20000, 'среднебюджетная'],
        [0, 'малобюджетная']
      ]
}

const getPriceLabel = (priceLabels: PriceLabel[], total: number) => {
  const item = priceLabels.find(([price]) => total >= price)

  return item ? item[1] : null
}

export const useCrm =
  (root: ComponentInstance) =>
  (
    {
      name,
      email,
      phone,
      source,
      address,
      products,
      total,
      planting,
      fertilizer,
      ground,
      shippingDate
    }: CrmData,
    serverData: any
  ) => {
    const group = root.$accessor.group
    if (root.$accessor.rocketCrmId) {
      const url = `https://b2b.rocketcrm.bz/api/channels/site/form?hash=${root.$accessor.rocketCrmId}`

      const cookies = {} as { [key: string]: string }
      const docCookies = document.cookie

      if (docCookies && docCookies !== '') {
        const cookiesSplit = docCookies.split(';')
        cookiesSplit.forEach((cookie) => {
          const cookieParts = cookie.split('=')
          const cookieName = decodeURIComponent(cookieParts[0]).replace(
            /\s/g,
            ''
          )
          cookies[cookieName] = decodeURIComponent(cookieParts[1])
        })
      }

      const data = {
        name,
        email,
        phone,
        source,
        cookies,
        address,
        shippingDate
      } as any
      if (serverData && serverData.requestId && products && total) {
        data.orderNumber = 'zak_' + serverData.requestId
        data.orderDate = serverData.date
        data.orderPrice = total
        data.planting = Number(planting)
        data.fertilizer = Number(fertilizer)
        data.ground = Number(ground)
        data.priceLevel = getPriceLabel(getPriceLabels(group), total)
        data.order = products
          .reduce((arr, cartProduct) => {
            arr.push(
              ...cartProduct.prices.map((cartPrice) => {
                return (
                  `${cartProduct.product.name}, ${cartPrice.priceData.size}, ` +
                  `кол.во: ${cartPrice.quantity}, цена. шт: ${cartPrice.priceData.price}, сумма ${cartPrice.total}`
                )
              })
            )
            return arr
          }, [] as string[])
          .join('<br>')
      }

      axios
        .post(url, data, {
          headers: {
            'Content-Type': 'application/json'
          }
        })
        .catch(() => {
          useReachGoal()(GTM_EVENTS.crmNotAvailable, {
            name,
            email,
            phone,
            source,
            cookies,
            url
          })
        })
    }
  }
