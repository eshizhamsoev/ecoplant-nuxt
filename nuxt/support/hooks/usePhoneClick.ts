import { useContext } from '@nuxtjs/composition-api'
import { GTM_EVENTS, useReachGoal } from '~/support/hooks/useReachGoal'

export const getPhoneLink = (number: string) => {
  return `tel:${number}`
}

export const useOnPhoneClick = () => {
  const { store } = useContext()
  const reachGoal = useReachGoal()
  return () => {
    reachGoal(GTM_EVENTS.phoneClick)
    window.location.replace(getPhoneLink(store.state.contacts.phone.number))
  }
}
