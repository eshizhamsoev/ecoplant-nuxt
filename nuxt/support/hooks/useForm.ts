import type { ComponentInstance } from '@nuxtjs/composition-api'
import { ref } from '@nuxtjs/composition-api'
import Axios from 'axios'

import { GTM_EVENTS, useReachGoal } from '~/support/hooks/useReachGoal'
import { useCrm } from '~/support/hooks/useCrm'

type FormOptions = {
  goal: GTM_EVENTS
}

export enum FORM_TYPES {
  PHONE = 1,
  EMAIL = 2,
  DIRECTOR = 4,
  VACANCY = 10,
  ORDER = 13,
  REVIEW = 14,
  PRICE_LIST = 15,
  UNSUBSCRIBE = 16
}

export const submitForm = (formId: FORM_TYPES, formData: Object) =>
  Axios.post('/api/form', {
    ...formData,
    form_id: formId,
    current_page: window.location.href
  }).then((response) => {
    const data = response.data
    if (data.success) {
      return data.success
    } else {
      throw new Error(data.error)
    }
  })

export type OldProductPrice = {
  priceData: {
    size: string
    price: number
  }
  quantity: number
  total: number
}

export type OldProductType = {
  product: {
    name: string
  }
  prices: OldProductPrice[]
}

type FormData = {
  name?: string
  email?: string
  phone?: string
  source?: string
  question?: string
  address?: string
  products?: OldProductType[]
  total?: number
  planting?: boolean
  ground?: boolean
  fertilizer?: boolean
  shippingDate?: string
}

export const useForm = (
  root: ComponentInstance,
  formId: FORM_TYPES,
  options?: FormOptions | null
) => {
  const processing = ref<boolean>(false)
  const successMessage = ref<string>('')
  const errorFields = ref<string[]>([])
  const fatalError = ref<boolean>(false)

  const reachGoal = useReachGoal()
  const sendToCrm = useCrm(root)

  const serverData = ref<any>(null)

  const submit = (formData: FormData, submitOptions?: FormOptions) => {
    if (processing.value) {
      return Promise.reject(new Error('Pending'))
    }
    processing.value = true
    errorFields.value = []
    successMessage.value = ''
    fatalError.value = false

    const goal = submitOptions?.goal || options?.goal
    return Axios.post('/api/form', {
      ...formData,
      form_id: formId,
      current_page: window.location.href
    })
      .then((response) => {
        processing.value = false
        serverData.value = response.data
        if (serverData.value.success) {
          successMessage.value = serverData.value.success
          if (goal) {
            reachGoal(goal)
          }
          sendToCrm(formData, serverData.value)
        } else if (
          serverData.value.error &&
          Array.isArray(serverData.value.error)
        ) {
          errorFields.value = serverData.value.error
        } else {
          fatalError.value = true
        }
      })
      .catch(() => {
        fatalError.value = true
      })
  }
  return {
    submit,
    processing,
    successMessage,
    errorFields,
    fatalError,
    serverData
  }
}
