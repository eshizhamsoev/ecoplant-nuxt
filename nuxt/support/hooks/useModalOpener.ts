import { AsyncComponent } from 'vue/types/options'
import { useContext } from '@nuxtjs/composition-api'
import { BlackFridayProduct } from '~/components/blocks/block-black-friday/types/product'
import { GrassProduct as TypeGrassProduct } from '~/store/grass'
import { ProductId, ServiceId } from '~/common-types/catalog'
import { GTM_EVENTS } from '~/support/hooks/useReachGoal'
import { FORM_TYPES } from '~/support/hooks/useForm'
import { Point } from '~/components/modals/content/modal-navigate/modal-navigate'
import { ClientPhoto } from '~/_generated/types'

type CallbackModal = {
  name: 'callback'
  source?: string
  goal?: GTM_EVENTS
  formType?: FORM_TYPES
}

type NavigateModal = {
  name: 'navigate'
  point: Point
}

type PartnershipModal = {
  name: 'partnership'
}

type HowToWater = {
  name: 'how-to-water'
}

type PriceListRequest = {
  name: 'price-list-request'
}

type Review = {
  name: 'review'
}

type VideoReviews = {
  name: 'video-reviews'
}

type Awards = {
  name: 'awards'
}

type BlackFridayProductModalProps = {
  name: 'black-friday-product'
  product: BlackFridayProduct
}

type PhoneUs = {
  name: 'phone-us'
}

type Sale = {
  name: 'sale'
}

type GrassProduct = {
  name: 'grass-product'
  product: TypeGrassProduct
}

type GrassPage = {
  name: 'grass-page'
}

type Unsubscribe = {
  name: 'unsubscribe'
}

type Service = {
  name: 'service'
  serviceId: ServiceId
}

type PlantingVideos = {
  name: 'planting-videos'
}

type VarietyInfo = {
  name: 'variety-info'
  varietyId: string
}

type Shipping = {
  name: 'shipping'
}

type Planting = {
  name: 'planting'
}

type BarkBeetlePrice = {
  name: 'bark-beetle-price'
}

type Address = {
  name: 'address'
  isAdditional: boolean
}

type AddressMobile = {
  name: 'address-mobile'
  isAdditional: boolean
}

type AddressSpb = {
  name: 'address=spb'
}

type GetSale = {
  name: 'get-sale'
}

type ProductDescription = {
  name: 'product-description'
  productId: ProductId
}

type YoutubeVideo = {
  name: 'youtube-video'
  code: string | null
}

type CommonVideo = {
  name: 'common-video'
  url: string | null
}

type PlantingStandard = {
  name: 'planting-standard'
}

type PlantingGuarantee = {
  name: 'planting-guarantee'
}

type VideoCall = {
  name: 'video-call'
  source: string
}

type Director = {
  name: 'director'
}

type SaleFir = {
  name: 'sale-fir'
}

type ImagesFeed = {
  name: 'images-feed'
  clients: ClientPhoto[]
  id: number
}

type MobileSippingAndPlanting = {
  name: 'mobile-shipping-and-planting'
}

type CoronaInfo = {
  name: 'corona-info'
  text?: string
}

type CoronaVideo = {
  name: 'corona-video'
  video: string
}

type Vacancy = {
  name: 'vacancy'
  source: string
}

type VacancyVideo = {
  name: 'vacancy-video'
  code: string
}

type ClientPhotos = {
  name: 'client-photos'
}

type LargeOrdersModal = {
  name: 'large-orders'
}

type Clients = {
  name: 'clients'
}

type Villages = {
  name: 'villages'
}

type Documents = {
  name: 'documents'
}

type ModalServiceDescription = {
  name: 'service-description'
  id: string
}

export type Modal =
  | CallbackModal
  | NavigateModal
  | PartnershipModal
  | HowToWater
  | BlackFridayProductModalProps
  | PriceListRequest
  | Review
  | PhoneUs
  | GrassPage
  | GrassProduct
  | Unsubscribe
  | Service
  | Sale
  | PlantingVideos
  | VarietyInfo
  | Shipping
  | Planting
  | Address
  | AddressSpb
  | BarkBeetlePrice
  | GetSale
  | ProductDescription
  | YoutubeVideo
  | CommonVideo
  | PlantingStandard
  | PlantingGuarantee
  | VideoCall
  | Director
  | SaleFir
  | ImagesFeed
  | CoronaInfo
  | CoronaVideo
  | Vacancy
  | VacancyVideo
  | MobileSippingAndPlanting
  | AddressMobile
  | VideoReviews
  | Awards
  | ClientPhotos
  | LargeOrdersModal
  | Clients
  | Villages
  | Documents
  | ModalServiceDescription

export const useModalOpener = () => {
  const { $modal, $device } = useContext()

  return (modalSettings: Modal) => {
    let component: AsyncComponent<any, any, any, any>
    const componentProps: any = {}
    const modalProps: any = {}
    const modalEventHandlers: any = {}
    switch (modalSettings.name) {
      case 'callback':
        component = () =>
          import('~/components/modals/content/modal-callback').then(
            (c) => c.ModalCallback
          )
        componentProps.source = modalSettings.source || 'Обратный звонок'
        if (modalSettings.goal) {
          componentProps.goal = modalSettings.goal
        }
        modalProps.width = '95%'
        modalProps.maxWidth = 533
        modalProps.styles = {
          background: 'none',
          boxShadow: 'none'
        }
        break
      case 'service-description':
        component = () =>
          import('~/components/modals/content/modal-service-description').then(
            (c) => c.ModalServiceDescription
          )
        modalProps.width = '95%'
        modalProps.maxWidth = 1140
        componentProps.id = modalSettings.id
        modalProps.styles = {
          background: '#fff',
          boxShadow: 'none'
        }
        break
      case 'navigate':
        component = () =>
          import('~/components/modals/content/modal-navigate').then(
            (c) => c.ModalNavigate
          )
        modalProps.width = 350
        componentProps.geoPoint = modalSettings.point
        break
      case 'partnership':
        component = () =>
          import('~/components/modals/content/modal-partnership').then(
            (c) => c.ModalPartnership
          )
        modalProps.width = 350
        break
      case 'how-to-water':
        component = () =>
          import('~/components/modals/content/modal-how-to-water').then(
            (c) => c.ModalHowToWater
          )
        modalProps.width = 800
        break
      case 'sale':
        component = () =>
          import(
            '~/components/modals/content/modal-sales/desktop/desktop-modal-sales'
          ).then((c) => c.DesktopModalSales)
        modalProps.width = '95%'
        modalProps.maxWidth = 1380
        break
      case 'black-friday-product':
        component = () =>
          import('~/components/modals/content/modal-black-friday-product').then(
            (c) => c.ModalBlackFridayProduct
          )
        modalProps.width = 900
        componentProps.product = modalSettings.product
        break
      case 'price-list-request':
        component = () =>
          import('~/components/modals/content/modal-price-list-request').then(
            (c) => c.ModalPriceListRequest
          )
        modalProps.width = '95%'
        modalProps.maxWidth = 533
        modalProps.styles = {
          background: 'none',
          boxShadow: 'none'
        }
        break
      case 'review':
        component = () =>
          import('~/components/modals/content/modal-review').then(
            (c) => c.ModalReview
          )
        modalProps.width = 600
        break
      case 'bark-beetle-price':
        component = () =>
          import('~/components/modals/content/modal-bark-beetle-price').then(
            (c) => c.ModalBarkBeetlePrice
          )
        modalProps.width = 500
        break
      case 'phone-us':
        component = () =>
          import('~/components/modals/content/modal-phone-us').then(
            (c) => c.ModalPhoneUs
          )
        modalProps.width = 500
        break
      case 'get-sale':
        component = () =>
          import('~/components/modals/content/modal-get-sale').then(
            (c) => c.ModalGetSale
          )
        modalProps.width = 500
        break
      case 'unsubscribe':
        component = () =>
          import('~/components/modals/content/modal-unsubscribe').then(
            (c) => c.ModalUnsubscribe
          )
        modalProps.width = 500
        break
      case 'grass-page':
        component = () =>
          import('~/components/modals/content/modal-grass-page').then(
            (c) => c.ModalGrassPage
          )
        modalProps.width = '95%'
        modalProps.maxWidth = 1400
        modalEventHandlers['before-open'] = () =>
          document.body.classList.add('grass-page-blocked-scroll')
        modalEventHandlers['before-close'] = () =>
          document.body.classList.remove('grass-page-blocked-scroll')
        break
      case 'planting-videos':
        component = () =>
          import('~/components/modals/content/modal-planting-videos').then(
            (c) => c.ModalPlantingVideos
          )
        modalProps.width = '95%'
        modalProps.maxWidth = 1400
        break
      case 'grass-product':
        component = () =>
          import('~/components/modals/content/modal-grass').then(
            (c) => c.ModalGrass
          )
        componentProps.product = modalSettings.product
        modalProps.width = '95%'
        modalProps.maxWidth = 1180
        break
      case 'service':
        component = () =>
          import('~/components/modals/content/modal-service').then(
            (c) => c.ModalService
          )
        modalProps.width = '95%'
        modalProps.maxWidth = 1000
        // modalEventHandlers['before-open'] = () =>
        //   document.body.classList.add('grass-page-blocked-scroll')
        // modalEventHandlers['before-close'] = () =>
        //   document.body.classList.remove('grass-page-blocked-scroll')
        componentProps.serviceId = modalSettings.serviceId
        break
      case 'variety-info':
        component = () =>
          import('~/components/modals/content/modal-variety-description').then(
            (c) => c.ModalVarietyDescription
          )
        modalProps.width = '95%'
        modalProps.maxWidth = 1000
        componentProps.varietyId = modalSettings.varietyId
        break
      case 'shipping':
        component = () =>
          import(
            '~/components/modals/content/modal-shipping/modal-shipping'
          ).then((c) => c.ModalShipping)
        modalProps.width = '95%'
        modalProps.maxWidth = 1000
        break
      case 'planting':
        component = () =>
          import(
            '~/components/modals/content/modal-planting/modal-planting'
          ).then((c) => c.ModalPlanting)
        modalProps.width = '95%'
        modalProps.maxWidth = 1000
        break
      case 'address':
        component = () =>
          import('~/components/modals/content/modal-address').then(
            (c) => c.ModalAddress
          )
        modalProps.width = '95%'
        modalProps.maxWidth = 756
        componentProps.isAdditional = modalSettings.isAdditional
        break
      case 'address-mobile':
        component = () =>
          import('~/components/modals/content/modal-address-mobile').then(
            (c) => c.ModalAddressMobile
          )
        modalProps.width = '95%'
        modalProps.maxWidth = 360
        componentProps.isAdditional = modalSettings.isAdditional
        break
      case 'product-description':
        component = () =>
          import('~/components/modals/content/modal-product-description').then(
            (v) => v.ModalProductDescription
          )
        modalProps.width = '100%'
        modalProps.maxWidth = 1000
        componentProps.productId = modalSettings.productId
        break
      case 'youtube-video':
        component = () =>
          import(
            '~/components/modals/content/modal-youtube/modal-youtube'
          ).then((v) => v.ModalYoutube)
        modalProps.width = '100%'
        modalProps.maxWidth = 1000
        componentProps.code = modalSettings.code
        break
      case 'common-video':
        component = () =>
          import('~/components/modals/content/modal-video/modal-video').then(
            (v) => v.ModalVideo
          )
        modalProps.width = '100%'
        modalProps.maxWidth = 1000
        componentProps.video = modalSettings.url
        break
      case 'planting-standard':
        component = () =>
          import(
            '~/components/modals/content/modal-planting/modal-planting-standard'
          ).then((v) => v.ModalPlantingStandard)
        break
      case 'planting-guarantee':
        component = () =>
          import(
            '~/components/modals/content/modal-planting/modal-planting-guarantee'
          ).then((v) => v.ModalPlantingGuarantee)
        break
      case 'video-call':
        component = () =>
          import('~/components/modals/content/modal-video-call').then(
            (v) => v.ModalVideoCall
          )
        modalProps.width = 1350
        componentProps.source = modalSettings.source
        break
      case 'director':
        component = () =>
          import('~/components/modals/content/modal-director').then(
            (v) => v.ModalDirector
          )
        break
      case 'sale-fir':
        component = () =>
          import('~/components/modals/content/modal-sale-fir').then(
            (v) => v.ModalSaleFir
          )
        modalProps.width = $device.isMobileOrTablet ? 569 : 800
        break
      case 'images-feed':
        component = () =>
          import('~/components/modals/content/modal-images-feed').then(
            (v) => v.ModalImagesFeed
          )
        modalProps.width = $device.isMobileOrTablet ? 569 : 800
        componentProps.clients = modalSettings.clients
        componentProps.id = modalSettings.id
        break
      case 'corona-info':
        component = () =>
          import('~/components/modals/content/modal-corona-info').then(
            (c) => c.ModalCoronaInfo
          )
        componentProps.text = modalSettings.text
        modalProps.width = 450
        break
      case 'corona-video':
        component = () =>
          import('~/components/modals/content/modal-video').then(
            (c) => c.ModalVideo
          )
        componentProps.video =
          'https://ecoplant-pitomnik.ru/files/videos/contactless-planting.mp4'
        modalProps.width = 1280
        break
      case 'vacancy':
        component = () =>
          import('~/components/modals/content/modal-vacancy/mobile').then(
            (c) => c.MobileModalVacancy
          )
        componentProps.source = modalSettings.source
        modalProps.width = 320
        break
      case 'vacancy-video':
        component = () =>
          import('~/components/modals/content/modal-youtube').then(
            (c) => c.ModalYoutube
          )
        componentProps.code = modalSettings.code
        modalProps.width = '100%'
        break
      case 'video-reviews':
        component = () =>
          import(
            '~/components/modals/content/modal-video-reviews/modal-video-reviews'
          ).then((v) => v.ModalVideoReviews)
        modalProps.width = '100%'
        modalProps.maxWidth = 1000
        break
      case 'awards':
        component = () =>
          import('~/components/modals/content/modal-awards/modal-awards').then(
            (v) => v.ModalAwards
          )
        modalProps.width = '100%'
        modalProps.maxWidth = 1000
        break
      case 'client-photos':
        component = () =>
          import(
            '~/components/modals/content/modal-client-photos/modal-client-photos'
          ).then((v) => v.ModalClientPhotos)
        modalProps.width = '100%'
        modalProps.maxWidth = 1400
        break
      case 'large-orders':
        component = () =>
          import('~/components/modals/content/modal-large-orders').then(
            (v) => v.ModalLargeOrders
          )
        modalProps.width = '95%'
        modalProps.maxWidth = 1280
        break
      case 'villages':
        component = () =>
          import(
            '~/components/modals/content/modal-villages/modal-villages'
          ).then((value) => value.ModalVillages)
        modalProps.width = '100%'
        modalProps.maxWidth = 1000
        break
      case 'clients':
        component = () =>
          import(
            '~/components/blocks/block-client-logos/desktop/desktop-block-client-logos'
          ).then((value) => value.DesktopBlockClientLogos)
        modalProps.width = '100%'
        modalProps.maxWidth = 1000
        break
      case 'documents':
        component = () =>
          import(
            '~/components/blocks/block-documents/desktop/desktop-block-documents'
          ).then((value) => value.DesktopBlockDocuments)
        modalProps.width = '100%'
        modalProps.maxWidth = 1000
        break
      default:
        return
    }
    $modal.show(component, componentProps, modalProps, modalEventHandlers)
  }
}
