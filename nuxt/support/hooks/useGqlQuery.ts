import { useQuery } from '@vue/apollo-composable'
import {
  Ref,
  isRef,
  computed,
  ref,
  ssrRef,
  watch
} from '@nuxtjs/composition-api'

import {
  OptionsParameter,
  UseQueryOptions
} from '@vue/apollo-composable/dist/useQuery'
import { Query } from '~/graphql/queries'

type OnResultCallback = () => void

type QueryResult<T> = {
  result: Ref<T | null>
  loading: Ref<boolean>
  error: Ref<any>
  onResult: (callback: OnResultCallback) => void
}

export function useGqlQuery<TResult, TVariables>(
  query: Query<TResult, TVariables>,
  props: TVariables | Ref<TVariables>,
  queryOptions?: OptionsParameter<TResult, TVariables>
): QueryResult<TResult> {
  const vars = isRef(props) ? props.value : props
  const result = ssrRef<TResult | null>(null, query.getQueryKey(vars))
  const loading = ref(false)
  const error = ref<any>(null)
  const variables = computed<TVariables>(() => ({
    ...(isRef(props) ? props.value : props)
  }))

  const resultCallbacks = [] as OnResultCallback[]

  const options =
    queryOptions ||
    ref({
      enabled: !result.value
    })

  const {
    result: queryResult,
    loading: queryLoading,
    error: queryError,
    onError,
    onResult
  } = useQuery<TResult, TVariables>(query.getQuery(), variables, options)

  loading.value = queryLoading.value

  onError(() => {
    error.value = queryError.value
  })

  onResult(() => {
    if (!queryLoading.value) {
      result.value = queryResult.value !== undefined ? queryResult.value : null
    }
    loading.value = queryLoading.value
    resultCallbacks.forEach((callback) => callback())
  })

  watch(
    () => variables.value,
    () => {
      if (isRef<UseQueryOptions<TResult, TVariables>>(options)) {
        if (!options.value.enabled && !queryOptions) {
          options.value.enabled = true
          loading.value = true
        }
      } else if ('enabled' in options && options.enabled === false) {
        options.enabled = true
        loading.value = true
      }
    }
  )

  const customOnResult = (callback: OnResultCallback) => {
    if (result.value) {
      callback()
    } else {
      resultCallbacks.push(callback)
    }
  }

  return { result, loading, error, onResult: customOnResult }
}
