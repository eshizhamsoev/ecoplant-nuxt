module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  extends: [
    'plugin:vue/recommended',
    '@nuxtjs/eslint-config-typescript',
    'prettier',
    'plugin:prettier/recommended',
    'plugin:nuxt/recommended'
  ],
  plugins: ['prettier'],
  rules: {
    '@typescript-eslint/no-unused-vars': [
      'error',
      { vars: 'all', args: 'after-used', ignoreRestSiblings: false }
    ],
    'import/order': [
      'error',
      {
        pathGroups: [
          {
            /* Import own styles in last order */
            pattern: './*.scss?module',
            group: 'object',
            position: 'after'
          }
        ]
      }
    ]
  },
  globals: {
    JSX: true
  }
}
