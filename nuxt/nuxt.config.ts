import { IncomingMessage } from 'connect'
import hostReplacer from './server-middleware/dev-host-replacer'

const serverMiddleware = []

if (process.env.CUSTOM_HOST) {
  serverMiddleware.push('~/server-middleware/dev-host-replacer.ts')
}
serverMiddleware.push(
  { path: '/api/client-logos', handler: '~/api/server/http/client-logos.ts' },
  '~/server-middleware/landing-config.ts',
  { path: '/api/specials', handler: '~/api/server/http/specials.ts' },
  { path: '/api/form', handler: '~/api/server/http/form.ts' },
  { path: '/api/attributes', handler: '~/api/server/http/attributes.ts' },
  { path: '/sitemap.xml', handler: '~/api/server/http/xml/sitemap.ts' },
  { path: '/robots.txt', handler: '~/api/server/http/xml/robots.ts' },
  {
    path: '/xml/smart-banners.xml',
    handler: '~/api/server/http/xml/smart-banners.ts'
  },
  {
    path: '/xml/main-products.xml',
    handler: '~/api/server/http/xml/main-products.ts'
  },
  {
    path: '/xml/yandex-o.xml',
    handler: '~/api/server/http/xml/yandex-o.ts'
  }
)

module.exports = {
  version: process.env.npm_package_version,
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Питомник растений',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, viewport-fit=cover'
      },
      { name: 'theme-color', content: '#434648' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    htmlAttrs: {
      lang: 'ru'
    }
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    // '@/plugins/composition-api',
    '@/plugins/modernizr',
    '@/plugins/vue-js-modal',
    '@/plugins/device'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxtjs/composition-api/module',
    '@nuxtjs/gtm',
    'nuxt-typed-vuex',
    '@nuxt/typescript-build',
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    '@aceforth/nuxt-optimized-images',
    '@nuxtjs/svg-sprite'
  ],
  router: {
    middleware: 'remove-double-slash'
  },
  serverMiddleware,
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/sentry',
    '@nuxtjs/proxy',
    // 'nuxt-ssr-cache',
    // '@nuxtjs/device',
    '@nuxtjs/apollo',
    [
      '@nuxtjs/redirect-module',
      {
        rules: [
          {
            from: '^/category/(.*)$',
            to: '/categories/$1'
          },
          {
            from: '^/products/810(.*)$',
            to: '/products/13221$1'
          },
          {
            from: '^/categories/$',
            to: '/'
          },
          {
            from: '^/products/$',
            to: '/'
          },
          {
            from: '^/services/$',
            to: '/categories/services/'
          }
        ],
        onDecode: (req: IncomingMessage) => {
          return req.url
        }
      }
    ]
  ],
  // cache: {
  //   key(route: string, context: Context) {
  //     let mobile
  //     const host = context.req.headers.host
  //     // eslint-disable-next-line no-console
  //     if (context.req.headers['cf-device-type'] === 'mobile') {
  //       mobile = true
  //     } else {
  //       const ua = parser(context.req.headers['user-agent'])
  //       mobile = ua.device.type === 'mobile'
  //     }
  //     if ((route && route !== '/') || process.env.NODE_ENV !== 'production') {
  //       return false
  //     }
  //     return [host, mobile].join('-')
  //   },
  //
  //   pages: [/^\/$/],
  //
  //   store: {
  //     type: 'memory',
  //
  //     // maximum number of pages to store in memory
  //     // if limit is reached, least recently used page
  //     // is removed.
  //     max: 100,
  //
  //     // number of seconds to store this page in cache
  //     ttl: 600
  //   }
  // },
  svgSprite: {
    input: '~/components/base/base-icon/resources/'
  },
  optimizedImages: {
    handleImages: ['jpeg', 'png', 'webp', 'gif', 'svg'],
    optimizeImages: true
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    // extractCSS: process.env.NODE_ENV === 'production',
    transpile: [/typed-vuex/],
    loaders: {
      vueStyle: {
        manualInject: true
      }
    },
    babel: {
      presets: ['vca-jsx', '@nuxt/babel-preset-app']
    }
  },
  typescript: {
    typeCheck:
      process.env.NODE_ENV === 'development'
        ? {
            eslint: {
              files: './**/*.{ts,tsx}'
            }
          }
        : false
  },
  gtm: {
    id: 'GTM-T9THG9K',
    pageTracking: true
  },
  sentry: {
    dsn: 'https://ec0fe0b2d1c24adead3045b0b6d6b72a@o437253.ingest.sentry.io/5404935', // Enter your project's DSN here
    disabled: process.env.NODE_ENV === 'development',
    config: {},
    disableClientSide: true
  },
  apollo: {
    clientConfigs: {
      default: '~/plugins/apollo-client'
    },
    defaultOptions: {
      // See 'apollo' definition
      // For example: default query options
      $query: {
        fetchPolicy: 'network-only'
      }
    }
  },
  proxy: {
    '/gql': {
      target: 'http://localhost:4000',
      onProxyReq(proxyReq: any, req: IncomingMessage, res: any) {
        if (process.env.CUSTOM_HOST) {
          hostReplacer(req, res, () =>
            proxyReq.setHeader('host', req.headers.host)
          )
        } else {
          proxyReq.setHeader('host', req.headers.host)
        }
      }
    }
  }
}
