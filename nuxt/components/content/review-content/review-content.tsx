import {
  computed,
  defineComponent,
  onMounted,
  ref
} from '@nuxtjs/composition-api'
import autosize from 'autosize'
import logo from './resources/logo.svg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { FORM_TYPES, useForm } from '~/support/hooks/useForm'
import { GTM_EVENTS } from '~/support/hooks/useReachGoal'
import { BaseFormResponse, BaseIcon, BaseLoading } from '~/components/base'
import { BasePhoneInput } from '~/components/base/base-phone-input'
import { BasePolicyLink } from '~/components/base/base-policy-link/base-policy-link'
import { ModalCloseButton } from '~/components/modals/layout/modal-close-button/modal-close-button'
import style from './review-content.scss?module'

const cn = useClassNames('review-content', style)

const classes = {
  main: cn(),
  headingLine: cn('heading-line'),
  headingWrapper: cn('heading-wrapper'),
  heading: cn('heading'),
  leading: cn('leading'),
  close: cn('close'),
  form: cn('form'),
  fieldWrapper: cn('field-wrapper'),
  label: cn('label'),
  field: cn('field'),
  policy: cn('policy'),
  buttonWrapper: cn('button-wrapper'),
  errorMessage: cn('error-message'),
  button: cn('button')
}

const defaults = {
  policyText: 'Согласен с политикой конфиденциальности'
}

export const ReviewContent = injectStyles(
  style,
  defineComponent({
    name: 'ReviewContent',
    setup: (props, { slots, emit, root }) => {
      const data = ref({
        name: '',
        phone: '',
        orderNumber: '',
        review: ''
      })

      const { processing, fatalError, submit, errorFields, serverData } =
        useForm(root, FORM_TYPES.REVIEW, { goal: GTM_EVENTS.review })

      const successMessage = computed(
        () =>
          serverData.value &&
          serverData.value.requestId &&
          `Благодарим за ваш отзыв. Его номер — ${serverData.value.requestId}. Он проходит модерацию, в течении суток он будет вывешен на всех площадках.`
      )

      const onSubmit = (e: Event) => {
        e.preventDefault()
        submit({ ...data.value, source: 'Форма отзыва' })
      }

      onMounted(() => {
        autosize(root.$refs.textarea as HTMLElement)
      })

      function createUpdateValueFunction(fieldName: keyof typeof data.value) {
        return (event: InputEvent & { target?: HTMLInputElement }) => {
          // @ts-ignore
          data.value[fieldName] = event.target?.value as string
        }
      }
      // const buttonDisabled = computed(() => data.value.review.length < 35)

      return () => (
        <div class={classes.main}>
          <div class={classes.headingLine}>
            <img src={logo} alt="" />
            <div class={classes.headingWrapper}>
              <div class={classes.heading}>Оставить отзыв</div>
              <div class={classes.leading}>на всех площадках</div>
            </div>
            <ModalCloseButton
              class={cn('close')}
              onClick={() => emit('close')}
            />
          </div>
          <div class={classes.form}>
            <BaseFormResponse
              mainClass={classes.form}
              fatalError={fatalError.value}
              successMessage={successMessage.value}
            >
              <form onSubmit={onSubmit}>
                <div class={classes.fieldWrapper}>
                  <label class={classes.label} for="review-name">
                    Введите ФИО (по желанию)
                  </label>
                  <input
                    id="review-name"
                    type="text"
                    class={classes.field}
                    placeholder="Ивано Иван Иванович"
                    value={data.value.name}
                    onInput={createUpdateValueFunction('name')}
                  />
                </div>
                <div class={classes.fieldWrapper}>
                  <label class={classes.label} for="review-phone">
                    Номер телефона (по желанию)
                  </label>
                  <BasePhoneInput
                    id="review-phone"
                    class={classes.field}
                    value={data.value.phone}
                    onInput={(e: string) => {
                      data.value.phone = e
                    }}
                    error={
                      errorFields.value && errorFields.value.includes('phone')
                    }
                  />
                </div>
                <div class={classes.fieldWrapper}>
                  <label class={classes.label} for="review-order-number">
                    Номер заказа (по желанию)
                  </label>
                  <input
                    id="review-order-number"
                    type="text"
                    class={classes.field}
                    value={data.value.orderNumber}
                    onInput={createUpdateValueFunction('orderNumber')}
                    placeholder="2589"
                  />
                </div>
                <div class={classes.fieldWrapper}>
                  <label class={classes.label} for="review-message">
                    Напишите отзыв, минимум 35 символов
                  </label>
                  <textarea
                    id="review-message"
                    required
                    ref="textarea"
                    rows={10}
                    class={classes.field}
                    value={data.value.review}
                    onInput={createUpdateValueFunction('review')}
                    minlength={35}
                  />
                </div>
                <BasePolicyLink
                  class={classes.policy}
                  // @ts-ignore
                  text={
                    slots.policyText ? slots.policyText() : defaults.policyText
                  }
                />
                <div class={classes.buttonWrapper}>
                  <BaseLoading loading={processing.value}>
                    <button
                      buttonOptions={{ size: 'custom' }}
                      type="submit"
                      disabled={processing.value}
                      class={[classes.button]}
                    >
                      <BaseIcon name="click" width={25} />
                      <span>Отправить</span>
                    </button>
                  </BaseLoading>
                </div>
              </form>
            </BaseFormResponse>
          </div>
        </div>
      )
    }
  })
)
