import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { cnThemeWrapper } from '~/components/theme/theme-wrapper'
import { BarkBeetleContentYouKnow } from '~/components/content/bark-beetle-content/components/bark-beetle-content-you-know'
import { BarkBeetleContentHeading } from '~/components/content/bark-beetle-content/components/bark-beetle-content-heading/bark-beetle-content-heading'
// import { BarkBeetleServices } from '~/components/content/bark-beetle-content/components/bark-beetle-services'
import { BarkBeetleContentList } from '~/components/content/bark-beetle-content/components/bark-beetle-content-list'
import { BaseButton, BaseVideoBlockWithButton } from '~/components/base'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { Color } from '~/components/theme/theme-button'
import style from './bark-beetle-content.scss?module'

const cn = useClassNames('bark-beetle-content', style)

const classes = {
  main: [cn()],
  wrapper: cnThemeWrapper(),
  barkImageContainer: cn('bark-image-container'),
  barkImageDescription: cn('bark-image-description'),
  barkImageHeading: cn('bark-image-heading'),
  barkImage: cn('bark-image'),
  contentPicture: cn('content-picture'),
  list: cn('list'),
  video: cn('video'),
  barkButton: cn('bark-button')
}

export const BarkBeetleContent = injectStyles(
  style,
  defineComponent({
    name: 'BarkBeetleContent',
    props: {},
    setup: (_, { root }) => {
      const modalOpener = useModalOpener()
      const openBarkBeetlePriceModal = () =>
        modalOpener({
          name: 'bark-beetle-price'
        })
      return () => (
        <div class={classes.main}>
          <div class={classes.wrapper}>
            <div class={classes.barkImageContainer}>
              <img
                class={classes.barkImage}
                src="https://ecoplant-pitomnik.ru/image/catalog/blocks/block-bark-beetle/bark-beetle.jpg"
                alt=""
              />
              <div class={classes.barkImageDescription}>
                <h2 class={classes.barkImageHeading}>
                  Короед: как выглядит, чем опасен, как избавиться
                </h2>
                <p>
                  Несмотря на то, что короеды не отличаются крупными размерами,
                  они наносят крупный ущерб садовым хозяйствам, деревянным
                  строениям и лесным массивам. За последние годы смена
                  климатических условий привела к массовому размножению этих
                  насекомых-вредителей. Кроме того, способствует этому
                  масштабная вырубка лесов без должной расчистки участков от
                  поваленных деревьев и пней, являющихся рассадниками паразитов.
                </p>
                <p>
                  В связи с этим каждый владелец дачного хозяйства должен знать,
                  что такое жук-короед и как бороться с ним в доме.
                </p>
              </div>
            </div>
            <BarkBeetleContentYouKnow>
              Жуки-короеды атакуют здоровые деревья только в том случае, если их
              много. Если популяция насекомых малочисленная, они заселяются в
              старые, слабые и больные деревья. Больше всего короедам по вкусу
              хвойные деревья, особенно сосна. Однако некоторые виды вредителя
              заселяют и другие породы, в том числе плодовые. В условиях средних
              широт наиболее распространен короед шестизубый.
            </BarkBeetleContentYouKnow>
            <div class={classes.barkButton}>
              <BaseButton
                onClick={openBarkBeetlePriceModal}
                size="l"
                color={Color.yellow}
              >
                Прайс по обработке от короеда
              </BaseButton>
            </div>
            <BarkBeetleContentHeading>
              Жук-короед: как выглядит вредитель
            </BarkBeetleContentHeading>
            <p>
              Жуки-короеды образуют группу жуков одноименного подсемейства,
              описание которых насчитывает всего 750 видов, из них 140
              распространено в Европе. Короед получил свое название из-за того,
              что большинство его разновидностей ведут свою жизнедеятельность
              под корой. Самые крупные короеды, когда-либо найденные на
              европейском континенте, достигали длины 8 мм, а самые мелкие – не
              более 1 мм. Однако в тропиках встречаются виды до 1,5 см.
            </p>
            <picture class={classes.contentPicture}>
              <img
                src={
                  'https://ecoplant-pitomnik.ru/image/catalog/blocks/block-bark-beetle/content/01.jpg'
                }
              />
            </picture>
            <p>
              Для того чтобы вовремя распознать вредителя и успеть предотвратить
              потери, которые он может нанести, очень важно изучить, как
              выглядит короед. Голова насекомого маленькая, плохо выраженная,
              вогнутая или резко выпуклая, в зависимости от пола: самцы имеют
              плоский лоб, самки – выпуклый. На передней спинке неправильными
              рядами расположены зубчики, образуя группу заметных бугорков.
              Форма спинки почти шарообразная, четырёхугольная, удлиненная. На
              надкрыльях жука – полоски или чешуйки, которые густо покрывают их
              поверхность. Взрослые особи темно-коричневого цвета, голова
              темнее, почти черная. Личинка короеда – безногая, желтовато-белая,
              похожа на маленького червя.
            </p>

            <picture class={classes.contentPicture}>
              <img
                src={
                  'https://ecoplant-pitomnik.ru/image/catalog/blocks/block-bark-beetle/content/02.jpg'
                }
              />
            </picture>
            <BarkBeetleContentYouKnow>
              Несмотря на то, что жук-короед – опасный вредитель древесных
              пород, существует также определенная польза, которую приносит
              насекомое. Короеды перерабатывают целлюлозу, благодаря чему
              считаются «санитарами леса».
            </BarkBeetleContentYouKnow>
            <p>
              Чаще всего в домашнем хозяйстве и в саду можно встретить
              вредителей трех видов:
            </p>
            <p>
              <b>Домашний жук</b> – самый крупный, достигает длины 12 - 15 мм.
              Серо-черного цвета, появляется летом, оставляет в древесине
              заметное овальное отверстие. Жизненный цикл этого вида жука самый
              продолжительный – до 10 лет, что делает борьбу с данным вредителем
              довольно затруднительной, поскольку паразит успевает разрушить
              дерево изнутри задолго до того, как его обнаружат.
            </p>
            <p>
              <b>Мебельный жук</b> – до 3 мм, темно-коричневый. Взрослые особи
              покидают дерево в конце весны или в начале лета. В теплое время
              года их можно обнаружить на стенах или потолках. Отверстия,
              которые паразиты проедают в дереве, имеют диаметр до 1,5 мм,
              поэтому, чтобы обнаружить жука, нужно тщательно осмотреть полы,
              мебель, плинтусы и стропила. Жизненный цикл насекомого составляет
              2-3 года.{' '}
            </p>
            <p>
              <b>Порошковый жук</b> – прозван так благодаря своей способности
              перемалывать дерево в порошок, похожий на муку. Взрослые особи
              также темно-коричневые, но несколько крупнее, чем предыдущие, – до
              6 мм. Появляются эти жуки в конце весны и летом, летают чаще в
              темное время суток. Больше предпочитают свежесрубленное дерево,
              поэтому их часто можно встретить в местах лесозаготовки. Жизненный
              цикл – до 1 года.
            </p>
            <div class={classes.barkButton}>
              <BaseButton
                onClick={openBarkBeetlePriceModal}
                size="l"
                color={Color.yellow}
              >
                Прайс по обработке от короеда
              </BaseButton>
            </div>
            <BarkBeetleContentHeading>
              Особенности жизненного цикла жука-короеда, чем опасен вредитель в
              саду
            </BarkBeetleContentHeading>
            <p>
              Места обитания жука могут отличаться в зависимости от вида.
              Некоторые из них развиваются только на живых деревьях, другие
              предпочитают спиленные заготовки древесины. Также специфичным для
              разных видов является и строение ходов-лабиринтов, которые
              проделывают паразиты в дереве. Таким образом, по виду ходов можно
              определить, каким видом короеда оно заражено.
            </p>

            <picture class={classes.contentPicture}>
              <img
                src={
                  'https://ecoplant-pitomnik.ru/image/catalog/blocks/block-bark-beetle/content/03.jpg'
                }
              />
            </picture>
            <p>
              Жизненный цикл большинства видов насекомых одинаков, но число
              поколений и популяций у них может отличаться и зависеть от
              географического положения и климатических условий мест обитания.
              Период от кладки яиц до взросления особей у жуков достаточно
              длительный. Сначала самка откладывает яйца – до 80 штук
              одновременно в трещины отверстия коры. Через 4-5 недель появляются
              личинки, которые сразу поедают древесину. Личинка жука-короеда
              перемещается в дереве около 3-4 лет, уничтожая за это время до
              15-20 см дерева, где она живет. После этого личинка выгрызает
              полость ближе к поверхности древесины, где превращается в куколку.
              Еще через 7-8 недель молодое насекомое покидает дерево и
              отправляется на поиски пары. После спаривания жизненный цикл
              повторяется. Сезонная активность у всех видов короедов различная:
              в средних широтах есть как весенние виды, так и те, которые могут
              летать все лето. Суточная активность у всех видов, присутствующих
              на европейском континенте, одинакова – они летают на закате.
            </p>

            <picture class={classes.contentPicture}>
              <img
                src={
                  'https://ecoplant-pitomnik.ru/image/catalog/blocks/block-bark-beetle/content/04.jpg'
                }
              />
            </picture>
            <p>
              Вредитель способен нанести непоправимый вред не только взрослым
              деревьям всех существующих пород, но и уничтожить молодые. Однако
              это не всё, чем опасен жук короед. Такие виды, как заболонник
              плодовый и морщинистый, способны уничтожить насаждения плодовых и
              косточковых деревьев, причинив ущерб всему саду. Особенно опасны
              эти жуки для деревянных домов-срубов, а также деревянных
              конструкций и других составляющих строений. Вред, который они
              способны нанести, по величине сопоставим с ущербом от пожара.
            </p>
          </div>
          <div class={classes.wrapper}>
            <BaseVideoBlockWithButton
              class={classes.video}
              buttonVisual={{ size: root.$device.isMobileOrTablet ? 100 : 200 }}
              withOverlay={true}
              poster={
                'https://ecoplant-pitomnik.ru/image/catalog/blocks/block-bark-beetle/poster.jpg'
              }
              video={
                'https://ecoplant-pitomnik.ru/files/videos/bark-beetle.mp4'
              }
              buttonText={'Видео о том,\n' + 'как мы боремся\n' + 'с короедом'}
            />
          </div>
          {/* <BarkBeetleServices class={classes.wrapper} /> */}
          <div class={classes.wrapper}>
            <div class={classes.barkButton}>
              <BaseButton
                onClick={openBarkBeetlePriceModal}
                size="l"
                color={Color.yellow}
              >
                Прайс по обработке от короеда
              </BaseButton>
            </div>
            <BarkBeetleContentHeading>
              Признаки короеда в саду, как обнаружить вредителя
            </BarkBeetleContentHeading>
            <p>
              Основную опасность для сада представляет короед плодовый и
              морщинистый, который селится на яблоне, вишне, сливе глубоко в
              древесине, что крайне затрудняет борьбу с ним. Эти паразиты и их
              личинки выгрызают лабиринты ходов, приводя к гибели деревьев.
              Стоит отметить, что насекомое выбирает в основном больные, старые
              и слабые деревья, редко нападая на здоровые культуры. Очень важно
              для эффективной борьбы с вредителем выявить его как можно раньше.
              Признаки жизнедеятельности насекомого легко заметить: регулярное
              появление дятлов на деревьях – признак поражения короедом; мелкие
              отверстия в коре – явный признак, свидетельствующий не только о
              том, что дерево поражено короедом, но и что личинки уже успели его
              покинуть и заражение может перерасти в эпидемию; осыпаются листья
              или хвоя;
            </p>
            <picture class={classes.contentPicture}>
              <img
                src={
                  'https://ecoplant-pitomnik.ru/image/catalog/blocks/block-bark-beetle/content/05.jpg'
                }
              />
            </picture>
            <p>
              Существует также ряд особо тревожных симптомов, сигнализирующих о
              том, что дерево, где обитает короед, спасти не удастся и его
              необходимо немедленно уничтожить: под стволом появляется мелкая
              труха, напоминающая сухую кофейную гущу. Это значит, что дерево
              заражено большим количеством паразитов; если со ствола дерева
              отмирает и опадает кора, значит, дерево погибло, даже если на нем
              еще присутствуют живые ветки.
            </p>
            <div class={classes.barkButton}>
              <BaseButton
                onClick={openBarkBeetlePriceModal}
                size="l"
                color={Color.yellow}
              >
                Прайс по обработке от короеда
              </BaseButton>
            </div>
            <BarkBeetleContentHeading>
              Как бороться с короедом на даче, лучшие советы
            </BarkBeetleContentHeading>
            <p>
              Основная сложность эффективной борьбы с короедом на деревьях
              заключается в том, что он практически весь свой жизненный цикл
              проводит глубоко в древесине, а это значит, что химические меры
              борьбы не дают стопроцентного результата: препарату сложно
              проникнуть вглубь дерева.
            </p>
            <p>
              Но все же бороться с такой напастью, как жук-короед, можно и
              делать это нужно незамедлительно, как только появляются подозрения
              на его наличие в саду или на деревянных строениях.
            </p>
            <p>
              Во-первых, стоит оценить степень поражения дерева. При отслаивании
              кусков коры в местах с отверстиями, общей слабости и увядании
              спасти его не получится. В таком случае пораженное растение
              спиливают и сжигают. Если же на стволе замечено не более двух-трех
              мелких отверстий, а дерево в целом выглядит здоровым, кора не
              отстает, если попытаться проткнуть ее ножом, то его можно спасти.
            </p>
            <p>
              Перед тем как бороться с вредителем, нужно провести подготовку к
              работам: надеть средства индивидуальной защиты и очистить жесткой
              щеткой дерево от грязи, чтобы инсектициды подействовали
              эффективнее.
            </p>
            <picture class={classes.contentPicture}>
              <img
                src={
                  'https://ecoplant-pitomnik.ru/image/catalog/blocks/block-bark-beetle/content/06.jpg'
                }
              />
            </picture>
            <p>
              Препараты впрыскиваются с помощью шприца в ходы, проделанные
              жуками. Доказали свою эффективность такие средства, как «Антижук»,
              «Антишашелин», «Конфидор», «Калипсо» и «Эмпайр-20». Раствор
              препаратов вводится в отверстия два-четыре раза, по мере
              впитывания, после чего ходы замазывают садовым варом.
            </p>
            <p>
              Среди методов, как еще можно бороться с вредителем, – биологически
              активные препараты на основе нематод. Это прекрасное дополнение к
              традиционной обработке инсектицидами, но возможно его
              самостоятельное использование при незначительном поражении дерева.
              Для этого применяют деготь или керосин, раствором которых
              обрабатывают отверстия на деревьях. Обработку таким раствором
              можно проводить и в профилактических целях в весеннее время, сразу
              после цветения. Обильное опрыскивание веток, стволов и кроны
              проводят дважды с промежутком в две недели.
            </p>
            <div class={classes.barkButton}>
              <BaseButton
                onClick={openBarkBeetlePriceModal}
                size="l"
                color={Color.yellow}
              >
                Прайс по обработке от короеда
              </BaseButton>
            </div>
            <BarkBeetleContentHeading>
              Профилактические действия, как обезопасить сад от вредителя
            </BarkBeetleContentHeading>
            <p>
              Профилактика появления короеда является одной из важнейших мер
              борьбы с этим вредителем в саду. Рассмотрим самые эффективные
              профилактические меры, которые могут спасти сад и деревянные
              строения от необходимости уничтожения:
            </p>
            <BarkBeetleContentList
              class={classes.list}
              items={[
                'регулярная обрезка сухих и больных веток на деревьях. Это не даст вредителям возможность распространяться.',
                'ежегодная покраска штамбов известковым раствором;',
                'обработка фосфорными и органическими препаратами в период активного отрождения личинок короеда и выхода жуков из коры, так как бороться с личинкой невозможно другими методами;',
                'при обработке деревьев химикатами нужно добавлять в раствор натертое хозяйственное мыло. Это позволит препарату лучше «прилипать» к коре. Обработку повторяют через две-три недели;',
                'создание так называемых ловушек для короедов. В летний период раскладывают свежеспиленные стволы лиственных деревьев по всему периметру участка. Самки насекомых вероятнее выберут именно эти «ловушки» для кладки яиц. В конце лета эти куски стволов сжигают;',
                'очень важно изучить, кто из полезных насекомых и птиц ест жука-короеда, чтобы создать в саду условия для их привлечения. Например, эффективным уничтожителем короедов является черный дятел;',
                'проводить регулярную обработку штамба и толстых ветвей смесью глины с перегноем или навоза с гашеной известью.'
              ]}
            />
            <p>
              Поскольку жук не любит заселять здоровые деревья, самое главное –
              регулярно проводить мероприятия по уходу за садом, чтобы
              поддерживать их иммунитет и жизнеспособность.
            </p>
          </div>
          {/* <BarkBeetleServices class={classes.wrapper} /> */}
        </div>
      )
    }
  })
)
