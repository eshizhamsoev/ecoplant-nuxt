import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './bark-beetle-content-heading.scss?module'

const cn = useClassNames('bark-beetle-content-heading', style)

export const BarkBeetleContentHeading = injectStyles(
  style,
  defineComponent({
    name: 'BarkBeetleContentHeading',
    props: {},
    setup: (_, { slots }) => {
      return () => (
        <div class={cn()}>
          <div class={cn('text')}>{slots.default && slots.default()}</div>
        </div>
      )
    }
  })
)
