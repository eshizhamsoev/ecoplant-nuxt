import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './bark-beetle-icons.scss?module'

const cn = useClassNames('bark-beetle-icons', style)

const classes = {
  main: cn(),
  icon: cn('icon')
}

export const BarkBeetleIcons = injectStyles(
  style,
  defineComponent({
    name: 'BarkBeetleIcons',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <img
            src="https://ecoplant-pitomnik.ru/image/catalog/blocks/block-bark-beetle/headers/1.jpg"
            class={classes.icon}
            alt=""
          />
          <img
            src="https://ecoplant-pitomnik.ru/image/catalog/blocks/block-bark-beetle/headers/2.jpg"
            class={classes.icon}
            alt=""
          />
          <img
            src="https://ecoplant-pitomnik.ru/image/catalog/blocks/block-bark-beetle/headers/3.jpg"
            class={classes.icon}
            alt=""
          />
        </div>
      )
    }
  })
)
