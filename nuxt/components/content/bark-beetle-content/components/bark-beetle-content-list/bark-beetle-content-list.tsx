import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import style from './bark-beetle-content-list.scss?module'

const cn = useClassNames('bark-beetle-content-list', style)

const classes = {
  main: cn(),
  item: cn('item'),
  icon: cn('icon'),
  text: cn('text')
}

export const BarkBeetleContentList = injectStyles(
  style,
  defineComponent({
    name: 'BarkBeetleContentList',
    props: {
      items: {
        type: Array as () => string[],
        required: true
      }
    },
    setup: (props, { root }) => {
      return () => (
        <div>
          <ul class={classes.main}>
            {props.items.map((item) => (
              <li class={classes.item}>
                <BaseIcon
                  name="check"
                  width={root.$device.isMobileOrTablet ? 20 : 40}
                  height={root.$device.isMobileOrTablet ? 40 : 40}
                  class={classes.icon}
                />
                <span class={classes.text}>{item}</span>
              </li>
            ))}
          </ul>
        </div>
      )
    }
  })
)
