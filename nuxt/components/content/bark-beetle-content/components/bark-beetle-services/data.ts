export const prices = [
  { text: 'до 1 метра', price: '250' },
  { text: '1.0-2.0 метра', price: '300' },
  { text: '2.0-3.0 метра', price: '400' },
  { text: '3.0-4.0 метра', price: '450' },
  { text: '4.0-5.0 метров', price: '500' },
  { text: '5.0-6.0 метров', price: '700' },
  { text: '6.0-7.0 метров', price: '850' },
  { text: '7.0-8.0 метров', price: '1000' },
  { text: '1 п.м. живая изгородь до 1 м.', price: '300' },
  { text: '1 п.м. живая изгородь 1-2 м.', price: '400' }
]
