import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { prices } from '~/components/content/bark-beetle-content/components/bark-beetle-services/data'
import { BarkBeetleIcons } from '~/components/content/bark-beetle-content/components/bark-beetle-icons'
import style from './bark-beetle-services.scss?module'

const cn = useClassNames('bark-beetle-services', style)

const classes = {
  main: cn(),
  inner: cn('inner'),
  images: cn('images'),
  image: cn('image'),
  topText: cn('top-text'),
  bottomHeading: cn('bottom-heading'),
  bottomText: cn('bottom-text'),
  table: cn('table'),
  tableWrapper: cn('table-wrapper')
}

export const BarkBeetleServices = injectStyles(
  style,
  defineComponent({
    name: 'BarkBeetleServices',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <div class={classes.inner}>
            <BarkBeetleIcons class={classes.images} />
            <div class={classes.topText}>
              Если Вы не имеете садовника, если у Вас нет ни времени, ни желания
              заниматься этим, мы готовы сделать это за Вас по этим ценам:
            </div>
            <div class={classes.tableWrapper}>
              <table class={classes.table}>
                <thead>
                  <tr>
                    <th>АРАМЕТРЫ</th>
                    <th>ЦЕНА РАБОТ, РУБ.</th>
                  </tr>
                </thead>
                <tbody>
                  {prices.map(({ text, price }) => (
                    <tr key={text}>
                      <td>{text}</td>
                      <td>{price}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
            <div class={classes.bottomHeading}>ВНИМАНИЕ, ОЧЕНЬ ВАЖНО!!!</div>
            <div class={classes.bottomText}>
              Покупая дерево, вы покупаете живой организм, как кошку или собаку,
              выполнять эти рекомендации нужно в любом случае, будь это наша
              компания или кто-то другой, иначе дерево погибнет!
            </div>
          </div>
        </div>
      )
    }
  })
)
