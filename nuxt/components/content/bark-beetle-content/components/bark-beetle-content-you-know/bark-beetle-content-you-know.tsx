import { defineComponent } from '@nuxtjs/composition-api'
import man from './resources/man.jpg'
// @ts-ignore
import manWebp from './resources/man.jpg?webp'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseWebpPicture } from '~/components/base/base-webp-picture'
import style from './bark-beetle-content-you-know.scss?module'

const cn = useClassNames('content-you-know', style)

const classes = {
  main: cn(),
  image: cn('image'),
  text: cn('text')
}

export const BarkBeetleContentYouKnow = injectStyles(
  style,
  defineComponent({
    name: 'BarkBeetleContentYouKnow',
    props: {},
    setup: (props, { slots }) => {
      return () => (
        <div class={classes.main}>
          <BaseWebpPicture
            webpImage={manWebp}
            image={man}
            class={classes.image}
          />
          <div class={classes.text}>
            <b>Знаете ли вы?</b> {slots.default && slots.default()}
          </div>
        </div>
      )
    }
  })
)
