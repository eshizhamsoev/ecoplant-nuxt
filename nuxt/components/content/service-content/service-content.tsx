import { computed, defineComponent, PropType } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseText } from '~/components/base/base-text/base-text'
import { ServiceContent as ServiceContentItem } from '~/_generated/types'
import { BaseImageGrid } from '~/components/base'
import style from './service-content.scss?module'

const cn = useClassNames('service-content', style)

export const ServiceContent = injectStyles(
  style,
  defineComponent({
    name: 'ServiceContent',
    props: {
      content: {
        type: Object as PropType<ServiceContentItem>,
        required: true
      }
    },
    setup: (props) => {
      const images = computed(
        () =>
          props.content.__typename === 'ServiceGallery' &&
          props.content.images.map(({ original, large }) => ({
            small: large,
            large: original
          }))
      )
      return () => {
        switch (props.content.__typename) {
          case 'ServiceText':
            return (
              props.content.text && (
                <BaseText
                  text={props.content.text}
                  class={cn({ type: 'text' })}
                />
              )
            )

          case 'ServiceGallery':
            return (
              images.value && (
                <BaseImageGrid
                  popupAvailable={true}
                  lg={Math.min(images.value.length, 4)}
                  images={images.value}
                  class={cn({ type: 'gallery' })}
                />
              )
            )
          default:
            return null
        }
      }
    }
  })
)
