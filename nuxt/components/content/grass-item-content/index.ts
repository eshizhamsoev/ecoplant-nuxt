import type { ComponentInstance } from '@nuxtjs/composition-api'

export const GrassItemContentFactory = (root: ComponentInstance) => () =>
  root.$device.isMobileOrTablet
    ? import('./mobile').then((c) => c.MobileGrassItemContent)
    : import('./desktop').then((c) => c.DesktopGrassItemContent)
