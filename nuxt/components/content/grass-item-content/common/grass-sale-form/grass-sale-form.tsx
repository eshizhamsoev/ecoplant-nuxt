import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { DesktopSaleFormLine } from '~/components/blocks/shared-components/sale-form-line/desktop'
import { MobileSaleFormLine } from '~/components/blocks/shared-components/sale-form-line/mobile'
import style from './grass-sale-form.scss?module'

const cn = useClassNames('grass-sale-form', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  form: cn('form'),
  policy: cn('policy')
}

export const GrassSaleForm = injectStyles(
  style,
  defineComponent({
    name: 'GrassSaleForm',
    props: {
      source: {
        type: String,
        required: true
      }
    },
    setup: (props, { root }) => {
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>
            Забронируйте за собой цену,
            <br /> оставив номер телефона
          </div>
          {root.$device.isMobileOrTablet ? (
            <MobileSaleFormLine source={props.source} class={classes.form} />
          ) : (
            <DesktopSaleFormLine source={props.source} class={classes.form} />
          )}
          <div class={classes.policy}>
            Согласен с политикой конфиденциальности
          </div>
        </div>
      )
    }
  })
)
