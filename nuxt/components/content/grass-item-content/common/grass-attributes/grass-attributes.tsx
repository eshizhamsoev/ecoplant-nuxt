import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { GrassProductAttribute } from '~/store/grass'
import style from './grass-attributes.scss?module'

const cn = useClassNames('grass-attributes', style)

const classes = {
  main: cn(),
  table: cn('table')
}

export const GrassAttributes = injectStyles(
  style,
  defineComponent({
    name: 'GrassAttributes',
    props: {
      attributes: {
        type: Array as () => GrassProductAttribute[],
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          <table class={classes.table}>
            <tbody>
              {props.attributes.map((attribute) => (
                <tr>
                  <th>{attribute.title}</th>
                  <td>{attribute.value}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )
    }
  })
)
