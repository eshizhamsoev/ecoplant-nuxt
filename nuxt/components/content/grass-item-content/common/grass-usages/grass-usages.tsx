import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { GrassProductUsage } from '~/store/grass'
import style from './grass-usages.scss?module'

const cn = useClassNames('grass-usages', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  items: cn('items'),
  item: cn('item'),
  icon: cn('icon')
}

export const GrassUsages = injectStyles(
  style,
  defineComponent({
    name: 'GrassUsages',
    props: {
      usages: {
        type: Array as () => GrassProductUsage[],
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>Места применения</div>
          <div class={classes.items}>
            {props.usages.map((usage) => (
              <div class={classes.item}>
                <BaseIcon
                  name={'grass/' + usage.icon}
                  width={42}
                  class={classes.icon}
                />
                <div>{usage.text}</div>
              </div>
            ))}
          </div>
        </div>
      )
    }
  })
)
