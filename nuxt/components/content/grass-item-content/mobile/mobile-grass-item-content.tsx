import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseSalePrice } from '~/components/base/base-sale-price'
import { GrassProduct } from '~/store/grass'
import { MobileImageSlider } from '~/components/blocks/shared-components/mobile-image-slider'
import { GrassUsages } from '~/components/content/grass-item-content/common/grass-usages'
import { GrassAttributes } from '~/components/content/grass-item-content/common/grass-attributes/grass-attributes'
import { GrassSaleForm } from '~/components/content/grass-item-content/common/grass-sale-form/grass-sale-form'
import { MobileBlockPhoneButton } from '~/components/blocks/block-phone-button/mobile'
import style from './mobile-grass-item-content.scss?module'

const cn = useClassNames('mobile-grass-item-content', style)

const classes = {
  main: cn(),
  slider: cn('slider'),
  heading: cn('heading'),
  usages: cn('usages'),
  attributes: cn('attributes'),
  sale: cn('sale'),
  saleImageWrapper: cn('sale-image-wrapper'),
  saleForm: cn('sale-form'),
  salePrice: cn('sale-price'),
  saleUntil: cn('sale-until'),
  salePriceLabel: cn('sale-price-label'),
  salePriceValue: cn('sale-price-value'),
  salePriceOldValue: cn('sale-price-old-value')
}

export const MobileGrassItemContent = injectStyles(
  style,
  defineComponent({
    name: 'MobileGrassItemContent',
    props: {
      product: {
        type: Object as () => GrassProduct,
        required: true
      }
    },
    setup: (props) => {
      const source = computed(() => 'Газоны: ' + props.product.heading)
      const images = computed(() =>
        props.product.images.map((image) => ({
          small: image.large,
          large: image.full
        }))
      )
      const saleBackground = computed(() => ({
        backgroundImage: `url(${props.product.image})`
      }))
      return () => (
        <div class={classes.main}>
          <MobileBlockPhoneButton />
          <MobileImageSlider images={images.value} class={classes.slider} />
          <div class={classes.heading}>{props.product.heading}</div>
          <GrassUsages usages={props.product.usage} class={classes.usages} />
          <GrassAttributes
            attributes={props.product.attributes}
            class={classes.attributes}
          />
          <div class={classes.sale}>
            <div class={classes.saleImageWrapper} style={saleBackground.value}>
              <BaseSalePrice
                priceData={props.product.price}
                size="m"
                class={classes.salePrice}
                classes={{
                  label: classes.salePriceLabel,
                  price: classes.salePriceValue,
                  oldPrice: classes.salePriceOldValue
                }}
              />
              <div class={classes.saleUntil}>
                <b>Внимание!</b>
                <br /> Скидка актуальна только сегодня
              </div>
            </div>
            <GrassSaleForm source={source.value} class={classes.saleForm} />
          </div>
        </div>
      )
    }
  })
)
