import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ProductCall } from '~/components/blocks/block-category/products-panel/product-actions/product-call'
import { ProductChat } from '~/components/blocks/block-category/products-panel/product-actions/product-chat'
import { BaseSliderWithThumbs } from '~/components/base/base-slider-with-thumbs/base-slider-with-thumbs'
import { BaseSalePrice } from '~/components/base/base-sale-price'
import { GrassProduct } from '~/store/grass'
import { GrassUsages } from '~/components/content/grass-item-content/common/grass-usages/grass-usages'
import { GrassAttributes } from '~/components/content/grass-item-content/common/grass-attributes/grass-attributes'
import { GrassSaleForm } from '~/components/content/grass-item-content/common/grass-sale-form/grass-sale-form'
import style from './desktop-grass-item-content.scss?module'

const cn = useClassNames('desktop-grass-item-content', style)

const classes = {
  main: cn(),
  headingLine: cn('heading-line'),
  heading: cn('heading'),
  buttons: cn('buttons'),
  imageLine: cn('image-line'),
  images: cn('images'),
  right: cn('right'),
  usages: cn('usages'),
  attributes: cn('attributes'),
  sale: cn('sale'),
  button: cn('button'),
  saleHeading: cn('sale-heading'),
  saleForm: cn('sale-form'),
  salePrice: cn('sale-price'),
  saleUntil: cn('sale-until'),
  salePolicy: cn('sale-policy'),
  saleLeft: cn('sale-left'),
  saleRight: cn('sale-right')
}

export const DesktopGrassItemContent = injectStyles(
  style,
  defineComponent({
    name: 'DesktopGrassItemContent',
    props: {
      product: {
        type: Object as () => GrassProduct,
        required: true
      }
    },
    setup: (props) => {
      const source = computed(() => 'Газоны: ' + props.product.heading)
      return () => (
        <div class={classes.main}>
          <div class={classes.headingLine}>
            <div class={classes.heading}>{props.product.heading}</div>
            <div class={classes.buttons}>
              <ProductCall productName={source.value} class={classes.button} />
              <ProductChat class={classes.button} />
            </div>
          </div>
          <div class={classes.imageLine}>
            <BaseSliderWithThumbs
              images={props.product.images}
              thumbSize={{ width: 100, height: 158 }}
              sliderSize={{ width: 376, height: 494 }}
            />
            <div class={classes.right}>
              <GrassUsages
                usages={props.product.usage}
                class={classes.usages}
              />
              <GrassAttributes
                attributes={props.product.attributes}
                class={classes.attributes}
              />
            </div>
          </div>
          <div class={classes.sale}>
            <BaseSalePrice
              priceData={props.product.price}
              size="m"
              class={classes.salePrice}
            />
            <div class={classes.saleLeft}>
              <img src={props.product.image} alt="" />
              <div class={classes.saleUntil}>
                <b>Внимание!</b> Скидка <br /> актуальна только сегодня
              </div>
            </div>
            <GrassSaleForm class={classes.saleRight} source={source.value} />
          </div>
        </div>
      )
    }
  })
)
