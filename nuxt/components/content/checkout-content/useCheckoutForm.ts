import type { ComponentInstance } from '@nuxtjs/composition-api'
import { ref, inject, computed } from '@nuxtjs/composition-api'

import { CheckoutUserFormData } from '~/components/content/checkout-content/types'
import {
  FORM_TYPES,
  OldProductPrice,
  OldProductType,
  useForm
} from '~/support/hooks/useForm'
import { GTM_EVENTS } from '~/support/hooks/useReachGoal'
import { cartSymbol, ProductType } from '~/data/providers/cart/symbol'

function mutateProductsToOldFormat(products: ProductType[]): OldProductType[] {
  return products.map((product) => ({
    product: {
      name: product.name
    },
    prices: product.prices
      .map((price) => ({
        priceData: {
          size: price.size,
          price: price.price
        },
        quantity: price.cartQuantity,
        total: price.total
      }))
      .filter(
        (price): price is OldProductPrice =>
          typeof price.priceData.size === 'string' &&
          typeof price.priceData.price === 'number' &&
          typeof price.total === 'number'
      )
  }))
}

export const useCheckoutForm = (root: ComponentInstance) => {
  const cart = inject(cartSymbol)
  const formData = ref<CheckoutUserFormData>({
    name: '',
    phone: '',
    email: '',
    address: '',
    shippingDate: '',
    planting: null,
    ground: null,
    fertilizer: null
  } as CheckoutUserFormData)
  const { successMessage, fatalError, submit, errorFields } = useForm(
    root,
    FORM_TYPES.ORDER,
    {
      goal: GTM_EVENTS.order
    }
  )
  const cartProducts = computed(() => cart?.products.value || [])
  const submitHandler = (e: Event) => {
    e.preventDefault()
    const products = mutateProductsToOldFormat(cartProducts.value)
    submit({
      ...formData.value,
      planting: !!formData.value.planting,
      ground: !!formData.value.ground,
      fertilizer: !!formData.value.fertilizer,
      products,
      total: cart?.total.value
    }).then(() => {
      if (successMessage.value) {
        if (cart?.clearCart) {
          return cart?.clearCart()
        }
      }
    })
  }

  return { formData, successMessage, fatalError, errorFields, submitHandler }
}
