import { defineComponent, inject } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { CheckoutForm } from '~/components/content/checkout-content/common/form'
import { CheckoutProducts } from '~/components/content/checkout-content/common/products'
import { cnThemeWrapper } from '~/components/theme/theme-wrapper'
import { CheckoutSummary } from '~/components/content/checkout-content/common/summary'
import { useCheckoutForm } from '~/components/content/checkout-content/useCheckoutForm'
import { BaseFormResponse } from '~/components/base'
import { CheckoutUserFormData } from '~/components/content/checkout-content/types'
import { cartSymbol } from '~/data/providers/cart/symbol'
import style from './desktop-checkout-content.scss?module'

const cn = useClassNames('desktop-checkout-content', style)

const classes = {
  main: [cn(), cnThemeWrapper()],
  heading: cn('heading'),
  inner: cn('inner'),
  left: cn('left'),
  right: cn('right'),
  summary: cn('summary')
}

export const DesktopCheckoutContent = injectStyles(
  style,
  defineComponent({
    name: 'DesktopCheckoutContent',
    props: {},
    setup: (_, { root }) => {
      const cart = inject(cartSymbol)
      const {
        formData,
        successMessage,
        fatalError,
        errorFields,
        submitHandler
      } = useCheckoutForm(root)
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>Ваш заказ</div>
          <BaseFormResponse
            fatalError={fatalError.value}
            successMessage={successMessage.value}
          >
            <form onSubmit={submitHandler}>
              <div class={classes.inner}>
                <div class={classes.left}>
                  <CheckoutForm
                    value={formData.value}
                    onInput={(value: CheckoutUserFormData) => {
                      formData.value = value
                    }}
                    errorFields={errorFields.value}
                  />
                </div>
                <div class={classes.right}>
                  <CheckoutProducts products={cart?.products.value || []} />
                  <CheckoutSummary
                    class={classes.summary}
                    total={cart?.total.value || 0}
                  />
                </div>
              </div>
            </form>
          </BaseFormResponse>
        </div>
      )
    }
  })
)
