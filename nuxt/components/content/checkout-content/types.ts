export type CheckoutUserFormData = {
  name: string
  phone: string
  email: string
  address: string
  shippingDate: string
  planting: boolean | null
  ground: boolean | null
  fertilizer: boolean | null
}
