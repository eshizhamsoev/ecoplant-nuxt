import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { CheckoutProductPrice } from '~/components/content/checkout-content/common/product-price/checkout-product-price'
import { decodeHTMLEntities } from '~/support/utils/decode-entities'
import { Product } from '~/_generated/types'
import { ExtendedPrice } from '~/data/providers/cart/symbol'
import style from './checkout-product.scss?module'

const cn = useClassNames('checkout-product', style)

const classes = {
  main: cn(),
  headingLine: cn('heading-line'),
  image: cn('image'),
  name: cn('name'),
  priceLine: cn('price-line'),
  priceSeparator: cn('price-separator')
}

export const CheckoutProduct = injectStyles(
  style,
  defineComponent({
    name: 'CheckoutProduct',
    props: {
      cartProductInfo: {
        type: Object as () => Pick<Product, 'name' | 'id' | 'image'> & {
          prices: ExtendedPrice[]
        },
        required: true
      },
      visibleColumns: {
        type: Array,
        default: () => ['size', 'price', 'quantity', 'total', 'remove']
      }
    },
    setup: (props) => {
      const prices = computed(() => props.cartProductInfo.prices || [])
      const name = computed(() =>
        decodeHTMLEntities(props.cartProductInfo.name)
      )
      const colspan = computed(() => props.visibleColumns.length)
      return () => (
        <tbody class={classes.main}>
          <tr>
            <td colspan={colspan.value}>
              <div class={classes.headingLine}>
                <img
                  src={props.cartProductInfo.image}
                  class={classes.image}
                  alt=""
                />
                <span class={classes.name}>{name.value}</span>
              </div>
            </td>
          </tr>
          {prices.value.map((cartPrice, i) => [
            i > 0 ? (
              <tr>
                <td
                  class={classes.priceSeparator}
                  colspan={colspan.value}
                  key={'sep' + i}
                />
              </tr>
            ) : null,
            <CheckoutProductPrice
              key={cartPrice.id}
              product={props.cartProductInfo}
              cartPriceInfo={cartPrice}
              visibleColumns={props.visibleColumns}
              class={classes.priceLine}
            />
          ])}
        </tbody>
      )
    }
  })
)
