import {
  defineComponent,
  PropOptions,
  ref,
  watch
} from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePhoneInput } from '~/components/base/base-phone-input'
import { CheckoutUserFormData } from '~/components/content/checkout-content/types'
import { BaseButtonSwitcher } from '~/components/base/base-button-switcher'
import style from './checkout-form.scss?module'

const cn = useClassNames('checkout-form', style)

const classes = {
  main: cn(),
  fieldLine: cn('field-line'),
  fieldLabel: cn('field-label'),
  fieldInput: cn('field-input'),
  fieldTextarea: cn('field-textarea')
}

export const CheckoutForm = injectStyles(
  style,
  defineComponent({
    name: 'CheckoutForm',
    props: {
      value: {
        type: Object as () => CheckoutUserFormData,
        required: true
      },
      errorFields: {
        type: Array as () => string[],
        required: false,
        default(): string[] {
          return []
        }
      } as PropOptions<string[], string[]>
    },
    setup: (props, { emit }) => {
      const innerValue = ref<CheckoutUserFormData>(props.value)
      watch(
        () => innerValue.value,
        () => {
          emit('input', innerValue.value)
        },
        { deep: true }
      )
      watch(
        () => props.value,
        () => {
          innerValue.value = props.value
        }
      )

      function createUpdateValueFunction(
        fieldName: keyof CheckoutUserFormData
      ) {
        return (event: InputEvent & { target?: HTMLInputElement }) => {
          // @ts-ignore
          innerValue.value[fieldName] = event.target?.value as string
        }
      }
      return () => (
        <div class={classes.main}>
          <div class={classes.fieldLine}>
            <label for="name" class={classes.fieldLabel}>
              Введите ФИО
            </label>
            <input
              value={innerValue.value.name}
              onInput={createUpdateValueFunction('name')}
              name="name"
              id="name"
              class={cn('field-input', {
                error: props.errorFields?.includes('name')
              })}
              placeholder="Введите ФИО"
              required
            />
          </div>
          <div class={classes.fieldLine}>
            <label for="phone" class={classes.fieldLabel}>
              Введите телефон
            </label>
            <BasePhoneInput
              value={innerValue.value.phone}
              onInput={(v: string) => (innerValue.value.phone = v)}
              id="phone"
              required="required"
              class={cn('field-input', {
                error: props.errorFields?.includes('phone')
              })}
            />
          </div>
          <div class={classes.fieldLine}>
            <label for="email" class={classes.fieldLabel}>
              Введите почту
            </label>
            <input
              value={innerValue.value.email}
              onInput={createUpdateValueFunction('email')}
              name="email"
              type="email"
              id="email"
              class={cn('field-input', {
                error: props.errorFields?.includes('email')
              })}
              placeholder="Введите почту"
              required
            />
          </div>
          <div class={classes.fieldLine}>
            <label for="address" class={classes.fieldLabel}>
              Введите адрес
            </label>
            <textarea
              class={cn('field-textarea', {
                error: props.errorFields?.includes('address')
              })}
              value={innerValue.value.address}
              onInput={createUpdateValueFunction('address')}
              name="address"
              id="address"
              required
              placeholder="Деревня Березки ул. Ленина дом 7"
            />
          </div>
          <div class={classes.fieldLine}>
            <label for="shipping_date" class={classes.fieldLabel}>
              Желаемая дата доставки
            </label>
            <input
              type="date"
              class={cn('field-input', {
                error: props.errorFields?.includes('shipping_date')
              })}
              value={innerValue.value.shippingDate}
              onInput={createUpdateValueFunction('shippingDate')}
              name="shipping_date"
              id="shipping_date"
              placeholder=""
            />
          </div>
          <div class={cn('field-line', { type: 'checkbox' })}>
            <label for="planting" class={classes.fieldLabel}>
              Нужна посадка
            </label>
            <BaseButtonSwitcher
              name="planting"
              value={innerValue.value.planting}
              onInput={(value: boolean) => {
                innerValue.value.planting = value
              }}
              inputId="planting"
              required
            />
          </div>
          <div class={cn('field-line', { type: 'checkbox' })}>
            <label for="ground" class={classes.fieldLabel}>
              Нужен грунт
            </label>
            <BaseButtonSwitcher
              name="ground"
              value={innerValue.value.ground}
              onInput={(value: boolean) => {
                innerValue.value.ground = value
              }}
              inputId="ground"
              required
            />
          </div>
          <div class={cn('field-line', { type: 'checkbox' })}>
            <label for="fertilizer" class={classes.fieldLabel}>
              Нужно удобрение
            </label>
            <BaseButtonSwitcher
              name="fertilizer"
              value={innerValue.value.fertilizer}
              onInput={(value: boolean) => {
                innerValue.value.fertilizer = value
              }}
              inputId="fertilizer"
              required
            />
          </div>
        </div>
      )
    }
  })
)
