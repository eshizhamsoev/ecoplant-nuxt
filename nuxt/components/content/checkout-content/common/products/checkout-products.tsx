import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { CheckoutProduct } from '~/components/content/checkout-content/common/product/checkout-product'
import { ExtendedPrice } from '~/data/providers/cart/symbol'
import { Product } from '~/_generated/types'
import style from './checkout-products.scss?module'

const cn = useClassNames('checkout-products', style)

const classes = {
  main: cn(),
  product: cn('product'),
  productSeparator: cn('product-separator')
}

type CartProduct = Pick<Product, 'name' | 'id' | 'image'> & {
  prices: ExtendedPrice[]
}

export const CheckoutProducts = injectStyles(
  style,
  defineComponent({
    name: 'CheckoutProducts',
    props: {
      visibleColumns: {
        type: Array,
        default: () => ['size', 'price', 'quantity', 'total', 'remove']
      },
      products: {
        type: Array as () => CartProduct[],
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <table class={classes.main}>
          {props.products.map((cartProduct, i) => [
            i > 0 ? (
              <tbody key={'sep' + i}>
                <tr>
                  <td
                    colspan={props.visibleColumns.length}
                    class={classes.productSeparator}
                  />
                </tr>
              </tbody>
            ) : null,
            <CheckoutProduct
              class={classes.product}
              cartProductInfo={cartProduct}
              visibleColumns={props.visibleColumns}
              key={cartProduct.id}
            />
          ])}
        </table>
      )
    }
  })
)
