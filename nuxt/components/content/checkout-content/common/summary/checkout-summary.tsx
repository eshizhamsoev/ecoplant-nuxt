import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton, BaseRubles } from '~/components/base'
import { Color } from '~/components/theme/theme-button'
import style from './checkout-summary.scss?module'

const cn = useClassNames('checkout-summary', style)

const classes = {
  main: cn(),
  total: cn('total'),
  totalValue: cn('total-value'),
  totalComment: cn('total-comment'),
  button: cn('button'),
  buttonText: cn('button-text'),
  buttonSum: cn('button-sum'),
  policy: cn('policy')
}

export const CheckoutSummary = injectStyles(
  style,
  defineComponent({
    name: 'CheckoutSummary',
    props: {
      total: {
        type: Number,
        required: true
      }
    },
    setup: (props, { root }) => {
      const showExtraText = computed(
        () => root.$accessor.showExpandedInformation
      )
      const text = computed(() =>
        showExtraText.value
          ? 'Доставка рассчитывается в зависимости от объема растений. Доставляем по всей России.'
          : 'Сумма без учета доставки'
      )
      return () => (
        <div class={classes.main}>
          <div class={classes.total}>
            Итого:
            <BaseRubles price={props.total} class={classes.totalValue} />
          </div>
          <div class={classes.totalComment}>{text.value}</div>
          <BaseButton
            color={Color.red}
            class={classes.button}
            shadow
            size="custom"
            type="submit"
          >
            <span>
              <span class={classes.buttonText}>Заказать</span>
              <span class={classes.buttonSum}>
                <BaseRubles price={props.total} />
              </span>
            </span>
          </BaseButton>
          <div class={classes.policy}>
            Согласен(-на) с политикой конфиденциальности
          </div>
        </div>
      )
    }
  })
)
