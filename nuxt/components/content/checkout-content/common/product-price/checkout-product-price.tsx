import { defineComponent } from '@nuxtjs/composition-api'
import { useMutation } from '@vue/apollo-composable'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon, BaseRubles } from '~/components/base'
import { BaseQuantity } from '~/components/base/base-quantity/base-quantity'
import { ExtendedPrice } from '~/data/providers/cart/symbol'
import {
  CartProductsQuery,
  MutationCartQuantityArgs,
  Product
} from '~/_generated/types'
import cartQuantityMutation from '~/graphql/mutations/CartQuantity.graphql'
import style from './checkout-product-price.scss?module'

const cn = useClassNames('checkout-product-price', style)

const classes = {
  main: cn(),
  size: cn('size'),
  price: cn('price'),
  quantity: cn('quantity'),
  plus: cn('plus'),
  minus: cn('minus'),
  total: cn('total'),
  remove: cn('remove'),
  removeButton: cn('remove-button')
}

export const CheckoutProductPrice = injectStyles(
  style,
  defineComponent({
    name: 'CheckoutProductPrice',
    props: {
      product: {
        type: Object as () => Pick<Product, 'name' | 'id'>,
        required: true
      },
      cartPriceInfo: {
        type: Object as () => ExtendedPrice,
        required: true
      },
      visibleColumns: {
        type: Array,
        default: () => ['size', 'price', 'quantity', 'total', 'remove']
      }
    },
    setup: (props) => {
      const { mutate } = useMutation<
        CartProductsQuery,
        MutationCartQuantityArgs
      >(cartQuantityMutation)
      const updateQuantity = (quantity: number) =>
        mutate({
          productId: props.product.id,
          priceId: props.cartPriceInfo.id,
          quantity
        })
      const remove = () =>
        mutate({
          productId: props.product.id,
          priceId: props.cartPriceInfo.id,
          quantity: 0
        })
      return () => (
        <tr class={classes.main}>
          {props.visibleColumns.includes('size') && (
            <td class={classes.size}>{props.cartPriceInfo.size}</td>
          )}
          {props.visibleColumns.includes('price') && (
            <td class={classes.price}>
              <BaseRubles price={props.cartPriceInfo.price || 0} />
            </td>
          )}
          {props.visibleColumns.includes('quantity') && (
            <td class={classes.quantity}>
              <BaseQuantity
                minimum={0}
                maximum={99999}
                value={props.cartPriceInfo.cartQuantity}
                onInput={updateQuantity}
              />
            </td>
          )}
          {props.visibleColumns.includes('total') && (
            <td class={classes.total}>
              <BaseRubles price={props.cartPriceInfo.total || 0} />
            </td>
          )}
          {props.visibleColumns.includes('remove') && (
            <td class={classes.remove}>
              <button class={classes.removeButton} onClick={remove}>
                <BaseIcon name="times-circle" width={40} />
              </button>
            </td>
          )}
        </tr>
      )
    }
  })
)
