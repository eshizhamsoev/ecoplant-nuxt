import { defineComponent, inject } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { CheckoutForm } from '~/components/content/checkout-content/common/form'
import { CheckoutProducts } from '~/components/content/checkout-content/common/products'
import { CheckoutSummary } from '~/components/content/checkout-content/common/summary'
import { BaseFormResponse } from '~/components/base'
import { useCheckoutForm } from '~/components/content/checkout-content/useCheckoutForm'
import { CheckoutUserFormData } from '~/components/content/checkout-content/types'
import { cartSymbol } from '~/data/providers/cart/symbol'
import style from './mobile-checkout-content.scss?module'

const cn = useClassNames('mobile-checkout-content', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  form: cn('form'),
  products: cn('products'),
  productHeading: cn('product-heading'),
  summary: cn('summary')
}

export const MobileCheckoutContent = injectStyles(
  style,
  defineComponent({
    name: 'MobileCheckoutContent',
    props: {},
    setup: (_, { root }) => {
      const cart = inject(cartSymbol)
      const {
        formData,
        successMessage,
        fatalError,
        errorFields,
        submitHandler
      } = useCheckoutForm(root)
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>Ваш заказ</div>
          <BaseFormResponse
            fatalError={fatalError.value}
            successMessage={successMessage.value}
          >
            <form onSubmit={submitHandler}>
              <CheckoutForm
                value={formData.value}
                onInput={(value: CheckoutUserFormData) => {
                  formData.value = value
                }}
                class={classes.form}
                errorFields={errorFields.value}
              />
              <div class={classes.productHeading}>Состав заказа</div>
              <div class={classes.products}>
                <CheckoutProducts
                  visibleColumns={['price', 'size', 'quantity']}
                  products={cart?.products.value || []}
                />
              </div>
              <CheckoutSummary
                class={classes.summary}
                total={cart?.total.value || 0}
              />
            </form>
          </BaseFormResponse>
        </div>
      )
    }
  })
)
