import { useClassNames } from '~/support/utils/bem-classnames'
import style from './theme-wrapper.scss?module'

style.__inject__ && style.__inject__()

export const cnThemeWrapper = useClassNames('theme-wrapper', style)
