import { useClassNames } from '~/support/utils/bem-classnames'
import style from './theme-button.scss?module'

style.__inject__ && style.__inject__()

export const enum Visual {
  default = 'none',
  gradient = 'gradient',
  outline = 'outline',
  dark = 'dark'
}

export const enum Color {
  green = 'green',
  yellow = 'yellow',
  red = 'red'
}

interface ThemeButtonProps {
  color: Color
  visual?: Visual | null
  shadow?: boolean
}

type ThemeButtonClassNameFormatter = (mods?: ThemeButtonProps) => string

export const cnThemeButton = useClassNames(
  'theme-button',
  style
) as ThemeButtonClassNameFormatter
