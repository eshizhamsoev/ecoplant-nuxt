// @ts-ignore
import fertilizer from '~/assets/common/fertilizer.png?resize&size=55'
import catalogue from '~/components/blocks/block-heading/resources/icons/catalogue.png'
import tree from '~/components/blocks/block-heading/resources/icons/tree.png'

export const headingIcons = [
  {
    image: catalogue.toString(),
    content: () => [<b>9670 видов</b>, <br />, 'растений']
  },
  {
    image: fertilizer.toString(),
    content: () => ['Посадим', <br />, <b>за 1 день</b>]
  },
  {
    image: tree.toString(),
    content: () => [<b>100%</b>, <br />, 'приживаемость']
  }
]
