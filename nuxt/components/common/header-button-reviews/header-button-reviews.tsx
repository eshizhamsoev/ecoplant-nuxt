import {
  computed,
  defineComponent,
  onMounted,
  ref
} from '@nuxtjs/composition-api'
import img from './images/nagiev.png'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton, BaseIcon } from '~/components/base'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { videoReviewsQuery } from '~/graphql/queries'
import style from './header-button-reviews.scss?module'

const cn = useClassNames('header-button-reviews', style)
const classes = {
  main: cn(),
  button: cn('button'),
  inner: cn('inner'),
  image: cn('image'),
  play: cn('play'),
  blink: cn('blink'),
  blinkInner: cn('blink-inner')
}

export const HeaderButtonReviews = injectStyles(
  style,
  defineComponent({
    name: 'HeaderButtonReviews',
    setup(props, { refs }) {
      const modalOpener = useModalOpener()
      const openVideoReviewsModal = () => modalOpener({ name: 'video-reviews' })
      const enabled = ref(false)
      const options = computed(() => ({
        enabled: enabled.value
      }))
      useGqlQuery(videoReviewsQuery, {}, options)
      onMounted(() => {
        // instanceof vue component
        const buttonEl = refs.button
        if (!('$el' in buttonEl)) {
          return
        }
        if (!buttonEl.$el) {
          return
        }
        buttonEl.$el.addEventListener(
          'mouseover',
          () => {
            enabled.value = true
          },
          {
            once: true
          }
        )
      })
      return () => (
        <div class={classes.main}>
          <BaseButton
            ref="button"
            onClick={openVideoReviewsModal}
            size="m"
            class={classes.button}
          >
            <div class={classes.blink}>
              <div class={classes.blinkInner}></div>
            </div>
            <div class={classes.inner}>
              <span>Посмотреть отзывы</span>
              <BaseIcon name="play" width={18} class={classes.play} />
              <img src={img} class={classes.image} alt="" />
            </div>
          </BaseButton>
        </div>
      )
    }
  })
)
