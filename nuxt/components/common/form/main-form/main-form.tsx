import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseForm, BaseFormDefaultSlot } from '~/components/base/base-form'
import { BaseLoadingButton } from '~/components/base'
import { BaseButtonProps } from '~/components/base/base-button/props'
import { PhoneLink } from '~/components/action-providers/phone-link'
import { BasePolicyLink } from '~/components/base/base-policy-link/base-policy-link'
import style from './main-form.scss?module'

const cn = useClassNames('main-form', style)

const classes = {
  main: cn(),
  field: cn('field'),
  policy: cn('policy'),
  button: cn('button')
}

export const MainForm = injectStyles(
  style,
  defineComponent({
    name: 'MainForm',
    props: {
      buttonOptions: {
        type: Object as () => BaseButtonProps,
        required: true
      },
      buttonClass: {
        type: String,
        default: ''
      },
      formOptions: {
        type: Object,
        required: true
      }
    },
    setup: (props, { slots }) => {
      const mainSlot = ({ loading }: BaseFormDefaultSlot) => (
        <div>
          {/* <div class={classes.policy}> */}
          {/*  Согласен с политикой конфиденциальности */}
          {/* </div> */}
          <BasePolicyLink class={classes.policy} />
          <BaseLoadingButton
            loading={loading}
            buttonOptions={{ type: 'submit', ...props.buttonOptions }}
            class={[props.buttonClass, classes.button]}
          >
            {slots.button && slots.button()}
          </BaseLoadingButton>
        </div>
      )
      return () => (
        <BaseForm
          fields={props.formOptions.fields}
          class={classes.main}
          fieldClass={classes.field}
          scopedSlots={{ default: mainSlot }}
        >
          <template slot="success">
            {slots.success
              ? slots.success()
              : 'Спасибо, за заявку. В ближайшее время наши менеджеры с Вами свяжутся!'}
          </template>
          <template slot="error">
            К сожалению произошла ошибка при отправке формы, но вы можете{' '}
            <PhoneLink>нам позвонить</PhoneLink>
          </template>
        </BaseForm>
      )
    }
  })
)
