import { computed, defineComponent, inject } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon, BaseRubles } from '~/components/base'
import { cartSymbol } from '~/data/providers/cart/symbol'
import { BaseLink } from '~/components/base/base-link'
import style from './common-cart-button.scss?module'

const cn = useClassNames('common-cart-button', style)

export const CommonCartButton = injectStyles(
  style,
  defineComponent({
    name: 'CommonCartButton',
    props: {},
    setup: () => {
      const cart = inject(cartSymbol)
      const total = computed(() => cart?.total.value || 0)
      const isActive = computed(() => total.value > 0)
      return () => (
        <client-only>
          <BaseLink to="/checkout" class={cn({ disabled: !isActive.value })}>
            <BaseIcon
              name="cart/wheelbarrow-filled"
              width={67}
              class={cn('icon')}
            />
            <span class={cn('text')}>
              {isActive.value ? <BaseRubles price={total.value} /> : 'корзина'}
            </span>
          </BaseLink>
        </client-only>
      )
    }
  })
)
