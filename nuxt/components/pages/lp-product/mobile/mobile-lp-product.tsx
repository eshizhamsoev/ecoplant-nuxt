import { computed, defineComponent, toRef } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton, BaseDashedButton } from '~/components/base'
import { PhoneLink } from '~/components/action-providers/phone-link'
import { MobileImageSlider } from '~/components/blocks/shared-components/mobile-image-slider'
import { ProductPrices } from '~/components/blocks/shared-components/product-prices'
import { MobileSaleForm } from '~/components/blocks/shared-components/product-sale/mobile/mobile-sale-form'
import { MobileFooterBottom } from '~/components/blocks/shared-components/footer-bottom/mobile'
import { EmailLink } from '~/components/action-providers/email-link'
import { ProductIdConstructor } from '~/common-types/catalog'
import { getProductById } from '~/queries/graphql/get-product-by-id'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './mobile-lp-product.scss?module'

const cn = useClassNames('mobile-lp-product', style)

const classes = {
  main: cn(),
  top: cn('top'),
  email: cn('email'),
  callback: cn('callback'),
  phone: cn('phone'),
  heading: cn('heading'),
  leading: cn('leading'),
  thirdLine: cn('third-line'),
  images: cn('images'),
  buttonWrapper: cn('button-wrapper'),
  prices: cn('prices'),
  form: cn('form'),
  footer: cn('footer')
}

export const MobileLpProduct = injectStyles(
  style,
  defineComponent({
    name: 'MobileLpProduct',
    props: {
      productId: {
        type: ProductIdConstructor,
        required: true
      }
    },
    setup: (props) => {
      const productId = toRef(props, 'productId')

      const { product } = getProductById(productId)

      const modalOpener = useModalOpener()
      const openModal = () =>
        modalOpener({
          name: 'callback',
          source: product.value ? product.value.name : undefined
        })
      const openProductDescription = () =>
        modalOpener({
          name: 'product-description',
          productId: productId.value
        })
      const name = computed(() => (product.value ? product.value.name : ''))
      return () => (
        <div class={classes.main}>
          <div class={classes.top}>
            <EmailLink class={classes.email} />
            <BaseDashedButton
              onClick={openModal}
              color="black"
              size="s"
              class={classes.callback}
            >
              Заказать звонок
            </BaseDashedButton>
            <PhoneLink class={classes.phone} />
          </div>
          <div class={classes.heading}>{name.value}</div>
          <div class={classes.leading}>Напрямую из питомника</div>
          <div class={classes.thirdLine}>без посредников и без переплат</div>
          {product.value && (
            <MobileImageSlider
              images={product.value.images}
              class={classes.images}
            />
          )}
          <div class={classes.buttonWrapper}>
            <BaseButton size="m" onClick={openProductDescription}>
              Открыть описание
            </BaseButton>
          </div>
          {product.value && (
            <ProductPrices product={product.value} class={classes.prices} />
          )}
          <MobileSaleForm source={name.value} class={classes.form} />
          <MobileFooterBottom withSocials={false} class={classes.footer} />
        </div>
      )
    }
  })
)
