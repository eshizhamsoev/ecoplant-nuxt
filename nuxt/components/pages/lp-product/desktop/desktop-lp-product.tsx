import { defineComponent, toRef } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ProductInfo } from '~/components/blocks/block-category/products-panel/product-info'
import { ProductSale } from '~/components/blocks/shared-components/product-sale'
import { FooterBottom } from '~/components/blocks/block-footer/footer-bottom'
import { DesktopBlockHeadingOneLine } from '~/components/blocks/block-heading-one-line/desktop'
import { ProductIdConstructor } from '~/common-types/catalog'
import { getProductById } from '~/queries/graphql/get-product-by-id'
import style from './desktop-lp-product.scss?module'

const cn = useClassNames('desktop-lp-product', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('leading'),
  productInfo: cn('product-info'),
  productSale: cn('product-sale'),
  footer: cn('footer')
}

export const DesktopLpProduct = injectStyles(
  style,
  defineComponent({
    name: 'DesktopLpProduct',
    props: {
      productId: {
        type: ProductIdConstructor,
        required: true
      }
    },
    setup: (props) => {
      const productId = toRef(props, 'productId')

      const { product } = getProductById(productId)

      return () => (
        <div>
          <DesktopBlockHeadingOneLine />
          <div class={classes.heading}>
            {product.value && product.value.name} напрямую из питомника
          </div>
          <div class={classes.leading}>
            без посредников и без переплаты для осенней посадки
          </div>
          {product.value && [
            <ProductInfo product={product.value} class={classes.productInfo} />,
            <ProductSale
              name={product.value.name}
              image={product.value.image}
              size={product.value.salePrice?.size}
              price={
                (product.value.showSalePrice && product.value.salePrice) || null
              }
              class={classes.productSale}
            />
          ]}
          <div class={classes.footer}>
            <FooterBottom withSocials={false} />
          </div>
        </div>
      )
    }
  })
)
