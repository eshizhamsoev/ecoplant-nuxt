import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BlockHowToWater } from '~/components/blocks/block-how-to-water/block-how-to-water'
import style from './how-to-water-content.scss?module'

const cn = useClassNames('how-to-water-content', style)

const classes = {
  main: cn(),
  howToWater: cn('how-to-water')
}

export const HowToWaterContent = injectStyles(
  style,
  defineComponent({
    name: 'HowToWaterContent',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <BlockHowToWater class={classes.howToWater} />
        </div>
      )
    }
  })
)
