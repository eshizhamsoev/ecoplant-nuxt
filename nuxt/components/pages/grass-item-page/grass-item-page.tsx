import { h } from '@vue/composition-api'
import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseBackButton } from '~/components/base'
import { GrassProduct } from '~/store/grass'
import { GrassItemContentFactory } from '~/components/content/grass-item-content'
import style from './grass-item-page.scss?module'

const cn = useClassNames('grass-item-page', style)

const classes = {
  back: cn('back')
}

export const GrassItemPage = injectStyles(
  style,
  defineComponent({
    name: 'GrassItemPage',
    props: {
      product: {
        type: Object as () => GrassProduct,
        required: true
      }
    },
    setup: (props, { root }) => {
      const goBack = () => {
        root.$router.push('/grass')
      }
      const content = GrassItemContentFactory(root)
      return () => (
        <div>
          {root.$device.isMobileOrTablet && (
            <BaseBackButton onClick={goBack} class={classes.back} />
          )}
          {h(content, { props })}
        </div>
      )
    }
  })
)
