import {
  defineComponent,
  toRef,
  useMeta,
  useContext,
  computed
} from '@nuxtjs/composition-api'
import { injectStyles } from '~/support/utils/injectStyle'
import { useClassNames } from '~/support/utils/bem-classnames'
import { ProductsGridItemProduct } from '~/components/blocks/shared-components/products-grid-item/product/products-grid-item-product'
import { getCategoryBreadcrumb } from '~/data/breadcrumbs'
import { HeadingWithBreadcrumbs } from '~/components/layouts/_partitial/heading-with-breadcrumbs'
import { CategoryIdConstructor } from '~/common-types/catalog'
import { getCategoryById } from '~/queries/graphql/get-category-by-id'
import { BlockCategories } from '~/components/blocks/block-categories'
import style from './category-page-seo.scss?module'

const cn = useClassNames('category-page-seo', style)
export const CategoryPageSeo = injectStyles(
  style,
  defineComponent({
    name: 'CategoryPageSeo',
    props: {
      categoryId: {
        type: CategoryIdConstructor,
        required: true
      }
    },
    setup: (props) => {
      const meta = useMeta({})
      const categoryId = toRef(props, 'categoryId')
      const { category, onResult } = getCategoryById(categoryId, true)
      const { error } = useContext()

      onResult(() => {
        if (!category.value) {
          error({ statusCode: 404, message: 'Not Found' })
        }
        if (
          category.value?.__typename === 'ProductCategory' &&
          category.value.meta
        ) {
          meta.title.value = category.value.meta.title
          if (category.value.meta.description) {
            meta.meta.value = [
              {
                hid: 'description',
                name: 'description',
                content: category.value.meta.description
              }
            ]
          }
        }
      })
      const h1 = computed(() => {
        if (!category.value) {
          return ''
        }
        return category.value.__typename === 'ProductCategory'
          ? category.value.meta?.h1 || category.value.name
          : category.value.name
      })

      const items = computed(() => {
        if (!category.value) {
          return []
        }
        if (category.value.__typename === 'ProductCategory') {
          return category.value?.products.map((product) => ({
            id: product.id,
            name: product.name,
            image: product.image,
            link: `/products/${product.id}`
          }))
        } else if (category.value.__typename === 'ServiceCategory') {
          return category.value?.services.map((service) => ({
            id: service.id,
            name: service.name,
            image: service.images[0].large,
            link: `/services/${service.id}`
          }))
        }
        return []
      })

      const description = computed(
        () =>
          category.value?.__typename === 'ProductCategory' &&
          category.value.description
      )
      const breadcrumbs = getCategoryBreadcrumb(category)
      return () =>
        category.value && (
          <div>
            <HeadingWithBreadcrumbs
              heading={h1.value}
              breadcrumbs={breadcrumbs.value}
            />
            <div class={cn('products')}>
              {items.value.map((item) => (
                <ProductsGridItemProduct item={item} key={item.link} />
              ))}
            </div>
            {description.value && (
              <div
                class={cn('description')}
                domProps={{ innerHTML: description.value }}
              />
            )}
            <BlockCategories />
          </div>
        )
    },
    head: {}
  })
)
