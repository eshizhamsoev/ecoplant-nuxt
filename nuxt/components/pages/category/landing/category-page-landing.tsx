import { h } from '@vue/composition-api'
import { defineComponent } from '@nuxtjs/composition-api'
import { CategoryIdConstructor } from '~/common-types/catalog'
import { SERVICE_CATEGORY_ID } from '~/data/const/servicesCategory'
import { EXCLUSIVE_CATEGORY_ID } from '~/data/const/exclusivesCategory'

export const CategoryPageLanding = defineComponent({
  name: 'CategoryPageLanding',
  props: {
    categoryId: {
      type: CategoryIdConstructor,
      required: true
    }
  },
  setup: (props, { root }) => {
    if (root.$device.isMobileOrTablet) {
      const MobileBlockCatalog = () =>
        import('~/components/blocks/block-catalog/mobile').then(
          (c) => c.MobileBlockCatalog
        )
      return () =>
        h(MobileBlockCatalog, {
          props: {
            currentCategoryId: props.categoryId,
            categoryIds: [
              '2',
              '3',
              '1',
              '4',
              '5',
              EXCLUSIVE_CATEGORY_ID,
              SERVICE_CATEGORY_ID
            ]
          },
          domProps: {
            id: 'category-component'
          }
        })
    } else {
      const BlockCategory = () =>
        import('~/components/blocks/block-category').then(
          (c) => c.BlockCategory
        )
      return () => (
        <div>
          <div id="category-component" />
          {h(BlockCategory, { props: { categoryId: props.categoryId } })}
        </div>
      )
    }
  }
})
