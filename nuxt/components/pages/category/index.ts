import type { AsyncComponent } from 'vue'
import { CategoryPageType } from '~/data/const/page-types/category'
export function getComponent(type: CategoryPageType): AsyncComponent {
  switch (type) {
    case CategoryPageType.SEO:
      return () =>
        import('./seo/category-page-seo').then((c) => c.CategoryPageSeo)
    case CategoryPageType.LANDING:
      return () =>
        import('./landing/category-page-landing').then(
          (c) => c.CategoryPageLanding
        )
  }
}
