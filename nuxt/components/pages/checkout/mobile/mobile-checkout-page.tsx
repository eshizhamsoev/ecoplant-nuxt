import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobileCheckoutContent } from '~/components/content/checkout-content/mobile'
import { BaseBackButton } from '~/components/base'
import style from './mobile-checkout-page.scss?module'

const cn = useClassNames('mobile-checkout-page', style)

const classes = {
  main: cn(),
  back: cn('back')
}

export const MobileCheckoutPage = injectStyles(
  style,
  defineComponent({
    name: 'MobileCheckoutPage',
    props: {},
    setup: (props, { root }) => {
      return () => (
        <div class={classes.main}>
          <BaseBackButton
            class={classes.back}
            onClick={() => root.$router.back()}
          />
          <MobileCheckoutContent />
        </div>
      )
    }
  })
)
