import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { DesktopBlockHeadingOneLine } from '~/components/blocks/block-heading-one-line/desktop'
import { DesktopBlockFooter } from '~/components/blocks/block-footer/desktop-block-footer'
import { DesktopCheckoutContent } from '~/components/content/checkout-content/desktop'
import style from './desktop-checkout-page.scss?module'

const cn = useClassNames('desktop-checkout-page', style)

const classes = {
  main: cn()
}

export const DesktopCheckoutPage = injectStyles(
  style,
  defineComponent({
    name: 'DesktopCheckoutPage',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <DesktopBlockHeadingOneLine />
          <DesktopCheckoutContent />
          <DesktopBlockFooter />
        </div>
      )
    }
  })
)
