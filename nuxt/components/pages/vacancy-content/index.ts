import type { ComponentInstance } from '@nuxtjs/composition-api'

export const VacancyContentFactory = (root: ComponentInstance) =>
  root.$device.isMobileOrTablet
    ? () => import('./mobile').then((c) => c.MobileVacancyContent)
    : () => import('./mobile').then((c) => c.MobileVacancyContent)
