import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './mobile-vacancy-promo.scss?module'

const cn = useClassNames('mobile-vacancy-promo', style)

const classes = {
  main: [cn()],
  background: cn('background'),
  heading: cn('heading'),
  leading: cn('leading')
}

export const MobileVacancyPromo = injectStyles(
  style,
  defineComponent({
    name: 'MobileVacancyPromo',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <div class={classes.background}>eco plant</div>
          <div class={classes.heading}>
            Компания неоднократно являлась победителем различных конкурсов
          </div>
          <div class={classes.leading}>
            по озеленению парков и зон отдыха г. Москвы, награждена многими
            грамотами и дипломами.
          </div>
        </div>
      )
    }
  })
)
