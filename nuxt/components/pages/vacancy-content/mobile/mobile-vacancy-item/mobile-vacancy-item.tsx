import { defineComponent } from '@nuxtjs/composition-api'
import person from '../resources/person.svg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon, BaseRubles } from '~/components/base'
import { cnThemeButton, Color, Visual } from '~/components/theme/theme-button'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './mobile-vacancy-item.scss?module'

const cn = useClassNames('mobile-vacancy-item', style)

const classes = {
  main: cn(),
  topLine: cn('top-line'),
  image: cn('image'),
  heading: cn('heading'),
  leading: cn('leading'),
  items: cn('items'),
  item: cn('item'),
  price: cn('price'),
  priceNumber: cn('price-number'),
  button: [
    cn('button'),
    cnThemeButton({ visual: Visual.outline, color: Color.green })
  ],
  buttonWrapper: cn('button-wrapper'),
  buttonIcon: cn('button-icon')
}

export const MobileVacancyItem = injectStyles(
  style,
  defineComponent({
    name: 'MobileVacancyItem',
    props: {
      heading: {
        type: String,
        required: true
      },
      leading: {
        type: String,
        required: true
      },
      items: {
        type: Array as () => string[],
        required: true
      },
      priceFrom: {
        type: Number,
        required: true
      },
      priceTo: {
        type: Number,
        required: true
      }
    },
    setup: (props) => {
      const modalOpener = useModalOpener()
      const openModal = () => {
        modalOpener({ name: 'vacancy', source: props.heading })
      }
      return () => (
        <div class={classes.main}>
          <div class={classes.topLine}>
            <img src={person} class={classes.image} alt="" />
            <div>
              <div class={classes.heading}>{props.heading}</div>
              <div class={classes.leading}>{props.leading}</div>
            </div>
          </div>
          <ul class={classes.items}>
            {props.items.map((item) => (
              <li class={classes.item}>{item}</li>
            ))}
          </ul>
          <div class={classes.price}>
            <span>от</span>{' '}
            <BaseRubles
              price={props.priceFrom}
              postfix={''}
              class={classes.priceNumber}
            />{' '}
            <span>до</span>{' '}
            <BaseRubles
              price={props.priceTo}
              postfix={''}
              class={classes.priceNumber}
            />{' '}
            <span>руб. на руки</span>
          </div>
          <div class={classes.buttonWrapper}>
            <button class={classes.button} onClick={openModal}>
              <span>Откликнуться</span>
              <BaseIcon
                name="arrow-left"
                width={16}
                rotate={180}
                class={classes.buttonIcon}
              />
            </button>
          </div>
        </div>
      )
    }
  })
)
