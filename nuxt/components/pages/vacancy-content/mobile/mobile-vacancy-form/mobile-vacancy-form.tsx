import { defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base/base-icon'
import { BaseFormResponse } from '~/components/base/base-form-response/base-form-response'
import { FORM_TYPES, useForm } from '~/support/hooks/useForm'
import { GTM_EVENTS } from '~/support/hooks/useReachGoal'
import { BaseLoadingButton } from '~/components/base/base-loading-button/base-loading-button'
import { Color } from '~/components/theme/theme-button'
import { BaseLink } from '~/components/base/base-link'
import style from './mobile-vacancy-form.scss?module'

const cn = useClassNames('mobile-vacancy-form', style)

const classes = {
  main: cn(),
  fieldWrapper: cn('field-wrapper'),
  field: cn('field'),
  fieldIcon: cn('field-icon'),
  privacy: cn('privacy'),
  button: cn('button'),
  successForm: cn('success-form'),
  errorForm: cn('error-form')
}

export const MobileVacancyForm = injectStyles(
  style,
  defineComponent({
    name: 'MobileVacancyForm',
    props: {
      source: {
        type: String,
        default: 'Не нашли вакансию,﻿которая подходит вам?'
      }
    },
    setup: (props, { root }) => {
      const name = ref('')
      const phone = ref('')
      const { processing, successMessage, fatalError, submit, errorFields } =
        useForm(root, FORM_TYPES.VACANCY, { goal: GTM_EVENTS.vacancySend })

      const onClick = () => {
        submit({
          phone: phone.value,
          name: name.value,
          source: props.source
        })
      }

      function onPhoneInput(event: InputEvent & { target?: HTMLInputElement }) {
        phone.value = event.target?.value
      }

      function onNameInput(event: InputEvent & { target?: HTMLInputElement }) {
        name.value = event.target?.value
      }

      return () => (
        <BaseFormResponse
          class={classes.main}
          fatalError={fatalError.value}
          successMessage={successMessage.value}
          successClass={classes.successForm}
          errorClass={classes.errorForm}
        >
          <div class={classes.fieldWrapper}>
            <label
              class={cn('field', {
                error: Boolean(
                  errorFields.value && errorFields.value.includes('name')
                )
              })}
            >
              <BaseIcon name="user" width={16} class={classes.fieldIcon} />
              <span>
                <input
                  placeholder="Ваше имя"
                  value={name.value}
                  onInput={onNameInput}
                  error={
                    errorFields.value && errorFields.value.includes('name')
                  }
                />
              </span>
            </label>
          </div>
          <div class={classes.fieldWrapper}>
            <label
              class={cn('field', {
                error: Boolean(
                  errorFields.value && errorFields.value.includes('phone')
                )
              })}
            >
              <BaseIcon name="phone" width={13} class={classes.fieldIcon} />
              <span>
                <input
                  placeholder="Номер телефона"
                  value={phone.value}
                  onInput={onPhoneInput}
                />
              </span>
            </label>
          </div>
          <div class={classes.fieldWrapper}>
            <BaseLoadingButton
              loading={processing.value}
              buttonOptions={{ color: Color.red, shadow: true, size: 'm' }}
              class={classes.button}
              onClick={onClick}
            >
              Отправить резюме
            </BaseLoadingButton>
          </div>
          <div class={classes.privacy}>
            Нажимая на кнопку, вы соглашаетесь с условиями{' '}
            <BaseLink to={'/privacy'}>обработки данных</BaseLink>
          </div>
        </BaseFormResponse>
      )
    }
  })
)
