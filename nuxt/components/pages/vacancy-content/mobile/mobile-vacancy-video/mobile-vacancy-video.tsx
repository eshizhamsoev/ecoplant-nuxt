import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon, BaseRoundButton } from '~/components/base'
import { Visual } from '~/components/theme/theme-button'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './mobile-vacancy-video.scss?module'

const cn = useClassNames('mobile-vacancy-video', style)

const classes = {
  main: cn(),
  topHeadingText: cn('top-heading-text'),
  heading: cn('heading'),
  leading: cn('leading'),
  imageText: cn('image-text'),
  imageTextLine: cn('image-text-line'),
  videoButtonWrapper: cn('video-button-wrapper'),
  videoButton: cn('video-button')
}

export const MobileVacancyVideo = injectStyles(
  style,
  defineComponent({
    name: 'MobileVacancyVideo',
    props: {},
    setup: () => {
      const modalOpener = useModalOpener()
      const onClick = () => {
        modalOpener({ name: 'vacancy-video', code: 'Nz0wsLJEhcE' })
      }
      return () => (
        <div class={classes.main}>
          <div>
            <span class={classes.topHeadingText}>Вакансии</span>
          </div>
          <div class={classes.heading}>
            Питомник растений «EcoPlant» — <b>лидер рынка</b> по
            оптово-розничным продажам декоративных растений
          </div>
          <div class={classes.videoButtonWrapper} onClick={onClick}>
            <BaseRoundButton
              size={76}
              shadow={true}
              visual={Visual.gradient}
              class={classes.videoButton}
            >
              <BaseIcon name="play" width={23} />
            </BaseRoundButton>
          </div>
          <div class={classes.leading}>
            О нашей компании вы можете узнать из видеоролика
          </div>
          <div class={classes.imageText}>
            <div class={classes.imageTextLine}>
              <span>Площадь площадок</span>
            </div>
            <div class={classes.imageTextLine}>
              <span>в Москве и в Санкт-Петербурге</span>
            </div>
            <div class={classes.imageTextLine}>
              <span>
                более <b>37000 м2</b>
              </span>
            </div>
          </div>
        </div>
      )
    }
  })
)
