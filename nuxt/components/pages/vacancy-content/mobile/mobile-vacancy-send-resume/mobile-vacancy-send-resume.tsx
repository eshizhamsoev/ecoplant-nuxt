import { defineComponent } from '@nuxtjs/composition-api'
import formImage from '../resources/form-image.jpg'
// @ts-ignore-next-line
import formImageWebp from '../resources/form-image.jpg?webp'
import { MobileVacancyForm } from '../mobile-vacancy-form'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseWebpPicture } from '~/components/base/base-webp-picture'
import { cnThemeWrapper } from '~/components/theme/theme-wrapper'
import style from './mobile-vacancy-send-resume.scss?module'

const cn = useClassNames('mobile-vacancy-send-resume', style)

const classes = {
  main: [cn(), cnThemeWrapper()],
  heading: cn('heading'),
  leading: cn('leading'),
  image: cn('image'),
  formWrapper: cn('form-wrapper'),
  formHeading: cn('form-heading'),
  formInner: cn('form-inner')
}

export const MobileVacancySendResume = injectStyles(
  style,
  defineComponent({
    name: 'MobileVacancySendResume',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>
            Не нашли вакансию, которая подходит вам?
          </div>
          <div class={classes.leading}>
            Прикрепите свое
            <br />
            резюме и наш отдел
            <br />
            найма свяжется с вами
          </div>
          <BaseWebpPicture
            image={formImage}
            webpImage={formImageWebp}
            class={classes.image}
          />
          <div class={classes.formWrapper}>
            <div class={classes.formHeading}>Прикрепить резюме</div>
            <MobileVacancyForm class={classes.formInner} />
          </div>
        </div>
      )
    }
  })
)
