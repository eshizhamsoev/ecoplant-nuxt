import { defineComponent } from '@nuxtjs/composition-api'
import { MobileVacancyVideo } from './mobile-vacancy-video'
import { MobileVacancyPromo } from './mobile-vacancy-promo'
import { MobileVacancyItem } from './mobile-vacancy-item'
import { MobileVacancySendResume } from './mobile-vacancy-send-resume'
import { injectStyles } from '~/support/utils/injectStyle'
import { useClassNames } from '~/support/utils/bem-classnames'
import { MobileFooterBottom } from '~/components/blocks/shared-components/footer-bottom/mobile'
import { MobileTinyHeading } from '~/components/layouts/tiny/tiny-heading/mobile'
import { FORM_TYPES } from '~/support/hooks/useForm'
import { GTM_EVENTS } from '~/support/hooks/useReachGoal'
import style from './mobile-vacancy-content.scss?module'

const cn = useClassNames('mobile-block-vacancy', style)

const classes = {
  main: cn(),
  topBlocks: cn('top-blocks'),
  heading: cn('heading'),
  video: cn('video'),
  promo: cn('promo'),
  about: cn('about'),
  items: cn('items'),
  item: cn('item'),
  form: cn('form'),
  footer: cn('footer')
}

export const MobileVacancyContent = injectStyles(
  style,
  defineComponent({
    name: 'MobileVacancyContent',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <div class={classes.topBlocks}>
            <MobileTinyHeading
              class={classes.heading}
              email="hr@eco-plant.ru"
              formType={FORM_TYPES.VACANCY}
              goal={GTM_EVENTS.vacancyCallBack}
            />
            <MobileVacancyVideo class={classes.video} />
          </div>
          <MobileVacancyPromo class={classes.promo} />
          <div class={classes.items}>
            <MobileVacancyItem
              heading="Менеджер по работе﻿с клиентами в питомник растений"
              leading={
                'Требуемый опыт работы: 1–3 года\nЗанятость: полный день'
              }
              class={classes.item}
              items={[
                'Обслуживание и консультация клиентов',
                'Продажа ассортимента товара',
                'Выполнение KPI и показателей',
                'Оформление документации к продаже товара',
                'Работа в СРМ на базе 1С-Управление',
                'Активное участие в акциях и рекламных кампаниях'
              ]}
              priceFrom={75000}
              priceTo={180000}
            />
            <MobileVacancyItem
              heading="Менеджер по работе﻿с клиентами в питомник растений"
              leading={
                'Требуемый опыт работы: 1–3 года\nЗанятость: полный день'
              }
              class={classes.item}
              items={[
                'Обслуживание и консультация клиентов',
                'Продажа ассортимента товара',
                'Выполнение KPI и показателей',
                'Оформление документации к продаже товара',
                'Работа в СРМ на базе 1С-Управление',
                'Активное участие в акциях и рекламных кампаниях'
              ]}
              priceFrom={75000}
              priceTo={180000}
            />
            <MobileVacancyItem
              heading="Менеджер по работе﻿с клиентами в питомник растений"
              leading={
                'Требуемый опыт работы: 1–3 года\nЗанятость: полный день'
              }
              class={classes.item}
              items={[
                'Обслуживание и консультация клиентов',
                'Продажа ассортимента товара',
                'Выполнение KPI и показателей',
                'Оформление документации к продаже товара',
                'Работа в СРМ на базе 1С-Управление',
                'Активное участие в акциях и рекламных кампаниях'
              ]}
              priceFrom={75000}
              priceTo={180000}
            />
          </div>
          <MobileVacancySendResume class={classes.form} />
          <MobileFooterBottom class={classes.footer} theme="dark" />
        </div>
      )
    }
  })
)
