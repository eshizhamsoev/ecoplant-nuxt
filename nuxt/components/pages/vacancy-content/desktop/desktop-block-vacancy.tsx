import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './desktop-block-vacancy.scss?module'

const cn = useClassNames('desktop-block-vacancy', style)

const classes = {
  main: cn()
}

export const DesktopBlockVacancy = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlockVacancy',
    props: {},
    setup: () => {
      return () => <div class={classes.main}>{/* TODO add content */}</div>
    }
  })
)
