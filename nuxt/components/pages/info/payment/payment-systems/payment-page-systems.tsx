import { defineComponent } from '@nuxtjs/composition-api'
import mir from './logos/mir.svg'
import visa from './logos/visa.svg'
import mastercard from './logos/mastercard.svg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './payment-page-systems.scss?module'

const cn = useClassNames('payment-page-systems', style)

export const PaymentPageSystems = injectStyles(
  style,
  defineComponent({
    name: 'PaymentPageSystems',
    props: {},
    setup: () => {
      return () => (
        <div class={cn()}>
          <div class={cn('item')}>
            <div class={cn('image-wrapper')}>
              <img src={mir} alt="МИР" class={cn('image')} />
            </div>
          </div>
          <div class={cn('item')}>
            <div class={cn('image-wrapper')}>
              <img src={visa} alt="Visa" class={cn('image')} />
            </div>
          </div>
          <div class={cn('item')}>
            <div class={cn('image-wrapper')}>
              <img src={mastercard} alt="mastercard" class={cn('image')} />
            </div>
          </div>
        </div>
      )
    }
  })
)
