import { defineComponent, onMounted, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseBackButton } from '~/components/base'
import { MobileSaleFormLine } from '~/components/blocks/shared-components/sale-form-line/mobile'
import { getSpecials } from '~/api/client/get-specials'
import { SpecialItem } from '~/common-types/server/collections/specials'
import { BlockMobileFixedLine } from '~/components/blocks/block-mobile-fixed-line'
import style from './mobile-sale-content.scss?module'

const cn = useClassNames('mobile-sale-content', style)

const classes = {
  main: cn(),
  goBack: cn('go-back'),
  items: cn('items'),
  buttons: cn('buttons'),
  image: cn('image'),
  form: cn('form'),
  formHeading: cn('form-heading')
}

export const MobileSaleContent = injectStyles(
  style,
  defineComponent({
    name: 'MobileSaleContent',
    props: {},
    setup: (props, { root }) => {
      const goBack = () => {
        root.$router.back()
      }
      const images = ref<SpecialItem[]>([])
      onMounted(() => {
        getSpecials().then((data) => {
          images.value = data.images
        })
      })
      return () => (
        <div class={classes.main}>
          <div class={classes.items}>
            <BaseBackButton onClick={goBack} class={classes.goBack} />
            {images.value.map((item) => (
              <img
                class={classes.image}
                src={item.image.src}
                alt=""
                width={item.image.width}
                height={item.image.height}
              />
            ))}
            {/* <div class={classes.buttons}> */}
            {/*  <MobileWhatsappButton class={cn('button', { type: 'left' })} /> */}
            {/*  <MobilePhoneButton class={cn('button', { type: 'right' })} /> */}
            {/* </div> */}
          </div>
          <div class={classes.formHeading}>
            Сохранить за собой скидку этой недели
          </div>
          <MobileSaleFormLine
            class={classes.form}
            source="Сохранить скидку недели"
          >
            <template slot="button">Сохранить скидку</template>
          </MobileSaleFormLine>
          <BlockMobileFixedLine />
        </div>
      )
    }
  })
)
