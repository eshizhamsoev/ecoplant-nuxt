import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import style from './mobile-grass-back-button.scss?module'

const cn = useClassNames('mobile-grass-back-button', style)

const classes = {
  main: cn(),
  icon: cn('icon')
}

export const MobileGrassBackButton = injectStyles(
  style,
  defineComponent({
    name: 'MobileGrassBackButton',
    props: {},
    setup(props, { emit }) {
      return () => (
        <button onClick={() => emit('click')} class={classes.main}>
          <BaseIcon
            name={'arrow-left'}
            width={16}
            height={10}
            class={classes.icon}
          />
          <span>Назад</span>
        </button>
      )
    }
  })
)
