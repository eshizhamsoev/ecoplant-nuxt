import { defineComponent, PropType } from '@nuxtjs/composition-api'
import { BaseBreadcrumbs } from '~/components/base/base-breadcrumbs'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import type { BreadcrumbItem } from '~/components/base/base-breadcrumbs'
import style from './heading-with-breadcrumbs.scss?module'

const cn = useClassNames('heading-with-breadcrumbs', style)

export const HeadingWithBreadcrumbs = injectStyles(
  style,
  defineComponent({
    name: 'HeadingWithBreadcrumbs',
    props: {
      breadcrumbs: {
        type: Array as PropType<BreadcrumbItem[] | null>,
        required: true
      },
      heading: {
        type: String,
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={cn()}>
          <h1 class={cn('heading')}>{props.heading}</h1>
          {Array.isArray(props.breadcrumbs) && (
            <BaseBreadcrumbs
              items={props.breadcrumbs}
              class={cn('breadcrumbs')}
            />
          )}
        </div>
      )
    }
  })
)
