import {
  defineComponent,
  provide,
  useContext,
  watch,
  onBeforeUnmount
} from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BlockMenu } from '~/components/blocks/block-menu/block-menu'
import { useMenu } from '~/data/providers/menu/useMenu'
import { menuSymbol } from '~/data/providers/menu/menuSymbol'
import { clearAllScrollLocks, lockScroll } from '~/support/utils/scroll-lock'
import style from './menu-provider.scss?module'

const cn = useClassNames('menu-provider', style)

export const MenuProvider = injectStyles(
  style,
  defineComponent({
    name: 'MenuProvider',
    props: {},
    setup: (props, { slots }) => {
      const { open, close, isOpen } = useMenu()
      const { route, $device } = useContext()

      provide(menuSymbol, {
        close,
        open,
        isOpen
      })

      watch(
        () => route,
        () => {
          close()
        }
      )

      watch(
        () => isOpen.value,
        (value) => {
          const el = document.querySelector('#menu') // ref doesn't work, you should understand why?!
          if (el && value) {
            lockScroll(el)
          } else if (!value) {
            clearAllScrollLocks()
          }
        }
      )

      onBeforeUnmount(() => {
        clearAllScrollLocks()
      })

      return () => (
        <div class="">
          {slots.default && slots.default()}
          <div
            id="menu"
            class={cn('container', {
              open: isOpen.value,
              mobile: $device.isMobileOrTablet
            })}
          >
            <BlockMenu class={cn('menu')} />
          </div>
          {!$device.isMobileOrTablet && (
            <div
              class={cn('menu-overlay', { open: isOpen.value })}
              onClick={close}
            />
          )}
        </div>
      )
    }
  })
)
