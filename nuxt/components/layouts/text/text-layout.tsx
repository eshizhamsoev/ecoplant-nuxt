import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BlockHeadingOneLine } from '~/components/blocks/block-heading-one-line'
import { BlockFooter } from '~/components/blocks/block-footer'
import { cnThemeWrapper } from '~/components/theme/theme-wrapper'
import DefaultLayout from '~/components/layouts/default'
import style from './text-layout.scss?module'

const cn = useClassNames('text-layout', style)

export const TextLayout = injectStyles(
  style,
  defineComponent({
    name: 'TextLayout',
    props: {},
    setup: (props, { slots }) => {
      return () => (
        <DefaultLayout class={cn()}>
          <BlockHeadingOneLine />
          <div class={[cn('content'), cnThemeWrapper()]}>
            {slots.default && slots.default()}
          </div>
          <BlockFooter />
        </DefaultLayout>
      )
    }
  })
)
