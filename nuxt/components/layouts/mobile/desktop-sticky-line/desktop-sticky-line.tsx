import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import {
  EmailLink,
  EmailSlotData
} from '~/components/action-providers/email-link'
import {
  PhoneLink,
  PhoneSlotData
} from '~/components/action-providers/phone-link'
import { BaseIcon } from '~/components/base'
import { useOnWhatsAppClick } from '~/support/hooks/useWhatsApp'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { HeadingSaleButton } from '~/components/blocks/block-heading/desktop/heading-sale-button'
import { SaleButtonSize } from '~/components/blocks/block-heading/desktop/heading-sale-button/heading-sale-button'
import { useOnTelegramClick } from '~/support/hooks/useTelegram'
import style from './desktop-sticky-line.scss?module'

const cn = useClassNames('desktop-sticky-line', style)

const emailSlot = ({ email }: EmailSlotData) => [
  <BaseIcon name="mail" width={14} class={cn('email-icon')} />,
  email
]
const phoneSlot = ({ phone }: PhoneSlotData) => [
  <BaseIcon name="phone" width={14} class={cn('phone-icon')} />,
  phone.text
]

export const DesktopStickyLine = injectStyles(
  style,
  defineComponent({
    name: 'DesktopStickyLine',
    props: {},
    setup: () => {
      const onWhatsAppClick = useOnWhatsAppClick()
      const onTelegramClick = useOnTelegramClick()

      const modalOpener = useModalOpener()
      const openModal = () => modalOpener({ name: 'callback' })

      const openDelivery = () => modalOpener({ name: 'shipping' })
      const openPlanting = () => modalOpener({ name: 'planting' })
      const openClients = () => modalOpener({ name: 'client-photos' })
      const openReviews = () => modalOpener({ name: 'video-reviews' })
      const openAwards = () => modalOpener({ name: 'awards' })

      return () => (
        <div class={cn()}>
          <div class={cn('container')}>
            <HeadingSaleButton size={SaleButtonSize.s} withClose={false} />
            <button
              onClick={onWhatsAppClick}
              class={cn('whatsapp')}
              target="_blank"
            >
              <BaseIcon
                name="whatsapp"
                width={16}
                class={cn('whatsapp-icon')}
              />
              WhatsApp
            </button>
            <button
              onClick={onTelegramClick}
              class={cn('telegram')}
              target="_blank"
            >
              <BaseIcon
                name="telegram"
                width={16}
                class={cn('telegram-icon')}
              />
              Telegram
            </button>
            <button class={cn('button')} onClick={openDelivery}>
              Доставка
            </button>
            <button class={cn('button')} onClick={openPlanting}>
              Посадка
            </button>
            <button class={cn('button')} onClick={openClients}>
              Клиенты
            </button>
            <button class={cn('button')} onClick={openReviews}>
              Отзывы
            </button>
            <button class={cn('button')} onClick={openAwards}>
              Награды
            </button>
            <div class={cn('contacts')}>
              <EmailLink
                scopedSlots={{ default: emailSlot }}
                class={cn('email')}
              />
              <PhoneLink
                scopedSlots={{ default: phoneSlot }}
                class={cn('phone')}
              />
            </div>
            <button class={cn('callback')} onClick={openModal}>
              заказать звонок
            </button>
          </div>
        </div>
      )
    }
  })
)
