import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { useOnWhatsAppClick } from '~/support/hooks/useWhatsApp'
import { useOnTelegramClick } from '~/support/hooks/useTelegram'
import { BaseMobileBottomLineButton } from '~/components/base/base-mobile-bottom-line-button/base-mobile-bottom-line-button'
import { PhoneLink } from '~/components/action-providers/phone-link'
import { BaseMenuButton } from '~/components/base/base-menu-button/base-menu-button'
import { BaseLink } from '~/components/base/base-link'
import { BaseRingingButton } from '~/components/base/base-ringing-button/base-ringing-button'
import style from './mobile-button-line.scss?module'

const cn = useClassNames('mobile-button-line', style)

export const MobileButtonLine = injectStyles(
  style,
  defineComponent({
    name: 'MobileButtonLine',
    props: {},
    setup: () => {
      const onWhatsAppClick = useOnWhatsAppClick()
      const onTelegramClick = useOnTelegramClick()

      return () => (
        <div class={cn()}>
          <button onClick={onWhatsAppClick} class={cn('button')}>
            <BaseMobileBottomLineButton icon={'whatsapp'} text={'WhatsApp'} />
          </button>
          <button onClick={onTelegramClick} class={cn('button')}>
            <BaseMobileBottomLineButton
              icon={'telegram-no-color'}
              text={'Telegram'}
            />
          </button>
          <BaseMenuButton />
          <PhoneLink class={cn('button')}>
            <BaseRingingButton icon={'phone'} text={'Позвонить'} />
          </PhoneLink>
          <BaseLink class={cn('button')} to={'/checkout'}>
            <BaseMobileBottomLineButton icon={'cart'} text={'Корзина'} />
          </BaseLink>
        </div>
      )
    }
  })
)
