import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './desktop-tiny-heading.scss?module'

const cn = useClassNames('desktop-tiny-heading', style)

const classes = {
  main: cn()
}

export const DesktopTinyHeading = injectStyles(
  style,
  defineComponent({
    name: 'DesktopTinyHeading',
    props: {},
    setup: () => {
      return () => <div class={classes.main}>{/* TODO add content */}</div>
    }
  })
)
