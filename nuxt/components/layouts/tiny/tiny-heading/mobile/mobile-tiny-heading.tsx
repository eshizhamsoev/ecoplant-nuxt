import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseDashedButton, BaseIcon } from '~/components/base'
import { PhoneLink } from '~/components/action-providers/phone-link'
import { cnThemeWrapper } from '~/components/theme/theme-wrapper'
import { FORM_TYPES } from '~/support/hooks/useForm'
import { GTM_EVENTS } from '~/support/hooks/useReachGoal'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { EmailLink } from '~/components/action-providers/email-link'
import { BaseLink } from '~/components/base/base-link'
import style from './mobile-tiny-heading.scss?module'

const cn = useClassNames('mobile-tiny-heading', style)

const classes = {
  main: [cn(), cnThemeWrapper()],
  logoLink: cn('logo-link'),
  logo: cn('logo'),
  slogan: cn('slogan'),
  contacts: cn('contacts'),
  email: cn('email'),
  phone: cn('phone'),
  callback: cn('callback')
}

export const MobileTinyHeading = injectStyles(
  style,
  defineComponent({
    name: 'MobileTinyHeading',
    props: {
      email: {
        type: String,
        default: ''
      },
      formType: {
        type: Number as () => FORM_TYPES,
        default: FORM_TYPES.PHONE
      },
      goal: {
        type: String as () => GTM_EVENTS,
        default: GTM_EVENTS.callBack
      },
      source: {
        type: String,
        default: 'Заказать звонок в шапке'
      }
    },
    setup: (props, { root }) => {
      const siteName = computed(() => root.$accessor.contacts.siteName)

      const modalOpener = useModalOpener()
      const openModal = () =>
        modalOpener({
          name: 'callback',
          source: props.source,
          formType: props.formType,
          goal: props.goal
        })

      return () => (
        <div class={classes.main}>
          <BaseLink to="/" class={classes.logoLink}>
            <BaseIcon name="logo" width={60} class={classes.logo} />
          </BaseLink>
          <div class={classes.slogan}>{siteName.value}</div>
          <div class={classes.contacts}>
            <EmailLink class={classes.email} />
            <PhoneLink class={classes.phone} />
            <BaseDashedButton
              class={classes.callback}
              size="s"
              color="black"
              onClick={openModal}
            >
              Заказать звонок
            </BaseDashedButton>
          </div>
        </div>
      )
    }
  })
)
