import { h } from '@vue/composition-api'
import { defineComponent, onMounted } from '@nuxtjs/composition-api'
import fontsStyles from '~/assets/fonts/acrom/stylesheet.css'
import commonStyles from '~/assets/common.css'
import '~/components/theme/theme-wrapper'
import '~/components/theme/theme-button'
import '~/components/base'
import { CartProvider } from '~/data/providers/cart/cart-provider'
import { ApolloProvider } from '~/data/providers/apollo/apollo-provider'
import { saveUtm } from '~/support/utils/utmData'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { MenuProvider } from '~/components/layouts/_partitial/menu-provider/menu-provider'

fontsStyles.__inject__ && fontsStyles.__inject__()
commonStyles.__inject__ && commonStyles.__inject__()

export default defineComponent({
  name: 'DefaultLayout',
  setup: (props, { slots, root }) => {
    const modalOpener = useModalOpener()

    onMounted(() => {
      document.documentElement.style.setProperty(
        '--scroll-bar-size',
        `${window.innerWidth - document.documentElement.clientWidth}px`
      )

      // initiateCrm(root.$accessor.rocketCrmId)
      if (root.$route.hash) {
        const tag = document.querySelector(root.$route.hash)
        if (tag) {
          tag.scrollIntoView()
        } else if (root.$route.hash === '#modal-water') {
          modalOpener({ name: 'how-to-water' })
        } else if (root.$route.hash === '#modal-planting') {
          modalOpener({ name: 'planting-videos' })
        }
      }

      saveUtm(window.location)
    })

    return () => (
      <ApolloProvider>
        <CartProvider>
          <MenuProvider>
            {h('div', [
              slots.default && slots.default(),
              h('modals-container')
            ])}
          </MenuProvider>
        </CartProvider>
      </ApolloProvider>
    )
  }
})
