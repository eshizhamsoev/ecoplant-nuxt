import type { AsyncComponent } from 'vue'
import { h } from '@vue/composition-api'
import { defineComponent } from '@nuxtjs/composition-api'

export const UtilUniversalComponentFactory = (
  name: string,
  mobileComponent: AsyncComponent,
  desktopComponent: AsyncComponent
) =>
  defineComponent({
    name,
    setup(props, ctx) {
      return () =>
        // @ts-ignore
        h(
          ctx.root.$device.isMobileOrTablet
            ? mobileComponent
            : desktopComponent,
          {
            props: ctx.attrs,
            on: ctx.listeners,
            scopedSlots: ctx.slots,
            attrs: ctx.attrs,
            class: ctx.attrs.class
          }
        )
    }
  })
