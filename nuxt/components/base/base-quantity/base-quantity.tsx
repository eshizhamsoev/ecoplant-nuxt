import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { BaseIntegerInput } from '~/components/base/base-integer-input'
import {
  QuantityProvider,
  QuantityProviderSlotData
} from '~/components/action-providers/quantity-provider/quantity-provider'
import style from './base-quantity.scss?module'

const cn = useClassNames('base-quantity', style)

const classes = {
  main: cn(),
  plus: cn('plus'),
  minus: cn('minus'),
  input: cn('input')
}

export const BaseQuantity = injectStyles(
  style,
  defineComponent({
    name: 'BaseQuantity',
    props: {
      value: {
        type: Number,
        required: true
      },
      minimum: {
        type: Number,
        default: -Infinity
      },
      maximum: {
        type: Number,
        default: Infinity
      }
    },
    setup: (props, { emit }) => {
      const slot = ({
        minus,
        plus,
        value,
        changeValue
      }: QuantityProviderSlotData) => (
        <div>
          <button class={classes.minus} onClick={minus} type="button">
            <BaseIcon name="minus" width={20} />
          </button>
          <BaseIntegerInput
            class={classes.input}
            value={value}
            onChange={changeValue}
            min={0}
            max={99999}
          />
          <button class={classes.plus} onClick={plus} type="button">
            <BaseIcon name="plus" width={20} />
          </button>
        </div>
      )
      return () => (
        <QuantityProvider
          onInput={(value: number) => emit('input', value)}
          value={props.value}
          minimum={props.minimum}
          maximum={props.maximum}
          class={classes.main}
          scopedSlots={{ default: slot }}
        />
      )
    }
  })
)
