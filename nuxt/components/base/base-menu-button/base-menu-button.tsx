import { computed, defineComponent, inject } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { menuSymbol } from '~/data/providers/menu/menuSymbol'
import style from './base-menu-button.scss?module'

const cn = useClassNames('base-menu-button', style)

export const BaseMenuButton = injectStyles(
  style,
  defineComponent({
    name: 'BaseMenuButton',
    props: {},
    setup: () => {
      const menu = inject(menuSymbol)

      const menuState = computed(() => menu?.isOpen.value)

      const menuClick = () => {
        menuState.value ? closeMenu() : openMenu()
      }

      const openMenu = () => {
        menu?.open()
      }

      const closeMenu = () => {
        menu?.close()
      }

      return () => (
        <button onClick={menuClick} class={cn()}>
          <span class={cn('icon-wrapper')}>
            <div class={cn('burger', { active: menuState.value })}>
              <span class={cn('burger-box')}>
                <span class={cn('burger-inner')} />
              </span>
            </div>
          </span>
          <span class={cn('button-text')}>Меню</span>
        </button>
      )
    }
  })
)
