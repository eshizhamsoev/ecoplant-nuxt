import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseLink } from '~/components/base/base-link'
import style from './base-dashed-button.scss?module'

const cn = useClassNames('base-dashed-button', style)

export const BaseDashedButton = injectStyles(
  style,
  defineComponent({
    name: 'BaseDashedButton',
    props: {
      color: {
        type: String as () => 'white' | 'black',
        required: true
      },
      size: {
        type: String as () => 'xs' | 's' | 'l',
        required: true
      },
      to: {
        type: Object,
        default: null
      }
    },
    setup: (props, { slots, emit }) => {
      const mainClass = computed(() =>
        cn({ color: props.color, size: props.size })
      )
      return () =>
        props.to ? (
          <BaseLink class={mainClass.value} to={props.to}>
            {slots.default && slots.default()}
          </BaseLink>
        ) : (
          <button
            class={mainClass.value}
            type="button"
            onClick={() => emit('click')}
          >
            {slots.default && slots.default()}
          </button>
        )
    }
  })
)
