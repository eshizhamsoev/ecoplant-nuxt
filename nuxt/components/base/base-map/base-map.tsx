import { defineComponent, onMounted } from '@nuxtjs/composition-api'
import ymaps from 'ymaps'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './base-map.scss?module'

const cn = useClassNames('base-map', style)

const mainClass = cn()

export const BaseMap = injectStyles(
  style,
  defineComponent({
    name: 'BaseMap',
    props: {
      latitude: {
        type: Number,
        required: true
      },
      longitude: {
        type: Number,
        required: true
      },
      zoom: {
        type: Number,
        default: 13
      },
      address: {
        type: String,
        default: null
      }
    },
    setup: (props, { refs }) => {
      onMounted(() => {
        ymaps
          .load('https://api-maps.yandex.ru/2.1/?lang=ru_RU')
          .then((maps) => {
            const map = new maps.Map(refs.root, {
              center: [props.latitude, props.longitude],
              zoom: props.zoom
            })
            if (props.address) {
              const placemark = new maps.Placemark(
                map.getCenter(),
                {
                  balloonContent: props.address
                },
                { preset: 'islands#darkGreenVegetationIcon' }
              )
              map.geoObjects.add(placemark)
            }
            map.controls.remove('geolocationControl')
            map.controls.remove('searchControl')
            map.controls.remove('trafficControl')
            map.controls.remove('typeSelector')
            map.controls.remove('fullscreenControl')
            map.controls.remove('rulerControl')
            map.behaviors.disable('scrollZoom')
          })
      })
      return () => <div class={mainClass} ref="root"></div>
    }
  })
)
