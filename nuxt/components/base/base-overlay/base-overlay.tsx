import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './base-overlay.scss?module'

const cn = useClassNames('base-overlay', style)

export const BaseOverlay = injectStyles(
  style,
  defineComponent({
    name: 'BaseOverlay',
    setup: (props, { slots }) => {
      return () => (
        <div class={cn()} style={style.value}>
          {slots.default && slots.default()}
        </div>
      )
    }
  })
)
