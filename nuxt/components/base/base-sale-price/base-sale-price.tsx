import { computed, defineComponent } from '@nuxtjs/composition-api'
import { SalePriceAlign, SalePriceClasses, SalePriceData } from './types'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseRubles } from '~/components/base'
import style from './base-sale-price.scss?module'

const cn = useClassNames('base-sale-price', style)

const classes = {
  main: cn(),
  label: cn('label'),
  price: cn('price'),
  oldPrice: cn('old-price')
}

export const BaseSalePrice = injectStyles(
  style,
  defineComponent({
    name: 'BaseSalePrice',
    props: {
      priceData: {
        type: Object as () => SalePriceData,
        required: true
      },
      classes: {
        type: Object as () => SalePriceClasses,
        default: () => ({} as SalePriceClasses)
      },
      size: {
        type: String as () => 'm',
        default: 'm'
      },
      align: {
        type: String as () => SalePriceAlign,
        default: 'left'
      }
    },
    setup: (props) => {
      const alignStyle = computed(() => ({
        'align-self': props.align === 'left' ? 'flex-start' : 'flex-end'
      }))
      return () => (
        <div class={cn({ size: props.size })}>
          <div class={[classes.label, props.classes.label]}>новая цена</div>
          <BaseRubles
            class={[classes.price, props.classes.price]}
            price={props.priceData.current}
          />
          <BaseRubles
            class={[classes.oldPrice, props.classes.oldPrice]}
            price={props.priceData.old}
            style={alignStyle.value}
          />
        </div>
      )
    }
  })
)
