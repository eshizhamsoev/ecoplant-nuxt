export type SalePriceData = {
  current: number
  old: number
}

export type SalePriceClasses = {
  label?: string
  price?: string
  oldPrice?: string
}

export type SalePriceAlign = 'left' | 'right'
