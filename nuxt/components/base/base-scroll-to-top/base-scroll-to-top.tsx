import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import style from './base-scroll-to-top.scss?module'

const cn = useClassNames('base-scroll-to-top', style)

export const BaseScrollToTop = injectStyles(
  style,
  defineComponent({
    name: 'BaseScrollToTop',
    props: {},
    setup: () => {
      const scrollTop = () => {
        window.scrollTo({
          behavior: 'smooth',
          top: 0
        })
      }

      return () => (
        <button class={cn()} onClick={scrollTop}>
          <BaseIcon name="angle-down" width={13} height={8} rotate={180} />
          <span>Наверх</span>
        </button>
      )
    }
  })
)
