import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseClientGalleryControlBar } from '~/components/base/base-client-gallery-control-bar/base-client-gallery-control-bar'
import style from './base-client-image.scss?module'

const cn = useClassNames('base-client-image', style)

export const BaseClientImage = injectStyles(
  style,
  defineComponent({
    name: 'BaseClientImage',
    props: {
      image: {
        type: String as () => string | undefined,
        required: true
      },
      text: {
        type: String,
        default: ''
      },
      height: {
        type: Number,
        required: true
      },
      width: {
        type: Number,
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={cn()}>
          <img
            class={cn('image')}
            src={props.image}
            alt={props.text}
            loading={'lazy'}
            height={props.height}
            width={props.width}
          />
          <div class={cn('content')}>
            <BaseClientGalleryControlBar class={cn('control-bar')} />
            <div class={cn('text')}>{props.text}</div>
          </div>
        </div>
      )
    }
  })
)
