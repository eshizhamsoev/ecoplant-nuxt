import {
  computed,
  defineComponent,
  onUpdated,
  ref,
  watch
} from '@nuxtjs/composition-api'

export const BasePhoneInput = defineComponent({
  name: 'BasePhoneInput',
  inheritAttrs: false,
  setup: (props, ctx) => {
    const template = '+7 (___) ___ - __ - __'

    const number = ref('')

    watch(
      () => number.value,
      () => {
        ctx.emit('input', number.value)
      }
    )

    const phoneText = computed(() => {
      let value = ''
      let index = 0
      for (const char of template) {
        if (char === '_' && number.value[index]) {
          value += number.value[index]
          index++
        } else {
          value += char
        }
      }
      return value
    })

    const updateNumber = (event: InputEvent) => {
      const str = (event.target as HTMLInputElement).value.replace(
        /[^\d]/gi,
        ''
      )
      number.value = str.slice(1)
    }

    const caretPosition = computed(() => {
      const index = phoneText.value.indexOf('_')
      if (index === -1) {
        return phoneText.value.length
      }
      return index
    })

    const setCaret = () => {
      const input = ctx.refs.input as HTMLInputElement
      if (document.activeElement === input) {
        input.selectionStart = caretPosition.value
        input.selectionEnd = caretPosition.value
      }
    }

    onUpdated(() => {
      setCaret()
    })

    function deleteChar(startPosition: number | null) {
      if (startPosition === null || startPosition === caretPosition.value) {
        number.value = number.value.slice(0, -1)
      } else {
        const len = template.slice(0, startPosition).match(/_/g)?.length || 0
        if (len) {
          number.value = number.value.slice(0, len - 1)
        } else {
          number.value = ''
        }
      }
    }

    const handleKey = (e: KeyboardEvent) => {
      const input = e.target as HTMLInputElement
      const key = e.key || ''
      if (
        e.altKey ||
        e.ctrlKey ||
        key.startsWith('Arrow') ||
        key === 'Tab' ||
        key === 'Enter'
      ) {
        return true
      }
      if (key === 'Backspace') {
        deleteChar(input.selectionStart)
      }
      if (key >= '0' && key <= '9') {
        if (number.value.length === 10) {
          e.preventDefault()
          return false
        }
      } else {
        e.preventDefault()
        return false
      }
      return true
    }

    return () => (
      <input
        attrs={ctx.attrs}
        onKeydown={handleKey}
        value={phoneText.value}
        onInput={updateNumber}
        onFocus={setCaret}
        placeholder={template}
        ref="input"
        type="tel"
      />
    )
  }
})
