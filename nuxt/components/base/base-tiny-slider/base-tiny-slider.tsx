import { TinySliderInstance, TinySliderSettings } from 'tiny-slider'
import {
  defineComponent,
  onMounted,
  watch,
  ref,
  ssrRef,
  computed
} from '@nuxtjs/composition-api'
import tinyStyles from './base-tiny-slider.scss'
import { useIntersectedOnce } from '~/support/hooks/useIntersectedOnce'

tinyStyles.__inject__ && tinyStyles.__inject__()
// export type BaseTinySliderProps = {
//   options: TinySliderSettings
//   currentIndex: number
//   prevClass: string
//   nextClass: string
// }

export const baseTinySliderProps = {
  options: {
    type: Object as () => TinySliderSettings,
    default: () => {
      return {} as TinySliderSettings
    }
  },
  selectedIndex: {
    type: Number,
    default: null
  },
  currentIndex: {
    type: Number,
    default: 0
  },
  prevClass: {
    type: String,
    default: null
  },
  nextClass: {
    type: String,
    default: null
  },
  sliderClass: {
    type: String,
    default: null
  },
  onInit: {
    type: Function,
    default: null
  },
  hideDuringInitiating: {
    type: Boolean,
    default: false
  }
}

export const BaseTinySlider = defineComponent({
  props: baseTinySliderProps,
  setup: (props, context) => {
    const refs = context.refs
    let slider: TinySliderInstance
    const intersected = useIntersectedOnce(() => refs.main as Element)
    const innerIndex = ref(props.currentIndex)
    const initialized = ssrRef<boolean>(process.server)

    onMounted(async () => {
      if (!refs.slider) {
        return
      }
      const { tns } = await import('tiny-slider/src/tiny-slider')

      const sliderElement = refs.slider as HTMLDivElement
      Array.prototype.forEach.call(
        sliderElement.children,
        (v, i) => (v.dataset.sliderNumber = i)
      )

      const updateSliderClasses = () => {
        Array.prototype.forEach.call(sliderElement.children, (v) => {
          v.classList.remove('tns-main-slide')
          v.classList.remove('tns-prev-slide')
          v.classList.remove('tns-next-slide')
        })
        const slides = sliderElement.querySelectorAll(
          `[data-slider-number="${props.currentIndex}"]`
        )
        Array.prototype.forEach.call(slides, (v) => {
          v.classList.add('tns-main-slide')
          const prev = v.previousSibling
          if (prev && prev !== v) {
            prev.classList.add('tns-prev-slide')
          }
          const next = v.nextSibling
          if (next && next !== v) {
            next.classList.add('tns-next-slide')
          }
        })
      }

      const updateSelectedClass = () => {
        if (props.selectedIndex === null) {
          return
        }
        const current = sliderElement.querySelector(
          `.tns-selected-slide`
        ) as HTMLElement
        const newSlide = sliderElement.querySelector(
          `[data-slider-number="${props.selectedIndex}"]`
        ) as HTMLElement
        if (current === newSlide) {
          return
        }
        if (current) {
          current.classList.remove('tns-selected-slide')
        }
        if (newSlide) {
          newSlide.classList.add('tns-selected-slide')
        }
      }

      updateSelectedClass()

      const checkLastItem = (displayIndex: number) => {
        if (!sliderElement.parentElement) {
          return
        }
        const index = displayIndex - 1
        let fullWidth = 0
        let offsetWidth = 0
        Array.prototype.forEach.call(
          sliderElement.children,
          (value, index2) => {
            fullWidth += value.offsetWidth
            if (index2 < index) {
              offsetWidth = fullWidth
            }
          }
        )
        ;(refs.next as HTMLButtonElement).disabled =
          fullWidth - offsetWidth < sliderElement.parentElement.offsetWidth
        // ;(refs.prev as HTMLButtonElement).disabled = 0
      }

      watch(() => props.currentIndex, updateSliderClasses)
      watch(() => props.selectedIndex, updateSelectedClass)

      const options = {
        container: sliderElement,
        slideBy: 1,
        items: 1,
        nav: false,
        speed: 200,
        loop: true,
        preventActionWhenRunning: true,
        startIndex: innerIndex.value,
        lazyload: true,
        onInit: () => {
          initialized.value = true
        },
        ...props.options
      } as TinySliderSettings

      if (refs.prev) {
        options.prevButton = refs.prev as HTMLButtonElement
      }
      if (refs.next) {
        options.nextButton = refs.next as HTMLButtonElement
      }
      watch(
        () => intersected.value,
        (value) => {
          if (value) {
            slider = tns(options)
            if (slider) {
              slider.events.on('indexChanged', (info) => {
                innerIndex.value = info.displayIndex - 1
                context.emit('change', innerIndex.value)
                if (!options.loop && options.autoWidth) {
                  checkLastItem(info.displayIndex)
                }
              })
            }
          }
        },
        {
          immediate: true
        }
      )
      updateSliderClasses()
    })

    watch(
      () => props.currentIndex,
      (value) => {
        if (slider && innerIndex.value !== value) {
          slider.goTo(value)
        }
      }
    )

    const handleItemClick = (e: { target: HTMLElement }) => {
      const item = e.target.closest('.tns-item') as HTMLElement
      if (item && item.dataset.sliderNumber !== undefined) {
        context.emit('click', +item.dataset.sliderNumber)
      }
    }

    const oneSlide = computed(
      () => (props.options?.items || 1) <= 1 && !props.options.autoWidth
    )

    return () => (
      <div
        class={{
          'tns-default-outer': true,
          'tns-one-slide': oneSlide.value
        }}
        ref="main"
      >
        {context.slots.prev && (
          <button
            ref="prev"
            class={['tns-button', props.prevClass]}
            disabled={
              props.options.loop === false &&
              (!props.options.startIndex || props.options.startIndex === 1)
            }
          >
            {context.slots.prev()}
          </button>
        )}
        {context.slots.next && (
          <button ref="next" class={['tns-button', props.nextClass]}>
            {context.slots.next()}
          </button>
        )}
        <div
          ref="slider"
          onClick={handleItemClick}
          class={[props.sliderClass, 'tns-default-inner']}
          style={{
            visibility:
              initialized.value || !props.hideDuringInitiating
                ? 'visible'
                : 'hidden'
          }}
        >
          {context.slots.default && context.slots.default()}
        </div>
      </div>
    )
  }
})
