import {
  computed,
  defineComponent,
  ref,
  useContext
} from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { baseVideoBlockProps } from '~/components/base/base-video-block/props'
import { BaseVideo } from '~/components/base/base-video'
import { BaseIcon } from '~/components/base'
import style from './base-video-block.scss?module'

const cn = useClassNames('base-video-block', style)

const classes = {
  main: cn(),
  poster: cn('poster'),
  video: cn('video')
}

export const BaseVideoBlock = injectStyles(
  style,
  defineComponent({
    props: {
      ...baseVideoBlockProps,
      showFullScreen: {
        type: Boolean,
        default: true
      }
    },
    setup: (props, { slots, emit, refs }) => {
      const { $device } = useContext()

      const started = ref<boolean>(false)
      const playing = ref<boolean>(false)
      const onPlay = () => {
        playing.value = true
        emit('play')
      }
      const onPause = () => {
        playing.value = false
        emit('pause')
      }
      const onStarted = () => {
        started.value = true
        emit('pause')
      }
      const showPoster = computed(
        () => !playing.value && !(props.withControls && started.value)
      )
      const openFull = () => {
        if (refs.videoBlock) {
          // @ts-ignore
          refs.videoBlock.openFullScreen()
        }
      }

      const showFullScreen = computed(
        () =>
          !showPoster.value && props.showFullScreen && !$device.isMobileOrTablet
      )
      return () => (
        <div class={classes.main}>
          <div
            class={[classes.poster, props.posterClass]}
            style={{ backgroundImage: `url(${props.poster})` }}
            vShow={showPoster.value}
            onClick={onPlay}
          >
            {slots.default && slots.default()}
          </div>
          {showFullScreen.value && (
            <button onClick={openFull} class={cn('fullscreen')}>
              <BaseIcon name="full-size" width={20} height={20} />
            </button>
          )}
          <BaseVideo
            ref="videoBlock"
            class={classes.video}
            video={props.video}
            poster={props.poster}
            withControls={props.withControls}
            playing={playing.value}
            autoFullscreen={props.autoFullscreen}
            onPlay={onPlay}
            onPause={onPause}
            onStarted={onStarted}
          />
        </div>
      )
    }
  })
)
