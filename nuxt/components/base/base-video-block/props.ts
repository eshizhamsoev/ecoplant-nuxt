import { baseVideoProps } from '~/components/base/base-video'

export const baseVideoBlockProps = {
  ...baseVideoProps,
  posterClass: {
    type: String,
    default: null
  }
}
