import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon, BaseTinySlider } from '~/components/base'
import style from './base-slider-select.scss?module'

const cn = useClassNames('base-slider-select', style)

const classes = {
  main: cn()
}

export const BaseSliderSelect = injectStyles(
  style,
  defineComponent({
    name: 'BaseSliderSelect',
    props: {
      value: {
        type: Number,
        required: true
      },
      options: {
        type: Array as () => readonly string[],
        required: true
      }
    },
    setup: (props, { emit }) => {
      const onClick = (newIndex: number) => {
        emit('input', newIndex)
      }

      return () => (
        <BaseTinySlider
          class={classes.main}
          options={{
            autoWidth: true,
            loop: false,
            gutter: 0,
            swipeAngle: false,
            preventScrollOnTouch: 'force'
          }}
          selectedIndex={props.value}
          onClick={onClick}
          prevClass={cn('prev')}
          nextClass={cn('next')}
        >
          {props.options.map((item, index) => (
            <div key={index}>
              <button class={cn('button')}>{item}</button>
            </div>
          ))}
          <template slot="prev">
            <BaseIcon name="arrow-left" width={15} />
          </template>
          <template slot="next">
            <BaseIcon name="arrow-left" width={15} rotate={180} />
          </template>
        </BaseTinySlider>
      )
    }
  })
)
