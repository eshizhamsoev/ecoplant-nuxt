import { computed, defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { baseVideoBlockProps } from '~/components/base/base-video-block/props'
import style from './base-youtube-with-poster.scss?module'

const cn = useClassNames('base-youtube-with-poster', style)

const classes = {
  main: cn(),
  poster: cn('poster'),
  posterImage: cn('poster-image'),
  wrapper: cn('wrapper'),
  button: cn('button'),
  video: cn('video')
}

export const BaseYoutubeWithPoster = injectStyles(
  style,
  defineComponent({
    props: {
      ...baseVideoBlockProps,
      showFullScreen: {
        type: Boolean,
        default: true
      }
    },
    setup: (props, { slots, emit, refs }) => {
      const started = ref<boolean>(false)
      const playing = ref<boolean>(false)
      const onPlay = () => {
        playing.value = true
        emit('play')
      }
      const playVideo = () => {
        // @ts-ignore
        refs.video.player.playVideo()
      }
      const showPoster = computed(
        () => !playing.value && !(props.withControls && started.value)
      )
      return () => (
        <div class={classes.main}>
          <img
            src={props.poster}
            class={classes.posterImage}
            alt=""
            style={{ visibility: showPoster.value ? 'visible' : 'hidden' }}
            loading={'lazy'}
          />
          <div
            class={[classes.wrapper, classes.button]}
            style={{ visibility: showPoster.value ? 'visible' : 'hidden' }}
            onClick={onPlay}
          >
            {slots.default && slots.default()}
          </div>
          <div class={classes.wrapper}>
            {!showPoster.value && (
              <youtube
                onReady={playVideo}
                ref="video"
                videoId={props.video}
                width="100%"
                height="100%"
                className={cn('video')}
              />
            )}
          </div>
        </div>
      )
    }
  })
)
