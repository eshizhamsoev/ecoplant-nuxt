import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon, BaseRoundButton } from '~/components/base'
import { Color, Visual } from '~/components/theme/theme-button'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { BaseBadgedText } from '~/components/base/base-badged-text/base-badged-text'
import { ImageWithSize, Video } from '~/_generated/types'
import style from './base-video-poster.scss?module'

const cn = useClassNames('base-video-poster', style)

const classes = {
  poster: cn('poster'),
  button: cn('button'),
  buttonText: cn('button-text'),
  icon: cn('icon')
}

type videoProp = string | null

export const BaseVideoPoster = injectStyles(
  style,
  defineComponent({
    name: 'BaseVideoPoster',
    props: {
      buttonText: {
        type: String as () => videoProp,
        default: null
      },
      classButtonText: {
        type: String,
        default: ''
      },
      poster: {
        type: String,
        default: null
      },
      videoUrl: {
        type: String as () => videoProp,
        default: null
      },
      youtubeId: {
        type: String as () => videoProp,
        default: null
      },
      videoObject: {
        type: Object as () => Video,
        default: null
      },
      posterObject: {
        type: Object as () => ImageWithSize,
        default: null
      },
      buttonSize: {
        type: Number,
        default: 55
      },
      buttonWithShadow: {
        type: Boolean,
        default: true
      },
      buttonColor: {
        default: Color.green,
        type: String as () => Color
      }
    },
    setup: (props) => {
      const modalOpener = useModalOpener()

      const openModal = () => {
        if (props.videoObject) {
          if (props.videoObject.__typename === 'YoutubeVideo') {
            modalOpener({
              name: 'youtube-video',
              code: props.videoObject.code
            })
          } else if (props.videoObject.__typename === 'FileVideo') {
            modalOpener({
              name: 'common-video',
              url: props.videoObject.source
            })
          }
        } else if (props.youtubeId) {
          modalOpener({
            name: 'youtube-video',
            code: props.youtubeId
          })
        } else {
          modalOpener({
            name: 'common-video',
            url: props.videoUrl
          })
        }
      }

      const iconSize = computed(() => Math.floor(props.buttonSize / 3))

      // TODO make button
      return () => (
        <div class={cn()} onClick={openModal}>
          {props.posterObject ? (
            <img
              src={props.posterObject.src}
              width={props.posterObject.width}
              height={props.posterObject.height}
              alt=""
              loading="lazy"
              class={classes.poster}
            />
          ) : (
            <img src={props.poster} alt="" class={classes.poster} />
          )}
          <div class={cn('overlay')}>
            <BaseRoundButton
              shadow={props.buttonWithShadow}
              size={props.buttonSize}
              visual={Visual.gradient}
              class={cn('button', { animated: true })}
              leftOffset={Math.floor(props.buttonSize / 1.9) + 'px'}
              color={props.buttonColor}
            >
              <BaseIcon
                width={iconSize.value}
                name="play"
                class={classes.icon}
              />
            </BaseRoundButton>

            {props.buttonText && (
              <BaseBadgedText
                lines={props.buttonText.split('\n')}
                leftOffset={Math.floor(props.buttonSize / 1.9) + 'px'}
                class={[classes.buttonText, props.classButtonText]}
                // style={buttonTextStyle.value}
              />
            )}
          </div>
        </div>
      )
    }
  })
)
