import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon, BaseRoundButton } from '~/components/base'
import { Color } from '~/components/theme/theme-button'
import style from './base-mobile-action-circle-button.scss?module'

const cn = useClassNames('base-mobile-action-circle-button', style)

const classes = {
  main: cn(),
  icon: cn('icon'),
  name: cn('name')
}

export const BaseMobileActionCircleButton = injectStyles(
  style,
  defineComponent({
    name: 'BaseMobileActionCircleButton',
    props: {
      iconName: {
        required: true,
        type: String
      },
      iconWidth: {
        required: true,
        type: Number
      },
      color: {
        type: String as () => Color,
        default: Color.green
      }
    },
    setup: (props, { emit, slots }) => {
      return () => (
        <BaseRoundButton
          class={classes.main}
          size={70}
          color={props.color}
          onClick={() => emit('click')}
        >
          <BaseIcon
            name={props.iconName}
            width={props.iconWidth}
            class={classes.icon}
          />
          <span class={classes.name}>{slots.text && slots.text()}</span>
        </BaseRoundButton>
      )
    }
  })
)
