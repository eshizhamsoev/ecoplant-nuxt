import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseLoadingEllipsis } from '~/components/base/base-loading-ellipsis'
import style from './base-loading.scss?module'

const cn = useClassNames('base-loading', style)

const classes = {
  main: cn(),
  ellipsis: cn('ellipsis')
}

export const BaseLoading = injectStyles(
  style,
  defineComponent({
    name: 'BaseLoading',
    props: {
      loading: {
        type: Boolean,
        required: true
      }
    },
    setup: (props, { slots }) => {
      return () => (
        <div class={classes.main}>
          {slots.default && slots.default()}
          {props.loading && <BaseLoadingEllipsis class={classes.ellipsis} />}
        </div>
      )
    }
  })
)
