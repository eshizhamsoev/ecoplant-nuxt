import { defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './base-youtube-poster-popup.scss?module'

const cn = useClassNames('base-youtube-poster-popup', style)

export const BaseYoutubePosterModal = injectStyles(
  style,
  defineComponent({
    name: 'BaseYoutubePosterModal',
    props: {
      videoId: {
        type: String,
        required: true
      }
    },
    setup: (props, { slots }) => {
      const shown = ref(false)
      const showVideo = () => {
        shown.value = true
      }
      return () => (
        <div class={cn()}>
          <div class={cn('poster')} onClick={showVideo}>
            {slots.default && slots.default()}
          </div>
          {shown.value && (
            <div class={cn('video')}>
              <youtube
                videoId={props.videoId}
                width="100%"
                height="100%"
                className={cn('video')}
                playerVars={{ autoplay: true }}
              />
            </div>
          )}
        </div>
      )
    }
  })
)
