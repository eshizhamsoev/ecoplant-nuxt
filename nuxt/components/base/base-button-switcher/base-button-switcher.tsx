import { computed, defineComponent, ref, watch } from '@nuxtjs/composition-api'
import type { PropOptions } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import style from './base-button-switcher.scss?module'

const cn = useClassNames('base-button-switcher', style)

export const BaseButtonSwitcher = injectStyles(
  style,
  defineComponent({
    name: 'BaseButtonSwitcher',
    props: {
      value: {
        validator: (prop: any) => typeof prop === 'boolean' || prop === null,
        required: true,
        default: null as null | boolean
      } as PropOptions<null | boolean>,
      name: {
        type: String,
        required: true
      },
      inputId: {
        type: String,
        default: null
      },
      required: {
        type: Boolean,
        default: false
      }
    },
    setup: (props, { emit }) => {
      const innerValue = ref(props.value)
      watch(
        () => innerValue.value,
        () => {
          if (innerValue.value !== props.value) {
            emit('input', innerValue.value)
          }
        }
      )
      watch(
        () => props.value,
        () => {
          innerValue.value = props.value
        }
      )
      const isFalse = computed(() => innerValue.value === false)
      const isTrue = computed(() => innerValue.value === true)
      return () => (
        <div class={cn()}>
          <label class={cn('button', { active: isTrue.value })}>
            Да <BaseIcon name="check" width={20} class={cn('icon')} />
            <input
              class={cn('input')}
              id={props.inputId}
              type="radio"
              value={1}
              name={props.name}
              required={props.required}
              checked={isTrue.value}
              onChange={() => (innerValue.value = true)}
            />
          </label>
          <label class={cn('button', { active: isFalse.value })}>
            Нет <BaseIcon name="times-circle" width={20} class={cn('icon')} />
            <input
              class={cn('input')}
              type="radio"
              value={0}
              name={props.name}
              checked={isFalse.value}
              onChange={() => (innerValue.value = false)}
            />
          </label>
        </div>
      )
    }
  })
)
