import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './base-text.scss?module'

const cn = useClassNames('base-text', style)

export const BaseText = injectStyles(
  style,
  defineComponent({
    name: 'BaseText',
    props: {
      text: {
        type: String,
        required: true
      }
    },
    setup: (props) => {
      const paragraphs = computed(() => props.text.split('\n'))
      return () => (
        <div class={cn()}>
          {paragraphs.value.map((paragraph) => (
            <p>{paragraph}</p>
          ))}
        </div>
      )
    }
  })
)
