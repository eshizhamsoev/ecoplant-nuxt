import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './base-bottom-fixes-line.scss?module'

const cn = useClassNames('base-fixed-line', style)

export const BaseBottomFixesLine = injectStyles(
  style,
  defineComponent({
    name: 'BaseBottomFixesLine',
    setup: (props, { slots }) => {
      return () => <div class={cn()}>{slots.default && slots.default()}</div>
    }
  })
)
