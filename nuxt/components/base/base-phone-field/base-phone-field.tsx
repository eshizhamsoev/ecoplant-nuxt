import { defineComponent } from '@nuxtjs/composition-api'
import { BaseField } from '~/components/base/base-field'
import { BaseIcon } from '~/components/base/base-icon'
import {
  cnPatternIconPlusIcon,
  Indent
} from '~/components/pattern/pattern-icon-plus'

import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePhoneInput } from '~/components/base/base-phone-input'
import style from './base-phone-field.scss?module'

const cn = useClassNames('base-phone-field', style)

export const BasePhoneField = injectStyles(
  style,
  defineComponent({
    props: {
      withIcon: {
        type: Boolean,
        default: true
      },
      value: {
        type: String,
        required: true
      },
      error: {
        type: Boolean,
        default: false
      },
      id: {
        type: String,
        default: null
      }
    },
    setup: (props, { emit }) => {
      const onInput = (newValue: string) => emit('input', newValue)

      return () => (
        <BaseField error={props.error}>
          {props.withIcon && (
            <BaseIcon
              name="phone"
              width={13}
              class={[cn('icon'), cnPatternIconPlusIcon({ indent: Indent.m })]}
            />
          )}
          <BasePhoneInput value={props.value} onInput={onInput} size="17" />
        </BaseField>
      )
    }
  })
)
