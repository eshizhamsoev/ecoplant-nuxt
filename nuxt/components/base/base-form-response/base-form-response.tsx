import { defineComponent } from '@nuxtjs/composition-api'
import { injectStyles } from '~/support/utils/injectStyle'
import { PhoneLink } from '~/components/action-providers/phone-link'
import style from './base-form-response.scss?module'

export const BaseFormResponse = injectStyles(
  style,
  defineComponent({
    name: 'BaseFormResponse',
    props: {
      fatalError: {
        type: Boolean,
        required: true
      },
      successMessage: {
        validator: (prop: any) => typeof prop === 'string' || prop === null,
        default: null as string | null
      },
      mainClass: {
        type: String,
        default: ''
      },
      successClass: {
        type: String,
        default: ''
      },
      errorClass: {
        type: String,
        default: ''
      }
    },
    setup: (props, { slots }) => {
      return () => {
        if (props.fatalError) {
          return (
            <div class={props.errorClass}>
              К сожалению произошла ошибка при отправке формы, но вы можете{' '}
              <PhoneLink>нам позвонить</PhoneLink>
            </div>
          )
        } else if (props.successMessage) {
          return (
            <div
              domProps={{ innerHTML: props.successMessage }}
              class={props.successClass}
            />
          )
        } else {
          return (
            <div class={props.mainClass}>
              {slots.default && slots.default()}
            </div>
          )
        }
      }
    }
  })
)
