import { defineComponent, computed } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseLink } from '~/components/base/base-link'
import style from './base-breadcrumbs.scss?module'

const cn = useClassNames('base-breadcrumbs', style)

export type BreadcrumbItem = {
  name: string
  url: string
}

export const BaseBreadcrumbs = injectStyles(
  style,
  defineComponent({
    name: 'BaseBreadcrumbs',
    props: {
      items: {
        type: Array as () => BreadcrumbItem[],
        required: true
      },
      lastItemIsLink: {
        type: Boolean,
        default: false
      }
    },
    setup: (props) => {
      const linkItems = computed(() =>
        props.lastItemIsLink ? props.items : props.items.slice(0, -1)
      )
      const nonLinkItem = computed(
        () => !props.lastItemIsLink && props.items[props.items.length - 1]
      )
      return () => (
        <ul class={cn()}>
          {linkItems.value.map((item) => (
            <li key={item.url} class={cn('item')}>
              <BaseLink to={item.url} class={cn('link')}>
                {item.name}
              </BaseLink>
            </li>
          ))}
          {nonLinkItem.value && (
            <li key={nonLinkItem.value.url} class={cn('item')}>
              <span class={cn('link', { disabled: true })}>
                {nonLinkItem.value.name}
              </span>
            </li>
          )}
        </ul>
      )
    }
  })
)
