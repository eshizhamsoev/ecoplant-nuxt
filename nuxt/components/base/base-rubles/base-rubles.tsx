import { h } from '@vue/composition-api'
import { computed, defineComponent } from '@nuxtjs/composition-api'

export const BaseRubles = defineComponent({
  props: {
    price: {
      type: Number,
      required: true
    },
    postfix: {
      type: String,
      default: 'р.'
    },
    delimiter: {
      type: String,
      default: '&thinsp;'
    }
  },
  setup: (props) => {
    const price = computed(
      () =>
        props.price
          .toString()
          .replace('.', ',')
          .replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, `$1${props.delimiter}`) +
        props.postfix
    )
    return () =>
      h('span', {
        domProps: {
          innerHTML: price.value
        },
        style: {
          whiteSpace: 'nowrap'
        }
      })
  }
})
