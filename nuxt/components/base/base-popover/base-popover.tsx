import { defineComponent, onMounted, watch } from '@nuxtjs/composition-api'

export const BasePopover = defineComponent({
  name: 'BasePopover',
  props: {
    visible: {
      type: Boolean,
      required: true
    }
  },
  setup: (props, { slots, emit }) => {
    const close = () => {
      emit('close')
    }

    onMounted(() => {
      watch(
        () => props.visible,
        (newValue) => {
          if (newValue) {
            document.addEventListener('click', close)
          } else {
            document.removeEventListener('click', close)
          }
        }
      )
    })
    return () =>
      props.visible && (
        <div onmousedown={(e: Event) => e.stopPropagation()}>
          {slots.default && slots.default()}
        </div>
      )
  }
})
