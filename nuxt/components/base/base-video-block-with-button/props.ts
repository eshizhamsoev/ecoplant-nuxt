import { baseVideoProps } from '~/components/base/base-video'

type ButtonProps = {
  size?: number
  class?: string
  animate?: boolean
}

export const baseVideoBlockWithButtonProps = {
  ...baseVideoProps,
  buttonVisual: {
    type: Object as () => ButtonProps,
    default: () => {
      return {} as ButtonProps
    }
  },
  withOverlay: {
    type: Boolean,
    default: false
  },
  buttonText: {
    type: String,
    default: null
  },
  overlayColor: {
    type: String,
    default: 'rgba(0, 0, 0, 0.35)'
  },
  overlayClass: {
    type: String,
    default: ''
  },
  classButtonText: {
    type: String,
    default: ''
  }
}
