import { computed, defineComponent } from '@nuxtjs/composition-api'
import { BaseVideoBlock } from '~/components/base/base-video-block'
import { BaseRoundButton } from '~/components/base/base-round-button'
import { BaseIcon } from '~/components/base/base-icon'
import { useClassNames } from '~/support/utils/bem-classnames'
import { Visual } from '~/components/theme/theme-button'
import { injectStyles } from '~/support/utils/injectStyle'
import { baseVideoBlockWithButtonProps } from '~/components/base/base-video-block-with-button/props'
import { BaseBadgedText } from '~/components/base/base-badged-text/base-badged-text'
import style from './base-video-block-with-button.scss?module'

const cn = useClassNames('base-video-block-with-button', style)

const classes = {
  poster: cn('poster'),
  overlay: cn('overlay'),
  button: cn('button'),
  icon: cn('icon'),
  buttonText: cn('button-text')
}

export const BaseVideoBlockWithButton = injectStyles(
  style,
  defineComponent({
    props: baseVideoBlockWithButtonProps,
    setup: (props, { slots, emit, root }) => {
      const buttonSize = computed(
        () =>
          props.buttonVisual.size || (root.$device.isMobileOrTablet ? 65 : 120)
      )
      const iconSize = computed(() => Math.floor(buttonSize.value / 3))
      const buttonTextStyle = computed(() => ({
        marginLeft: -buttonSize.value / 2 + 'px'
      }))
      return () => (
        <BaseVideoBlock
          // @ts-ignore
          onPlay={() => emit('play')}
          onPause={() => emit('pause')}
          withControls={props.withControls}
          class={cn()}
          poster-class={classes.poster}
          video={props.video}
          autoFullscreen={props.autoFullscreen}
          poster={props.poster}
          type={props.type}
        >
          {props.withOverlay && (
            <div
              class={[classes.overlay, props.overlayClass]}
              style={{ backgroundColor: props.overlayColor }}
            />
          )}
          <BaseRoundButton
            shadow={true}
            size={buttonSize.value}
            visual={Visual.gradient}
            class={[
              cn('button', { animated: props.buttonVisual.animate }),
              props.buttonVisual.class
            ]}
          >
            <BaseIcon width={iconSize.value} name="play" class={classes.icon} />
          </BaseRoundButton>

          {props.buttonText && (
            <BaseBadgedText
              lines={props.buttonText.split('\n')}
              leftOffset={Math.floor(buttonSize.value / 1.9) + 'px'}
              class={[classes.buttonText, props.classButtonText]}
              style={buttonTextStyle.value}
            />
          )}
          {slots.default && slots.default()}
        </BaseVideoBlock>
      )
    }
  })
)
