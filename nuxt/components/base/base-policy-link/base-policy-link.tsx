import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseLink } from '~/components/base/base-link'
import style from './base-policy-link.scss?module'

const cn = useClassNames('base-policy-link', style)

export const BasePolicyLink = injectStyles(
  style,
  defineComponent({
    name: 'BasePolicyLink',
    props: {
      text: {
        type: String,
        default: 'Согласен с политикой конфиденциальности\n'
      }
    },
    setup: (props) => {
      return () => (
        <BaseLink to={'/privacy-policy'} class={cn()}>
          {props.text}
        </BaseLink>
      )
    }
  })
)
