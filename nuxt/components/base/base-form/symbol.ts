import type { InjectionKey } from '@nuxtjs/composition-api'
import { FormHandler } from '~/components/base/base-form/types'

export const submitFormActionSymbol: InjectionKey<FormHandler> =
  Symbol('submit-form-action')
