import { h } from '@vue/composition-api'
import { defineComponent } from '@nuxtjs/composition-api'
import { BaseField } from '~/components/base'
import { Field, FormData } from '~/components/base/base-form/types'

export const BaseFormFieldsFactory = defineComponent({
  name: 'BaseFormFieldsFactory',
  props: {
    fields: {
      type: Array as () => Field[],
      required: true
    },
    value: {
      type: Object as () => FormData,
      required: true
    },
    fieldClass: {
      type: String,
      required: true
    }
  },
  setup: (props, { slots, emit }) => {
    return () => (
      <div>
        {props.fields.map((field) =>
          slots.fieldGroup ? (
            slots.fieldGroup(field)
          ) : (
            <BaseField class={[props.fieldClass, field.options.class]}>
              {h(field.component, {
                props: {
                  value: props.value[field.name]
                },
                attrs: {
                  name: field.name,
                  ...field.options
                },
                on: {
                  input(event: any) {
                    emit('input', {
                      ...props.value,
                      [field.name]:
                        typeof event === 'string' ? event : event?.target?.value
                    })
                  }
                }
              })}
            </BaseField>
          )
        )}
      </div>
    )
  }
})
