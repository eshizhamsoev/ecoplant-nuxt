import { Component } from 'vue/types/options'

export type Field = {
  component: Component | string
  label: string
  name: string
  options?: any
}

export type FormData = { [key: string]: string }

export type FormHandler = (data: FormData) => Promise<any>

export type BaseFormDefaultSlot = { loading: boolean }
