import { defineComponent, ref, inject } from '@nuxtjs/composition-api'
import { Field, FormData } from './types'
import { submitFormActionSymbol } from './symbol'
import { BaseFormFieldsFactory } from '~/components/base/base-form/base-form-fields-factory'

function createFormDataObjectFromFields(fields: Field[]): FormData {
  const dataObject = {} as FormData
  fields.forEach((field) => {
    dataObject[field.name] = ''
  })
  return dataObject
}

export const BaseForm = defineComponent({
  name: 'BaseFormData',
  props: {
    fields: {
      type: Array as () => Field[],
      required: true
    },
    fieldClass: {
      type: String,
      required: true
    }
  },
  setup: (props, { slots }) => {
    const submitForm = inject(submitFormActionSymbol)
    const success = ref(null)
    const error = ref(null)
    const loading = ref(false)

    const formData = ref(createFormDataObjectFromFields(props.fields))
    const submit = (e: Event) => {
      if (loading.value) {
        return
      }
      loading.value = true
      e.preventDefault()
      if (submitForm) {
        submitForm(formData.value)
          .then((result) => {
            success.value = result
            loading.value = false
          })
          .catch((err) => {
            error.value = err
          })
      }
    }
    const onInput = (newData: FormData) => {
      formData.value = newData
    }
    return () => (
      <div>
        {error.value ? (
          slots.error && slots.error({ error: error.value })
        ) : success.value ? (
          slots.success && slots.success({ success: success.value })
        ) : (
          <form onSubmit={submit}>
            <BaseFormFieldsFactory
              fieldClass={props.fieldClass}
              fields={props.fields}
              value={formData.value}
              onInput={onInput}
            />
            {slots.default && slots.default({ loading: loading.value })}
          </form>
        )}
      </div>
    )
  }
})
