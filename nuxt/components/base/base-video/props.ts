export const baseVideoProps = {
  video: {
    type: String as () => string | null,
    required: true as true
  },
  poster: {
    type: String as () => string,
    required: true as true
  },
  type: {
    type: String,
    default: 'video/mp4'
  },
  playing: {
    type: Boolean,
    default: false
  },
  withControls: {
    type: Boolean,
    default: false
  },
  autoFullscreen: {
    type: Boolean,
    default: false
  }
}
