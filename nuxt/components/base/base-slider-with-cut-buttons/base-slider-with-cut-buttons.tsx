import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import {
  BaseTinySlider,
  baseTinySliderProps
} from '~/components/base/base-tiny-slider/base-tiny-slider'
import { BaseIcon } from '~/components/base/base-icon'
import style from './base-slider-with-cut-buttons.scss?module'

const cn = useClassNames('base-slider-with-cut-buttons', style)

export const BaseSliderWithCutButtons = injectStyles(
  style,
  defineComponent({
    name: 'BaseSliderWithCutButtons',
    props: { ...baseTinySliderProps },
    setup(props, { slots, emit }) {
      return () => (
        <BaseTinySlider
          options={props.options}
          class={cn()}
          currentIndex={props.currentIndex}
          onChange={(newIndex: number) => emit('change', newIndex)}
          onClick={(slideNumber: number) => emit('click', slideNumber)}
          prevClass={[cn('prev'), props.prevClass].join(' ')}
          nextClass={[cn('next'), props.nextClass].join(' ')}
        >
          {slots.default && slots.default()}
          <template slot="prev">
            <BaseIcon name="chevron-left" width={12} />
          </template>
          <template slot="next">
            <BaseIcon name="chevron-left" rotate={180} width={12} />
          </template>
        </BaseTinySlider>
      )
    }
  })
)
