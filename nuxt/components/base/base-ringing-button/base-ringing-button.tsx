import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import style from './base-ringing-button.scss?module'

const cn = useClassNames('base-ringing-button', style)

export const BaseRingingButton = injectStyles(
  style,
  defineComponent({
    name: 'BaseRingingButton',
    props: {
      icon: {
        required: true,
        type: String
      },
      text: {
        required: true,
        type: String
      }
    },
    setup: (props) => {
      return () => (
        <div class={cn()}>
          <div class={cn('icon-wrapper')}>
            <BaseIcon
              class={cn('icon')}
              name={props.icon}
              height={20}
              width={20}
            />
            <div class={cn('icon-background')} />
          </div>
          <span class={cn('text')}>{props.text}</span>
        </div>
      )
    }
  })
)
