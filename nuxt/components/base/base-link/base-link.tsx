import {
  defineComponent,
  onMounted,
  PropOptions,
  useContext
} from '@nuxtjs/composition-api'

export const BaseLink = defineComponent({
  name: 'BaseLink',
  props: {
    to: {
      validator(value) {
        return typeof value === 'string' || typeof value === 'object'
      },
      required: true
    } as PropOptions<string | object>
  },
  setup: (props, { slots, emit, refs, root }) => {
    const { route } = useContext()
    const click = () => {
      emit('click')
      if (!route.value.hash) {
        return
      }
      // @ts-ignore
      if (!refs.link?.$el) {
        return
      }
      // @ts-ignore
      const el: Element = refs.link.$el
      if (!el.classList.contains('nuxt-link-exact-active')) {
        return
      }
      const routeObject = { ...route.value }
      // @ts-ignore
      root.$router.replace({ ...route.value, hash: '' }).then(() => {
        // @ts-ignore
        root.$router.replace(routeObject)
      })
    }

    onMounted(() => {
      // @ts-ignore
      refs.link?.$el.addEventListener('click', click, {
        // It should be fired before route changed
        capture: true
      })
    })
    return () => (
      <nuxt-link to={props.to} ref="link">
        {slots.default && slots.default()}
      </nuxt-link>
    )
  }
})
