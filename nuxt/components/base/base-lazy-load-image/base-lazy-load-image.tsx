import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './base-lazy-load-image.scss?module'

const cn = useClassNames('base-lazy-load-image', style)

const className = cn()

export const BaseLazyLoadImage = injectStyles(
  style,
  defineComponent({
    name: 'BaseLazyLoadImage',
    props: {
      width: {
        type: Number,
        required: true
      },
      height: {
        type: Number,
        required: true
      },
      src: {
        type: String,
        required: true
      },
      alt: {
        type: String,
        default: ''
      }
    },
    setup: (props, { listeners }) => {
      return () => (
        <img
          src={props.src}
          loading="lazy"
          alt={props.alt}
          class={className}
          width={props.width}
          height={props.height}
          on={listeners}
        />
      )
    }
  })
)
