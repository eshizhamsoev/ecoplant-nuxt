import cartesian from 'storybook-cartesian'
import { storiesOf } from '@storybook/vue'
import BaseButton from './base-button'

import { color, size, round, visual } from './props'

const renderLine = (h, getChildren) => {
  return h(
    'div',
    {
      components: { BaseButton }
    },
    getChildren()
  )
}

cartesian(storiesOf('Button', module)).add(
  () => ({
    // color: color.variants,
    // size: size.variants,
    round: round.variants,
    visual: visual.variants,
    shadow: [true, false]
  }),
  (baseProps) => (h, additionalProps) => {
    const props = Object.assign({}, baseProps, additionalProps)
    return h(
      BaseButton,
      {
        props,
        style: {
          margin: '7px'
        }
      },
      Object.values(props).join(' ')
    )
  },
  {
    apply(stories, candidates) {
      stories.add('All variants', () => ({
        render(h) {
          return renderLine(h, () =>
            color.variants.map((color) =>
              renderLine(h, () =>
                size.variants.map((size) =>
                  renderLine(h, () =>
                    candidates.map((candidate) =>
                      candidate.story(h, { color, size })
                    )
                  )
                )
              )
            )
          )
        }
      }))
    }
  }
)
// export default { title: 'Button' }
