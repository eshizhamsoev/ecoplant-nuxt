import { defineComponent, computed } from '@nuxtjs/composition-api'
import { baseButtonProps } from './props'
import { useClassNames } from '~/support/utils/bem-classnames'
import { cnThemeButton } from '~/components/theme/theme-button'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseLink } from '~/components/base/base-link'
import style from './base-button.scss?module'

export const cnBaseButton = useClassNames('base-button', style)

export const BaseButton = injectStyles(
  style,
  defineComponent({
    name: 'BaseButton',
    props: baseButtonProps,
    setup: (props, { slots, emit }) => {
      const className = computed(() => [
        cnBaseButton({ size: props.size, round: props.round }),
        cnThemeButton({
          color: props.color,
          visual: props.visual,
          shadow: props.shadow
        })
      ])
      return () =>
        props.href ? (
          <a
            href={props.href}
            target={props.target}
            class={className.value}
            onClick={() => emit('click')}
            type={props.type}
          >
            {slots.default && slots.default()}
          </a>
        ) : props.to ? (
          <BaseLink
            to={props.to}
            class={className.value}
            onClick={() => emit('click')}
            type={props.type}
          >
            {slots.default && slots.default()}
          </BaseLink>
        ) : (
          <button
            class={className.value}
            onClick={() => emit('click')}
            type={props.type}
          >
            {slots.default && slots.default()}
          </button>
        )
    }
  })
)
