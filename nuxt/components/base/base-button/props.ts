import { PropOptions } from '@nuxtjs/composition-api'
import { Color, Visual } from '~/components/theme/theme-button'

export type Round = 's' | 'full'

export type Size = 's' | 'm' | 'l' | 'custom'

export type BaseButtonProps = {
  size: Size
  color?: Color
  visual?: Visual
  round?: Round
  shadow?: boolean
  type?: 'button' | 'submit'
}

export const baseButtonProps = {
  size: {
    required: true,
    type: String as () => Size
  },
  color: {
    default: Color.green,
    type: String as () => Color
  },
  visual: {
    default: null,
    type: String as () => Visual
  },
  round: {
    default: 'full',
    type: String as () => Round
  },
  shadow: {
    default: false,
    type: Boolean
  },
  type: {
    default: 'button',
    type: String as () => 'button' | 'submit'
  },
  to: {
    validator(value) {
      return typeof value === 'string' || typeof value === 'object'
    },
    default: null
  } as PropOptions<string | object>,
  target: {
    type: String,
    default: null
  },
  href: {
    type: String,
    default: null
  }
}
