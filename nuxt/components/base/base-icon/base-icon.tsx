import { defineComponent, computed } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './base-icon.scss?module'

const cn = useClassNames('base-icon', style)

const classes = {
  main: cn(),
  icon: cn('icon')
}

const proportions: { [key: string]: number } = {
  'chevron-left': 20 / 12,
  logo: 900 / (1320 - 190)
}

const useColorForStroke = [
  'arrow-left',
  'plus',
  'minus',
  'curve-arrow',
  'angle-down',
  'arrow-down'
]

export const BaseIcon = injectStyles(
  style,
  defineComponent({
    name: 'BaseIcon',
    props: {
      name: {
        required: true,
        type: String
      },
      rotate: {
        default: null,
        type: Number
      },
      width: {
        required: true,
        type: Number
      },
      height: {
        default: null,
        type: Number
      }
    },
    setup: (props, { emit }) => {
      const style = computed(() => {
        const style = {} as CSSStyleDeclaration

        if (useColorForStroke.includes(props.name)) {
          style.stroke = 'currentColor'
        } else {
          style.fill = 'currentColor'
        }
        if (props.rotate) {
          style.transform = `rotate(${props.rotate}deg)`
        }
        return style
      })

      const height = computed(() => {
        if (props.height) {
          return props.height
        }
        if (props.name in proportions) {
          return props.width * proportions[props.name]
        }
        return props.width
      })

      return () => {
        const componentData = (
          <svg-icon
            onClick={() => emit('click')}
            name={props.name}
            style={style.value}
            class={classes.icon}
            width={props.width + 'px'}
            height={height.value + 'px'}
          />
        )
        return <span class={classes.main}>{componentData}</span>
      }
    }
  })
)
