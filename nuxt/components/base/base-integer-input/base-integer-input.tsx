import { computed, defineComponent, ref, watch } from '@nuxtjs/composition-api'
import { clamp } from '~/support/utils/clamp'

export const BaseIntegerInput = defineComponent({
  name: 'BaseIntegerInput',
  props: {
    value: {
      type: Number,
      required: true
    },
    min: {
      type: Number,
      default: -Infinity
    },
    max: {
      type: Number,
      default: Infinity
    }
  },
  setup: (props, { emit }) => {
    const innerValue = ref<string>(String(props.value))
    const numberInnerValue = computed(() => {
      const value = parseInt(innerValue.value)
      return Number.isFinite(value) ? value : 0
    })
    watch(
      () => props.value,
      () => (innerValue.value = String(props.value))
    )
    watch(
      () => innerValue.value,
      () => {
        if (innerValue.value !== String(props.value)) {
          emit('input', numberInnerValue.value)
        }
      }
    )
    const onInput = (e: { target: HTMLInputElement }) => {
      const input = e.target.value
      if (input === '') {
        innerValue.value = ''
      } else {
        const value = parseInt(input)
        if (!Number.isFinite(value)) {
          innerValue.value = ''
        } else {
          innerValue.value = String(clamp(value, props.min, props.max))
          e.target.value = String(clamp(value, props.min, props.max))
        }
      }
    }

    const onChange = () => {
      emit('change', clamp(parseInt(innerValue.value), props.min, props.max))
    }

    const isNumber = (e: KeyboardEvent) => {
      const key = e.key
      if (
        (key >= '0' && key <= '9') ||
        key === 'Backspace' ||
        key === 'Enter' ||
        key === 'Delete' ||
        e.ctrlKey
      ) {
        return true
      }
      e.preventDefault()
    }

    return () => (
      <input
        type="text"
        onKeydown={isNumber}
        onInput={onInput}
        onChange={onChange}
        value={innerValue.value}
      />
    )
  }
})
