import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base/base-icon'
import style from './base-back-button.scss?module'

const cn = useClassNames('base-back-button', style)

const classes = {
  main: cn(),
  icon: cn('icon')
}

export const BaseBackButton = injectStyles(
  style,
  defineComponent({
    name: 'BaseBackButton',
    props: {},
    setup(props, { emit }) {
      return () => (
        <button onClick={() => emit('click')} class={classes.main}>
          <BaseIcon
            name={'arrow-left'}
            width={8}
            height={5}
            class={classes.icon}
          />
          <span>Назад</span>
        </button>
      )
    }
  })
)
