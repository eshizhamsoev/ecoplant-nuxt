import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './base-loading-ellipsis.scss?module'

const cn = useClassNames('base-loading-ellipsis', style)

const classes = {
  main: cn(),
  item: cn('item')
}

export const BaseLoadingEllipsis = injectStyles(
  style,
  defineComponent({
    name: 'BaseLoadingEllipsis',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <div class={classes.item} />
          <div class={classes.item} />
          <div class={classes.item} />
          <div class={classes.item} />
        </div>
      )
    }
  })
)
