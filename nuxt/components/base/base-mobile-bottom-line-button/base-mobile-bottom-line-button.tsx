import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import style from './base-mobile-bottom-line-button.scss?module'

const cn = useClassNames('base-mobile-bottom-line-button', style)

export const BaseMobileBottomLineButton = injectStyles(
  style,
  defineComponent({
    name: 'BaseMobileBottomLineButton',
    props: {
      icon: {
        required: true,
        type: String
      },
      text: {
        required: true,
        type: String
      }
    },
    setup: (props) => {
      return () => (
        <div class={cn()}>
          <BaseIcon
            class={cn('icon')}
            name={props.icon}
            height={20}
            width={20}
          />
          <span class={cn('text')}>{props.text}</span>
        </div>
      )
    }
  })
)
