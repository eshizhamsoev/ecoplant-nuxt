import { defineComponent } from '@nuxtjs/composition-api'
import { BasePicture } from '~/components/base/base-picture'

export const BaseWebpPicture = defineComponent({
  name: 'BaseWebpPicture',
  props: {
    image: {
      type: String,
      required: true
    },
    webpImage: {
      type: String,
      required: true
    },
    alt: {
      type: String,
      default: ''
    }
  },
  setup: (props) => {
    return () => (
      <BasePicture
        mainImage={props.image}
        images={[{ src: props.webpImage, type: 'image/webp' }]}
        alt={props.alt}
      />
    )
  }
})
