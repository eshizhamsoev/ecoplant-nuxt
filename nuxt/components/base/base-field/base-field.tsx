import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './base-field.scss?module'

const cn = useClassNames('base-field', style)

export const BaseField = injectStyles(
  style,
  defineComponent({
    props: {
      error: {
        type: Boolean,
        default: false
      }
    },
    setup: (props, { slots }) => {
      const className = computed(() => cn({ error: !!props.error }))
      return () => (
        <label class={className.value}>
          {slots.default && slots.default()}
        </label>
      )
    }
  })
)
