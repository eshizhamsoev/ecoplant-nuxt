import { defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import style from './base-client-gallery-control-bar.scss?module'

const cn = useClassNames('base-client-gallery-control-bar', style)

export const BaseClientGalleryControlBar = injectStyles(
  style,
  defineComponent({
    name: 'BaseClientGalleryControlBar',
    props: {
      smallIcons: {
        type: Boolean,
        default: false
      }
    },
    setup: (props, { refs }) => {
      const isLiked = ref<boolean>(false)

      const iconsWidth = props.smallIcons ? 22 : 28

      const addLike = () => {
        if (refs.like && refs.like instanceof Element) {
          isLiked.value = !isLiked.value
          if (isLiked.value) {
            refs.like.classList.add(cn('heart-button-active'))
            return
          }

          refs.like.classList.remove(cn('heart-button-active'))
        }
      }

      return () => (
        <div class={cn()}>
          <div class={cn('control-bar-left')}>
            <button
              ref="like"
              class={[cn('control-bar-item'), cn('heart-button')]}
              onClick={addLike}
            >
              <BaseIcon
                class={cn('heart-icon')}
                name={'images-feed/heart'}
                width={iconsWidth}
              />
            </button>
            <BaseIcon
              class={cn('control-bar-item')}
              name={'images-feed/comment'}
              width={iconsWidth}
            />
            <BaseIcon
              class={cn('control-bar-item')}
              name={'images-feed/send'}
              width={iconsWidth}
            />
          </div>
          <BaseIcon
            class={cn('control-bar-item')}
            name={'images-feed/bookmark'}
            width={iconsWidth}
          />
        </div>
      )
    }
  })
)
