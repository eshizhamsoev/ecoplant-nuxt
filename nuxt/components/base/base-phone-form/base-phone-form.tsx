import { defineComponent, ref } from '@nuxtjs/composition-api'
import { BasePhoneField } from '~/components/base'
import { BaseLoadingButton } from '~/components/base/base-loading-button/base-loading-button'
import { BaseFormResponse } from '~/components/base/base-form-response/base-form-response'
import { FORM_TYPES, useForm } from '~/support/hooks/useForm'
import { BaseButtonProps } from '~/components/base/base-button/props'
import { GTM_EVENTS } from '~/support/hooks/useReachGoal'

export const BasePhoneForm = defineComponent({
  name: 'BasePhoneForm',
  props: {
    mainClass: {
      type: String,
      default: ''
    },
    successClass: {
      type: String,
      default: ''
    },
    errorClass: {
      type: String,
      default: ''
    },
    phoneClass: {
      type: String,
      default: ''
    },
    buttonClass: {
      type: String,
      default: ''
    },
    buttonOptions: {
      type: Object as () => BaseButtonProps,
      required: true
    },
    source: {
      type: String,
      required: true
    },
    goal: {
      type: String as () => GTM_EVENTS,
      default: GTM_EVENTS.callBack
    },
    withIcon: {
      type: Boolean,
      default: true
    }
  },
  setup: (props, { slots, root }) => {
    const phone = ref('')
    const { processing, successMessage, fatalError, submit, errorFields } =
      useForm(root, FORM_TYPES.PHONE, { goal: props.goal })

    const onClick = () => {
      submit({
        phone: phone.value,
        source: props.source
      })
    }
    return () => (
      <BaseFormResponse
        mainClass={props.mainClass}
        fatalError={fatalError.value}
        successMessage={successMessage.value}
        successClass={props.successClass}
        errorClass={props.errorClass}
      >
        {slots.before && slots.before()}
        <BasePhoneField
          withIcon={props.withIcon}
          class={props.phoneClass}
          // @ts-ignore
          value={phone.value}
          onInput={(value: string) => {
            phone.value = value
          }}
          error={!!(errorFields.value && errorFields.value.includes('phone'))}
        />
        {slots.between && slots.between()}
        <BaseLoadingButton
          loading={processing.value}
          buttonOptions={props.buttonOptions}
          class={props.buttonClass}
          onClick={onClick}
        >
          {slots.button && slots.button()}
        </BaseLoadingButton>
        {slots.after && slots.after()}
      </BaseFormResponse>
    )
  }
})
