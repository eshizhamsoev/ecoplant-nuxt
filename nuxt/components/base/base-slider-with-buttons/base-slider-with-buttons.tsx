import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon, BaseTinySlider } from '~/components/base'
import { baseTinySliderProps } from '~/components/base/base-tiny-slider'
import { cnBaseRoundButton } from '~/components/base/base-round-button'
import { cnThemeButton, Color } from '~/components/theme/theme-button'
import style from './base-slider-with-buttons.scss?module'

const cn = useClassNames('base-slider-with-buttons', style)

const classes = {
  main: cn(),
  icon: [cnBaseRoundButton(), cnThemeButton({ color: Color.green })]
}

export const BaseSliderWithButtons = injectStyles(
  style,
  defineComponent({
    name: 'BaseSliderWithButtons',
    props: {
      ...baseTinySliderProps,
      buttonSize: {
        type: Number,
        default: 60
      }
    },
    setup: (props, { emit, slots }) => {
      const style = computed(() => ({
        width: props.buttonSize + 'px',
        height: props.buttonSize + 'px',
        marginTop: `-${props.buttonSize / 2}px`
      }))
      const iconSize = computed(() => Math.floor(props.buttonSize / 5))
      return () => (
        <BaseTinySlider
          options={props.options}
          class={cn()}
          currentIndex={props.currentIndex}
          onChange={(newIndex: number) => emit('change', newIndex)}
          onClick={(slideNumber: number) => emit('click', slideNumber)}
          prevClass={[cn('button'), props.prevClass].join(' ')}
          nextClass={[cn('button'), props.nextClass].join(' ')}
        >
          {slots.default && slots.default()}
          <template slot="prev">
            <span class={cn('prev')}>
              <span style={style.value} class={classes.icon}>
                <BaseIcon name="chevron-left" width={iconSize.value} />
              </span>
            </span>
          </template>
          <template slot="next">
            <span class={cn('next')}>
              <span style={style.value} class={classes.icon}>
                <BaseIcon
                  name="chevron-left"
                  rotate={180}
                  width={iconSize.value}
                />
              </span>
            </span>
          </template>
        </BaseTinySlider>
      )
    }
  })
)
