import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './base-badged-text.scss?module'

const cn = useClassNames('base-badged-text', style)

const classes = {
  main: cn(),
  line: cn('line'),
  text: cn('text')
}

export const BaseBadgedText = injectStyles(
  style,
  defineComponent({
    name: 'BaseBadgedText',
    props: {
      lines: {
        type: Array as () => string[],
        required: true
      },
      background: {
        type: String,
        default: '#fff'
      },
      offset: {
        type: String,
        default: '0.45em'
      },
      horizontalOffset: {
        type: String,
        default: ''
      },
      leftOffset: {
        type: String,
        default: ''
      },
      borderRadius: {
        type: String,
        default: '0.3em'
      }
    },
    setup: (props) => {
      const lineStyle = {
        paddingTop: props.offset,
        paddingBottom: props.offset,
        paddingLeft: props.leftOffset || props.horizontalOffset || props.offset,
        paddingRight: props.horizontalOffset || props.offset,
        marginTop: `-${props.offset}`,
        marginBottom: `-${props.offset}`,
        background: props.background,
        borderRadius: props.borderRadius
      }
      return () => (
        <div class={classes.main}>
          {props.lines.map((text) => (
            <div class={classes.line} style={lineStyle}>
              <span class={classes.text}>{text}</span>
            </div>
          ))}
        </div>
      )
    }
  })
)
