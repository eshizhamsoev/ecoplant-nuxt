import { defineComponent, ref } from '@nuxtjs/composition-api'
import { FORM_TYPES, useForm } from '~/support/hooks/useForm'
import { BaseFormResponse } from '~/components/base/base-form-response/base-form-response'
import { BaseField } from '~/components/base'
import { BaseLoadingButton } from '~/components/base/base-loading-button/base-loading-button'
import { BaseButtonProps } from '~/components/base/base-button/props'
import { GTM_EVENTS } from '~/support/hooks/useReachGoal'

export const BaseEmailForm = defineComponent({
  name: 'BaseEmailForm',
  props: {
    mainClass: {
      type: String,
      default: ''
    },
    successClass: {
      type: String,
      default: ''
    },
    errorClass: {
      type: String,
      default: ''
    },
    fieldClass: {
      type: String,
      default: ''
    },
    buttonClass: {
      type: String,
      default: ''
    },
    buttonOptions: {
      type: Object as () => BaseButtonProps,
      required: true
    },
    source: {
      type: String,
      required: true
    },
    goal: {
      type: String as () => GTM_EVENTS,
      default: GTM_EVENTS.mailBack
    }
  },
  setup: (props, { slots, root }) => {
    const email = ref('')
    const { processing, successMessage, fatalError, submit, errorFields } =
      useForm(root, FORM_TYPES.EMAIL, { goal: props.goal })

    const onClick = () => {
      submit({
        email: email.value,
        source: props.source
      })
    }

    function onInput(event: InputEvent & { target?: HTMLInputElement }) {
      email.value = event.target?.value
    }
    return () => (
      <BaseFormResponse
        mainClass={props.mainClass}
        fatalError={fatalError.value}
        successMessage={successMessage.value}
        successClass={props.successClass}
        errorClass={props.errorClass}
      >
        {slots.before && slots.before()}
        <BaseField
          error={!!(errorFields.value && errorFields.value.includes('email'))}
          class={props.fieldClass}
        >
          <input
            type="email"
            value={email.value}
            onInput={onInput}
            placeholder="Ваш email"
          />
        </BaseField>
        {slots.between && slots.between()}
        <BaseLoadingButton
          loading={processing.value}
          buttonOptions={props.buttonOptions}
          class={props.buttonClass}
          onClick={onClick}
        >
          {slots.button && slots.button()}
        </BaseLoadingButton>
        {slots.after && slots.after()}
      </BaseFormResponse>
    )
  }
})
