import { computed, defineComponent, ref, watch } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import style from './base-checkbox.scss?module'

const cn = useClassNames('base-checkbox', style)

export const BaseCheckbox = injectStyles(
  style,
  defineComponent({
    name: 'BaseCheckbox',
    model: {
      prop: 'value',
      event: 'input'
    },
    props: {
      value: {
        type: Boolean,
        required: true
      },
      size: {
        type: Number,
        required: true
      },
      name: {
        type: String,
        default: null
      },
      inputId: {
        type: String,
        default: null
      }
    },
    setup: (props, { emit }) => {
      const innerValue = ref(props.value)

      watch(
        () => innerValue.value,
        () => {
          if (innerValue.value !== props.value) {
            emit('input', innerValue.value)
          }
        }
      )

      watch(
        () => props.value,
        () => {
          innerValue.value = props.value
        }
      )

      const style = computed(() => ({
        width: props.size + 'px',
        height: props.size + 'px'
      }))

      const iconSize = computed(() => Math.floor(props.size / 1.4))

      const change = (e: { target: HTMLInputElement }) => {
        innerValue.value = Boolean(e.target.checked)
      }

      return () => (
        <label class={cn({ active: innerValue.value })} style={style.value}>
          <BaseIcon name="check" width={iconSize.value} class={cn('icon')} />
          <input
            class={cn('input')}
            type="checkbox"
            name={props.name}
            id={props.inputId}
            checked={innerValue.value}
            onChange={change}
          />
        </label>
      )
    }
  })
)
