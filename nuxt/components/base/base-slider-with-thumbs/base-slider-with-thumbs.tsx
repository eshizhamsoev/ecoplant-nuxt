import { computed, defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePopupGallery, BaseSliderWithCutButtons } from '~/components/base'
import { usePopupGallery } from '~/components/base/base-popup-gallery/usePopupGallery'
import style from './base-slider-with-thumbs.scss?module'

const cn = useClassNames('base-slider-with-thumbs', style)

const classes = {
  main: cn(),
  slider: cn('slider'),
  sliderImage: cn('slider-image'),
  thumbs: cn('thumbs'),
  thumb: cn('thumb'),
  thumbImage: cn('thumb-image')
}

const options = {
  lazyload: true
}

type Image = {
  small: string
  large: string
  full?: string
}

type InputImage = string | Image

type ImageSize = {
  readonly width: number
  readonly height: number
}

export const BaseSliderWithThumbs = injectStyles(
  style,
  defineComponent({
    name: 'BaseSliderWithThumbs',
    props: {
      images: {
        type: Array as () => InputImage[],
        required: true
      },
      sliderSize: {
        type: Object as () => ImageSize,
        required: true
      },
      thumbSize: {
        type: Object as () => ImageSize,
        required: true
      }
    },
    setup: (props) => {
      const index = ref(0)
      const sliderImages = computed(() =>
        props.images.map((image) =>
          typeof image === 'string' ? image : image.large
        )
      )
      const thumbImages = computed(() =>
        props.images
          .slice(0, 3)
          .map((image) => (typeof image === 'string' ? image : image.small))
      )
      const popupImages = computed(() =>
        props.images.map((image) => ({
          small: typeof image === 'string' ? image : image.small,
          large: typeof image === 'string' ? image : image.full || image.large
        }))
      )
      const updateIndex = (newIndex: number) => {
        index.value = newIndex
      }
      const {
        open: openPopupGallery,
        close: closePopupGallery,
        index: popupIndex
      } = usePopupGallery()
      return () =>
        props.images.length && (
          <div class={classes.main}>
            <div
              style={{ width: `${props.sliderSize.width}px` }}
              class={classes.slider}
            >
              <BaseSliderWithCutButtons
                options={options}
                currentIndex={index.value}
                onChange={(index: number) => updateIndex(index)}
                onClick={(index: number) => openPopupGallery(index)}
              >
                {sliderImages.value.map((image) => (
                  <div>
                    <img
                      data-src={image}
                      alt=""
                      class={['tns-lazy-img', classes.sliderImage]}
                      width={props.sliderSize.width}
                      height={props.sliderSize.height}
                    />
                  </div>
                ))}
              </BaseSliderWithCutButtons>
            </div>
            <div class={classes.thumbs}>
              {thumbImages.value.map((image, index) => (
                <button
                  key={image}
                  class={classes.thumb}
                  onClick={() => updateIndex(index)}
                >
                  <img
                    src={image}
                    alt=""
                    class={classes.thumbImage}
                    width={props.thumbSize.width}
                    height={props.thumbSize.height}
                  />
                </button>
              ))}
            </div>
            <BasePopupGallery
              images={popupImages.value}
              index={popupIndex.value}
              show-thumbnails={true}
              onClose={closePopupGallery}
            />
          </div>
        )
    }
  })
)
