import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { baseGridProps } from '~/components/base/base-grid/props'
import style from './base-grid.scss?module'

const cn = useClassNames('base-grid', style)

export const BaseGrid = injectStyles(
  style,
  defineComponent({
    name: 'BaseGrid',
    props: baseGridProps,
    setup: (props, { slots }) => {
      const options = computed(() => {
        const obj = {} as { [key: string]: number }
        Object.entries(props).forEach(([size, value]) => {
          if (typeof value === 'number') {
            obj[size] = value
          }
        })
        return obj
      })
      return () => (
        <div class={cn(options.value)}>{slots.default && slots.default()}</div>
      )
    }
  })
)
