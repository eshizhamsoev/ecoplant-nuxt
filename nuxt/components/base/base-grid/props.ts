export const baseGridProps = {
  xs: {
    type: Number,
    default: 2
  },
  sm: {
    type: Number,
    default: null
  },
  md: {
    type: Number,
    default: null
  },
  lg: {
    type: Number,
    default: null
  },
  xl: {
    type: Number,
    default: null
  }
}
