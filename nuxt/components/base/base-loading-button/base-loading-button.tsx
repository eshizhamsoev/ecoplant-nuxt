import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton } from '~/components/base'

import { BaseButtonProps } from '~/components/base/base-button/props'
import { BaseLoadingEllipsis } from '~/components/base/base-loading-ellipsis'
import style from './base-loading-button.scss?module'

const cn = useClassNames('base-loading-button', style)

const classes = {
  main: cn(),
  loading: cn('loading'),
  ellipsis: cn('ellipsis')
}

export const BaseLoadingButton = injectStyles(
  style,
  defineComponent({
    name: 'BaseLoadingButton',
    props: {
      buttonOptions: {
        type: Object as () => BaseButtonProps,
        required: true
      },
      loading: {
        type: Boolean,
        required: true
      }
    },
    setup: (props, { slots, emit }) => {
      return () => (
        <BaseButton
          props={props.buttonOptions}
          class={classes.main}
          onClick={() => emit('click')}
        >
          {slots.default && slots.default()}
          {props.loading && <BaseLoadingEllipsis class={classes.ellipsis} />}
        </BaseButton>
      )
    }
  })
)
