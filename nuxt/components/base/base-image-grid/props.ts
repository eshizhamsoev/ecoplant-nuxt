import { baseGridProps } from '~/components/base/base-grid'
import { ServerImageWithSize } from '~/common-types/server/common/image'

type Image =
  | string
  | {
      small: string
      large: string
      title?: string
    }
  | {
      image: string
      title?: string
    }
  | {
      image: ServerImageWithSize
      title?: string
    }
  | {
      thumb: ServerImageWithSize
      large: ServerImageWithSize
      title?: string | null
    }

export const baseImageGridProps = {
  images: {
    type: Array as () => readonly Image[],
    required: true as true
  },
  itemClass: {
    type: String,
    default: null
  },
  popupAvailable: {
    type: Boolean,
    default: false
  },
  ...baseGridProps
}
