import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseGrid, baseGridProps } from '~/components/base/base-grid'
import {
  BasePopupGalleryImage,
  BasePopupGallery
} from '~/components/base/base-popup-gallery'

import { baseImageGridProps } from '~/components/base/base-image-grid/props'

import { usePopupGallery } from '~/components/base/base-popup-gallery/usePopupGallery'
import style from './base-image-grid.scss?module'

const cn = useClassNames('base-image-grid', style)

const baseGridPropNames = Object.keys(baseGridProps)

const classes = {
  main: cn(),
  grid: cn('grid'),
  item: cn('item'),
  image: cn('image'),
  text: cn('text')
}

export const BaseImageGrid = injectStyles(
  style,
  defineComponent({
    name: 'BaseImageGrid',
    props: baseImageGridProps,
    setup: (props, { root }) => {
      const gridProps = computed(() => {
        const obj = {} as { [key: string]: number }
        for (const [key, value] of Object.entries(props)) {
          if (baseGridPropNames.includes(key)) {
            if (typeof value === 'number') {
              obj[key] = value
            }
          }
        }
        return obj
      })

      const items = computed(() =>
        props.images.map((image) => {
          if (typeof image === 'string') {
            return {
              image,
              title: null
            }
          }
          if ('image' in image) {
            return {
              image: image.image,
              title: image.title
            }
          } else if ('thumb' in image) {
            return {
              image: image.thumb,
              title: image.title
            }
          } else {
            return {
              image: image.small,
              title: image.title
            }
          }
        })
      )

      const galleryImages = computed<BasePopupGalleryImage[]>(() =>
        props.images.map((image) => {
          const obj = {} as BasePopupGalleryImage
          if (typeof image === 'string') {
            obj.small = image
            obj.large = image
          } else if ('image' in image) {
            if (typeof image.image === 'object') {
              obj.small = image.image.src
              obj.large = image.image.src
            } else {
              obj.small = image.image
              obj.large = image.image
            }
            obj.title = image.title
          } else if ('small' in image) {
            obj.small = image.small
            obj.large = image.large
            obj.title = image.title
          } else if ('thumb' in image) {
            obj.small = image.thumb.src
            obj.large = image.large.src
            obj.title = image.title ?? ''
          }
          return obj
        })
      )

      const {
        open: openPopupGallery,
        close: closePopupGallery,
        index: popupIndex
      } = usePopupGallery()

      return () => (
        <div class={cn({ 'with-popup': props.popupAvailable })}>
          <BaseGrid props={gridProps.value} class={classes.grid}>
            {items.value.map((item, index) => (
              <div class={classes.item}>
                {typeof item.image === 'object' ? (
                  <img
                    src={item.image.src}
                    width={item.image.width}
                    height={item.image.height}
                    alt=""
                    class={[classes.image, props.itemClass]}
                    onClick={openPopupGallery.bind(null, index)}
                    loading={'lazy'}
                  />
                ) : (
                  <img
                    src={item.image}
                    alt=""
                    class={[classes.image, props.itemClass]}
                    onClick={openPopupGallery.bind(null, index)}
                    loading={'lazy'}
                  />
                )}
                {item.title && (
                  <div
                    domProps={{ innerHTML: item.title }}
                    class={classes.text}
                  />
                )}
              </div>
            ))}
          </BaseGrid>
          {props.popupAvailable && (
            <BasePopupGallery
              images={galleryImages.value}
              index={popupIndex.value}
              onClose={closePopupGallery}
              showThumbnails={root.$device.isDesktop}
            />
          )}
        </div>
      )
    }
  })
)
