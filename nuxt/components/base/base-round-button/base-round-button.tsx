import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { cnThemeButton } from '~/components/theme/theme-button'
import { injectStyles } from '~/support/utils/injectStyle'
import { baseRoundButtonProps } from '~/components/base/base-round-button/props'
import style from './base-round-button.scss?module'

export const cnBaseRoundButton = useClassNames('base-round-button', style)

export const BaseRoundButton = injectStyles(
  style,
  defineComponent({
    props: baseRoundButtonProps,
    setup: ({ size, color, visual, shadow }, { slots, emit }) => {
      const className = computed(() => [
        cnBaseRoundButton(typeof size === 'string' ? { size } : {}),
        cnThemeButton({ color, visual, shadow })
      ])
      // TODO refactor this to set size via css
      const style = computed(() => {
        if (typeof size === 'string') {
          return {}
        }
        return {
          width: size + 'px',
          height: size + 'px'
        }
      })
      return () => (
        <button
          class={className.value}
          onClick={() => emit('click')}
          style={style.value}
        >
          {slots.default && slots.default()}
        </button>
      )
    }
  })
)
