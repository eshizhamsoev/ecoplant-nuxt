import { Color, Visual } from '~/components/theme/theme-button'

export type SizeProp = number | 'ss' | 's' | 'm' | 'l'
export const sizeProp = {
  default: 'm' as SizeProp,
  validator(v: any) {
    return typeof v === 'number' || typeof v === 'string'
  }
}

export const baseRoundButtonProps = {
  size: sizeProp,
  color: {
    default: Color.green,
    type: String as () => Color
  },
  visual: {
    default: null,
    type: String as () => Visual
  },
  shadow: {
    default: false,
    type: Boolean
  }
}

export type BaseRoundButtonProps = {
  size: SizeProp
  color: Color
  visual: Visual
  shadow: boolean
}
