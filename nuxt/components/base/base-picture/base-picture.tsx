import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './base-picture.scss?module'

const cn = useClassNames('base-picture', style)

type Image = {
  src: string
  type?: string
}

const classes = {
  main: cn(),
  image: cn('image')
}

export const BasePicture = injectStyles(
  style,
  defineComponent({
    name: 'BasePicture',
    props: {
      mainImage: {
        type: String,
        required: true
      },
      images: {
        type: Array as () => Image[],
        required: true
      },
      alt: {
        type: String,
        default: ''
      }
    },
    setup: (props) => {
      return () => (
        <picture class={classes.main}>
          {props.images.map((image) => (
            <source srcset={image.src} type={image.type} />
          ))}
          <img src={props.mainImage} alt={props.alt} class={classes.image} />
        </picture>
      )
    }
  })
)
