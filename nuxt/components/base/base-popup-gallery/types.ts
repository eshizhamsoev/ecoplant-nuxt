export type BasePopupGalleryImage = {
  small: string
  large: string
  title?: string
  largeSizes?: {
    height: number
    width: number
  }
}
