import { defineComponent, ref, watch } from '@nuxtjs/composition-api'
import { basePopupGalleryProps } from './props'
import { lockScroll, clearAllScrollLocks } from '~/support/utils/scroll-lock'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseTinySlider } from '~/components/base/base-tiny-slider'
import { BaseLazyLoadImage } from '~/components/base/base-lazy-load-image'
import { preloadImage } from '~/support/utils/preloadImage'
import { BaseClientImage } from '~/components/base/base-client-image/base-client-image'
import style from './base-popup-gallery.scss?module'

const cn = useClassNames('base-popup-gallery', style)

const classes = {
  main: cn(),
  overlay: cn('overlay'),
  close: cn('close'),
  prev: cn('prev'),
  next: cn('next'),
  mainWrapper: cn('main-wrapper'),
  mainSlider: cn('main-slider'),
  mainItem: cn('main-item'),
  mainItemCentered: cn('main-item-centered'),
  mainItemInner: cn('main-item-inner'),
  mainTitle: cn('main-title'),
  mainImage: cn('main-image'),
  mainImageCentered: cn('main-image-centered'),
  thumbnailsWrapper: cn('thumbnails-wrapper'),
  thumbnailsSlider: cn('thumbnails-slider'),
  thumbnailsItem: cn('thumbnails-item'),
  thumbnailsImage: cn('thumbnails-image')
}

export const BasePopupGallery = injectStyles(
  style,
  defineComponent({
    name: 'BasePopupGallery',
    props: basePopupGalleryProps,
    setup: (props, { emit, root }) => {
      const imgIndex = ref<number | null>(props.index)

      if (!root.$isServer) {
        watch(
          () => imgIndex.value,
          (newIndex) => {
            if (newIndex !== null) {
              preloadImage(
                props.images[(newIndex + 1) % props.images.length].large
              )
              preloadImage(
                props.images[(newIndex || props.images.length) - 1].large
              )
            }
          }
        )
      }
      watch(
        () => props.index,
        (newIndex) => {
          imgIndex.value = newIndex
          if (newIndex !== null) {
            root.$nextTick(() => {
              lockScroll(root.$el)
            })
          } else {
            clearAllScrollLocks()
          }
        }
      )

      const close = (e: { target: HTMLInputElement }) => {
        if (
          !e.target.closest('.' + classes.mainImage) &&
          !e.target.closest('.' + classes.prev) &&
          !e.target.closest('.' + classes.next) &&
          !e.target.closest('.' + classes.thumbnailsWrapper)
        ) {
          emit('close')
        }
      }

      return () =>
        imgIndex.value === null ? null : (
          <div
            class={cn({ 'show-thumbnails': props.showThumbnails })}
            onClick={close}
          >
            <button class={classes.close} type="button">
              &times;
            </button>
            <div class={classes.mainWrapper}>
              <BaseTinySlider
                hideDuringInitiating={true}
                currentIndex={imgIndex.value}
                onChange={(newIndex: number) => {
                  imgIndex.value = newIndex
                }}
                options={{ lazyload: true }}
                class={classes.mainSlider}
                prevClass={classes.prev}
                nextClass={classes.next}
              >
                <template slot="prev">&lsaquo;</template>
                <template slot="next">&rsaquo;</template>
                {props.isInstaLookalike
                  ? props.images.map((image) => (
                      <div class={classes.mainItemCentered}>
                        <BaseClientImage
                          class={classes.mainImage}
                          image={image.large}
                          text={image.title}
                          height={image.largeSizes!.height}
                          width={image.largeSizes!.width}
                        />
                      </div>
                    ))
                  : props.images.map((image) => (
                      <div class={classes.mainItem}>
                        <div class={classes.mainItemInner}>
                          <img
                            data-src={image.large}
                            class={['tns-lazy-img', classes.mainImage]}
                            alt={''}
                          />
                          {image.title && (
                            <div
                              class={classes.mainTitle}
                              domProps={{ innerHTML: image.title }}
                            />
                          )}
                        </div>
                      </div>
                    ))}
              </BaseTinySlider>
            </div>
            {props.showThumbnails && (
              <div class={classes.thumbnailsWrapper}>
                {props.staticThumbnails ? (
                  <div class={cn('static-thumbnails')}>
                    {props.images.map((image, i) => (
                      <BaseLazyLoadImage
                        src={image.small}
                        width={100}
                        height={100}
                        onClick={() => (imgIndex.value = i)}
                        class={[
                          cn('static-thumbnail', {
                            selected: imgIndex.value === i
                          })
                        ]}
                      />
                    ))}
                  </div>
                ) : (
                  <BaseTinySlider
                    hideDuringInitiating={true}
                    class={classes.thumbnailsSlider}
                    options={{
                      lazyload: true,
                      fixedWidth: 100,
                      nav: false,
                      controls: false,
                      center: true,
                      loop: true,
                      gutter: 20
                    }}
                    currentIndex={imgIndex.value}
                    onClick={(newIndex: number) => (imgIndex.value = newIndex)}
                  >
                    {props.images.map((image) => (
                      <div class={classes.thumbnailsItem}>
                        <img
                          data-src={image.small}
                          class={['tns-lazy-img', cn('thumbnails-image')]}
                        />
                      </div>
                    ))}
                  </BaseTinySlider>
                )}
              </div>
            )}
          </div>
        )
    }
  })
)
