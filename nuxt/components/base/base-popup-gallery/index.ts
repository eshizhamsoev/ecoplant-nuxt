import { basePopupGalleryProps } from './props'
import { BasePopupGalleryImage } from './types'
export { BasePopupGallery } from './base-popup-gallery'

export { basePopupGalleryProps, BasePopupGalleryImage }
