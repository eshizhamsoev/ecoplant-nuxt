import { ref } from '@nuxtjs/composition-api'

export const usePopupGallery = () => {
  const index = ref<number | null>(null)
  return {
    index,
    close: () => {
      index.value = null
    },
    open: (newIndex: number) => {
      index.value = newIndex
    }
  }
}
