import { PropOptions } from '@nuxtjs/composition-api'
import { BasePopupGalleryImage } from './types'

export const basePopupGalleryProps = {
  images: {
    type: Array as () => readonly BasePopupGalleryImage[],
    required: true as true
  },
  index: {
    validator: (prop: any) => typeof prop === 'number' || prop === null,
    required: true,
    default: null
  } as PropOptions<null | number> & { required: true },
  showThumbnails: {
    type: Boolean,
    default: true
  },
  staticThumbnails: {
    type: Boolean,
    default: false
  },
  isInstaLookalike: {
    type: Boolean,
    default: false
  }
}
