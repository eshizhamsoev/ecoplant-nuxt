import { PhoneData } from '~/store/contacts'

export type PhoneSlotData = {
  phone: PhoneData
}
