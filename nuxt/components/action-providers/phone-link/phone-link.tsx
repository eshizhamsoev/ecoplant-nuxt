import { computed, defineComponent } from '@nuxtjs/composition-api'
import { GTM_EVENTS, useReachGoal } from '~/support/hooks/useReachGoal'

export const PhoneLink = defineComponent({
  name: 'PhoneLink',
  props: {
    isAdditional: {
      type: Boolean,
      default: false
    }
  },
  setup: (props, { root, slots, emit }) => {
    const phone = computed(() =>
      props.isAdditional
        ? root.$accessor.contacts.additionalContacts.phone
        : root.$accessor.contacts.phone
    )
    const link = computed(() => 'tel:' + phone.value.number)
    const reachGoal = useReachGoal()
    const click = () => {
      emit('click')
      reachGoal(GTM_EVENTS.phoneClick)
    }
    return () => (
      <a href={link.value} onClick={click}>
        {(slots.default && slots.default({ phone: phone.value })) ||
          phone.value.text}
      </a>
    )
  }
})
