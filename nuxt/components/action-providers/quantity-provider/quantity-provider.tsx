import { computed, defineComponent, ref, watch } from '@nuxtjs/composition-api'
import { clamp } from '~/support/utils/clamp'

export type QuantityProviderSlotData = {
  canMinus: boolean
  value: number
  minus: () => void
  plus: () => void
  changeValue: (number: number) => void
}

const maxAmount: number = 99999

export const QuantityProvider = defineComponent({
  name: 'QuantityProvider',
  props: {
    value: {
      type: Number,
      required: true
    },
    minimum: {
      type: Number,
      default: -Infinity
    }
  },
  setup: (props, { slots, emit }) => {
    const innerValue = ref(props.value)
    watch(
      () => innerValue.value,
      (newValue) => {
        if (newValue !== props.value) {
          emit('input', newValue)
        }
      }
    )
    watch(
      () => props.value,
      (newValue) => {
        innerValue.value = newValue
      }
    )
    const canMinus = computed(() => innerValue.value > props.minimum)
    const minus = () => {
      changeValue(canMinus.value ? innerValue.value - 1 : props.minimum)
    }
    const plus = () => {
      changeValue(innerValue.value + 1)
    }
    const changeValue = (value: number) => {
      innerValue.value = clamp(value, props.minimum, maxAmount)
    }
    return () =>
      slots.default &&
      slots.default({
        plus,
        minus,
        canMinus: canMinus.value,
        value: innerValue.value,
        changeValue
      } as QuantityProviderSlotData)
  }
})
