import { defineComponent, provide } from '@nuxtjs/composition-api'
import { submitFormActionSymbol } from '~/components/base/base-form'
import { FORM_TYPES, submitForm } from '~/support/hooks/useForm'
import { useCrm } from '~/support/hooks/useCrm'

export const ApiFormSubmitProvider = defineComponent({
  name: 'ApiFormSubmitProvider',
  props: {
    formId: {
      type: Number as () => FORM_TYPES,
      required: true
    }
  },
  setup: (props, { slots, emit, root }) => {
    const sendToCrm = useCrm(root)
    provide(submitFormActionSymbol, (formData) => {
      emit('beforeSend', formData)
      const promise = submitForm(props.formId, formData)
      promise.then(
        (data) => {
          sendToCrm(formData, data)
          emit('success', data)
        },
        (err) => {
          emit('error', err)
        }
      )
      return promise
    })
    return () => slots.default && slots.default()
  }
})
