import { computed, defineComponent } from '@nuxtjs/composition-api'
import { GTM_EVENTS, useReachGoal } from '~/support/hooks/useReachGoal'

export const EmailLink = defineComponent({
  name: 'EmailLink',
  props: {
    email: {
      type: String,
      default: null
    }
  },
  setup: (props, { root, slots }) => {
    const email = computed(() =>
      props.email ? props.email : root.$accessor.contacts.email
    )
    const link = computed(() => 'mailto:' + email.value)
    const reachGoal = useReachGoal()
    const click = () => {
      reachGoal(GTM_EVENTS.emailClick)
    }
    return () => (
      <a href={link.value} onClick={click}>
        {(slots.default && slots.default({ email: email.value })) ||
          email.value}
      </a>
    )
  }
})
