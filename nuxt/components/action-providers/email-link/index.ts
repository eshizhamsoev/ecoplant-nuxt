import { EmailLink } from './email-link'
import { EmailSlotData } from './types'

export { EmailSlotData, EmailLink }
