import { useClassNames } from '~/support/utils/bem-classnames'
import style from './pattern-icon-plus.scss?module'

style.__inject__ && style.__inject__()

export const enum VerticalAlign {
  top = 'top',
  center = 'center',
  bottom = 'bottom'
}

export const enum Indent {
  s = 's',
  m = 'm',
  l = 'l',
  xl = 'xl',
  xxl = 'xxl'
}

type PatternIconPlusProps = {
  'vertical-align': VerticalAlign
}

type PatternIconPlusIconProps = {
  indent: Indent
}

const cn = useClassNames('pattern-icon-plus', style)

export const cnPatternIconPlus = cn as (mods?: PatternIconPlusProps) => string

export const cnPatternIconPlusIcon = (mods?: PatternIconPlusIconProps) =>
  cn('icon', mods)
