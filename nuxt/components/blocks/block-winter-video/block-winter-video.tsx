import { defineComponent, ref } from '@nuxtjs/composition-api'
import preview from './images/preview.jpg'
import snowflake from './images/snowflake.png'
import flower from './images/flower.png'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton, BaseIcon, BaseRoundButton } from '~/components/base'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { GTM_EVENTS } from '~/support/hooks/useReachGoal'
import style from './block-winter-video.scss?module'

const video = 'https://ecoplant-pitomnik.ru/files/videos/winter-warehouse.mp4'
const cn = useClassNames('block-winter-video', style)

export const BlockWinterVideo = injectStyles(
  style,
  defineComponent({
    name: 'BlockFeaturedProducts',
    props: {},
    setup: (_, { refs }) => {
      const modalOpener = useModalOpener()

      const callbackClick = () => {
        modalOpener({
          name: 'callback',
          goal: GTM_EVENTS.bookForm
        })
      }

      const hidePreview = ref(false)
      const startVideo = () => {
        hidePreview.value = true
        const video = refs.video
        if (video instanceof HTMLVideoElement) {
          video.play()
          video.controls = true
        }
      }
      return () => (
        <div class={cn()}>
          <div class={cn('flowers-container')}>
            <img src={flower} class={cn('flower')} alt="" />
            <img src={flower} class={cn('flower', { blurred: true })} alt="" />
          </div>
          <div class={cn('container')}>
            <div class={cn('header')}>
              На улице зима, но вы можете посетить
              <br />
              <span class={cn('green-text')}>
                наш оптовый склад летних растений
              </span>
            </div>
            <div class={cn('video-wrapper')}>
              <video
                ref={'video'}
                preload="none"
                class={cn('video')}
                src={video}
                poster={preview}
              />
              <BaseRoundButton
                onClick={startVideo}
                size={120}
                class={cn('play-button', { hidden: hidePreview.value })}
              >
                <BaseIcon
                  width={29}
                  height={29}
                  name="play"
                  class={cn('icon')}
                />
              </BaseRoundButton>
            </div>
            <div class={cn('button-wrapper')}>
              <BaseButton onClick={callbackClick} size={'m'}>
                Забронировать растения
              </BaseButton>
            </div>
          </div>
          <img
            src={snowflake}
            class={cn('snowflake', { position: 'left' })}
            alt=""
          />
          <img
            src={snowflake}
            class={cn('snowflake', { position: 'right' })}
            alt=""
          />
        </div>
      )
    }
  })
)
