import { computed, defineComponent } from '@nuxtjs/composition-api'
import { MobilePlantingServiceButton } from './mobile-planting-service-button'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
// @ts-ignore
import standardImage from '~/components/blocks/block-planting/resources/standard.jpg?resize&size=90'
// @ts-ignore
import guaranteeImage from '~/components/blocks/block-planting/resources/guarantee.jpg?resize&size=90'
import { useIntersectedOnce } from '~/support/hooks/useIntersectedOnce'
import { BaseVideoPoster } from '~/components/base/base-video-poster/base-video-poster'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './mobile-block-planting.scss?module'

const cn = useClassNames('mobile-block-planting', style)

const classes = {
  main: cn(),
  firstLine: cn('first-line'),
  left: cn('left'),
  heading: cn('heading'),
  beforeHeading: cn('before-heading'),
  video: cn('video'),
  buttons: cn('buttons'),
  button: cn('button'),
  info: cn('info')
}

export const MobileBlockPlanting = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockPlanting',
    setup(_, { root, refs }) {
      const modalOpener = useModalOpener()

      const openStandardModal = () =>
        modalOpener({
          name: 'planting-standard'
        })

      const openGuaranteeModal = () =>
        modalOpener({
          name: 'planting-guarantee'
        })

      const planting = computed(() => root.$accessor.planting)
      const intersected = useIntersectedOnce(() => refs.main as Element)
      return () => (
        <div class={classes.main} ref="main">
          <div class={classes.firstLine}>
            <div class={classes.left}>
              <div class={classes.beforeHeading}>Посадка</div>
              <div class={classes.heading}>
                Привезем и посадим <br />
                <b>в течение 1 дня</b>
              </div>
            </div>
            {planting.value && (
              // <BaseVideoBlockWithButton
              //   class={classes.video}
              //   classButtonText={cn('video-button-text')}
              //   video={planting.value.video}
              //   poster={intersected.value ? planting.value.poster : ''}
              //   button-text={planting.value.buttonText}
              // />
              <BaseVideoPoster
                class={classes.video}
                classButtonText={cn('video-button-text')}
                videoUrl={planting.value.video}
                poster={intersected.value ? planting.value.poster : ''}
                button-text={planting.value.buttonText}
              />
            )}
          </div>
          <div class={classes.info}>
            Нажмите на один из способов посадки <br />
            для того, чтобы узнать о нем подробнее
          </div>
          <div class={classes.buttons}>
            <MobilePlantingServiceButton
              onClick={openStandardModal}
              class={classes.button}
              image={standardImage.src}
              heading="Стандартная"
              percent={30}
            />
            <MobilePlantingServiceButton
              onClick={openGuaranteeModal}
              class={classes.button}
              image={guaranteeImage.src}
              heading="Гарантийная"
              percent={50}
            />
          </div>
        </div>
      )
    }
  })
)
