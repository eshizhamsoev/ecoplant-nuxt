import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import ArrowDownImage from '~/components/blocks/block-planting/desktop/planting-service-button/resources/arrow-down.svg'
import { BaseButton } from '~/components/base/base-button'
import { Visual } from '~/components/theme/theme-button'
import style from './mobile-planting-service-button.scss?module'

const cn = useClassNames('mobile-planting-service-button', style)

const classes = {
  main: cn(),
  wrapper: cn('wrapper'),
  image: cn('image'),
  imageWrapper: cn('image-wrapper'),
  heading: cn('heading'),
  leading: cn('leading'),
  percent: cn('percent'),
  text: cn('text'),
  textWrapper: cn('text-wrapper'),
  buttonLine: cn('button-line'),
  button: cn('button'),
  fromPrice: cn('from-price'),
  arrow: cn('arrow')
}

export const MobilePlantingServiceButton = injectStyles(
  style,
  defineComponent({
    name: 'MobilePlantingServiceButton',
    props: {
      heading: {
        type: String,
        required: true
      },
      percent: {
        type: Number,
        required: true
      },
      image: {
        type: String,
        required: true
      }
    },
    setup: (props, { emit }) => {
      return () => (
        <div class={classes.main}>
          <div class={classes.imageWrapper}>
            <img src={props.image} alt={props.heading} class={classes.image} />
          </div>
          <div class={classes.heading}>{props.heading}</div>
          <div class={classes.leading}>
            <span class={classes.percent}>{props.percent}%</span>
            <span class={classes.fromPrice}>от стоимости</span>
            <img src={ArrowDownImage} class={classes.arrow} alt="" />
          </div>
          <div class={classes.buttonLine}>
            <BaseButton
              class={classes.button}
              size="s"
              visual={Visual.outline}
              onClick={() => emit('click')}
            >
              Посмотреть
            </BaseButton>
          </div>
        </div>
      )
    }
  })
)
