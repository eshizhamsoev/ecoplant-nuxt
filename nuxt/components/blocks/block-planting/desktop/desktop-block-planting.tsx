import { computed, defineComponent } from '@nuxtjs/composition-api'
import standardImage from '../resources/standard.jpg'
import guaranteeImage from '../resources/guarantee.jpg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { PlantingServiceButton } from '~/components/blocks/block-planting/desktop/planting-service-button'
import { injectStyles } from '~/support/utils/injectStyle'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { BaseVideoBlockWithButton } from '~/components/base'
import style from './desktop-block-planting.scss?module'

const cn = useClassNames('block-planting', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  video: cn('video'),
  buttons: cn('buttons'),
  button: cn('button')
}

export const DesktopBlockPlanting = injectStyles(
  style,
  defineComponent({
    name: 'BlockPlanting',
    setup: (props, { root }) => {
      const modalOpener = useModalOpener()

      const openStandardModal = () =>
        modalOpener({
          name: 'planting-standard'
        })

      const openGuaranteeModal = () =>
        modalOpener({
          name: 'planting-guarantee'
        })
      const planting = computed(() => root.$accessor.planting)
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>
            Привезем и посадим <b>в течение 1 дня</b>
          </div>
          {planting.value && (
            <BaseVideoBlockWithButton
              class={classes.video}
              video={planting.value.video}
              poster={planting.value.poster}
              button-text={planting.value.buttonText}
            />
          )}
          <div class={classes.buttons}>
            <PlantingServiceButton
              onClick={openStandardModal}
              class={classes.button}
              image={standardImage}
              heading="Стандартная"
              percent={30}
            />
            <PlantingServiceButton
              onClick={openGuaranteeModal}
              class={classes.button}
              image={guaranteeImage}
              heading="Гарантийная"
              percent={50}
            />
          </div>
        </div>
      )
    }
  })
)
