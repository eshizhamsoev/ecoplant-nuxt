import { defineComponent } from '@nuxtjs/composition-api'
import ArrowDownImage from './resources/arrow-down.svg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { BaseButton } from '~/components/base/base-button'
import { Visual } from '~/components/theme/theme-button'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './planting-service-button.scss?module'

const cn = useClassNames('planting-service-button', style)

const classes = {
  main: cn(),
  wrapper: cn('wrapper'),
  image: cn('image'),
  imageWrapper: cn('image-wrapper'),
  heading: cn('heading'),
  leading: cn('leading'),
  percent: cn('percent'),
  text: cn('text'),
  buttonLine: cn('button-line'),
  button: cn('button'),
  fromPrice: cn('from-price'),
  arrow: cn('arrow')
}

export const PlantingServiceButton = injectStyles(
  style,
  defineComponent({
    name: 'PlantingServiceButton',
    props: {
      heading: {
        type: String,
        required: true
      },
      percent: {
        type: Number,
        required: true
      },
      image: {
        type: String,
        required: true
      }
    },
    setup: (props, { emit }) => {
      return () => (
        <div class={classes.main}>
          <div class={classes.wrapper}>
            <div class={classes.imageWrapper}>
              <img src={props.image} alt={props.heading} loading={'lazy'} />
            </div>
            <div class={classes.text}>
              <div class={classes.heading}>{props.heading}</div>
              <div class={classes.leading}>
                <span class={classes.percent}>{props.percent}%</span>
                <span class={classes.fromPrice}>от стоимости</span>
              </div>
            </div>
            <img
              src={ArrowDownImage}
              class={classes.arrow}
              alt=""
              loading={'lazy'}
            />
          </div>
          <div class={classes.buttonLine}>
            <BaseButton
              class={classes.button}
              size="m"
              visual={Visual.outline}
              onClick={() => emit('click')}
            >
              Посмотреть
            </BaseButton>
          </div>
        </div>
      )
    }
  })
)
