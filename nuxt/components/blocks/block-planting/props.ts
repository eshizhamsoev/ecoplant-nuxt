export const blockPlantingProps = {
  video: {
    type: String,
    required: true as true
  },
  poster: {
    type: String,
    required: true as true
  },
  buttonText: {
    type: String,
    required: true as true
  }
}
