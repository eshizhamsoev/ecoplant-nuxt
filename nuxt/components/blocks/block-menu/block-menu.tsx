import { computed, defineComponent, inject } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import consult from '~/assets/common/manager.jpg'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { CategoriesQuery, CategoriesQueryVariables } from '~/_generated/types'
import { categoriesQuery } from '~/graphql/queries'
import { menuSymbol } from '~/data/providers/menu/menuSymbol'
import { BaseLink } from '~/components/base/base-link'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { SERVICE_CATEGORY_ID } from '~/data/const/servicesCategory'
import { EXCLUSIVE_CATEGORY_ID } from '~/data/const/exclusivesCategory'
import style from './block-menu.scss?module'

const cn = useClassNames('block-menu', style)

export const BlockMenu = injectStyles(
  style,
  defineComponent({
    name: 'BlockMenu',
    props: {},
    setup: (props, { root }) => {
      const vars = computed(() => ({
        ids: root.$accessor.categoryIds.filter(
          (id) => ![SERVICE_CATEGORY_ID, EXCLUSIVE_CATEGORY_ID].includes(id)
        )
      }))
      const modalOpener = useModalOpener()
      const openShippingModal = () => modalOpener({ name: 'shipping' })
      const openPlantingModal = () => modalOpener({ name: 'planting' })
      const openFeedbackModal = () => root.$router.push('/review')
      const openReviewModal = () => modalOpener({ name: 'review' })
      const addressModal = root.$device.isMobileOrTablet
        ? 'address-mobile'
        : 'address'
      const openCurrentContactsModal = () =>
        modalOpener({
          name: addressModal,
          isAdditional: false
        })
      const openAlternativeContacts = () =>
        modalOpener({
          name: addressModal,
          isAdditional: true
        })
      const { result } = useGqlQuery<
        CategoriesQuery,
        Omit<CategoriesQueryVariables, 'site'>
      >(categoriesQuery, vars)
      const categoryLinks = computed(() => {
        return (
          result.value?.categories.map(({ id, name }) => ({
            link: `/categories/${id}#category-page`,
            text: name
          })) || []
        )
      })

      const menu = inject(menuSymbol)
      const close = () => {
        menu?.close()
      }

      return () => (
        <div class={cn()}>
          <div class={cn('close-container')}>
            <button
              aria-label="Закрыть меню"
              class={cn('close')}
              onClick={close}
            >
              &times;
            </button>
          </div>
          <BaseIcon name="logo" width={93} height={93} class={cn('logo')} />
          <nav class={cn('nav')}>
            <ul class={cn('links')}>
              {categoryLinks.value.map(({ link, text }) => (
                <li key={text} class={cn('item')}>
                  <BaseLink to={link} class={cn('link')} onClick={close}>
                    {text}
                  </BaseLink>
                </li>
              ))}
              <li class={cn('item')}>
                <button class={cn('link')} onClick={openShippingModal}>
                  Доставка
                </button>
              </li>
              <li class={cn('item')}>
                <button class={cn('link')} onClick={openPlantingModal}>
                  Посадка
                </button>
              </li>
              <li class={cn('item')}>
                <BaseLink
                  onClick={close}
                  class={cn('link')}
                  to="/?tabs=BlockVideoReviews#tabs-BlockVideoReviews"
                >
                  Отзывы
                </BaseLink>
              </li>
              <li class={cn('item')}>
                <button
                  class={cn('link')}
                  onClick={
                    root.$device.isMobileOrTablet
                      ? openFeedbackModal
                      : openReviewModal
                  }
                >
                  Оставить отзыв
                </button>
              </li>
              <li class={cn('item')}>
                <button class={cn('link')} onClick={openCurrentContactsModal}>
                  Контакты {root.$accessor.contacts.currentRegion}
                </button>
              </li>
              <li class={cn('item')}>
                <button class={cn('link')} onClick={openAlternativeContacts}>
                  Контакты {root.$accessor.contacts.additionalContacts.region}
                </button>
              </li>
            </ul>
          </nav>
          <div class={cn('consult')}>
            <img
              src={consult}
              width={80}
              height={80}
              alt="Эксперт по подбору растений"
              class={cn('consult-image')}
              loading="lazy"
            />
            <div class={cn('consult-text')}>
              Для консультации позвоните
              <br />
              или напишите эксперту по растениям
            </div>
          </div>
        </div>
      )
    }
  })
)
