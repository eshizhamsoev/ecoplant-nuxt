import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { BaseLink } from '~/components/base/base-link'
import style from './mobile-heading-sale-button.scss?module'

const cn = useClassNames('mobile-heading-sale-button', style)

export const MobileHeadingSaleButton = injectStyles(
  style,
  defineComponent({
    name: 'MobileHeadingSaleButton',
    props: {},
    setup: () => {
      return () => (
        <BaseLink class={cn()} to="/sale">
          <BaseIcon class={cn('icon')} name={'fire'} width={12} height={16} />
          <span class={cn('text')}>-70%</span>
        </BaseLink>
      )
    }
  })
)
