import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { HeadingIcon } from '~/components/blocks/block-heading/desktop/heading-bottom-line/data/types'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './mobile-heading-bottom-line.scss?module'

const cn = useClassNames('mobile-heading-bottom-line', style)

const classes = {
  main: cn(),
  item: cn('item'),
  imageWrapper: cn('image-wrapper'),
  image: cn('image')
}

export const MobileHeadingBottomLine = injectStyles(
  style,
  defineComponent({
    name: 'MobileHeadingBottomLine',
    props: {
      icons: {
        type: Array as () => HeadingIcon[],
        required: true
      }
    },
    setup: (props) => {
      const modalOpener = useModalOpener()
      const onClick = (item: any) => {
        if (item.modalSettings) {
          modalOpener(item.modalSettings)
        }
      }
      return () => (
        <div class={classes.main}>
          {props.icons.map((item) => (
            <div class={classes.item} onClick={onClick.bind(null, item)}>
              <div class={classes.imageWrapper}>
                <img
                  src={item.image}
                  width={item.width}
                  height={item.height}
                  class={classes.image}
                  alt=""
                />
              </div>
              <div>{item.text()}</div>
            </div>
          ))}
        </div>
      )
    }
  })
)
