import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { BaseLink } from '~/components/base/base-link'
import style from './mobile-heading-cart.scss?module'

const cn = useClassNames('mobile-heading-cart', style)

export const MobileHeadingCart = injectStyles(
  style,
  defineComponent({
    name: 'MobileHeadingCart',
    props: {},
    setup: () => {
      return () => (
        <BaseLink class={cn()} to={'/checkout'}>
          <BaseIcon name={'cart'} class={cn('icon')} width={14} />
        </BaseLink>
      )
    }
  })
)
