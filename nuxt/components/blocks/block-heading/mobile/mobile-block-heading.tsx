import {
  computed,
  defineComponent,
  onBeforeUnmount,
  onMounted,
  ref
} from '@nuxtjs/composition-api'
import { MobileHeadingTopLine } from '~/components/blocks/shared-components/heading/heading-top-line/mobile'
import { blockHeadingProps } from '~/components/blocks/block-heading/props'
import { useClassNames } from '~/support/utils/bem-classnames'
import { MobileHeadingMiddleLine } from '~/components/blocks/block-heading/mobile/mobile-heading-middle-line'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobileHeadingBottomLine } from '~/components/blocks/block-heading/mobile/mobile-heading-bottom-line'
import { HeadingWrapper } from '~/components/blocks/shared-components/heading/heading-wrapper'
import { BaseIcon } from '~/components/base'
import { getIcons } from '~/components/blocks/block-heading/desktop/heading-bottom-line/data'
import { HeaderButtonReviews } from '~/components/common/header-button-reviews/header-button-reviews'
import { BlockVideoAbout } from '~/components/blocks/block-video-about'
import style from './mobile-block-heading.scss?module'

const cn = useClassNames('mobile-block-heading', style)

const classes = {
  main: cn(),
  inner: cn('inner'),
  top: cn('top'),
  middle: cn('middle'),
  bottom: cn('bottom'),
  arrow: cn('arrow')
}

export const MobileBlockHeading = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockHeading',
    props: blockHeadingProps,
    setup: (props, { refs }) => {
      const icons = getIcons(props.bottomIconsType).mobile // Not reactive
      const scrollY = ref(0)
      const height = ref(100)

      const listener = () => {
        scrollY.value = window.scrollY
      }
      onMounted(() => {
        const component = refs.root
        if (component && '$el' in component) {
          height.value = component.$el.clientHeight
        }
        listener()
        window.addEventListener('scroll', listener, { passive: true })
      })
      onBeforeUnmount(() => {
        window.removeEventListener('scroll', listener)
      })

      const headerIsScrolled = computed(
        () => height.value - 100 < scrollY.value
      )
      return () => (
        <HeadingWrapper
          class={classes.main}
          withOverlay={props.withOverlay}
          background={props.mobileBackground}
          ref="root"
        >
          <MobileHeadingTopLine
            class={classes.top}
            props={props.topLineOptions}
          />
          <MobileHeadingMiddleLine
            heading={props.heading}
            leading={props.leading}
            class={classes.middle}
          />
          {props.withBottomLine && (
            <MobileHeadingBottomLine class={classes.bottom} icons={icons} />
          )}
          <HeaderButtonReviews />
          {props.withScrollDownArrow && (
            <BaseIcon
              name="arrow-left"
              width={30}
              rotate={-90}
              class={classes.arrow}
            />
          )}
          <BlockVideoAbout
            class={cn('video', { hidden: !headerIsScrolled.value })}
          />
        </HeadingWrapper>
      )
    }
  })
)
