import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { headingMiddleLineProps } from '~/components/blocks/block-heading/desktop/heading-middle-line'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './mobile-heading-middle-line.scss?module'

const cn = useClassNames('mobile-heading-middle-line', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('leading')
}

export const MobileHeadingMiddleLine = injectStyles(
  style,
  defineComponent({
    name: 'MobileHeadingMiddleLine',
    props: headingMiddleLineProps,
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          <h1 class={classes.heading} domProps={{ innerHTML: props.heading }} />
          {props.showLeading && (
            <div class={classes.leading}>{props.leading}</div>
          )}
        </div>
      )
    }
  })
)
