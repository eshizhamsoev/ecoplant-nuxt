export const headingMiddleLineProps = {
  heading: {
    type: String,
    required: true as true
  },
  leading: {
    type: String,
    default: ''
  },
  showLeading: {
    type: Boolean,
    default: true
  },
  showCatalogButton: {
    type: Boolean,
    default: true
  }
}
