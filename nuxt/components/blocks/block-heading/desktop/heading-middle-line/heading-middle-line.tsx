import { defineComponent } from '@nuxtjs/composition-api'
import { headingMiddleLineProps } from './props'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './heading-middle-line.scss?module'

const cn = useClassNames('heading-middle-line', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('leading'),
  buttonLine: cn('button-line')
}

export const HeadingMiddleLine = injectStyles(
  style,
  defineComponent({
    name: 'HeadingMiddleLine',
    props: headingMiddleLineProps,
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          <div>
            <h1
              class={classes.heading}
              domProps={{ innerHTML: props.heading }}
            />
            {props.showLeading && (
              <div class={classes.leading}>{props.leading}</div>
            )}
          </div>
        </div>
      )
    }
  })
)
