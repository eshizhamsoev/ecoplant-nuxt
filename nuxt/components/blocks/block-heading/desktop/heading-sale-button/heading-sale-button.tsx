import {
  computed,
  defineComponent,
  PropOptions,
  ref
} from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { cnThemeButton, Color, Visual } from '~/components/theme/theme-button'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './heading-sale-button.scss?module'

const cn = useClassNames('heading-sale-button', style)

const classes = {
  main: cn(),
  button: [cn('button')],
  left: cn('left'),
  icon: cn('icon'),
  close: cn('close'),
  heading: cn('heading'),
  leading: cn('leading')
}

export enum SaleButtonSize {
  s = 's',
  m = 'm'
}

export const HeadingSaleButton = injectStyles(
  style,
  defineComponent({
    name: 'HeadingSaleButton',
    props: {
      size: {
        type: String,
        default() {
          return SaleButtonSize.m
        }
      } as PropOptions<SaleButtonSize>,
      withClose: {
        type: Boolean,
        default() {
          return true
        }
      } as PropOptions<boolean>
    },
    setup: (props) => {
      const show = ref(true)
      const close = () => {
        show.value = false
      }
      const modalOpener = useModalOpener()
      const openModal = () => {
        modalOpener({ name: 'sale' })
      }
      const buttonClass = computed(() => [
        cn('button'),
        cnThemeButton({
          color: Color.red,
          shadow: props.size === SaleButtonSize.m,
          visual: Visual.gradient
        })
      ])
      return () =>
        show.value && (
          <div class={cn({ size: props.size })}>
            <button class={buttonClass.value} onClick={openModal}>
              <span class={classes.left}>
                <span class={classes.heading}>Ликвидация</span>
                <span class={classes.leading}>-70%</span>
              </span>
              <BaseIcon name="click" width={29} class={classes.icon} />
            </button>
            {props.withClose && (
              <button class={classes.close} onclick={close}>
                &times;
              </button>
            )}
          </div>
        )
    }
  })
)
