import {
  computed,
  defineComponent,
  onBeforeUnmount,
  onMounted,
  ref
} from '@nuxtjs/composition-api'
import { HeadingTopLine } from '../../shared-components/heading/heading-top-line/desktop'
import { HeadingBottomLine } from './heading-bottom-line'
import { HeadingMiddleLine } from './heading-middle-line'
import { HeadingSaleButton } from '~/components/blocks/block-heading/desktop/heading-sale-button'
import { BaseIcon } from '~/components/base/base-icon'
import { useClassNames } from '~/support/utils/bem-classnames'
import { blockHeadingProps } from '~/components/blocks/block-heading/props'
import { injectStyles } from '~/support/utils/injectStyle'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  Indent,
  VerticalAlign
} from '~/components/pattern/pattern-icon-plus'
import { HeadingWrapper } from '~/components/blocks/shared-components/heading/heading-wrapper'
import { getIcons } from '~/components/blocks/block-heading/desktop/heading-bottom-line/data'
import { DesktopStickyLine } from '~/components/layouts/mobile/desktop-sticky-line'
import { HeaderButtonReviews } from '~/components/common/header-button-reviews/header-button-reviews'
import { BlockVideoAbout } from '~/components/blocks/block-video-about'
import style from './desktop-block-heading.scss?module'

const cn = useClassNames('desktop-block-heading', style)

const classes = {
  main: cn(),
  middleLine: cn('middle-line'),
  bottomLine: cn('bottom-line'),
  scrollDown: [
    cn('scroll-down'),
    cnPatternIconPlus({ 'vertical-align': VerticalAlign.center })
  ],
  scrollDownIcon: [
    cn('scroll-down-icon'),
    cnPatternIconPlusIcon({ indent: Indent.s })
  ],
  sale: cn('sale'),
  water: cn('water')
}

export const DesktopBlockHeading = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlockHeading',
    props: blockHeadingProps,
    setup(props, { refs }) {
      const scrollY = ref(0)
      const height = ref(100)

      const listener = () => {
        scrollY.value = window.scrollY
      }
      onMounted(() => {
        const component = refs.main
        if (component && '$el' in component) {
          height.value = component.$el.clientHeight
        }
        listener()
        window.addEventListener('scroll', listener, { passive: true })
      })
      onBeforeUnmount(() => {
        window.removeEventListener('scroll', listener)
      })

      const headerIsScrolled = computed(
        () => height.value - 100 < scrollY.value
      )
      const icons = getIcons(props.bottomIconsType).desktop // Not reactive
      return () => (
        <HeadingWrapper
          ref="main"
          class={classes.main}
          withOverlay={props.withOverlay}
          background={props.background}
        >
          <HeadingTopLine props={props.topLineOptions} />
          {(props.heading || props.leading) && (
            <HeadingMiddleLine
              heading={props.heading}
              leading={props.leading}
              showCatalogButton={props.showCatalogButton}
              showLeading={props.showLeading}
              class={classes.middleLine}
            />
          )}
          {props.withBottomLine && (
            <HeadingBottomLine class={classes.bottomLine} icons={icons} />
          )}
          <HeaderButtonReviews />
          {props.withScrollDownArrow && (
            <div class={classes.scrollDown}>
              <BaseIcon
                name="mouse"
                width={30}
                class={classes.scrollDownIcon}
              />
              листайте
            </div>
          )}
          {props.withSaleButton && <HeadingSaleButton class={cn('sale')} />}
          <DesktopStickyLine
            class={cn('sticky-line', { visible: headerIsScrolled.value })}
          />
          <BlockVideoAbout
            class={cn('video', { hidden: !headerIsScrolled.value })}
          />
        </HeadingWrapper>
      )
    }
  })
)
