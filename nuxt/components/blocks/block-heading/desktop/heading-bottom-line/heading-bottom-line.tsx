import { defineComponent } from '@nuxtjs/composition-api'
import { HeadingBottomLineItem } from './item'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { HeadingIcon } from '~/components/blocks/block-heading/desktop/heading-bottom-line/data/types'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './heading-bottom-line.scss?module'

const cn = useClassNames('heading-bottom-line', style)
const classes = {
  main: cn(),
  item: cn('item')
}

export const HeadingBottomLine = injectStyles(
  style,
  defineComponent({
    name: 'HeadingBottomLine',
    props: {
      icons: {
        type: Array as () => HeadingIcon[],
        required: true
      }
    },
    setup(props) {
      const modalOpener = useModalOpener()
      const onClick = (item: any) => {
        if (item.modalSettings) {
          modalOpener(item.modalSettings)
        }
      }
      return () => (
        <div class={classes.main}>
          {props.icons.map((item) => (
            <HeadingBottomLineItem
              onClick={onClick.bind(null, item)}
              image={item.image}
              class={classes.item}
              width={item.width}
              height={item.height}
            >
              {item.text()}
            </HeadingBottomLineItem>
          ))}
        </div>
      )
    }
  })
)
