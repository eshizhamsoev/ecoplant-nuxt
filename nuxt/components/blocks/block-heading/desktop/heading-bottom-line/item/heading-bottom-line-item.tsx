import { defineComponent } from '@nuxtjs/composition-api'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  Indent,
  VerticalAlign
} from '~/components/pattern/pattern-icon-plus'

import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './heading-bottom-line-item.scss?module'

const cn = useClassNames('heading-bottom-line-item', style)

const classes = {
  main: [cn(), cnPatternIconPlus({ 'vertical-align': VerticalAlign.center })],
  icon: [cnPatternIconPlusIcon({ indent: Indent.m }), cn('icon')]
}

export const HeadingBottomLineItem = injectStyles(
  style,
  defineComponent({
    name: 'HeadingBottomLineItem',
    props: {
      image: {
        type: String,
        required: true
      },
      width: {
        type: Number as () => number | undefined,
        default: undefined
      },
      height: {
        type: Number as () => number | undefined,
        default: undefined
      }
    },
    setup(props, { slots, emit }) {
      return () => (
        <div class={classes.main} onClick={() => emit('click')}>
          <img
            src={props.image}
            class={classes.icon}
            alt=""
            width={props.width}
            height={props.height}
          />
          <span>{slots.default && slots.default()}</span>
        </div>
      )
    }
  })
)
