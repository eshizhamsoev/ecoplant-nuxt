export const desktop = [
  {
    image: require('../../../resources/icons/catalogue.png'),
    text: () => [<b>9670 видов</b>, <br />, 'растений'],
    modalSettings: {
      name: 'price-list-request'
    },
    width: 98,
    height: 83
  },
  {
    image: require('../../../resources/icons/fertilizer.png'),
    text: () => ['Посадим', <br />, <b>за 1 день</b>],
    modalSettings: {
      name: 'planting'
    },
    width: 68,
    height: 89
  },
  {
    image: require('../../../resources/icons/reviews.png'),
    text: () => ['Отзывы'],
    modalSettings: {
      name: 'video-reviews'
    },
    width: 134,
    height: 116
  }
]

export const mobile = [
  {
    image: require('../../../resources/icons/catalogue.png'),
    width: 50,
    height: 42,
    text: () => [<b>9670 видов</b>, <br />, 'растений'],
    modalSettings: {
      name: 'price-list-request'
    }
  },
  {
    image: require('../../../resources/icons/fertilizer.png'),
    width: 40,
    height: 52,
    text: () => ['Посадим', <br />, <b>за 1 день</b>],
    modalSettings: {
      name: 'planting'
    }
  },
  {
    image: require('../../../resources/icons/reviews.png'),
    width: 67,
    height: 58,
    modalSettings: {
      name: 'video-reviews'
    },
    text: () => ['Отзывы']
  }
]
