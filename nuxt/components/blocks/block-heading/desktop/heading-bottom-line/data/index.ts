import { HeadingIcon } from './types'
import * as classic from './classic'
import * as opt from './opt'
import {
  CLASSIC,
  OPT
} from '~/components/blocks/block-heading/desktop/heading-bottom-line/data/const'

type config = {
  desktop: HeadingIcon[]
  mobile: HeadingIcon[]
}
export function getIcons(type: string): config {
  switch (type) {
    case CLASSIC:
      return classic
    case OPT:
      return opt
    default:
      return {
        desktop: [],
        mobile: []
      }
  }
}
