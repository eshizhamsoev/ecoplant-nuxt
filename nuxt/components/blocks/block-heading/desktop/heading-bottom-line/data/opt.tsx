export const desktop = [
  {
    image: require('./opt/sale.svg'),
    text: () => [<b>Со скидкой 70%</b>],
    width: 100
  },
  {
    image: require('./opt/standard.svg'),
    text: () => ['Соответствие', <br />, <b>ГОСТ-ам</b>],
    width: 80
  },
  {
    image: require('./opt/delivery.svg'),
    text: () => [<b>Поставка в день</b>, <br />, 'обращения'],
    width: 100
  }
]

export const mobile = [
  {
    image: require('./opt/sale.svg'),
    width: 50,
    text: () => [<b>Со скидкой 70%</b>]
  },
  {
    image: require('./opt/standard.svg'),
    width: 50,
    text: () => ['Соответствие', <br />, <b>ГОСТ-ам</b>]
  },
  {
    image: require('./opt/delivery.svg'),
    width: 50,
    text: () => [<b>Поставка в день</b>, <br />, 'обращения']
  }
]
