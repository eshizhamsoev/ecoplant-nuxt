import { CLASSIC, OPT } from './const'
import { Modal } from '~/support/hooks/useModalOpener'
export type HeadingIconType = typeof CLASSIC | typeof OPT

type HeadingIconText = JSX.Element | string

export type HeadingIcon = {
  text: () => HeadingIconText[]
  image: string
  width?: number
  height?: number
  modal?: Modal
}
