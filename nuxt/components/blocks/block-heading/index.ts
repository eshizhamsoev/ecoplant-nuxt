import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const BlockHeading = UtilUniversalComponentFactory(
  'BlockHeading',
  () => import('./mobile/index').then((c) => c.MobileBlockHeading),
  () => import('./desktop/index').then((c) => c.DesktopBlockHeading)
)
