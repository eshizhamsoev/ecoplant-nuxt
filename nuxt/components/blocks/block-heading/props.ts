import { headingMiddleLineProps } from '~/components/blocks/block-heading/desktop/heading-middle-line'
import { HeadingIconType } from '~/components/blocks/block-heading/desktop/heading-bottom-line/data/types'
import { CLASSIC } from '~/components/blocks/block-heading/desktop/heading-bottom-line/data/const'

export const blockHeadingProps = {
  background: {
    type: String,
    required: true as true
  },
  mobileBackground: {
    type: String,
    default: null
  },
  topLineOptions: {
    type: Object,
    default: null
  },
  withBottomLine: {
    type: Boolean,
    default: true
  },
  withOverlay: {
    type: Boolean,
    default: true
  },
  withPhone: {
    type: Boolean,
    default: true
  },
  withSaleButton: {
    type: Boolean,
    default: true
  },
  withScrollDownArrow: {
    type: Boolean,
    default: false
  },
  bottomIconsType: {
    type: String as () => HeadingIconType,
    default: CLASSIC
  },
  ...headingMiddleLineProps
}
