import type { ComponentInstance } from '@nuxtjs/composition-api'
import { ComponentsFactory } from '~/components/blocks/block-components/components-factory'

import { DesktopComponents } from '~/components/blocks/block-components/desktop'

import { MobileComponents } from '~/components/blocks/block-components/mobile'

export const BlockDesktopComponents = ComponentsFactory(DesktopComponents)
export const BlockMobileComponents = ComponentsFactory(MobileComponents)
export const renderBlockComponents = (isMobile: boolean) =>
  isMobile ? BlockMobileComponents : BlockDesktopComponents

export const getBlockRenderer = (root: ComponentInstance, name: string) => {
  return () =>
    root.$device.isMobileOrTablet
      ? MobileComponents[name]
      : DesktopComponents[name]
}
