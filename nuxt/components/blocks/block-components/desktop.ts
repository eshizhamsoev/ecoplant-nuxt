import { AsyncComponent } from 'vue/types/options'

export const DesktopComponents = {
  BlockHeading: () =>
    import('~/components/blocks/block-heading/desktop').then(
      (component) => component.DesktopBlockHeading
    ),
  BlockWinterVideo: () =>
    import('~/components/blocks/block-winter-video').then(
      (component) => component.BlockWinterVideo
    ),
  BlockContent: () =>
    import('~/components/blocks/block-content').then(
      (component) => component.BlockContent
    ),
  BlockCategory: () =>
    import('~/components/blocks/block-category').then(
      (component) => component.BlockCategory
    ),
  BlockSlider: () =>
    import('~/components/blocks/block-slider/desktop').then(
      (component) => component.DesktopBlockSlider
    ),
  BlockMainClient: () =>
    import('~/components/blocks/block-main-client').then(
      (component) => component.BlockMainClient
    ),
  BlockClients: () =>
    import('~/components/blocks/block-clients').then(
      (component) => component.BlockClients
    ),
  BlockDirector: () =>
    import('~/components/blocks/block-director').then(
      (component) => component.BlockDirector
    ),
  BlockTabs: () =>
    import('~/components/blocks/block-tabs/desktop').then(
      (component) => component.DesktopBlockTabs
    ),
  BlockFooter: () =>
    import('~/components/blocks/block-footer/desktop-block-footer').then(
      (component) => component.DesktopBlockFooter
    ),
  BlockCategories: () =>
    import('~/components/blocks/block-categories/desktop').then(
      (component) => component.DesktopBlockCategories
    ),
  BlockPlanting: () =>
    import('~/components/blocks/block-planting/desktop').then(
      (component) => component.DesktopBlockPlanting
    ),
  BlockShipping: () =>
    import('~/components/blocks/block-shipping/desktop').then(
      (component) => component.DesktopBlockShipping
    ),
  BlockDocuments: () =>
    import('~/components/blocks/block-documents/desktop').then(
      (component) => component.DesktopBlockDocuments
    ),
  BlockClientLogos: () =>
    import('~/components/blocks/block-client-logos/desktop').then(
      (component) => component.DesktopBlockClientLogos
    ),
  BlockVideoReviews: () =>
    import('~/components/blocks/block-video-reviews/desktop').then(
      (component) => component.DesktopBlockVideoReviews
    ),
  BlockAwards: () =>
    import('~/components/blocks/block-awards/desktop').then(
      (component) => component.DesktopBlockAwards
    ),
  BlockGrass: () =>
    import('~/components/blocks/block-grass').then(
      (component) => component.BlockGrass
    ),
  BlockGoogleTableInstructions: () =>
    import('~/components/blocks/block-google-table-instructions').then(
      (component) => component.BlockGoogleTableInstructions
    ),
  BlockHeadingOneLine: () =>
    import('~/components/blocks/block-heading-one-line/desktop').then(
      (component) => component.DesktopBlockHeadingOneLine
    ),
  BlockFooterOneLine: () =>
    import('~/components/blocks/block-footer-one-line/desktop').then(
      (component) => component.DesktopBlockFooterOneLine
    ),
  BlockCartButton: () =>
    import('~/components/blocks/block-cart-button').then(
      (component) => component.BlockCartButton
    ),
  BlockVideoAbout: () =>
    import('~/components/blocks/block-video-about').then(
      (component) => component.BlockVideoAbout
    ),
  BlockBarkBeetle: () =>
    import('~/components/blocks/block-bark-beetle/desktop').then(
      (component) => component.DesktopBlockBarkBeetle
    ),
  BlockPlantingCareRecommendation: () =>
    import('~/components/blocks/block-planting-care-recommendation').then(
      (component) => component.BlockPlantingCareRecommendation
    ),
  BlockBlackFriday: () =>
    import('~/components/blocks/block-black-friday').then(
      (component) => component.BlockBlackFriday
    ),
  BlockVideo: () =>
    import('~/components/blocks/block-video').then(
      (component) => component.BlockVideo
    ),
  BlockCharitableFoundation: () =>
    import('~/components/blocks/block-charitable-foundation').then(
      (component) => component.BlockCharitableFoundation
    ),
  BlockServicePanel: () =>
    import('~/components/blocks/block-service-panel').then(
      (component) => component.ServicePanel
    ),
  BlockPresent: () =>
    import('~/components/blocks/block-present').then(
      (component) => component.BlockPresent
    ),
  BlockFeaturedProducts: () =>
    import('~/components/blocks/block-featured-products').then(
      (component) => component.BlockFeaturedProducts
    ),
  BlockCareSeasons: () =>
    import('~/components/blocks/block-care-seasons/block-care-seasons.vue'),
  BlockPlantingVideos: () =>
    import('~/components/blocks/block-planting-videos').then(
      (c) => c.BlockPlantingVideos
    ),
  BlockHowToWater: () =>
    import('~/components/blocks/block-how-to-water/index').then(
      (c) => c.BlockHowToWater
    ),
  UniversalBlockImageGrid: () =>
    import('~/components/blocks/universal-block-image-grid').then(
      (component) => {
        return component.UniversalBlockImageGrid
      }
    ),
  BlockLandscapeAdvantages: () =>
    import(
      '~/components/blocks/landscape/block-landscape-advantages/block-landscape-advantages.vue'
    ),
  BlockLandscapeCalculate: () =>
    import(
      '~/components/blocks/landscape/block-landscape-calculate/desktop/desktop-block-landscape-calculate.vue'
    ),
  BlockLandscapeSecrets: () =>
    import(
      '~/components/blocks/landscape/block-landscape-secrets/desktop/desktop-block-landscape-secrets.vue'
    ),
  BlockLandscapeServices: () =>
    import(
      '~/components/blocks/landscape/block-landscape-services/block-landscape-services.vue'
    ),
  BlockLandscapeDendrologist: () =>
    import(
      '~/components/blocks/landscape/block-landscape-dendrologist/block-landscape-dendrologist.vue'
    ),
  BlockLandscapeHeading: () =>
    import(
      '~/components/blocks/landscape/block-landscape-heading/desktop/desktop-block-landscape-heading.vue'
    ),
  BlockLandscapePartnership: () =>
    import(
      '~/components/blocks/landscape/block-landscape-partnership/block-landscape-partnership.vue'
    ),
  BlockLandscapePopups: () =>
    import(
      '~/components/blocks/landscape/block-landscape-popups/block-landscape-popups.vue'
    ),
  BlockLandscapeExamples: () =>
    import(
      '~/components/blocks/landscape/block-landscape-examples/block-landscape-examples.vue'
    )
} as {
  [key: string]: AsyncComponent<any, any, any, any>
}
