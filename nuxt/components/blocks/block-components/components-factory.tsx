import { h } from '@vue/composition-api'
import { defineComponent } from '@nuxtjs/composition-api'
import { AsyncComponent } from 'vue/types/options'
import { Component } from '~/store/components'

export const ComponentsFactory = (asyncComponents: {
  [key: string]: AsyncComponent<any, any, any, any>
}) =>
  defineComponent({
    name: 'BlockComponents',
    props: {
      components: {
        type: Array as () => readonly Component[],
        required: true
      },
      prefix: {
        type: String,
        default: ''
      }
    },
    setup: (props) => {
      return () =>
        h(
          'div',
          props.components.map(({ name, id, props: componentProps }) => {
            return h(asyncComponents[name], {
              props: componentProps,
              domProps: { id: `${props.prefix}${id}` }
            })
          })
        )
    }
  })
