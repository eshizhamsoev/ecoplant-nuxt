import { AsyncComponent } from 'vue/types/options'

export const MobileComponents = {
  BlockHeading: () =>
    import('~/components/blocks/block-heading/mobile').then(
      (component) => component.MobileBlockHeading
    ),
  BlockWinterVideo: () =>
    import('~/components/blocks/block-winter-video').then(
      (component) => component.BlockWinterVideo
    ),
  BlockContent: () =>
    import('~/components/blocks/block-content').then(
      (component) => component.BlockContent
    ),
  BlockCatalog: () =>
    import('~/components/blocks/block-catalog/mobile').then(
      (component) => component.MobileBlockCatalog
    ),
  BlockSlider: () =>
    import('~/components/blocks/block-slider/mobile').then(
      (component) => component.MobileBlockSlider
    ),
  BlockMainClient: () =>
    import('~/components/blocks/block-main-client/mobile').then(
      (component) => component.MobileBlockMainClient
    ),
  BlockClients: () =>
    import('~/components/blocks/block-clients/mobile').then(
      (component) => component.MobileBlockClients
    ),
  BlockSale: () =>
    import('~/components/blocks/block-sale/mobile').then(
      (component) => component.MobileBlockSale
    ),
  BlockDirector: () =>
    import('~/components/blocks/block-director').then(
      (component) => component.BlockDirector
    ),
  BlockTabs: () =>
    import('~/components/blocks/block-tabs/mobile').then(
      (component) => component.MobileBlockTabs
    ),
  BlockFooter: () =>
    import('~/components/blocks/block-footer/mobile').then(
      (component) => component.MobileBlockFooter
    ),
  BlockCategories: () =>
    import('~/components/blocks/block-categories/mobile').then(
      (component) => component.MobileBlockCategories
    ),
  BlockPlanting: () =>
    import('~/components/blocks/block-planting/mobile').then(
      (component) => component.MobileBlockPlanting
    ),
  BlockShipping: () =>
    import('~/components/blocks/block-shipping/mobile').then(
      (component) => component.MobileBlockShipping
    ),
  BlockDocuments: () =>
    import('~/components/blocks/block-documents/mobile').then(
      (component) => component.MobileBlockDocuments
    ),
  BlockClientLogos: () =>
    import('~/components/blocks/block-client-logos/mobile').then(
      (component) => component.MobileBlockClientLogos
    ),
  BlockVideoReviews: () =>
    import('~/components/blocks/block-video-reviews/mobile').then(
      (component) => component.MobileBlockVideoReviews
    ),
  BlockAwards: () =>
    import('~/components/blocks/block-awards/mobile').then(
      (component) => component.MobileBlockAwards
    ),
  BlockPhoneButton: () =>
    import('~/components/blocks/block-phone-button/mobile').then(
      (component) => component.MobileBlockPhoneButton
    ),
  BlockGrass: () =>
    import('~/components/blocks/block-grass').then(
      (component) => component.BlockGrass
    ),
  BlockHeadingOneLine: () =>
    import('~/components/blocks/block-heading-one-line/mobile').then(
      (component) => component.MobileBlockHeadingOneLine
    ),
  BlockGoogleTableInstructions: () =>
    import('~/components/blocks/block-google-table-instructions').then(
      (component) => component.BlockGoogleTableInstructions
    ),
  BlockFooterOneLine: () =>
    import('~/components/blocks/block-footer-one-line/mobile').then(
      (component) => component.MobileBlockFooterOneLine
    ),
  BlockBarkBeetle: () =>
    import('~/components/blocks/block-bark-beetle/mobile').then(
      (component) => component.MobileBlockBarkBeetle
    ),
  BlockPlantingCareRecommendation: () =>
    import('~/components/blocks/block-planting-care-recommendation').then(
      (component) => component.BlockPlantingCareRecommendation
    ),
  BlockMobileFixedLine: () =>
    import('~/components/blocks/block-mobile-fixed-line').then(
      (component) => component.BlockMobileFixedLine
    ),
  BlockBlackFriday: () =>
    import('~/components/blocks/block-black-friday').then(
      (component) => component.BlockBlackFriday
    ),
  BlockVideo: () =>
    import('~/components/blocks/block-video').then(
      (component) => component.BlockVideo
    ),
  BlockCharitableFoundation: () =>
    import('~/components/blocks/block-charitable-foundation').then(
      (component) => component.BlockCharitableFoundation
    ),
  BlockPresent: () =>
    import('~/components/blocks/block-present').then(
      (component) => component.BlockPresent
    ),
  BlockFeaturedProducts: () =>
    import('~/components/blocks/block-featured-products').then(
      (component) => component.BlockFeaturedProducts
    ),
  BlockCareSeasons: () =>
    import('~/components/blocks/block-care-seasons/block-care-seasons.vue'),
  BlockHowToWater: () =>
    import('~/components/blocks/block-how-to-water/index').then(
      (c) => c.BlockHowToWater
    ),
  BlockPlantingVideos: () =>
    import('~/components/blocks/block-planting-videos').then(
      (c) => c.BlockPlantingVideos
    ),
  BlockVideoAbout: () =>
    import('~/components/blocks/block-video-about').then(
      (component) => component.BlockVideoAbout
    ),
  BlockContacts: () =>
    import('~/components/blocks/block-contacts/mobile').then(
      (c) => c.MobileBlockContacts
    ),
  UniversalBlockImageGrid: () =>
    import('~/components/blocks/universal-block-image-grid').then(
      (component) => {
        return component.UniversalBlockImageGrid
      }
    ),
  BlockLandscapeAdvantages: () =>
    import(
      '~/components/blocks/landscape/block-landscape-advantages/block-landscape-advantages.vue'
    ),
  BlockLandscapeCalculate: () =>
    import(
      '~/components/blocks/landscape/block-landscape-calculate/mobile/mobile-block-landscape-calculate.vue'
    ),
  BlockLandscapeSecrets: () =>
    import(
      '~/components/blocks/landscape/block-landscape-secrets/mobile/mobile-block-landscape-secrets.vue'
    ),
  BlockLandscapeServices: () =>
    import(
      '~/components/blocks/landscape/block-landscape-services/block-landscape-services.vue'
    ),
  BlockLandscapeDendrologist: () =>
    import(
      '~/components/blocks/landscape/block-landscape-dendrologist/block-landscape-dendrologist.vue'
    ),
  BlockLandscapeHeading: () =>
    import(
      '~/components/blocks/landscape/block-landscape-heading/mobile/mobile-block-landscape-heading.vue'
    ),
  BlockLandscapePartnership: () =>
    import(
      '~/components/blocks/landscape/block-landscape-partnership/block-landscape-partnership.vue'
    ),
  BlockLandscapePopups: () =>
    import(
      '~/components/blocks/landscape/block-landscape-popups/block-landscape-popups.vue'
    ),
  BlockLandscapeExamples: () =>
    import(
      '~/components/blocks/landscape/block-landscape-examples/block-landscape-examples.vue'
    )
} as {
  [key: string]: AsyncComponent<any, any, any, any>
}
