import { computed, defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { cnBaseRoundButton } from '~/components/base/base-round-button'
import { cnThemeButton, Color } from '~/components/theme/theme-button'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePopupGallery } from '~/components/base/base-popup-gallery'
import { usePopupGallery } from '~/components/base/base-popup-gallery/usePopupGallery'
import { BaseButton, BaseIcon } from '~/components/base'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { clientPhotos } from '~/graphql/queries'
import { DesktopVideoReviews } from '~/components/blocks/block-clients/video-reviews/desktop'
import style from './block-clients.scss?module'
const cn = useClassNames('block-clients', style)

const roundButtonClass = cnBaseRoundButton({ size: 'm' })
const themeButtonClass = cnThemeButton({ color: Color.green })

const classes = {
  main: cn(),
  heading: cn('heading'),
  slider: cn('slider'),
  prev: [cn('prev'), roundButtonClass, themeButtonClass].join(' '),
  next: [cn('next'), roundButtonClass, themeButtonClass].join(' '),
  icon: cn('icon'),
  image: cn('image'),
  imageWrapper: cn('image-wrapper'),
  button: cn('button'),
  buttonText: cn('button-text'),
  arrow: cn('arrow'),
  arrowWrapper: cn('arrow-wrapper'),
  videoReviews: cn('video-reviews')
}

export const BlockClients = injectStyles(
  style,
  defineComponent({
    props: {},
    setup: (_, { refs }) => {
      const { result } = useGqlQuery(clientPhotos, {})
      const galleryImages = computed(
        () =>
          result.value?.clientPhotos.map((client) => ({
            small: client.image.thumb.src,
            large: client.image.large.src,
            largeSizes: {
              height: client.image.large.height,
              width: client.image.large.width
            },
            title: client.name
          })) || []
      )

      const {
        open: openPopupGallery,
        close: closePopupGallery,
        index: popupIndex
      } = usePopupGallery()

      const rowHeight = 214

      const galleryMaxHeight = ref<string>(`${rowHeight * 2}px`)

      const isGalleryOpened = ref(false)

      const toggleListHeight = () => {
        if (refs.gallery instanceof HTMLElement) {
          isGalleryOpened.value = !isGalleryOpened.value
          if (isGalleryOpened.value) {
            galleryMaxHeight.value = `${refs.gallery.scrollHeight}px`
            return
          }
          galleryMaxHeight.value = `${rowHeight * 2}px`
          window.scroll(0, refs.gallery.offsetTop - 100)
        }
      }

      function imageClick(i: number) {
        openPopupGallery(i)
      }

      const gallery = () => (
        <div
          style={{ height: galleryMaxHeight.value }}
          class={cn('gallery')}
          ref="gallery"
        >
          {result.value?.clientPhotos.map((client, i) => (
            <div class={classes.imageWrapper}>
              <img
                loading="lazy"
                id={`gallery-image-${i}`}
                class={classes.image}
                src={client.image.thumb.src}
                width={client.image.thumb.width}
                height={client.image.thumb.height}
                alt={''}
                onClick={() => {
                  imageClick(i)
                }}
              />
            </div>
          ))}
        </div>
      )

      return () => (
        <div class={classes.main}>
          {gallery()}
          <BaseButton
            onClick={toggleListHeight}
            size={'m'}
            class={classes.button}
          >
            <div class={classes.arrowWrapper}>
              <BaseIcon
                class={classes.arrow}
                name={'arrow-down'}
                width={17}
                rotate={isGalleryOpened.value ? 180 : 0}
              />
            </div>
            <div class={classes.buttonText}>
              {isGalleryOpened.value ? 'Скрыть' : 'Показать еще'}
            </div>
            <div class={classes.arrowWrapper}>
              <BaseIcon
                class={classes.arrow}
                name={'arrow-down'}
                width={17}
                rotate={isGalleryOpened.value ? 180 : 0}
              />
            </div>
          </BaseButton>
          <BasePopupGallery
            images={galleryImages.value}
            index={popupIndex.value}
            onClose={closePopupGallery}
            showThumbnails={false}
            isInstaLookalike={true}
          />
          <DesktopVideoReviews class={classes.videoReviews} />
        </div>
      )
    }
  })
)
