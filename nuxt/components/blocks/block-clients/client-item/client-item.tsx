import { defineComponent } from '@nuxtjs/composition-api'

import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { svgPlaceholder } from '~/support/utils/svg-placeholder'
import style from './client-item.scss?module'

const cn = useClassNames('client-item', style)

const classes = {
  main: cn(),
  image: cn('image'),
  name: cn('name')
}

export const ClientItem = injectStyles(
  style,
  defineComponent({
    props: {
      width: {
        type: Number,
        required: true
      },
      height: {
        type: Number,
        required: true
      },
      photo: {
        type: String,
        required: true
      },
      text: {
        type: String,
        required: true
      }
    },
    setup: (props, { emit }) => {
      return () => (
        <div class={classes.main} onClick={() => emit('click')}>
          <img
            src={svgPlaceholder(props.width, props.height)}
            data-src={props.photo}
            alt=""
            width={props.width}
            height={props.height}
            class={[classes.image, 'tns-lazy-img']}
          />
          <div domProps={{ innerHTML: props.text }} class={classes.name} />
        </div>
      )
    }
  })
)
