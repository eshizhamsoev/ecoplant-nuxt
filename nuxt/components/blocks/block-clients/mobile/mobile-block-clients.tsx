import { defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { BaseButton, BaseIcon } from '~/components/base'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { clientPhotos } from '~/graphql/queries'
import { MobileVideoReviews } from '~/components/blocks/block-clients/video-reviews/mobile'
import style from './mobile-block-client.scss?module'

const cn = useClassNames('mobile-block-client', style)

const classes = {
  main: cn(),
  button: cn('button'),
  slider: cn('slider'),
  gallery: cn('gallery'),
  imageWrapper: cn('image-wrapper'),
  image: cn('image'),
  videoReviews: cn('video-reviews')
}

export const MobileBlockClients = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockClients',
    props: {},
    setup: (props, { refs }) => {
      const { result } = useGqlQuery(clientPhotos, {})
      const modalOpener = useModalOpener()
      const openModal = (imageId: number) =>
        modalOpener({
          name: 'images-feed',
          clients: result.value?.clientPhotos || [],
          id: imageId
        })

      function imageClick(imageId: number) {
        openModal(imageId)
      }

      const isGalleryOpened = ref(false)

      const galleryMaxHeight = ref<null | string>(null)

      const toggleListHeight = () => {
        if (refs.gallery instanceof HTMLElement) {
          isGalleryOpened.value = !isGalleryOpened.value
          if (isGalleryOpened.value) {
            galleryMaxHeight.value = `${refs.gallery.scrollHeight}px`
            return
          }
          galleryMaxHeight.value = null
          window.scroll(0, refs.gallery.offsetTop - 50)
          // refs.gallery.scrollIntoView()
        }
      }

      return () => (
        <div class={classes.main} ref="main">
          <div
            id={'gallery'}
            ref="gallery"
            style={{ height: galleryMaxHeight.value }}
            class={cn('gallery', { opened: isGalleryOpened.value })}
          >
            {result.value?.clientPhotos.map((client, i) => (
              <div class={classes.imageWrapper}>
                <img
                  loading={'lazy'}
                  id={`gallery-image-${i}`}
                  class={classes.image}
                  src={client.image.thumb.src}
                  width={client.image.thumb.width}
                  height={client.image.thumb.height}
                  alt={''}
                  onClick={() => {
                    imageClick(i)
                  }}
                />
              </div>
            ))}
          </div>
          <BaseButton
            onClick={toggleListHeight}
            size={'l'}
            class={classes.button}
          >
            <div class={cn('arrow-wrapper')}>
              <BaseIcon
                class={cn('arrow')}
                name={'arrow-down'}
                width={17}
                rotate={isGalleryOpened.value ? 180 : 0}
              />
            </div>
            <div class={cn('button-text')}>
              {isGalleryOpened.value ? 'Скрыть' : 'Показать еще'}
            </div>
            <div class={cn('arrow-wrapper')}>
              <BaseIcon
                class={cn('arrow')}
                name={'arrow-down'}
                width={17}
                rotate={isGalleryOpened.value ? 180 : 0}
              />
            </div>
          </BaseButton>
          <MobileVideoReviews class={classes.videoReviews} />
        </div>
      )
    }
  })
)
