import { defineComponent } from '@nuxtjs/composition-api'
import imageLeft from '../../resources/reviews-left.jpg'
import imageRight from '../../resources/reviews-right.jpg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton, BaseIcon } from '~/components/base'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './desktop-video-reviews.scss?module'
const cn = useClassNames('video-reviews', style)

const classes = {
  main: cn(),
  imageBg: cn('image-bg'),
  inner: cn('inner'),
  reviewTitle: cn('review-title'),
  reviewIcon: cn('review-icon')
}

export const DesktopVideoReviews = injectStyles(
  style,
  defineComponent({
    setup: () => {
      const openModal = useModalOpener()
      const openModalVideoReviews = () => openModal({ name: 'video-reviews' })

      return () => (
        <div class={classes.main}>
          <img
            class={classes.imageBg}
            src={imageLeft}
            width="363"
            height="360"
            alt=""
          />
          <img
            class={classes.imageBg}
            src={imageRight}
            width="361"
            height="359"
            alt=""
          />
          <div class={classes.inner}>
            <BaseIcon class={classes.reviewIcon} name="play-video" width={50} />
            <div class={classes.reviewTitle}>видео-отзывы</div>
            <BaseButton onClick={openModalVideoReviews} size="l">
              Смотреть видео-отзывы
            </BaseButton>
          </div>
        </div>
      )
    }
  })
)
