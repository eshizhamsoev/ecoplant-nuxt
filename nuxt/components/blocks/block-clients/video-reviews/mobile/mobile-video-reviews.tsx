import { defineComponent } from '@nuxtjs/composition-api'
import background from '../../resources/reviews-sm.jpg'
import background2x from '../../resources/reviews-sm-2x.jpg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton } from '~/components/base'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './mobile-video-reviews.scss?module'
const cn = useClassNames('video-reviews', style)

const classes = {
  main: cn(),
  imageBg: cn('image-bg'),
  reviewTitle: cn('review-title')
}

export const MobileVideoReviews = injectStyles(
  style,
  defineComponent({
    setup: () => {
      const openModal = useModalOpener()
      const openModalVideoReviews = () => openModal({ name: 'video-reviews' })
      const srcset = `${background}, ${background2x} 2x`
      return () => (
        <div class={classes.main}>
          <div class={classes.reviewTitle}>видео-отзывы</div>
          <img
            class={classes.imageBg}
            src={background}
            srcset={srcset}
            alt=""
          />
          <BaseButton onClick={openModalVideoReviews} size="l">
            Смотреть видео-отзывы
          </BaseButton>
        </div>
      )
    }
  })
)
