import { defineComponent } from '@vue/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseImageGrid } from '~/components/base'
import { Image } from '~/components/blocks/block-documents/props'
import style from './universal-block-image-grid.scss?module'

const cn = useClassNames('universal-block-image-grid', style)

export const UniversalBlockImageGrid = injectStyles(
  style,
  defineComponent({
    name: 'UniversalBlockImageGrid',
    props: {
      heading: {
        type: String,
        required: true
      },
      images: {
        type: Array as () => Image[],
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={cn()}>
          <div class={cn('heading')}>{props.heading}</div>
          <BaseImageGrid
            md={6}
            sm={4}
            xs={2}
            class={cn('items')}
            images={props.images}
          />
        </div>
      )
    }
  })
)
