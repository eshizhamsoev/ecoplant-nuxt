import { defineComponent, ssrRef } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './mobile-currently-viewing.scss?module'

const cn = useClassNames('mobile-currently-viewing', style)

export const MobileCurrentlyViewing = injectStyles(
  style,
  defineComponent({
    name: 'MobileCurrentlyViewing',
    setup: () => {
      const minPeople = 177
      const maxPeople = 250
      const numberOfPeople = ssrRef<number>(0)
      if (!numberOfPeople.value) {
        numberOfPeople.value = Math.floor(
          Math.random() * (maxPeople - minPeople) + minPeople
        )
      }
      return () => (
        <div class={cn()}>
          Сейчас с вами смотрят{' '}
          <span class={cn('text', { dark: true })}>
            {numberOfPeople.value} человек
          </span>
        </div>
      )
    }
  })
)
