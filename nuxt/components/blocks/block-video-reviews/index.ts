import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const BlockVideoReviews = UtilUniversalComponentFactory(
  'BlockVideoReviews',
  () =>
    import('./mobile/index').then(
      ({ MobileBlockVideoReviews }) => MobileBlockVideoReviews
    ),
  () =>
    import('./desktop/index').then(
      ({ DesktopBlockVideoReviews }) => DesktopBlockVideoReviews
    )
)
