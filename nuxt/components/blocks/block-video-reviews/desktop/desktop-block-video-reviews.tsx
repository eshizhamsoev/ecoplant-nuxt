import { defineComponent } from '@nuxtjs/composition-api'
import icon from '../resources/icon.svg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseGrid } from '~/components/base/base-grid'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  Indent,
  VerticalAlign
} from '~/components/pattern/pattern-icon-plus'
import { BaseVideoPoster } from '~/components/base/base-video-poster/base-video-poster'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { videoReviewsQuery } from '~/graphql/queries'
import style from './desktop-block-video-reviews.scss?module'

const cn = useClassNames('desktop-block-video-reviews', style)

const classes = {
  main: cn(),
  heading: [
    cn('heading'),
    cnPatternIconPlus({ 'vertical-align': VerticalAlign.center })
  ],
  icon: [cn('icon'), cnPatternIconPlusIcon({ indent: Indent.l })],
  items: cn('items'),
  item: cn('item'),
  poster: cn('poster'),
  button: cn('button')
}

export const DesktopBlockVideoReviews = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlockVideoReviews',
    props: {},
    setup: () => {
      const { result } = useGqlQuery(videoReviewsQuery, {})
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>
            <img src={icon} class={classes.icon} alt="" />
            Реальные отзывы от наших клиентов
          </div>
          {result.value && (
            <BaseGrid class={classes.items} md={4}>
              {result.value.videoReviews.map((item) => (
                <BaseVideoPoster
                  class={classes.item}
                  video-object={item.video}
                  poster-object={item.poster.desktop}
                />
              ))}
            </BaseGrid>
          )}
        </div>
      )
    }
  })
)
