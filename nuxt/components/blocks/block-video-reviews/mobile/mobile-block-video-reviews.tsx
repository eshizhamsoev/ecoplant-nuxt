import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseGrid } from '~/components/base/base-grid'
import { BaseVideoPoster } from '~/components/base/base-video-poster/base-video-poster'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { videoReviewsQuery } from '~/graphql/queries'
import style from './mobile-block-video-reviews.scss?module'

const cn = useClassNames('mobile-block-video-reviews', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  items: cn('items'),
  item: cn('item'),
  poster: cn('poster'),
  button: cn('button'),
  icon: cn('icon')
}

export const MobileBlockVideoReviews = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockVideoReviews',
    props: {},
    setup: () => {
      const { result } = useGqlQuery(videoReviewsQuery, {})
      return () =>
        result.value && (
          <div class={classes.main}>
            <div class={classes.heading}>Отзывы</div>

            <BaseGrid class={classes.items} xs={3}>
              {result.value.videoReviews.map((item) => (
                <BaseVideoPoster
                  class={classes.item}
                  video-object={item.video}
                  poster-object={item.poster.desktop}
                  buttonWithShadow={false}
                  button-size={30}
                />
              ))}
            </BaseGrid>
          </div>
        )
    }
  })
)
