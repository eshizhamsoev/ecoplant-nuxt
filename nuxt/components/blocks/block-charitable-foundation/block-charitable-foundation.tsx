import { defineComponent } from '@nuxtjs/composition-api'
import charityOriginal from './assets/charity-original.jpg'
import charity from './assets/charity.jpg'
import charityX2 from './assets/charity-x2.jpg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePopupGallery } from '~/components/base'
import { usePopupGallery } from '~/components/base/base-popup-gallery/usePopupGallery'
import style from './block-charitable-foundation.scss?module'

const cn = useClassNames('block-charitable-foundation', style)

const images = [
  {
    small: charity,
    large: charityOriginal
  }
]

export const BlockCharitableFoundation = injectStyles(
  style,
  defineComponent({
    name: 'BlockCharitableFoundation',
    props: {},
    setup: () => {
      const {
        open: openPopupGallery,
        close: closePopupGallery,
        index: popupIndex
      } = usePopupGallery()
      return () => (
        <div class={cn()}>
          <div class={cn('heading')}>
            Фонд <b>Хабенского</b>
          </div>
          <div class={cn('content')}>
            <img
              onClick={() => openPopupGallery(0)}
              src={charity}
              srcset={`${charity}, ${charityX2} 2x`}
              class={cn('certificate')}
              alt="Сертификат благотворительности участника акции Я помогаю! Ты помогаешь! Мы помогаем!"
              loading={'lazy'}
            />
            <div class={cn('info')}>
              <div class={cn('text')}>
                <p>
                  Нашей дружной командой принято единогласное решение оказывать
                  посильную помощь детям, поэтому с октября 2020 года 3% с
                  каждого заказа, а также часть средств от оптовых продаж мы
                  перечисляем в Благотворительный Фонд Константина Хабенского.
                </p>
                <p>
                  Помимо этого мы стараемся помогать детям необходимыми для их
                  жизни и здоровья товарами и услугами.
                </p>
                <p>
                  У каждого ребенка есть свои мечты и надежды, но не у каждого
                  есть возможность их осуществить, поэтому все дружно мы можем
                  помочь это сделать!
                </p>
              </div>
              <div class={cn('total')}>
                <div class={cn('total-label')}>Уже перечислено на фонд</div>
                <div class={cn('total-text')}>
                  <span class={cn('total-value')}>1 497 000</span>
                  <span class={cn('total-currency')}>Руб.</span>
                </div>
              </div>
            </div>
            <BasePopupGallery
              images={images}
              index={popupIndex.value}
              showThumbnails={false}
              onClose={closePopupGallery}
            />
          </div>
        </div>
      )
    }
  })
)
