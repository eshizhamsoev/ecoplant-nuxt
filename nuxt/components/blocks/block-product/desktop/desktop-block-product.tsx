import { computed, defineComponent, toRefs } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ProductInfo } from '~/components/blocks/block-category/products-panel/product-info'
import { ProductSale } from '~/components/blocks/shared-components/product-sale'
import { Maybe, Price, Product } from '~/_generated/types'
import { getProductBreadcrumb } from '~/data/breadcrumbs'
import { HeadingWithBreadcrumbs } from '~/components/layouts/_partitial/heading-with-breadcrumbs'
import style from './desktop-block-product.scss?module'

const cn = useClassNames('desktop-block-product', style)

export const DesktopBlockProduct = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlockProduct',
    props: {
      product: {
        type: Object as () => Pick<
          Product,
          | 'images'
          | 'name'
          | 'id'
          | 'categoryId'
          | 'prices'
          | 'isSameSize'
          | 'showSalePrice'
          | 'description'
          | 'image'
          | 'varieties'
        > & {
          salePrice?: Maybe<Pick<Price, 'price' | 'size' | 'oldPrice'>>
        } & { meta?: { h1: string } },
        required: true
      }
    },
    setup: (props, { root }) => {
      const showExpandedInformation = computed(
        () => root.$accessor.showExpandedInformation
      )
      const { product } = toRefs(props)
      const breadcrumbs = getProductBreadcrumb(product)
      return () =>
        product.value && (
          <div class={cn()}>
            {showExpandedInformation.value ? (
              <HeadingWithBreadcrumbs
                heading={product.value.meta?.h1 || product.value.name}
                breadcrumbs={breadcrumbs.value}
                class={cn('heading-block')}
              />
            ) : (
              <h1 class={cn('heading')}>{product.value.name}</h1>
            )}
            <ProductInfo product={product.value} class={cn('info')} />
            <ProductSale
              name={product.value.name}
              image={product.value.image}
              size={product.value.salePrice?.size}
              price={
                (product.value.showSalePrice && product.value.salePrice) || null
              }
              class={cn('sale')}
            />
            {showExpandedInformation.value && product.value.description && (
              <div
                domProps={{ innerHTML: product.value.description }}
                class={cn('description')}
              />
            )}
          </div>
        )
    }
  })
)
