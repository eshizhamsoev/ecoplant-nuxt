import { computed, defineComponent } from '@nuxtjs/composition-api'
import { MobileProductName } from '../../shared-components/mobile-product-name'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobileImageSlider } from '~/components/blocks/shared-components/mobile-image-slider'
import { ProductPrices } from '~/components/blocks/shared-components/product-prices'
import { MobileProductSale } from '~/components/blocks/shared-components/product-sale/mobile'
import { MobileProductCartButton } from '~/components/blocks/block-product/mobile/mobile-product-cart-button'
import { MobileBlockPlanting } from '~/components/blocks/block-planting/mobile'
import { Maybe, Price, Product } from '~/_generated/types'
import { ProductVarieties } from '~/components/blocks/shared-components/product-varieties/product-varieties'
import { BlockMobileFixedLine } from '~/components/blocks/block-mobile-fixed-line'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { BaseButton } from '~/components/base'
import { Visual } from '~/components/theme/theme-button'
import { MobileProductTopLine } from '~/components/blocks/shared-components/mobile-product-top-line/mobile-product-top-line'
import style from './mobile-block-product.scss?module'

const cn = useClassNames('mobile-block-product', style)

const classes = {
  main: cn(),
  phone: [cn('phone')],
  sameSize: [cn('same-size')],
  cart: cn('cart'),
  cartWrapper: cn('cart-wrapper'),
  topCartWrapper: cn('top-cart-wrapper'),
  sale: cn('sale'),
  nav: cn('nav')
}

export const MobileBlockProduct = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockProduct',
    props: {
      product: {
        type: Object as () => Pick<
          Product,
          | 'images'
          | 'name'
          | 'id'
          | 'prices'
          | 'isSameSize'
          | 'showSalePrice'
          | 'hasAttributes'
          | 'categoryId'
          | 'varieties'
          | 'squaredImage'
        > & {
          salePrice?: Maybe<Pick<Price, 'price' | 'size' | 'oldPrice'>>
        } & { meta?: { h1: string } },
        required: true
      }
    },
    setup(props) {
      const modalOpener = useModalOpener()

      const name = computed(() => {
        return props.product.meta?.h1 || props.product.name
      })

      const openProductDescription = () =>
        modalOpener({
          name: 'product-description',
          productId: props.product.id
        })

      return () =>
        props.product && (
          <div class={classes.main}>
            <MobileProductTopLine />
            <MobileProductName title={name.value} class={cn('name')} />
            <MobileImageSlider
              images={props.product.images}
              class={cn('images')}
            />
            <div class={classes.topCartWrapper}>
              <BaseButton
                size="s"
                class={cn('description-button')}
                visual={Visual.outline}
                onClick={openProductDescription}
              >
                открыть описание
              </BaseButton>
              {props.product.hasAttributes && (
                <MobileProductCartButton class={classes.cart} />
              )}
            </div>
            {props.product.isSameSize && (
              <div class={classes.sameSize}>Размер не имеет значения</div>
            )}
            <div class={cn('prices-container')}>
              <ProductPrices product={props.product} class={cn('prices')} />
            </div>
            <div class={classes.cartWrapper}>
              <ProductVarieties product={props.product.varieties} />
            </div>

            <MobileProductSale product={props.product} class={classes.sale} />
            <MobileBlockPlanting
              video="https://ecoplant-pitomnik.ru/files/videos/planting/03.mp4"
              poster="https://ecoplant-pitomnik.ru/image/catalog/blocks/planting/videos/new-covers/leaf.jpg"
              buttonText="Посадка\nлиственных"
            />
            <BlockMobileFixedLine withCart={true} />
          </div>
        )
    }
  })
)
