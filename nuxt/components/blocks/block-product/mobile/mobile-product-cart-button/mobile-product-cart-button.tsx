import { computed, defineComponent, inject } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton, BaseIcon, BaseRubles } from '~/components/base'
import { cartSymbol } from '~/data/providers/cart/symbol'
import style from './mobile-product-cart-button.scss?module'

const cn = useClassNames('mobile-product-cart-button', style)

export const MobileProductCartButton = injectStyles(
  style,
  defineComponent({
    name: 'MobileProductCartButton',
    props: {},
    setup: () => {
      const cart = inject(cartSymbol)
      const productSum = computed<number>(() => cart?.total.value || 0)
      return () => (
        <BaseButton to="/checkout" class={cn()} size="l">
          <BaseIcon
            name="cart/wheelbarrow-filled"
            width={40}
            class={cn('icon')}
          />
          <span class={cn('text')}>
            <span class={cn('label')}>Корзина</span>
            <BaseRubles price={productSum.value} class={cn('total')} />
          </span>
        </BaseButton>
      )
    }
  })
)
