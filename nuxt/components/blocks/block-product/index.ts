import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const BlockProduct = UtilUniversalComponentFactory(
  'BlockProduct',
  () => import('./mobile').then((c) => c.MobileBlockProduct),
  () => import('./desktop').then((c) => c.DesktopBlockProduct)
)
