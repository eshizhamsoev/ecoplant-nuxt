import { defineComponent, useContext } from '@nuxtjs/composition-api'
import poster from './assets/poster.jpg'
import price from './assets/price.svg'
import fertika from './assets/fertika.png'
import fertilizer from './assets/fertilizer.png'
import plant from './assets/plant.png'
import plantMobile from './assets/plant-mobile.png'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { SaleFormLine } from '~/components/blocks/shared-components/sale-form-line'
import { BaseVideoBlockWithButton } from '~/components/base'
import style from './block-present.scss?module'

const cn = useClassNames('block-present', style)

export const BlockPresent = injectStyles(
  style,
  defineComponent({
    name: 'BlockPresent',
    props: {},
    setup: () => {
      const { $device } = useContext()

      const video = $device.isMobileOrTablet
        ? 'https://ecoplant-pitomnik.ru/files/videos/present/2021-08/mobile.mp4'
        : 'https://ecoplant-pitomnik.ru/files/videos/present/2021-08/desktop.mp4'
      return () => (
        <div class={cn()}>
          <div class={cn('grid')}>
            <div class={cn('heading')}>
              <b>Бесплатный</b> набор осенних удобрений в подарок
            </div>
            <div class={cn('leading')}>
              Оставьте номер и получите набор осенних удобрений!
            </div>
            <div class={cn('video')}>
              <img src={fertika} alt="" class={cn('fertika')} />
              <img src={fertilizer} alt="" class={cn('fertilizer')} />
              <picture>
                <source media="(max-width: 1000px)" srcset={plantMobile} />
                <img src={plant} alt="" class={cn('plant')} />
              </picture>
              <BaseVideoBlockWithButton
                autoFullscreen={true}
                video={video}
                poster={poster}
                buttonVisual={{ animate: true }}
                class={cn('video-inner')}
              />
            </div>
            <div class={cn('price')}>
              <img src={price} alt="Скидка 100%!" class={cn('price-image')} />
            </div>
            <div class={cn('form')}>
              <SaleFormLine source="Оставьте номер и получите набор удобрений и голубую ель!">
                <template slot="button">
                  <span class={cn('form-button')}>получить подарок</span>
                </template>
              </SaleFormLine>
              <div class={cn('policy')}>
                Согласен с политикой конфиденциальности
              </div>
            </div>
          </div>
        </div>
      )
    }
  })
)
