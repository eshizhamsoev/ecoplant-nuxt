import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { howToWaterItems } from '~/components/blocks/block-how-to-water/data'
import style from './block-how-to-water.scss?module'

const cn = useClassNames('block-how-to-water', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  headingLine: cn('heading-line'),
  headingCell: cn('heading-cell'),
  cell: cn('cell'),
  bridge: cn('bridge'),
  line: cn('line')
}

export const BlockHowToWater = injectStyles(
  style,
  defineComponent({
    name: 'BlockHowToWater',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>Как нужно правильно поливать:</div>
          <div class={classes.headingLine}>
            <div class={classes.headingCell}>Высота растения</div>
            <div class={classes.headingCell}>Поливать 1 раз в 2&nbsp;дня</div>
          </div>
          {howToWaterItems.map((item, itemIndex) => (
            <div key={itemIndex} class={classes.line}>
              <div class={classes.cell}>{item.label}</div>
              <div class={classes.cell}>{item.value}</div>
            </div>
          ))}
        </div>
      )
    }
  })
)
