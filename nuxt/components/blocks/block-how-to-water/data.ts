export const howToWaterItems = [
  {
    label: 'до 1 м.',
    value: '2.5 л.'
  },
  {
    label: '1.0 - 2.0 м.',
    value: '10 л.'
  },
  {
    label: '2.0 - 3.0 м.',
    value: '20 л.'
  },
  {
    label: '3.0 - 4.0 м.',
    value: '33 л.'
  },
  {
    label: '4.0 - 5.0 м.',
    value: '46 л.'
  },
  {
    label: '5.0 - 6.0 м.',
    value: '60 л.'
  },
  {
    label: '6.0 - 7.0 м.',
    value: '75 л.'
  },
  {
    label: '7.0 - 8.0 м.',
    value: '100 л.'
  },
  {
    label: '8.0 - 10.0 м.',
    value: '135 л.'
  }
]
