import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BarkBeetleIcons } from '~/components/content/bark-beetle-content/components/bark-beetle-icons'
import { BarkBeetleContent } from '~/components/content/bark-beetle-content/bark-beetle-content'
import { MobileHeadingTopLine } from '~/components/blocks/shared-components/heading/heading-top-line/mobile'
import { MobileHeadingMiddleLine } from '~/components/blocks/block-heading/mobile/mobile-heading-middle-line'
import style from './mobile-block-bark-beetle.scss?module'

const cn = useClassNames('mobile-block-bark-beetle', style)

const classes = {
  main: cn(),
  header: cn('header'),
  headerInner: cn('header-inner'),
  headerIcons: cn('header-icons'),
  headerMiddle: cn('header-middle'),
  heading: cn('heading')
}

export const MobileBlockBarkBeetle = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockBarkBeetle',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <div
            class={classes.header}
            style={{
              backgroundImage:
                'url(https://ecoplant-pitomnik.ru/image/cache/catalog/landings/headings/large-2-0x0.jpg)'
            }}
          >
            <div class={classes.headerInner}>
              <MobileHeadingTopLine hideElements={['water']} />
              <MobileHeadingMiddleLine
                class={classes.headerMiddle}
                heading={
                  'Привет!\nЭто мы — твои ели и сосны из сада и нас ест короед. \nСпаси нас, пожалуйста'
                }
                showLeading={false}
              />
              <BarkBeetleIcons class={classes.headerIcons} />
            </div>
          </div>
          <BarkBeetleContent />
        </div>
      )
    }
  })
)
