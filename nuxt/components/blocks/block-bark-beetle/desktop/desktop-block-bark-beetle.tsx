import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BarkBeetleContent } from '~/components/content/bark-beetle-content/bark-beetle-content'
import { HeadingTopLine } from '~/components/blocks/shared-components/heading/heading-top-line/desktop'
import { cnThemeWrapper } from '~/components/theme/theme-wrapper'
import { BarkBeetleIcons } from '~/components/content/bark-beetle-content/components/bark-beetle-icons'
import style from './desktop-block-bark-beetle.scss?module'

const cn = useClassNames('desktop-block-bark-beetle', style)

const classes = {
  main: cn(),
  header: cn('header'),
  headerInner: [cn('header-inner'), cnThemeWrapper()],
  headerIcons: cn('header-icons'),
  heading: cn('heading')
}

export const DesktopBlockBarkBeetle = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlockBarkBeetle',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <div
            class={classes.header}
            style={{
              backgroundImage:
                'url(https://ecoplant-pitomnik.ru/image/cache/catalog/landings/headings/large-2-0x0.jpg)'
            }}
          >
            <div class={classes.headerInner}>
              <HeadingTopLine />
              <div class={classes.heading}>
                Привет! <br />
                Это мы &mdash; твои ели и сосны <br />
                из сада и нас ест короед. <br />
                Спаси нас, пожалуйста
              </div>
              <BarkBeetleIcons class={classes.headerIcons} />
            </div>
          </div>
          <BarkBeetleContent />
        </div>
      )
    }
  })
)
