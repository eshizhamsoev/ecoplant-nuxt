import { computed, defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base/base-icon'
import {
  PhoneLink,
  PhoneSlotData
} from '~/components/action-providers/phone-link'
import { FooterButton } from '~/components/blocks/block-footer/footer-button'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { BaseLazyLoadImage } from '~/components/base/base-lazy-load-image'
import { EmailLink } from '~/components/action-providers/email-link'
import style from './mobile-block-contacts.scss?module'

const cn = useClassNames('mobile-block-contacts', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  button: cn('button'),
  buttonLine: cn('button-line'),
  info: cn('info'),
  item: cn('item'),
  phone: cn('phone'),
  phoneIcon: cn('phone-icon'),
  map: cn('map'),
  mapWrapper: cn('map-wrapper'),
  secondBlock: cn('second-block'),
  email: cn('email'),
  tabPanel: cn('tab-panel'),
  tabButton: cn('tab-button'),
  tabMenuItem: cn('tab-menu-item'),
  tabMenu: cn('tab-menu')
}

export const MobileBlockContacts = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockContacts',
    props: {},
    setup: (props, { root }) => {
      const currentTab = ref(0)
      const contacts = computed(() => root.$accessor.contacts)
      const address = computed(() => contacts.value.address)
      const geoPoint = computed(() => contacts.value.geoPoint)
      const email = computed(() => contacts.value.email)
      const additionalContacts = computed(
        () => root.$accessor.contacts.additionalContacts
      )
      const contactsArray = computed(() => [
        contacts.value.currentRegion,
        additionalContacts.value.region
      ])
      const getStaticMapUrl = (longitude: number, latitude: number) => {
        return `https://static-maps.yandex.ru/1.x/?ll=${longitude},${latitude}&pt=${longitude},${latitude},pm2dgm&z=13&l=map&size=335,400`
      }
      const phone = ({ phone: { text } }: PhoneSlotData) => [
        <BaseIcon
          name="phone"
          width={13}
          height={13}
          class={classes.phoneIcon}
        />,
        text
      ]
      const modalOpener = useModalOpener()
      const openModal = () =>
        modalOpener({ name: 'navigate', point: geoPoint.value })
      const setActiveTab = (id: number) => (currentTab.value = id)
      return () => (
        <div class={classes.main}>
          <ul class={classes.tabMenu}>
            {contactsArray.value.map((item, idx) => (
              <li class={classes.tabMenuItem} key={idx}>
                <button
                  aria-controls={idx}
                  aria-selected={currentTab.value === idx}
                  class={classes.tabButton}
                  onclick={() => setActiveTab(idx)}
                >
                  {item}
                </button>
              </li>
            ))}
          </ul>
          <div class={classes.tabPanel}>
            <div hidden={currentTab.value !== 0}>
              <div class={classes.heading}>{contacts.value.currentRegion}</div>
              <div class={classes.buttonLine}>
                <FooterButton class={classes.button} point={geoPoint.value} />
              </div>
              <div class={classes.info}>
                <div class={classes.item}>
                  <b>Адрес</b> {address.value}
                </div>
                <div class={classes.item}>
                  <b>С 8.00 до 20.00</b> без выходных и праздников
                </div>
                <div class={classes.item}>
                  <PhoneLink
                    scopedSlots={{ default: phone }}
                    class={classes.phone}
                  />
                </div>
                <div class={classes.item}>
                  <EmailLink class={classes.email} email={email.value}>
                    {email.value}
                  </EmailLink>{' '}
                  – для клиентов <br />{' '}
                  <EmailLink class={classes.email} email={'work@eco-plant.ru'}>
                    work@eco-plant.ru
                  </EmailLink>{' '}
                  – для предложений
                </div>
              </div>
              <div class={classes.mapWrapper} onClick={openModal}>
                <BaseLazyLoadImage
                  src={getStaticMapUrl(
                    geoPoint.value.longitude,
                    geoPoint.value.latitude
                  )}
                  class={classes.map}
                  alt="Маршрут"
                  width={335}
                  height={400}
                />
              </div>
            </div>
            <div hidden={currentTab.value !== 1} class={classes.secondBlock}>
              <div class={classes.heading}>
                {additionalContacts.value.region}
              </div>
              <div class={classes.buttonLine}>
                <FooterButton
                  class={classes.button}
                  point={additionalContacts.value.geoPoint}
                />
              </div>
              <div class={classes.info}>
                <div class={classes.item}>
                  <b>Адрес</b> {additionalContacts.value.address}
                </div>
                <div class={classes.item}>
                  <b>С 8.00 до 20.00</b> без выходных и праздников
                </div>
                <div class={classes.item}>
                  <PhoneLink
                    isAdditional={true}
                    scopedSlots={{ default: phone }}
                    class={classes.phone}
                  />
                </div>
                <div class={classes.item}>
                  <EmailLink
                    class={classes.email}
                    email={additionalContacts.value.email}
                  >
                    {additionalContacts.value.email}
                  </EmailLink>{' '}
                  – для клиентов <br />{' '}
                  <EmailLink class={classes.email} email={'work@eco-plant.ru'}>
                    work@eco-plant.ru
                  </EmailLink>{' '}
                  – для предложений
                </div>
              </div>
              <div class={classes.mapWrapper} onClick={openModal}>
                <BaseLazyLoadImage
                  src={getStaticMapUrl(
                    additionalContacts.value.geoPoint.longitude,
                    additionalContacts.value.geoPoint.latitude
                  )}
                  class={classes.map}
                  alt="Маршрут"
                  width={335}
                  height={400}
                />
              </div>
            </div>
          </div>
        </div>
      )
    }
  })
)
