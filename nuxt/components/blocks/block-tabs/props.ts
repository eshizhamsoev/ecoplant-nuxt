type BlockType = {
  name: string
  title: string
  props?: Object
  isDesktop: boolean
  isMobile: boolean
}

export const blockTabsProps = {
  showBlocks: {
    type: Array as () => BlockType[],
    required: true as true
  }
}

export type BlockTabsProps = {
  showBlocks: BlockType[]
}
