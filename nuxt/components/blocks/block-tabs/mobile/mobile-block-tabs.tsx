import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { TabsNav } from '~/components/blocks/block-tabs/common/tabs-nav'
import { useTab } from '~/components/blocks/block-tabs/useTabs'
import { blockTabsProps } from '~/components/blocks/block-tabs/props'
import style from './mobile-block-tabs.scss?module'

const cn = useClassNames('mobile-block-tabs', style)

const classes = {
  main: cn(),
  nav: cn('nav')
}

export const MobileBlockTabs = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockTabs',
    props: blockTabsProps,
    setup(props, { root }) {
      const { blockNames, currentIndex, render } = useTab(props, root)

      return () => {
        return (
          <div class={classes.main} id={'#delivery'}>
            <TabsNav
              class={classes.nav}
              componentNames={blockNames.value}
              currentIndex={currentIndex.value}
              onClick={(index: number) => {
                currentIndex.value = index
              }}
            />
            {render()}
          </div>
        )
      }
    }
  })
)
