import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './tabs-nav.scss?module'

const cn = useClassNames('tabs-nav', style)

export const TabsNav = injectStyles(
  style,
  defineComponent({
    name: 'TabsNav',
    props: {
      componentNames: {
        type: Array as () => readonly string[],
        required: true
      },
      currentIndex: {
        type: Number,
        required: true
      }
    },
    setup: (props, { emit }) => {
      return () => (
        <div class={cn()}>
          {props.componentNames.map((name, index) => (
            <button
              key={name}
              onClick={() => emit('click', index)}
              class={cn('item', { active: index === props.currentIndex })}
            >
              {name}
            </button>
          ))}
        </div>
      )
    }
  })
)
