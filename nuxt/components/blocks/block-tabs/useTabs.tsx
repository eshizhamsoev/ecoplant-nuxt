import { h } from '@vue/composition-api'
import type { ComponentInstance } from '@nuxtjs/composition-api'
import {
  computed,
  // nextTick,
  // onMounted,
  ref,
  watch
} from '@nuxtjs/composition-api'

import { BlockTabsProps } from '~/components/blocks/block-tabs/props'
import { DesktopComponents } from '~/components/blocks/block-components/desktop'
import { MobileComponents } from '~/components/blocks/block-components/mobile'

const idPrefix = 'tabs-'
// const hashPrefix = '#' + idPrefix

export const useTab = (props: BlockTabsProps, root: ComponentInstance) => {
  const blocks = computed(() =>
    props.showBlocks.filter(
      (block) =>
        (block.isDesktop && root.$device.isDesktop) ||
        (block.isMobile && root.$device.isMobileOrTablet)
    )
  )

  const getIndex = (route: any) => {
    if (!route.query.tabs) {
      return -1
    }

    const blockName = route.query.tabs
    return blocks.value.findIndex(({ name }) => name === blockName)
  }

  const currentIndex = ref(root.$route.query.tabs ? getIndex(root.$route) : 0)

  const currentBlock = computed(
    () => blocks.value && blocks.value[currentIndex.value]
  )

  const blockNames = computed(() => blocks.value.map((block) => block.title))

  const id = computed(() => idPrefix + currentBlock.value.name)

  const render = () => {
    if (!currentBlock.value) {
      return
    }
    const component = root.$device.isMobileOrTablet
      ? MobileComponents[currentBlock.value.name]
      : DesktopComponents[currentBlock.value.name]

    return (
      <div id={id.value}>
        {h(component, {
          key: id.value,
          props: currentBlock.value.props
        })}
      </div>
    )
  }

  watch(
    () => root.$route,
    (value) => {
      const index = getIndex(value)
      if (index !== -1) {
        currentIndex.value = index
      }
    },
    {
      flush: 'pre'
    }
  )

  return {
    render,
    currentBlock,
    blockNames,
    currentIndex
  }
}
