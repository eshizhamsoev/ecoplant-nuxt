import { computed, defineComponent, ref } from '@nuxtjs/composition-api'
import goDown from './resources/arrow-down.jpg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import {
  BaseButton,
  BaseIcon,
  BaseVideoBlockWithButton
} from '~/components/base'
import { TabsNav } from '~/components/blocks/block-tabs/common/tabs-nav'
import { BaseSliderSelect } from '~/components/base/base-slider-select'
import { cnThemeButton, Color } from '~/components/theme/theme-button'
import { getPhoneLink } from '~/support/hooks/usePhoneClick'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './block-google-table-instructions.scss?module'

const cn = useClassNames('block-google-table-instructions', style)

const classes = {
  main: cn(),
  tabs: cn('tabs'),
  heading: cn('heading'),
  video: cn('video'),
  buttonWrapper: cn('button-wrapper'),
  linkText: cn('link-text'),
  buttons: cn('buttons'),
  arrow: cn('arrow'),
  iframe: cn('iframe'),
  phone: [cn('phone'), cnThemeButton({ color: Color.green })],
  email: [cn('email'), cnThemeButton({ color: Color.green })],
  callback: [cn('phone')],
  phoneIcon: cn('phone-icon')
}

export const BlockGoogleTableInstructions = injectStyles(
  style,
  defineComponent({
    name: 'BlockGoogleTableInstructions',
    props: {},
    setup: (props, { root }) => {
      const index = ref(0)
      const items = ref([
        {
          name: 'Можжевельники',
          src: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT-vZheSvnLA-bW4uUFjpiwRb-lx0P-_koWwrZ6lqssgwu1Az0Q6V05DBxlEv54gp_oUN7kw4XdbMSq/pubhtml?gid=0&single=true&widget=true&headers=false',
          height: 1750
        },
        {
          name: 'Туи',
          src: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT-vZheSvnLA-bW4uUFjpiwRb-lx0P-_koWwrZ6lqssgwu1Az0Q6V05DBxlEv54gp_oUN7kw4XdbMSq/pubhtml?gid=1025657098&single=true&widget=true&headers=false',
          height: 2950
        },
        {
          name: 'Блю Эрроу',
          src: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT-vZheSvnLA-bW4uUFjpiwRb-lx0P-_koWwrZ6lqssgwu1Az0Q6V05DBxlEv54gp_oUN7kw4XdbMSq/pubhtml?gid=421288557&single=true&widget=true&headers=false',
          height: 750
        },
        {
          name: 'Ели голубые',
          src: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT-vZheSvnLA-bW4uUFjpiwRb-lx0P-_koWwrZ6lqssgwu1Az0Q6V05DBxlEv54gp_oUN7kw4XdbMSq/pubhtml?gid=196492461&single=true&widget=true&headers=false',
          height: 750
        },
        {
          name: 'Ели',
          src: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT-vZheSvnLA-bW4uUFjpiwRb-lx0P-_koWwrZ6lqssgwu1Az0Q6V05DBxlEv54gp_oUN7kw4XdbMSq/pubhtml?gid=798246962&single=true&widget=true&headers=false',
          height: 2950
        },
        {
          name: 'Сосны',
          src: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT-vZheSvnLA-bW4uUFjpiwRb-lx0P-_koWwrZ6lqssgwu1Az0Q6V05DBxlEv54gp_oUN7kw4XdbMSq/pubhtml?gid=253596197&single=true&widget=true&headers=false',
          height: 1900
        },
        {
          name: 'Бонсаи',
          src: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT-vZheSvnLA-bW4uUFjpiwRb-lx0P-_koWwrZ6lqssgwu1Az0Q6V05DBxlEv54gp_oUN7kw4XdbMSq/pubhtml?gid=234100844&single=true&widget=true&headers=false',
          height: 2850
        },
        {
          name: 'Хвойная коллекция Б-1',
          src: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT-vZheSvnLA-bW4uUFjpiwRb-lx0P-_koWwrZ6lqssgwu1Az0Q6V05DBxlEv54gp_oUN7kw4XdbMSq/pubhtml?gid=119106105&single=true&widget=true&headers=false',
          height: 1600
        },
        {
          name: 'Хвойная коллекция В-6',
          src: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT-vZheSvnLA-bW4uUFjpiwRb-lx0P-_koWwrZ6lqssgwu1Az0Q6V05DBxlEv54gp_oUN7kw4XdbMSq/pubhtml?gid=1301571312&single=true&widget=true&headers=false',
          height: 3450
        },
        {
          name: 'Плодовые',
          src: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT-vZheSvnLA-bW4uUFjpiwRb-lx0P-_koWwrZ6lqssgwu1Az0Q6V05DBxlEv54gp_oUN7kw4XdbMSq/pubhtml?gid=719218471&single=true&widget=true&headers=false',
          height: 1700
        },
        {
          name: 'Кусты',
          src: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT-vZheSvnLA-bW4uUFjpiwRb-lx0P-_koWwrZ6lqssgwu1Az0Q6V05DBxlEv54gp_oUN7kw4XdbMSq/pubhtml?gid=855263687&single=true&widget=true&headers=false',
          height: 1500
        },
        {
          name: 'Премиум Кусты',
          src: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT-vZheSvnLA-bW4uUFjpiwRb-lx0P-_koWwrZ6lqssgwu1Az0Q6V05DBxlEv54gp_oUN7kw4XdbMSq/pubhtml?gid=1480321030&single=true&widget=true&headers=false',
          height: 10400
        },
        {
          name: 'Лиственные',
          src: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT-vZheSvnLA-bW4uUFjpiwRb-lx0P-_koWwrZ6lqssgwu1Az0Q6V05DBxlEv54gp_oUN7kw4XdbMSq/pubhtml?gid=977300828&single=true&widget=true&headers=false',
          height: 4900
        },
        {
          name: 'Ротодендрон',
          src: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vT-vZheSvnLA-bW4uUFjpiwRb-lx0P-_koWwrZ6lqssgwu1Az0Q6V05DBxlEv54gp_oUN7kw4XdbMSq/pubhtml?gid=1733867076&single=true&widget=true&headers=false',
          height: 1000
        }
      ])

      const currentItem = computed(() => items.value[index.value])

      const changeIndex = (value: number) => {
        index.value = value
      }

      const phone = computed(() => root.$accessor.contacts.phone)

      const phoneLink = computed(() => getPhoneLink(phone.value.number))

      const email = computed(() => root.$accessor.contacts.email)

      const emailLink = computed(() => 'mailto:' + email.value)

      const names = computed(() => items.value.map((item) => item.name))

      const modalOpener = useModalOpener()
      const openModal = () => modalOpener({ name: 'callback' })

      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>
            Сидите на карантине? Но желание посадить что-нибудь новенькое в
            своем саду растет каждый день! Мы собрали все наши растения в
            гугл-таблице с фото и ценами, чтобы вы могли делать заказы онлайн.
            Посмотрите видео-инструкцию как ей пользоваться и переходите по
            ссылке ниже
          </div>
          <BaseVideoBlockWithButton
            withControls={true}
            class={classes.video}
            video={
              'https://ecoplant-pitomnik.ru/files/videos/table-instructions.mp4'
            }
            poster={
              'https://ecoplant-pitomnik.ru/image/catalog/blocks/contactless-planting/table-instructions-poster.jpg'
            }
            buttonVisual={{
              size: root.$device.isMobileOrTablet ? 75 : 150,
              animate: true
            }}
            buttonText={'Посмотри нашу\n видеоинструкцию'}
            withOverlay={true}
          />
          <div class={classes.buttonWrapper}>
            <img src={goDown} alt="" class={classes.arrow} />
            <div class={classes.linkText}>
              Смотри весь ассортимент в таблице,
              <br /> лучше с компа
            </div>
          </div>
          {root.$device.isMobileOrTablet ? (
            <BaseSliderSelect
              value={index.value}
              options={names.value}
              onInput={changeIndex}
            />
          ) : (
            <TabsNav
              class={classes.tabs}
              currentIndex={index.value}
              componentNames={names.value}
              onClick={changeIndex}
            />
          )}
          {!root.$device.isMobileOrTablet && (
            <div class={classes.buttons}>
              <BaseButton size="l" href={emailLink.value} class={classes.phone}>
                {email.value}
              </BaseButton>
              <BaseButton size="l" color={Color.red} onClick={openModal}>
                Заказать обратный звонок
              </BaseButton>
              <BaseButton
                size="l"
                href={phoneLink.value}
                class={classes.phone}
                color={Color.yellow}
              >
                <BaseIcon name="phone" width={20} class={classes.phoneIcon} />
                {phone.value.text}
              </BaseButton>
            </div>
          )}
          <iframe
            key={currentItem.value.src}
            src={currentItem.value.src}
            class={classes.iframe}
            style={{ height: currentItem.value.height + 'px' }}
          />
          {root.$device.isMobileOrTablet ? (
            <BaseSliderSelect
              value={index.value}
              options={names.value}
              onInput={changeIndex}
            />
          ) : (
            <TabsNav
              class={classes.tabs}
              currentIndex={index.value}
              componentNames={names.value}
              onClick={changeIndex}
            />
          )}
          {!root.$device.isMobileOrTablet && (
            <div class={classes.buttons}>
              <BaseButton size="l" href={emailLink.value} class={classes.phone}>
                {email.value}
              </BaseButton>
              <BaseButton size="l" color={Color.red} onClick={openModal}>
                Заказать обратный звонок
              </BaseButton>
              <BaseButton
                size="l"
                href={phoneLink.value}
                class={classes.phone}
                color={Color.yellow}
              >
                <BaseIcon name="phone" width={20} class={classes.phoneIcon} />
                {phone.value.text}
              </BaseButton>
            </div>
          )}
        </div>
      )
    }
  })
)
