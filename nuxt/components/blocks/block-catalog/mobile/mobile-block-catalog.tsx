import { computed, defineComponent, watch } from '@nuxtjs/composition-api'
import { MobileCatalogCategoriesNav } from './mobile-catalog-categories-nav'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobileCatalogItemsGrid } from '~/components/blocks/block-catalog/mobile/mobile-catalog-products'
import { BaseBackButton } from '~/components/base'
import { CategoryId, CategoryIdConstructor } from '~/common-types/catalog'
import { MobileCatalogLinkImageItem } from '~/components/blocks/block-catalog/mobile/mobile-catalog-products/components/mobile-catalog-product'
// import { MobileCatalogWeekSale } from '~/components/blocks/block-catalog/mobile/mobile-catalog-products/components/mobile-catalog-week-sale/mobile-catalog-week-sale'
import { getCategoryById } from '~/queries/graphql/get-category-by-id'
import { MobileCatalogWholesaler } from '~/components/blocks/block-catalog/mobile/mobile-catalog-products/components/mobile-catalog-wholesaler/mobile-catalog-wholesaler'
import style from './mobile-block-catalog.scss?module'

const cn = useClassNames('mobile-block-catalog', style)

const classes = {
  main: cn(),
  nav: cn('nav'),
  products: cn('products'),
  back: cn('back'),
  backWrapper: cn('back-wrapper')
}

export const MobileBlockCatalog = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockCatalog',
    props: {
      categoryIds: {
        type: Array as () => CategoryId[],
        required: true
      },
      currentCategoryId: {
        type: CategoryIdConstructor,
        required: true
      }
    },
    setup: (props, { root, refs }) => {
      const currentId = computed(() => props.currentCategoryId)

      const { category } = getCategoryById(currentId)

      const updateCategory = (newId: CategoryId) => {
        root.$router.push(`/categories/${newId}`)
      }

      watch(
        () => props.currentCategoryId,
        () => {
          if (refs.el instanceof Element) {
            refs.el.scrollIntoView({ block: 'start', behavior: 'smooth' })
          }
        }
      )

      const goBack = () => {
        root.$router.back()
      }

      const items = computed(() => {
        if (!category.value) {
          return []
        }
        if (category.value.__typename === 'ProductCategory') {
          return category.value?.products.map((product) => ({
            id: product.id,
            name: product.name,
            image: product.image,
            link: `/products/${product.id}`
          }))
        } else if (category.value.__typename === 'ServiceCategory') {
          return category.value?.services.map((service) => ({
            id: service.id,
            name: service.name,
            image: service.images[0].large,
            link: `/services/${service.id}`
          }))
        } else if (category.value.__typename === 'ExclusiveCategory') {
          return category.value?.exclusives.map((exclusive) => ({
            id: exclusive.id,
            name: exclusive.name,
            image: exclusive.images[0].large,
            link: `/exclusives/${exclusive.id}`
          }))
        }
      })

      return () => (
        <div class={classes.main} ref="el">
          <MobileCatalogCategoriesNav
            onChange={updateCategory}
            currentId={currentId.value}
            categoryIds={props.categoryIds}
          />
          {root.$route.path !== '/' && (
            <div class={classes.backWrapper}>
              <BaseBackButton class={classes.back} onClick={goBack} />
            </div>
          )}
          {items.value && (
            <MobileCatalogItemsGrid class={classes.products}>
              {items.value.map((item, i) => (
                <MobileCatalogLinkImageItem
                  key={item.id}
                  item={item}
                  id={`product-link-${item.id}`}
                  lazyLoad={i > 1}
                />
              ))}
              {/* <MobileCatalogWeekSale /> */}
              <MobileCatalogWholesaler />
            </MobileCatalogItemsGrid>
          )}
          <MobileCatalogCategoriesNav
            onChange={updateCategory}
            currentId={currentId.value}
            categoryIds={props.categoryIds}
          />
        </div>
      )
    }
  })
)
