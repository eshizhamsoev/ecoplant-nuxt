import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon, BaseTinySlider } from '~/components/base'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { CategoriesQuery, CategoriesQueryVariables } from '~/_generated/types'
import { categoriesQuery } from '~/graphql/queries'
import { CategoryId, CategoryIdConstructor } from '~/common-types/catalog'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './mobile-catalog-categories-nav.scss?module'

const cn = useClassNames('mobile-catalog-categories-nav', style)

const classes = {
  main: cn()
}

// TODO make it using base-slider-select or delete

export const MobileCatalogCategoriesNav = injectStyles(
  style,
  defineComponent({
    name: 'MobileCatalogCategoriesNav',
    props: {
      currentId: {
        type: CategoryIdConstructor,
        // required: true
        default: ''
      },
      categoryIds: {
        type: Array as () => CategoryId[],
        required: true
      }
    },
    emits: ['change'],
    setup: (props, { emit }) => {
      const { result } = useGqlQuery<
        CategoriesQuery,
        Omit<CategoriesQueryVariables, 'site'>
      >(categoriesQuery, {
        ids: props.categoryIds
      })

      const categories = computed(() => {
        if (!result.value) {
          return []
        }
        return result.value.categories
      })

      const currentIndex = computed(() =>
        props.categoryIds.indexOf(props.currentId)
      )
      const changeCategory = (newIndex: number) => {
        if (categories.value[newIndex]) {
          emit('change', categories.value[newIndex].id)
        }
      }
      const modalOpener = useModalOpener()
      const openLargeOrdersModal = () => modalOpener({ name: 'large-orders' })

      return () =>
        result.value && (
          <BaseTinySlider
            class={classes.main}
            options={{
              autoWidth: true,
              loop: false,
              gutter: 0,
              swipeAngle: false,
              preventScrollOnTouch: 'force',
              startIndex: props.categoryIds.indexOf(props.currentId)
            }}
            selectedIndex={currentIndex.value}
            onClick={changeCategory}
            prevClass={cn('prev')}
            nextClass={cn('next')}
          >
            {categories.value.map((category) => (
              <div key={category.id}>
                <button class={cn('button')}>{category.name}</button>
              </div>
            ))}
            <div>
              <button onClick={openLargeOrdersModal} class={cn('button')}>
                Оптовикам
              </button>
            </div>
            <template slot="prev">
              <BaseIcon name="arrow-left" width={15} />
            </template>
            <template slot="next">
              <BaseIcon name="arrow-left" width={15} rotate={180} />
            </template>
          </BaseTinySlider>
        )
    }
  })
)
