import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './mobile-catalog-products.scss?module'

const cn = useClassNames('mobile-catalog-products', style)

const classes = {
  main: cn()
}

export const MobileCatalogItemsGrid = injectStyles(
  style,
  defineComponent({
    name: 'MobileCatalogProducts',
    setup: (props, { slots }) => {
      return () => (
        <div class={classes.main}>{slots.default && slots.default()}</div>
      )
    }
  })
)
