import { defineComponent } from '@nuxtjs/composition-api'
import { MobileCatalogItem } from '../mobile-catalog-item'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './mobile-catalog-wholesaler.scss?module'
const cn = useClassNames('mobile-catalog-wholesaler', style)

const classes = {
  main: cn(),
  headingWrapper: cn('heading-wrapper'),
  heading: cn('heading'),
  icon: cn('icon'),
  leading: cn('leading'),
  image: cn('image')
}

export const MobileCatalogWholesaler = injectStyles(
  style,
  defineComponent({
    name: 'MobileCatalogWholesaler',
    props: {},
    setup: () => {
      const openModal = useModalOpener()
      const openModalLargeOrders = () => openModal({ name: 'large-orders' })
      return () => (
        <MobileCatalogItem
          class={classes.main}
          name="Индивидуальные скидки"
          onClick={openModalLargeOrders}
        >
          <div class={classes.main}>
            <div class={classes.heading}>оптовикам</div>
          </div>
        </MobileCatalogItem>
      )
    }
  })
)
