import { defineComponent } from '@nuxtjs/composition-api'
import { MobileCatalogItem } from '../mobile-catalog-item'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseLazyLoadImage } from '~/components/base/base-lazy-load-image/base-lazy-load-image'
import style from './mobile-catalog-product.scss?module'

const cn = useClassNames('mobile-catalog-product', style)

const classes = {
  main: cn(),
  image: cn('image')
}

type Item = {
  image: string
  name: string
  link: string
}

export const MobileCatalogLinkImageItem = injectStyles(
  style,
  defineComponent({
    name: 'MobileCatalogLinkImageItem',
    props: {
      item: {
        type: Object as () => Item,
        required: true
      },
      lazyLoad: {
        type: Boolean,
        default: true
      }
    },
    setup: (props) => {
      return () => (
        <MobileCatalogItem
          class={classes.main}
          to={props.item.link}
          name={props.item.name}
        >
          {props.lazyLoad ? (
            <BaseLazyLoadImage
              src={props.item.image}
              width={138}
              height={145}
              class={classes.image}
            />
          ) : (
            <img
              src={props.item.image}
              width={138}
              height={145}
              class={classes.image}
              alt={''}
            />
          )}
        </MobileCatalogItem>
      )
    }
  })
)
