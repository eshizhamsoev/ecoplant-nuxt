import { defineComponent } from '@nuxtjs/composition-api'
import { MobileCatalogItem } from '../mobile-catalog-item'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import style from './mobile-catalog-week-sale.scss?module'

const cn = useClassNames('mobile-catalog-week-sale', style)

const classes = {
  main: cn(),
  headingWrapper: cn('heading-wrapper'),
  heading: cn('heading'),
  icon: cn('icon'),
  leading: cn('leading')
}

export const MobileCatalogWeekSale = injectStyles(
  style,
  defineComponent({
    name: 'MobileCatalogWeekSale',
    props: {},
    setup: () => {
      return () => (
        <MobileCatalogItem class={classes.main} to="/sale" name="Акция недели">
          <div class={classes.headingWrapper}>
            <div class={classes.heading}>ликвидация</div>
            <div class={classes.leading}>
              -70
              <BaseIcon
                class={classes.icon}
                name="click"
                width={30}
                height={30}
              />
              %
            </div>
          </div>
        </MobileCatalogItem>
      )
    }
  })
)
