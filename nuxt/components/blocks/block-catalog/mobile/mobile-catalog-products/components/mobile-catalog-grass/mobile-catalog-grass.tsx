import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobileCatalogItem } from '~/components/blocks/block-catalog/mobile/mobile-catalog-products/components/mobile-catalog-item'
import style from './mobile-catalog-grass.scss?module'

const cn = useClassNames('mobile-catalog-grass', style)

const classes = {
  main: cn()
}

export const MobileCatalogGrass = injectStyles(
  style,
  defineComponent({
    name: 'MobileCatalogGrass',
    props: {},
    setup: () => {
      return () => (
        <MobileCatalogItem
          class={classes.main}
          to="/grass"
          name="Газоны"
          style={{
            backgroundImage:
              'url(https://ecoplant-pitomnik.ru/image/catalog/landings/grass/mobile-item-image.jpg)'
          }}
        />
      )
    }
  })
)
