import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { decodeHTMLEntities } from '~/support/utils/decode-entities'
import { BaseLink } from '~/components/base/base-link'
import style from './mobile-catalog-item.scss?module'

const cn = useClassNames('mobile-catalog-item', style)

const classes = {
  main: cn(),
  wrapper: cn('wrapper'),
  name: cn('name'),
  word: cn('word')
}

export const MobileCatalogItem = injectStyles(
  style,
  defineComponent({
    name: 'MobileCatalogItem',
    props: {
      name: {
        type: String,
        required: true
      },
      to: {
        type: String,
        required: false,
        default: null
      }
    },
    setup: (props, { slots, emit }) => {
      const words = computed(() => {
        const items = decodeHTMLEntities(props.name).split(/\s/g)
        const words = [] as string[]
        let max = 8
        let word = ''
        items.forEach((item) => {
          if (!word) {
            word = item
            if (word.length > max) {
              max = word.length
            }
          } else if (item.length + word.length > max) {
            words.push(word)
            word = item
            if (word.length > max) {
              max = word.length
            }
          } else {
            word += ' ' + item
          }
        })
        words.push(word)
        return words
      })
      const Tag = props.to ? BaseLink : 'span'
      return () => (
        <Tag to={props.to} class={classes.main} onClick={() => emit('click')}>
          {slots.default && slots.default()}
          <div class={classes.name}>
            {words.value.map((word) => (
              <div class={classes.word}>
                <span>{word}</span>
              </div>
            ))}
          </div>
        </Tag>
      )
    }
  })
)
