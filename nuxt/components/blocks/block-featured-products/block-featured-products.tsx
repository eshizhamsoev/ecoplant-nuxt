import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { featuredProductsQuery } from '~/graphql/queries'
import { cnThemeWrapper } from '~/components/theme/theme-wrapper'
import { BlockFeaturedProductItem } from '~/components/blocks/block-featured-product-item'
import { WholeSaleButton } from '~/components/blocks/block-categories/desktop/wholesale-button/wholesale-button'
import style from './block-featured-products.scss?module'

const cn = useClassNames('block-featured-products', style)

export const BlockFeaturedProducts = injectStyles(
  style,
  defineComponent({
    name: 'BlockFeaturedProducts',
    props: {},
    setup: () => {
      const query = useGqlQuery(featuredProductsQuery, {})
      const products = computed(
        () => query.result.value?.featuredProducts || []
      )
      return () =>
        products.value && (
          <div ref="root" class={[cn(), cnThemeWrapper()]}>
            <div class={cn('header')}>Популярные товары</div>
            <div class={cn('products')}>
              {products.value.map((item) => (
                <BlockFeaturedProductItem item={item} />
              ))}
              <WholeSaleButton />
            </div>
          </div>
        )
    }
  })
)
