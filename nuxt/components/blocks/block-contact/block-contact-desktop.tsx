import { defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ContactItem } from '~/components/blocks/block-footer/footer-contacts/contact-item'
import { BaseIcon } from '~/components/base'
import { FooterButton } from '~/components/blocks/block-footer/footer-button'
import { BaseMap } from '~/components/base/base-map/base-map'
import { useIntersectedOnce } from '~/support/hooks/useIntersectedOnce'
import { EmailLink } from '~/components/action-providers/email-link'
import { PhoneLink } from '~/components/action-providers/phone-link'
import style from './block-contact-desktop.scss?module'

const cn = useClassNames('block-contact-desktop', style)

export const BlockContactDesktop = injectStyles(
  style,
  defineComponent({
    name: 'BlockContactDesktop',
    props: {
      isAdditional: {
        type: Boolean,
        default: false
      },
      isLazy: {
        type: Boolean,
        default: false
      },
      showRegion: {
        type: Boolean,
        default: true
      }
    },
    setup: (props, { root, refs }) => {
      const intersected = props.isLazy
        ? useIntersectedOnce(() => refs.root as HTMLElement)
        : ref(true)

      const data = props.isAdditional
        ? ref(root.$accessor.contacts.additionalContacts)
        : ref(root.$accessor.contacts)
      const region = props.isAdditional
        ? root.$accessor.contacts.additionalContacts.region
        : root.$accessor.contacts.currentRegion

      return () => (
        <div class={cn()} ref="root">
          <div class={cn('group-contacts')}>
            <ContactItem class={cn('item')}>
              <template slot="icon">
                <BaseIcon name="point" width={18} height={26} />
              </template>
              {props.showRegion && <template slot="heading">{region}</template>}
              <template slot="text">{data.value.address}</template>
            </ContactItem>
            <ContactItem class={cn('item')}>
              <template slot="icon">
                <BaseIcon name="phone" width={18} height={28} />
              </template>
              <template slot="heading">
                <PhoneLink isAdditional={props.isAdditional} class={cn('link')}>
                  {data.value.phone}
                </PhoneLink>
              </template>
              <template slot="text">
                <EmailLink class={cn('link')} email={data.value.email}>
                  {data.value.email}
                </EmailLink>{' '}
                – для клиентов <br />{' '}
                <EmailLink class={cn('link')} email={'work@eco-plant.ru'}>
                  work@eco-plant.ru
                </EmailLink>{' '}
                – для предложений
              </template>
            </ContactItem>
          </div>
          <div class={cn('map-wrapper')}>
            <div class={cn('map-clip-path')} />
            <div class={cn('button-wrapper')}>
              <FooterButton class={cn('button')} point={data.value.geoPoint} />
            </div>
            <div class={cn('map')}>
              {intersected.value && (
                <BaseMap
                  class={cn('map')}
                  address={data.value.address}
                  longitude={data.value.geoPoint.longitude}
                  latitude={data.value.geoPoint.latitude}
                />
              )}
            </div>
          </div>
        </div>
      )
    }
  })
)
