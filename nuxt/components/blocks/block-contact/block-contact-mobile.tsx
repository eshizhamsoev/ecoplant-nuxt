import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { FooterButton } from '~/components/blocks/block-footer/footer-button'
import { BaseMap } from '~/components/base/base-map/base-map'
import { EmailLink } from '~/components/action-providers/email-link'
import { PhoneLink } from '~/components/action-providers/phone-link'
import style from './block-contact-mobile.scss?module'

const cn = useClassNames('block-contact-mobile', style)

export const BlockContactMobile = injectStyles(
  style,
  defineComponent({
    name: 'BlockContactMobile',
    props: {
      isAdditional: {
        type: Boolean,
        default: false
      }
    },
    setup: (props, { root }) => {
      const data = computed(() => {
        return props.isAdditional
          ? root.$accessor.contacts.additionalContacts
          : root.$accessor.contacts
      })
      const region = props.isAdditional
        ? root.$accessor.contacts.additionalContacts.region
        : root.$accessor.contacts.currentRegion
      return () => (
        <div class={cn()}>
          <div class={cn('header')}>Контакты {region}</div>
          <div class={cn('data')}>
            <BaseIcon class={cn('icon')} name="phone" width={18} />
            <PhoneLink isAdditional={props.isAdditional} class={cn('phone')}>
              {data.value.phone}
            </PhoneLink>
            <BaseIcon class={cn('icon')} name="mail-filled" width={20} />
            <div class={cn('emails')}>
              <div class={cn('email')}>
                <EmailLink email={data.value.email} class={cn('email-address')}>
                  {data.value.email}
                </EmailLink>
                <span class={cn('email-text')}> - для клиентов</span>
              </div>
              <div class={cn('email')}>
                <EmailLink
                  email={'work@eco-plant.ru'}
                  class={cn('email-address')}
                >
                  work@eco-plant.ru
                </EmailLink>
                <span class={cn('email-text')}> - для предложений</span>
              </div>
            </div>
            <BaseIcon class={cn('icon')} name="point" width={20} />
            <div class={cn('address')}>{data.value.address}</div>
          </div>
          <div class={cn('map-wrapper')}>
            <div class={cn('map-clip-path')} />
            <div class={cn('button-wrapper')}>
              <FooterButton class={cn('button')} point={data.value.geoPoint} />
            </div>
            <div class={cn('map')}>
              <BaseMap
                class={cn('map')}
                address={data.value.address}
                longitude={data.value.geoPoint.longitude}
                latitude={data.value.geoPoint.latitude}
              />
            </div>
          </div>
        </div>
      )
    }
  })
)
