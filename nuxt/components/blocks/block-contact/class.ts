export type Address = {
  region: string
  phone: string
  emailClient: string
  emailSuggestion: string
  address: string
  geoPoint: { latitude: number; longitude: number }
}
