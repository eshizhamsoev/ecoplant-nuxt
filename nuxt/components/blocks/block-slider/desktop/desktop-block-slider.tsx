import { defineComponent } from '@nuxtjs/composition-api'
import { BaseTinySlider } from '~/components/base/base-tiny-slider'
import { BaseIcon } from '~/components/base/base-icon'
import { useClassNames } from '~/support/utils/bem-classnames'
import { cnBaseRoundButton } from '~/components/base/base-round-button'
import { cnThemeButton, Color } from '~/components/theme/theme-button'
import { SliderBadge } from '~/components/blocks/block-slider/slider-badge'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePopupGallery } from '~/components/base/base-popup-gallery'
import { usePopupGallery } from '~/components/base/base-popup-gallery/usePopupGallery'
import { blockSliderProps } from '~/components/blocks/block-slider/props'
import { useBlockSlider } from '~/components/blocks/block-slider/useBlockSlider'
import { vOnceFabric } from '~/support/utils/vOnce'
import style from './desktop-block-slider.scss?module'

const cn = useClassNames('desktop-block-slider', style)

const options = {
  autoWidth: true,
  center: true,
  gutter: 50,
  lazyload: true
}

const roundButtonClass = cnBaseRoundButton({ size: 'm' })
const themeButtonClass = cnThemeButton({ color: Color.green })

const classes = {
  main: cn(),
  prev: [cn('prev'), roundButtonClass, themeButtonClass].join(' '),
  next: [cn('next'), roundButtonClass, themeButtonClass].join(' '),
  icon: cn('icon'),
  slider: cn('slider'),
  badge: cn('badge'),
  heading: cn('heading'),
  leading: cn('leading')
}

export const DesktopBlockSlider = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlockSlider',
    props: blockSliderProps,
    setup: (props) => {
      const { sliderImages, galleryImages } = useBlockSlider(props)

      const {
        open: openPopupGallery,
        close: closePopupGallery,
        index: popupIndex
      } = usePopupGallery()

      const sliderFabric = vOnceFabric(() => (
        <BaseTinySlider
          options={options}
          class={classes.slider}
          prevClass={classes.prev}
          nextClass={classes.next}
          onClick={openPopupGallery}
        >
          {sliderImages.value.map((image) => (
            <div key={image.src}>
              <img
                data-src={image.src}
                alt=""
                class="tns-lazy-img"
                width={image.width}
                height={image.height}
              />
            </div>
          ))}
          <template slot="prev">
            <BaseIcon name="chevron-left" class={classes.icon} width={18} />
          </template>
          <template slot="next">
            <BaseIcon
              name="chevron-left"
              rotate={180}
              class={classes.icon}
              width={18}
            />
          </template>
        </BaseTinySlider>
      ))
      return () => (
        <div class={classes.main}>
          {props.heading && <div class={classes.heading}>{props.heading}</div>}
          {props.leading && <div class={classes.leading}>{props.leading}</div>}
          {sliderFabric()}
          {props.label && <SliderBadge class={classes.badge} />}
          <BasePopupGallery
            images={galleryImages.value}
            index={popupIndex.value}
            onClose={closePopupGallery}
          />
        </div>
      )
    }
  })
)
