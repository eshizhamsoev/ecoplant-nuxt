import { computed } from '@nuxtjs/composition-api'
import { BlockSliderProps } from '~/components/blocks/block-slider/props'

export const useBlockSlider = (props: BlockSliderProps) => {
  const sliderImages = computed(() => props.images.map((image) => image.small))

  const galleryImages = computed(() =>
    props.images.map((image) => ({
      small: image.small.src,
      large: image.large
    }))
  )
  return { sliderImages, galleryImages }
}
