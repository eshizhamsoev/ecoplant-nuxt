type Image = {
  small: {
    src: string
    width: number
    height: number
  }
  large: string
}

export const blockSliderProps = {
  heading: {
    type: String,
    default: null
  },
  leading: {
    type: String,
    default: null
  },
  images: {
    type: Array as () => Image[],
    required: true as true
  },
  label: {
    type: Boolean,
    default: true
  }
}

export type BlockSliderProps = {
  heading: string | null
  leading: string | null
  images: Image[]
}
