import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePopupGallery } from '~/components/base/base-popup-gallery'
import { useBlockSlider } from '~/components/blocks/block-slider/useBlockSlider'
import { usePopupGallery } from '~/components/base/base-popup-gallery/usePopupGallery'
import { blockSliderProps } from '~/components/blocks/block-slider/props'
import { vOnceFabric } from '~/support/utils/vOnce'
import { BaseSliderWithButtons } from '~/components/base/base-slider-with-buttons'
import style from './mobile-block-slider.scss?module'

const cn = useClassNames('mobile-block-slider', style)

const options = {
  gutter: 10,
  lazyload: true,
  center: true,
  fixedWidth: 240
}

const classes = {
  main: cn(),
  slider: cn('slider'),
  badge: cn('badge'),
  heading: cn('heading'),
  leading: cn('leading'),
  item: cn('item'),
  image: cn('image')
}

export const MobileBlockSlider = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockSlider',
    props: blockSliderProps,
    setup: (props) => {
      const { sliderImages, galleryImages } = useBlockSlider(props)

      const {
        open: openPopupGallery,
        close: closePopupGallery,
        index: popupIndex
      } = usePopupGallery()

      const sliderFabric = vOnceFabric(() => (
        <BaseSliderWithButtons
          options={options}
          class={classes.slider}
          onClick={openPopupGallery}
        >
          {sliderImages.value.map((image) => (
            <div class={classes.item}>
              <img
                width={image.width}
                height={image.height}
                data-src={image.src}
                alt=""
                class={[classes.image, 'tns-lazy-img']}
              />
            </div>
          ))}
        </BaseSliderWithButtons>
      ))

      return () => (
        <div class={classes.main} ref="main">
          {(props.heading || props.leading) && (
            <div class={classes.heading}>
              {props.heading && <b>{props.heading}</b>}{' '}
              {props.leading && props.leading}
            </div>
          )}
          {sliderFabric()}
          <BasePopupGallery
            images={galleryImages.value}
            index={popupIndex.value}
            showThumbnails={false}
            onClose={closePopupGallery}
          />
        </div>
      )
    }
  })
)
