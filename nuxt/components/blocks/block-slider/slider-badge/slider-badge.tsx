import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { BaseIcon } from '~/components/base/base-icon'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './slider-badge.scss?module'

const cn = useClassNames('slider-badge', style)

const classes = {
  main: cn(),
  icon: cn('icon'),
  text: cn('text')
}

export const SliderBadge = injectStyles(
  style,
  defineComponent({
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <BaseIcon name="forest" class={classes.icon} width={36} />
          <div class={classes.text}>
            Крупнейший
            <br />
            питомник растений
            <br />в России
          </div>
        </div>
      )
    }
  })
)
