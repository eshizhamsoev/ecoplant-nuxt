import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const BlockCategories = UtilUniversalComponentFactory(
  'BlockCategories',
  () => import('./mobile/index').then((c) => c.MobileBlockCategories),
  () => import('./desktop/index').then((c) => c.DesktopBlockCategories)
)
