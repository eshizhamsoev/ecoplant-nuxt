import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { blockCategoriesProps } from '~/components/blocks/block-categories/props'
import { BaseButton, BaseIcon } from '~/components/base'
import { BaseLazyLoadImage } from '~/components/base/base-lazy-load-image'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { CategoriesQuery, CategoriesQueryVariables } from '~/_generated/types'
import { categoriesQuery } from '~/graphql/queries'
import { BaseLink } from '~/components/base/base-link'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './mobile-block-categories.scss?module'

const cn = useClassNames('mobile-block-categories', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  items: cn('items'),
  item: cn('item'),
  image: cn('image'),
  itemTitle: cn('item-title'),
  button: cn('button'),
  buttonWrapper: cn('button-wrapper'),
  buttonIcon: cn('button-icon'),
  itemBtn: cn('item', { type: 'button' })
}

export const MobileBlockCategories = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockCategories',
    props: blockCategoriesProps,
    setup(props) {
      const { result } = useGqlQuery<
        CategoriesQuery,
        Omit<CategoriesQueryVariables, 'site'>
      >(categoriesQuery, {
        ids: props.ids
      })

      const categories = computed(() => {
        if (!result.value) {
          return []
        }
        return result.value.categories.map((category) => ({
          ...category,
          link: `/categories/${category.id}#category-page`
        }))
      })
      const openModal = useModalOpener()
      const openModalLargeOrders = () => openModal({ name: 'large-orders' })
      return () =>
        result.value && (
          <div class={classes.main} ref="main">
            <div class={classes.heading}>
              Также в нашем питомнике вы можете приобрести:
            </div>
            <div class={classes.items}>
              {categories.value.map((category) => (
                <BaseLink to={category.link} class={classes.item}>
                  <BaseLazyLoadImage
                    width={60}
                    height={60}
                    src={category.image}
                    alt={category.name}
                    class={classes.image}
                  />
                  <div class={classes.itemTitle}>{category.name}</div>
                  <div class={classes.buttonWrapper}>
                    <span class={classes.button}>
                      Ассортимент
                      <br />и цены
                      <BaseIcon
                        rotate={180}
                        name="arrow-left"
                        width={15}
                        class={classes.buttonIcon}
                      />
                    </span>
                  </div>
                </BaseLink>
              ))}
              <BaseButton
                class={classes.itemBtn}
                onClick={openModalLargeOrders}
                size={'custom'}
              >
                <div>оптовикам</div>
              </BaseButton>
            </div>
          </div>
        )
    }
  })
)
