import { CategoryId } from '~/common-types/catalog'

export const blockCategoriesProps = {
  ids: {
    type: Array as () => CategoryId[],
    default: () => {
      return ['2', '3', '1', '4', '5']
    }
  },
  showBlockHeading: {
    type: Boolean,
    default: true
  }
}
