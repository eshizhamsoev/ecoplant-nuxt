import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { blockCategoriesProps } from '~/components/blocks/block-categories/props'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { CategoriesQuery, CategoriesQueryVariables } from '~/_generated/types'
import { categoriesQuery } from '~/graphql/queries'
import { BaseLink } from '~/components/base/base-link'
import { WholeSaleButton } from '~/components/blocks/block-categories/desktop/wholesale-button/wholesale-button'
import style from './desktop-block-categories.scss?module'

const cn = useClassNames('desktop-block-categories', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  items: cn('items'),
  item: cn('item'),
  image: cn('image'),
  itemTitle: cn('item-title')
}

export const DesktopBlockCategories = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlockCategories',
    props: blockCategoriesProps,
    setup(props) {
      const vars = computed(() => ({
        ids: props.ids
      }))
      const { result } = useGqlQuery<
        CategoriesQuery,
        Omit<CategoriesQueryVariables, 'site'>
      >(categoriesQuery, vars)
      return () =>
        result.value && (
          <div class={classes.main}>
            {props.showBlockHeading && (
              <div class={classes.heading}>
                Также в нашем питомнике вы можете приобрести:
              </div>
            )}
            <div class={classes.items}>
              {result.value.categories.map((category) => (
                <BaseLink
                  class={classes.item}
                  to={`/categories/${category.id}#category-page`}
                >
                  <img
                    src={category.image}
                    alt={category.name}
                    class={classes.image}
                    loading={'lazy'}
                    height={210}
                    width={175}
                  />
                  <div class={classes.itemTitle}>{category.name}</div>
                </BaseLink>
              ))}
              <WholeSaleButton class={classes.item} />
            </div>
          </div>
        )
    }
  })
)
