import { defineComponent } from '@nuxtjs/composition-api'
import percent from '../../images/persent.svg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './wholesale-button.scss?module'

const cn = useClassNames('wholesale-button', style)

const classes = {
  main: cn(),
  saleTitle: cn('title'),
  saleSubTitle: cn('subtitle'),
  itemInner: cn('inner'),
  imagePercent: cn('image', { type: 'percent' })
}

export const WholeSaleButton = injectStyles(
  style,
  defineComponent({
    name: 'WholeSaleButton',
    setup() {
      const openModal = useModalOpener()
      const openModalLargeOrders = () => openModal({ name: 'large-orders' })
      return () => (
        <button class={classes.main} onClick={openModalLargeOrders}>
          <span class={classes.itemInner}>
            <img
              class={classes.imagePercent}
              src={percent}
              alt=""
              width="42"
              height="37"
            />
            <span class={classes.saleTitle}>ОПТОВИКАМ</span>
            <span class={classes.saleSubTitle}>Индивидуальные скидки</span>
          </span>
        </button>
      )
    }
  })
)
