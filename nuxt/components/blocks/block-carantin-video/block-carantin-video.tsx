import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseVideoBlockWithButton } from '~/components/base'
import style from './block-carantin-video.scss?module'

const cn = useClassNames('block-carantin-video', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  video: cn('video'),
  videoButtonText: cn('video-button-text')
}

export const BlockCarantinVideo = injectStyles(
  style,
  defineComponent({
    name: 'BlockCarantinVideo',
    props: {},
    setup: (props, { root }) => {
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>
            Карантин :( Бесконтактная доставка и посадка деревьев. Скидка 70%
          </div>
          <BaseVideoBlockWithButton
            class={classes.video}
            video={
              'https://ecoplant-pitomnik.ru/files/videos/contactless-planting.mp4'
            }
            poster={
              'https://ecoplant-pitomnik.ru/image/catalog/blocks/contactless-planting/poster.jpg'
            }
            buttonVisual={{
              animate: true,
              size: root.$device.isMobileOrTablet ? 100 : 250
            }}
            classButtonText={classes.videoButtonText}
            buttonText={
              'Посмотрите как мы\n доставляем и сажаем\n деревья в карантин'
            }
          />
        </div>
      )
    }
  })
)
