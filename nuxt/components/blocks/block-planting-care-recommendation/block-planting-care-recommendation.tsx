import { h } from '@vue/composition-api'
import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { cnThemeWrapper } from '~/components/theme/theme-wrapper'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './block-planting-care-recommendation.scss?module'

const cn = useClassNames('block-planting-care-recommendation', style)

const classes = {
  main: [cn(), cnThemeWrapper()],
  item: cn('item'),
  image: cn('image'),
  imageWrapper: cn('image-wrapper'),
  icon: cn('icon'),
  title: cn('title')
}

interface ItemLink {
  image: string
  title: string
  link: string
}

interface ItemButton {
  image: string
  title: string
  onClick: () => {}
}

type BaseItemProps = {
  class: string
}

type ItemLinkProps = BaseItemProps & {
  href: string
}

type Item = ItemLink | ItemButton

const isItemLink = function (item: Item): item is ItemLink {
  return (item as ItemLink).link !== undefined
}

const isItemButton = function (item: Item): item is ItemButton {
  return (item as ItemButton).onClick !== undefined
}

export const BlockPlantingCareRecommendation = injectStyles(
  style,
  defineComponent({
    name: 'BlockPlantingCareRecommendation',
    props: {},
    setup: () => {
      const modalOpener = useModalOpener()
      const openHowToWater = () => modalOpener({ name: 'how-to-water' })
      const items = [
        {
          image:
            'https://ecoplant-pitomnik.ru/image/catalog/landings/care-recommendations/01.jpg',
          title: 'Как правильно нужно поливать',
          onClick: openHowToWater
        },
        {
          image:
            'https://ecoplant-pitomnik.ru/image/catalog/landings/care-recommendations/02.jpg',
          title:
            'Если у вас посажены ели, сосны или пихты на участке, посмотрите что нужно делать',
          link: 'http://xn--1-gtbctuan.xn--p1ai/'
        },
        {
          image:
            'https://ecoplant-pitomnik.ru/image/catalog/landings/care-recommendations/03.jpg',
          title:
            'Какие уходные мероприятия нужно проводить со всеми растениями в вашем саду в Мае',
          link: 'http://may.xn--1-gtbctuan.xn--p1ai/'
        },
        {
          image:
            'https://ecoplant-pitomnik.ru/image/catalog/landings/care-recommendations/04.jpg',
          title: 'Также в нашем питомнике вы можете приобрести',
          link: 'https://xn--80atdjcgy5f.xn--p1ai/#BlockContent1'
        }
      ] as Item[]
      return () => (
        <div class={classes.main}>
          {items.map((item) => {
            const attrs = {
              class: classes.item
            } as ItemLinkProps
            const on = {} as { [key: string]: Function | Function[] }
            if (isItemButton(item)) {
              on.click = item.onClick
            } else if (isItemLink(item)) {
              attrs.href = item.link
            } else {
              return ''
            }
            return h(attrs.href ? 'a' : 'div', { attrs, on }, [
              <div class={classes.imageWrapper}>
                <img src={item.image} alt={item.title} class={classes.image} />
                <BaseIcon
                  name="external-link"
                  width={30}
                  class={classes.icon}
                />
              </div>,
              <div class={classes.title}>{item.title}</div>
            ])
          })}
        </div>
      )
    }
  })
)
