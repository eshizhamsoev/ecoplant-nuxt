import { defineComponent } from '@nuxtjs/composition-api'
import bg from './resources/bg.jpg'
import bgMobile from './resources/bg-mobile.jpg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { BaseButton } from '~/components/base/base-button'
import { injectStyles } from '~/support/utils/injectStyle'
import { Color } from '~/components/theme/theme-button'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './block-director.scss?module'

const cn = useClassNames('block-director', style)

const classes = {
  main: cn(),
  imageWrapper: cn('image-wrapper'),
  image: cn('image'),
  content: cn('content'),
  heading: cn('heading'),
  text: cn('text'),
  buttonWrapper: cn('button-wrapper'),
  button: cn('button')
}

export const BlockDirector = injectStyles(
  style,
  defineComponent({
    name: 'BlockDirector',
    props: {
      stamp: {
        type: String,
        required: true
      }
    },
    setup: () => {
      const modalOpener = useModalOpener()
      const openUnsubscribeModal = () => modalOpener({ name: 'unsubscribe' })
      const openModal = () =>
        modalOpener({
          name: 'director'
        })
      return () => (
        <div class={classes.main}>
          <div class={classes.content}>
            <div class={classes.heading}>
              Добрый день, <br /> уважаемые клиенты
            </div>
            <div class={classes.text}>
              Здесь вы можете написать мне, собственнику бизнеса, напрямую.
              Отвечаю на письма каждый понедельник и четверг. Принимаем к
              рассмотрению только те письма, в которых указан номер заказа.
              Оставляйте номера телефонов, в индивидуальном порядке я могу
              перезвонить.
            </div>
            <div class={classes.buttonWrapper}>
              <BaseButton
                size="custom"
                onClick={openModal}
                class={classes.button}
              >
                Написать директору
              </BaseButton>
              <BaseButton
                size="custom"
                onClick={openUnsubscribeModal}
                color={Color.yellow}
                class={classes.button}
              >
                Отписаться от рассылки
              </BaseButton>
            </div>
          </div>
          <div class={classes.imageWrapper}>
            <picture>
              <source media="(max-width: 768px)" srcset={bgMobile} />
              <img src={bg} alt="" class={classes.image} loading="lazy" />
            </picture>
          </div>
        </div>
      )
    }
  })
)
