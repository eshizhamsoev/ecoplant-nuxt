import { defineComponent } from '@nuxtjs/composition-api'
import director from './resources/director.jpg'
import directorX2 from './resources/directorx2.jpg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
// @ts-ignore
import sign from '~/components/blocks/block-director/resources/sign.png?resize&size=72'
import { BaseButton } from '~/components/base/base-button'
import { BaseLazyLoadImage } from '~/components/base/base-lazy-load-image'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { Color } from '~/components/theme/theme-button'
import style from './mobile-block-director.scss?module'

const cn = useClassNames('mobile-block-director', style)

const classes = {
  main: cn(),
  imageWrapper: cn('image-wrapper'),
  image: cn('image'),
  content: cn('content'),
  cite: cn('cite'),
  heading: cn('heading'),
  text: cn('text'),
  buttonWrapper: cn('button-wrapper'),
  button: cn('button'),
  stampWrapper: cn('stamp-wrapper'),
  stamp: cn('stamp'),
  sign: cn('sign')
}

export const MobileBlockDirector = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockDirector',
    props: {
      stamp: {
        type: String,
        required: true
      }
    },
    setup: (props) => {
      const modalOpener = useModalOpener()
      const openModal = () =>
        modalOpener({
          name: 'director'
        })
      const openUnsubscribeModal = () => modalOpener({ name: 'unsubscribe' })
      return () => (
        <div class={classes.main}>
          <div class={classes.content}>
            <div class={classes.cite}>
              <div class={classes.heading}>Добрый день, уважаемые клиенты</div>
              <div class={classes.text}>
                Здесь вы можете написать мне, собственнику бизнеса, напрямую.
                Отвечаю на письма каждый понедельник и четверг. Принимаем к
                рассмотрению только те письма, в которых указан номер заказа.
                Оставляйте номера телефонов, в индивидуальном порядке я могу
                перезвонить.
              </div>
            </div>
          </div>
          <div class={classes.imageWrapper}>
            <div class={classes.stampWrapper}>
              <BaseLazyLoadImage
                src={props.stamp}
                alt=""
                class={classes.stamp}
                width={80}
                height={80}
              />
              <BaseLazyLoadImage
                src={sign.toString()}
                alt="Подпись директора"
                class={classes.sign}
                width={72}
                height={59}
              />
            </div>
            <div class={classes.buttonWrapper}>
              <BaseButton size="m" class={classes.button} onClick={openModal}>
                Написать директору
              </BaseButton>
              <BaseButton
                size="m"
                class={classes.button}
                color={Color.yellow}
                onClick={openUnsubscribeModal}
              >
                Отписаться от рассылки
              </BaseButton>
            </div>
            {/* <BaseLazyLoadImage */}
            {/*  src={director.toString()} */}
            {/*  width={375} */}
            {/*  height={258} */}
            {/*  alt="Афонина Анастасия, Генеральный директор" */}
            {/*  class={classes.image} */}
            {/* /> */}
            <img
              srcset={`${director}, ${directorX2} 1.5x`}
              width={375}
              height={258}
              alt="Афонина Анастасия, Генеральный директор"
              class={classes.image}
              src={director}
              loading="lazy"
            />
          </div>
        </div>
      )
    }
  })
)
