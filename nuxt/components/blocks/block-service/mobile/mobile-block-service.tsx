import { computed, defineComponent, PropType } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { AbstractService } from '~/_generated/types'
import { BaseButton } from '~/components/base'
import { MobileImageSlider } from '~/components/blocks/shared-components/mobile-image-slider'
import { MobileProductName } from '~/components/blocks/shared-components/mobile-product-name'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { BlockMobileFixedLine } from '~/components/blocks/block-mobile-fixed-line'
import { MobileProductTopLine } from '~/components/blocks/shared-components/mobile-product-top-line/mobile-product-top-line'
import { Visual } from '~/components/theme/theme-button'
import style from './mobile-block-service.scss?module'

const cn = useClassNames('mobile-block-service', style)

export const MobileBlockService = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockService',
    props: {
      service: {
        type: Object as PropType<AbstractService & { __typename: string }>,
        required: true
      }
    },
    setup: (props) => {
      const modalOpener = useModalOpener()
      const openDescriptionModal = () => {
        modalOpener({ name: 'service', serviceId: props.service.id })
      }
      const openCallbackModal = () => modalOpener({ name: 'callback' })

      const images = computed(() => {
        return props.service.images.map((image) => ({
          small: image.large,
          large: image.large
        }))
      })

      return () => (
        <div class={cn()}>
          <MobileProductTopLine />
          <MobileProductName
            class={cn('heading-line')}
            title={props.service.name}
            onShowDescriptionClick={openDescriptionModal}
          />
          <MobileImageSlider images={images.value} class={cn('images')} />
          {props.service?.__typename === 'Service' && (
            <div class={cn('description-wrapper')}>
              <BaseButton
                size="m"
                class={cn('description-button')}
                visual={Visual.outline}
                onClick={openDescriptionModal}
              >
                открыть описание
              </BaseButton>
            </div>
          )}
          <div class={cn('price')}>
            <div class={cn('price-label')}>
              {props.service.price
                ? `Цена: ${props.service.price}`
                : 'Цена по запросу'}
            </div>
            <BaseButton
              size="m"
              class={cn('button')}
              onClick={openCallbackModal}
            >
              {props.service.price ? 'Заказать сейчас' : 'Узнай цену сейчас'}
            </BaseButton>
          </div>
          <BlockMobileFixedLine withCart={true} />
        </div>
      )
    }
  })
)
