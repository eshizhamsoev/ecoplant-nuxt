import { defineComponent, PropType } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ServiceInfo } from '~/components/blocks/shared-components/service-info/service-info'
import { Exclusive, Service } from '~/_generated/types'
import style from './desktop-block-service.scss?module'

const cn = useClassNames('desktop-block-service', style)

export const DesktopBlockService = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlockService',
    props: {
      service: {
        type: Object as PropType<Service | Exclusive>,
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={cn()}>
          <h1 class={cn('heading')}>{props.service.name}</h1>
          <ServiceInfo service={props.service} class={cn('info')} />
        </div>
      )
    }
  })
)
