import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './list-menu.scss?module'

const cn = useClassNames('list-menu', style)

type Item = {
  id: string
  name: string
}

export const ListMenu = injectStyles(
  style,
  defineComponent({
    name: 'ListMenu',
    props: {
      value: {
        type: String,
        required: true
      },
      items: {
        type: Array as () => Item[],
        required: true
      },
      maxRows: {
        type: Number,
        default: 8
      }
    },
    setup(props, { emit }) {
      const len = computed(() => props.items.length)
      const twoColumns = computed(() => len.value > props.maxRows)
      const orderedItems = computed(() => {
        if (!twoColumns.value) {
          return props.items
        }
        const items = new Array(len.value)
        const half = Math.ceil(len.value / 2)
        props.items.forEach((id, index) => {
          items[index >= half ? (index - half) * 2 + 1 : index * 2] = id
        })
        return items
      })

      const mainClass = computed(() => {
        return cn({ 'two-columns': twoColumns.value })
      })

      return () => (
        <div class={mainClass.value}>
          {orderedItems.value.map((item) => (
            <button
              type="button"
              key={item.id}
              class={cn('item', { active: item.id === props.value })}
              onClick={() => emit('input', item.id)}
            >
              {item.name}
            </button>
          ))}
        </div>
      )
    }
  })
)
