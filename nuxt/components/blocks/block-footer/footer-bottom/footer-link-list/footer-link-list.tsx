import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './footer-links.scss?module'

const cn = useClassNames('footer-link-list', style)

type Link = {
  to: string
  text: string
}

export const FooterLinkList = injectStyles(
  style,
  defineComponent({
    name: 'FooterLinkList',
    props: {
      links: {
        type: Array as () => Link[],
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div>
          <ul class={cn('list')}>
            {props.links.map((link) => (
              <li class={cn('item')}>
                <a href={link.to} class={cn('link')}>
                  {link.text}
                </a>
              </li>
            ))}
          </ul>
        </div>
      )
    }
  })
)
