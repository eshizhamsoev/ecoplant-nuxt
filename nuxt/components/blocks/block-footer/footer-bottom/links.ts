export const links = [
  {
    to: '/info/delivery',
    text: 'Доставка'
  },
  {
    to: '/info/payment',
    text: 'Оплата'
  },
  {
    to: '/info/return',
    text: 'Возврат'
  },
  {
    to: '/info/legal',
    text: 'Юридическая информация'
  }
]
