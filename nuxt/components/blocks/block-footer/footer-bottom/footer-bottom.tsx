import { computed, defineComponent } from '@nuxtjs/composition-api'
import { FooterSocials } from './footer-socials'
import { BaseIcon } from '~/components/base/base-icon'
import { useClassNames } from '~/support/utils/bem-classnames'
import { cnThemeWrapper } from '~/components/theme/theme-wrapper'
import { injectStyles } from '~/support/utils/injectStyle'
import {
  EmailLink,
  EmailSlotData
} from '~/components/action-providers/email-link'
import {
  PhoneLink,
  PhoneSlotData
} from '~/components/action-providers/phone-link'
import { BaseButton, BaseLink } from '~/components/base'
import { Visual } from '~/components/theme/theme-button'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { FooterLinkList } from '~/components/blocks/block-footer/footer-bottom/footer-link-list/footer-link-list'
import { links } from '~/components/blocks/block-footer/footer-bottom/links'
import { navItems } from '~/components/blocks/block-footer/footer-bottom/navItems'
import style from './footer-bottom.scss?module'

const cn = useClassNames('footer-bottom', style)

const classes = {
  main: cn(),
  logo: cn('logo'),
  legal: cn('legal'),
  socials: cn('socials'),
  links: cn('links'),
  linkList: cn('link-list')
}

const email = ({ email }: EmailSlotData) => [
  <BaseIcon name="mail" width={16} class={cn('email-icon')} />,
  email
]
const phone = ({ phone: { text } }: PhoneSlotData) => [
  <BaseIcon name="phone" width={16} class={cn('phone-icon')} />,
  text
]

const year = new Date().getFullYear()

export const FooterBottom = injectStyles(
  style,
  defineComponent({
    name: 'FooterBottom',
    props: {
      withSaleFir: {
        type: Boolean,
        default: false
      },
      withSocials: {
        type: Boolean,
        default: true
      },
      withLinks: {
        type: Boolean,
        default: false
      }
    },
    setup: (props, { root }) => {
      const modalOpener = useModalOpener()
      const openModal = (modalName: any) => modalOpener({ name: modalName })
      const openCallback = () => modalOpener({ name: 'callback' })
      const openModalLargeOrders = () => modalOpener({ name: 'large-orders' })

      const legal = root.$accessor.contacts.legal
      const navItemsList = computed(() => {
        return navItems
      })
      const linkList = computed(() => {
        const items = [
          {
            to: '/privacy-policy',
            text: 'Политика конфиденциальности'
          }
        ]
        if (props.withLinks) {
          items.push(...links)
        }
        return items
      })
      return () => (
        <div class={classes.main}>
          <div class={[cn('first-line'), cnThemeWrapper()]}>
            <div class={cn('logo')}>
              <BaseIcon name="logo" width={105} class={cn('logo-icon')} />
              <div class={classes.legal}>{legal}</div>
            </div>
            <ul class={cn('nav-menu', { type: 'mobile' })}>
              {navItemsList.value.map((item) => (
                <li class={cn('nav-menu-item')}>
                  <button
                    class={cn('nav-menu-button')}
                    onClick={openModal.bind(null, item.modalName)}
                  >
                    {item.text}
                  </button>
                </li>
              ))}
              <li class={cn('nav-menu-item')}>
                <nuxt-link
                  to={{
                    path: root.$route.path,
                    query: { tabs: 'BlockVideoReviews' },
                    hash: 'tabs-BlockVideoReviews'
                  }}
                  class={cn('nav-menu-link')}
                >
                  Отзывы
                </nuxt-link>
              </li>
            </ul>
            <div class={cn('contacts')}>
              <PhoneLink scopedSlots={{ default: phone }} class={cn('phone')} />
              <EmailLink scopedSlots={{ default: email }} class={cn('email')} />
            </div>

            <BaseButton
              size="m"
              visual={Visual.outline}
              class={cn('button')}
              onClick={openCallback}
            >
              заказать звонок
            </BaseButton>
          </div>
          <div class={cn('second-line')}>
            <div class={[cnThemeWrapper(), cn('second-line-inner')]}>
              <div class={cn('second-line-top')}>
                <ul class={cn('nav-menu', { type: 'desktop' })}>
                  {navItemsList.value.map((item) => (
                    <li class={cn('nav-menu-item')}>
                      <button
                        class={cn('nav-menu-button')}
                        onClick={openModal.bind(null, item.modalName)}
                      >
                        {item.text}
                      </button>
                    </li>
                  ))}
                  <li class={cn('nav-menu-item')}>
                    <button
                      onclick={openModalLargeOrders}
                      class={cn('nav-menu-button')}
                    >
                      Оптовикам
                    </button>
                  </li>
                  <li class={cn('nav-menu-item')}>
                    <BaseLink
                      to={{
                        path: root.$route.path,
                        query: { tabs: 'BlockVideoReviews' },
                        hash: 'tabs-BlockVideoReviews'
                      }}
                      class={cn('nav-menu-link')}
                    >
                      Отзывы
                    </BaseLink>
                  </li>
                </ul>
                <div class={cn('statistic', { type: 'desktop' })}>
                  <img src={require('./stat.svg')} alt="" loading={'lazy'} />
                </div>
              </div>
              <div class={cn('second-line-bottom')}>
                <div class={cn('copyright')}>&copy; Ecoplant, 1999-{year}</div>
                <FooterLinkList
                  links={linkList.value}
                  class={cn('link-list')}
                />
                {props.withSocials ? (
                  <FooterSocials class={classes.socials} />
                ) : (
                  <div class={classes.socials} />
                )}
                <div class={cn('statistic', { type: 'mobile' })}>
                  <img src={require('./stat.svg')} alt="" loading={'lazy'} />
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    }
  })
)
