export const navItems = [
  {
    text: 'Доставка',
    modalName: 'shipping'
  },
  {
    text: 'Посадка',
    modalName: 'planting-videos'
  },
  {
    text: 'Полив',
    modalName: 'how-to-water'
  },
  {
    text: 'Прайс-лист',
    modalName: 'price-list-request'
  }
]
