import { defineComponent, useContext } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { socialsDictionary } from '~/data/const/socials'
import { useOnWhatsAppClick } from '~/support/hooks/useWhatsApp'
import { BaseIcon } from '~/components/base'
import { PhoneLink } from '~/components/action-providers/phone-link'
import style from './footer-socials.scss?module'

const cn = useClassNames('footer-socials', style)

const socialIcons = [
  'ig',
  'yt',
  'vk',
  'tg',
  'whatsapp',
  'phone',
  'fb',
  'ok'
].map((icon) => ({
  icon,
  link: socialsDictionary[icon]
}))

export const FooterSocials = injectStyles(
  style,
  defineComponent({
    name: 'FooterSocials',
    props: {},
    setup: () => {
      const { $device } = useContext()
      const onWhatsAppClick = useOnWhatsAppClick()
      return () => (
        <div class={cn()}>
          {socialIcons.map((social) =>
            social.icon === 'whatsapp' ? (
              <button class={cn('item')} onClick={onWhatsAppClick}>
                <BaseIcon name={`colored-socials/whatsapp`} width={32} />
              </button>
            ) : social.icon === 'phone' ? (
              $device.isDesktop && (
                <PhoneLink class={cn('item')}>
                  <BaseIcon
                    name={`phone`}
                    width={22}
                    height={22}
                    class={cn('phone-icon')}
                  />
                </PhoneLink>
              )
            ) : (
              <a href={social.link} class={cn('item')} target="_blank">
                <BaseIcon name={`colored-socials/${social.icon}`} width={32} />
              </a>
            )
          )}
        </div>
      )
    }
  })
)
