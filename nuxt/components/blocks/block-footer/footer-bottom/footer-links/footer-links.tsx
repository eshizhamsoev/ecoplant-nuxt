import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseLink } from '~/components/base/base-link'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './footer-links.scss?module'

const cn = useClassNames('footer-links', style)

const classes = {
  main: cn(),
  line: cn('line'),
  link: cn('link'),
  button: cn('button')
}

export const FooterLinks = injectStyles(
  style,
  defineComponent({
    name: 'FooterLinks',
    props: {
      withSaleFir: {
        type: Boolean,
        default: false
      }
    },
    setup: (props) => {
      const modalOpener = useModalOpener()

      const openSaleFir = () =>
        modalOpener({
          name: 'sale-fir'
        })
      return () => (
        <div class={classes.main}>
          {props.withSaleFir && (
            <div class={classes.line}>
              <button onClick={openSaleFir} class={classes.button}>
                Голубая ель бесплатно
              </button>
            </div>
          )}
          <div class={classes.line}>Не является публичной офертой</div>
          <div class={classes.line}>
            <BaseLink to="/privacy-policy" class={classes.link}>
              Политика конфиденциальности
            </BaseLink>
          </div>
        </div>
      )
    }
  })
)
