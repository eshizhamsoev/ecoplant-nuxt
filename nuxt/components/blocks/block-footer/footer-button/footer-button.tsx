import { defineComponent, PropType } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  Indent,
  VerticalAlign
} from '~/components/pattern/pattern-icon-plus'
import { BaseIcon } from '~/components/base/base-icon'
import { cnBaseRoundButton } from '~/components/base/base-round-button'
import { BaseButton } from '~/components/base/base-button'
import { Color } from '~/components/theme/theme-button'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { Point } from '~/components/modals/content/modal-navigate/modal-navigate'
import style from './footer-button.scss?module'

const cn = useClassNames('footer-button', style)

const classes = {
  main: cn(),
  icon: cn('icon')
}

export const FooterButton = injectStyles(
  style,
  defineComponent({
    name: 'FooterButton',
    props: {
      point: {
        type: Object as PropType<Point>,
        required: true
      }
    },
    setup: (props) => {
      const modalOpener = useModalOpener()
      const openModal = () =>
        modalOpener({ name: 'navigate', point: props.point })
      return () => (
        <BaseButton
          size="s"
          color={Color.yellow}
          class={classes.main}
          shadow={true}
          onClick={openModal}
        >
          <span
            class={[
              cnPatternIconPlus({ 'vertical-align': VerticalAlign.center })
            ]}
          >
            <BaseIcon
              name="yandex-navigator"
              width={15}
              class={[
                classes.icon,
                cnBaseRoundButton({ size: 'ss' }),
                cnPatternIconPlusIcon({ indent: Indent.l })
              ]}
            />
            <span>Проложить маршрут</span>
          </span>
        </BaseButton>
      )
    }
  })
)
