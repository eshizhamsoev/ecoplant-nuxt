import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { navItems } from '~/components/blocks/block-footer/footer-bottom/navItems'
import { links } from '~/components/blocks/block-footer/footer-bottom/links'
import { BaseIcon, BaseLink } from '~/components/base'
import {
  EmailLink,
  EmailSlotData
} from '~/components/action-providers/email-link'
import {
  PhoneLink,
  PhoneSlotData
} from '~/components/action-providers/phone-link'
import { FooterLinkList } from '~/components/blocks/block-footer/footer-bottom/footer-link-list/footer-link-list'
import { FooterSocials } from '~/components/blocks/block-footer/footer-bottom/footer-socials'
import { BaseScrollToTop } from '~/components/base/base-scroll-to-top/base-scroll-to-top'
import style from './mobile-block-footer.scss?module'

const cn = useClassNames('mobile-block-footer', style)

const email = ({ email }: EmailSlotData) => [
  <BaseIcon name="mail" width={16} class={cn('email-icon')} />,
  email
]
const phone = ({ phone: { text } }: PhoneSlotData) => [
  <BaseIcon name="phone" width={16} class={cn('phone-icon')} />,
  text
]
const year = new Date().getFullYear()

export const MobileBlockFooter = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockFooter',
    props: {
      withSaleFir: {
        type: Boolean,
        default: false
      },
      withSocials: {
        type: Boolean,
        default: true
      },
      withLinks: {
        type: Boolean,
        default: false
      }
    },
    setup: (props, { root }) => {
      const modalOpener = useModalOpener()
      const openModal = (modalName: any) => modalOpener({ name: modalName })
      const openModalLargeOrders = () => modalOpener({ name: 'large-orders' })

      const legal = root.$accessor.contacts.legal
      const navItemsList = computed(() => {
        return navItems
      })
      const linkList = computed(() => {
        const items = [
          {
            to: '/privacy-policy',
            text: 'Политика конфиденциальности'
          }
        ]
        if (props.withLinks) {
          items.push(...links)
        }
        return items
      })
      return () => (
        <div class={cn()}>
          <ul class={cn('nav-menu', { type: 'mobile' })}>
            {navItemsList.value.map((item) => (
              <li class={cn('nav-menu-item')}>
                <button
                  class={cn('nav-menu-button')}
                  onClick={openModal.bind(null, item.modalName)}
                >
                  {item.text}
                </button>
              </li>
            ))}
            <li class={cn('nav-menu-item')}>
              <button
                onclick={openModalLargeOrders}
                class={cn('nav-menu-button')}
              >
                Оптовикам
              </button>
            </li>
            <li class={cn('nav-menu-item')}>
              <nuxt-link
                to={{
                  path: root.$route.path,
                  query: { tabs: 'BlockVideoReviews' },
                  hash: 'tabs-BlockVideoReviews'
                }}
                class={cn('nav-menu-link')}
              >
                Отзывы
              </nuxt-link>
            </li>
          </ul>
          <PhoneLink scopedSlots={{ default: phone }} class={cn('phone')} />
          <EmailLink scopedSlots={{ default: email }} class={cn('email')} />
          {props.withSocials && <FooterSocials class={cn('socials')} />}
          {props.withLinks ? (
            <FooterLinkList links={linkList.value} class={cn('link-list')} />
          ) : (
            <div class={cn('privacy-policy-line')}>
              <BaseScrollToTop class={cn('to-top')} />
              <BaseLink to="/privacy-policy" class={cn('privacy-link')}>
                Политика конфиденциальности
              </BaseLink>
            </div>
          )}
          <div class={cn('copyright')}>&copy; Ecoplant, 1999-{year}</div>
          <div class={cn('bottom-line')}>
            <div class={cn('info')}>
              <div class={cn('legal')}>{legal}</div>
            </div>
            <div class={cn('statistic', { type: 'mobile' })}>
              <img
                src={require('../footer-bottom/stat.svg')}
                alt=""
                loading={'lazy'}
              />
            </div>
          </div>
        </div>
      )
    }
  })
)
