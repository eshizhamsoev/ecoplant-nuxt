import { computed, defineComponent } from '@nuxtjs/composition-api'
import { ContactItem } from '../footer-contacts/contact-item'
import { useClassNames } from '~/support/utils/bem-classnames'
import { cnThemeWrapper } from '~/components/theme/theme-wrapper'
import { BaseMap } from '~/components/base/base-map/base-map'
import { BaseIcon } from '~/components/base/base-icon'
import { injectStyles } from '~/support/utils/injectStyle'
import { FooterButton } from '~/components/blocks/block-footer/footer-button'
import style from './footer-main-contacts.scss?module'

const cn = useClassNames('footer-main-contacts', style)

const classes = {
  main: cn(),
  wrapper: cnThemeWrapper(),
  heading: cn('heading'),
  items: cn('items'),
  item: cn('item'),
  map: cn('map'),
  button: cn('button')
}

export const FooterMainContacts = injectStyles(
  style,
  defineComponent({
    name: 'FooterMainContacts',
    props: {},
    setup: (_, { root }) => {
      const contacts = computed(() => root.$accessor.contacts)
      const address = computed(() => contacts.value.address)
      const geoPoint = computed(() => contacts.value.geoPoint)
      const email = computed(() => contacts.value.email)
      return () => (
        <div class={classes.main}>
          <div class={classes.wrapper}>
            <div class={classes.heading}>Контактная информация</div>
            <div class={classes.items}>
              <ContactItem class={classes.item}>
                <template slot="icon">
                  <BaseIcon name="point" width={18} height={26} />
                </template>
                <template slot="heading">Адрес</template>
                <template slot="text">{address.value}</template>
              </ContactItem>
              <ContactItem class={classes.item}>
                <template slot="icon">
                  <BaseIcon name="phone" width={18} height={28} />
                </template>
                <template slot="heading">{contacts.value.phone.text}</template>
                <template slot="text">
                  {email.value} – для клиентов <br /> work@eco-plant.ru – для
                  предложений
                </template>
              </ContactItem>
              <ContactItem class={classes.item}>
                <template slot="icon">
                  <BaseIcon name="clock" width={22} height={22} />
                </template>
                <template slot="heading">С 8.00 до 20.00</template>
                <template slot="text">без выходных и праздников</template>
              </ContactItem>
            </div>
          </div>
          <div class={cn('button-wrapper')}>
            <FooterButton class={classes.button} point={geoPoint.value} />
          </div>
          <BaseMap
            class={classes.map}
            address={address.value}
            longitude={geoPoint.value.longitude}
            latitude={geoPoint.value.latitude}
          />
        </div>
      )
    }
  })
)
