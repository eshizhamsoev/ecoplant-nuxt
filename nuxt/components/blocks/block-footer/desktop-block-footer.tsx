import { h } from '@vue/composition-api'
import { defineComponent } from '@nuxtjs/composition-api'
import { AsyncComponent } from 'vue/types/options'
import { FooterBottom } from './footer-bottom'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './block-footer.scss?module'

const cn = useClassNames('block-footer', style)

const classes = {
  main: cn(),
  button: cn('button'),
  buttonWrapper: cn('button-wrapper'),
  bottom: cn('bottom')
}

export const DesktopBlockFooter = injectStyles(
  style,
  defineComponent({
    name: 'BlockFooter',
    props: {
      withSaleFir: {
        type: Boolean,
        default: false
      },
      withLinks: {
        type: Boolean,
        default: false
      },
      showBothContacts: {
        type: Boolean,
        default: true
      }
    },
    setup: (props) => {
      const FooterContacts: AsyncComponent = () =>
        props.showBothContacts
          ? import('./footer-contacts').then((c) => c.FooterContacts)
          : import('./footer-main-contacts').then((c) => c.FooterMainContacts)
      return () => (
        <div class={classes.main}>
          {h(FooterContacts)}
          <FooterBottom
            withSaleFir={props.withSaleFir}
            withLinks={props.withLinks}
            withSocials={!props.withLinks}
            class={classes.bottom}
          />
        </div>
      )
    }
  })
)
