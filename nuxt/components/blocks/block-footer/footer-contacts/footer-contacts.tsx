import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { cnThemeWrapper } from '~/components/theme/theme-wrapper'
import { injectStyles } from '~/support/utils/injectStyle'
import { BlockContactDesktop } from '~/components/blocks/block-contact/block-contact-desktop'
import style from './footer-contacts.scss?module'

const cn = useClassNames('footer-contacts', style)

const classes = {
  main: cn(),
  wrapper: cnThemeWrapper(),
  heading: cn('heading'),
  items: cn('items'),
  item: cn('item'),
  map: cn('map'),
  button: cn('button')
}

export const FooterContacts = injectStyles(
  style,
  defineComponent({
    name: 'FooterContacts',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <div class={classes.wrapper}>
            <div class={classes.heading}>Контактная информация</div>
          </div>
          <div class={cn('groups-grid')}>
            <BlockContactDesktop class={cn('group')} isLazy={true} />
            <BlockContactDesktop
              class={cn('group')}
              isLazy={true}
              isAdditional={true}
            />
          </div>
        </div>
      )
    }
  })
)
