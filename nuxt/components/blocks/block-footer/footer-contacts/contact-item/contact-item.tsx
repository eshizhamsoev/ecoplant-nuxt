import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './contact-item.scss?module'

const cn = useClassNames('contact-item', style)

const classes = {
  main: cn(),
  icon: cn('icon'),
  heading: cn('heading'),
  text: cn('text')
}

export const ContactItem = injectStyles(
  style,
  defineComponent({
    name: 'ContactItem',
    setup: (props, { slots }) => {
      return () => (
        <div class={classes.main}>
          <div class={classes.icon}>{slots.icon && slots.icon()}</div>
          <div>
            <div class={classes.heading}>
              {slots.heading && slots.heading()}
            </div>
            <div class={classes.text}>{slots.text && slots.text()}</div>
          </div>
        </div>
      )
    }
  })
)
