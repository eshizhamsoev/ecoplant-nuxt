import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const BlockFooter = UtilUniversalComponentFactory(
  'BlockFooter',
  () => import('./mobile/mobile-block-footer').then((c) => c.MobileBlockFooter),
  () => import('./desktop-block-footer').then((c) => c.DesktopBlockFooter)
)
