import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseDashedButton, BaseIcon, BaseRoundButton } from '~/components/base'
import {
  PhoneLink,
  PhoneSlotData
} from '~/components/action-providers/phone-link'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { headingIcons } from '~/components/common/heading-icons'
import { BaseLink } from '~/components/base/base-link'
import style from './desktop-block-heading-one-line.scss?module'

const cn = useClassNames('desktop-block-heading-one-line', style)

const classes = {
  main: cn(),
  slogan: cn('slogan'),
  logo: cn('logo'),
  items: cn('items'),
  item: cn('item'),
  itemText: cn('item-text'),
  itemImage: cn('item-image'),
  right: cn('right'),
  phone: cn('phone'),
  phoneIcon: cn('phone-icon'),
  callback: cn('callback')
}

export const DesktopBlockHeadingOneLine = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlockHeadingOneLine',
    props: {},
    setup: (props, { root }) => {
      const siteName = computed(() => root.$accessor.contacts.siteName)

      const modalOpener = useModalOpener()
      const openModal = () => modalOpener({ name: 'callback' })

      const phoneLinkRenderer = ({ phone: { text } }: PhoneSlotData) => [
        <BaseRoundButton size="s" class={classes.phoneIcon}>
          <BaseIcon name="phone" width={13} />
        </BaseRoundButton>,
        <span>{text}</span>
      ]
      return () => (
        <div class={classes.main}>
          <BaseLink to="/">
            <BaseIcon name="logo" width={94} class={classes.logo} />
          </BaseLink>
          <div class={classes.slogan}>{siteName.value}</div>

          <div class={classes.items}>
            {headingIcons.map(({ image, content }) => (
              <div class={classes.item} key={image}>
                <img src={image} class={classes.itemImage} />
                <div class={classes.itemText}>{content()}</div>
              </div>
            ))}
          </div>
          <div class={classes.right}>
            <PhoneLink
              scopedSlots={{ default: phoneLinkRenderer }}
              class={classes.phone}
            />
            <BaseDashedButton
              color="black"
              size="l"
              class={classes.callback}
              onClick={openModal}
            >
              Обратный звонок
            </BaseDashedButton>
          </div>
        </div>
      )
    }
  })
)
