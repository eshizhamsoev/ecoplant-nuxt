import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const BlockHeadingOneLine = UtilUniversalComponentFactory(
  'BlockHeadingOneLine',
  () => import('./mobile').then((c) => c.MobileBlockHeadingOneLine),
  () => import('./desktop').then((c) => c.DesktopBlockHeadingOneLine)
)
