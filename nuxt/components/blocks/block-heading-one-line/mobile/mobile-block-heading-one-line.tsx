import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { EmailLink } from '~/components/action-providers/email-link'
import { BaseDashedButton } from '~/components/base'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { MobileBlockPhoneButton } from '~/components/blocks/block-phone-button/mobile'
import style from './mobile-block-heading-one-line.scss?module'

const cn = useClassNames('mobile-block-heading-one-line', style)

const classes = {
  main: cn(),
  top: cn('top'),
  email: cn('email'),
  callback: cn('callback'),
  offers: cn('offers')
}

export const MobileBlockHeadingOneLine = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockHeadingOneLine',
    props: {},
    setup: () => {
      const modalOpener = useModalOpener()

      const openModal = () =>
        modalOpener({
          name: 'callback',
          source: 'Заказать звонок в шапке'
        })
      const openOfferModal = () => modalOpener({ name: 'partnership' })

      return () => (
        <div class={classes.main}>
          <div class={classes.top}>
            <EmailLink class={classes.email} />
            <BaseDashedButton
              onClick={openModal}
              color="black"
              size="s"
              class={classes.callback}
            >
              Заказать звонок
            </BaseDashedButton>
            <BaseDashedButton
              class={classes.offers}
              size="xs"
              color="black"
              onClick={openOfferModal}
            >
              Предложения
            </BaseDashedButton>
          </div>
          <MobileBlockPhoneButton />
        </div>
      )
    }
  })
)
