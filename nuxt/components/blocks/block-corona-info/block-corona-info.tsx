import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { useCoronaPopups } from '~/components/modals/content/modal-corona-info/modals'
import style from './block-corona-info.scss?module'

const cn = useClassNames('block-corona-info', style)

const classes = {
  main: cn(),
  item: cn('item'),
  icon: cn('icon'),
  itemTitle: cn('item-title')
}

export const BlockCoronaInfo = injectStyles(
  style,
  defineComponent({
    name: 'BlockCoronaInfo',
    props: {},
    setup: () => {
      const modals = useCoronaPopups()
      return () => (
        <div class={classes.main}>
          <div class={classes.item} onClick={modals.work}>
            <BaseIcon
              name="corona-info/check.svg"
              width={45}
              class={classes.icon}
            />
            <span class={classes.itemTitle}>
              Работаем <br />
              без выходных <br />
              <b>в обычном режиме</b>
            </span>
          </div>
          <div class={classes.item} onClick={modals.shipping}>
            <BaseIcon
              name="corona-info/tree.svg"
              width={53}
              class={classes.icon}
            />
            <span class={classes.itemTitle}>
              Бесконтактная <br />
              доставка <br />
              <b>и посадка</b>
            </span>
          </div>
          <div class={classes.item} onClick={modals.online}>
            <BaseIcon
              name="corona-info/whatsapp.svg"
              width={41}
              class={classes.icon}
            />
            <span class={classes.itemTitle}>
              <b>Дистанционный</b>
              <br />
              Онлайн-заказ <br />
              по What's App <br />
              или Facetime
            </span>
          </div>
          <div class={classes.item} onClick={modals.safe}>
            <BaseIcon
              name="corona-info/meeting.svg"
              width={39}
              class={classes.icon}
            />
            <span class={classes.itemTitle}>
              <b>Не назначаем</b>
              <br />
              массовые <br />
              встречи
            </span>
          </div>
        </div>
      )
    }
  })
)
