import { defineComponent } from '@nuxtjs/composition-api'
import arrow from './resources/arrow.svg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './mobile-client-quote.scss?module'

const cn = useClassNames('mobile-client-quote', style)

const classes = {
  main: cn(),
  statement: cn('statement'),
  text: cn('text')
}

export const MobileClientQuote = injectStyles(
  style,
  defineComponent({
    name: 'MobileClientQuote',
    props: {},
    setup: () => {
      return () => (
        <blockquote class={classes.main}>
          <div class={classes.text}>
            ... у меня нет времени ездить и выбирать, они мне сами рассказали
            что мне нужно, привезли и посадили в течение полудня...
          </div>
          <div class={classes.statement}>
            <b>EcoPlant</b> — рекомендую <img src={arrow} />
          </div>
        </blockquote>
      )
    }
  })
)
