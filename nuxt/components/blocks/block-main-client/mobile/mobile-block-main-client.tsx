import { defineComponent } from '@nuxtjs/composition-api'
import { MobileClientQuote } from './mobile-client-quote'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { useIntersectedOnce } from '~/support/hooks/useIntersectedOnce'
import { svgPlaceholder } from '~/support/utils/svg-placeholder'
import { BlockYoutubeVideo } from '~/components/blocks/block-youtube-video'
import style from './mobile-block-main-client.scss?module'

const cn = useClassNames('mobile-block-main-client', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('leading'),
  content: cn('content'),
  image: cn('image'),
  quoteBlock: cn('quote-block'),
  quote: cn('quote'),
  video: cn('video'),
  videoText: cn('video-text'),
  videoTextLine: cn('video-text-line')
}

const video = 'DdoOwPuvkMc'

export const MobileBlockMainClient = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockMainClient',
    props: {},
    setup: (root, { refs }) => {
      const intersected = useIntersectedOnce(() => refs.main as Element)
      return () => (
        <div class={classes.main} ref="main">
          <div class={classes.heading}>Дмитрий Нагиев</div>
          <div class={classes.leading}>о сотрудничестве с нами</div>
          <div class={classes.content}>
            <div class={classes.quoteBlock}>
              <MobileClientQuote class={classes.quote} />
              <BlockYoutubeVideo
                buttonVisual={{ size: 55 }}
                class={classes.video}
                poster={
                  intersected.value
                    ? 'https://ecoplant-pitomnik.ru/image/catalog/blocks/video_reviews/main-review.jpg'
                    : svgPlaceholder(590, 332)
                }
                video={video}
              >
                <div class={classes.videoText}>
                  <span class={classes.videoTextLine}>смотреть</span>
                  <br />
                  <span class={classes.videoTextLine}>видео-отзыв</span>
                </div>
              </BlockYoutubeVideo>
            </div>
          </div>
        </div>
      )
    }
  })
)
