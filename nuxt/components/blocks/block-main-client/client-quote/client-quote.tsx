import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './client-quote.scss?module'

const cn = useClassNames('client-quote', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('text')
}

export const ClientQuote = injectStyles(
  style,
  defineComponent({
    props: {},
    setup: () => {
      return () => (
        <blockquote class={classes.main}>
          <div class={classes.leading}>
            ... у меня нет времени ездить и выбирать, они мне сами рассказали
            что мне нужно, привезли и посадили в течение полудня...
          </div>
          <div class={classes.heading}>
            <b>EcoPlant</b> — рекомендую
          </div>
        </blockquote>
      )
    }
  })
)
