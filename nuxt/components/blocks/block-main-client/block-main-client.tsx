import { defineComponent } from '@nuxtjs/composition-api'
import nagiev from './resources/nagiev.png'
import { useClassNames } from '~/support/utils/bem-classnames'
import { ClientQuote } from '~/components/blocks/block-main-client/client-quote'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './block-main-client.scss?module'

const cn = useClassNames('block-main-client', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('leading'),
  content: cn('content'),
  image: cn('image'),
  quoteBlock: cn('quote-block'),
  quote: cn('quote'),
  video: cn('video')
}
export const BlockMainClient = injectStyles(
  style,
  defineComponent({
    setup: () => {
      return () => (
        <div class={classes.main} ref="main">
          <div class={classes.content}>
            {/* <BlockYoutubeVideo */}
            {/*  with-overlay={true} */}
            {/*  class={classes.video} */}
            {/*  poster="https://ecoplant-pitomnik.ru/image/catalog/blocks/video_reviews/main-review.jpg" */}
            {/*  video="DdoOwPuvkMc" */}
            {/* /> */}
            <div class={classes.quoteBlock}>
              <div class={classes.heading}>Дмитрий Нагиев</div>
              <div class={classes.leading}>о сотрудничестве с нами</div>
              <ClientQuote class={classes.quote} />
            </div>
            <img
              src={nagiev}
              alt="Дмитрий Нагиев"
              class={classes.image}
              loading="lazy"
            />
            {/* <div class={classes.quoteBlock}> */}
            {/*  <ClientQuote class={classes.quote} /> */}
            {/*  <BlockYoutubeVideo */}
            {/*    with-overlay={true} */}
            {/*    class={classes.video} */}
            {/*    poster="https://ecoplant-pitomnik.ru/image/catalog/blocks/video_reviews/main-review.jpg" */}
            {/*    video="DdoOwPuvkMc" */}
            {/*  /> */}
            {/* </div> */}
          </div>
        </div>
      )
    }
  })
)
