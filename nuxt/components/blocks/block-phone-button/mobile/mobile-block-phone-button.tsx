import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobilePhoneButton } from '~/components/blocks/shared-components/mobile-phone-button'
import style from './mobile-block-phone-button.scss?module'

const cn = useClassNames('mobile-block-phone-button', style)

const classes = {
  main: cn()
}

export const MobileBlockPhoneButton = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockPhoneButton',
    props: {},
    setup: () => {
      return () => <MobilePhoneButton class={classes.main} />
    }
  })
)
