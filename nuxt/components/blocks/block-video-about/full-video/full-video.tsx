import {
  computed,
  defineComponent,
  onMounted,
  ref,
  watch
} from '@nuxtjs/composition-api'
// @ts-ignore
import nagievPoster from '../assets/nagiev_poster.jpg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton, BaseIcon } from '~/components/base'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { Color } from '~/components/theme/theme-button'
import style from './full-video.scss?module'

const cn = useClassNames('full-video', style)
const MODAL_CONTAINER_ID = 'modals-container'
const nagievFull =
  'https://ecoplant-pitomnik.ru/files/videos/nagiev_vertical.mp4'
export const FullVideo = injectStyles(
  style,
  defineComponent({
    name: 'FullVideo',
    props: {
      isVideoOpen: {
        type: Boolean,
        default: false
      }
    },
    setup: (props, { refs, emit }) => {
      const blockId = ref('')
      const videoVisible = computed(() => {
        return props.isVideoOpen
      })
      const closeVideo = () => {
        emit('close')
      }
      onMounted(() => {
        blockId.value = (refs.block as HTMLElement).id
      })
      watch(videoVisible, () => {
        if (videoVisible.value) {
          refs.mainVideo instanceof HTMLMediaElement && refs.mainVideo.play()
          document.addEventListener('click', handleGlobalClick, {
            capture: true
          })
        } else {
          refs.mainVideo instanceof HTMLMediaElement && refs.mainVideo.pause()
          document.removeEventListener('click', handleGlobalClick, {
            capture: true
          })
        }
      })
      const modalOpener = useModalOpener()
      const openModal = () => {
        refs.mainVideo instanceof HTMLMediaElement && refs.mainVideo.pause()
        modalOpener({
          name: 'callback',
          source: 'Заявка из видео в кружочке'
        })
      }
      const handleGlobalClick = (e: MouseEvent): void => {
        const isVideoClick = e.composedPath().some((el: EventTarget) => {
          return (
            el instanceof HTMLElement &&
            (el.id === blockId.value || el.id === MODAL_CONTAINER_ID)
          )
        })
        if (!isVideoClick) {
          closeVideo()
        }
      }

      return () => (
        <div
          ref={'block'}
          id={'id-full-video'}
          class={cn({ visible: videoVisible.value })}
        >
          <div class={cn('wrapper')}>
            <video
              ref="mainVideo"
              preload={'none'}
              controls={true}
              poster={nagievPoster}
              src={nagievFull}
              class={cn('content')}
              playsinline
            />
            <button onClick={closeVideo} class={cn('close-button')}>
              <BaseIcon
                class={cn('close-button-icon')}
                name={'times'}
                height={15}
                width={15}
              />
            </button>
            <div class={cn('callback-button-wrapper')}>
              <BaseButton size={'m'} onClick={openModal} color={Color.yellow}>
                Оставить заявку
              </BaseButton>
            </div>
          </div>
        </div>
      )
    }
  })
)
