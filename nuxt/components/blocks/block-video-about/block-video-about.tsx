import { defineComponent, ref } from '@nuxtjs/composition-api'
// @ts-ignore
import nagievSmall from './assets/nagiev_small.mp4'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { FullVideo } from '~/components/blocks/block-video-about/full-video'
import { BaseIcon } from '~/components/base'
import style from './block-video-about.scss?module'

const cn = useClassNames('block-video-about', style)

export const BlockVideoAbout = injectStyles(
  style,
  defineComponent({
    name: 'BlockVideoAbout',
    setup: () => {
      const videoVisible = ref(false)
      const videoHide = ref(false)
      const closeVideo = () => {
        videoVisible.value = false
      }
      const openVideo = () => {
        videoVisible.value = true
      }
      const hideVideo = () => {
        videoHide.value = true
      }

      return () => (
        <div class={cn({ hide: videoHide.value })}>
          <button class={cn('button')} onClick={openVideo}>
            <span class={cn('tint')}>
              <span class={cn('text')}>Дмитрий Нагиев об Экоплант</span>
            </span>
            <video
              class={cn('video-small')}
              loop={true}
              autoplay={true}
              muted={true}
              playsinline
              src={nagievSmall}
            />
          </button>
          <button onClick={hideVideo} class={cn('close-button')}>
            <BaseIcon name={'times'} height={15} width={15} />
          </button>
          <FullVideo isVideoOpen={videoVisible.value} onClose={closeVideo} />
        </div>
      )
    }
  })
)
