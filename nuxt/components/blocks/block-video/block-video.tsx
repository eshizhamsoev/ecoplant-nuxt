import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseVideoBlockWithButton } from '~/components/base'
import style from './block-video.scss?module'

const cn = useClassNames('block-video', style)

export const BlockVideo = injectStyles(
  style,
  defineComponent({
    name: 'BlockVideo',
    props: {
      video: {
        type: String,
        required: true
      },
      poster: {
        type: String,
        required: true
      },
      heading: {
        type: String,
        required: true
      }
    },
    setup: (props, { root }) => {
      return () => (
        <div class={cn()}>
          {props.heading && <div class={cn('heading')}>{props.heading}</div>}
          <BaseVideoBlockWithButton
            class={cn('video')}
            video={props.video}
            poster={props.poster}
            buttonVisual={{
              animate: true,
              size: root.$device.isMobileOrTablet ? 150 : 250
            }}
            // buttonVisual={{ size: 200 }}
          />
        </div>
      )
    }
  })
)
