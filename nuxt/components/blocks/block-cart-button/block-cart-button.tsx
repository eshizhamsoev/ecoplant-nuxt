import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { CommonCartButton } from '~/components/common/common-cart-button/common-cart-button'
import style from './block-cart-button.scss?module'

const cn = useClassNames('block-cart-button', style)

export const BlockCartButton = injectStyles(
  style,
  defineComponent({
    name: 'BlockCartButton',
    props: {},
    setup: () => {
      return () => <CommonCartButton class={cn()} />
    }
  })
)
