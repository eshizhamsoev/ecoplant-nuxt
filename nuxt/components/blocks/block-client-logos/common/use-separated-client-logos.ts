import type { Ref } from '@nuxtjs/composition-api'
import { computed } from '@nuxtjs/composition-api'
import { ClientLogo } from '~/common-types/server/collections/client-logos'

export function useSeparatedClientLogos(clientLogos: Ref<ClientLogo[]>) {
  return {
    stateItems: computed(() =>
      clientLogos.value
        .filter((item) => item.isState)
        .map((item) => ({ image: item.image, title: item.title }))
    ),
    privateItems: computed(() =>
      clientLogos.value
        .filter((item) => !item.isState)
        .map((item) => ({ image: item.image, title: item.title }))
    )
  }
}
