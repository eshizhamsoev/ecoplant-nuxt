import { computed, defineComponent, ref } from '@nuxtjs/composition-api'
import privateImage from './resources/private.png'
import stateImage from './resources/state.png'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton } from '~/components/base/base-button'
import { BaseImageGrid } from '~/components/base/base-image-grid'
import { Visual } from '~/components/theme/theme-button'
import { getClientLogos } from '~/api/client/get-client-logos'
import { ClientLogo } from '~/common-types/server/collections/client-logos'
import { useSeparatedClientLogos } from '~/components/blocks/block-client-logos/common/use-separated-client-logos'
import style from './mobile-block-client-logos.scss?module'

const cn = useClassNames('mobile-client-logos', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  buttons: cn('buttons'),
  button: cn('button'),
  buttonText: cn('button-text'),
  items: cn('items')
}

export const MobileBlockClientLogos = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockClientLogos',
    props: {},
    setup: () => {
      const images = ref([] as ClientLogo[])
      const { stateItems, privateItems } = useSeparatedClientLogos(images)
      getClientLogos().then((data) => (images.value = data.clientLogos))

      const isState = ref(true)

      const showItems = computed(() =>
        isState.value ? stateItems.value : privateItems.value
      )

      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>Наши клиенты — это</div>
          <div class={classes.buttons}>
            <BaseButton
              class={classes.button}
              size="l"
              visual={isState.value ? Visual.default : Visual.outline}
              onClick={() => {
                isState.value = true
              }}
            >
              <img src={stateImage} alt="" />
              <span class={classes.buttonText}>
                Гос.
                <br />
                учреждения
              </span>
            </BaseButton>
            <BaseButton
              class={classes.button}
              size="l"
              visual={!isState.value ? Visual.default : Visual.outline}
              onClick={() => {
                isState.value = false
              }}
            >
              <img src={privateImage} alt="" />
              <span class={classes.buttonText}>
                Крупный
                <br />
                бизнес
              </span>
            </BaseButton>
          </div>
          <BaseImageGrid images={showItems.value} class={classes.items} />
        </div>
      )
    }
  })
)
