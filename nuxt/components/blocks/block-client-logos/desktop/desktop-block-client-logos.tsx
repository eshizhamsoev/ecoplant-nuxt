import { defineComponent, computed } from '@nuxtjs/composition-api'
import icon from '../resources/heading-icon.svg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseImageGrid } from '~/components/base/base-image-grid'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { clients } from '~/graphql/queries'
import style from './desktop-block-client-logos.scss?module'

const cn = useClassNames('desktop-block-client-logos', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  iconWrapper: cn('icon-wrapper'),
  icon: cn('icon'),
  grid: cn('grid'),
  item: cn('item'),
  groups: cn('groups'),
  group: cn('group'),
  groupName: cn('group-name')
}

export const DesktopBlockClientLogos = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlockClientLogos',
    props: {},
    setup: () => {
      const { result } = useGqlQuery(clients, {})
      const images = computed(
        () =>
          result.value?.clients.map((item) => {
            return {
              large: item.image.large,
              thumb: item.image.thumb,
              title: item.title,
              isState: item.isState
            }
          }) || []
      )
      const stateImages = computed(() =>
        images.value.filter((image) => image.isState)
      )
      const businessImages = computed(() =>
        images.value.filter((image) => !image.isState)
      )
      return () => (
        <div class={classes.main}>
          <div class={classes.iconWrapper}>
            <img src={icon} alt="" class={classes.icon} loading={'lazy'} />
          </div>
          <div class={classes.heading}>Клиенты</div>
          <div class={classes.groups}>
            <div class={classes.group}>
              <div class={classes.groupName}>Гос. учреждения</div>
              <BaseImageGrid
                md={3}
                images={stateImages.value}
                class={classes.grid}
                itemClass={classes.item}
              />
            </div>
            <div>
              <div class={classes.groupName}>Крупный бизнес</div>
              <BaseImageGrid
                md={3}
                images={businessImages.value}
                class={classes.grid}
                itemClass={classes.item}
              />
            </div>
          </div>
        </div>
      )
    }
  })
)
