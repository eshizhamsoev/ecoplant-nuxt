import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobileSaleFormLine } from '~/components/blocks/shared-components/sale-form-line/mobile'
import style from './mobile-block-sale.scss?module'

const cn = useClassNames('mobile-block-sale', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('leading'),
  form: cn('form')
}

export const MobileBlockSale = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockSale',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>Получить скидку 70%</div>
          <div class={classes.leading}>Скидка действует до конца недели</div>
          <MobileSaleFormLine
            source={'Независимый блок получить услугу'}
            class={classes.form}
          />
        </div>
      )
    }
  })
)
