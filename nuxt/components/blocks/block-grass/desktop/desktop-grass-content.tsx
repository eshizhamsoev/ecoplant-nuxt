import { computed, defineComponent } from '@nuxtjs/composition-api'
import { grassObjects, grassSlider } from '../data'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { DesktopGrassItems } from '~/components/blocks/block-grass/common/grass-items/desktop/desktop-grass-items'
import { GrassServices } from '~/components/blocks/block-grass/common/grass-services'
import { DesktopBlockSlider } from '~/components/blocks/block-slider/desktop'
import { DesktopGrassPallet } from '~/components/blocks/block-grass/common/grass-pallet/desktop-grass-pallet'
import { DesktopGrassVideo } from '~/components/blocks/block-grass/desktop/components/desktop-grass-video'
import { DesktopGrassObjects } from '~/components/blocks/block-grass/desktop/components/desktop-grass-objects'
import { blockGrassProps } from '~/components/blocks/block-grass/props'
import { GrassStructure } from '~/components/blocks/block-grass/common/grass-structure'
import { BaseButton, BaseIcon } from '~/components/base'
import { Visual } from '~/components/theme/theme-button'
import style from './desktop-grass-content.scss?module'

const cn = useClassNames('desktop-grass-content', style)

const classes = {
  main: cn(),
  items: cn('items'),
  services: cn('services'),
  slider: cn('slider'),
  structure: cn('structure'),
  pallet: cn('pallet'),
  video: cn('video'),
  objects: cn('objects'),
  categories: cn('categories'),
  backContainer: cn('back-container'),
  back: cn('back'),
  backIcon: cn('back-icon')
}

export const DesktopGrassContent = injectStyles(
  style,
  defineComponent({
    name: 'DesktopGrassContent',
    props: blockGrassProps,
    setup: (props, { root }) => {
      const grassProducts = computed(() => root.$accessor.grass.products)
      const grassServices = computed(() => root.$accessor.grass.services)
      return () => (
        <div class={classes.main}>
          {props.withBackButton && (
            <div class={classes.backContainer}>
              <BaseButton
                size="m"
                class={classes.back}
                onClick={() => root.$router.push('/')}
                visual={Visual.outline}
              >
                <BaseIcon
                  name={'arrow-left'}
                  width={24}
                  height={15}
                  class={classes.backIcon}
                />
                <span>Назад</span>
              </BaseButton>
            </div>
          )}

          <DesktopGrassItems
            class={classes.items}
            items={grassProducts.value}
          />
          <GrassServices items={grassServices.value} class={classes.services} />
          <DesktopBlockSlider
            class={classes.slider}
            images={grassSlider}
            heading={'Наш питомник'}
          />
          <GrassStructure class={classes.structure} props={props.structure} />
          <DesktopGrassPallet class={classes.pallet} props={props.pallet} />
          <DesktopGrassVideo class={classes.video} />
          <DesktopGrassObjects items={grassObjects} class={classes.objects} />
        </div>
      )
    }
  })
)
