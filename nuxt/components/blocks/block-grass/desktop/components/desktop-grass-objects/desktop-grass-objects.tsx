import { computed, defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { GrassObject } from '~/components/blocks/block-grass/types'
import { BaseIcon } from '~/components/base/base-icon'
import { BaseSliderWithThumbs } from '~/components/base/base-slider-with-thumbs/base-slider-with-thumbs'
import style from './desktop-grass-objects.scss?module'

const cn = useClassNames('desktop-grass-objects', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  inner: cn('inner'),
  items: cn('items'),
  button: cn('button'),
  buttonIcon: cn('button-icon'),
  buttonNumber: cn('button-number'),
  buttonName: cn('button-name'),
  sliderWrapper: cn('slider-wrapper'),
  slider: cn('slider'),
  sliderItem: cn('slider-item'),
  image: cn('image')
}

export const DesktopGrassObjects = injectStyles(
  style,
  defineComponent({
    name: 'DesktopGrassObjects',
    props: {
      items: {
        type: Array as () => GrassObject[],
        required: true
      }
    },
    setup: (props) => {
      const currentIndex = ref(0)
      const currentItem = computed(() => props.items[currentIndex.value])
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>
            Наши выполненные
            <br /> объекты
          </div>
          <div class={classes.inner}>
            <div class={classes.items}>
              {props.items.map((item, index) => (
                <button
                  onClick={() => {
                    currentIndex.value = index
                  }}
                  class={cn('button', {
                    active: currentIndex.value === index
                  })}
                >
                  <span class={classes.buttonNumber}>
                    {(index + 1).toString().padStart(2, '0')}
                  </span>
                  <span class={classes.buttonName}>{item.name}</span>
                  <BaseIcon
                    class={classes.buttonIcon}
                    name="angle-down"
                    rotate={270}
                    width={16}
                    height={16}
                  />
                </button>
              ))}
            </div>
            <div class={classes.sliderWrapper}>
              <BaseSliderWithThumbs
                key={currentIndex.value}
                images={currentItem.value.images.map((image) => ({
                  small: image,
                  large: image
                }))}
                sliderSize={{
                  width: 508,
                  height: 521
                }}
                thumbSize={{
                  width: 121,
                  height: 166
                }}
              />
            </div>
          </div>
        </div>
      )
    }
  })
)
