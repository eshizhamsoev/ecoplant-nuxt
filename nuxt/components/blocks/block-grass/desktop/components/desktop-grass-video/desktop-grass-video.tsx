import { defineComponent, ref } from '@nuxtjs/composition-api'
import lawnImage from './resources/lawn.jpg'
// @ts-ignore
import webpLawnImage from './resources/lawn.jpg?webp'
import manImage from './resources/man.png'
// @ts-ignore
import webpManImage from './resources/man.png?webp'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseVideoBlockWithButton } from '~/components/base'
import { BaseWebpPicture } from '~/components/base/base-webp-picture'
import style from './desktop-grass-video.scss?module'

const cn = useClassNames('desktop-grass-video', style)

const classes = {
  main: cn(),
  videoWrapper: cn('video-wrapper'),
  videoOverlay: cn('video-overlay'),
  video: cn('video'),
  man: cn('man'),
  definition: cn('definition'),
  text: cn('text'),
  lawn: cn('lawn'),
  lawnImage: cn('lawn-image')
}

export const DesktopGrassVideo = injectStyles(
  style,
  defineComponent({
    name: 'DesktopGrassVideo',
    props: {},
    setup: () => {
      const isPlaying = ref(false)
      return () => (
        <div class={classes.main}>
          <div class={classes.videoWrapper}>
            <BaseVideoBlockWithButton
              buttonVisual={{ size: 120 }}
              buttonText={'Видео о том,\n' + 'как мы готовим\n' + 'наш газон'}
              withOverlay={true}
              overlayClass={classes.videoOverlay}
              overlayColor="rgba(255, 255, 255, 0.45)"
              class={classes.video}
              video="https://ecoplant-pitomnik.ru/files/videos/grass.mp4"
              poster="https://ecoplant-pitomnik.ru/image/catalog/blocks/grass/video_poster.jpg"
              // @ts-ignore
              onPlay={() => (isPlaying.value = true)}
              onPause={() => (isPlaying.value = false)}
            />
            <BaseWebpPicture
              vShow={!isPlaying.value}
              webpImage={webpManImage}
              image={manImage}
              class={classes.man}
            />
          </div>
          <div class={classes.lawn}>
            <div class={classes.definition}>
              <b>Рулонный газон</b> — это созревший натуральный газон, который
              срезают и заворачивают в рулоны для удобной транспортировки.
            </div>
            <div class={classes.text}>
              Для того, чтобы получить качественный газон,{' '}
              <b>наши дендрологи подберут только виды трав</b>, которые лучше
              всего подходят под российские условия
            </div>
            <BaseWebpPicture
              image={lawnImage}
              webpImage={webpLawnImage}
              class={classes.lawnImage}
            />
          </div>
        </div>
      )
    }
  })
)
