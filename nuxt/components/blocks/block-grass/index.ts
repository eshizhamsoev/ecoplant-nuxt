import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const BlockGrass = UtilUniversalComponentFactory(
  'BlockGrass',
  () => import('./mobile').then((c) => c.MobileGrassContent),
  () => import('./desktop').then((c) => c.DesktopGrassContent)
)
