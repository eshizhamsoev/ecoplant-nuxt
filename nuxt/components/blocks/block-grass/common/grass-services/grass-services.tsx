import { defineComponent } from '@nuxtjs/composition-api'
import { GrassService as GrassServiceComponent } from './components/grass-sevice'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { GrassService } from '~/store/grass'
import style from './grass-services.scss?module'

const cn = useClassNames('grass-services', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('leading'),
  items: cn('items'),
  item: cn('item')
}

export const GrassServices = injectStyles(
  style,
  defineComponent({
    name: 'GrassServices',
    props: {
      items: {
        type: Array as () => readonly GrassService[],
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>Работы</div>
          <div class={classes.leading}>(Укладка рулонного газона)</div>
          <div class={classes.items}>
            {props.items.map((item) => (
              <GrassServiceComponent
                class={classes.item}
                heading={item.title}
                leading={item.info}
                price={item.price}
                items={item.items}
              />
            ))}
          </div>
        </div>
      )
    }
  })
)
