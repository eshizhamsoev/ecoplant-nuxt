import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseSalePrice } from '~/components/base/base-sale-price'
import { BaseButton } from '~/components/base'
import { Visual } from '~/components/theme/theme-button'
import { GrassProductPrice } from '~/store/grass'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './desktop-grass-item.scss?module'

const cn = useClassNames('desktop-grass-item', style)

const classes = {
  main: cn(),
  image: cn('image'),
  imageWrapper: cn('image-wrapper'),
  heading: cn('heading'),
  description: cn('description'),
  button: cn('button'),
  price: cn('price'),
  priceLabel: cn('price-label'),
  priceValue: cn('price-value'),
  priceOldValue: cn('price-old-value'),
  buttons: cn('buttons')
}

export const DesktopGrassItem = injectStyles(
  style,
  defineComponent({
    name: 'DesktopGrassItem',
    props: {
      heading: {
        type: String,
        required: true
      },
      image: {
        type: String,
        required: true
      },
      content: {
        type: String,
        required: true
      },
      price: {
        type: Object as () => GrassProductPrice,
        required: true
      }
    },
    setup: (props, { emit }) => {
      const modalOpener = useModalOpener()
      const openModal = () =>
        modalOpener({
          name: 'callback',
          source: `Позвонить нам. Выбранный товар: ${props.heading}`
        })

      return () => (
        <div class={classes.main}>
          <div class={classes.imageWrapper}>
            <img
              src={props.image}
              alt={props.heading}
              class={classes.image}
              loading={'lazy'}
            />
            <BaseSalePrice
              class={classes.price}
              priceData={props.price}
              classes={{
                label: classes.priceLabel,
                price: classes.priceValue,
                oldPrice: classes.priceOldValue
              }}
              size="m"
              align="right"
            />
          </div>
          <div class={classes.heading}>{props.heading}</div>
          <div class={classes.description}>{props.content}</div>
          <div class={classes.buttons}>
            <BaseButton class={classes.button} size="m" onClick={openModal}>
              Заказать
            </BaseButton>
            <BaseButton
              class={classes.button}
              visual={Visual.outline}
              size="m"
              onClick={() => emit('click')}
            >
              Подробнее
            </BaseButton>
          </div>
        </div>
      )
    }
  })
)
