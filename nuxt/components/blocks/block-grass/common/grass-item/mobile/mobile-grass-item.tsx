import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { GrassProduct } from '~/store/grass'
import { BaseSalePrice } from '~/components/base/base-sale-price'
import { BaseButton } from '~/components/base'
import { Visual } from '~/components/theme/theme-button'
import style from './mobile-grass-item.scss?module'

const cn = useClassNames('mobile-grass-item', style)

const classes = {
  main: cn(),
  image: cn('image'),
  imageWrapper: cn('image-wrapper'),
  heading: cn('heading'),
  button: cn('button'),
  price: cn('price'),
  priceLabel: cn('price-label'),
  priceValue: cn('price-value'),
  priceOldValue: cn('price-old-value')
}

export const MobileGrassItem = injectStyles(
  style,
  defineComponent({
    name: 'MobileGrassItem',
    props: {
      product: {
        type: Object as () => GrassProduct,
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          <BaseSalePrice
            class={classes.price}
            priceData={props.product.price}
            classes={{
              label: classes.priceLabel,
              price: classes.priceValue,
              oldPrice: classes.priceOldValue
            }}
            size="m"
            align="right"
          />
          <div class={classes.imageWrapper}>
            <img
              src={props.product.image}
              alt={props.product.heading}
              class={classes.image}
            />
          </div>
          <div class={classes.heading}>
            {props.product.heading.split(' ').map((word) => (
              <span>{word}</span>
            ))}
          </div>
          <BaseButton
            class={classes.button}
            visual={Visual.outline}
            size="custom"
            to={`/grass/${props.product.id}`}
          >
            Подробнее
          </BaseButton>
        </div>
      )
    }
  })
)
