import { defineComponent } from '@nuxtjs/composition-api'
// @ts-ignore
import palletImage from './resources/pallet.png?resize&size=231'
// @ts-ignore
import palletWebpImage from './resources/pallet.png?webp&resize&size=231'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseWebpPicture } from '~/components/base/base-webp-picture'
import { grassPalletProps } from '~/components/blocks/block-grass/common/grass-pallet/props'
import style from './mobile-grass-pallet.scss?module'

const cn = useClassNames('mobile-grass-pallet', style)

const classes = {
  main: cn(),
  headingLine: cn('heading-line'),
  heading: cn('heading'),
  table: cn('table'),
  headingImage: cn('heading-image')
}

export const MobileGrassPallet = injectStyles(
  style,
  defineComponent({
    name: 'MobileGrassPallet',
    props: grassPalletProps,
    setup: (props) => {
      return () => (
        <div>
          <div class={classes.headingLine}>
            <div class={classes.heading}>Информация о поддоне</div>
            <BaseWebpPicture
              class={classes.headingImage}
              image={palletImage.toString()}
              webpImage={palletWebpImage.toString()}
              alt=""
            />
          </div>
          <div>
            <table class={classes.table}>
              <tbody>
                <tr>
                  <td>1 поддон</td>
                  <td>{props.palletVolume}</td>
                </tr>
                <tr>
                  <td>Размер поддона</td>
                  <td>{props.palletSize}</td>
                </tr>
                <tr>
                  <td>Вес 1 рулона</td>
                  <td>{props.grassWeight}</td>
                </tr>
                <tr>
                  <td>Вес 1 поддона</td>
                  <td>{props.palletWeight}</td>
                </tr>
                <tr>
                  <td>{props.deliverLabel}</td>
                  <td>
                    {props.deliverySize}
                    <br />
                    <small>({props.deliveryAdditionalInfo})</small>
                  </td>
                </tr>
                {props.currierSize && (
                  <tr>
                    <td>Мы отправляем курьерские товары</td>
                    <td>{props.currierSize}</td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      )
    }
  })
)
