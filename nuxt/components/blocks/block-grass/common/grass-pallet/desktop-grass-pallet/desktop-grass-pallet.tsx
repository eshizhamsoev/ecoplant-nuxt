import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { grassPalletProps } from '~/components/blocks/block-grass/common/grass-pallet/props'
import style from './desktop-grass-pallet.scss?module'

const cn = useClassNames('desktop-grass-pallet', style)

const classes = {
  main: cn(),
  inner: cn('inner'),
  tablet: cn('tablet'),
  headingLine: cn('heading-line'),
  logo: cn('logo'),
  heading: cn('heading'),
  table: cn('table'),
  headingImage: cn('heading-image'),
  image: cn('image')
}

export const DesktopGrassPallet = injectStyles(
  style,
  defineComponent({
    name: 'DesktopGrassPallet',
    props: grassPalletProps,
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          <div class={classes.inner}>
            <div class={classes.tablet}>
              <div class={classes.headingLine}>
                <BaseIcon name="logo" width={76} class={classes.logo} />
                <div class={classes.heading}>
                  Информация
                  <br />о поддоне
                </div>
              </div>
              <div>
                <table class={classes.table}>
                  <tbody>
                    <tr>
                      <td>1 поддон</td>
                      <td>{props.palletVolume}</td>
                    </tr>
                    <tr>
                      <td>Размер поддона</td>
                      <td>{props.palletSize}</td>
                    </tr>
                    <tr>
                      <td>Вес 1 рулона</td>
                      <td>{props.grassWeight}</td>
                    </tr>
                    <tr>
                      <td>Вес 1 поддона</td>
                      <td>{props.palletWeight}</td>
                    </tr>
                    <tr>
                      <td>{props.deliverLabel}</td>
                      <td>
                        {props.deliverySize}
                        <br />
                        <b>({props.deliveryAdditionalInfo})</b>
                      </td>
                    </tr>
                    {props.currierSize && (
                      <tr>
                        <td>Мы отправляем курьерские товары</td>
                        <td>{props.currierSize}</td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      )
    }
  })
)
