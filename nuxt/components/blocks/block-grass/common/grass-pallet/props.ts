export const grassPalletProps = {
  palletVolume: {
    type: String,
    default: '40 м2 в рулоне'
  },
  palletSize: {
    type: String,
    default: '80 х 120 см х 140см'
  },
  grassWeight: {
    type: String,
    default: '22-25 кг'
  },
  palletWeight: {
    type: String,
    default: '1 тонна'
  },
  deliverLabel: {
    type: String,
    default: 'В одну грузовую машину можно загрузить'
  },
  deliverySize: {
    type: String,
    default: 'около 24-25 поддонов'
  },
  deliveryAdditionalInfo: {
    type: String,
    default: '960-1000 m2 в зависимости от погоды'
  },
  currierSize: {
    validator: (v: any) => typeof v === 'string' || v === null,
    default: 'от 1м2 до 160 м2' as string | null
  }
}

export type GrassPalletProps = {
  palletVolume: string
  palletSize: string
  grassWeight: string
  palletWeight: string
  deliverLabel: string
  deliverySize: string
  deliveryAdditionalInfo: string
  currierSize: string | null
}
