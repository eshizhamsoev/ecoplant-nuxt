import { defineComponent } from '@nuxtjs/composition-api'
import structureImage from './resources/structure.png'
// @ts-ignore
import structureImageWebp from './resources/structure.png?webp'
import sizeImage from './resources/size.png'
// @ts-ignore
import sizeImageWebp from './resources/size.png?webp'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePicture } from '~/components/base/base-picture'
import { BaseBadgedText } from '~/components/base/base-badged-text/base-badged-text'
import { grassStructureProps } from '~/components/blocks/block-grass/common/grass-structure/props'
import style from './desktop-grass-structure.scss?module'

const cn = useClassNames('mobile-grass-structure', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  structure: cn('structure'),
  structureImage: cn('structure-image'),
  size: cn('size'),
  sizeHeading: cn('size-heading'),
  sizeImage: cn('size-image'),
  sizeImageWrapper: cn('size-image-wrapper'),
  sizeWidth: cn('size-width'),
  sizeLength: cn('size-length'),
  comment: cn('comment')
}

const structureImages = [
  {
    src: `${structureImageWebp}`,
    type: 'image/webp'
  },
  {
    src: `${structureImage}`
  }
]

const sizeImages = [
  {
    src: `${sizeImageWebp}`,
    type: 'image/webp'
  },
  {
    src: `${sizeImage}`
  }
]

export const DesktopGrassStructure = injectStyles(
  style,
  defineComponent({
    name: 'DesktopGrassStructure',
    props: grassStructureProps,
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>Структура и размеры</div>
          <div class={classes.structure}>
            <BasePicture
              mainImage={structureImage}
              images={structureImages}
              alt="Структура газона"
              class={classes.structureImage}
            />
            <BaseBadgedText
              class={classes.comment}
              horizontalOffset="12px"
              lines={[
                'Биоразлагаемая сетка не видна,',
                'она встроена в корень и представляет',
                'собой дополнительное подкрепление'
              ]}
            />
          </div>
          <div class={classes.size}>
            <div class={classes.sizeHeading}>
              Выпускается в рулонах <br /> стандартных размеров шириной <br />
              <b>{props.sizeText}</b>
            </div>
            <div class={classes.sizeImageWrapper}>
              <BasePicture
                mainImage={sizeImage}
                images={sizeImages}
                alt="Размеры газона"
                class={classes.sizeImage}
              />
              <div class={classes.sizeWidth}>{props.widthText}</div>
              <div class={classes.sizeLength}>{props.lengthText}</div>
            </div>
          </div>
        </div>
      )
    }
  })
)
