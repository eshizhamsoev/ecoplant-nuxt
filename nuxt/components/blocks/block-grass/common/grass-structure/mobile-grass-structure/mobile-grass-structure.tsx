import { defineComponent } from '@nuxtjs/composition-api'
import structureImage from './resources/structure.png'
import structureImageX2 from './resources/structure-x2.png'
// @ts-ignore
import structureImageWebp from './resources/structure.png?webp'
// @ts-ignore
import structureImageWebpX2 from './resources/structure-x2.png?webp'
import sizeImage from './resources/size.png'
import sizeImageX2 from './resources/size-x2.png'
// @ts-ignore
import sizeImageWebp from './resources/size.png?webp'
// @ts-ignore
import sizeImageWebpX2 from './resources/size-x2.png?webp'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePicture } from '~/components/base/base-picture'
import { grassStructureProps } from '~/components/blocks/block-grass/common/grass-structure/props'
import style from './mobile-grass-structure.scss?module'

const cn = useClassNames('mobile-grass-structure', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  structureImage: cn('structure-image'),
  sizeHeading: cn('size-heading'),
  sizeImage: cn('size-image'),
  comment: cn('comment'),
  sizeImageWrapper: cn('size-image-wrapper'),
  sizeWidth: cn('size-width'),
  sizeLength: cn('size-length')
}

const structureImages = [
  {
    src: `${structureImageWebp}, ${structureImageWebpX2} 2x`,
    type: 'image/webp'
  },
  {
    src: `${structureImage}, ${structureImageX2} 2x`
  }
]

const sizeImages = [
  {
    src: `${sizeImageWebp}, ${sizeImageWebpX2} 2x`,
    type: 'image/webp'
  },
  {
    src: `${sizeImage}, ${sizeImageX2} 2x`
  }
]

export const MobileGrassStructure = injectStyles(
  style,
  defineComponent({
    name: 'MobileGrassStructure',
    props: grassStructureProps,
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>Структура и размеры</div>
          <BasePicture
            mainImage={structureImage}
            images={structureImages}
            alt="Структура газона"
            class={classes.structureImage}
          />
          <div class={classes.sizeHeading}>
            Выпускается в рулонах <br /> стандартных размеров шириной <br />
            <b>{props.sizeText}</b>
          </div>
          <div class={classes.sizeImageWrapper}>
            <BasePicture
              mainImage={sizeImage}
              images={sizeImages}
              alt="Размеры газона"
              class={classes.sizeImage}
            />
            <div class={classes.sizeWidth}>{props.widthText}</div>
            <div class={classes.sizeLength}>{props.lengthText}</div>
          </div>
          <div class={classes.comment}>
            <span>*</span> Биоразлагаемая сетка не видна, она встроена в корень
            и представляет собой дополнительное подкрепление
          </div>
        </div>
      )
    }
  })
)
