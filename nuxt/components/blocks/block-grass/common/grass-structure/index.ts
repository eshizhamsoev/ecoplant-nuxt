import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const GrassStructure = UtilUniversalComponentFactory(
  'GrassStructure',
  () => import('./mobile-grass-structure').then((c) => c.MobileGrassStructure),
  () => import('./desktop-grass-structure').then((c) => c.DesktopGrassStructure)
)
