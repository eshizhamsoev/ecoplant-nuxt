export const grassStructureProps = {
  sizeText: {
    type: String,
    default: '0,4 x 2,5 м и 0,6 х 1,67 м'
  },
  widthText: {
    type: String,
    default: '0,40м (0,60м)'
  },
  lengthText: {
    type: String,
    default: '2.50м  (1,67м)'
  }
}

export type GrassStructureProps = {
  sizeText: string
  widthText: string
  lengthText: string
}
