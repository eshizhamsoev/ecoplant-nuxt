import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { GrassProduct } from '~/store/grass'
import {
  GrassItemsFactory,
  grassItemsProps
} from '~/components/blocks/block-grass/common/grass-items/factory'
import { DesktopGrassItem } from '~/components/blocks/block-grass/common/grass-item/desktop/desktop-grass-item'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './desktop-grass-items.scss?module'

const cn = useClassNames('desktop-grass-items', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  lines: cn('lines'),
  line: cn('line'),
  item: cn('item'),
  lineSeparator: cn('line-separator'),
  itemSeparator: cn('item-separator')
}

export const DesktopGrassItems = injectStyles(
  style,
  defineComponent({
    name: 'DesktopGrassItems',
    props: grassItemsProps,
    setup: (props) => {
      const modalOpener = useModalOpener()
      const itemFactory = (item: GrassProduct) => (
        <DesktopGrassItem
          class={classes.item}
          price={item.price}
          image={item.image}
          content={item.content}
          heading={item.heading}
          onClick={() => modalOpener({ name: 'grass-product', product: item })}
        />
      )
      return () => (
        <GrassItemsFactory
          classes={classes}
          scopedSlots={{ item: itemFactory }}
          items={props.items}
        />
      )
    }
  })
)
