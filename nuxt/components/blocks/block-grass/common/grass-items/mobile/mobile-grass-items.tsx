import { defineComponent } from '@nuxtjs/composition-api'
import { GrassItemsFactory, grassItemsProps } from '../factory'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobileGrassItem } from '~/components/blocks/block-grass/common/grass-item/mobile'
import { GrassProduct } from '~/store/grass'
import style from './mobile-grass-items.scss?module'

const cn = useClassNames('mobile-grass-items', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  lines: cn('lines'),
  line: cn('line'),
  item: cn('item'),
  lineSeparator: cn('line-separator'),
  itemSeparator: cn('item-separator')
}

export const MobileGrassItems = injectStyles(
  style,
  defineComponent({
    name: 'MobileGrassItems',
    props: grassItemsProps,
    setup: (props) => {
      const itemFactory = (item: GrassProduct) => (
        <MobileGrassItem product={item} class={classes.item} />
      )
      return () => (
        <GrassItemsFactory
          classes={classes}
          scopedSlots={{ item: itemFactory }}
          items={props.items}
        />
      )
    }
  })
)
