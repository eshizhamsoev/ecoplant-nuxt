import { computed, defineComponent } from '@nuxtjs/composition-api'
import chunk from 'lodash/chunk'
import { grassItemsProps } from '~/components/blocks/block-grass/common/grass-items/factory/props'

type GrassItemsClasses = {
  main: string
  heading: string
  lines: string
  line: string
  item: string
  lineSeparator: string
  itemSeparator: string
}

export const GrassItemsFactory = defineComponent({
  name: 'GrassItems',
  props: {
    ...grassItemsProps,
    classes: {
      type: Object as () => GrassItemsClasses,
      required: true
    }
  },
  setup: (props, { slots }) => {
    const classes = props.classes
    const lines = computed(() => chunk(props.items, 2))
    return () => (
      <div class={classes.main}>
        <div class={classes.heading}>
          Газоны, которые вы можете у нас приобрести
        </div>
        <div class={classes.lines}>
          {lines.value.map((line) => [
            <div class={classes.line}>
              {line.map((item, i) => [
                i > 0 && <div class={classes.itemSeparator} />,
                slots.item && slots.item(item)
              ])}
            </div>,
            <div class={classes.lineSeparator} />
          ])}
        </div>
      </div>
    )
  }
})
