import { GrassProduct } from '~/store/grass'

export const grassItemsProps = {
  items: {
    type: Array as () => readonly GrassProduct[],
    required: true as true
  }
}
