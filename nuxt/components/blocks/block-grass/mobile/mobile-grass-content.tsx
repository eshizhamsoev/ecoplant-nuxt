import { computed, defineComponent } from '@nuxtjs/composition-api'
import { MobileGrassServices } from './components/mobile-grass-services'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobileGrassPallet } from '~/components/blocks/block-grass/common/grass-pallet/mobile-grass-pallet'
import { grassObjects, grassSlider } from '~/components/blocks/block-grass/data'
import { MobileGrassItems } from '~/components/blocks/block-grass/common/grass-items/mobile'
import { MobileGrassVideo } from '~/components/blocks/block-grass/mobile/components/mobile-grass-video'
import { MobileGrassObjects } from '~/components/blocks/block-grass/mobile/components/mobile-grass-objects'
import { MobileBlockSlider } from '~/components/blocks/block-slider/mobile'
import { MobileBlockPhoneButton } from '~/components/blocks/block-phone-button/mobile'
import { blockGrassProps } from '~/components/blocks/block-grass/props'
import { GrassStructure } from '~/components/blocks/block-grass/common/grass-structure'
import style from './mobile-grass-content.scss?module'

const cn = useClassNames('mobile-grass-content', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  headingTop: cn('heading-top'),
  headingBottom: cn('heading-bottom'),
  products: cn('products'),
  services: cn('services'),
  slider: cn('slider'),
  structure: cn('structure'),
  pallet: cn('pallet'),
  video: cn('video'),
  objects: cn('objects'),
  categories: cn('categories'),
  footer: cn('footer')
}

export const MobileGrassContent = injectStyles(
  style,
  defineComponent({
    name: 'MobileGrassContent',
    props: blockGrassProps,
    setup: (props, { root }) => {
      const grassProducts = computed(() => root.$accessor.grass.products)
      const grassServices = computed(() => root.$accessor.grass.services)
      return () => (
        <div class={classes.main}>
          <MobileGrassItems
            items={grassProducts.value}
            class={classes.products}
          />
          <MobileGrassServices
            items={grassServices.value}
            class={classes.services}
          />
          <MobileBlockPhoneButton />
          <MobileBlockSlider
            class={classes.slider}
            images={grassSlider}
            heading={'Наш питомник'}
          />
          <GrassStructure class={classes.structure} props={props.structure} />
          <MobileGrassPallet class={classes.pallet} props={props.pallet} />
          <MobileGrassVideo class={classes.video} />
          <MobileGrassObjects items={grassObjects} class={classes.objects} />
        </div>
      )
    }
  })
)
