import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { GrassService } from '~/store/grass'
import { MobileGrassService } from '~/components/blocks/block-grass/mobile/components/mobile-grass-services/components/mobile-grass-sevice/mobile-grass-service'
import style from './mobile-grass-services.scss?module'

const cn = useClassNames('mobile-grass-services', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('leading'),
  items: cn('items'),
  item: cn('item')
}

export const MobileGrassServices = injectStyles(
  style,
  defineComponent({
    name: 'MobileGrassServices',
    props: {
      items: {
        type: Array as () => readonly GrassService[],
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>Работы</div>
          <div class={classes.leading}>(Укладка рулонного газона)</div>
          <div class={classes.items}>
            {props.items.map((item) => (
              <MobileGrassService
                class={classes.item}
                heading={item.title}
                leading={item.info}
                price={item.price}
                items={item.items}
              />
            ))}
          </div>
        </div>
      )
    }
  })
)
