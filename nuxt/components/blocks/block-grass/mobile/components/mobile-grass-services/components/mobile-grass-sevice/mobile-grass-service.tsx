import { computed, defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton, BaseIcon } from '~/components/base'
import { Color } from '~/components/theme/theme-button'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './mobile-grass-service.scss?module'

const cn = useClassNames('mobile-grass-service', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  headingInner: cn('heading-inner'),
  price: cn('price'),
  priceValue: cn('price-value'),
  leading: cn('leading'),
  itemsHeading: cn('items-heading'),
  itemsWrapper: cn('items-wrapper'),
  items: cn('items'),
  item: cn('item'),
  itemIcon: cn('item-icon'),
  showMoreButton: cn('show-more-button'),
  showMoreWrapper: cn('show-more-wrapper'),
  orderButton: cn('order-button'),
  orderButtonWrapper: cn('order-button-wrapper')
}

export const MobileGrassService = injectStyles(
  style,
  defineComponent({
    name: 'MobileGrassService',
    props: {
      heading: {
        type: String,
        required: true
      },
      leading: {
        type: String,
        required: true
      },
      price: {
        type: Number,
        required: true
      },
      items: {
        type: Array as () => readonly string[],
        required: true
      }
    },
    setup: (props) => {
      const isLimited = ref(props.items.length > 5)
      const showAll = () => {
        isLimited.value = false
      }
      const visibleItems = computed(() =>
        isLimited.value ? props.items.slice(0, 5) : props.items
      )
      const modalOpener = useModalOpener()

      const openCallbackPopup = (source: string) =>
        modalOpener({ name: 'callback', source })

      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>
            <div class={classes.headingInner}>{props.heading}</div>
          </div>
          <div class={classes.price}>
            от <span class={classes.priceValue}>{props.price}</span> руб./м2
          </div>
          <div
            class={cn('leading', {
              size: props.leading.includes('\n') ? 's' : 'm'
            })}
          >
            {props.leading}
          </div>
          <div class={classes.itemsWrapper}>
            <div class={classes.itemsHeading}>В пакет входит:</div>
            <ul class={classes.items}>
              {visibleItems.value.map((item) => (
                <li class={classes.item}>
                  <BaseIcon
                    name="check"
                    width={13}
                    height={10}
                    class={classes.itemIcon}
                  />
                  <span>{item}</span>
                </li>
              ))}
            </ul>
            {isLimited.value && (
              <div class={classes.showMoreWrapper}>
                <button class={classes.showMoreButton} onClick={showAll}>
                  открыть весь список
                </button>
              </div>
            )}
          </div>
          <div class={classes.orderButtonWrapper}>
            <BaseButton
              class={classes.orderButton}
              color={Color.red}
              size="l"
              shadow={true}
              onClick={() => openCallbackPopup(props.heading)}
            >
              Заказать
            </BaseButton>
          </div>
        </div>
      )
    }
  })
)
