import { defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { GrassObject } from '~/components/blocks/block-grass/types'
import { BaseIcon } from '~/components/base/base-icon'
import { BaseSliderWithButtons } from '~/components/base/base-slider-with-buttons'
import style from './mobile-grass-objects.scss?module'

const cn = useClassNames('mobile-grass-objects', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  items: cn('items'),
  item: cn('item'),
  buttonIcon: cn('button-icon'),
  buttonNumber: cn('button-number'),
  buttonName: cn('button-name'),
  buttonWrapper: cn('button-wrapper'),
  slider: cn('slider'),
  sliderItem: cn('slider-item'),
  image: cn('image')
}

export const MobileGrassObjects = injectStyles(
  style,
  defineComponent({
    name: 'MobileGrassObjects',
    props: {
      items: {
        type: Array as () => GrassObject[],
        required: true
      }
    },
    setup: (props) => {
      const currentItem = ref(0)
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>Наши выполненные объекты</div>
          <div class={classes.items}>
            {props.items.map((item, index) => (
              <div class={classes.item}>
                <div class={classes.buttonWrapper}>
                  <button
                    onClick={() => {
                      currentItem.value = index
                    }}
                    class={cn('button', {
                      active: currentItem.value === index
                    })}
                  >
                    <span class={classes.buttonNumber}>
                      {(index + 1).toString().padStart(2, '0')}
                    </span>
                    <span class={classes.buttonName}>{item.name}</span>
                    <BaseIcon
                      class={classes.buttonIcon}
                      name="angle-down"
                      width={18}
                      height={10}
                    />
                  </button>
                </div>
                {currentItem.value === index && (
                  <BaseSliderWithButtons
                    options={{
                      gutter: 10,
                      lazyload: true,
                      center: true,
                      fixedWidth: 240
                    }}
                    class={classes.slider}
                  >
                    {item.images.map((image) => (
                      <div class={classes.sliderItem}>
                        <img
                          data-src={image}
                          alt=""
                          class={[classes.image, 'tns-lazy-img']}
                        />
                      </div>
                    ))}
                  </BaseSliderWithButtons>
                )}
              </div>
            ))}
          </div>
        </div>
      )
    }
  })
)
