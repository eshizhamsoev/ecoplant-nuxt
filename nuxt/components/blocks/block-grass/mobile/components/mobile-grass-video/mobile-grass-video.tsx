import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon, BaseVideoBlockWithButton } from '~/components/base'
import { MobileBlockPhoneButton } from '~/components/blocks/block-phone-button/mobile'
import style from './mobile-grass-video.scss?module'

const cn = useClassNames('mobile-grass-video', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  headingIcon: cn('heading-icon'),
  video: cn('video'),
  definition: cn('definition'),
  text: cn('text')
}

export const MobileGrassVideo = injectStyles(
  style,
  defineComponent({
    name: 'MobileGrassVideo',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>
            <BaseIcon
              name="curve-arrow"
              width={53}
              class={classes.headingIcon}
            />
            Видео о том,
            <br /> как мы готовим
            <br /> наш газон
          </div>
          <BaseVideoBlockWithButton
            withOverlay={true}
            class={classes.video}
            video="https://ecoplant-pitomnik.ru/files/videos/grass.mp4"
            poster="https://ecoplant-pitomnik.ru/image/catalog/blocks/grass/video_poster.jpg"
          />
          <div class={classes.definition}>
            Рулонный газон — это созревший натуральный газон, который срезают и
            заворачивают в рулоны для удобной транспортировки.
          </div>
          <div class={classes.text}>
            Для того, чтобы получить качественный газон, наши дендрологи
            подберут только виды трав, которые лучше всего подходят под
            российские условия
          </div>
          <MobileBlockPhoneButton />
        </div>
      )
    }
  })
)
