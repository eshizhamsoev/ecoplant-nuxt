import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import {
  PhoneLink,
  PhoneSlotData
} from '~/components/action-providers/phone-link'
import { BaseIcon } from '~/components/base'
import style from './mobile-grass-heading.scss?module'

const cn = useClassNames('mobile-grass-heading', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('leading'),
  phone: cn('phone'),
  phoneIcon: cn('phone-icon')
}

export const MobileGrassHeading = injectStyles(
  style,
  defineComponent({
    name: 'MobileGrassHeading',
    props: {},
    setup: () => {
      const phone = ({ phone: { text } }: PhoneSlotData) => [
        <BaseIcon name="phone" width={20} class={classes.phoneIcon} />,
        text
      ]
      return () => (
        <div class={classes.main}>
          <div>
            <PhoneLink scopedSlots={{ default: phone }} class={classes.phone} />
          </div>
          <div class={classes.heading}>
            РУЛОННЫЙ ГАЗОН «ПОД&nbsp;КЛЮЧ» ВЫЕЗД И&nbsp;ПРОЕКТ АБСОЛЮТНО
            БЕСПЛАТНО
          </div>
          <div class={classes.leading}>
            Газон без посредников <br />
            из собственного питомника
          </div>
        </div>
      )
    }
  })
)
