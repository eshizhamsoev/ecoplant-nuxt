import { GrassObject } from '~/components/blocks/block-grass/types'

const getImages = function (path: string, quantity: number) {
  const images = []
  for (let i = 1; i <= quantity; i++) {
    images.push(
      `https://ecoplant-pitomnik.ru/image/catalog/blocks/grass/slider/${path}-${i}.jpg`
    )
  }
  return images
}

export const grassObjects: GrassObject[] = [
  {
    name: 'ВДНХ',
    images: getImages('01-vdnh', 5)
  },
  {
    name: 'ИНКОМ',
    images: getImages('02-incom', 5)
  },
  {
    name: 'Лужники',
    images: getImages('03-lujniki', 5)
  },
  {
    name: 'Новая волна',
    images: getImages('04-novaia-volna', 5)
  },
  {
    name: 'Парк Культуры',
    images: getImages('05-park-kulturi', 5)
  },
  {
    name: 'Пенсионный фонд',
    images: getImages('06-pfr', 5)
  },
  {
    name: 'Сокольники',
    images: getImages('07-sokolniki', 5)
  },
  {
    name: 'Струнино',
    images: getImages('08-strumino', 5)
  }
]

type Image = {
  small: {
    src: string
    width: number
    height: number
  }
  large: string
}

export const grassSlider: Image[] = []

for (let i = 1; i < 9; i++) {
  grassSlider.push({
    small: {
      src: `https://ecoplant-pitomnik.ru/image/catalog/blocks/grass/photo-slider/0${i}.jpg`,
      width: 1000,
      height: 535
    },
    large: `https://ecoplant-pitomnik.ru/image/catalog/blocks/grass/photo-slider/0${i}.jpg`
  })
}
