import { GrassStructureProps } from '~/components/blocks/block-grass/common/grass-structure/props'
import { GrassPalletProps } from '~/components/blocks/block-grass/common/grass-pallet/props'

export const blockGrassProps = {
  structure: {
    type: Object as () => GrassStructureProps,
    default() {
      return {}
    }
  },
  pallet: {
    type: Object as () => GrassPalletProps,
    default() {
      return {}
    }
  },
  withBackButton: {
    type: Boolean,
    default: false
  }
}
