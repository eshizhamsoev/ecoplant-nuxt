import type { ComponentInstance } from '@nuxtjs/composition-api'
import {
  DeliveryTable,
  RubleLine,
  Cell,
  Line,
  Rubles
} from './table-constructor'

export const msk = [
  new DeliveryTable(['Газель 4 метра', 'Фургон\n5.4м кузов'])
    .addLine(
      new Line('С пропуском в Москву:', false)
        .addCell(new Rubles(7000, 1))
        .addCell(new Rubles(11900))
    )
    .addLine(new Line('Московская область', true))
    .addLine(RubleLine.make('до 10км', [3970, 8970]))
    .addLine(RubleLine.make('11-30км', [4570, 9970]))
    .addLine(RubleLine.make('31-50км', [4970, 10970]))
    .addLine(RubleLine.make('51-70км', [5970, 11970]))
    .addLine(RubleLine.make('71-100км', [7970, 12970]))
    .addLine(RubleLine.make('101-150км', [8970, 15970]))
    .addLine(RubleLine.make('151-200км', [9970, 16970]))
    .addLine(new Line('более 200км', false).addCell(new Cell('Договорная', 2)))
    .addLine(
      new Line('Смена').addCell(new Cell('4 часа')).addCell(new Cell('7 часов'))
    )
    .addLine(
      new Line('Стоимость доп. часа')
        .addCell(new Rubles(1000))
        .addCell(new Rubles(1250))
    ),
  new DeliveryTable(['Манипулятор 5м. кузов', 'Манипулятор 7м. кузов'])
    .addLine(
      new Line('С пропуском в Москву:', false).addCell(
        new Cell('Договорная', 2)
      )
    )
    .addLine(new Line('Московская область', true))
    .addLine(RubleLine.make('до 10км', [8970, 12970]))
    .addLine(RubleLine.make('11-30км', [9970, 13970]))
    .addLine(RubleLine.make('31-50км', [10970, 14970]))
    .addLine(RubleLine.make('51-70км', [11970, 15970]))
    .addLine(RubleLine.make('71-100км', [15970, 24970]))
    .addLine(RubleLine.make('101-150км', [23970, 29970]))
    .addLine(RubleLine.make('151-200км', [28970, 35970]))
    .addLine(new Line('более 200км').addCell(new Cell('Договорная', 2)))
    .addLine(new Line('Смена').addCell(new Cell('8 часов', 2)))
    .addLine(new Line('Стоимость доп. часа').addCell(new Rubles(2000, 2)))
]

export const spb = [
  new DeliveryTable(['Газель 4 метра', 'Фургон\n5.4м кузов'])
    .addLine(
      new Line('С пропуском в центр Санкт-Петербурга:', false).addCell(
        new Cell('Договорная', 2)
      )
    )
    .addLine(new Line('Ленинградская область', true))
    .addLine(RubleLine.make('до 10км', [2500, 4000]))
    .addLine(RubleLine.make('11-30км', [3000, 5000]))
    .addLine(RubleLine.make('31-50км', [4200, 6000]))
    .addLine(RubleLine.make('51-70км', [5500, 7000]))
    .addLine(RubleLine.make('71-100км', [7000, 8000]))
    .addLine(RubleLine.make('101-150км', [9500, 10500]))
    .addLine(RubleLine.make('151-200км', [11800, 12000]))
    .addLine(new Line('более 200км', false).addCell(new Cell('Договорная', 2)))
    .addLine(
      new Line('Смена').addCell(new Cell('4 часа')).addCell(new Cell('7 часов'))
    )
    .addLine(
      new Line('Стоимость доп. часа')
        .addCell(new Rubles(800))
        .addCell(new Rubles(1200))
    ),
  new DeliveryTable([
    'Манипулятор 5м. кузов',
    'Манипулятор 7м. кузов',
    'Кран стрела 25м.'
  ])
    .addLine(
      new Line('С пропуском в центр Санкт-Петербурга:', false).addCell(
        new Cell('Договорная', 3)
      )
    )
    .addLine(new Line('Ленинградская область', true))
    .addLine(RubleLine.make('до 10км', [7500, 8500, 15000]))
    .addLine(RubleLine.make('11-30км', [9000, 11000, 16000]))
    .addLine(RubleLine.make('31-50км', [11000, 13000, 18000]))
    .addLine(RubleLine.make('51-70км', [13000, 15000, 20000]))
    .addLine(RubleLine.make('71-100км', [15000, 20000, 25000]))
    .addLine(RubleLine.make('101-150км', [19000, 24000, 30000]))
    .addLine(RubleLine.make('151-200км', [25000, 32000, 35000]))
    .addLine(new Line('более 200км').addCell(new Cell('Договорная', 3)))
    .addLine(new Line('Смена').addCell(new Cell('8 часов', 3)))
    .addLine(
      new Line('Стоимость доп. часа')
        .addCell(new Rubles(1500, 2))
        .addCell(new Rubles(2500, 2))
    )
]

export const getData = (root: ComponentInstance) =>
  root.$accessor.group === 'spb' ? spb : msk
