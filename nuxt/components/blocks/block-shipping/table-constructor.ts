class Cell {
  cols: number
  value: string | number

  constructor(value: string | number, cols = 1) {
    this.cols = cols
    this.value = value
  }

  toString() {
    return this.value
  }

  get type() {
    return 'raw'
  }
}

class Rubles extends Cell {
  constructor(value: number, cols = 1) {
    super(value, cols)
  }

  get type() {
    return 'rubles'
  }
}

class Line {
  label: string | null
  isHeading: boolean
  columns: number
  rows: number
  cells: Cell[]

  constructor(label: string | null, isHeading = false, rows = 1) {
    this.isHeading = isHeading
    this.label = label
    this.columns = 0
    this.rows = rows
    this.cells = []
  }

  addCell(cell: Cell) {
    this.cells.push(cell)
    return this
  }
}

class RubleLine extends Line {
  static make(label: string, cells: number[]) {
    const line = new RubleLine(label)
    cells.forEach((v) => {
      line.addCell(new Rubles(v))
    })
    return line
  }
}

class DeliveryTable {
  fields: string[]
  columns: number
  body: Line[]

  constructor(fields: string[]) {
    this.fields = fields
    this.columns = fields.length + 1
    this.body = []
  }

  addLine(line: Line) {
    line.columns = this.columns
    this.body.push(line)
    return this
  }
}

export { DeliveryTable, RubleLine, Cell, Rubles, Line }
