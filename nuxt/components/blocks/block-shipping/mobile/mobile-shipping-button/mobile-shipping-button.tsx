import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './mobile-shipping-button.scss?module'

const cn = useClassNames('mobile-shipping-button', style)

const classes = {
  main: cn(),
  text: cn('text'),
  image: cn('image')
}

export const MobileShippingButton = injectStyles(
  style,
  defineComponent({
    name: 'MobileShippingButton',
    props: {
      name: {
        type: String,
        required: true
      },
      image: {
        type: String,
        required: true
      },
      theme: {
        type: String,
        default: null
      }
    },
    setup: (props, { emit }) => {
      return () => (
        <button
          class={cn({ theme: props.theme })}
          onClick={() => emit('click')}
        >
          <div class={classes.text}>{props.name}</div>
          <img
            class={classes.image}
            src={props.image}
            width={140}
            height={77}
            alt={props.name}
            loading="lazy"
          />
        </button>
      )
    }
  })
)
