import { computed, defineComponent, ref } from '@nuxtjs/composition-api'
import { MobileShippingButton } from './mobile-shipping-button'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import manipulator from '~/components/blocks/block-shipping/resources/manipulator_280.png'
import truck from '~/components/blocks/block-shipping/resources/truck_280.png'

import pathIcon from '~/components/blocks/block-shipping/resources/path-icon.svg'
import { ShippingTable } from '~/components/blocks/block-shipping/shipping-table'
import { getData } from '~/components/blocks/block-shipping/tables'
import { BaseBackButton } from '~/components/base'
import style from './mobile-block-shipping.scss?module'

const cn = useClassNames('mobile-block-shipping', style)

const classes = {
  main: cn(),
  inner: cn('inner'),
  iconWrapper: cn('icon-wrapper'),
  icon: cn('icon'),
  heading: cn('heading'),
  text: cn('text'),
  buttons: cn('buttons'),
  button: cn('button'),
  back: cn('back')
}

export const MobileBlockShipping = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockShipping',
    props: {},
    setup(props, { root }) {
      const tableIndex = ref<number | null>(null)
      const data = getData(root)
      const table = computed(() =>
        tableIndex.value !== null ? data[tableIndex.value] : null
      )
      return () => (
        <div class={classes.main}>
          {table.value ? (
            <ShippingTable fields={table.value.fields} lines={table.value.body}>
              <BaseBackButton
                onClick={() => {
                  tableIndex.value = null
                }}
                class={classes.back}
              />
            </ShippingTable>
          ) : (
            <div class={classes.inner}>
              <div class={classes.iconWrapper}>
                <img
                  src={pathIcon}
                  class={classes.icon}
                  alt=""
                  width={55}
                  height={43}
                  loading="lazy"
                />
              </div>
              <div class={classes.heading}>Доставка</div>
              <div class={classes.text}>
                Нажмите на кнопку, чтобы посмотреть подробную информацию о
                способе доставки
              </div>
              <div class={classes.buttons}>
                <MobileShippingButton
                  theme="green"
                  name={'Газель'}
                  image={truck}
                  class={classes.button}
                  onClick={() => {
                    tableIndex.value = 0
                  }}
                />
                <MobileShippingButton
                  name={'Манипулятор'}
                  image={manipulator}
                  class={classes.button}
                  onClick={() => {
                    tableIndex.value = 1
                  }}
                />
              </div>
            </div>
          )}
        </div>
      )
    }
  })
)
