import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import pathIcon from '~/components/blocks/block-shipping/resources/path-icon.svg'
import { ShippingTable } from '~/components/blocks/block-shipping/shipping-table'
import { getData } from '~/components/blocks/block-shipping/tables'
// @ts-ignore
import manipulator from '~/components/blocks/block-shipping/resources/manipulator.png?resize&size=170'
// @ts-ignore
import truck from '~/components/blocks/block-shipping/resources/truck.png?resize&size=170'
import style from './desktop-block-shipping.scss?module'

const cn = useClassNames('block-shipping', style)

const classes = {
  main: cn(),
  iconWrapper: cn('icon-wrapper'),
  icon: cn('icon'),
  heading: cn('heading'),
  wrapper: cn('wrapper'),
  table: cn('table')
}

export const DesktopBlockShipping = injectStyles(
  style,
  defineComponent({
    name: 'BlockShipping',
    setup(props, { root }) {
      const data = getData(root)
      return () => (
        <div class={classes.main}>
          <div class={classes.iconWrapper}>
            <img
              src={pathIcon}
              class={classes.icon}
              alt=""
              width={55}
              height={43}
            />
          </div>
          <div class={classes.heading}>Доставка</div>
          <div class={classes.wrapper}>
            <ShippingTable
              fields={data[0].fields}
              lines={data[0].body}
              class={classes.table}
            >
              <img
                src={truck.src}
                alt=""
                loading={'lazy'}
                width={170}
                height={93}
              />
            </ShippingTable>
            <ShippingTable
              fields={data[1].fields}
              lines={data[1].body}
              class={classes.table}
            >
              <img
                src={manipulator.src}
                alt=""
                loading={'lazy'}
                width={170}
                height={93}
              />
            </ShippingTable>
          </div>
        </div>
      )
    }
  })
)
