import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { Line } from '~/components/blocks/block-shipping/table-constructor'
import { BaseRubles } from '~/components/base/base-rubles'
import style from './shipping-table.scss?module'

const cn = useClassNames('shipping-table', style)

const classes = {
  main: cn(),
  firstCell: cn('first-cell'),
  lineHeading: cn('line-heading'),
  emptyCell: cn('empty-cell')
}

export const ShippingTable = injectStyles(
  style,
  defineComponent({
    name: 'ShippingTable',
    props: {
      fields: {
        type: Array as () => string[],
        required: true
      },
      lines: {
        type: Array as () => Line[],
        required: true
      }
    },
    setup: (props, { slots }) => {
      return () => (
        <table class={classes.main}>
          <thead>
            <tr>
              <td class={classes.firstCell}>
                {slots.default && slots.default()}
              </td>
              {props.fields.map((field) => (
                <th>{field}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {props.lines.map((line) => (
              <tr>
                {line.isHeading
                  ? [
                      <td class={classes.emptyCell} />,
                      <td
                        colspan={line.columns - 1}
                        class={classes.lineHeading}
                      >
                        {line.label}
                      </td>
                    ]
                  : [
                      line.rows > 0 && (
                        <th rowspan={line.rows}>{line.label}</th>
                      ),
                      line.cells.map((cell) => (
                        <td colspan={cell.cols}>
                          {cell.type === 'rubles' ? (
                            <BaseRubles price={cell.value as number} />
                          ) : (
                            cell.value
                          )}
                        </td>
                      ))
                    ]}
              </tr>
            ))}
          </tbody>
        </table>
      )
    }
  })
)
