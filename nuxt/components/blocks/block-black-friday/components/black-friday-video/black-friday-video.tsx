import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon, BaseVideoBlock } from '~/components/base'
import style from './black-friday-video.scss?module'

const cn = useClassNames('black-friday-video', style)

const video = {
  poster: 'https://ecoplant-pitomnik.ru/image/catalog/black-friday/poster1.jpg',
  src: 'https://ecoplant-pitomnik.ru/files/videos/black_friday/how-going.mp4'
}

export const BlackFridayVideo = injectStyles(
  style,
  defineComponent({
    name: 'BlackFridayVideo',
    props: {},
    setup: () => {
      return () => (
        <div class={cn()}>
          <div class={cn('video-backdrop')}>
            <div class={cn('video-container')}>
              <BaseVideoBlock
                class={cn('video')}
                video={video.src}
                poster={video.poster}
              >
                <div class={cn('content')}>
                  <button class={cn('play')}>
                    <BaseIcon name="play" width={40} />
                  </button>
                  <div class={cn('text')}>
                    <div class={cn('title')}>
                      КАК ПРОХОДИТ ЧЕРНАЯ ПЯТНИЦА В НАШЕМ ПИТОМНИКЕ
                    </div>
                    <div class={cn('subtitle')}>
                      чтобы вы понимали о чем речь и успели приобрести себе
                      растение
                    </div>
                  </div>
                </div>
              </BaseVideoBlock>
            </div>
          </div>
        </div>
      )
    }
  })
)
