import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BlackFridayProductGroup as ProductGroup } from '~/components/blocks/block-black-friday/types/product'
import { BlackFridayProduct } from '~/components/blocks/block-black-friday/components/black-friday-product'
import style from './black-friday-product-group.scss?module'

const cn = useClassNames('black-friday-product-group', style)

export const BlackFridayProductGroup = injectStyles(
  style,
  defineComponent({
    name: 'BlackFridayProductGroup',
    props: {
      group: {
        type: Object as () => ProductGroup,
        required: true
      },
      showTitleOnMobile: {
        type: Boolean,
        default: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={cn()}>
          {props.group.title ? (
            <div
              class={cn('title', { 'mobile-hidden': props.showTitleOnMobile })}
            >
              {props.group.title}
            </div>
          ) : (
            ''
          )}
          <div class={cn('products')}>
            <BlackFridayProduct product={props.group.firstProduct} />
            {props.group.secondProduct && (
              <BlackFridayProduct
                product={props.group.secondProduct}
                isSecond={true}
              />
            )}
          </div>
        </div>
      )
    }
  })
)
