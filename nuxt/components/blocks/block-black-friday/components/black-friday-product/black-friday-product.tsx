import { defineComponent, onBeforeMount, ref } from '@nuxtjs/composition-api'
import { BlackFridayProductInfo } from './black-friday-product-info'
import { BlackFridayProductSizes } from './black-friday-product-sizes'
import { BlackFridayProductImages } from './black-friday-product-images/black-friday-product-images'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BlackFridayProduct as Product } from '~/components/blocks/block-black-friday/types/product'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './black-friday-product.scss?module'

const cn = useClassNames('black-friday-product', style)

export const BlackFridayProduct = injectStyles(
  style,
  defineComponent({
    name: 'BlackFridayProduct',
    props: {
      product: {
        type: Object as () => Product,
        required: true
      },
      isSecond: {
        type: Boolean,
        default: false
      }
    },
    setup: (props, { root }) => {
      const mobileView = ref(root.$device.isMobileOrTablet)
      onBeforeMount(function () {
        mobileView.value = window.innerWidth < 600
      })

      const modalOpener = useModalOpener()
      const openModal = () =>
        modalOpener({
          name: 'black-friday-product',
          product: props.product
        })
      return () => (
        <div class={cn()}>
          <div class={cn('title')}>{props.product.info.name}</div>
          <div class={cn('main')}>
            <div class={cn('image')}>
              <BlackFridayProductImages
                images={props.product.images}
                isMobileView={mobileView.value}
              />
            </div>
            <BlackFridayProductInfo
              info={props.product.info}
              class={cn('info')}
            />
          </div>
          {props.product.variants.length > 0 && (
            <div class={cn('variants-button-wrapper')}>
              <button onClick={openModal} class={cn('green-miracle-button')}>
                {props.product.variantsButton || 'Посмотреть всю коллекцию'}
              </button>
            </div>
          )}
          {props.product.sizes.length > 0 && (
            <BlackFridayProductSizes
              isSameSize={props.product.info.isSameSize}
              class={cn('sizes')}
              title={props.product.info.name}
              sizes={props.product.sizes}
            />
          )}
        </div>
      )
    }
  })
)
