import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseRubles } from '~/components/base'
import style from './black-friday-product-price.scss?module'

const cn = useClassNames('black-friday-product-price', style)

export const BlackFridayProductPrice = injectStyles(
  style,
  defineComponent({
    name: 'BlackFridayProductPrice',
    props: {
      value: {
        type: Number,
        required: true
      },
      isNew: {
        type: Boolean,
        default: false
      }
    },
    setup: (props) => {
      return () => (
        <div class={cn()}>
          <div class={cn('label', { old: !props.isNew })}>
            {props.isNew ? 'Цена черной пятницы:' : 'Обычная цена:'}
          </div>
          <div class={cn('price', { type: props.isNew ? 'new' : 'old' })}>
            <BaseRubles price={props.value} />
          </div>
        </div>
      )
    }
  })
)
