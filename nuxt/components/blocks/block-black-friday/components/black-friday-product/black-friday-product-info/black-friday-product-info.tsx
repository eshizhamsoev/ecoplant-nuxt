import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BlackFridayProductPrice } from '~/components/blocks/block-black-friday/components/black-friday-product/black-friday-product-info/black-friday-product-price'
import { BlackFridayProductInfo as Info } from '~/components/blocks/block-black-friday/types/product'
import { BlackFridaySameSize } from '~/components/blocks/block-black-friday/components/black-friday-same-size'
import style from './black-friday-product-info.scss?module'

const cn = useClassNames('black-friday-product-info', style)

export const BlackFridayProductInfo = injectStyles(
  style,
  defineComponent({
    name: 'BlackFridayProductInfo',
    props: {
      info: {
        type: Object as () => Info,
        required: true
      }
    },
    setup: (props) => {
      const percent = computed(() => {
        if (props.info.percent) {
          return props.info.percent
        }
        return Math.round(
          ((props.info.price.old - props.info.price.new) /
            props.info.price.old) *
            100
        )
      })
      return () => (
        <div class={cn()}>
          <div class={cn('top')}>
            <div class={cn('name-wrapper')}>
              <div class={cn('name')}>{props.info.name}</div>
              <div class={cn('size')}>Высота: {props.info.size}</div>
            </div>
            <div class={cn('percent', { type: 'mobile' })}>
              {`-${percent.value}%`}
            </div>
          </div>
          <div class={cn('border')}></div>
          <div class={cn('bottom')}>
            {props.info.isSameSize && (
              <BlackFridaySameSize class={cn('same-size')} />
            )}
            <div class={cn('prices')}>
              <BlackFridayProductPrice
                value={props.info.price.old}
                isNew={false}
                class={cn('price-old')}
              />
              <BlackFridayProductPrice
                value={props.info.price.new}
                isNew={true}
                class={cn('price-new')}
              />
            </div>
            <div
              class={cn('percent', { type: 'desktop' })}
            >{`-${percent.value}%`}</div>
          </div>
        </div>
      )
    }
  })
)
