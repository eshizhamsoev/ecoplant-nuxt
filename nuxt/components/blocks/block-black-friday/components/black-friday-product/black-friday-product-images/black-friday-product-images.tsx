import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BlackFridayProductImage } from '~/components/blocks/block-black-friday/types/product'
import { BaseSliderWithCutButtons } from '~/components/base'
import { svgPlaceholder } from '~/support/utils/svg-placeholder'
import style from './black-friday-product-images.scss?module'

const cn = useClassNames('black-friday-product-images', style)
const options = { gutter: 10 }
export const BlackFridayProductImages = injectStyles(
  style,
  defineComponent({
    name: 'BlackFridayProductImages',
    props: {
      images: {
        type: Array as () => readonly BlackFridayProductImage[],
        required: true
      },
      isMobileView: {
        type: Boolean,
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={cn()}>
          <BaseSliderWithCutButtons options={options}>
            {props.images.map((image) => (
              <div>
                <img
                  src={svgPlaceholder(315, 420)}
                  data-src={image.small}
                  width={315}
                  height={420}
                  class={[cn('image'), 'tns-lazy-img']}
                />
              </div>
            ))}
          </BaseSliderWithCutButtons>
        </div>
      )
    }
  })
)
