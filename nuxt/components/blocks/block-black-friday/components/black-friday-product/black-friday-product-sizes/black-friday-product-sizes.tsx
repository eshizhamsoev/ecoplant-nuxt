import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseRubles } from '~/components/base'
import { BlackFridayProductSize } from '~/components/blocks/block-black-friday/types/product'
import { BlackFridaySameSize } from '~/components/blocks/block-black-friday/components/black-friday-same-size'
import style from './black-friday-product-sizes.scss?module'

const cn = useClassNames('black-friday-product-sizes', style)

export const BlackFridayProductSizes = injectStyles(
  style,
  defineComponent({
    name: 'BlackFridayProductSizes',
    props: {
      title: {
        type: String,
        required: true
      },
      sizes: {
        type: Array as () => readonly BlackFridayProductSize[],
        required: true
      },
      isSameSize: {
        type: Boolean,
        default: false
      }
    },
    setup: (props) => {
      return () => (
        <div class={cn()}>
          {props.isSameSize && <BlackFridaySameSize class={cn('same-size')} />}
          <div class={cn('table')}>
            <div class={cn('cell', { type: 'size', 'is-heading': true })}>
              Размер
            </div>
            <div class={cn('cell', { type: 'new-price', 'is-heading': true })}>
              Цена черной пятницы
            </div>
            <div class={cn('cell', { type: 'old-price', 'is-heading': true })}>
              Цена обычного дня
            </div>
            {props.sizes.map((priceItem) => [
              <div class={cn('cell', { type: 'size' })}>
                {priceItem.name}
                {priceItem.saleLabel && (
                  <span class={cn('sale')}> SUPERSALE</span>
                )}
              </div>,
              <div class={cn('cell', { type: 'new-price' })}>
                <BaseRubles price={priceItem.price.new} />
                {/* {priceItem.saleLabel && ( */}
                {/*  <BaseIcon name={'sale'} width={40} class={cn('sale-icon')} /> */}
                {/* )} */}
              </div>,
              <div class={cn('cell', { type: 'old-price' })}>
                <BaseRubles
                  price={priceItem.price.old}
                  class={cn('old-price')}
                />
              </div>
            ])}
          </div>
        </div>
      )
    }
  })
)
