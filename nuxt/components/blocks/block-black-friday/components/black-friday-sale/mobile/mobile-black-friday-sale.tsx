import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobileSaleFormLine } from '~/components/blocks/shared-components/sale-form-line/mobile'
import fertilizerMobile from '~/assets/common/fertilizer-sale-mobile.png'
import stripeMobileTop from '~/assets/common/stripe-sale-mob-top.svg'
import stripeMobileBottom from '~/assets/common/stripe-sale-mob-bottom.svg'
import style from './mobile-black-friday-sale.scss?module'

const cn = useClassNames('mobile-black-friday-sale', style)

const classes = {
  main: cn(),
  inner: cn('inner'),
  stripe: cn('stripe'),
  heading: cn('heading'),
  fertilizerLine: cn('fertilizer-line'),
  fertilizer: cn('fertilizer'),
  banner: cn('banner'),
  bannerImage: cn('banner-image'),
  plus: cn('plus'),
  fertilizerText: cn('fertilizer-text'),
  form: cn('form'),
  phone: cn('phone'),
  button: cn('button'),
  policy: cn('policy'),
  circleButtonsWrapper: cn('circle-buttons-wrapper'),
  whatsappButton: cn('whatsapp-button'),
  phoneButton: cn('phone-button')
}

export const MobileBlackFridaySale = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlackFridaySale',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <div class={cn('stripe', { type: 'top' })}>
            <img src={stripeMobileTop} alt="Черная пятница" />
          </div>
          <div class={classes.inner}>
            <div class={classes.circleButtonsWrapper}>
              <div class={classes.banner}>
                <div class={classes.heading}>
                  Получить
                  <br />
                  <b>скидку 90%</b>
                </div>
                <div class={classes.fertilizerLine}>
                  <span class={classes.fertilizerText}>
                    Удобрение в подарок!
                  </span>
                </div>
              </div>
              <div class={classes.bannerImage}>
                <img src={fertilizerMobile} alt="" />
              </div>
            </div>
            <MobileSaleFormLine class={classes.form} source="Черная Пятница">
              <template slot="button">Забронировать скидку</template>
            </MobileSaleFormLine>
            <div class={classes.policy}>
              Согласен с политикой конфиденциальности
            </div>
          </div>
          <div class={cn('stripe', { type: 'bottom' })}>
            <img src={stripeMobileBottom} alt="Черная пятница" />
          </div>
        </div>
      )
    }
  })
)
