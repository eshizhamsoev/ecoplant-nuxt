import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const BlackFridaySale = UtilUniversalComponentFactory(
  'BlackFridaySale',
  () => import('./mobile').then((c) => c.MobileBlackFridaySale),
  () => import('./desktop').then((c) => c.DesktopBlackFridaySale)
)
