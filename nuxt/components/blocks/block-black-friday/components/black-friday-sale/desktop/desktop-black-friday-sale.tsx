import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { DesktopSaleFormLine } from '~/components/blocks/shared-components/sale-form-line/desktop'
import fertilizer from '~/assets/common/fertilizer-sale.png'
import stripe from '~/assets/common/stripe-sale.png'
import { BasePolicyLink } from '~/components/base/base-policy-link/base-policy-link'
import style from './desktop-black-friday-sale.scss?module'

const cn = useClassNames('desktop-black-friday-sale', style)

export const DesktopBlackFridaySale = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlackFridaySale',
    props: {},
    setup: () => {
      return () => (
        <div class={cn()}>
          <div class={cn('backdrop')}>
            <div class={cn('inner')}>
              <div class={cn('content')}>
                <div class={cn('heading')}>
                  Получите <b>скидку 90%</b>
                </div>
                <div class={cn('leading')}>и получите удобрение в подарок</div>
                <DesktopSaleFormLine
                  class={cn('form')}
                  source="Забронировать скидку"
                >
                  <template slot="button">Забронировать скидку</template>
                </DesktopSaleFormLine>
                <BasePolicyLink
                  class={cn('policy')}
                  text={'Согласен с политикой конфиденциальности'}
                />
              </div>
            </div>
            <div class={cn('image')}>
              <img src={fertilizer} alt="Удобрение" />
            </div>
          </div>
          <div class={cn('image', { type: 'stripe' })}>
            <img src={stripe} alt="Полоса черная пятница" />
          </div>
        </div>
      )
    }
  })
)
