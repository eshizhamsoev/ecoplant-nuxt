import { defineComponent, useContext } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BlackFridayPlant } from '~/components/blocks/block-black-friday/types/plant'
import { BaseBadgedText } from '~/components/base/base-badged-text/base-badged-text'
import style from './black-friday-plant-grid.scss?module'

const cn = useClassNames('black-friday-plant-grid', style)

export const BlackFridayPlantGrid = injectStyles(
  style,
  defineComponent({
    name: 'BlackFridayPlantGrid',
    props: {
      plants: {
        type: Array as () => readonly BlackFridayPlant[],
        required: true
      }
    },
    setup: (props) => {
      const { $device } = useContext()
      return () => (
        <div class={cn()} ref="main">
          {props.plants.map((plant) => (
            <div class={cn('item')}>
              <img
                width={136}
                height={136}
                src={plant.src}
                alt={plant.title}
                class={cn('image')}
                loading="lazy"
              />
              {$device.isMobileOrTablet ? (
                <BaseBadgedText lines={plant.title} class={cn('title')} />
              ) : (
                <div class={cn('title')}>{plant.title}</div>
              )}
            </div>
          ))}
        </div>
      )
    }
  })
)
