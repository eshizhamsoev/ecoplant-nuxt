import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BlackFridayProduct as Product } from '~/components/blocks/block-black-friday/types/product'
import { BlackFridayProduct } from '~/components/blocks/block-black-friday/components/black-friday-product'
import style from './black-friday-product-single.scss?module'

const cn = useClassNames('black-friday-product-single', style)

export const BlackFridayProductSingle = injectStyles(
  style,
  defineComponent({
    name: 'BlackFridayProductSingle',
    props: {
      product: {
        type: Object as () => Product,
        required: true
      },
      title: {
        type: String,
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={cn()}>
          <div class={cn('title')}>{props.title}</div>
          <div class={cn('products')}>
            <BlackFridayProduct product={props.product} />
          </div>
        </div>
      )
    }
  })
)
