import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BlackFridayPlantGrid } from '~/components/blocks/block-black-friday/components/black-friday-plant-grid/black-friday-plant-grid'
import { BlackFridayPlant } from '~/components/blocks/block-black-friday/types/plant'
import style from './black-friday-plants.scss?module'

const cn = useClassNames('black-friday-plants', style)

export const BlackFridayPlants = injectStyles(
  style,
  defineComponent({
    name: 'BlackFridayPlants',
    props: {
      images: {
        type: Array as () => readonly BlackFridayPlant[],
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={cn()}>
          <div class={cn('title')}>
            Так же на черной пятнице вы можете приобрести еще{' '}
            <b>
              более 17 000 наименований растений и деревьев нашего питомника, со
              скидкой до -90%
            </b>
          </div>
          <BlackFridayPlantGrid class={cn('items')} plants={props.images} />
        </div>
      )
    }
  })
)
