import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './black-friday-same-size.scss?module'

const cn = useClassNames('black-friday-same-size', style)

export const BlackFridaySameSize = injectStyles(
  style,
  defineComponent({
    name: 'BlackFridaySameSize',
    props: {},
    setup: () => {
      return () => (
        <div class={cn()}>
          <div class={cn('inner')}>Размер не имеет значение</div>
        </div>
      )
    }
  })
)
