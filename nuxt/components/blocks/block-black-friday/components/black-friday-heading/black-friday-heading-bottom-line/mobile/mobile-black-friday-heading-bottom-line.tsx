import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { useCoronaPopups } from '~/components/modals/content/modal-corona-info/modals'
import style from './mobile-black-friday-heading-bottom-line.scss?module'

const cn = useClassNames('mobile-black-friday-heading-bottom-line', style)

export const MobileBlackFridayHeadingBottomLine = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlackFridayHeadingBottomLine',
    props: {},
    setup: () => {
      const modals = useCoronaPopups()
      return () => (
        <div class={cn()}>
          <div class={cn('item')} onClick={modals.shipping}>
            <div class={cn('image-wrapper')}>
              <img
                src={require('~/assets/common/corona-icons/shipping.png?resize&size=60')}
                width={60}
                class={cn('image')}
              />
            </div>
            <div>
              <b>Бесконтактная</b>
              <br /> доставка и посадка
            </div>
          </div>
          <div class={cn('item')} onClick={modals.safe}>
            <div class={cn('image-wrapper')}>
              <img
                src={require('~/assets/common/corona-icons/safe.png?resize&size=60')}
                width={60}
                class={cn('image')}
              />
            </div>
            <div>
              Не назначаем
              <br /> <b>массовые</b> встречи
            </div>
          </div>
        </div>
      )
    }
  })
)
