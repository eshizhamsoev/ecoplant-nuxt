import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const BlackFridayHeadingBottomLine = UtilUniversalComponentFactory(
  'BlackFridayHeadingBottomLine',
  () =>
    import('./mobile/mobile-black-friday-heading-bottom-line').then(
      (c) => c.MobileBlackFridayHeadingBottomLine
    ),
  () =>
    import('./desktop/desktop-black-friday-heading-bottom-line').then(
      (c) => c.DesktopBlackFridayHeadingBottomLine
    )
)
