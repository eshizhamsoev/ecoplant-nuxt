import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { HeadingBottomLineItem } from '~/components/blocks/block-heading/desktop/heading-bottom-line/item'
import { BaseButton } from '~/components/base'
import { Visual } from '~/components/theme/theme-button'
import { scrollToBlock } from '~/support/utils/scroll-to-block'
import { useCoronaPopups } from '~/components/modals/content/modal-corona-info/modals'
import style from './desktop-black-friday-heading-bottom-line.scss?module'

const cn = useClassNames('desktop-black-friday-heading-bottom-line', style)

export const DesktopBlackFridayHeadingBottomLine = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlackFridayHeadingBottomLine',
    props: {},
    setup: () => {
      const modals = useCoronaPopups()
      return () => (
        <div class={cn()}>
          <HeadingBottomLineItem
            image={require('~/assets/common/corona-icons/shipping.png?resize&size=133').toString()}
            class={cn('corona-button')}
            onClick={modals.shipping}
          >
            <b>Бесконтактная</b>
            <br /> доставка и посадка
          </HeadingBottomLineItem>
          <BaseButton
            size={'l'}
            visual={Visual.gradient}
            shadow={true}
            class={cn('button')}
            onClick={() => scrollToBlock('black-friday-content')}
          >
            Смотреть самые выгодные предложения
          </BaseButton>
          <HeadingBottomLineItem
            image={require('~/assets/common/corona-icons/safe.png?resize&size=126').toString()}
            class={cn('corona-button')}
            onClick={modals.safe}
          >
            Не назначаем
            <br /> <b>массовые</b> встречи
          </HeadingBottomLineItem>
        </div>
      )
    }
  })
)
