import {
  computed,
  defineComponent,
  onBeforeUnmount,
  onMounted,
  ref,
  useContext
} from '@nuxtjs/composition-api'
import Vue from 'vue'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { HeadingWrapper } from '~/components/blocks/shared-components/heading/heading-wrapper'
import { HeadingTopLine } from '~/components/blocks/shared-components/heading/heading-top-line'
import stripe from '~/assets/common/stripe-heading.svg'
import { DesktopStickyLine } from '~/components/layouts/mobile/desktop-sticky-line'
import style from './black-friday-heading.scss?module'

const cn = useClassNames('black-friday-heading', style)

export const BlackFridayHeading = injectStyles(
  style,
  defineComponent({
    name: 'BlackFridayHeading',
    props: {},
    setup: (props, { refs }) => {
      const { $device } = useContext()
      const scrollY = ref(0)
      const height = ref(100)
      const listener = () => {
        scrollY.value = window.scrollY
      }
      onMounted(() => {
        if (refs.main) {
          height.value = (refs.main as Vue).$el.clientHeight - 100
        }
        listener()
        window.addEventListener('scroll', listener, { passive: true })
      })
      onBeforeUnmount(() => {
        window.removeEventListener('scroll', listener)
      })

      const headerIsScrolled = computed(() => height.value < scrollY.value)
      return () => (
        <HeadingWrapper
          ref="main"
          background={
            'https://ecoplant-pitomnik.ru//image/cache/catalog/landings/headings/large-2-0x0.jpg'
          }
          withOverlay={true}
          class={cn()}
        >
          <div class={cn('stripe', { type: 'left' })}>
            <img src={stripe} alt="" />
          </div>
          <div class={cn('stripe', { type: 'right' })}>
            <img src={stripe} alt="" />
          </div>
          <HeadingTopLine class={cn('top-line')} sloganClass={cn('slogan')} />
          <div class={cn('content')}>
            <div class={cn('date')}>22, 23, 24 октября</div>
            <div class={cn('heading')}>
              ЧЕРНАЯ <br /> <b>ПЯТНИЦА</b>
            </div>
            <div class={cn('leading')}>
              <span>Скидки до 90%</span> на 37 500 наименований растений нашего
              питомника в течение 3 дней!
            </div>
          </div>
          {$device.isDesktop ? (
            <DesktopStickyLine
              class={cn('sticky-line', { visible: headerIsScrolled.value })}
            />
          ) : (
            ''
          )}
        </HeadingWrapper>
      )
    }
  })
)
