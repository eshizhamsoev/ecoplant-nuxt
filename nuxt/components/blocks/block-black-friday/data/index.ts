import type { ComponentInstance } from '@nuxtjs/composition-api'
import { groups as msk } from './msk'
import { groups as spb } from './spb'
export { plantImages } from './plants'
export const getProductGroups = (root: ComponentInstance) =>
  root.$accessor.group === 'spb' ? spb : msk
