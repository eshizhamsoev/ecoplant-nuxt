const basePrefix = 'https://ecoplant-pitomnik.ru/image/catalog/black-friday/'
export const getImage = (imageName: string) => basePrefix + imageName

const getProductImage = (imageDirectory: string, imageBaseName: string) => ({
  small: getImage('product/small/' + imageDirectory + '/' + imageBaseName),
  large: getImage('product/' + imageDirectory + '/' + imageBaseName)
})

export const getProductImageByNumber = (
  imageDirectory: string,
  imageNumber: number
) => getProductImage(imageDirectory, imageNumber + '.jpg')
