import { getImage } from './helpers'

export const plantImages = [
  {
    src: getImage('plants/1.jpg'),
    title: ['Азалия']
  },
  {
    src: getImage('plants/2.jpg'),
    title: ['Барбарис']
  },
  {
    src: getImage('plants/3.jpg'),
    title: ['Береза', 'плакучая']
  },
  {
    src: getImage('plants/4.jpg'),
    title: ['Бересклет']
  },
  {
    src: getImage('plants/5.jpg'),
    title: ['Вейгела']
  },
  {
    src: getImage('plants/6.jpg'),
    title: ['Вишня', 'умбракулифера']
  },
  {
    src: getImage('plants/7.jpg'),
    title: ['Голубика']
  },
  {
    src: getImage('plants/8.jpg'),
    title: ['Гортензия']
  },
  {
    src: getImage('plants/9.jpg'),
    title: ['Дерен']
  },
  {
    src: getImage('plants/10.jpg'),
    title: ['Дуб']
  },
  {
    src: getImage('plants/11.jpg'),
    title: ['Ель', 'глаука', 'глобоза']
  },
  {
    src: getImage('plants/12.jpg'),
    title: ['Ель инверса']
  },
  {
    src: getImage('plants/13.jpg'),
    title: ['Ель коника']
  },
  {
    src: getImage('plants/14.jpg'),
    title: ['Ель костер']
  },
  {
    src: getImage('plants/15.jpg'),
    title: ['Ель', 'ольденбург']
  },
  {
    src: getImage('plants/16.jpg'),
    title: ['Ель сербская']
  },
  {
    src: getImage('plants/17.jpg'),
    title: ['Ива']
  },
  {
    src: getImage('plants/18.jpg'),
    title: ['Ирга']
  },
  {
    src: getImage('plants/19.jpg'),
    title: ['Каштан']
  },
  {
    src: getImage('plants/20.jpg'),
    title: ['Кизильник']
  },
  {
    src: getImage('plants/21.jpg'),
    title: ['Клен', 'гиннала']
  },
  {
    src: getImage('plants/22.jpg'),
    title: ['Клен глобозум']
  },
  {
    src: getImage('plants/23.jpg'),
    title: ['Клен красный']
  },
  {
    src: getImage('plants/24.jpg'),
    title: ['Клен', 'остролистный']
  },
  {
    src: getImage('plants/25.jpg'),
    title: ['Можжевельник', 'блю эрроу']
  },
  {
    src: getImage('plants/26.jpg'),
    title: ['Пихта кореана']
  },
  {
    src: getImage('plants/27.jpg'),
    title: ['Пузыреплодник']
  },
  {
    src: getImage('plants/28.jpg'),
    title: ['Родендрон']
  },
  {
    src: getImage('plants/29.jpg'),
    title: ['Рябина']
  },
  {
    src: getImage('plants/30.jpg'),
    title: ['Самшит']
  },
  {
    src: getImage('plants/31.jpg'),
    title: ['Сирень']
  },
  {
    src: getImage('plants/32.jpg'),
    title: ['Сосна валентайн']
  },
  {
    src: getImage('plants/33.jpg'),
    title: ['Сосна ватерери']
  },
  {
    src: getImage('plants/34.jpg'),
    title: ['Сосна на', 'штамбе']
  },
  {
    src: getImage('plants/35.jpg'),
    title: ['Сосна', 'умбракулифера']
  },
  {
    src: getImage('plants/36.jpg'),
    title: ['Сосна цембра']
  },
  {
    src: getImage('plants/37.jpg'),
    title: ['Спирея']
  },
  {
    src: getImage('plants/38.jpg'),
    title: ['Тополь']
  },
  {
    src: getImage('plants/39.jpg'),
    title: ['Тсуга канадская']
  },
  {
    src: getImage('plants/40.jpg'),
    title: ['Туя еллоу', 'риббон']
  },
  {
    src: getImage('plants/41.jpg'),
    title: ['Туя колумна']
  },
  {
    src: getImage('plants/42.jpg'),
    title: ['Туя шаровидная']
  },
  {
    src: getImage('plants/43.jpg'),
    title: ['Черемуха']
  },
  {
    src: getImage('plants/44.jpg'),
    title: ['Чубушник']
  },
  {
    src: getImage('plants/45.jpg'),
    title: ['Ясень']
  }
]
