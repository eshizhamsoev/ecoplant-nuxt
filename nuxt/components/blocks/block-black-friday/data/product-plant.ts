import { getImage } from './helpers'

export const collections = {
  bush: Object.freeze([
    {
      src: getImage('plant_cust/1.jpg'),
      title: ['Арония']
    },
    {
      src: getImage('plant_cust/2.jpg'),
      title: ['Барбарис']
    },
    {
      src: getImage('plant_cust/3.jpg'),
      title: ['Бересклет']
    },
    {
      src: getImage('plant_cust/4.jpg'),
      title: ['Бирючина']
    },
    {
      src: getImage('plant_cust/5.jpg'),
      title: ['Боярышник']
    },
    {
      src: getImage('plant_cust/6.jpg'),
      title: ['Бузина']
    },
    {
      src: getImage('plant_cust/7.jpg'),
      title: ['Виноград']
    },
    {
      src: getImage('plant_cust/8.jpg'),
      title: ['Голубика']
    },
    {
      src: getImage('plant_cust/9.jpg'),
      title: ['Дерен']
    },
    {
      src: getImage('plant_cust/10.jpg'),
      title: ['Жимолость']
    },
    {
      src: getImage('plant_cust/11.jpg'),
      title: ['Ива Пурпурная Нана']
    },
    {
      src: getImage('plant_cust/12.jpg'),
      title: ['Карагана']
    },
    {
      src: getImage('plant_cust/13.jpg'),
      title: ['Кизильник Блестящий']
    },
    {
      src: getImage('plant_cust/14.jpg'),
      title: ['Клен Гиннала']
    },
    {
      src: getImage('plant_cust/15.jpg'),
      title: ['Облепиха']
    },
    {
      src: getImage('plant_cust/16.jpg'),
      title: ['Пузыреплодник']
    },
    {
      src: getImage('plant_cust/17.jpg'),
      title: ['Смородина']
    }
  ]),
  flower: Object.freeze([
    {
      src: getImage('plant_cvet/1.jpg'),
      title: ['Азалия']
    },
    {
      src: getImage('plant_cvet/2.jpg'),
      title: ['Ирга']
    },
    {
      src: getImage('plant_cvet/3.jpg'),
      title: ['Гортензия']
    },
    {
      src: getImage('plant_cvet/4.jpg'),
      title: ['Калина']
    },
    {
      src: getImage('plant_cvet/5.jpg'),
      title: ['Лапчатка']
    },
    {
      src: getImage('plant_cvet/6.jpg'),
      title: ['Рододендрон']
    },
    {
      src: getImage('plant_cvet/7.jpg'),
      title: ['Роза']
    },
    {
      src: getImage('plant_cvet/8.jpg'),
      title: ['Рябинник Рябинолистный']
    },
    {
      src: getImage('plant_cvet/9.jpg'),
      title: ['Сирень']
    },
    {
      src: getImage('plant_cvet/10.jpg'),
      title: ['Снежноягодник']
    },
    {
      src: getImage('plant_cvet/11.jpg'),
      title: ['Спирея']
    },
    {
      src: getImage('plant_cvet/12.jpg'),
      title: ['Форзиция']
    },
    {
      src: getImage('plant_cvet/13.jpg'),
      title: ['Чубушник (Жасмин)']
    }
  ]),
  leaf: Object.freeze([
    {
      src: getImage('plant_list/1.jpg'),
      title: ['Акация']
    },
    {
      src: getImage('plant_list/2.jpg'),
      title: ['Берёза']
    },
    {
      src: getImage('plant_list/3.jpg'),
      title: ['Берёза плакучая']
    },
    {
      src: getImage('plant_list/4.jpg'),
      title: ['Вяз']
    },
    {
      src: getImage('plant_list/5.jpg'),
      title: ['Дуб']
    },
    {
      src: getImage('plant_list/6.jpg'),
      title: ['Ива']
    },
    {
      src: getImage('plant_list/7.jpg'),
      title: ['Каштан']
    },
    {
      src: getImage('plant_list/8.jpg'),
      title: ['Клен глобозум']
    },
    {
      src: getImage('plant_list/9.jpg'),
      title: ['Клен Красный']
    },
    {
      src: getImage('plant_list/10.jpg'),
      title: ['Клен остролистный']
    },
    {
      src: getImage('plant_list/11.jpg'),
      title: ['Клен Серебристый']
    },
    {
      src: getImage('plant_list/12.jpg'),
      title: ['Липа']
    },
    {
      src: getImage('plant_list/13.jpg'),
      title: ['Орех Маньчжурский']
    },
    {
      src: getImage('plant_list/14.jpg'),
      title: ['Осина']
    },
    {
      src: getImage('plant_list/15.jpg'),
      title: ['Рябина']
    },
    {
      src: getImage('plant_list/16.jpg'),
      title: ['Тополь']
    },
    {
      src: getImage('plant_list/17.jpg'),
      title: ['Черемуха']
    },
    {
      src: getImage('plant_list/18.jpg'),
      title: ['Ясень']
    }
  ]),
  fruit: Object.freeze([
    {
      src: getImage('plant_plod/0.jpg'),
      title: ['Яблоня']
    },
    {
      src: getImage('plant_plod/1.jpg'),
      title: ['Вишня']
    },
    {
      src: getImage('plant_plod/2.jpg'),
      title: ['Груша']
    },
    {
      src: getImage('plant_plod/3.jpg'),
      title: ['Слива']
    },
    {
      src: getImage('plant_plod/4.jpg'),
      title: ['Черешня']
    }
  ]),
  pine: Object.freeze([
    {
      src: getImage('plant_pine/1.jpg'),
      title: ['Ель голубая']
    },
    {
      src: getImage('plant_pine/2.jpg'),
      title: ['Сосна чёрная']
    },
    {
      src: getImage('plant_pine/3.jpg'),
      title: ['Ель сербская']
    },
    {
      src: getImage('plant_pine/4.jpg'),
      title: ['Пихта Кореана']
    },
    {
      src: getImage('plant_pine/5.jpg'),
      title: ['Пихта Конколор']
    },
    {
      src: getImage('plant_pine/6.jpg'),
      title: ['Ель Коника']
    },
    {
      src: getImage('plant_pine/7.jpg'),
      title: ['Лиственница Сибирская']
    },
    {
      src: getImage('plant_pine/8.jpg'),
      title: ['Лиственница плакучая']
    },
    {
      src: getImage('plant_pine/9.jpg'),
      title: ['Кедр']
    },
    {
      src: getImage('plant_pine/10.jpg'),
      title: ['Псевдотсуга']
    },
    {
      src: getImage('plant_pine/11.jpg'),
      title: ['Тис']
    },
    {
      src: getImage('plant_pine/12.jpg'),
      title: ['Тсуга']
    },
    {
      src: getImage('plant_pine/13.jpg'),
      title: ['Ель Нидиформис']
    },
    {
      src: getImage('plant_pine/14.jpg'),
      title: ['Бонсай']
    }
  ])
}
