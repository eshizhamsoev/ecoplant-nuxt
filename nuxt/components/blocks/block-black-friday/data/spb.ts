import { getProductImageByNumber } from './helpers'
import { collections } from './product-plant'

export const groups = [
  {
    firstProduct: {
      images: [
        getProductImageByNumber('01-smaragd', 1),
        getProductImageByNumber('01-smaragd', 2),
        getProductImageByNumber('01-smaragd', 3),
        getProductImageByNumber('01-smaragd', 4)
      ],
      info: {
        name: 'Туя Смарагд',
        size: '40\u00A0‑\u00A060\u00A0см.',
        price: {
          new: 199,
          old: 699
        }
      },
      variants: [],
      sizes: [
        {
          name: '40\u00A0‑\u00A060\u00A0см.',
          price: {
            new: 199,
            old: 699
          }
        },
        {
          name: '60\u00A0‑\u00A080\u00A0см.',
          price: {
            new: 399,
            old: 897
          }
        },
        {
          name: '80\u00A0‑\u00A0100\u00A0см.',
          price: {
            new: 599,
            old: 1450
          }
        },
        {
          name: '100\u00A0‑\u00A0120\u00A0см.',
          price: {
            new: 1497,
            old: 2970
          }
        },
        {
          name: '120\u00A0‑\u00A0140\u00A0см.',
          price: {
            new: 1790,
            old: 3599
          }
        },
        {
          name: '140\u00A0‑\u00A0160\u00A0см.',
          price: {
            new: 2799,
            old: 5598
          }
        },
        {
          name: '160\u00A0‑\u00A0180\u00A0см.',
          price: {
            new: 2970,
            old: 5940
          }
        },
        {
          name: '220\u00A0‑\u00A0240\u00A0см.',
          price: {
            new: 8700,
            old: 14700
          }
        },
        {
          name: '240\u00A0‑\u00A0260\u00A0см.',
          price: {
            new: 9970,
            old: 19500
          }
        },
        {
          name: '260\u00A0‑\u00A0280\u00A0см.',
          price: {
            new: 10750,
            old: 25800
          }
        },
        {
          name: '280\u00A0‑\u00A0300\u00A0см.',
          price: {
            new: 12970,
            old: 29500
          }
        }
      ]
    },
    secondProduct: {
      images: [
        getProductImageByNumber('02-brabant', 1),
        getProductImageByNumber('02-brabant', 2),
        getProductImageByNumber('02-brabant', 3),
        getProductImageByNumber('02-brabant', 4),
        getProductImageByNumber('02-brabant', 5)
      ],
      info: {
        name: 'Туя Брабант',
        size: '200\u00A0‑\u00A0250\u00A0см.',
        price: {
          new: 2570,
          old: 5170
        }
      },
      variants: [],
      sizes: [
        {
          name: '80\u00A0‑\u00A0100\u00A0см.',
          price: {
            new: 499,
            old: 970
          }
        },
        {
          name: '100\u00A0‑\u00A0120\u00A0см.',
          price: {
            new: 1497,
            old: 2994
          }
        },
        {
          name: '120\u00A0‑\u00A0140\u00A0см.',
          price: {
            new: 1799,
            old: 3580
          }
        },
        {
          name: '180\u00A0‑\u00A0200\u00A0см.',
          price: {
            new: 2170,
            old: 4340
          }
        },
        {
          name: '200\u00A0‑\u00A0250\u00A0см.',
          price: {
            new: 2570,
            old: 5170
          }
        }
      ]
    }
  },
  {
    firstProduct: {
      images: [
        getProductImageByNumber('08-mojevel', 1),
        getProductImageByNumber('08-mojevel', 2),
        getProductImageByNumber('08-mojevel', 3),
        getProductImageByNumber('08-mojevel', 4),
        getProductImageByNumber('08-mojevel', 5)
      ],
      info: {
        name: 'Можжевельники (Все сорта)',
        size: '0,50\u00A0‑\u00A00,75\u00A0м.',
        price: {
          new: 399,
          old: 997
        }
      },
      variants: [],
      sizes: [
        {
          name: '0,50\u00A0‑\u00A00,75\u00A0м.',
          price: {
            new: 399,
            old: 997
          }
        },
        {
          name: '1,00\u00A0‑\u00A01,20\u00A0м.',
          price: {
            new: 1970,
            old: 3500
          }
        },
        {
          name: '2,20\u00A0‑\u00A02,60\u00A0м.',
          price: {
            new: 4970,
            old: 45700
          }
        }
      ]
    },
    secondProduct: {
      images: [
        getProductImageByNumber('15-tui-shar', 1),
        getProductImageByNumber('15-tui-shar', 2),
        getProductImageByNumber('15-tui-shar', 3),
        getProductImageByNumber('15-tui-shar', 4),
        getProductImageByNumber('15-tui-shar', 5)
      ],
      info: {
        name: 'ШАРОВИДНЫЕ ТУИ',
        size: '0,20\u00A0‑\u00A00,40\u00A0м.',
        price: {
          new: 299,
          old: 997
        }
      },
      variants: [],
      sizes: [
        {
          name: '0,20\u00A0‑\u00A00,40\u00A0м.',
          price: {
            new: 299,
            old: 997
          }
        },
        {
          name: '0,60\u00A0‑\u00A00,80\u00A0м.',
          price: {
            new: 1750,
            old: 3500
          }
        },
        {
          name: '0,80\u00A0‑\u00A01,00\u00A0м.',
          price: {
            new: 2997,
            old: 7500
          }
        },
        {
          name: '1,00\u00A0‑\u00A01,50\u00A0м.',
          price: {
            new: 2997,
            old: 9900
          }
        }
      ]
    }
  },
  {
    firstProduct: {
      images: [
        getProductImageByNumber('04-el', 1),
        getProductImageByNumber('04-el', 2),
        getProductImageByNumber('04-el', 3),
        getProductImageByNumber('04-el', 4)
      ],
      info: {
        name: 'Ель обыкновенная',
        size: '3,00\u00A0‑\u00A04,00\u00A0м.',
        price: {
          new: 5970,
          old: 17910
        }
      },
      variants: [],
      sizes: [
        {
          name: '1,00\u00A0‑\u00A02,00\u00A0м.',
          price: {
            new: 1970,
            old: 5910
          }
        },
        {
          name: '2,00\u00A0‑\u00A03,00\u00A0м.',
          price: {
            new: 2697,
            old: 8091
          }
        },
        {
          name: '3,00\u00A0‑\u00A04,00\u00A0м.',
          price: {
            new: 5970,
            old: 17910
          }
        },
        {
          name: '4,00\u00A0‑\u00A05,00\u00A0м.',
          price: {
            new: 9970,
            old: 29910
          }
        },
        {
          name: '5,00\u00A0‑\u00A06,00\u00A0м.',
          price: {
            new: 15970,
            old: 47910
          }
        },
        {
          name: '7,00\u00A0‑\u00A08,00\u00A0м.',
          price: {
            new: 22970,
            old: 68910
          }
        }
      ]
    },
    secondProduct: {
      images: [
        getProductImageByNumber('03-sosna', 1),
        getProductImageByNumber('03-sosna', 2),
        getProductImageByNumber('03-sosna', 3),
        getProductImageByNumber('03-sosna', 4),
        getProductImageByNumber('03-sosna', 5)
      ],
      info: {
        name: 'Сосна обыкновенная',
        size: '3,00\u00A0‑\u00A04,00\u00A0м.',
        price: {
          new: 5970,
          old: 17910
        }
      },
      variants: [],
      sizes: [
        {
          name: '1,00\u00A0‑\u00A02,00\u00A0м.',
          price: {
            new: 2397,
            old: 7191
          }
        },
        {
          name: '2,00\u00A0‑\u00A03,00\u00A0м.',
          price: {
            new: 3570,
            old: 10710
          }
        },
        {
          name: '3,00\u00A0‑\u00A04,00\u00A0м.',
          price: {
            new: 5970,
            old: 17910
          }
        },
        {
          name: '4,00\u00A0‑\u00A05,00\u00A0м.',
          price: {
            new: 9970,
            old: 29910
          }
        }
      ]
    }
  },
  {
    firstProduct: {
      images: [
        getProductImageByNumber('05-el-blue', 1),
        getProductImageByNumber('05-el-blue', 2),
        getProductImageByNumber('05-el-blue', 3),
        getProductImageByNumber('05-el-blue', 4),
        getProductImageByNumber('05-el-blue', 5)
      ],
      info: {
        name: 'Ель Голубая',
        size: '1,00\u00A0‑\u00A01,50\u00A0м.',
        price: {
          new: 2570,
          old: 3990
        }
      },
      variants: [],
      sizes: [
        {
          name: '1,00\u00A0‑\u00A01,50\u00A0м.',
          price: {
            new: 2570,
            old: 3990
          }
        },
        {
          name: '1,50\u00A0‑\u00A02,00\u00A0м.',
          price: {
            new: 4750,
            old: 9900
          }
        },
        {
          name: '2,00\u00A0‑\u00A02,50\u00A0м.',
          price: {
            new: 15970,
            old: 28700
          }
        },
        {
          name: '2,50\u00A0‑\u00A03,00\u00A0м.',
          price: {
            new: 17500,
            old: 33550
          }
        },
        {
          name: '3,00\u00A0‑\u00A03,50\u00A0м.',
          price: {
            new: 19970,
            old: 45700
          }
        },
        {
          name: '3,50\u00A0‑\u00A04,00\u00A0м.',
          price: {
            new: 25970,
            old: 49800
          }
        },
        {
          name: '4,00\u00A0‑\u00A04,50\u00A0м.',
          price: {
            new: 27500,
            old: 52900
          }
        }
      ]
    },
    secondProduct: {
      images: [
        getProductImageByNumber('16-coreana', 1),
        getProductImageByNumber('16-coreana', 2),
        getProductImageByNumber('16-coreana', 3),
        getProductImageByNumber('16-coreana', 4),
        getProductImageByNumber('16-coreana', 5)
      ],
      info: {
        name: 'Пихта Кореана',
        size: '120\u00A0‑\u00A0140\u00A0cм.',
        price: {
          new: 2970,
          old: 4990
        }
      },
      variants: [],
      sizes: [
        {
          name: '120\u00A0‑\u00A0140\u00A0cм.',
          price: {
            new: 2970,
            old: 4990
          }
        },
        {
          name: '140\u00A0‑\u00A0160\u00A0cм.',
          price: {
            new: 3970,
            old: 9500
          }
        },
        {
          name: '160\u00A0‑\u00A0180\u00A0cм.',
          price: {
            new: 4970,
            old: 10800
          }
        },
        {
          name: '180\u00A0‑\u00A0200\u00A0cм.',
          price: {
            new: 5970,
            old: 13990
          }
        },
        {
          name: '200\u00A0‑\u00A0220\u00A0cм.',
          price: {
            new: 6970,
            old: 15970
          }
        }
      ]
    }
  },
  {
    title: 'КУСТАРНИКИ И ЦВЕТУЩИЕ',
    firstProduct: {
      images: [
        getProductImageByNumber('11-kust', 1),
        getProductImageByNumber('11-kust', 2),
        getProductImageByNumber('11-kust', 3),
        getProductImageByNumber('11-kust', 4),
        getProductImageByNumber('11-kust', 5)
      ],
      info: {
        name: 'Кустарники',
        size: '0,5\u00A0‑\u00A01,0\u00A0м.',
        price: {
          new: 199,
          old: 970
        }
      },
      variants: collections.bush,
      variantsButton: 'Посмотреть всю коллекцию кустарников',
      sizes: [],
      isSameSize: true
    },
    secondProduct: {
      images: [
        getProductImageByNumber('12-kust-cvet', 1),
        getProductImageByNumber('12-kust-cvet', 2),
        getProductImageByNumber('12-kust-cvet', 3),
        getProductImageByNumber('12-kust-cvet', 4),
        getProductImageByNumber('12-kust-cvet', 5),
        getProductImageByNumber('12-kust-cvet', 6),
        getProductImageByNumber('12-kust-cvet', 7),
        getProductImageByNumber('12-kust-cvet', 8),
        getProductImageByNumber('12-kust-cvet', 9)
      ],
      info: {
        name: 'Цветущие кустарники',
        size: '0,5\u00A0‑\u00A01,0\u00A0м.',
        price: {
          new: 199,
          old: 970
        }
      },
      variants: collections.flower,
      variantsButton: 'Посмотреть всю коллекцию цветущих',
      sizes: [],
      isSameSize: true
    }
  },
  {
    title: 'ЛИСТВЕННЫЕ И ПЛОДОВЫЕ ДЕРЕВЬЯ',
    firstProduct: {
      images: [
        getProductImageByNumber('13-list', 1),
        getProductImageByNumber('13-list', 2),
        getProductImageByNumber('13-list', 3),
        getProductImageByNumber('13-list', 4),
        getProductImageByNumber('13-list', 5),
        getProductImageByNumber('13-list', 6),
        getProductImageByNumber('13-list', 7)
      ],
      info: {
        name: 'Лиственные',
        size: '2.00\u00A0‑\u00A03.00\u00A0м.',
        price: {
          new: 2970,
          old: 5970
        }
      },
      sizes: [
        {
          name: '2.00\u00A0‑\u00A03.00\u00A0м.',
          price: {
            new: 2970,
            old: 5970
          }
        },
        {
          name: '3.00\u00A0‑\u00A04.00\u00A0м.',
          price: {
            new: 5970,
            old: 9650
          }
        },
        {
          name: '4.00\u00A0‑\u00A05.00\u00A0м.',
          price: {
            new: 9970,
            old: 19000
          }
        }
      ],
      variants: collections.leaf,
      variantsButton: 'Посмотреть всю коллекцию лиственных'
    },
    secondProduct: {
      images: [
        getProductImageByNumber('14-plod', 1),
        getProductImageByNumber('14-plod', 2),
        getProductImageByNumber('14-plod', 3),
        getProductImageByNumber('14-plod', 4),
        getProductImageByNumber('14-plod', 5)
      ],
      info: {
        name: 'Плодовые',
        size: '2,00\u00A0‑\u00A03,00\u00A0м.',
        price: {
          new: 470,
          old: 970
        }
      },
      sizes: [
        {
          name: '0,50\u00A0‑\u00A01,00\u00A0м.',
          price: {
            new: 199,
            old: 970
          }
        },
        {
          name: '2.00\u00A0‑\u00A03.00\u00A0м.',
          price: {
            new: 470,
            old: 970
          }
        },
        {
          name: '3.00\u00A0‑\u00A04.00\u00A0м. (крупномер)',
          price: {
            new: 9970,
            old: 19970
          }
        },
        {
          name: '4.00\u00A0‑\u00A05.00\u00A0м. (крупномер)',
          price: {
            new: 9970,
            old: 25700
          }
        }
      ],
      variants: collections.fruit,
      variantsButton: 'Посмотреть всю коллекцию плодовых'
    }
  },
  {
    firstProduct: {
      images: [
        getProductImageByNumber('21-el-serb', 1),
        getProductImageByNumber('21-el-serb', 2),
        getProductImageByNumber('21-el-serb', 3),
        getProductImageByNumber('21-el-serb', 4),
        getProductImageByNumber('21-el-serb', 5)
      ],
      variants: [],
      info: {
        name: 'Ель Сербская',
        size: '120\u00A0‑\u00A0140\u00A0cм.',
        price: {
          new: 2970,
          old: 4990
        }
      },
      sizes: [
        {
          name: '120\u00A0‑\u00A0140\u00A0cм.',
          price: {
            new: 2970,
            old: 4990
          }
        },
        {
          name: '140\u00A0‑\u00A0160\u00A0cм.',
          price: {
            new: 3570,
            old: 8700
          }
        },
        {
          name: '160\u00A0‑\u00A0180\u00A0cм.',
          price: {
            new: 3970,
            old: 9950
          }
        },
        {
          name: '180\u00A0‑\u00A0200\u00A0cм.',
          price: {
            new: 4970,
            old: 11700
          }
        },
        {
          name: '200\u00A0‑\u00A0220\u00A0cм.',
          price: {
            new: 5570,
            old: 13500
          }
        }
      ]
    },
    secondProduct: {
      images: [
        getProductImageByNumber('20-kedr', 1),
        getProductImageByNumber('20-kedr', 2),
        getProductImageByNumber('20-kedr', 3),
        getProductImageByNumber('20-kedr', 4),
        getProductImageByNumber('20-kedr', 5)
      ],
      info: {
        name: 'Кедр',
        size: '2.00\u00A0‑\u00A02.50\u00A0м.',
        price: {
          new: 11970,
          old: 29500
        }
      },
      variants: [],
      sizes: [
        {
          name: '1.50\u00A0‑\u00A02.00\u00A0м.',
          price: {
            new: 8970,
            old: 16500
          }
        },
        {
          name: '2.00\u00A0‑\u00A02.50\u00A0м.',
          price: {
            new: 11970,
            old: 29500
          }
        },
        {
          name: '2.50\u00A0‑\u00A03.00\u00A0м.',
          price: {
            new: 17970,
            old: 38700
          }
        },
        {
          name: '3.00\u00A0‑\u00A03.50\u00A0м.',
          price: {
            new: 19970,
            old: 47900
          }
        },
        {
          name: '3.50\u00A0‑\u00A04.00\u00A0м.',
          price: {
            new: 21970,
            old: 55800
          }
        },
        {
          name: '4.00\u00A0‑\u00A04.50\u00A0м.',
          price: {
            new: 23970,
            old: 65700
          }
        }
      ]
    }
  }
]
