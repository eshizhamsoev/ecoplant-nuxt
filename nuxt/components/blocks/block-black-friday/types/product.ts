import { BlackFridayPlant } from './plant'

export type BlackFridayProductImage = {
  small: string
  large: string
}

export type BlackFridayProductPrice = {
  old: number
  new: number
}

export type BlackFridayProductInfo = {
  name: string
  size: string
  price: BlackFridayProductPrice
  percent?: number
  isSameSize?: boolean
}

export type BlackFridayProductSize = {
  name: string
  price: BlackFridayProductPrice
  saleLabel?: boolean
}

export type BlackFridayProduct = {
  images: readonly BlackFridayProductImage[]
  info: BlackFridayProductInfo
  sizes: readonly BlackFridayProductSize[]
  variants: readonly BlackFridayPlant[]
  variantsButton?: string
}

export type BlackFridayProductGroup = {
  title?: string
  firstProduct: BlackFridayProduct
  secondProduct?: BlackFridayProduct
}
