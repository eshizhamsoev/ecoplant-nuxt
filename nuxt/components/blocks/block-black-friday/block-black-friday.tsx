import { defineComponent } from '@nuxtjs/composition-api'
import { plantImages, getProductGroups } from './data'
import { BlackFridayPlants } from './components/black-friday-plants'
import { BlackFridayProductGroup } from './components/black-friday-product-group/black-friday-product-group'
import { BlackFridayHeading } from './components/black-friday-heading/black-friday-heading'
import { BlackFridayVideo } from './components/black-friday-video/black-friday-video'
import { BlackFridaySale } from './components/black-friday-sale'
import { injectStyles } from '~/support/utils/injectStyle'
import { useClassNames } from '~/support/utils/bem-classnames'
import style from './block-black-friday.scss?module'

const cn = useClassNames('block-black-friday', style)

export const BlockBlackFriday = injectStyles(
  style,
  defineComponent({
    name: 'BlockBlackFriday',
    props: {},
    setup: (props, { root }) => {
      const groups = getProductGroups(root)
      return () => (
        <div class={cn()}>
          <BlackFridayHeading />
          <div class={cn('bg')} id="black-friday-content">
            <div class={cn('wrapper')}>
              <BlackFridayProductGroup
                showTitleOnMobile={false}
                class={cn('product-group')}
                group={groups[0]}
              />
              <BlackFridayProductGroup
                showTitleOnMobile={false}
                class={cn('product-group')}
                group={groups[1]}
              />
              <BlackFridayProductGroup
                showTitleOnMobile={false}
                class={cn('product-group')}
                group={groups[2]}
              />
              <BlackFridayVideo class={cn('video')} />
            </div>
            <BlackFridaySale class={cn('sale')} />
            <div class={cn('wrapper')}>
              <BlackFridayProductGroup
                showTitleOnMobile={false}
                class={cn('product-group')}
                group={groups[3]}
              />
              <BlackFridayProductGroup
                class={cn('product-group')}
                group={groups[4]}
              />
            </div>
            <BlackFridaySale class={cn('sale')} />
            <div class={cn('wrapper')}>
              <BlackFridayProductGroup
                showTitleOnMobile={false}
                class={cn('product-group')}
                group={groups[5]}
              />
              <BlackFridayProductGroup
                showTitleOnMobile={false}
                class={cn('product-group')}
                group={groups[6]}
              />
              {groups[7] ? (
                <BlackFridayProductGroup
                  showTitleOnMobile={false}
                  class={cn('product-group')}
                  group={groups[7]}
                />
              ) : (
                ''
              )}
            </div>
            <BlackFridaySale class={cn('sale')} />
          </div>
          <div class={cn('wrapper')}>
            <BlackFridayPlants images={plantImages} />
          </div>
        </div>
      )
    }
  })
)
