import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseRubles, BaseLink } from '~/components/base'
import style from './block-featured-product-item.scss?module'

const cn = useClassNames('block-featured-product-item', style)

function extractAdditional(value: string): {
  main: string
  additional: string | null
} {
  const main = value.replace(/\(.*?\)/g, '')
  const additionalMatch = value.match(/\((.*?)\)/)
  return {
    main,
    additional: additionalMatch ? additionalMatch[0] : null
  }
}

export const BlockFeaturedProductItem = injectStyles(
  style,
  defineComponent({
    name: 'BlockFeaturedProductItem',
    props: {
      item: {
        type: Object,
        required: true
      }
    },
    setup: (props) => {
      const name = computed(() => extractAdditional(props.item.name || ''))
      const size = computed(() =>
        extractAdditional(props.item.salePrice.size || '')
      )
      return () => (
        <BaseLink
          to={`/category-products/${props.item.id}#category-products`}
          class={cn()}
        >
          <img
            alt={props.item.name}
            class={cn('image')}
            src={props.item.image}
          />
          <div class={cn('product-properties')}>
            <div class={cn('name')}>
              {name.value.main}
              {name.value.additional && (
                <span class={cn('name-additional')}>
                  {name.value.additional}
                </span>
              )}
            </div>
            <div class={cn('height')}>
              {size.value.main}
              {size.value.additional && (
                <span class={cn('height-additional')}>
                  {size.value.additional}
                </span>
              )}
            </div>
          </div>
          {props.item.salePrice.price && (
            <div class={cn('prices')}>
              <BaseRubles
                class={cn('old-price')}
                price={props.item.salePrice.oldPrice}
                postfix={'₽'}
              />
              <BaseRubles
                class={cn('new-price')}
                price={props.item.salePrice.price}
                postfix={'₽'}
              />
            </div>
          )}
        </BaseLink>
      )
    }
  })
)
