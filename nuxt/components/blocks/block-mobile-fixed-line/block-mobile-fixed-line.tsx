import { defineComponent } from '@nuxtjs/composition-api'
import { BaseBottomFixesLine } from '~/components/base/base-bottom-fixes-line/base-bottom-fixes-line'
import { MobileButtonLine } from '~/components/layouts/mobile/mobile-button-line'

export const BlockMobileFixedLine = defineComponent({
  name: 'BlockMobileFixedLine',
  props: {
    withCart: {
      type: Boolean,
      default: false
    }
  },
  setup: (props) => {
    return () => (
      <BaseBottomFixesLine>
        <MobileButtonLine withCart={props.withCart} />
      </BaseBottomFixesLine>
    )
  }
})
