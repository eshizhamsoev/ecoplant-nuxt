import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseImageGrid } from '~/components/base/base-image-grid'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { villages } from '~/graphql/queries'
import style from './block-villages.scss?module'

const cn = useClassNames('block-villages', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  items: cn('items')
}

export const BlockVillages = injectStyles(
  style,
  defineComponent({
    name: 'BlockVillages',
    props: {},
    setup: () => {
      const { result } = useGqlQuery(villages, {})
      const images = computed(
        () =>
          result.value?.villages.map((item) => {
            return {
              large: item.image.large,
              thumb: item.image.thumb,
              title: item.title
            }
          }) || []
      )
      return () =>
        result.value && (
          <div class={classes.main}>
            <div class={classes.heading}>Наши уже растут</div>
            <BaseImageGrid
              class={classes.items}
              images={images.value}
              md={6}
              popupAvailable={true}
            />
          </div>
        )
    }
  })
)
