import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './awards-title.scss?module'

const cn = useClassNames('awards-title', style)

const classes = {
  main: cn(),
  inner: cn('inner')
}

export const AwardsTitle = injectStyles(
  style,
  defineComponent({
    name: 'AwardsTitle',
    setup: () => {
      return () => (
        <div class={classes.main}>
          <span class={classes.inner}>Награды</span>
        </div>
      )
    }
  })
)
