import { computed, defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseImageGrid } from '~/components/base/base-image-grid'
import { BaseButton } from '~/components/base/base-button'
import { AwardsTitle } from '~/components/blocks/block-awards/common/awards-title/awards-title'
import { Visual } from '~/components/theme/theme-button'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { awards } from '~/graphql/queries'
import style from './mobile-block-awards.scss?module'

const cn = useClassNames('mobile-block-awards', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  innerHeading: cn('inner-heading'),
  items: cn('items'),
  buttonWrapper: cn('button-wrapper')
}

export const MobileBlockAwards = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockAwards',
    props: {},
    setup: () => {
      const { result } = useGqlQuery(awards, {})
      const shownAll = ref(false)
      const images = computed(() => {
        if (!result.value) {
          return []
        }
        return result.value.awards.map(({ image }) => image)
      })
      const shownImages = computed(() => {
        return shownAll.value ? images.value : images.value.slice(0, 6)
      })

      const showAll = () => {
        shownAll.value = true
      }
      return () =>
        result.value && (
          <div class={classes.main}>
            <AwardsTitle class={classes.heading} />
            <BaseImageGrid
              class={classes.items}
              images={shownImages.value}
              xs={3}
              popupAvailable={true}
            />
            {!shownAll.value && (
              <div class={classes.buttonWrapper}>
                <BaseButton size="m" visual={Visual.outline} onClick={showAll}>
                  показать еще
                </BaseButton>
              </div>
            )}
          </div>
        )
    }
  })
)
