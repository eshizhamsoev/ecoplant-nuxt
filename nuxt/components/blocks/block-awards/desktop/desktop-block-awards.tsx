import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { AwardsTitle } from '~/components/blocks/block-awards/common/awards-title/awards-title'
import { BaseImageGrid } from '~/components/base/base-image-grid'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { awards } from '~/graphql/queries'
import style from './desktop-block-awards.scss?module'

const cn = useClassNames('desktop-block-awards', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  items: cn('items')
}

export const DesktopBlockAwards = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlockAwards',
    props: {},
    setup: () => {
      const { result } = useGqlQuery(awards, {})
      const images = computed(
        () => result.value?.awards.map((item) => item.image) || []
      )
      return () =>
        result.value && (
          <div class={classes.main}>
            <AwardsTitle class={classes.heading} />
            <BaseImageGrid
              class={classes.items}
              images={images.value}
              md={7}
              popupAvailable={true}
            />
          </div>
        )
    }
  })
)
