import { defineComponent } from '@nuxtjs/composition-api'

export const BlockContent = defineComponent({
  name: 'BlockContent',
  props: {},
  setup: () => {
    return () => (
      <div>
        <nuxt-child />
      </div>
    )
  }
})
