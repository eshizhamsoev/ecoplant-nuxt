import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const BlockFooterOneLine = UtilUniversalComponentFactory(
  'BlockFooterOneLine',
  () => import('./mobile').then((c) => c.MobileBlockFooterOneLine),
  () => import('./desktop').then((c) => c.DesktopBlockFooterOneLine)
)
