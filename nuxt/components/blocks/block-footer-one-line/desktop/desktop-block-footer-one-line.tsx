import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { FooterBottom } from '~/components/blocks/block-footer/footer-bottom'
import style from './desktop-block-footer-one-line.scss?module'

const cn = useClassNames('desktop-block-footer-one-line', style)

const classes = {
  main: cn()
}

export const DesktopBlockFooterOneLine = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlockFooterOneLine',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <FooterBottom withSocials={false} />
        </div>
      )
    }
  })
)
