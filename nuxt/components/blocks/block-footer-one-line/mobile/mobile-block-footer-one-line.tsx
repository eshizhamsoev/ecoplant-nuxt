import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobileFooterBottom } from '~/components/blocks/shared-components/footer-bottom/mobile'
import style from './mobile-block-footer-one-line.scss?module'

const cn = useClassNames('mobile-block-footer-one-line', style)

const classes = {
  main: cn()
}

export const MobileBlockFooterOneLine = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockFooterOneLine',
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <MobileFooterBottom withSocials={false} />
        </div>
      )
    }
  })
)
