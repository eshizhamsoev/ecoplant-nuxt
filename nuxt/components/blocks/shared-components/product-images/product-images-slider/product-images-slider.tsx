import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { BaseIcon } from '~/components/base/base-icon'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseSliderWithCutButtons } from '~/components/base/base-slider-with-cut-buttons'
import style from './product-images-slider.scss?module'

const cn = useClassNames('product-images-slider', style)

export const ProductImagesSlider = injectStyles(
  style,
  defineComponent({
    components: { BaseIcon },
    props: {
      images: {
        type: Array as () => readonly string[],
        required: true
      },
      currentIndex: {
        type: Number,
        required: true
      }
    },
    setup: (props, { emit }) => {
      return () => (
        <BaseSliderWithCutButtons
          class={cn()}
          currentIndex={props.currentIndex}
          onChange={(index: number) => emit('change', index)}
          onClick={(index: number) => emit('click', index)}
        >
          {props.images.map((image, index) => (
            <div>
              {index === 0 ? (
                <img
                  src={image}
                  alt=""
                  width={378}
                  height={504}
                  class={[cn('item')]}
                  loading="lazy"
                />
              ) : (
                <img
                  data-src={image}
                  alt=""
                  class={['tns-lazy-img', cn('item')]}
                  width={378}
                  height={504}
                />
              )}
            </div>
          ))}
        </BaseSliderWithCutButtons>
      )
    }
  })
)
