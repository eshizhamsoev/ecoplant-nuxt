import { computed, defineComponent, ref } from '@nuxtjs/composition-api'
import { ProductImagesGallery } from './product-images-gallery'
import { ProductImagesSlider } from './product-images-slider'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePopupGallery } from '~/components/base/base-popup-gallery'
import { usePopupGallery } from '~/components/base/base-popup-gallery/usePopupGallery'
import { ProductImage } from '~/_generated/types'
import style from './product-images.scss?module'

const cn = useClassNames('product-images', style)
const classes = {
  main: cn(),
  slider: cn('slider'),
  gallery: cn('gallery')
}

export const ProductImages = injectStyles(
  style,
  defineComponent({
    name: 'ProductImages',
    components: {
      ProductImagesGallery,
      ProductImagesSlider
    },
    props: {
      images: {
        type: Array as () => ProductImage[],
        required: true
      }
    },
    setup: (props) => {
      const index = ref(0)
      const sliderImages = computed(() =>
        props.images.map((image) => image.small)
      )
      const galleryImages = computed(() =>
        props.images.slice(0, 3).map((image) => image.small)
      )
      const updateIndex = (newIndex: number) => {
        index.value = newIndex
      }
      const {
        open: openPopupGallery,
        close: closePopupGallery,
        index: popupIndex
      } = usePopupGallery()
      return () => (
        <div class={classes.main}>
          <div>
            <div class={cn('image-wrapper')}>
              <ProductImagesSlider
                class={classes.slider}
                images={sliderImages.value}
                onChange={updateIndex}
                onClick={openPopupGallery}
                currentIndex={index.value}
              />
            </div>
          </div>
          <ProductImagesGallery
            class={classes.gallery}
            images={galleryImages.value}
            onChange={updateIndex}
          />
          <BasePopupGallery
            images={props.images}
            index={popupIndex.value}
            showThumbnails={true}
            staticThumbnails={true}
            onClose={closePopupGallery}
          />
        </div>
      )
    }
  })
)
