import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './product-images-gallery.scss?module'

const cn = useClassNames('product-images-gallery', style)
const classes = {
  main: cn(),
  button: cn('button'),
  image: cn('image')
}

export const ProductImagesGallery = injectStyles(
  style,
  defineComponent({
    name: 'ProductImagesGallery',
    props: {
      images: {
        type: Array as () => readonly string[],
        required: true
      }
    },
    setup: (props, { emit }) => {
      return () => (
        <div class={classes.main}>
          {props.images.map((image, index) => (
            <button
              key={image}
              class={classes.button}
              onClick={() => emit('change', index)}
            >
              <img src={image} alt="" class={classes.image} loading="lazy" />
            </button>
          ))}
        </div>
      )
    }
  })
)
