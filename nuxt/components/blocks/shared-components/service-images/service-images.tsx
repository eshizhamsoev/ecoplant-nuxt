import {
  computed,
  defineComponent,
  PropType,
  ref
} from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePopupGallery, BaseSliderWithCutButtons } from '~/components/base'
import { usePopupGallery } from '~/components/base/base-popup-gallery/usePopupGallery'
import { preloadImage } from '~/support/utils/preloadImage'
import style from './service-images.scss?module'

const cn = useClassNames('service-images', style)

type Image = {
  original: string
  large: string
  small: string
}

export enum Orientation {
  VERTICAL = 'vertical',
  HORIZONTAL = 'horizontal'
}

export const ServiceImages = injectStyles(
  style,
  defineComponent({
    name: 'ServiceImages',
    props: {
      images: {
        type: Array as () => Image[],
        required: true
      },
      orientation: {
        type: String as PropType<Orientation>,
        required: true
      }
    },
    setup: (props) => {
      const index = ref(0)
      const galleryImages = computed(() =>
        props.images.slice(0, 3).map((image) => image.small)
      )

      const popupImages = computed(() =>
        props.images.map(({ original, small }) => ({ small, large: original }))
      )

      const updateIndex = (newIndex: number) => {
        index.value = newIndex
        if (props.images.length > 0) {
          preloadImage(props.images[(newIndex + 1) % props.images.length].large)
          preloadImage(
            props.images[newIndex > 0 ? newIndex - 1 : props.images.length - 1]
              .large
          )
        }
      }
      const {
        open: openPopupGallery,
        close: closePopupGallery,
        index: popupIndex
      } = usePopupGallery()
      return () => (
        <div class={cn({ orientation: props.orientation })}>
          <BaseSliderWithCutButtons
            class={cn('main')}
            currentIndex={index.value}
            onChange={updateIndex}
            onClick={openPopupGallery}
          >
            {props.images.map(({ large }) => (
              <div>
                <img
                  data-src={large}
                  alt=""
                  class={['tns-lazy-img', cn('item')]}
                />
              </div>
            ))}
          </BaseSliderWithCutButtons>
          <div class={cn('gallery')}>
            {galleryImages.value.map((image, index) => (
              <button
                key={image}
                class={cn('gallery-item')}
                onClick={() => updateIndex(index)}
              >
                <img
                  src={image}
                  alt=""
                  class={cn('gallery-image')}
                  loading={'lazy'}
                />
              </button>
            ))}
          </div>
          <BasePopupGallery
            images={popupImages.value}
            index={popupIndex.value}
            showThumbnails={true}
            staticThumbnails={true}
            onClose={closePopupGallery}
          />
        </div>
      )
    }
  })
)
