import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseMobileActionCircleButton } from '~/components/base/base-mobile-action-cirle-button'
import { useOnPhoneClick } from '~/support/hooks/usePhoneClick'
import { Color } from '~/components/theme/theme-button'
import style from './mobile-phone-button.scss?module'

const cn = useClassNames('mobile-phone-button', style)

export const MobilePhoneButton = injectStyles(
  style,
  defineComponent({
    name: 'MobilePhoneButton',
    props: {},
    setup: () => {
      const onPhoneClick = useOnPhoneClick()
      return () => (
        <BaseMobileActionCircleButton
          iconWidth={18}
          iconName="phone"
          color={Color.yellow}
          onClick={onPhoneClick}
          class={cn()}
        >
          <template slot="text">Звонок</template>
        </BaseMobileActionCircleButton>
      )
    }
  })
)
