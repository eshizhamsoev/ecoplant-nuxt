import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseMobileActionCircleButton } from '~/components/base/base-mobile-action-cirle-button'
import { useOnTelegramClick } from '~/support/hooks/useTelegram'
import style from './mobile-telegram-button.scss?module'

const cn = useClassNames('mobile-telegram-button', style)

export const MobileTelegramButton = injectStyles(
  style,
  defineComponent({
    name: 'MobileTelegramButton',
    props: {},
    setup: () => {
      const onTelegramClick = useOnTelegramClick()
      return () => (
        <BaseMobileActionCircleButton
          iconWidth={21}
          iconName="telegram"
          onClick={onTelegramClick}
          class={cn()}
        >
          <template slot="text">Telegram</template>
        </BaseMobileActionCircleButton>
      )
    }
  })
)
