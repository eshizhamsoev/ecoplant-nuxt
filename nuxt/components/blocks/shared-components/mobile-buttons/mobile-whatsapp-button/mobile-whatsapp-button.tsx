import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseMobileActionCircleButton } from '~/components/base/base-mobile-action-cirle-button'
import { useOnWhatsAppClick } from '~/support/hooks/useWhatsApp'
import style from './mobile-whatsapp-button.scss?module'

const cn = useClassNames('mobile-whatsapp-button', style)

export const MobileWhatsappButton = injectStyles(
  style,
  defineComponent({
    name: 'MobileWhatsappButton',
    props: {},
    setup: () => {
      const onWhatsAppClick = useOnWhatsAppClick()
      return () => (
        <BaseMobileActionCircleButton
          iconWidth={21}
          iconName="whatsapp"
          onClick={onWhatsAppClick}
          class={cn()}
        >
          <template slot="text">WhatsApp</template>
        </BaseMobileActionCircleButton>
      )
    }
  })
)
