import { computed, defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePopupGallery } from '~/components/base/base-popup-gallery'
import { usePopupGallery } from '~/components/base/base-popup-gallery/usePopupGallery'
import { BaseSliderWithButtons } from '~/components/base/base-slider-with-buttons'
import style from './mobile-product-images.scss?module'

const cn = useClassNames('mobile-image-slider', style)
const classes = {
  main: cn(),
  slider: cn('slider'),
  image: cn('image')
}

const options = {
  gutter: 0,
  lazyload: true,
  center: true,
  fixedWidth: 257
}

type Image = {
  small: string
  large: string
}

export const MobileImageSlider = injectStyles(
  style,
  defineComponent({
    name: 'MobileImageSlider',
    props: {
      images: {
        type: Array as () => readonly Image[],
        required: true
      }
    },
    setup: (props) => {
      const index = ref(0)
      const updateIndex = (newIndex: number) => {
        index.value = newIndex
      }

      const images = computed(() =>
        props.images.map((image) => ({
          small: image.small,
          large: image.small
        }))
      )

      const {
        open: openPopupGallery,
        close: closePopupGallery,
        index: popupIndex
      } = usePopupGallery()

      return () => (
        <div class={classes.main}>
          <BaseSliderWithButtons
            options={options}
            class={classes.main}
            currentIndex={index.value}
            onChange={updateIndex}
            onClick={openPopupGallery}
          >
            {props.images.map((image, i) =>
              i === 0 ? (
                <div key={i} class="tns-main-slide">
                  <img
                    src={image.small}
                    class={[classes.image]}
                    width={378}
                    height={504}
                  />
                </div>
              ) : (
                <div key={i}>
                  <img
                    data-src={image.small}
                    class={['tns-lazy-img', classes.image]}
                    width={378}
                    height={504}
                  />
                </div>
              )
            )}
          </BaseSliderWithButtons>
          <BasePopupGallery
            images={images.value}
            index={popupIndex.value}
            show-thumbnails={false}
            onClose={closePopupGallery}
          />
        </div>
      )
    }
  })
)
