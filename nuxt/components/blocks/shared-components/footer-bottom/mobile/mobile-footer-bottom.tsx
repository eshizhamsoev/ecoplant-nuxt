import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base/base-icon'
import { FooterSocials } from '~/components/blocks/block-footer/footer-bottom/footer-socials'
import { FooterLinks } from '~/components/blocks/block-footer/footer-bottom/footer-links'
import { FooterLinkList } from '~/components/blocks/block-footer/footer-bottom/footer-link-list/footer-link-list'
import { links } from '~/components/blocks/block-footer/footer-bottom/links'
import style from './mobile-footer-bottom.scss?module'

const cn = useClassNames('mobile-footer-bottom', style)

const classes = {
  logoLine: cn('logo-line'),
  logo: cn('logo'),
  legal: cn('legal'),
  socials: cn('socials'),
  links: cn('links'),
  linkList: cn('link-list')
}

export const MobileFooterBottom = injectStyles(
  style,
  defineComponent({
    name: 'MobileFooterBottom',
    props: {
      theme: {
        type: String,
        default: () => {
          return 'light' as 'light' | 'dark'
        }
      },
      withVacancyButton: {
        type: Boolean,
        default: true
      },
      withSaleFir: {
        type: Boolean,
        default: false
      },
      withSocials: {
        type: Boolean,
        default: true
      }
    },
    setup: (props, { root }) => {
      const legal = computed(() => root.$accessor.contacts.legal)
      return () => (
        <div class={cn({ theme: props.theme })}>
          <div class={classes.logoLine}>
            <BaseIcon name="logo" width={72} class={classes.logo} />
            <div class={classes.legal}>{legal.value}</div>
          </div>
          {props.withSocials && <FooterSocials class={classes.socials} />}
          <FooterLinks class={classes.links} withSaleFir={props.withSaleFir} />
          <FooterLinkList class={classes.linkList} links={links} />
        </div>
      )
    }
  })
)
