import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './mobile-product-name.scss?module'

const cn = useClassNames('mobile-product-name', style)

export const MobileProductName = injectStyles(
  style,
  defineComponent({
    name: 'MobileProductInfo',
    props: {
      title: {
        type: String,
        required: true
      },
      showH1: {
        type: Boolean,
        default: true
      }
    },
    emits: ['showDescriptionClick'],
    setup: (props) => {
      const fontSize = computed(() => {
        const length = Math.max(
          ...props.title.split(' ').map((str) => str.length)
        )
        if (length < 8) {
          return (26 / 375) * 100
        }
        return (((18 / length) * 12) / 375) * 100
      })

      return () => (
        <div class={cn()}>
          {props.showH1 ? (
            <h1 class={cn('name')} style={{ fontSize: fontSize.value + 'vw' }}>
              {props.title}
            </h1>
          ) : (
            <div class={cn('name')} style={{ fontSize: fontSize.value + 'vw' }}>
              {props.title}
            </div>
          )}
        </div>
      )
    }
  })
)
