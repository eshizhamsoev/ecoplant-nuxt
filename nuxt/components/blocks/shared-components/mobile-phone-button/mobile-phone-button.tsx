import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import {
  PhoneLink,
  PhoneSlotData
} from '~/components/action-providers/phone-link'
import { BaseIcon } from '~/components/base'
import { cnThemeButton, Color } from '~/components/theme/theme-button'
import style from './mobile-phone-button.scss?module'

const cn = useClassNames('mobile-phone-button', style)

const classes = {
  main: [cn(), cnThemeButton({ color: Color.yellow })],
  icon: cn('icon')
}

export const MobilePhoneButton = injectStyles(
  style,
  defineComponent({
    name: 'MobilePhoneButton',
    props: {},
    setup: () => {
      const phone = ({ phone: { text } }: PhoneSlotData) => [
        <BaseIcon name="phone" width={20} class={classes.icon} />,
        text
      ]
      return () => (
        <PhoneLink class={classes.main} scopedSlots={{ default: phone }} />
      )
    }
  })
)
