import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { cnThemeButton, Color } from '~/components/theme/theme-button'
import { BaseLink } from '~/components/base/base-link'
import style from './mobile-sale-button.scss?module'

const cn = useClassNames('mobile-sale-button', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('leading')
}

export const MobileSaleButton = injectStyles(
  style,
  defineComponent({
    name: 'MobileSaleButton',
    props: {},
    setup: () => {
      return () => (
        <BaseLink
          to="/sale"
          class={[
            classes.main,
            cnThemeButton({ color: Color.red, shadow: true })
          ]}
        >
          <div class={classes.heading}>Акция</div>
          <div class={classes.leading}>недели</div>
        </BaseLink>
      )
    }
  })
)
