import {
  computed,
  defineComponent,
  PropType,
  ref
} from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton } from '~/components/base'
import { Visual } from '~/components/theme/theme-button'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './product-varieties.scss?module'

const cn = useClassNames('product-varieties', style)

const VARIETIES_AMOUNT = 7

export const ProductVarieties = injectStyles(
  style,
  defineComponent({
    name: 'ProductVarieties',
    props: {
      product: {
        type: Array as PropType<{ id: string; name: string }[]>,
        required: true
      }
    },
    setup: (props) => {
      const productData = computed(() => props.product)
      const showAllButton = computed(
        () => productData.value.length > VARIETIES_AMOUNT
      )
      const showAllVarieties = ref(false)
      const varietiesToShow = computed(() =>
        showAllButton.value && showAllVarieties.value
          ? productData.value
          : productData.value.slice(0, VARIETIES_AMOUNT)
      )
      const modalOpener = useModalOpener()
      const createOpenVarietyModalCallback = (varietyId: string) => () =>
        modalOpener({
          name: 'variety-info',
          varietyId
        })

      const toggle = () => {
        showAllVarieties.value = !showAllVarieties.value
      }

      return () => {
        return (
          productData.value &&
          productData.value.length > 0 && (
            <div class={cn('varieties')}>
              {varietiesToShow.value.map((item) => (
                <BaseButton
                  key={item.id}
                  size="custom"
                  onClick={createOpenVarietyModalCallback(item.id)}
                  class={cn('button')}
                >
                  {item.name}
                </BaseButton>
              ))}
              {showAllButton.value && (
                <BaseButton
                  onClick={toggle}
                  visual={Visual.outline}
                  size="custom"
                  class={cn('button')}
                >
                  {showAllVarieties.value
                    ? 'Скрыть'
                    : `+ ${productData.value.length - VARIETIES_AMOUNT} сортов`}
                </BaseButton>
              )}
            </div>
          )
        )
      }
    }
  })
)
