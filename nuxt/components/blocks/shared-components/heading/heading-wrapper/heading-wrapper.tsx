import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseOverlay } from '~/components/base/base-overlay'
import style from './heading-wrapper.scss?module'

const cn = useClassNames('heading-wrapper', style)

export const HeadingWrapper = injectStyles(
  style,
  defineComponent({
    name: 'HeadingWrapper',
    props: {
      withOverlay: {
        type: Boolean,
        required: true
      },
      background: {
        type: String,
        required: true
      }
    },
    setup: (props, { slots }) => {
      return () => (
        <header class={cn()} style={style.value}>
          <BaseOverlay class={cn('overlay')}>
            <img src={props.background} alt="" />
          </BaseOverlay>
          <div class={cn('inner')}>{slots.default && slots.default()}</div>
        </header>
      )
    }
  })
)
