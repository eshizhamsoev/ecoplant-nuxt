import { defineComponent } from '@nuxtjs/composition-api'
import { HeadingEmail } from '~/components/blocks/shared-components/heading/heading-top-line/desktop/heading-email'
import { HeadingPhone } from '~/components/blocks/shared-components/heading/heading-top-line/desktop/heading-phone'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './heading-contacts.scss?module'

const cn = useClassNames('heading-contacts', style)

const classes = {
  main: cn(),
  item: cn('item')
}

export const DesktopHeadingContacts = injectStyles(
  style,
  defineComponent({
    name: 'DesktopHeadingContacts',
    props: {},
    setup() {
      return () => (
        <div class={classes.main}>
          <HeadingPhone class={classes.item} />
          <HeadingEmail class={classes.item} />
        </div>
      )
    }
  })
)
