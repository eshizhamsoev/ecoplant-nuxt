import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import style from './desktop-heading-button.scss?module'

const cn = useClassNames('desktop-heading-button', style)

const classes = {
  main: cn(),
  icon: cn('icon'),
  text: cn('text')
}

export const DesktopHeadingButton = injectStyles(
  style,
  defineComponent({
    name: 'DesktopHeadingButton',
    props: {
      icon: {
        type: String,
        default: null
      }
    },
    setup: (props, { slots, emit }) => {
      const onClick = (e: Event) => {
        emit('click', e)
      }
      return () => (
        <button class={classes.main} type="button" onClick={onClick}>
          {props.icon && (
            <BaseIcon name={props.icon} width={18} class={classes.icon} />
          )}
          <span class={classes.text}>{slots.default && slots.default()}</span>
        </button>
      )
    }
  })
)
