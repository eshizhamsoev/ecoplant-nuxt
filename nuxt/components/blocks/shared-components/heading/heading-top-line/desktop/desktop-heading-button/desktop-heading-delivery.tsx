import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { BaseLink } from '~/components/base/base-link'
import style from './desktop-heading-button.scss?module'

const cn = useClassNames('desktop-heading-button', style)

const classes = {
  main: cn(),
  icon: cn('icon'),
  text: cn('text')
}

export const DesktopHeadingDelivery = injectStyles(
  style,
  defineComponent({
    name: 'DesktopHeadingDelivery',
    props: {
      icon: {
        type: String,
        required: true
      }
    },
    setup: (props, { slots, root }) => {
      return () => (
        <BaseLink
          class={classes.main}
          to={{
            path: root.$route.path,
            query: { tabs: 'BlockShipping' },
            hash: 'tabs-BlockShipping'
          }}
        >
          <BaseIcon
            name={props.icon}
            width={32}
            height={27}
            class={classes.icon}
          />
          <span class={classes.text}>{slots.default && slots.default()}</span>
        </BaseLink>
      )
    }
  })
)
