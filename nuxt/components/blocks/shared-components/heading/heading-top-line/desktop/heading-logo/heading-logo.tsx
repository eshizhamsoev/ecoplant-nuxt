import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { BaseIcon } from '~/components/base/base-icon'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  VerticalAlign,
  Indent
} from '~/components/pattern/pattern-icon-plus'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseLink } from '~/components/base/base-link'
import style from './heading-logo.scss?module'

const cn = useClassNames('heading-logo', style)

const mainClass = [
  cn(),
  cnPatternIconPlus({ 'vertical-align': VerticalAlign.center })
]
const logoClass = [cn('logo'), cnPatternIconPlusIcon({ indent: Indent.xxl })]
const sloganClass = [cn('slogan')]

export const HeadingLogo = injectStyles(
  style,
  defineComponent({
    name: 'HeadingLogo',
    props: {
      slogan: {
        type: String,
        required: true
      },
      sloganClass: {
        type: String,
        default: null
      }
    },
    setup(props) {
      return () => (
        <BaseLink to="/" class={mainClass}>
          <BaseIcon name="logo" class={logoClass} width={96} />
          <span class={[sloganClass, props.sloganClass]}>{props.slogan}</span>
        </BaseLink>
      )
    }
  })
)
