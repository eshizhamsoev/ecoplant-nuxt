import {
  computed,
  defineComponent,
  inject,
  nextTick,
  ref,
  watch
} from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { HeadingLogo } from '~/components/blocks/shared-components/heading/heading-top-line/desktop/heading-logo'
import { DesktopHeadingContacts } from '~/components/blocks/shared-components/heading/heading-top-line/desktop/heading-contacts'
import { DesktopHeadingButton } from '~/components/blocks/shared-components/heading/heading-top-line/desktop/desktop-heading-button'
import { DesktopHeadingCallback } from '~/components/blocks/shared-components/heading/heading-top-line/desktop/heading-callback'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { BaseIcon } from '~/components/base'
import { menuSymbol } from '~/data/providers/menu/menuSymbol'
import { BlockSearch } from '~/components/blocks/block-search/block-search'
import { clearAllScrollLocks, lockScroll } from '~/support/utils/scroll-lock'
import style from './heading-top-line.scss?module'

const cn = useClassNames('heading-top-line', style)

const classes = {
  main: cn(),
  firstLine: cn('first-line'),
  secondLine: cn('second-line'),
  left: cn('left'),
  buttons: cn('buttons'),
  searchBlock: cn('search-block'),
  searchWrapper: cn('search-wrapper'),
  search: cn('search'),
  searchBackground: cn('search-background'),
  right: cn('right'),
  button: cn('button'),
  callback: cn('callback'),
  supportModal: cn('support-modal')
}

export const HeadingTopLine = injectStyles(
  style,
  defineComponent({
    name: 'HeadingTopLine',
    props: {
      withNavigation: {
        type: Boolean,
        default: true
      },
      sloganClass: {
        type: String,
        default: null
      }
    },
    setup(props, { root, refs }) {
      const modalOpener = useModalOpener()
      const openWaterModal = () => modalOpener({ name: 'how-to-water' })
      const openPriceListRequestModal = () =>
        modalOpener({
          name: 'price-list-request'
        })

      const openPlantingVideosModal = () => {
        modalOpener({
          name: 'planting-videos'
        })
      }

      const openShippingModal = () =>
        modalOpener({
          name: 'shipping'
        })

      const openLargeOrdersModal = () => modalOpener({ name: 'large-orders' })
      const menu = inject(menuSymbol)

      const slogan = computed(() => root.$accessor.contacts.siteName)

      const search = () => (
        <div class={classes.searchBlock}>
          <div class={classes.searchWrapper} ref="search">
            <BlockSearch onClose={closeSearch} class={classes.search} />
          </div>
          <div onclick={closeSearch} class={classes.searchBackground} />
        </div>
      )

      const searchVisible = ref<boolean>(false)

      const openSearch = () => {
        searchVisible.value = true
        nextTick(() => {
          if (refs.search instanceof Element) {
            lockScroll(refs.search)
          }
        })
      }

      const closeSearch = () => {
        searchVisible.value = false
        clearAllScrollLocks()
      }

      watch(
        () => root.$route,
        () => {
          closeSearch()
        }
      )

      return () => (
        <div class={classes.main}>
          {searchVisible.value && search()}
          <div class={classes.firstLine}>
            <button onClick={() => menu?.open()} class={cn('menu')}>
              <BaseIcon name="hamburger" width={24} height={24} />
              <span>меню</span>
            </button>
            <HeadingLogo
              slogan={slogan.value}
              sloganClass={props.sloganClass}
              class={classes.left}
            />
            {props.withNavigation && (
              <div class={classes.buttons}>
                <DesktopHeadingButton
                  class={classes.button}
                  onClick={openPlantingVideosModal}
                >
                  Посадка
                </DesktopHeadingButton>
                <DesktopHeadingButton
                  class={classes.button}
                  onClick={openShippingModal}
                >
                  Доставка
                </DesktopHeadingButton>
                <DesktopHeadingButton
                  class={classes.button}
                  onClick={openPriceListRequestModal}
                >
                  Прайс-лист
                </DesktopHeadingButton>
                <DesktopHeadingButton
                  class={classes.button}
                  onClick={openWaterModal}
                >
                  Полив
                </DesktopHeadingButton>
                <DesktopHeadingButton
                  class={classes.button}
                  onClick={openLargeOrdersModal}
                >
                  Оптовикам
                </DesktopHeadingButton>
                <DesktopHeadingButton
                  class={classes.button}
                  icon="magnifying-glass"
                  onClick={openSearch}
                >
                  Поиск по сайту
                </DesktopHeadingButton>
              </div>
            )}
            <DesktopHeadingContacts class={classes.right} />
          </div>
          <div class={classes.secondLine}>
            <DesktopHeadingCallback class={classes.callback} />
          </div>
        </div>
      )
    }
  })
)
