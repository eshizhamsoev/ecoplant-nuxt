import { defineComponent } from '@nuxtjs/composition-api'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  Indent,
  VerticalAlign
} from '~/components/pattern/pattern-icon-plus'

import { BaseIcon } from '~/components/base/base-icon'
import {
  EmailLink,
  EmailSlotData
} from '~/components/action-providers/email-link'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseRoundButton } from '~/components/base'
import { Color } from '~/components/theme/theme-button'
import style from './heading-email.scss?module'

const cn = useClassNames('heading-email', style)
const classes = {
  main: [cn(), cnPatternIconPlus({ 'vertical-align': VerticalAlign.center })],
  icon: [cn('icon'), cnPatternIconPlusIcon({ indent: Indent.l })]
}

export const HeadingEmail = injectStyles(
  style,
  defineComponent({
    name: 'HeadingEmail',
    props: {},
    setup() {
      const emailConstructor = (props: EmailSlotData) => [
        <BaseRoundButton size="s" class={classes.icon} color={Color.yellow}>
          <BaseIcon width={20} name="mail" />
        </BaseRoundButton>,
        <span>{props.email}</span>
      ]
      return () => (
        <EmailLink
          scopedSlots={{ default: emailConstructor }}
          class={classes.main}
        />
      )
    }
  })
)
