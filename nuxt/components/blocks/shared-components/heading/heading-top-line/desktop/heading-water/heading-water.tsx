import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseDashedButton } from '~/components/base'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './heading-water.scss?module'

const cn = useClassNames('heading-water', style)

const classes = {
  main: cn()
}

export const HeadingWater = injectStyles(
  style,
  defineComponent({
    name: 'HeadingWater',
    props: {},
    setup: () => {
      const modalOpener = useModalOpener()

      const openHowToWater = () =>
        modalOpener({
          name: 'sale-fir'
        })
      return () => (
        <BaseDashedButton
          class={classes.main}
          color="white"
          size="l"
          onClick={openHowToWater}
        >
          Полив
        </BaseDashedButton>
      )
    }
  })
)
