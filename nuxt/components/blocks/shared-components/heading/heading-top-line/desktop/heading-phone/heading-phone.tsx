import { defineComponent } from '@nuxtjs/composition-api'
import {
  PhoneLink,
  PhoneSlotData
} from '~/components/action-providers/phone-link'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  Indent,
  VerticalAlign
} from '~/components/pattern/pattern-icon-plus'
import { BaseIcon } from '~/components/base/base-icon'
import { BaseRoundButton } from '~/components/base/base-round-button'
import { injectStyles } from '~/support/utils/injectStyle'
import { useClassNames } from '~/support/utils/bem-classnames'
import style from './heading-phone.scss?module'

const cn = useClassNames('heading-phone', style)
const classes = {
  main: [cn(), cnPatternIconPlus({ 'vertical-align': VerticalAlign.center })],
  icon: [cn('icon'), cnPatternIconPlusIcon({ indent: Indent.l })]
}

export const HeadingPhone = injectStyles(
  style,
  defineComponent({
    name: 'HeadingPhone',
    props: {},
    setup() {
      const phoneConstructor = (props: PhoneSlotData) => [
        <BaseRoundButton size="s" class={classes.icon}>
          <BaseIcon name="phone" width={13} />
        </BaseRoundButton>,
        <span>{props.phone.text}</span>
      ]
      return () => (
        <PhoneLink
          scopedSlots={{ default: phoneConstructor }}
          class={classes.main}
        />
      )
    }
  })
)
