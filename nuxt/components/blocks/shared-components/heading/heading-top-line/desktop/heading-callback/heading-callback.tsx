import { defineComponent } from '@nuxtjs/composition-api'
import { injectStyles } from '~/support/utils/injectStyle'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './heading-callback.scss?module'

const classes = {
  main: style['heading-callback']
}

export const DesktopHeadingCallback = injectStyles(
  style,
  defineComponent({
    name: 'DesktopHeadingCallback',
    props: {},
    setup() {
      const modalOpener = useModalOpener()
      const openModal = () =>
        modalOpener({
          name: 'callback',
          source: 'Заказать звонок в шапке'
        })

      return () => (
        <button class={classes.main} onClick={openModal}>
          Заказать звонок
        </button>
      )
    }
  })
)
