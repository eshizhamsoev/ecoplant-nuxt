import { defineComponent, nextTick, ref, watch } from '@nuxtjs/composition-api'
import {
  EmailLink,
  EmailSlotData
} from '~/components/action-providers/email-link'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import {
  PhoneLink,
  PhoneSlotData
} from '~/components/action-providers/phone-link'
import { BaseIcon } from '~/components/base'
import { MobileHeadingTopLineItem } from '~/components/blocks/shared-components/heading/heading-top-line/mobile/mobile-heading-top-line-item/mobile-heading-top-line-item'
import { MobileHeadingSaleButton } from '~/components/blocks/block-heading/mobile/mobile-heading-sale-button/mobile-heading-sale-button'
import { clearAllScrollLocks, lockScroll } from '~/support/utils/scroll-lock'
import { BlockSearch } from '~/components/blocks/block-search/block-search'
import style from './mobile-heading-top-line.scss?module'

const cn = useClassNames('mobile-heading-top-line', style)

const classes = {
  main: cn(),
  line: cn('line'),
  item: cn('item'),
  innerItem: cn('inner-item'),
  right: cn('right'),
  phone: cn('phone'),
  email: cn('email'),
  callback: cn('callback'),
  water: cn('water'),
  supportModal: cn('support-modal'),
  supportModalPointer: cn('support-modal-pointer'),
  menu: cn('menu'),
  phoneIcon: cn('phone-icon'),
  emailIcon: cn('email-icon'),
  deliveryItem: cn('delivery-item'),
  button: cn('button'),
  search: cn('search')
}

type possibleElement =
  | 'menu'
  | 'email'
  | 'offer'
  | 'callback'
  | 'water'
  | 'phone'

export const MobileHeadingTopLine = injectStyles(
  style,
  defineComponent({
    name: 'MobileHeadingTopLine',
    props: {
      hideElements: {
        type: Array,
        default: () => [] as possibleElement[]
      },
      withNavigation: {
        type: Boolean,
        default: true
      }
    },
    setup: (props, { root, refs }) => {
      const modalOpener = useModalOpener()

      const openPlantingModal = () => modalOpener({ name: 'planting' })
      const openShippingModal = () => modalOpener({ name: 'shipping' })

      const openPriceListModal = () =>
        modalOpener({
          name: 'price-list-request'
        })

      const email = ({ email }: EmailSlotData) => [
        <BaseIcon name="mail" width={20} class={classes.emailIcon} />,
        email
      ]
      const phone = ({ phone: { text } }: PhoneSlotData) => [
        <BaseIcon name="phone" width={20} class={classes.phoneIcon} />,
        text
      ]

      const search = () => (
        <div class={classes.search} ref="search">
          <BlockSearch onClose={closeSearch} />
          <div onclick={closeSearch} class={cn('search-background')} />
        </div>
      )

      const searchVisible = ref<boolean>(false)

      const openSearch = () => {
        searchVisible.value = true
        nextTick(() => {
          if (refs.search instanceof Element) {
            lockScroll(refs.search)
          }
        })
      }

      const closeSearch = () => {
        searchVisible.value = false
        clearAllScrollLocks()
      }

      watch(
        () => root.$route,
        () => {
          closeSearch()
        }
      )
      return () => (
        <div class={classes.main}>
          {searchVisible.value && search()}
          {props.withNavigation && (
            <div class={classes.line}>
              <MobileHeadingTopLineItem
                class={classes.item}
                icon="sprout"
                onClick={openPlantingModal}
              >
                посадка
              </MobileHeadingTopLineItem>
              <MobileHeadingTopLineItem
                class={classes.item}
                icon="heading-top-line/delivery"
                onClick={openShippingModal}
              >
                доставка
              </MobileHeadingTopLineItem>
              <MobileHeadingTopLineItem
                class={classes.item}
                icon="heading-top-line/catalog"
                onClick={openPriceListModal}
              >
                каталог <br /> прайс
              </MobileHeadingTopLineItem>
              <MobileHeadingTopLineItem
                class={classes.item}
                icon="magnifying-glass"
                onClick={openSearch}
              >
                поиск <br /> по сайту
              </MobileHeadingTopLineItem>
              <MobileHeadingSaleButton class={classes.button} />
            </div>
          )}
          {!props.hideElements.includes('phone') && (
            <PhoneLink scopedSlots={{ default: phone }} class={classes.phone} />
          )}
          {!props.hideElements.includes('email') && (
            <EmailLink scopedSlots={{ default: email }} class={classes.email} />
          )}
        </div>
      )
    }
  })
)
