import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { BaseLink } from '~/components/base/base-link'
import style from './mobile-heading-top-line-item.scss?module'

const cn = useClassNames('mobile-heading-top-line-item', style)

const classes = {
  main: cn(),
  icon: cn('icon'),
  text: cn('text')
}

export const MobileHeadingTopLineDelivery = injectStyles(
  style,
  defineComponent({
    name: 'MobileHeadingTopLineDelivery',
    props: {
      icon: {
        type: String,
        required: true
      }
    },
    setup: (props, { slots, root }) => {
      return () => (
        <BaseLink
          class={classes.main}
          to={{
            path: root.$route.path,
            query: { tabs: 'BlockShipping' },
            hash: 'tabs-BlockShipping'
          }}
        >
          <BaseIcon
            name={props.icon}
            width={25}
            height={20}
            class={classes.icon}
          />
          <span class={classes.text}>{slots.default && slots.default()}</span>
        </BaseLink>
      )
    }
  })
)
