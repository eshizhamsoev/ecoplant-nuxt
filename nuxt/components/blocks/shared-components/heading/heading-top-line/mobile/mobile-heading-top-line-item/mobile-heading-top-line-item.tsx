import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import style from './mobile-heading-top-line-item.scss?module'

const cn = useClassNames('mobile-heading-top-line-item', style)

const classes = {
  main: cn(),
  icon: cn('icon'),
  text: cn('text')
}

export const MobileHeadingTopLineItem = injectStyles(
  style,
  defineComponent({
    name: 'MobileHeadingTopLineItem',
    props: {
      icon: {
        type: String,
        required: true
      }
    },
    setup: (props, { slots, emit }) => {
      const onClick = (e: Event) => {
        emit('click', e)
      }
      return () => (
        <button class={classes.main} type="button" onClick={onClick}>
          <BaseIcon name={props.icon} width={18} class={classes.icon} />
          <span class={classes.text}>{slots.default && slots.default()}</span>
        </button>
      )
    }
  })
)
