import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const HeadingTopLine = UtilUniversalComponentFactory(
  'HeadingTopLine',
  () => import('./mobile').then((c) => c.MobileHeadingTopLine),
  () => import('./desktop').then((c) => c.HeadingTopLine)
)
