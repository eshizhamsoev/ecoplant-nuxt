import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ProductsGridItemBase } from '~/components/blocks/shared-components/products-grid-item/base'
import style from './products-grid-item-image.scss?module'

const cn = useClassNames('products-grid-item-image', style)

export const ProductsGridItemImage = injectStyles(
  style,
  defineComponent({
    name: 'ProductsGridItemImage',
    props: {
      name: {
        type: String,
        required: true
      },
      to: {
        type: String,
        required: true
      },
      image: {
        type: String,
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <ProductsGridItemBase name={props.name} to={props.to}>
          <img src={props.image} alt={props.name} class={cn('image')} />
        </ProductsGridItemBase>
      )
    }
  })
)
