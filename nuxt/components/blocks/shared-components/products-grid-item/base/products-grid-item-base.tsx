import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { decodeHTMLEntities } from '~/support/utils/decode-entities'
import { BaseIcon } from '~/components/base'
import { BaseBadgedText } from '~/components/base/base-badged-text/base-badged-text'
import { BaseLink } from '~/components/base/base-link'
import style from './products-grid-item-base.scss?module'

const cn = useClassNames('products-grid-item-base', style)

const classes = {
  main: cn(),
  wrapper: cn('wrapper'),
  name: cn('name'),
  arrow: cn('arrow'),
  arrowIcon: cn('arrow-icon')
}

export const ProductsGridItemBase = injectStyles(
  style,
  defineComponent({
    name: 'ProductsGridItemBase',
    props: {
      name: {
        type: String,
        required: true
      },
      to: {
        type: String,
        required: true
      }
    },
    setup: (props, { slots }) => {
      const lines = computed(() => {
        const items = decodeHTMLEntities(props.name).split(/\s/g)
        const lines = [] as string[]
        let max = 8
        let line = ''
        items.forEach((item) => {
          if (!line) {
            line = item
            if (line.length > max) {
              max = line.length
            }
          } else if (item.length + line.length > max) {
            lines.push(line)
            line = item
            if (line.length > max) {
              max = line.length
            }
          } else {
            line += ' ' + item
          }
        })
        lines.push(line)
        return lines
      })
      return () => (
        <BaseLink to={props.to} class={classes.main}>
          {slots.default && slots.default()}
          <BaseBadgedText lines={lines.value} class={classes.name} />
          <div class={classes.arrow}>
            <BaseIcon
              name="arrow-left"
              width={15}
              class={classes.arrowIcon}
              rotate={180}
            />
          </div>
        </BaseLink>
      )
    }
  })
)
