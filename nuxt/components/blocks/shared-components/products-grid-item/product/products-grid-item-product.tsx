import { defineComponent, PropType } from '@nuxtjs/composition-api'
import { ProductsGridItemImage } from '~/components/blocks/shared-components/products-grid-item/image'

type Item = {
  link: string
  name: string
  image: string
}

export const ProductsGridItemProduct = defineComponent({
  name: 'ProductsGridItemImage',
  props: {
    item: {
      type: Object as PropType<Item>,
      required: true
    }
  },
  setup: (props) => {
    return () => (
      <ProductsGridItemImage
        name={props.item.name}
        to={props.item.link}
        image={props.item.image}
      />
    )
  }
})
