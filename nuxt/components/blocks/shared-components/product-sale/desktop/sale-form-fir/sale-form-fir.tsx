import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'

import { injectStyles } from '~/support/utils/injectStyle'
import { DesktopSaleFormLine } from '~/components/blocks/shared-components/sale-form-line/desktop'
import { SaleFirBanner } from '~/components/blocks/shared-components/product-sale/desktop/sale-fir-banner'
import style from './sale-form-fir.scss?module'

const cn = useClassNames('sale-form-fir', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('leading'),
  form: cn('form'),
  policy: cn('policy'),
  phone: cn('phone'),
  button: cn('button')
}

export const SaleFormFir = injectStyles(
  style,
  defineComponent({
    name: 'SaleFormFir',
    props: {
      source: {
        type: String,
        required: true
      }
    },
    setup(props, { refs }) {
      const focus = () => {
        if ('$el' in refs.form) {
          refs.form.$el.querySelector('input')?.focus()
        }
      }
      return () => (
        <div class={classes.main}>
          <SaleFirBanner onClick={focus} />
          <DesktopSaleFormLine
            class={classes.form}
            source={props.source}
            ref="form"
          />
          <div class={classes.policy}>
            Согласен с политикой конфиденциальности
          </div>
        </div>
      )
    }
  })
)
