import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'

import desktopFir from '~/assets/common/fir-sale/desktop-fir.svg'
import style from './sale-fir-banner.scss?module'

const cn = useClassNames('sale-fir-banner', style)

const classes = {
  main: cn(),
  text: cn('text'),
  heading: cn('heading'),
  leading: cn('leading'),
  banner: cn('banner')
}

export const SaleFirBanner = injectStyles(
  style,
  defineComponent({
    name: 'SaleFirBanner',
    setup: (root, { emit }) => {
      return () => (
        <div class={classes.main} onClick={() => emit('click')}>
          <img src={desktopFir} alt="" class={cn('content')} />
        </div>
      )
    }
  })
)
