import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'

import { injectStyles } from '~/support/utils/injectStyle'
import { DesktopSaleFormLine } from '~/components/blocks/shared-components/sale-form-line/desktop'
import { BasePolicyLink } from '~/components/base/base-policy-link/base-policy-link'
import style from './sale-form-fertilize.scss?module'

const cn = useClassNames('sale-form-fertilize', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('leading'),
  form: cn('form'),
  policy: cn('policy'),
  phone: cn('phone'),
  button: cn('button')
}

export const SaleFormFertilize = injectStyles(
  style,
  defineComponent({
    name: 'SaleFormFir',
    props: {
      source: {
        type: String,
        required: true
      }
    },
    setup(props, { refs }) {
      const focus = () => {
        if ('$el' in refs.form) {
          refs.form.$el.querySelector('input')?.focus()
        }
      }
      return () => (
        <div class={classes.main}>
          <div class={cn('heading')} onClick={focus}>
            Забронируйте за собой <b>скидку 70%</b>
          </div>
          <div class={cn('leading')} onClick={focus}>
            и получите удобрение в подарок
          </div>
          <DesktopSaleFormLine
            class={classes.form}
            source={props.source}
            ref="form"
          />
          <BasePolicyLink class={classes.policy} />
        </div>
      )
    }
  })
)
