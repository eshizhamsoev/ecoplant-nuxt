import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { BaseRubles } from '~/components/base/base-rubles/base-rubles'
import { injectStyles } from '~/support/utils/injectStyle'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './sale-price-info.scss?module'

const cn = useClassNames('sale-price-info', style)

const classes = {
  main: cn(),
  label: cn('label'),
  price: cn('price'),
  formLabel: cn('form-label'),
  oldPrice: cn('old-price')
}

export const SalePriceInfo = injectStyles(
  style,
  defineComponent({
    props: {
      price: {
        type: Number,
        required: true
      },
      oldPrice: {
        type: Number,
        required: true
      }
    },
    setup: (props) => {
      const modalOpener = useModalOpener()
      const openGetSaleModal = () => {
        modalOpener({
          name: 'get-sale'
        })
      }

      return () => (
        <div class={classes.main}>
          <div class={classes.label}>новая цена</div>

          <BaseRubles
            class={classes.price}
            price={props.price}
            nativeOnClick={openGetSaleModal}
          />

          <BaseRubles class={classes.oldPrice} price={props.oldPrice} />
        </div>
      )
    }
  })
)
