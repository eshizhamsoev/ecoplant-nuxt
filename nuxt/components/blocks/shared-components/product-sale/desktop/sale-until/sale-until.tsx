import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './sale-until.scss?module'

const cn = useClassNames('sale-until', style)

const classes = {
  main: cn(),
  accent: cn('accent')
}

export const SaleUntil = injectStyles(
  style,
  defineComponent<{ class: string }>({
    props: {},
    setup: () => {
      return () => (
        <div class={classes.main}>
          <b class={classes.accent}>Внимание!</b> Скидка действует до конца
          недели
        </div>
      )
    }
  })
)
