import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './sale-image.scss?module'

const cn = useClassNames('sale-image', style)

const classes = {
  main: cn(),
  image: cn('image'),
  size: cn('size')
}

export const SaleImage = injectStyles(
  style,
  defineComponent({
    props: {
      image: {
        type: String,
        required: true
      },
      size: {
        type: String as () => string | null,
        default: null
      }
    },
    setup: (props, { slots, emit }) => {
      return () => (
        <div class={classes.main} onClick={() => emit('click', 0)}>
          <img class={classes.image} src={props.image} loading={'lazy'} />
          {props.size ? (
            <div class={classes.size}>Высота: {props.size}</div>
          ) : null}
          {slots.default ? slots.default() : null}
        </div>
      )
    }
  })
)
