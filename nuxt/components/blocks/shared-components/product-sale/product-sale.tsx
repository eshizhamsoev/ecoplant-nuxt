import { computed, defineComponent, PropOptions } from '@nuxtjs/composition-api'
import { SalePriceInfo } from './desktop/sale-price-info'
import { SaleUntil } from './desktop/sale-until'
import { SaleImage } from './desktop/sale-image'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import fertilizer from '~/assets/common/fertilizer.png'
import { SaleFormFertilize } from '~/components/blocks/shared-components/product-sale/desktop/sale-form-fertilize/sale-form-fertilize'
import style from './product-sale.scss?module'

const cn = useClassNames('product-sale', style)

const classes = {
  main: cn(),
  left: cn('left'),
  right: cn('right'),
  form: cn('form'),
  price: cn('price'),
  until: cn('until')
}

type Price = {
  price?: number | null
  oldPrice?: number | null
}

export const ProductSale = injectStyles(
  style,
  defineComponent({
    props: {
      name: {
        type: String,
        required: true
      },
      image: {
        type: String,
        required: true
      },
      size: {
        validator: (prop: any) => typeof prop === 'string' || prop === null,
        default: null
      } as PropOptions<string | null>,
      price: {
        type: Object,
        required: false,
        default() {
          return null
        }
      } as PropOptions<Price | null>
    },
    setup: (props) => {
      const showPrice = computed(
        () => props.price?.price && props.price?.oldPrice
      )
      return () => (
        <div class={classes.main}>
          <img
            src={fertilizer}
            class={cn('fertilizer')}
            loading={'lazy'}
            width={248}
            height={325}
            alt=""
          />
          <div class={classes.left}>
            <SaleImage
              image={props.image}
              size={props.size}
              class={cn('image')}
            >
              {showPrice.value && (
                <SalePriceInfo
                  price={props.price?.price as number}
                  oldPrice={props.price?.oldPrice as number}
                  class={classes.price}
                />
              )}
            </SaleImage>
            {showPrice.value && <SaleUntil class={classes.until} />}
          </div>
          <div class={classes.right}>
            <SaleFormFertilize source={props.name} />
          </div>
        </div>
      )
    }
  })
)
