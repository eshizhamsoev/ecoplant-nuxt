import { computed, defineComponent } from '@nuxtjs/composition-api'
import { MobileSaleForm } from './mobile-sale-form'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { SaleImage } from '~/components/blocks/shared-components/product-sale/desktop/sale-image'
import { SalePriceInfo } from '~/components/blocks/shared-components/product-sale/desktop/sale-price-info'
import { SaleUntil } from '~/components/blocks/shared-components/product-sale/desktop/sale-until'
// import { SaleForm } from '~/components/blocks/shared-components/product-sale/sale-form/sale-form'
import { Maybe, Price, Product } from '~/_generated/types'
import { BasePopupGallery } from '~/components/base'
import { usePopupGallery } from '~/components/base/base-popup-gallery/usePopupGallery'
import style from './mobile-product-sale.scss?module'

const cn = useClassNames('mobile-product-sale', style)

const classes = {
  main: cn(),
  firstLine: cn('first-line'),
  right: cn('right'),
  form: cn('form'),
  image: cn('image'),
  price: cn('price'),
  until: cn('until')
}

export const MobileProductSale = injectStyles(
  style,
  defineComponent({
    name: 'MobileProductSale',
    props: {
      product: {
        type: Object as () => Pick<
          Product,
          'squaredImage' | 'showSalePrice' | 'name'
        > & { salePrice?: Maybe<Pick<Price, 'price' | 'size' | 'oldPrice'>> },
        required: true
      }
    },
    setup: (props) => {
      const {
        open: openPopupGallery,
        close: closePopupGallery,
        index: popupIndex
      } = usePopupGallery()
      const images = computed(() => [
        {
          small: props.product.squaredImage.thumb,
          large: props.product.squaredImage.original
        }
      ])

      return () =>
        props.product.salePrice && (
          <div class={classes.main}>
            {props.product.showSalePrice && (
              <div class={classes.firstLine}>
                <SaleImage
                  class={classes.image}
                  image={props.product.squaredImage.thumb}
                  size={props.product.salePrice.size}
                  onClick={openPopupGallery}
                />
                {props.product.salePrice.price &&
                  props.product.salePrice.oldPrice && (
                    <div class={classes.right}>
                      <SalePriceInfo
                        price={props.product.salePrice.price}
                        oldPrice={props.product.salePrice.oldPrice}
                        class={classes.price}
                      />
                      <SaleUntil class={classes.until} />
                    </div>
                  )}
              </div>
            )}
            <MobileSaleForm source={props.product.name} class={classes.form} />
            <BasePopupGallery
              images={images.value}
              index={popupIndex.value}
              show-thumbnails={false}
              onClose={closePopupGallery}
            />
          </div>
        )
    }
  })
)
