import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobileSaleFormLine } from '~/components/blocks/shared-components/sale-form-line/mobile/mobile-sale-form-line'
import { MobileWhatsappButton } from '~/components/blocks/shared-components/mobile-buttons/mobile-whatsapp-button'

import fertilizer40 from '~/assets/common/fertilizer_40.png'
import fertilizer80 from '~/assets/common/fertilizer_80.png'
import { MobileTelegramButton } from '~/components/blocks/shared-components/mobile-buttons/mobile-telegram-button'
import style from './mobile-sale-form.scss?module'

const cn = useClassNames('mobile-sale-form', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  fertilizerLine: cn('fertilizer-line'),
  fertilizer: cn('fertilizer'),
  banner: cn('banner'),
  bannerImage: cn('banner-image'),
  plus: cn('plus'),
  fertilizerText: cn('fertilizer-text'),
  form: cn('form'),
  phone: cn('phone'),
  button: cn('button'),
  policy: cn('policy'),
  circleButtonsWrapper: cn('circle-buttons-wrapper'),
  whatsappButton: cn('whatsapp-button'),
  phoneButton: cn('phone-button')
}

export const MobileSaleForm = injectStyles(
  style,
  defineComponent({
    name: 'MobileSaleForm',
    props: {
      source: {
        type: String,
        required: true
      },
      withButtons: {
        type: Boolean,
        default: true
      }
    },
    setup(props, { refs }) {
      const focus = () => {
        const form = refs.form
        if ('$el' in form) {
          if (form.$el?.querySelector) {
            form.$el.querySelector('input')?.focus()
          }
        }
      }
      return () => (
        <div class={classes.main}>
          {props.withButtons && (
            <div class={classes.circleButtonsWrapper}>
              <MobileWhatsappButton class={classes.whatsappButton} />
              <div class={classes.banner}>
                <div class={classes.heading}>
                  Получить
                  <br />
                  скидку <b>70%</b>
                </div>
                <div class={classes.fertilizerLine} onClick={focus}>
                  <img
                    src={fertilizer40}
                    srcset={`${fertilizer40}, ${fertilizer80} 2x`}
                    width={40}
                    height={52}
                  />
                  <span class={classes.plus}>+</span>
                  <span class={classes.fertilizerText}>
                    Удобрение
                    <br /> в подарок!
                  </span>
                </div>
              </div>
              <MobileTelegramButton class={classes.phoneButton} />
            </div>
          )}
          <MobileSaleFormLine
            class={classes.form}
            source={props.source}
            ref="form"
          />
          <div class={classes.policy}>
            Согласен с политикой конфиденциальности
          </div>
        </div>
      )
    }
  })
)
