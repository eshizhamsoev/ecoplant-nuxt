import { computed, defineComponent, inject } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { BaseIntegerInput } from '~/components/base/base-integer-input'
import {
  QuantityProvider,
  QuantityProviderSlotData
} from '~/components/action-providers/quantity-provider/quantity-provider'

import { cartSymbol } from '~/data/providers/cart/symbol'
import style from './product-price-cart.scss?module'

const cn = useClassNames('product-price-cart', style)

const classes = {
  main: cn(),
  button: cn('button'),
  icon: cn('icon'),
  circleButton: cn('circle-button'),
  circleInput: cn('circle-input'),
  input: cn('input'),
  price: cn('price')
}

export const ProductPriceCart = injectStyles(
  style,
  defineComponent({
    name: 'ProductPriceCart',
    props: {
      productId: {
        type: String,
        required: true
      },
      priceId: {
        type: String,
        required: true
      }
    },
    setup: (props, { slots }) => {
      const cart = inject(cartSymbol)
      const price = computed(
        () => cart && cart.getPrice(props.productId, props.priceId)
      )
      const quantity = computed(() => price.value?.cartQuantity || 0)

      const scopedSlots = {
        default: ({
          minus,
          plus,
          value,
          changeValue,
          canMinus
        }: QuantityProviderSlotData) => (
          <div>
            <button
              class={cn('circle-button', { active: canMinus })}
              onClick={minus}
            >
              <BaseIcon name={'minus'} width={20} />
            </button>
            <span class={classes.price}>
              {slots.default && slots.default()}
            </span>
            <button
              class={cn('circle-button', { active: true })}
              onClick={plus}
            >
              <BaseIcon name={'plus'} width={20} />
            </button>
            <label
              class={cn('circle-input', { active: value !== 0 })}
              title="Изменить количество"
            >
              <BaseIntegerInput
                min={0}
                max={99999}
                value={value}
                onChange={changeValue}
                class={classes.input}
              />
            </label>
          </div>
        )
      }

      const updateProductPriceQuantity = (value: number) => {
        if (!cart) {
          return
        }
        cart.updateProductPriceQuantity(props.productId, props.priceId, value)
      }

      return () => (
        <QuantityProvider
          class={classes.main}
          scopedSlots={scopedSlots}
          onInput={updateProductPriceQuantity}
          minimum={0}
          value={quantity.value}
        />
      )
    }
  })
)
