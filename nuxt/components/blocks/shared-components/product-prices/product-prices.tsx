import {
  computed,
  defineComponent,
  ref,
  useContext,
  watch
} from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { BaseRubles } from '~/components/base/base-rubles/base-rubles'
import { injectStyles } from '~/support/utils/injectStyle'
import { ProductPriceCart } from '~/components/blocks/shared-components/product-prices/components'
import { Price, Product } from '~/_generated/types'
import { BaseButton, BaseIcon } from '~/components/base'
import { Visual } from '~/components/theme/theme-button'
import { MobileProductCartButton } from '~/components/blocks/block-product/mobile/mobile-product-cart-button'
import style from './product-prices.scss?module'

const cn = useClassNames('product-prices', style)

export const ProductPrices = injectStyles(
  style,
  defineComponent({
    name: 'ProductPrices',
    props: {
      product: {
        type: Object as () => Pick<Product, 'id'> & {
          prices: Pick<Price, 'id' | 'size' | 'price' | 'weekPrice'>[]
        },
        required: true
      }
    },
    setup: (props, { root, refs }) => {
      const { $device } = useContext()
      const buttonText = computed(() =>
        $device.isMobileOrTablet ? 'Все размеры' : 'Показать все размеры'
      )
      const prices = computed(() => props.product.prices)

      const shouldHideItems = computed(() => prices.value.length > 7)
      const allPricesShown = ref(false)
      watch(
        () => props.product,
        () => {
          allPricesShown.value = false
        }
      )
      const visibleQuantity = computed(() =>
        shouldHideItems.value && !allPricesShown.value ? 6 : prices.value.length
      )

      const toggle = () => {
        allPricesShown.value = !allPricesShown.value
      }

      const itemsState = computed(() => {
        if (!shouldHideItems.value) {
          return undefined
        }
        if (!root.$device.isMobileOrTablet) {
          return 'desktop'
        }
        return allPricesShown.value ? 'mobile-shown' : 'mobile-hidden'
      })

      watch(
        () => allPricesShown.value,
        (show) => {
          if (refs.root instanceof HTMLElement) {
            if (show) {
              refs.root.style.height = refs.root.scrollHeight + 'px'
            } else {
              refs.root.style.height = ''
            }
          }
        }
      )

      const isSale = computed(() =>
        props.product.prices.some((item) => item.weekPrice)
      )

      const sales = computed(() => {
        if (!isSale.value) {
          return []
        }

        const items = []
        let start: number | null = null
        for (let i = 0; i < prices.value.length; i++) {
          const price = prices.value[i]
          if (price.weekPrice) {
            if (start === null) {
              start = i
            }
            if (
              i === prices.value.length - 1 ||
              !prices.value[i + 1].weekPrice
            ) {
              items.push({
                start,
                end: i
              })
            }
          }
        }
        return items
      })

      if (sales.value.length && sales.value[sales.value.length - 1].end > 5) {
        allPricesShown.value = true
      }

      return () => (
        <div
          class={cn({
            'has-hidden-items': shouldHideItems.value,
            'items-state': itemsState.value
          })}
          ref="root"
        >
          {sales.value.map(({ start, end }) => (
            <div
              class={cn('sale')}
              style={{
                'grid-row-start': start + 2,
                'grid-row-end': end + 3
              }}
            />
          ))}
          <div class={cn('item', { line: 'head', type: 'space' })} />
          <div class={cn('item', { line: 'head', type: 'size' })}>Размер</div>
          <div class={cn('item', { line: 'head', type: 'space' })} />
          <div class={cn('item', { line: 'head', type: 'price' })}>
            Стоимость
          </div>
          <div class={cn('item', { line: 'head', type: 'space' })} />
          {prices.value
            .filter((price) => !!price)
            .map((price, index: number) => {
              const line = index % 2 ? 'even' : 'odd'
              const hidden = index >= visibleQuantity.value
              const priceIsSale = price.weekPrice && isSale.value
              const items = [
                <div
                  class={cn('item', {
                    line,
                    hidden,
                    type: 'space'
                  })}
                />,
                <div
                  class={cn('item', {
                    line,
                    hidden,
                    type: 'size'
                  })}
                >
                  {price.size}
                </div>,
                <div
                  class={cn('item', {
                    line,
                    hidden,
                    type: priceIsSale ? 'sale' : 'space'
                  })}
                >
                  {priceIsSale ? '!sale!' : null}
                </div>,
                <div
                  class={cn('item', {
                    line,
                    hidden,
                    type: 'price',
                    sale: priceIsSale
                  })}
                >
                  {price.price ? (
                    <ProductPriceCart
                      class={cn('cart')}
                      productId={props.product.id}
                      priceId={price.id}
                    >
                      <BaseRubles price={price.price} />
                    </ProductPriceCart>
                  ) : (
                    'Цена по запросу'
                  )}
                </div>,
                <div
                  class={cn('item', {
                    line,
                    hidden,
                    type: 'space',
                    sale: priceIsSale
                  })}
                />
              ]
              if (!priceIsSale && price.weekPrice) {
                items.push(
                  <div
                    class={cn('item', {
                      line,
                      hidden,
                      type: 'week-price'
                    })}
                  >
                    Цена недели
                  </div>
                )
              }
              return items
            })}
          {shouldHideItems.value && root.$device.isMobileOrTablet && (
            <MobileProductCartButton class={cn('cart')} />
          )}
          {shouldHideItems.value && (
            <BaseButton
              onClick={toggle}
              type="button"
              visual={Visual.outline}
              size="custom"
              class={cn('show-all-button', { type: 'additional' })}
            >
              <BaseIcon
                name="arrow-down"
                width={12}
                height={22}
                class={cn('show-all-button-icon', { position: 'left' })}
                rotate={allPricesShown.value ? 180 : 0}
              />
              <span class={cn('show-all-button-inner')}>
                {allPricesShown.value ? 'скрыть размеры' : buttonText.value}
              </span>
              <BaseIcon
                name="arrow-down"
                width={12}
                height={22}
                class={cn('show-all-button-icon', { position: 'right' })}
                rotate={allPricesShown.value ? 180 : 0}
              />
            </BaseButton>
          )}
        </div>
      )
    }
  })
)
