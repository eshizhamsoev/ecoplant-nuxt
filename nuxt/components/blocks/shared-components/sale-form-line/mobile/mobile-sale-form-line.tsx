import { componentFactory } from '~/components/blocks/shared-components/sale-form-line/componentFactory'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './mobile-sale-form-line.scss?module'

const cn = useClassNames('mobile-sale-form-line', style)

const classes = {
  main: cn(),
  phone: cn('phone'),
  button: cn('button')
}
export const MobileSaleFormLine = injectStyles(
  style,
  componentFactory('MobileSaleFormLine', classes)
)
