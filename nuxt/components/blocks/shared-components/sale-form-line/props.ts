import { GTM_EVENTS } from '~/support/hooks/useReachGoal'

export const saleFormLineProps = {
  source: {
    type: String,
    required: true as true
  },
  goal: {
    type: String as () => GTM_EVENTS,
    default: GTM_EVENTS.callBack
  }
}
