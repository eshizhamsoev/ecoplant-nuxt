import { componentFactory } from '~/components/blocks/shared-components/sale-form-line/componentFactory'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './desktop-sale-form-line.scss?module'

const cn = useClassNames('desktop-sale-form-line', style)

const classes = {
  main: cn(),
  phone: cn('phone'),
  button: cn('button'),
  error: cn('error')
}
export const DesktopSaleFormLine = injectStyles(
  style,
  componentFactory('DesktopSaleFormLine', classes)
)
