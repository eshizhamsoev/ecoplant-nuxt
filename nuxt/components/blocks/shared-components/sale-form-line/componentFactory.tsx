import { defineComponent } from '@nuxtjs/composition-api'
import { saleFormLineProps } from '~/components/blocks/shared-components/sale-form-line/props'
import { BasePhoneForm } from '~/components/base/base-phone-form/base-phone-form'
import { Color } from '~/components/theme/theme-button'

interface Classes {
  main?: string
  phone?: string
  button?: string
  error?: string
}

export const componentFactory = (name: string, classes: Classes) =>
  defineComponent({
    name,
    props: saleFormLineProps,
    setup: (props, { slots }) => {
      return () => (
        <BasePhoneForm
          mainClass={classes.main}
          errorClass={classes.error}
          phoneClass={classes.phone}
          buttonClass={classes.button}
          buttonOptions={{ size: 'l', color: Color.red, shadow: true }}
          source={props.source}
          goal={props.goal}
        >
          <template slot="button">
            {(slots.button && slots.button()) || 'Получить скидку'}
          </template>
        </BasePhoneForm>
      )
    }
  })
