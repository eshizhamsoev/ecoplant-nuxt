import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const SaleFormLine = UtilUniversalComponentFactory(
  'SaleFormLine',
  () => import('./mobile/index').then((c) => c.MobileSaleFormLine),
  () => import('./desktop/index').then((c) => c.DesktopSaleFormLine)
)
