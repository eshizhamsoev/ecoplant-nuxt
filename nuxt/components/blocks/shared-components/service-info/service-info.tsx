import { defineComponent, PropType, toRefs } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import {
  Orientation,
  ServiceImages
} from '~/components/blocks/shared-components/service-images/service-images'
import { BaseButton } from '~/components/base'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './service-info.scss?module'

const cn = useClassNames('service-info', style)

type Service = {
  __typename?: string
  id: string
  name: string
  price?: string | null
  images: {
    small: string
    large: string
    original: string
  }[]
}

export const ServiceInfo = injectStyles(
  style,
  defineComponent({
    name: 'ServiceInfo',
    props: {
      service: {
        type: Object as PropType<Service>,
        required: true
      }
    },
    setup: (props) => {
      const { service } = toRefs(props)
      const modalOpener = useModalOpener()
      const openCallbackModal = () => modalOpener({ name: 'callback' })
      return () =>
        service.value && (
          <div class={cn()}>
            <ServiceImages
              class={cn('images')}
              images={service.value.images}
              orientation={
                service.value.__typename === 'Service'
                  ? Orientation.HORIZONTAL
                  : Orientation.VERTICAL
              }
              key={service.value.id}
            />
            <div class={cn('price')}>
              <div class={cn('price-label')}>
                {service.value.price
                  ? `Цена: ${service.value.price}`
                  : 'Цена по запросу'}
              </div>
              <BaseButton
                size="m"
                class={cn('button')}
                onClick={openCallbackModal}
              >
                {service.value.price ? 'Заказать сейчас' : 'Узнай цену сейчас'}
              </BaseButton>
            </div>
          </div>
        )
    }
  })
)
