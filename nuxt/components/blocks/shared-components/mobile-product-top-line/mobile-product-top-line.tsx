import { defineComponent, useRouter } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseBackButton } from '~/components/base'
import { MobileCurrentlyViewing } from '~/components/blocks/block-currently-viewing'
import style from './mobile-product-top-line.scss?module'

const cn = useClassNames('mobile-product-top-line', style)

export const MobileProductTopLine = injectStyles(
  style,
  defineComponent({
    name: 'MobileProductTopLine',
    props: {},
    setup: () => {
      const router = useRouter()
      const goBack = () => {
        router.back()
      }
      return () => (
        <div class={cn()}>
          <BaseBackButton class={cn('back')} onClick={goBack} />
          <MobileCurrentlyViewing class={cn('viewing')} />
        </div>
      )
    }
  })
)
