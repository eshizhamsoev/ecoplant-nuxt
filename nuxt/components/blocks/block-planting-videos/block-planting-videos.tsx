import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseYoutubePosterModal } from '~/components/base/base-youtube-poster-modal/base-youtube-poster-modal'
import { BaseIcon } from '~/components/base'
import { cnThemeWrapper } from '~/components/theme/theme-wrapper'
import style from './block-planting-videos.scss?module'

const cn = useClassNames('block-planting-videos', style)

const VIDEOS = [
  {
    video: '3CQsdIKrPXU',
    title: 'Как сажать клубнику осенью?'
  },
  {
    video: 'tIBcg8K2878',
    title: 'Как посадить виноград?',
    thumbnail: 'mqdefault'
  },
  {
    video: 'Erl-cEJaXaE',
    title: 'Как посадить малину?'
  },
  {
    video: 'kBQpNZ38T18',
    title: 'Как посадить вишню?'
  },
  {
    video: '92Ik6oP2_uA',
    title: 'Как посадить лиственницу?'
  },
  {
    video: 'EVrJ_VGfI1E',
    title: 'Как посадить яблоню?'
  },
  {
    video: '30I9Id82i5g',
    title: 'Как посадить рябину?',
    thumbnail: 'mqdefault'
  },
  {
    video: 'kF2wORz3I88',
    title: 'Как посадить лапчатку?',
    thumbnail: 'mqdefault'
  },
  {
    video: 'h33C9hB3zFA',
    title: 'Как посадить калину?',
    thumbnail: 'mqdefault'
  },
  {
    video: 'LWosXeWO5Cw',
    title: 'Как посадить бонсай?'
  },
  {
    video: 'bkRcyZmKroQ',
    title: 'Как посадить гортензию?'
  },
  {
    video: '3g3O7LeEf3g',
    title: 'Как посадить розы?'
  },
  {
    video: 'HcKDRgzbTOo',
    title: 'Как посадить иву?'
  },
  {
    video: 'BjF_lQXAS3I',
    title: 'Как посадить дёрен? '
  },
  {
    video: 'wUbPcki-wKo',
    title: 'Как посадить березу?'
  },
  {
    video: 'vwroWhXwJaI',
    title: 'Как посадить каштан?'
  },
  {
    video: '7ncbY3Wl0Zg',
    title: 'Как посадить клен?'
  },
  {
    video: 'nY9YdTrtENE',
    title: 'Как посадить живую изгородь из ели?'
  },
  {
    video: '9uQ3BN-AXVE',
    title: 'Как посадить кустарник?'
  },
  {
    video: 'jF0NfygBuNA',
    title: 'Как посадить живую изгородь из туи?'
  },
  {
    video: 'Ea4DUjjmXf8',
    title: 'Как посадить ель?'
  },
  {
    video: 'wr6qt31nw70',
    title: 'Как посадить сосну горную?'
  },
  {
    video: 'adenQ6gmzSY',
    title: 'Как посадить тую?'
  },
  {
    video: '9G4NdO3iGnQ',
    title: 'Как посадить сосну?'
  },
  {
    video: 'U_L3QzA-0K0',
    title: 'Как посадить можжевельник?'
  },
  {
    video: 'CEMYIBm320s',
    title: 'Как посадить пихту?'
  },
  {
    video: 'yE_wMkM9Dn0',
    title: 'Как посадить кедр?'
  }
]

export const BlockPlantingVideos = injectStyles(
  style,
  defineComponent({
    name: 'BlockPlantingVideos',
    props: {},
    setup: () => {
      return () => (
        <div class={[cn(), cnThemeWrapper()]}>
          <div class={cn('heading')}>Как правильно посадить</div>
          <div class={cn('videos')}>
            {VIDEOS.map((video) => (
              <div class={cn('item')}>
                <BaseYoutubePosterModal videoId={video.video}>
                  <div class={cn('poster-container')}>
                    <img
                      alt={video.title}
                      width={1280}
                      height={720}
                      class={cn('poster')}
                      loading="lazy"
                      src={`https://img.youtube.com/vi/${video.video}/${
                        video.thumbnail ?? 'hq720'
                      }.jpg`}
                    />
                    <BaseIcon
                      name="youtube"
                      width={120}
                      height={100}
                      class={cn('icon')}
                    />
                  </div>
                </BaseYoutubePosterModal>
                <div class={cn('video-title')}>{video.title}</div>
              </div>
            ))}
          </div>
        </div>
      )
    }
  })
)
