export type Image = {
  small: string
  large: string
}

export const blockDocumentsProps = Object.freeze({
  images: {
    type: Array as () => Image[],
    required: true as true
  }
})
