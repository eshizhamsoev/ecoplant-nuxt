import { computed, defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { blockDocumentsProps } from '~/components/blocks/block-documents/props'
import { BasePopupGallery } from '~/components/base/base-popup-gallery'
import { usePopupGallery } from '~/components/base/base-popup-gallery/usePopupGallery'
import style from './mobile-block-documents.scss?module'

const cn = useClassNames('mobile-block-documents', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  wrapper: cn('wrapper'),
  panel: cn('panel'),
  panelImage: cn('panel-image'),
  mainImageWrapper: cn('main-image-wrapper'),
  mainImage: cn('main-image')
}

export const MobileBlockDocuments = injectStyles(
  style,
  defineComponent({
    name: 'MobileBlockDocuments',
    props: blockDocumentsProps,
    setup(props) {
      const index = ref(0)

      const {
        open: openPopupGallery,
        close: closePopupGallery,
        index: popupIndex
      } = usePopupGallery()

      const onMainImageClick = () => openPopupGallery(index.value)

      const mainImage = computed(() => props.images[index.value])
      return () => (
        <div>
          <div class={classes.heading}>Документы</div>
          <div class={classes.wrapper}>
            <div class={classes.panel}>
              {props.images.map((image, i) => (
                <div class={cn('panel-item', { active: i === index.value })}>
                  <img
                    src={image.small}
                    onClick={() => (index.value = i)}
                    class={classes.panelImage}
                  />
                </div>
              ))}
            </div>
            <div class={classes.mainImageWrapper}>
              <img
                src={mainImage.value.large}
                class={classes.mainImage}
                alt=""
                onClick={onMainImageClick}
              />
            </div>
          </div>
          <BasePopupGallery
            images={props.images}
            index={popupIndex.value}
            onClose={closePopupGallery}
            showThumbnails={false}
          />
        </div>
      )
    }
  })
)
