import { computed, defineComponent } from '@nuxtjs/composition-api'
import icon from '../resources/icon.svg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseImageGrid } from '~/components/base/base-image-grid'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  Indent,
  VerticalAlign
} from '~/components/pattern/pattern-icon-plus'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { documents } from '~/graphql/queries'
import style from './desktop-block-documents.scss?module'

const cn = useClassNames('block-documents', style)

const classes = {
  main: cn(),
  heading: [
    cn('heading'),
    cnPatternIconPlus({ 'vertical-align': VerticalAlign.bottom })
  ],
  icon: cnPatternIconPlusIcon({ indent: Indent.m }),
  grid: cn('grid'),
  item: cn('item')
}

export const DesktopBlockDocuments = injectStyles(
  style,
  defineComponent({
    name: 'DesktopBlockDocuments',
    setup: () => {
      const { result } = useGqlQuery(documents, {})
      const images = computed(
        () => result.value?.documents.map((item) => item.image) || []
      )
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>
            <img src={icon} alt="" class={classes.icon} loading={'lazy'} />
            <span>Документы</span>
          </div>
          <BaseImageGrid
            images={images.value}
            xs={2}
            md={4}
            itemClass={classes.item}
            class={classes.grid}
            popupAvailable={true}
          />
        </div>
      )
    }
  })
)
