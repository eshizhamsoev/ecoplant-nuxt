import {
  computed,
  defineComponent,
  PropType,
  ref,
  watch
} from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ProductActions } from '~/components/blocks/block-category/products-panel/product-actions'
import { ListMenu } from '~/components/blocks/list-menu'
import { ServiceInfo } from '~/components/blocks/shared-components/service-info/service-info'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import { ServiceImage } from '~/_generated/types'
import { ProductSale } from '~/components/blocks/shared-components/product-sale'
import style from './block-service-panel.scss?module'

const cn = useClassNames('block-service-panel', style)

type Service = {
  __typename?: string
  id: string
  name: string
  images: ServiceImage[]
}

export const ServicePanel = injectStyles(
  style,
  defineComponent({
    name: 'BlockServicePanel',
    props: {
      services: {
        type: Array as PropType<Service[]>,
        required: true
      }
    },
    setup(props, { slots }) {
      const services = computed(() => {
        return props.services
      })
      const serviceId = ref<string | null>(services.value[0].id)

      watch(
        () => props.services,
        () => {
          serviceId.value = props.services[0].id
        }
      )

      const onSelectMenuItem = (value: string) => {
        serviceId.value = value
      }

      const service = computed(() =>
        services.value.find((service) => service.id === serviceId.value)
      )

      const modalOpener = useModalOpener()

      const openDescriptionModal = () => {
        if (serviceId.value) {
          modalOpener({ name: 'service', serviceId: serviceId.value })
        }
      }

      const hasDescription = computed(() => {
        return service.value?.__typename === 'Service'
      })

      const saleImage = computed(() => {
        if (!service.value) {
          return
        }
        if (service.value.images.length === 0) {
          return
        }
        return service.value.images[service.value.images.length - 1].large
      })

      return () =>
        services.value &&
        serviceId.value && (
          <div class={cn()}>
            <div class={cn('grid')}>
              <div class={cn('slot')}>{slots.default && slots.default()}</div>
              {service.value && (
                <ProductActions
                  productName={service.value.name}
                  showDescriptionButton={hasDescription.value}
                  onShowDescriptionClick={openDescriptionModal}
                  class={cn('actions')}
                />
              )}
              <ListMenu
                value={serviceId.value}
                onInput={onSelectMenuItem}
                items={services.value}
                class={cn('navigation')}
              />
              {service.value && (
                <ServiceInfo service={service.value} class={cn('info')} />
              )}
            </div>

            {service.value && saleImage.value && (
              <ProductSale
                name={service.value.name}
                image={saleImage.value}
                class={cn('sale')}
              />
            )}
          </div>
        )
    }
  })
)
