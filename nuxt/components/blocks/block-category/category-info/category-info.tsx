import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  Indent,
  VerticalAlign
} from '~/components/pattern/pattern-icon-plus'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './category-info.scss?module'

const cn = useClassNames('category-info', style)

const classes = {
  main: [cn(), cnPatternIconPlus({ 'vertical-align': VerticalAlign.bottom })],
  image: [cn('image'), cnPatternIconPlusIcon({ indent: Indent.l })],
  text: [cn('text')]
}

type Info = {
  name: string
  image: string
}

export const CategoryInfo = injectStyles(
  style,
  defineComponent({
    props: {
      category: {
        type: Object as () => Info,
        required: true
      }
    },
    setup: (props) => {
      const icon = computed(() => props.category.image)
      return () => (
        <div class={classes.main}>
          {icon.value ? (
            <img
              src={icon.value}
              alt={props.category.name}
              class={classes.image}
              loading={'lazy'}
            />
          ) : null}
          <div class={classes.text}>{props.category.name}</div>
        </div>
      )
    }
  })
)
