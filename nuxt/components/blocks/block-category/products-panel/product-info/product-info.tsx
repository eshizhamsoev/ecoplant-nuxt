import {
  computed,
  defineComponent,
  inject,
  PropType
} from '@nuxtjs/composition-api'
import { ProductImages } from '@/components/blocks/shared-components/product-images'
import { ProductPrices } from '~/components/blocks/shared-components/product-prices'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { Product } from '~/_generated/types'
import { BaseButton } from '~/components/base'
import { cartSymbol } from '~/data/providers/cart/symbol'
import { ProductVarieties } from '~/components/blocks/shared-components/product-varieties/product-varieties'
import style from './product-info.scss?module'

const cn = useClassNames('product-info', style)

const classes = {
  main: cn(),
  right: cn('right'),
  sameSize: cn('same-size')
}

export const ProductInfo = injectStyles(
  style,
  defineComponent({
    name: 'ProductInfo',
    props: {
      product: {
        type: Object as PropType<
          Pick<Product, 'images' | 'name' | 'id' | 'prices' | 'isSameSize'> & {
            varieties: { id: string; name: string }[]
          }
        >,
        required: true
      }
    },
    setup: (props, { root }) => {
      const product = computed(() => props.product)
      const showButton = computed(() => root.$accessor.showExpandedInformation)
      const cart = inject(cartSymbol)
      const inCart = computed(() => cart?.productInCart(product.value.id) || 0)
      return () => {
        if (!product.value) {
          return
        }
        return (
          <div class={cn()}>
            {product.value.images && product.value.images.length > 0 && (
              <ProductImages
                images={product.value.images}
                class={cn('images')}
              />
            )}
            {product.value.prices && (
              <div class={classes.right}>
                <ProductPrices product={product.value} class={cn('prices')} />

                {product.value.isSameSize && (
                  <div class={classes.sameSize}>Размер не имеет значения</div>
                )}

                {showButton.value && (
                  <BaseButton
                    to="/checkout"
                    size="m"
                    class={cn('button')}
                    disabled={inCart.value === 0}
                  >
                    Купить
                  </BaseButton>
                )}
                <ProductVarieties
                  product={product.value.varieties}
                  class={cn('varieties')}
                />
              </div>
            )}
          </div>
        )
      }
    }
  })
)
