import { defineComponent } from '@nuxtjs/composition-api'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  Indent,
  VerticalAlign
} from '@/components/pattern/pattern-icon-plus'

import { BaseIcon } from '@/components/base/base-icon'
import { BaseButton } from '@/components/base/base-button'
import { injectStyles } from '~/support/utils/injectStyle'
import { useClassNames } from '~/support/utils/bem-classnames'
import { Visual } from '~/components/theme/theme-button'
import { GTM_EVENTS, useReachGoal } from '~/support/hooks/useReachGoal'
import style from './product-chat.scss?module'

const cn = useClassNames('product-chat', style)
const classes = {
  main: [cn(), cnPatternIconPlus({ 'vertical-align': VerticalAlign.center })],
  icon: [cn('icon'), cnPatternIconPlusIcon({ indent: Indent.m })],
  text: cn('text')
}

export default injectStyles(
  style,
  defineComponent({
    setup() {
      const reachGoal = useReachGoal()
      return () => (
        <BaseButton
          class={classes.main}
          size="m"
          visual={Visual.outline}
          onClick={() => reachGoal(GTM_EVENTS.openChat)}
        >
          <BaseIcon name="chat" class={classes.icon} width={16} />
          <span class={classes.text}>
            начать чат <br />
            с&nbsp;дендрологом
          </span>
        </BaseButton>
      )
    }
  })
)
