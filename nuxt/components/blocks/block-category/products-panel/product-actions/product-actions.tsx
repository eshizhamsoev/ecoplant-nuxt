import { defineComponent } from '@nuxtjs/composition-api'
// import { ProductCall } from './product-call'
// import { ProductChat } from './product-chat'
// import { ProductWhatsapp } from './product-whatsapp'
import { ProductDescription } from './product-description'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ProductDelivery } from '~/components/blocks/block-category/products-panel/product-actions/product-delivery/product-delivery'
import { ProductPrice } from '~/components/blocks/block-category/products-panel/product-actions/product-price/product-price'
import { ProductPlanting } from '~/components/blocks/block-category/products-panel/product-actions/product-planting/product-planting'
import style from './product-actions.scss?module'

const cn = useClassNames('product-actions', style)
const classes = {
  main: cn(),
  item: cn('item')
}

export const ProductActions = injectStyles(
  style,
  defineComponent({
    name: 'ProductActions',
    props: {
      productName: {
        type: String,
        required: true
      },
      showDescriptionButton: {
        type: Boolean,
        required: true
      }
    },
    emits: ['showDescriptionClick'],
    setup(props, { emit }) {
      return () => (
        <div class={classes.main}>
          <ProductDelivery class={classes.item} />
          <ProductPrice class={classes.item} />
          <ProductPlanting class={classes.item} />
          {props.showDescriptionButton && (
            <ProductDescription
              class={cn('item', { type: 'description' })}
              onClick={() => {
                emit('showDescriptionClick')
              }}
            />
          )}
        </div>
      )
    }
  })
)
