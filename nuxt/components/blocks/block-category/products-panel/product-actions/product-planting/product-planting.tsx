import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton, BaseIcon } from '~/components/base'
import { Visual } from '~/components/theme/theme-button'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  Indent,
  VerticalAlign
} from '~/components/pattern/pattern-icon-plus'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './product-planting.scss?module'

const cn = useClassNames('product-planting', style)
const classes = {
  main: [cn(), cnPatternIconPlus({ 'vertical-align': VerticalAlign.center })],
  icon: [cn('icon'), cnPatternIconPlusIcon({ indent: Indent.m })],
  text: cn('text')
}

export const ProductPlanting = injectStyles(
  style,
  defineComponent({
    name: 'ProductPlanting',
    props: {},
    setup: () => {
      const modalOpener = useModalOpener()
      const openPlantingModal = () =>
        modalOpener({
          name: 'planting'
        })
      return () => (
        <div>
          <BaseButton
            class={classes.main}
            size="m"
            visual={Visual.outline}
            // to={{
            //   path: root.$route.path,
            //   query: { tabs: 'BlockPlanting' },
            //   hash: 'tabs-BlockPlanting'
            // }}
            onClick={openPlantingModal}
          >
            <BaseIcon name="planting/plant" class={classes.icon} width={24} />
            <span class={classes.text}>Посадка</span>
          </BaseButton>
        </div>
      )
    }
  })
)
