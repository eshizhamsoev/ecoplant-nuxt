import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton, BaseIcon } from '~/components/base'
import { Visual } from '~/components/theme/theme-button'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  Indent,
  VerticalAlign
} from '~/components/pattern/pattern-icon-plus'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './product-price.scss?module'

const cn = useClassNames('product-price', style)
const classes = {
  main: [cn(), cnPatternIconPlus({ 'vertical-align': VerticalAlign.center })],
  icon: [cn('icon'), cnPatternIconPlusIcon({ indent: Indent.m })],
  text: cn('text')
}

export const ProductPrice = injectStyles(
  style,
  defineComponent({
    name: 'ProductPrice',
    props: {},
    setup: () => {
      const modalOpener = useModalOpener()
      const openPriceListRequestModal = () =>
        modalOpener({
          name: 'price-list-request'
        })
      return () => (
        <BaseButton
          class={classes.main}
          size="m"
          visual={Visual.outline}
          onClick={openPriceListRequestModal}
        >
          <BaseIcon
            name="heading-top-line/catalog"
            class={classes.icon}
            width={24}
          />
          <span class={classes.text}>
            Прайс-лист <br />
            Весь ассортимент
          </span>
        </BaseButton>
      )
    }
  })
)
