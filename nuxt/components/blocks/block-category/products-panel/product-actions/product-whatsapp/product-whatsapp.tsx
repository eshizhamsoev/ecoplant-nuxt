import { defineComponent } from '@nuxtjs/composition-api'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  Indent,
  VerticalAlign
} from '@/components/pattern/pattern-icon-plus'

import { BaseIcon } from '@/components/base/base-icon'
import { BaseButton } from '@/components/base/base-button'
import { injectStyles } from '~/support/utils/injectStyle'
import { useClassNames } from '~/support/utils/bem-classnames'
import { Visual } from '~/components/theme/theme-button'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './product-whatsapp.scss?module'

const cn = useClassNames('product-whatsapp', style)
const classes = {
  main: [cn(), cnPatternIconPlus({ 'vertical-align': VerticalAlign.center })],
  icon: [cn('icon'), cnPatternIconPlusIcon({ indent: Indent.m })],
  text: cn('text')
}

export const ProductWhatsapp = injectStyles(
  style,
  defineComponent({
    name: 'ProductWhatsapp',
    props: {
      productName: {
        type: String,
        required: true
      }
    },
    setup(props) {
      const modalOpener = useModalOpener()

      const openModal = () =>
        modalOpener({
          name: 'video-call',
          source: `Позвонить нам. Выбранный товар: ${props.productName}`
        })
      return () => (
        <BaseButton
          class={classes.main}
          size="m"
          visual={Visual.outline}
          onClick={openModal}
        >
          <BaseIcon name="whatsapp" class={classes.icon} width={16} />
          <span class={classes.text}>
            видео&nbsp;звонок <br />
            по WhatsApp
          </span>
        </BaseButton>
      )
    }
  })
)
