import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton, BaseIcon } from '~/components/base'
import { Visual } from '~/components/theme/theme-button'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  Indent,
  VerticalAlign
} from '~/components/pattern/pattern-icon-plus'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './product-delivery.scss?module'

const cn = useClassNames('product-delivery', style)
const classes = {
  main: [cn(), cnPatternIconPlus({ 'vertical-align': VerticalAlign.center })],
  icon: [cn('icon'), cnPatternIconPlusIcon({ indent: Indent.m })],
  text: cn('text')
}

export const ProductDelivery = injectStyles(
  style,
  defineComponent({
    name: 'ProductDelivery',
    props: {},
    setup: () => {
      const modalOpener = useModalOpener()
      const openShippingModal = () =>
        modalOpener({
          name: 'shipping'
        })
      return () => (
        <div>
          <BaseButton
            class={classes.main}
            size="m"
            visual={Visual.outline}
            // to={{
            //   path: root.$route.path,
            //   query: { tabs: 'BlockShipping' },
            //   hash: 'tabs-BlockShipping'
            // }}
            onClick={openShippingModal}
          >
            <BaseIcon
              name="heading-top-line/delivery"
              class={classes.icon}
              width={28}
            />
            <span class={classes.text}>Доставка</span>
          </BaseButton>
        </div>
      )
    }
  })
)
