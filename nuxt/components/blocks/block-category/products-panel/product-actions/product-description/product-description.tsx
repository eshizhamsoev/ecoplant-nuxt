import { defineComponent } from '@nuxtjs/composition-api'
import { BaseButton } from '@/components/base/base-button'
import { injectStyles } from '~/support/utils/injectStyle'
import { useClassNames } from '~/support/utils/bem-classnames'
import { Visual } from '~/components/theme/theme-button'
import style from './product-description.scss?module'

const cn = useClassNames('product-description', style)
const classes = {
  main: cn()
}

export const ProductDescription = injectStyles(
  style,
  defineComponent({
    name: 'ProductDescription',
    setup(props, { emit }) {
      return () => (
        <BaseButton
          class={classes.main}
          size="m"
          visual={Visual.outline}
          onClick={() => emit('click')}
        >
          Открыть подробное описание
        </BaseButton>
      )
    }
  })
)
