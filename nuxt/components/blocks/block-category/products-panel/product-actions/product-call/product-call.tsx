import { defineComponent } from '@nuxtjs/composition-api'
import {
  cnPatternIconPlus,
  cnPatternIconPlusIcon,
  Indent,
  VerticalAlign
} from '@/components/pattern/pattern-icon-plus'

import { BaseIcon } from '@/components/base/base-icon'
import { BaseButton } from '@/components/base/base-button'
import { injectStyles } from '~/support/utils/injectStyle'
import { useClassNames } from '~/support/utils/bem-classnames'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './product-call.scss?module'

const cn = useClassNames('product-call', style)
const classes = {
  main: [cn(), cnPatternIconPlus({ 'vertical-align': VerticalAlign.center })],
  icon: [cn('icon'), cnPatternIconPlusIcon({ indent: Indent.m })]
}

export const ProductCall = injectStyles(
  style,
  defineComponent({
    name: 'ProductCall',
    components: { BaseIcon, BaseButton },
    props: {
      productName: {
        type: String,
        required: true
      }
    },
    setup(props) {
      const modalOpener = useModalOpener()
      const openModal = () =>
        modalOpener({
          name: 'callback',
          source: `Позвонить нам. Выбранный товар: ${props.productName}`
        })

      return () => (
        <BaseButton class={classes.main} size="m" onClick={openModal}>
          <BaseIcon name="phone" class={classes.icon} width={13} />
          <span>позвонить нам</span>
        </BaseButton>
      )
    }
  })
)
