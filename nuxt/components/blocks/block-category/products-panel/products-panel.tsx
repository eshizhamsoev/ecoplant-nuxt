import { computed, defineComponent, ref, watch } from '@nuxtjs/composition-api'
import { ListMenu } from '../../list-menu'
import { ProductActions } from './product-actions'
import { ProductInfo } from './product-info'
import { ProductSale } from '@/components/blocks/shared-components/product-sale'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import {
  Product,
  ProductInfoQuery,
  ProductInfoQueryVariables
} from '~/_generated/types'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { productQuery } from '~/graphql/queries'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './products-panel.scss?module'

const cn = useClassNames('products-panel', style)
const classes = {
  main: cn(),
  panel: cn('panel'),
  slot: cn('slot'),
  actions: cn('actions'),
  navigation: cn('navigation'),
  productInfo: cn('product-info'),
  sale: cn('sale')
}

export const ProductsPanel = injectStyles(
  style,
  defineComponent({
    name: 'ProductsPanel',
    props: {
      products: {
        type: Array as () => Pick<Product, 'id' | 'name'>[],
        required: true
      }
    },
    setup(props, { slots }) {
      const productId = ref(props.products[0].id)
      const modalOpener = useModalOpener()
      const queryVars = computed(() => ({
        id: productId.value
      }))

      watch(
        () => props.products,
        () => {
          productId.value = props.products[0].id
        }
      )

      const { result } = useGqlQuery<
        ProductInfoQuery,
        Omit<ProductInfoQueryVariables, 'site'>
      >(productQuery, queryVars)

      const items = computed(() => props.products)

      const onSelectMenuItem = (value: string) => {
        productId.value = value
      }

      const openProductDescription = () =>
        modalOpener({
          name: 'product-description',
          productId: productId.value
        })

      const product = computed(() => result.value && result.value.product)
      return () => (
        <div class={classes.main}>
          <div class={classes.panel}>
            <div class={classes.slot}>{slots.default && slots.default()}</div>
            {product.value && (
              <ProductActions
                productName={product.value.name}
                showDescriptionButton={product.value.hasAttributes}
                onShowDescriptionClick={openProductDescription}
                class={classes.actions}
              />
            )}
            <ListMenu
              value={productId.value}
              onInput={onSelectMenuItem}
              items={items.value}
              class={classes.navigation}
            />
            {product.value && (
              <ProductInfo
                key={product.value.id}
                product={product.value}
                class={classes.productInfo}
              />
            )}
          </div>
          {product.value && (
            <ProductSale
              name={product.value.name}
              image={product.value.image}
              size={product.value.salePrice?.size}
              price={
                (product.value.showSalePrice && product.value.salePrice) || null
              }
              class={classes.sale}
            />
          )}
        </div>
      )
    }
  })
)
