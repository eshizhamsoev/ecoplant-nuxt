import chunk from 'lodash/chunk'
import { computed, defineComponent, toRefs } from '@nuxtjs/composition-api'
import { ProductsPanel } from './products-panel'
import { cnThemeWrapper } from '~/components/theme/theme-wrapper'
import { CategoryInfo } from '~/components/blocks/block-category/category-info'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { CategoryIdConstructor } from '~/common-types/catalog'
import { ServicePanel } from '~/components/blocks/block-service-panel'
import { getCategoryById } from '~/queries/graphql/get-category-by-id'
import style from './block-category.scss?module'

const cn = useClassNames('block-category', style)

const classes = {
  main: [cn(), cnThemeWrapper()]
}

export const BlockCategory = injectStyles(
  style,
  defineComponent({
    name: 'BlockCategory',
    props: {
      categoryId: {
        type: CategoryIdConstructor,
        required: true
      }
    },
    setup(props) {
      const { categoryId } = toRefs(props)

      const { category } = getCategoryById(categoryId)

      const isService = computed(() => {
        return (
          category.value?.__typename === 'ServiceCategory' ||
          category.value?.__typename === 'ExclusiveCategory'
        )
      })

      const items = computed(() => {
        if (!category.value) {
          return
        }

        switch (category.value.__typename) {
          case 'ServiceCategory':
            return category.value.services
          case 'ExclusiveCategory':
            return category.value.exclusives
          default:
            // @ts-ignore
            return category.value?.products || []
        }
      })

      const itemGroups = computed(() => chunk(items.value, 18))

      return () =>
        category.value && (
          <div class={classes.main}>
            {itemGroups.value.map((products, index) =>
              isService.value ? (
                // @ts-ignore
                <ServicePanel services={products}>
                  {index === 0 && category.value && (
                    <CategoryInfo category={category.value} />
                  )}
                </ServicePanel>
              ) : (
                // @ts-ignore
                <ProductsPanel products={products}>
                  {index === 0 && category.value && (
                    <CategoryInfo category={category.value} />
                  )}
                </ProductsPanel>
              )
            )}
          </div>
        )
    }
  })
)
