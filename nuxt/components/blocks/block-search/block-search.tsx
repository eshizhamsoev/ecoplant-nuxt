import { defineComponent, ref, useContext } from '@nuxtjs/composition-api'
import { onMounted } from '@vue/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseButton, BaseIcon } from '~/components/base'
import { getProductsByName } from '~/queries/graphql/get-products-by-name'
import { BaseLink } from '~/components/base/base-link'
import style from './block-search.scss?module'

const cn = useClassNames('block-search', style)

export const BlockSearch = injectStyles(
  style,
  defineComponent({
    name: 'BlockMobileSearch',
    props: {},
    setup: (_, { emit, refs }) => {
      const { $device } = useContext()

      const inputValue = ref<string>('')

      const needToSend = ref<boolean>(false)

      const clearInput = () => {
        needToSend.value = false
        inputValue.value = ''
        result.value = null
      }

      const closeSearch = () => {
        emit('close')
      }

      const { result, onResult } = getProductsByName(inputValue, needToSend)
      const emptySearch = ref<boolean>(false)
      const debounceInit = () => {
        let timeoutId: number | null = null

        return () => {
          if (!(inputValue.value.length > 1)) {
            result.value = null
          }
          if (timeoutId) {
            window.clearTimeout(timeoutId)
          }
          emptySearch.value = false
          needToSend.value = false
          timeoutId = window.setTimeout(() => {
            needToSend.value = inputValue.value.length > 1
          }, 300)
        }
      }

      onResult(() => {
        emptySearch.value = result.value
          ? !(result.value?.search.length > 0)
          : false
      })

      const submit = (e: Event) => {
        e.preventDefault()
      }

      const inputDebounce = debounceInit()

      const inputHandler = () => {
        if (refs.input instanceof HTMLInputElement) {
          inputValue.value = refs.input.value
        }
        inputDebounce()
      }

      onMounted(() => {
        if (refs.input instanceof HTMLInputElement) {
          refs.input.focus()
        }
      })

      return () => (
        <div class={cn()}>
          <form id={'search-form'} onsubmit={submit}>
            <div class={cn('form-wrapper', { desktop: $device.isDesktop })}>
              <input
                type={'text'}
                onInput={inputHandler}
                class={cn('input')}
                ref="input"
                placeholder={'Поиск'}
                value={inputValue.value}
              />
              {$device.isMobileOrTablet && inputValue.value.length > 0 && (
                <button
                  type={'button'}
                  class={cn('clear')}
                  onClick={clearInput}
                >
                  <BaseIcon
                    name={'clear-search'}
                    width={10}
                    class={'clear-icon'}
                  />
                </button>
              )}
              {$device.isMobileOrTablet ? (
                <button
                  onClick={closeSearch}
                  class={cn('mobile-search-button')}
                  type={'button'}
                >
                  <BaseIcon
                    name={'arrow-left'}
                    width={20}
                    class={cn('close-icon')}
                  />
                </button>
              ) : (
                <BaseButton
                  size={'s'}
                  type="button"
                  onClick={closeSearch}
                  class={cn('desktop-button')}
                >
                  <BaseIcon
                    name={'arrow-left'}
                    width={25}
                    class={cn('close-icon')}
                  />
                  Закрыть
                </BaseButton>
              )}
            </div>
          </form>
          <div class={cn('results-wrapper')}>
            {result.value && result.value.search.length > 0
              ? result.value.search.map((product) => (
                  <BaseLink
                    to={
                      $device.isDesktop
                        ? {
                            path: `/category-products/${product.id}`,
                            hash: '#BlockContent1'
                          }
                        : `/products/${product.id}`
                    }
                    class={cn('search-result')}
                  >
                    <div class={cn('search-result-left')}>
                      <BaseIcon
                        name={'search'}
                        width={14}
                        class={cn('search-result-icon')}
                      />
                      <span class={cn('search-result-text')}>
                        {product.name}
                      </span>
                    </div>
                    <BaseIcon
                      class={cn('search-result-chevron')}
                      name={'chevron-left'}
                      width={6}
                    />
                  </BaseLink>
                ))
              : inputValue.value.length > 1 &&
                emptySearch.value && (
                  <div class={cn('search-result')}>Ничего не найдено</div>
                )}
          </div>
        </div>
      )
    }
  })
)
