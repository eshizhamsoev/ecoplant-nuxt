import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BlackFridayPlantGrid } from '~/components/blocks/block-black-friday/components/black-friday-plant-grid/black-friday-plant-grid'
import { BlackFridayProduct } from '~/components/blocks/block-black-friday/types/product'
import { BlackFridayProductSizes } from '~/components/blocks/block-black-friday/components/black-friday-product/black-friday-product-sizes'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { BaseBackButton } from '~/components/base'
import style from './modal-black-friday-product.scss?module'

const cn = useClassNames('modal-black-friday-product', style)

export const ModalBlackFridayProduct = injectStyles(
  style,
  defineComponent({
    name: 'ModalBlackFridayProduct',
    props: {
      product: {
        type: Object as () => BlackFridayProduct,
        required: true
      }
    },
    setup: (props, { emit }) => {
      return () => (
        <ModalWithClose class={cn()} onClose={() => emit('close')}>
          <BaseBackButton onClick={() => emit('close')} />
          <div class={cn('title')}>{props.product.info.name}</div>
          {props.product.variants.length > 0 && (
            <BlackFridayPlantGrid
              class={cn('plants')}
              plants={props.product.variants}
            />
          )}
          {props.product.sizes.length > 0 && (
            <BlackFridayProductSizes
              class={cn('sizes')}
              sizes={props.product.sizes}
              title={props.product.info.name}
            />
          )}
        </ModalWithClose>
      )
    }
  })
)
