import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { ApiFormSubmitProvider } from '~/components/action-providers/api-form-submit-provider/api-form-submit-provider'
import { FORM_TYPES } from '~/support/hooks/useForm'
import { MainForm } from '~/components/common/form/main-form'
import { GTM_EVENTS, useReachGoal } from '~/support/hooks/useReachGoal'
import { BasePhoneInput } from '~/components/base/base-phone-input'
import style from './modal-get-sale.scss?module'

const cn = useClassNames('modal-get-sale', style)

export const ModalGetSale = injectStyles(
  style,
  defineComponent({
    name: 'ModalGetSale',
    props: {},
    setup: (props, { emit }) => {
      const reachGoal = useReachGoal()
      const success = () => {
        reachGoal(GTM_EVENTS.callBack)
      }
      const fields = [
        {
          component: BasePhoneInput,
          name: 'phone',
          label: 'Телефон',
          options: {
            required: false
          }
        }
      ]
      return () => (
        <ModalWithClose class={cn()} onClose={() => emit('close')}>
          <div class={cn('heading')}>Получить скидку 70%</div>
          <div class={cn('leading')}>Скидка действует до конца недели</div>
          <ApiFormSubmitProvider formId={FORM_TYPES.PHONE} onSuccess={success}>
            <MainForm
              formOptions={{ fields }}
              buttonOptions={{ size: 'l' }}
              class={cn('form')}
            >
              <template slot="button">
                <span>Получить скидку</span>
              </template>
              <template slot="success">Спасибо за вашу заявку</template>
            </MainForm>
          </ApiFormSubmitProvider>
        </ModalWithClose>
      )
    }
  })
)
