import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './navigate-item.scss?module'

const cn = useClassNames('navigate-item', style)

const classes = {
  main: cn(),
  link: cn('link'),
  icon: cn('icon'),
  text: cn('text')
}

export const NavigateItem = injectStyles(
  style,
  defineComponent({
    name: 'NavigateItem',
    props: {
      link: {
        type: String,
        required: true
      },
      icon: {
        type: String,
        required: true
      },
      text: {
        type: String,
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          <a href={props.link} target="_blank" class={classes.link}>
            <img src={props.icon} alt="" class={classes.icon} />
            <span class={classes.text}>{props.text}</span>
          </a>
        </div>
      )
    }
  })
)
