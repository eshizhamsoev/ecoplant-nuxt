import { computed, defineComponent, PropType } from '@nuxtjs/composition-api'
import yandexNavigator from './resources/yandex-navigator.svg'
import google from './resources/google.svg'
import yandex from './resources/yandex.svg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { NavigateItem } from '~/components/modals/content/modal-navigate/navigate-item'
import style from './modal-navigate.scss?module'

const cn = useClassNames('modal-navigate', style)

const classes = {
  main: cn(),
  item: cn('item')
}

export type Point = {
  latitude: number
  longitude: number
}

export const ModalNavigate = injectStyles(
  style,
  defineComponent({
    name: 'ModalNavigate',
    props: {
      geoPoint: {
        type: Object as PropType<Point>,
        required: true
      }
    },
    setup: (props, { root }) => {
      const point = computed(() => props.geoPoint)
      const links = computed(() => ({
        yandex: `https://yandex.ru/maps/?rtext=~${point.value.latitude},${point.value.longitude}&rtt=auto`,
        google: `https://maps.google.com/maps?daddr=${point.value.latitude},${point.value.longitude}&ll=`,
        yandexNavigator: `yandexnavi://build_route_on_map?lat_to=${point.value.latitude}&lon_to=${point.value.longitude}`
      }))
      return () => (
        <div class={classes.main}>
          {root.$device.isMobileOrTablet && (
            <NavigateItem
              text="Яндекс.Навигатор"
              link={links.value.yandexNavigator}
              class={classes.item}
              icon={yandexNavigator}
            />
          )}
          <NavigateItem
            text="Google Maps"
            link={links.value.google}
            class={classes.item}
            icon={google}
          />
          <NavigateItem
            text="Яндекс.Карты"
            link={links.value.yandex}
            class={classes.item}
            icon={yandex}
          />
        </div>
      )
    }
  })
)
