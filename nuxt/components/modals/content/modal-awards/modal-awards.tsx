import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { awards } from '~/graphql/queries'
import { DesktopBlockAwards } from '~/components/blocks/block-awards/desktop'
import style from './modal-awards.scss?module'

const cn = useClassNames('modal-awards', style)

export const ModalAwards = injectStyles(
  style,
  defineComponent({
    name: 'ModalAwards',
    props: {},
    setup: (props, { emit }) => {
      const { loading } = useGqlQuery(awards, {})
      return () =>
        !loading.value && (
          <ModalWithClose class={cn()} onClose={() => emit('close')}>
            <DesktopBlockAwards />
          </ModalWithClose>
        )
    }
  })
)
