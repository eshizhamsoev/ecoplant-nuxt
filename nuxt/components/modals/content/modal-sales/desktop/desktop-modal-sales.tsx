import { defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { ModalHeadings } from '~/components/modals/layout/modal-headings'
import { BaseImageGrid } from '~/components/base'
import { DesktopSaleFormLine } from '~/components/blocks/shared-components/sale-form-line/desktop'
import { SpecialItem } from '~/common-types/server/collections/specials'
import { getSpecials } from '~/api/client/get-specials'
import style from './desktop-modal-sales.scss?module'

const cn = useClassNames('desktop-modal-sales', style)

const classes = {
  main: cn(),
  headings: cn('headings'),
  items: cn('items'),
  image: cn('image'),
  buttons: cn('buttons'),
  formHeading: cn('form-heading'),
  form: cn('form')
}

export const DesktopModalSales = injectStyles(
  style,
  defineComponent({
    name: 'DesktopModalSales',
    setup: (props, { emit }) => {
      const loaded = ref<boolean>(false)
      const images = ref<SpecialItem[]>([])
      getSpecials().then((data) => {
        images.value = data.images
        loaded.value = true
      })
      return () =>
        loaded.value && (
          <ModalWithClose onClose={() => emit('close')} class={classes.main}>
            <ModalHeadings
              heading="Скидки питомника"
              class={classes.headings}
            />
            <BaseImageGrid
              popupAvailable={false}
              class={classes.items}
              itemClass={classes.image}
              images={images.value.map(({ image }) => image.src)}
            />
            <div class={classes.formHeading}>
              Сохранить за собой скидку этой недели
            </div>
            <DesktopSaleFormLine
              class={classes.form}
              source="Сохранить скидку недели"
            >
              <template slot="button">Сохранить скидку</template>
            </DesktopSaleFormLine>
          </ModalWithClose>
        )
    }
  })
)
