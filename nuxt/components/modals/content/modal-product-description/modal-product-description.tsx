import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { productAttributesQuery } from '~/graphql/queries'
import { ProductIdConstructor } from '~/common-types/catalog'
import style from './modal-product-description.scss?module'

const cn = useClassNames('modal-product-description', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  table: cn('table'),
  loading: cn('loading')
}

export const ModalProductDescription = injectStyles(
  style,
  defineComponent({
    name: 'ModalProductDescription',
    props: {
      productId: {
        type: ProductIdConstructor,
        required: true
      }
    },
    setup: (props, { emit }) => {
      const { result, error, loading } = useGqlQuery(productAttributesQuery, {
        productId: props.productId
      })
      const attributes = computed(
        () => result.value && result.value.productAttributes
      )
      return () =>
        !loading.value && (
          <ModalWithClose class={classes.main} onClose={() => emit('close')}>
            <div class={classes.heading}>Описание товара</div>
            {error.value ? (
              <div>Произошла ошибка при загрузке данных</div>
            ) : (
              attributes.value && (
                <table class={classes.table}>
                  <tbody>
                    {attributes.value.map((attribute) => (
                      <tr>
                        <td>{attribute.name}</td>
                        <td>{attribute.text}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              )
            )}
          </ModalWithClose>
        )
    }
  })
)
