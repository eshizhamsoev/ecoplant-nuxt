import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import style from './modal-phone-us.scss?module'

const cn = useClassNames('modal-phone-us', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  phone: cn('phone')
}

export const ModalPhoneUs = injectStyles(
  style,
  defineComponent({
    name: 'ModalPhoneUs',
    props: {},
    setup: (props, ctx) => {
      return () => (
        <ModalWithClose onClose={() => ctx.emit('close')} class={classes.main}>
          <div class={classes.heading}>
            Если у вас возникли вопросы по поводу вашего заказа с доставкой,
            посадкой, документами или иные вопросы, свяжитесь пожалуйста с нами
            по телефону техподдержки.
          </div>
          <div class={classes.phone}>+7(499) 283-21-56</div>
        </ModalWithClose>
      )
    }
  })
)
