import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { BlockPlantingVideos } from '~/components/blocks/block-planting-videos'
import style from './modal-planting-videos.scss?module'

const cn = useClassNames('modal-planting-videos', style)

export const ModalPlantingVideos = injectStyles(
  style,
  defineComponent({
    name: 'ModalPlantingVideos',
    props: {},
    setup: (props, ctx) => {
      return () => (
        <ModalWithClose onClose={() => ctx.emit('close')} class={cn()}>
          <BlockPlantingVideos />
        </ModalWithClose>
      )
    }
  })
)
