import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseEmailForm } from '~/components/base/base-email-form/base-email-form'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { FORM_TYPES } from '~/support/hooks/useForm'
import style from './modal-email-back.scss?module'

const cn = useClassNames('modal-email-back', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('leading'),
  form: cn('form')
}

export const ModalEmailBack = injectStyles(
  style,
  defineComponent({
    name: 'ModalEmailBack',
    props: {
      heading: {
        type: String,
        default: 'Заполните форму'
      },
      leading: {
        type: String,
        default: 'и мы напишем вам в течение 1 минуты'
      },
      buttonText: {
        type: String,
        default: 'Отправить'
      },
      formId: {
        type: Number,
        default: FORM_TYPES.EMAIL
      },
      source: {
        type: String,
        required: true
      }
    },
    setup: (props, { emit }) => {
      return () => (
        <ModalWithClose onClose={() => emit('close')} class={classes.main}>
          {props.heading && <div class={classes.heading}>{props.heading}</div>}
          {props.leading && <div class={classes.leading}>{props.leading}</div>}
          <BaseEmailForm
            buttonOptions={{ size: 'm' }}
            source={props.source}
            buttonClass={cn('button')}
            fieldClass={cn('field')}
          >
            <template slot="button">{props.buttonText}</template>
          </BaseEmailForm>
        </ModalWithClose>
      )
    }
  })
)
