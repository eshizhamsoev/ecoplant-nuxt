import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { FORM_TYPES } from '~/support/hooks/useForm'
import { BaseIcon } from '~/components/base'
import { BasePhoneInput } from '~/components/base/base-phone-input'
import { ApiFormSubmitProvider } from '~/components/action-providers/api-form-submit-provider/api-form-submit-provider'
import { MainForm } from '~/components/common/form/main-form'
import { ModalWithTree } from '~/components/modals/layout/modal-with-tree/modal-with-tree'
import style from './mobile-modal-price-list-request.scss?module'

const cn = useClassNames('mobile-modal-price-list-request', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  headingLine: cn('heading-line'),
  headingIcon: cn('heading-icon'),
  form: cn('form'),
  button: cn('button'),
  buttonIcon: cn('button-icon')
}

export const MobileModalPriceListRequest = injectStyles(
  style,
  defineComponent({
    name: 'MobileModalPriceListRequest',
    props: {
      buttonText: {
        type: String,
        default: 'Скачать каталог/прайс'
      },
      formId: {
        type: Number,
        default: FORM_TYPES.EMAIL
      }
    },
    setup: (props, { emit }) => {
      const fields = [
        {
          component: BasePhoneInput,
          name: 'phone',
          label: 'Телефон',
          options: {
            pattern: '^\\+7 \\(\\d\\d\\d\\) \\d\\d\\d - \\d\\d - \\d\\d$',
            required: true
          }
        },
        {
          component: 'input',
          name: 'email',
          label: 'Е-mail',
          options: {
            required: true,
            type: 'email',
            placeholder: 'Ваш e-mail'
          }
        }
      ]
      return () => (
        <ModalWithTree class={cn()} onClose={() => emit('close')}>
          <div class={classes.headingLine}>
            <BaseIcon name="excel" width={48} class={classes.headingIcon} />
            <div class={classes.heading}>
              Оставьте номер и E-mail, мы вышлем вам прайс-лист
            </div>
          </div>
          <ApiFormSubmitProvider formId={FORM_TYPES.PRICE_LIST}>
            <MainForm
              formOptions={{ fields }}
              buttonOptions={{ size: 'l' }}
              class={classes.form}
              buttonClass={classes.button}
            >
              <template slot="button">
                <BaseIcon name="excel" width={22} class={classes.buttonIcon} />
                <span>Скачать каталог/прайс</span>
              </template>
            </MainForm>
          </ApiFormSubmitProvider>
        </ModalWithTree>
      )
    }
  })
)
