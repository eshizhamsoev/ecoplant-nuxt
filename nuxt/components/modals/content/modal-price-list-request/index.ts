import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const ModalPriceListRequest = UtilUniversalComponentFactory(
  'ModalPriceListRequest',
  () => import('./mobile').then((c) => c.MobileModalPriceListRequest),
  () => import('./desktop').then((c) => c.DesktopModalPriceListRequest)
)
