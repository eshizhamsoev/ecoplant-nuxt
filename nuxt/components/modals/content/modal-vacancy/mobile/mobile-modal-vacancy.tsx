import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobileVacancyForm } from '~/components/pages/vacancy-content/mobile/mobile-vacancy-form'
import { ModalHeadings } from '~/components/modals/layout/modal-headings'
import style from './mobile-modal-vacancy.scss?module'

const cn = useClassNames('mobile-modal-vacancy', style)

const classes = {
  main: cn(),
  headings: cn('headings'),
  form: cn('form')
}

export const MobileModalVacancy = injectStyles(
  style,
  defineComponent({
    name: 'MobileModalVacancy',
    props: {
      source: {
        type: String,
        default: 'Вакансия'
      }
    },
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          <ModalHeadings heading="Оставить резюме" class={classes.headings} />
          <MobileVacancyForm source={props.source} class={classes.form} />
        </div>
      )
    }
  })
)
