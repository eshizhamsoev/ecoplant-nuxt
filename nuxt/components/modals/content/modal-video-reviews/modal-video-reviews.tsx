import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { BlockVideoReviews } from '~/components/blocks/block-video-reviews'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { videoReviewsQuery } from '~/graphql/queries'
import style from './modal-video-reviews.scss?module'

const cn = useClassNames('modal-video-reviews', style)

export const ModalVideoReviews = injectStyles(
  style,
  defineComponent({
    name: 'ModalVideoReviews',
    props: {},
    setup: (props, { emit }) => {
      const { loading } = useGqlQuery(videoReviewsQuery, {})
      return () =>
        !loading.value && (
          <ModalWithClose class={cn()} onClose={() => emit('close')}>
            <BlockVideoReviews />
          </ModalWithClose>
        )
    }
  })
)
