import { defineComponent, ref } from '@nuxtjs/composition-api'
import manager from './manager.jpg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePhoneField } from '~/components/base/base-phone-field'
import { BaseLoadingButton } from '~/components/base/base-loading-button/base-loading-button'
import { FORM_TYPES, useForm } from '~/support/hooks/useForm'
import { PhoneLink } from '~/components/action-providers/phone-link'
import { GTM_EVENTS } from '~/support/hooks/useReachGoal'
import { ModalWithTree } from '~/components/modals/layout/modal-with-tree/modal-with-tree'
import { BasePolicyLink } from '~/components/base/base-policy-link/base-policy-link'
import { BaseIcon } from '~/components/base'
import style from './modal-callback.scss?module'

const cn = useClassNames('modal-callback', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leadingLine: cn('leading-line'),
  managerPhoto: cn('manager-photo'),
  leading: cn('leading'),
  form: cn('form'),
  phoneWrapper: cn('phone-wrapper'),
  policy: cn('policy'),
  buttonWrapper: cn('button-wrapper'),
  errorMessage: cn('error-message'),
  success: cn('success'),
  successText: cn('success-text'),
  successIcon: cn('success-icon')
}

const defaults = {
  heading: 'Заполните форму',
  button: 'Получить консультацию',
  policyText: 'Согласен с политикой конфиденциальности'
}

export const ModalCallback = injectStyles(
  style,
  defineComponent({
    name: 'ModalCallback',
    props: {
      goal: {
        type: String as () => GTM_EVENTS,
        default: GTM_EVENTS.callBack
      },
      source: {
        type: String,
        default: 'Обратная связь'
      },
      formType: {
        type: Number as () => FORM_TYPES,
        default: FORM_TYPES.PHONE
      }
    },
    setup: (props, { slots, emit, root }) => {
      const phone = ref('')
      const { processing, successMessage, fatalError, submit, errorFields } =
        useForm(root, props.formType, { goal: props.goal })

      const onSubmit = (e: Event) => {
        e.preventDefault()
        submit({
          phone: phone.value,
          source: props.source
        })
      }

      return () => (
        <ModalWithTree class={cn()} onClose={() => emit('close')}>
          <div class={classes.heading}>
            {slots.heading ? slots.heading() : defaults.heading}
          </div>
          <div class={classes.leadingLine}>
            <img
              src={manager}
              width={55}
              height={55}
              class={classes.managerPhoto}
            />
            <div class={classes.leading}>
              {slots.leading
                ? slots.leading()
                : 'Я перезвоню Вам за 1 минуту. Карина эксперт садового центра.'}
            </div>
          </div>
          <div class={classes.form}>
            {fatalError.value ? (
              <div class={classes.errorMessage}>
                К сожалению произошла ошибка при отправке формы, но вы можете{' '}
                <PhoneLink>нам позвонить</PhoneLink>
              </div>
            ) : successMessage.value ? (
              // <div domProps={{ innerHTML: successMessage.value }} />
              <div class={classes.success}>
                <BaseIcon
                  class={classes.successIcon}
                  name={'corona-info/check'}
                  width={40}
                />
                <span class={classes.successText}>
                  Спасибо, за заявку. В ближайшее время наши менеджеры с Вами
                  свяжутся!
                </span>
              </div>
            ) : (
              <form onSubmit={onSubmit}>
                <div class={classes.phoneWrapper}>
                  <BasePhoneField
                    // @ts-ignore
                    value={phone.value}
                    onInput={(value: string) => (phone.value = value)}
                    error={
                      errorFields.value && errorFields.value.includes('phone')
                    }
                  />
                </div>
                {/* <div class={classes.policy}> */}
                {/*  {slots.policyText ? slots.policyText() : defaults.policyText} */}
                {/* </div> */}
                <BasePolicyLink
                  class={classes.policy}
                  // @ts-ignore
                  text={
                    slots.policyText ? slots.policyText() : defaults.policyText
                  }
                />
                <div class={classes.buttonWrapper}>
                  <BaseLoadingButton
                    buttonOptions={{ size: 'l' }}
                    loading={processing.value}
                    // @ts-ignore
                    type="submit"
                  >
                    {slots.button ? slots.button() : defaults.button}
                  </BaseLoadingButton>
                </div>
              </form>
            )}
          </div>
        </ModalWithTree>
      )
    }
  })
)
