import { defineComponent } from '@nuxtjs/composition-api'
import { PlantingItems } from '~/components/modals/content/modal-planting/planting-items'
import { ModalHeadings } from '~/components/modals/layout/modal-headings'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { standardItems } from '~/components/modals/content/modal-planting/data'
import style from './modal-planting-standard.scss?module'

const cn = useClassNames('modal-planting-standard', style)

const classes = {
  main: cn(),
  items: cn('items')
}

export const ModalPlantingStandard = injectStyles(
  style,
  defineComponent({
    name: 'ModalPlantingStandard',
    props: {},
    setup: (props, { emit }) => {
      return () => (
        <ModalWithClose onClose={() => emit('close')} class={classes.main}>
          <ModalHeadings
            heading="Стандартная посадка"
            leading="30% от стоимости"
          />
          <PlantingItems items={standardItems} class={classes.items} />
        </ModalWithClose>
      )
    }
  })
)
