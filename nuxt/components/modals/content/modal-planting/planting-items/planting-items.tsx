import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import style from './planting-items.scss?module'

const cn = useClassNames('planting-items', style)

type PlantingItem = {
  icon: string
  width: number
  text: string
}

const classes = {
  main: cn(),
  item: cn('item'),
  icon: cn('icon'),
  text: cn('text')
}

export const PlantingItems = injectStyles(
  style,
  defineComponent({
    name: 'PlantingItems',
    props: {
      items: {
        type: Array as () => PlantingItem[],
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          {props.items.map((item) => (
            <div class={classes.item}>
              <BaseIcon
                name={item.icon}
                width={item.width}
                class={classes.icon}
              />
              <span class={classes.text}>{item.text}</span>
            </div>
          ))}
        </div>
      )
    }
  })
)
