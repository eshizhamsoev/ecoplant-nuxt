import { defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { ModalHeadings } from '~/components/modals/layout/modal-headings'
import { PlantingItems } from '~/components/modals/content/modal-planting/planting-items'
import {
  standardItems,
  guaranteeItems
} from '~/components/modals/content/modal-planting/data'
import { BaseRoundButton } from '~/components/base/base-round-button'
import { BaseIcon } from '~/components/base'
import style from './modal-planting-guarantee.scss?module'

const cn = useClassNames('modal-planting-guarantee', style)

const classes = {
  main: cn(),
  items: cn('items'),
  hiddenItems: cn('hidden-items'),
  sameLine: cn('same-line'),
  sameButton: cn('same-button')
}

export const ModalPlantingGuarantee = injectStyles(
  style,
  defineComponent({
    name: 'ModalPlantingGuarantee',
    props: {},
    setup: (props, { emit }) => {
      const shown = ref(false)
      const show = () => {
        shown.value = !shown.value
      }
      return () => (
        <ModalWithClose onClose={() => emit('close')} class={classes.main}>
          <ModalHeadings
            heading="Гарантийная посадка"
            leading="50% от стоимости"
          />
          <div class={classes.sameLine}>
            То же, что и при стандартной посадке
            <BaseRoundButton
              size={40}
              class={classes.sameButton}
              onClick={show}
            >
              <BaseIcon name={shown.value ? 'minus' : 'plus'} width={13} />
            </BaseRoundButton>
          </div>
          <PlantingItems
            items={standardItems}
            class={cn('hidden-items', { active: shown.value })}
          />
          <PlantingItems items={guaranteeItems} class={classes.items} />
        </ModalWithClose>
      )
    }
  })
)
