import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { DesktopBlockPlanting } from '~/components/blocks/block-planting/desktop'
import { MobileBlockPlanting } from '~/components/blocks/block-planting/mobile'
import style from './modal-planting.scss?module'

const cn = useClassNames('modal-planting', style)

export const ModalPlanting = injectStyles(
  style,
  defineComponent({
    name: 'ModalPlanting',
    props: {},
    setup: (props, { emit, root }) => {
      const close = () => emit('close')
      const modalType = computed(() => {
        return root.$device.isDesktop ? (
          <DesktopBlockPlanting />
        ) : (
          <MobileBlockPlanting />
        )
      })
      return () => (
        <ModalWithClose class={cn()} onClose={close}>
          {modalType.value}
        </ModalWithClose>
      )
    }
  })
)
