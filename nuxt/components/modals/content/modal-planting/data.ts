export const standardItems = [
  {
    text: 'Погрузо-разгрузочные работы',
    icon: 'planting/truck',
    width: 51
  },
  {
    text: 'Выемка грунта',
    icon: 'planting/wheelbarrow',
    width: 43
  },
  {
    text: 'Удобрения и стимуляторы для корневой системы',
    icon: 'planting/eco-friendly',
    width: 40
  },
  {
    text: 'Формирование приствольного круга',
    icon: 'planting/spade',
    width: 44
  },
  {
    text: 'Анкера и растяжки для фиксации',
    icon: 'planting/shield',
    width: 40
  }
]

export const guaranteeItems = [
  {
    text: 'Выезд агронома-дендролога 3-4 раза в год',
    icon: 'planting/agriculturist',
    width: 42
  },
  {
    text: 'Ввод и вывод деревьев из анабиоза',
    icon: 'planting/leaf',
    width: 41
  },
  {
    text: 'Стимуляция корневой системы',
    icon: 'planting/plant',
    width: 39
  },
  {
    text: 'Обработка от болезней и вредителей',
    icon: 'planting/analytics',
    width: 44
  },
  {
    text: 'Гарантия на растение',
    icon: 'planting/shield',
    width: 40
  }
]
