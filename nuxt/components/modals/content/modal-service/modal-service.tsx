import { defineComponent, PropType, toRefs } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ServiceContent } from '~/components/content/service-content/service-content'
import { ServiceId } from '~/common-types/catalog'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { getServiceById } from '~/queries/graphql/get-service-by-id'
import { BasePhoneForm } from '~/components/base/base-phone-form'
import manager from '~/components/modals/content/modal-callback/manager.jpg'
import style from './modal-service.scss?module'

const cn = useClassNames('modal-service', style)

export const ModalService = injectStyles(
  style,
  defineComponent({
    name: 'ModalService',
    props: {
      serviceId: {
        type: String as PropType<ServiceId>,
        required: true
      }
    },
    setup: (props, { emit }) => {
      const { serviceId } = toRefs(props)
      const { service } = getServiceById(serviceId)

      return () =>
        service.value && (
          <ModalWithClose class={cn()} onClose={() => emit('close')}>
            <div class={cn('heading')}>{service.value.name}</div>
            <img
              src={service.value.images[0].original}
              alt=""
              class={cn('main-image')}
            />
            {service.value.content.map((content) => (
              <ServiceContent content={content} />
            ))}
            <div class={cn('form-wrapper')}>
              <div class={cn('form')}>
                <div class={cn('form-heading')}>Оставить заявку</div>
                <div class={cn('form-leading-line')}>
                  <img
                    src={manager}
                    alt="Фото менеджера"
                    width={55}
                    height={55}
                    class={cn('manager-photo')}
                  />
                  <div class={cn('form-leading')}>
                    Я перезвоню Вам за 1 минуту. Карина эксперт садового центра.
                  </div>
                </div>
                <BasePhoneForm
                  class={cn('form-inner')}
                  buttonClass={cn('button')}
                  source={service.value.name}
                  buttonOptions={{ size: 'l' }}
                >
                  <template slot="button">получить консультацию</template>
                </BasePhoneForm>
              </div>
            </div>
          </ModalWithClose>
        )
    }
  })
)
