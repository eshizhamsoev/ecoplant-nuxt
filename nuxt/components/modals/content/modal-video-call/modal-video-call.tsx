import { defineComponent } from '@nuxtjs/composition-api'
import managerImage from './resources/person.png'
// @ts-ignore
import managerWebpImage from './resources/person.png?webp'
import phoneImage from './resources/phone.png'
// @ts-ignore
import phoneWebpImage from './resources/phone.png?webp'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseWebpPicture } from '~/components/base/base-webp-picture'
import { BasePhoneForm } from '~/components/base/base-phone-form'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { GTM_EVENTS } from '~/support/hooks/useReachGoal'
import { Color } from '~/components/theme/theme-button'
import style from './modal-video-call.scss?module'

const cn = useClassNames('modal-video-call', style)

const classes = {
  main: cn(),
  wrapper: cn('wrapper'),
  photo: cn('photo'),
  info: cn('info'),
  heading: cn('heading'),
  leading: cn('leading'),
  formLine: cn('form-line'),
  formWrapper: cn('form-wrapper'),
  form: cn('form'),
  phone: cn('phone'),
  button: cn('button')
}

export const ModalVideoCall = injectStyles(
  style,
  defineComponent({
    name: 'ModalVideoCall',
    props: {},
    setup: (props, { emit }) => {
      return () => (
        <ModalWithClose class={classes.main} onClose={() => emit('close')}>
          <div class={classes.wrapper}>
            <div class={classes.photo}>
              <BaseWebpPicture
                image={managerImage}
                webpImage={managerWebpImage}
              />
            </div>
            <div class={classes.info}>
              <div class={classes.heading}>
                Покажем наш питомник растений онлайн по WhatsApp или FaceTime
              </div>
              <div class={classes.formLine}>
                <BaseWebpPicture
                  image={phoneImage}
                  webpImage={phoneWebpImage}
                />
                <div class={classes.formWrapper}>
                  <div class={classes.leading}>
                    Наш менеджер свяжется с вами по видеосвязи и покажет все
                    интересующие наименования в режиме реального времени.
                  </div>
                  <BasePhoneForm
                    goal={GTM_EVENTS.videoCall}
                    source="Видеозвонок"
                    buttonOptions={{ size: 'l', color: Color.yellow }}
                    mainClass={classes.form}
                    phoneClass={classes.phone}
                    buttonClass={classes.button}
                  >
                    <template slot="button">Заказать видеозвонок</template>
                  </BasePhoneForm>
                </div>
              </div>
            </div>
          </div>
        </ModalWithClose>
      )
    }
  })
)
