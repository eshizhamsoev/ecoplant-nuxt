import { defineComponent } from '@nuxtjs/composition-api'
import { onMounted } from '@vue/composition-api' // We import this staff from this shit, because youtube provide to import to this component from this package
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalCloseButton } from '~/components/modals/layout/modal-close-button/modal-close-button'
import style from './modal-youtube.scss?module'

const cn = useClassNames('modal-youtube', style)

export const ModalYoutube = injectStyles(
  style,
  defineComponent({
    name: 'ModalYoutube',
    props: {
      code: {
        type: String,
        required: true
      }
    },
    setup: (props, { emit, refs }) => {
      onMounted(() => {
        if (refs.player && 'player' in refs.player) {
          // @ts-ignore
          refs.player.player.playVideo()
        }
      })
      return () => (
        <div class={cn()}>
          <ModalCloseButton
            class={cn('button')}
            onClick={() => emit('close')}
          />
          <youtube class={cn('iframe')} videoId={props.code} ref="player" />
        </div>
      )
    }
  })
)
