import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePhoneForm } from '~/components/base/base-phone-form'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { Color } from '~/components/theme/theme-button'
import style from './modal-sale-fir.scss?module'

const cn = useClassNames('modal-sale-fir', style)
const classes = {
  main: cn(),
  formWrapper: cn('form-wrapper'),
  form: cn('form'),
  button: cn('button'),
  phone: cn('phone'),
  error: cn('error')
}

const buttonOptions = { size: 'custom' as 'custom', color: Color.green }

export const ModalSaleFir = injectStyles(
  style,
  defineComponent({
    name: 'ModalSaleFir',
    props: {},
    setup: (props, { emit }) => {
      return () => (
        <ModalWithClose class={classes.main} onClose={() => emit('close')}>
          <div class={classes.formWrapper}>
            <BasePhoneForm
              source={'Голубая ель бесплатно'}
              mainClass={classes.form}
              errorClass={classes.error}
              phoneClass={classes.phone}
              withIcon={false}
              buttonClass={classes.button}
              buttonOptions={buttonOptions}
            >
              <template slot="button">Получить ель бесплатно</template>
            </BasePhoneForm>
          </div>
        </ModalWithClose>
      )
    }
  })
)
