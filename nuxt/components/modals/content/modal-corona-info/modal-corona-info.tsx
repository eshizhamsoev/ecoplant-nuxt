import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import style from './modal-corona-info.scss?module'

const cn = useClassNames('modal-corona-info', style)

const classes = {
  main: cn()
}

export const ModalCoronaInfo = injectStyles(
  style,
  defineComponent({
    name: 'ModalCoronaInfo',
    props: {
      text: {
        type: String,
        default: ''
      }
    },
    setup: (props, { emit }) => {
      const lines = computed(() => props.text.split('\n'))
      return () => (
        <ModalWithClose onClose={() => emit('close')} class={classes.main}>
          {lines.value.map((line) => (
            <p>{line}</p>
          ))}
        </ModalWithClose>
      )
    }
  })
)
