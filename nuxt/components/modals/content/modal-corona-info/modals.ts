import { useModalOpener } from '~/support/hooks/useModalOpener'

const text1 =
  'Мы можем устроить для вас онлайн прогулку, позвонить вам по WhatsApp или FaceTime.\nНаш специалист сделает для вас экскурсию по питомнику, где необходимо остановиться приблизит камеру или покажет фотографии. В общем создаст ощущение полного присутствия у нас в питомнике.\nТак вы сможете отобрать свои растения.'

// const text2 =
//   'Наши специалисты приезжают на участок и проводят работы, а вы, наблюдая за ними из окна, можете общаться по телефону с нашим бригадиром и руководить процессом посадки.'

const text3 =
  'Наше руководство запретило назначать массовые встречи, каждые 30 минут проводится дезинфекция помещений, везде антисептики. \nВсе сделано для вашей безопасности. Мы работаем в обычном режиме. Вы можете безопасно посетить наш питомник.'

const text4 =
  'Работаем c 8.00 до 20.00 каждый день без выходных и праздников. На территории питомника приняты все меры для его безопасного посещения (разметка дистанции, каждые 30 мин обработка антисептиками, средства личной защиты).'

export const useCoronaPopups = () => {
  const modalOpener = useModalOpener()

  const openModal = (text: string) => {
    modalOpener({ name: 'corona-info', text })
  }

  const openVideoModal = () => {
    modalOpener({
      name: 'corona-video',
      video:
        'https://ecoplant-pitomnik.ru/files/videos/contactless-planting.mp4'
    })
  }

  return {
    online: () => openModal(text1),
    shipping: () => openVideoModal(),
    safe: () => openModal(text3),
    work: () => openModal(text4)
  }
}
