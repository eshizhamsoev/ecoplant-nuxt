import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobileBlockPlanting } from '~/components/blocks/block-planting/mobile'
import { MobileBlockShipping } from '~/components/blocks/block-shipping/mobile'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import style from './modal-mobile-shipping-and-planting.scss?module'

const cn = useClassNames('modal-mobile-shipping-and-planting', style)

export const ModalMobileShippingAndPlanting = injectStyles(
  style,
  defineComponent({
    name: 'ModalMobileShippingAndPlanting',
    props: {},
    setup: (_, { emit }) => {
      const close = () => emit('close')

      return () => (
        <ModalWithClose class={cn()} onClose={close}>
          <div>
            <MobileBlockPlanting />
            <MobileBlockShipping />
          </div>
        </ModalWithClose>
      )
    }
  })
)
