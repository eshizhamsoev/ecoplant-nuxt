import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './modal-video.scss?module'

const cn = useClassNames('modal-video', style)

const classes = {
  main: cn(),
  video: cn('video')
}

export const ModalVideo = injectStyles(
  style,
  defineComponent({
    name: 'ModalVideo',
    props: {
      video: {
        type: String,
        required: true
      }
    },
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          <video controls autoplay class={classes.video}>
            <source type={'video/mp4'} src={props.video} />
          </video>
        </div>
      )
    }
  })
)
