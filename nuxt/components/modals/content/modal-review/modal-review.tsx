import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ReviewContent } from '~/components/content/review-content'
import style from './modal-review.scss?module'

const cn = useClassNames('modal-review', style)

export const ModalReview = injectStyles(
  style,
  defineComponent({
    name: 'ModalReview',
    props: {},
    setup: (props, ctx) => {
      return () => (
        <div class={cn()}>
          <ReviewContent onClose={() => ctx.emit('close')} />
        </div>
      )
    }
  })
)
