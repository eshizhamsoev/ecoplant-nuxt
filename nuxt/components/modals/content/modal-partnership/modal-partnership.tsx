import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { ModalHeadings } from '~/components/modals/layout/modal-headings'
import style from './modal-partnership.scss?module'

const mail = 'work@eco-plant.ru'
const cn = useClassNames('modal-partnership', style)

const classes = {
  main: cn(),
  headings: cn('headings'),
  emailLine: cn('email-line'),
  email: cn('email')
}

export const ModalPartnership = injectStyles(
  style,
  defineComponent({
    name: 'ModalPartnership',
    props: {},
    setup: (props, { emit }) => {
      return () => (
        <ModalWithClose onClose={() => emit('close')} class={classes.main}>
          <ModalHeadings
            heading="Ваше предложение"
            leading="Отправьте ваше предложение с указанием контактных данных, фото и видео на указанную почту для возможного сотрудничества:"
            class={classes.headings}
          />
          <div class={classes.emailLine}>
            <a href={`mailto:${mail}`} class={classes.email}>
              {mail}
            </a>
          </div>
        </ModalWithClose>
      )
    }
  })
)
