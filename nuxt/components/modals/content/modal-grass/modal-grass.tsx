import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { GrassProduct } from '~/store/grass'
import { DesktopGrassItemContent } from '~/components/content/grass-item-content/desktop/desktop-grass-item-content'
import style from './modal-grass.scss?module'

const cn = useClassNames('modal-grass', style)

const classes = {
  main: cn()
}

export const ModalGrass = injectStyles(
  style,
  defineComponent({
    name: 'ModalGrass',
    props: {
      product: {
        type: Object as () => GrassProduct,
        required: true
      }
    },
    setup: (props, { emit }) => {
      return () => (
        <ModalWithClose onClose={() => emit('close')} class={classes.main}>
          <DesktopGrassItemContent product={props.product} />
        </ModalWithClose>
      )
    }
  })
)
