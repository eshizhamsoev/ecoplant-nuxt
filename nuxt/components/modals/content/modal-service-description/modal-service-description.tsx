import { computed, defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { FORM_TYPES, useForm } from '~/support/hooks/useForm'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close/modal-with-close'
import { BaseButton, BaseFormResponse } from '~/components/base'
import { BaseField } from '~/components/base/base-field/base-field'
import { BasePhoneInput } from '~/components/base/base-phone-input/base-phone-input'
import { Color } from '~/components/theme/theme-button'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { galleryItem } from '~/graphql/queries'
import { GTM_EVENTS } from '~/support/hooks/useReachGoal'
import style from './modal-service-description.scss?module'

const cn = useClassNames('modal-service-description', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  description: cn('description'),
  inner: cn('inner'),
  images: cn('images'),
  imageWrapper: cn('image-wrapper'),
  image: cn('image'),
  formatResponse: cn('form-response'),
  form: cn('form'),
  formItem: cn('form-item'),
  inputDesc: cn('input-desc'),
  field: cn('field'),
  input: cn('input'),
  button: cn('button')
}

export const ModalServiceDescription = injectStyles(
  style,
  defineComponent({
    name: 'ModalServiceDescription',
    props: {
      id: {
        type: String,
        required: true
      }
    },
    setup: (props, { emit, root }) => {
      const phone = ref('')
      const name = ref('')
      const { submit, fatalError, successMessage } = useForm(
        root,
        FORM_TYPES.PHONE,
        { goal: GTM_EVENTS.calculateServicePrice }
      )
      const { result } = useGqlQuery(galleryItem, { id: props.id })
      const content = computed(() =>
        result.value?.galleryItem?.content
          ? JSON.parse(result.value.galleryItem.content)
          : null
      )

      const onSubmit = (e: Event) => {
        e.preventDefault()
        submit({
          phone: phone.value,
          name: name.value
        })
      }

      return () =>
        result.value && (
          <ModalWithClose class={cn()} onClose={() => emit('close')}>
            {content.value ? (
              <div class={classes.inner}>
                <div class={classes.heading}>{content.value.title}</div>
                <div
                  class={classes.description}
                  domProps={{ innerHTML: content.value.description }}
                />
                <div class={classes.images}>
                  {content.value.images.map((item: any) => (
                    <div class={classes.imageWrapper}>
                      <img
                        loading="lazy"
                        class={classes.image}
                        src={item}
                        alt=""
                      />
                    </div>
                  ))}
                </div>
                <BaseFormResponse
                  fatalError={fatalError.value}
                  successMessage={successMessage.value}
                  class={classes.formatResponse}
                >
                  <form class={classes.form} onSubmit={onSubmit}>
                    <div class={classes.formItem}>
                      <div class={classes.inputDesc}>Как вас зовут?</div>
                      <BaseField class={classes.field}>
                        <input
                          v-model={name.value}
                          class={classes.input}
                          placeholder="Алексей"
                        />
                      </BaseField>
                    </div>
                    <div class={classes.formItem}>
                      <div class={classes.inputDesc}>
                        Введите номер телефона
                      </div>
                      <BaseField class={classes.field}>
                        <BasePhoneInput
                          v-model={phone.value}
                          class={classes.input}
                        />
                      </BaseField>
                    </div>
                    <BaseButton
                      color={Color.red}
                      class={classes.button}
                      size={'m'}
                      type="submit"
                    >
                      Рассчитать стоимость работ
                    </BaseButton>
                  </form>
                </BaseFormResponse>
              </div>
            ) : (
              <div>
                Мы не смогли загрузить данные по услуге, но вы всегда можете
                рассчитать стоимость с нашим менеджером по телефону.
              </div>
            )}
          </ModalWithClose>
        )
    }
  })
)
