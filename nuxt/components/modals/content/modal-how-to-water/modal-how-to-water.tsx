import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { BlockHowToWater } from '~/components/blocks/block-how-to-water'
import { BaseBackButton } from '~/components/base'
import style from './modal-how-to-water.scss?module'

const cn = useClassNames('modal-how-to-water', style)

const classes = {
  main: cn(),
  back: cn('back')
}

export const ModalHowToWater = injectStyles(
  style,
  defineComponent({
    name: 'ModalHowToWater',
    props: {},
    setup: (props, { emit }) => {
      const close = () => emit('close')
      return () => (
        <ModalWithClose class={classes.main} onClose={close}>
          <BaseBackButton onClick={close} class={classes.back} />
          <BlockHowToWater />
        </ModalWithClose>
      )
    }
  })
)
