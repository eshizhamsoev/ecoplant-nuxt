import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { clientPhotos } from '~/graphql/queries'
import { BlockClients } from '~/components/blocks/block-clients'
import style from './modal-client-photos.scss?module'

const cn = useClassNames('modal-client-photos', style)

export const ModalClientPhotos = injectStyles(
  style,
  defineComponent({
    name: 'ModalClientPhotos',
    props: {},
    setup: (props, { emit }) => {
      const { loading } = useGqlQuery(clientPhotos, {})
      return () =>
        !loading.value && (
          <ModalWithClose class={cn()} onClose={() => emit('close')}>
            <div class={cn('heading')}>Наши клиенты</div>
            <BlockClients />
          </ModalWithClose>
        )
    }
  })
)
