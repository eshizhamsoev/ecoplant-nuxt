import { defineComponent } from '@nuxtjs/composition-api'
import { onMounted } from '@vue/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { BaseClientGalleryControlBar } from '~/components/base/base-client-gallery-control-bar/base-client-gallery-control-bar'
import { ClientPhoto } from '~/_generated/types'
import style from './modal-images-feed.scss?module'

const cn = useClassNames('modal-images-feed', style)

export const ModalImagesFeed = injectStyles(
  style,
  defineComponent({
    name: 'ModalImagesFeed',
    props: {
      id: {
        type: Number,
        default: 0
      },
      clients: {
        type: Array as () => ClientPhoto[],
        required: true
      }
    },
    setup: (props, { emit, refs }) => {
      onMounted(() => {
        // @ts-ignore
        refs.scrollContainer.closest('.scrollable').scrollTop =
          // @ts-ignore
          refs[`feedImage${props.id}`].offsetTop
      })
      return () => (
        <div class={cn()} ref="scrollContainer">
          {props.clients.map((client, i) => (
            <div class={cn('item')} ref={`feedImage${i}`}>
              <img
                id={`feed-image-${i}`}
                class={cn('image')}
                src={client.image.large.src}
                height={client.image.large.height}
                width={client.image.large.width}
                alt={client.name}
                loading="lazy"
              />
              <div class={cn('content')}>
                <BaseClientGalleryControlBar smallIcons={true} />
                <div class={cn('text')}>{client.name}</div>
              </div>
            </div>
          ))}
          <button class={cn('close-button')} onClick={() => emit('close')}>
            <BaseIcon
              name={'times'}
              width={12}
              class={cn('close-button-icon')}
            />
          </button>
        </div>
      )
    }
  })
)
