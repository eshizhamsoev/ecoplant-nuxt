import { defineComponent } from '@nuxtjs/composition-api'
import plants from './images/plants.jpg'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { BaseIcon } from '~/components/base'
import style from './modal-large-orders.scss?module'

const cn = useClassNames('modal-large-orders', style)

const classes = {
  main: cn(),
  imageWrapper: cn('image-wrapper'),
  image: cn('image'),
  content: cn('content'),
  title: cn('title'),
  contacts: cn('contacts'),
  contact: cn('contact'),
  contactTitle: cn('contact-title'),
  contactLink: cn('contact-link')
}

export const ModalLargeOrders = injectStyles(
  style,
  defineComponent({
    name: 'ModalLargeOrders',
    props: {},
    setup: (props, { emit }) => {
      const close = () => emit('close')
      const MAIL = 'opt@eco-plant.ru'
      const PHONE = '8 (499) 283 27 84'
      const PHONE_HREF = '+74992832784'

      return () => (
        <ModalWithClose class={classes.main} onClose={close}>
          <div class={classes.imageWrapper}>
            <img
              src={plants}
              alt="Саженцы"
              class={classes.image}
              width={1280}
              height={370}
            />
          </div>
          <div class={classes.content}>
            <div class={classes.title}>
              На крупные оптовые заказы даем индивидуальную скидку
            </div>
            <div class={classes.contacts}>
              <div class={classes.contact}>
                <div class={classes.contactTitle}>Напишите нам на почту:</div>
                <a href={'mailto:' + MAIL} class={classes.contactLink}>
                  <BaseIcon name="envelope" width={26} />
                  <span>{MAIL}</span>
                </a>
              </div>
              <div class={classes.contact}>
                <div class={classes.contactTitle}>или позвоните:</div>
                <a href={'tel:' + PHONE_HREF} class={classes.contactLink}>
                  <BaseIcon name="phone_2" width={25} />
                  <span>{PHONE}</span>
                </a>
              </div>
            </div>
          </div>
        </ModalWithClose>
      )
    }
  })
)
