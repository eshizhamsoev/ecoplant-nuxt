import { defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { ModalHeadings } from '~/components/modals/layout/modal-headings'
import { BaseFormResponse } from '~/components/base/base-form-response/base-form-response'
import { BaseField } from '~/components/base'
import { BaseLoadingButton } from '~/components/base/base-loading-button/base-loading-button'
import { FORM_TYPES, useForm } from '~/support/hooks/useForm'
import { GTM_EVENTS } from '~/support/hooks/useReachGoal'
import style from './modal-question.scss?module'

const cn = useClassNames('modal-question', style)

const classes = {
  main: cn(),
  headings: cn('headings'),
  form: cn('form'),
  error: cn('error'),
  success: cn('success'),
  fieldWrapper: cn('field-wrapper'),
  field: cn('field'),
  buttonWrapper: cn('button-wrapper'),
  button: cn('button')
}

const buttonOptions = {
  size: 'l' as 'l'
}

export const ModalQuestion = injectStyles(
  style,
  defineComponent({
    name: 'ModalQuestion',
    props: {
      goal: {
        type: String as () => GTM_EVENTS,
        default: null
      },
      formType: {
        type: Number as () => FORM_TYPES,
        required: true
      },
      heading: {
        type: String,
        default: ''
      },
      placeholder: {
        type: String,
        default: 'Введите ваш вопрос'
      },
      buttonText: {
        type: String,
        default: 'Отправить'
      }
    },
    setup: (props, { root, emit }) => {
      const question = ref('')
      const { processing, successMessage, fatalError, submit, errorFields } =
        useForm(
          root,
          props.formType,
          props.goal && {
            goal: props.goal
          }
        )

      const onClick = () => {
        submit({
          question: question.value
        })
      }

      function onInput(event: InputEvent & { target?: HTMLInputElement }) {
        question.value = event.target?.value
      }

      return () => (
        <ModalWithClose onClose={() => emit('close')} class={classes.main}>
          <ModalHeadings heading={props.heading} class={classes.headings} />
          <BaseFormResponse
            mainClass={classes.form}
            fatalError={fatalError.value}
            successMessage={successMessage.value}
            successClass={classes.success}
            errorClass={classes.error}
          >
            <BaseField
              error={
                !!(errorFields.value && errorFields.value.includes('question'))
              }
              class={classes.fieldWrapper}
            >
              <textarea
                class={classes.field}
                value={question.value}
                onInput={onInput}
                placeholder={props.placeholder}
              />
            </BaseField>
            <div class={classes.buttonWrapper}>
              <BaseLoadingButton
                loading={processing.value}
                buttonOptions={buttonOptions}
                class={classes.button}
                onClick={onClick}
              >
                {props.buttonText}
              </BaseLoadingButton>
            </div>
          </BaseFormResponse>
        </ModalWithClose>
      )
    }
  })
)
