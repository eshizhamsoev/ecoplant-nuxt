import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { villages } from '~/graphql/queries'
import { BlockVillages } from '~/components/blocks/block-villages/block-villages'
import style from './modal-villages.scss?module'

const cn = useClassNames('modal-villages', style)

export const ModalVillages = injectStyles(
  style,
  defineComponent({
    name: 'ModalVillages',
    props: {},
    setup: (props, { emit }) => {
      const { loading } = useGqlQuery(villages, {})
      return () =>
        !loading.value && (
          <ModalWithClose class={cn()} onClose={() => emit('close')}>
            <BlockVillages />
          </ModalWithClose>
        )
    }
  })
)
