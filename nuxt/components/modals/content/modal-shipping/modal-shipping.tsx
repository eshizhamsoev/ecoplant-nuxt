import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { DesktopBlockShipping } from '~/components/blocks/block-shipping/desktop'
import { MobileBlockShipping } from '~/components/blocks/block-shipping/mobile'
import style from './modal-shipping.scss?module'

const cn = useClassNames('modal-shipping', style)

const classes = {
  main: cn()
}

export const ModalShipping = injectStyles(
  style,
  defineComponent({
    name: 'ModalShipping',
    props: {},
    setup: (props, { emit, root }) => {
      const close = () => emit('close')
      const modalType = computed(() => {
        return root.$device.isDesktop ? (
          <DesktopBlockShipping />
        ) : (
          <MobileBlockShipping />
        )
      })
      return () => (
        <ModalWithClose class={classes.main} onClose={close}>
          {modalType.value}
        </ModalWithClose>
      )
    }
  })
)
