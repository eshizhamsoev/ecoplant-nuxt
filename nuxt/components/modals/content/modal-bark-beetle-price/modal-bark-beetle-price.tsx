import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { MobileButtonLine } from '~/components/layouts/mobile/mobile-button-line'
import style from './modal-bark-beetle-price.scss?module'

const cn = useClassNames('modal-bark-beetle-price', style)

export const ModalBarkBeetlePrice = injectStyles(
  style,
  defineComponent({
    name: 'ModalBarkBeetlePrice',
    props: {},
    setup: (props, { emit }) => {
      return () => (
        <ModalWithClose onClose={() => emit('close')} class={cn()}>
          <h2 class={cn('heading')}>Прайс по обработке от короеда</h2>
          <table class={cn('table')}>
            <thead>
              <tr>
                <th class={cn('head-cell')}>Размер дерева</th>
                <th class={cn('head-cell')}>Цена</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>до 1 м</td>
                <td class={cn('price-cell')}>150 рублей</td>
              </tr>
              <tr>
                <td>от 1-2 м</td>
                <td class={cn('price-cell')}>240 рублей</td>
              </tr>
              <tr>
                <td>от 2-3 м</td>
                <td class={cn('price-cell')}>290 рублей</td>
              </tr>
              <tr>
                <td>от 3-4 м</td>
                <td class={cn('price-cell')}>370 рублей</td>
              </tr>
              <tr>
                <td>от 4-5 м</td>
                <td class={cn('price-cell')}>450 рублей</td>
              </tr>
              <tr>
                <td>от 5-6 м</td>
                <td class={cn('price-cell')}>590 рублей</td>
              </tr>
              <tr>
                <td>от 6-7 м</td>
                <td class={cn('price-cell')}>750 рублей</td>
              </tr>
            </tbody>
          </table>
          <MobileButtonLine withCart={false} class={cn('buttons')} />
        </ModalWithClose>
      )
    }
  })
)
