import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { BlockGrass } from '~/components/blocks/block-grass'
import { Visual } from '~/components/theme/theme-button'
import { BaseButton, BaseIcon } from '~/components/base'
import style from './modal-grass-page.scss?module'

const cn = useClassNames('modal-grass', style)

const classes = {
  main: cn(),
  back: cn('back'),
  backIcon: cn('back-icon')
}

export const ModalGrassPage = injectStyles(
  style,
  defineComponent({
    name: 'ModalGrassPage',
    setup: (props, { emit, root }) => {
      const group = root.$accessor.group
      const grassProps = {
        withBackButton: false,
        structure: {
          sizeText:
            group === 'spb'
              ? '0,4 x 2,0 м и 0,4 x 2,5 м'
              : '0,4 x 2,5 м и 0,6 х 1,67 м',
          widthText: group === 'spb' ? '0,40м' : '0,40м (0,60м)',
          lengthText: group === 'spb' ? '2.0м  (2,5м)' : '2.50м  (1,67м)'
        },
        pallet: {
          deliverLabel:
            group === 'spb'
              ? 'В один манипулятор можно загрузить'
              : 'В одну грузовую машину можно загрузить',
          deliverySize: group === 'spb' ? '7 поддонов' : 'около 24-25 поддонов',
          deliveryAdditionalInfo:
            group === 'spb'
              ? '315 m2 в зависимости от погоды'
              : '960-1000 m2 в зависимости от погоды',
          currierSize: null
        }
      }
      return () => (
        <ModalWithClose onClose={() => emit('close')} class={classes.main}>
          <BaseButton
            onClick={() => emit('close')}
            size="m"
            class={classes.back}
            visual={Visual.outline}
          >
            <BaseIcon
              name={'arrow-left'}
              width={24}
              height={15}
              class={classes.backIcon}
            />
            <span>Назад</span>
          </BaseButton>
          <BlockGrass props={grassProps} />
        </ModalWithClose>
      )
    }
  })
)
