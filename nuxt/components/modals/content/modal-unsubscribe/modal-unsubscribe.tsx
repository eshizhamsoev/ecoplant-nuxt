import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BasePhoneInput } from '~/components/base/base-phone-input'
import { ApiFormSubmitProvider } from '~/components/action-providers/api-form-submit-provider/api-form-submit-provider'
import { FORM_TYPES } from '~/support/hooks/useForm'
import { MainForm } from '~/components/common/form/main-form'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { GTM_EVENTS, useReachGoal } from '~/support/hooks/useReachGoal'
import style from './modal-unsubscribe.scss?module'

const cn = useClassNames('modal-unsubscribe', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  form: cn('form')
}

export const ModalUnsubscribe = injectStyles(
  style,
  defineComponent({
    name: 'ModalUnsubscribe',
    props: {},
    setup: (_, { emit }) => {
      const fields = [
        {
          component: BasePhoneInput,
          name: 'phone',
          label: 'Телефон',
          options: {
            required: false
          }
        },
        {
          component: 'input',
          name: 'email',
          label: 'Е-mail',
          options: {
            required: false,
            type: 'email',
            placeholder: 'Ваш e-mail'
          }
        }
      ]
      const reachGoal = useReachGoal()
      const success = () => {
        reachGoal(GTM_EVENTS.unsubscribe)
      }
      return () => (
        <ModalWithClose class={classes.main} onClose={() => emit('close')}>
          <div class={classes.heading}>Отписаться от рассылки</div>
          <ApiFormSubmitProvider
            formId={FORM_TYPES.UNSUBSCRIBE}
            onSuccess={success}
          >
            <MainForm
              formOptions={{ fields }}
              buttonOptions={{ size: 'l' }}
              class={classes.form}
            >
              <template slot="button">
                <span>Отписаться от рассылки</span>
              </template>
              <template slot="success">
                Вы успешно отписались от рассылки
              </template>
            </MainForm>
          </ApiFormSubmitProvider>
        </ModalWithClose>
      )
    }
  })
)
