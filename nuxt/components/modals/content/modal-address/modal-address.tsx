import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { BlockContactDesktop } from '~/components/blocks/block-contact/block-contact-desktop'
import style from './modal-address.scss?module'

export type PopupAddress = {
  city: string
  phone: string
  mailClient: string
  mailSuggestion: string
  address: string
  geoPoint: { latitude: number; longitude: number }
}

const cn = useClassNames('modal-address', style)

export const ModalAddress = injectStyles(
  style,
  defineComponent({
    name: 'ModalAddress',
    props: {
      isAdditional: {
        type: Boolean,
        required: true
      }
    },
    setup: (props, { emit, root }) => {
      const close = () => emit('close')

      const region = props.isAdditional
        ? root.$accessor.contacts.additionalContacts.region
        : root.$accessor.contacts.currentRegion

      return () => (
        <ModalWithClose onClose={close}>
          <div class={cn('header')}>Контакты {region}</div>
          <BlockContactDesktop
            isAdditional={props.isAdditional}
            showRegion={false}
          />
        </ModalWithClose>
      )
    }
  })
)
