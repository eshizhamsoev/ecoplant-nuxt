import { computed, defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { varietyInfoQuery } from '~/graphql/queries'
import style from './modal-variety-description.scss?module'

const cn = useClassNames('modal-variety-description', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  table: cn('table'),
  loading: cn('loading')
}

export const ModalVarietyDescription = injectStyles(
  style,
  defineComponent({
    name: 'ModalVarietyDescription',
    props: {
      varietyId: {
        type: String,
        required: true
      }
    },
    setup: (props, { emit }) => {
      const { result, error, loading } = useGqlQuery(varietyInfoQuery, {
        id: props.varietyId
      })
      const variety = computed(() => result.value?.variety)
      const attributes = computed(
        () => variety.value && variety.value?.description
      )
      return () =>
        !loading.value && (
          <ModalWithClose class={classes.main} onClose={() => emit('close')}>
            {variety.value && (
              <div class={classes.heading}>
                Описание сорта "{variety.value.name}"
              </div>
            )}
            {error.value ? (
              <div>Произошла ошибка при загрузке данных</div>
            ) : (
              attributes.value && (
                <table class={classes.table}>
                  <tbody>
                    {attributes.value.map((attribute) => (
                      <tr>
                        <td>{attribute.name}</td>
                        <td>{attribute.value}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              )
            )}
          </ModalWithClose>
        )
    }
  })
)
