import { defineComponent, ref } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { ModalHeadings } from '~/components/modals/layout/modal-headings'
import { BaseFormResponse } from '~/components/base/base-form-response/base-form-response'
import { FORM_TYPES, useForm } from '~/support/hooks/useForm'
import { BaseField } from '~/components/base'
import { BaseLoadingButton } from '~/components/base/base-loading-button/base-loading-button'
import { GTM_EVENTS } from '~/support/hooks/useReachGoal'
import style from './modal-director.scss?module'

const cn = useClassNames('modal-director', style)

const classes = {
  main: cn(),
  headings: cn('headings'),
  form: cn('form'),
  error: cn('error'),
  success: cn('success'),
  fieldWrapper: cn('field-wrapper'),
  field: cn('field'),
  buttonWrapper: cn('button-wrapper'),
  button: cn('button')
}

const buttonOptions = {
  size: 'l' as 'l'
}

export const ModalDirector = injectStyles(
  style,
  defineComponent({
    name: 'ModalDirector',
    props: {},
    setup: (props, { emit, root }) => {
      const question = ref('')
      const { processing, successMessage, fatalError, submit, errorFields } =
        useForm(root, FORM_TYPES.DIRECTOR, {
          goal: GTM_EVENTS.writeToDirector
        })

      // const onClick = () => {
      //   submit({
      //     question: question.value,
      //     source: 'Письмо директору'
      //   })
      // }

      const onSubmit = (e: Event) => {
        e.preventDefault()
        submit({
          question: question.value,
          source: 'Письмо директору'
        })
      }

      function onInput(event: InputEvent & { target?: HTMLInputElement }) {
        question.value = event.target?.value
      }

      return () => (
        <ModalWithClose onClose={() => emit('close')} class={classes.main}>
          <ModalHeadings heading="Письмо директору" class={classes.headings} />
          <BaseFormResponse
            mainClass={classes.form}
            fatalError={fatalError.value}
            successMessage={successMessage.value}
            successClass={classes.success}
            errorClass={classes.error}
          >
            <form onSubmit={onSubmit}>
              <BaseField
                error={Boolean(
                  errorFields.value && errorFields.value.includes('question')
                )}
                class={classes.fieldWrapper}
              >
                <textarea
                  class={classes.field}
                  minlength={35}
                  required
                  value={question.value}
                  onInput={onInput}
                  placeholder="Здесь вы можете написать генеральному директору группы компаний «Экоплант» напрямую. Принимаем к рассмотрению только те письма, в которых указан номер заказа. Оставляйте номера телефонов, вам могут перезвонить в индивидуальном порядке."
                />
              </BaseField>
              <div class={classes.buttonWrapper}>
                <BaseLoadingButton
                  loading={processing.value}
                  buttonOptions={buttonOptions}
                  class={[classes.button]}
                  type={'submit'}
                >
                  Отправить письмо
                </BaseLoadingButton>
              </div>
            </form>
          </BaseFormResponse>
        </ModalWithClose>
      )
    }
  })
)
