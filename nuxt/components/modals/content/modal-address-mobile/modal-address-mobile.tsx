import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalWithClose } from '~/components/modals/layout/modal-with-close'
import { BlockContactMobile } from '~/components/blocks/block-contact/block-contact-mobile'
import style from './modal-address-mobile.scss?module'

const cn = useClassNames('modal-address-mobile', style)

export const ModalAddressMobile = injectStyles(
  style,
  defineComponent({
    name: 'ModalAddressMobile',
    props: {
      isAdditional: {
        type: Boolean,
        required: true
      }
    },
    setup: (props, { emit }) => {
      const close = () => emit('close')

      return () => (
        <ModalWithClose onClose={close}>
          <BlockContactMobile class={cn()} isAdditional={props.isAdditional} />
        </ModalWithClose>
      )
    }
  })
)
