import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { ModalCloseButton } from '~/components/modals/layout/modal-close-button/modal-close-button'
import style from './modal-with-close.scss?module'

const cn = useClassNames('modal-with-close', style)

const classes = {
  main: cn(),
  close: cn('close')
}

export const ModalWithClose = injectStyles(
  style,
  defineComponent({
    name: 'ModalWithClose',
    props: {},
    setup: (props, { slots, emit }) => {
      return () => (
        <div class={classes.main}>
          <div class={cn('close-container')}>
            <ModalCloseButton
              class={cn('close')}
              onClick={() => emit('close')}
            />
          </div>
          {slots.default && slots.default()}
        </div>
      )
    }
  })
)
