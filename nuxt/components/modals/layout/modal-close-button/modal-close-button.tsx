import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './modal-close-button.scss?module'

const cn = useClassNames('modal-close-button', style)

export const ModalCloseButton = injectStyles(
  style,
  defineComponent({
    name: 'ModalCloseButton',
    props: {},
    setup: (props, { emit }) => {
      return () => (
        <button onclick={() => emit('click')} class={cn()}>
          <svg-icon name="times" width={13} height={13} />
        </button>
      )
    }
  })
)
