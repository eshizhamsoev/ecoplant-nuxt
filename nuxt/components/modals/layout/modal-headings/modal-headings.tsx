import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './modal-headings.scss?module'

const cn = useClassNames('modal-headings', style)

const classes = {
  main: cn(),
  heading: cn('heading'),
  leading: cn('leading')
}

export const ModalHeadings = injectStyles(
  style,
  defineComponent({
    name: 'ModalHeadings',
    props: {
      heading: {
        type: String,
        required: true
      },
      leading: {
        type: String,
        default: ''
      }
    },
    setup: (props) => {
      return () => (
        <div class={classes.main}>
          <div class={classes.heading}>{props.heading}</div>
          {props.leading && <div class={classes.leading}>{props.leading}</div>}
        </div>
      )
    }
  })
)
