import { defineComponent } from '@nuxtjs/composition-api'
import bg from './assets/bg.png'
import bgX2 from './assets/bg-x2.png'
import tree from './assets/tree.png'
import treeX2 from './assets/tree-x2.png'
import treeX3 from './assets/tree-x3.png'
import { injectStyles } from '~/support/utils/injectStyle'
import { useClassNames } from '~/support/utils/bem-classnames'
import { ModalCloseButton } from '~/components/modals/layout/modal-close-button/modal-close-button'
import style from './modal-with-tree.scss?module'

const cn = useClassNames('modal-with-tree', style)

export const ModalWithTree = injectStyles(
  style,
  defineComponent({
    name: 'ModalWithTree',
    props: {},
    setup: (props, { emit, slots }) => {
      return () => (
        <div class={cn()}>
          <div class={cn('header')}>
            <img
              src={bg}
              class={cn('bg')}
              alt=""
              srcset={`${bg}, ${bgX2}`}
              loading={'lazy'}
            />
            <img
              src={tree}
              class={cn('tree')}
              alt=""
              srcset={`${tree}, ${treeX2} 2x, ${treeX3} 3x`}
              loading={'lazy'}
            />
          </div>
          <div class={cn('content')}>
            <ModalCloseButton
              class={cn('close')}
              onClick={() => emit('close')}
            />
            {slots.default && slots.default()}
            <svg
              class={cn('mask-svg')}
              width="0"
              height="0"
              viewBox="0 0 533 166"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <defs>
                <clipPath
                  id="modal-callback-mask"
                  clipPathUnits="objectBoundingBox"
                  transform="scale(0.0018761726, 0.00602409638)"
                >
                  <path d="M0 0.1 C0 52.809 3.93965 48.4918 9.08726 48.0199L522.087 1.00022C527.946 0.463235 533 5.07522 533 10.9585V166H0V57.9782Z" />
                </clipPath>
              </defs>
            </svg>
          </div>
        </div>
      )
    }
  })
)
