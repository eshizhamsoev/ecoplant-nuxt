import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { BasePopover } from '~/components/base/base-popover'
import { useOnWhatsAppClick } from '~/support/hooks/useWhatsApp'
import { useModalOpener } from '~/support/hooks/useModalOpener'
import style from './desktop-support-popover.scss?module'

const cn = useClassNames('desktop-support-popover', style)

const classes = {
  main: cn(),
  pointer: cn('pointer'),
  modal: cn('modal'),
  item: cn('item'),
  itemChat: cn('item', { type: 'chat' }),
  itemText: cn('item-text'),
  itemIcon: cn('item-icon')
}

export const DesktopSupportPopover = injectStyles(
  style,
  defineComponent({
    name: 'SupportPopover',
    props: {
      visible: {
        type: Boolean,
        required: true
      }
    },
    setup: (props, { emit }) => {
      const close = () => {
        emit('close')
      }
      const onWhatsApp = useOnWhatsAppClick()
      const modalOpener = useModalOpener()
      const openPhoneUsModal = () => modalOpener({ name: 'phone-us' })
      const onPhoneClick = () => {
        openPhoneUsModal()
        close()
      }

      const onWhatsAppClick = () => {
        onWhatsApp()
        close()
      }

      return () => (
        <BasePopover
          class={classes.main}
          onClose={close}
          visible={props.visible}
        >
          <div class={classes.pointer} />
          <div class={classes.modal}>
            <button class={classes.itemChat} onClick={onWhatsAppClick}>
              <BaseIcon name="whatsapp" width={23} class={classes.itemIcon} />
              <span class={classes.itemText}>Написать в техподдержку</span>
            </button>
            <button class={classes.item} onClick={onPhoneClick}>
              <BaseIcon name="phone" width={23} class={classes.itemIcon} />
              <span class={classes.itemText}>Позвонить в техподдержку</span>
            </button>
          </div>
        </BasePopover>
      )
    }
  })
)
