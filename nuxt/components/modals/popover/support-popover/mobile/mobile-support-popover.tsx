import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { BaseIcon } from '~/components/base'
import { useOnWhatsAppClick } from '~/support/hooks/useWhatsApp'
import { BasePopover } from '~/components/base/base-popover'
import { PhoneLink } from '~/components/action-providers/phone-link'
import style from './mobile-support-popover.scss?module'

const cn = useClassNames('mobile-support-popover', style)

const classes = {
  main: cn(),
  pointer: cn('pointer'),
  modal: cn('modal'),
  item: cn('item'),
  itemChat: cn('item', { type: 'chat' }),
  itemText: cn('item-text'),
  itemIcon: cn('item-icon')
}

export const MobileSupportPopover = injectStyles(
  style,
  defineComponent({
    name: 'SupportPopover',
    props: {
      visible: {
        type: Boolean,
        required: true
      }
    },
    setup: (props, { emit }) => {
      const close = () => {
        emit('close')
      }
      const onWhatsApp = useOnWhatsAppClick()

      const onWhatsAppClick = () => {
        onWhatsApp()
        close()
      }

      return () => (
        <BasePopover
          class={classes.main}
          onClose={close}
          visible={props.visible}
        >
          <div class={classes.pointer} />
          <div class={classes.modal}>
            <button class={classes.itemChat} onClick={onWhatsAppClick}>
              <BaseIcon name="whatsapp" width={23} class={classes.itemIcon} />
              <span class={classes.itemText}>Написать в техподдержку</span>
            </button>
            <PhoneLink class={classes.item} onClick={close}>
              <BaseIcon name="phone" width={23} class={classes.itemIcon} />
              <span class={classes.itemText}>Позвонить в техподдержку</span>
            </PhoneLink>
          </div>
        </BasePopover>
      )
    }
  })
)
