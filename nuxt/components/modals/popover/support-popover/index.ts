import { UtilUniversalComponentFactory } from '~/components/util-components/util-universal-component-factory'

export const SupportPopover = UtilUniversalComponentFactory(
  'SupportPopover',
  () =>
    import('./mobile/mobile-support-popover').then(
      (c) => c.MobileSupportPopover
    ),
  () =>
    import('./desktop/desktop-support-popover').then(
      (c) => c.DesktopSupportPopover
    )
)
