import { InjectionKey, Ref } from '@nuxtjs/composition-api'

type Menu = {
  open: () => {}
  close: () => {}
  isOpen: Ref<boolean>
}

export const menuSymbol: InjectionKey<Menu> = Symbol('menu')
