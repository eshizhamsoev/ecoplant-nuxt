import { ref } from '@nuxtjs/composition-api'

export function useMenu() {
  const isOpen = ref(false)
  const open = () => {
    isOpen.value = true
  }
  const close = () => {
    isOpen.value = false
  }
  return { open, close, isOpen }
}
