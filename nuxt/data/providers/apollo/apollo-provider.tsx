import { defineComponent, provide, useContext } from '@nuxtjs/composition-api'
import { DefaultApolloClient } from '@vue/apollo-composable'

export const ApolloProvider = defineComponent({
  name: 'ApolloProvider',
  props: {},
  setup: (props, { slots }) => {
    const ctx = useContext()
    provide(DefaultApolloClient, ctx.app.apolloProvider?.defaultClient)
    return () => slots.default && slots.default()
  }
})
