import {
  computed,
  onMounted,
  provide,
  ref,
  watch
} from '@nuxtjs/composition-api'
import { useMutation, useQuery } from '@vue/apollo-composable'
import {
  CartProductsQuery,
  ClearCartMutation,
  ClearCartMutationVariables,
  MutationCartQuantityArgs
} from '~/_generated/types'
import { cartProducts, productsInCart } from '~/graphql/queries'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { cartSymbol, ProductType } from '~/data/providers/cart/symbol'
import cartQuantityMutation from '~/graphql/mutations/CartQuantity.graphql'
import clearCartMutation from '~/graphql/mutations/ClearCart.graphql'
import { useEcommerceGoal } from '~/support/hooks/useEcommerceGoal'
import { PriceId, ProductId } from '~/common-types/catalog'

export const provideCart = () => {
  const products = ref<ProductType[]>([])
  const total = ref(0)

  onMounted(() => {
    const { result: cartProductsResult } = useQuery<CartProductsQuery>(
      cartProducts.getQuery()
    )

    const cart = computed(() => cartProductsResult.value?.cartProducts || [])
    const productIds = computed(() =>
      cart.value.map((cartProduct) => cartProduct.productId)
    )
    const priceIds = computed(() =>
      cart.value.reduce((arr, cartProduct) => {
        arr.push(
          ...cartProduct.prices
            .filter((price) => price.quantity > 0)
            .map((price) => price.priceId)
        )
        return arr
      }, [] as string[])
    )

    const props = computed(() => ({
      ids: productIds.value,
      priceIds: priceIds.value
    }))
    const options = computed(() => ({
      enabled: priceIds.value.length > 0
    }))
    const { result: productsInCartResult } = useGqlQuery(
      productsInCart,
      props,
      options
    )

    const getQuantity = (productId: string, priceId: string): number => {
      const product = cart.value.find(
        (product) => product.productId === productId
      )
      if (!product) {
        return 0
      }
      const price = product.prices.find((price) => price.priceId === priceId)
      if (!price) {
        return 0
      }
      return price.quantity
    }

    const productsWithQuantity = computed<ProductType[]>(() => {
      const items = productsInCartResult.value?.products || []
      return items
        .map((product) => ({
          ...product,
          prices: product.prices
            .map((price) => {
              const quantity = getQuantity(product.id, price.id)
              return {
                ...price,
                cartQuantity: quantity,
                total: price.price ? price.price * quantity : null
              }
            })
            .filter((price) => price.cartQuantity > 0)
        }))
        .filter((product) => product.prices.length > 0)
    })

    watch(
      () => productsWithQuantity.value,
      (value) => {
        products.value = value
      },
      {
        immediate: true,
        deep: true
      }
    )

    watch(
      () => products.value,
      () => {
        total.value = products.value.reduce(
          (sum, product) =>
            sum +
            product.prices.reduce((sum, price) => sum + (price.total || 0), 0),
          0
        )
      },
      {
        immediate: true,
        deep: true
      }
    )
  })

  const { mutate: updateQuantity } = useMutation<
    CartProductsQuery,
    MutationCartQuantityArgs
  >(cartQuantityMutation)

  const { mutate: clearCart } = useMutation<
    ClearCartMutation,
    ClearCartMutationVariables
  >(clearCartMutation)

  const { add, remove } = useEcommerceGoal()

  function getProduct(productId: ProductId): ProductType | null {
    return products.value.find((product) => product.id === productId) || null
  }

  const getPrice = (productId: string, priceId: string) => {
    const product = getProduct(productId)
    if (!product) {
      return null
    }
    const price = product.prices.find((price) => price.id === priceId)
    if (!price) {
      return null
    }
    return price
  }

  function getQuantity(productId: ProductId, priceId: PriceId): number {
    const price = getPrice(productId, priceId)
    if (!price) {
      return 0
    }
    return price.cartQuantity
  }

  const updateProductPriceQuantity = (
    productId: string,
    priceId: string,
    quantity: number
  ) => {
    const oldQuantity = getQuantity(productId, priceId)

    const promise = updateQuantity({
      productId,
      priceId,
      quantity
    })
    promise.then(() => {
      const product = getProduct(productId)
      const price = getPrice(productId, priceId)
      if (!product || !price) {
        return
      }
      if (oldQuantity < quantity) {
        add(product, price, quantity - oldQuantity)
      } else {
        remove(product, price, oldQuantity - quantity)
      }
    })

    return promise
  }

  const productInCart = function (productId: string) {
    const product = getProduct(productId)
    if (!product) {
      return 0
    }
    return product.prices.reduce((sum, item) => sum + item.cartQuantity, 0)
  }

  provide(cartSymbol, {
    products,
    total,
    getPrice,
    updateProductPriceQuantity,
    productInCart,
    clearCart
  })
}
