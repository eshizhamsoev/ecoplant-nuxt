import type { InjectionKey, Ref } from '@nuxtjs/composition-api'
import { ProductsInCartQuery } from '~/_generated/types'

export type ExtendedPrice = ProductsInCartQuery['products'][0]['prices'][0] & {
  total: number | null
  cartQuantity: number
}

export type ProductType = Omit<ProductsInCartQuery['products'][0], 'prices'> & {
  prices: ExtendedPrice[]
}

type Cart = {
  total: Ref<number>
  products: Ref<ProductType[]>
  getPrice: (productId: string, priceId: string) => ExtendedPrice | null
  updateProductPriceQuantity: (
    productId: string,
    priceId: string,
    quantity: number
  ) => Promise<any>
  clearCart: () => Promise<any>
  productInCart: (productId: string) => number
}

export const cartSymbol: InjectionKey<Cart> = Symbol('cart')
