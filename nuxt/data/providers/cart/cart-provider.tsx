import { defineComponent } from '@nuxtjs/composition-api'
import { provideCart } from '~/data/providers/cart/provideCart'

export const CartProvider = defineComponent({
  name: 'CartProvider',
  props: {},
  setup: (props, { slots }) => {
    provideCart()
    return () => slots.default && slots.default()
  }
})
