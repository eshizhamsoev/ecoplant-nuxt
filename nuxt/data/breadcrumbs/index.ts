import type { Ref } from '@nuxtjs/composition-api'
import { computed } from '@nuxtjs/composition-api'
import { Category, Product } from '~/_generated/types'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { categoryNameQuery } from '~/graphql/queries'

type CategoryBreadcrumbType = Pick<Category, 'id' | 'name'>
type ProductBreadcrumbType = Pick<Product, 'id' | 'name' | 'categoryId'>

const HOME_BREADCRUMB = {
  url: '/',
  name: 'Главная'
}

export function getCategoryBreadcrumb(
  category: Ref<CategoryBreadcrumbType | undefined | null>
) {
  return computed(() =>
    category.value
      ? [
          HOME_BREADCRUMB,
          {
            url: '/categories/' + category.value.id,
            name: category.value.name
          }
        ]
      : null
  )
}

export function getProductBreadcrumb(product: Ref<ProductBreadcrumbType>) {
  const queryVars = computed(() => ({
    id: product.value.categoryId
  }))
  const { result: categoryQuery } = useGqlQuery(categoryNameQuery, queryVars)
  const category = computed(
    () => categoryQuery.value && categoryQuery.value.category
  )
  return computed(() => {
    if (category.value) {
      return [
        ...(getCategoryBreadcrumb(category).value || []),
        {
          url: '/products/' + product.value.id,
          name: product.value.name
        }
      ]
    }
    return []
  })
}
