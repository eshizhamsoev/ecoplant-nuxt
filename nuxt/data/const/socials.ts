export const socials = [
  {
    link: 'https://vk.com/pitomniktuy',
    icon: 'vk'
  },
  {
    link: 'https://ok.ru/pitomniktu',
    icon: 'ok'
  },
  {
    link: 'https://www.facebook.com/tuivmoskve',
    icon: 'fb'
  },
  {
    link: 'http://instagram.com/pitomnikecoplant',
    icon: 'ig'
  },
  {
    link: 'https://t.me/eco_plant',
    icon: 'tg'
  },
  {
    link: 'https://www.youtube.com/channel/UCQBp8ZyXKSXP_z3K8pcws1Q/videos',
    icon: 'yt'
  }
]

export const socialsDictionary = Object.fromEntries(
  socials.map(({ link, icon }) => [icon, link])
)
