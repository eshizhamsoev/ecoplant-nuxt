import { defineComponent } from '@nuxtjs/composition-api'
import DefaultLayout from '~/components/layouts/default'

export default defineComponent({
  name: 'DesktopTinyLayout',
  setup: () => {
    return () => (
      <DefaultLayout>
        <nuxt />
      </DefaultLayout>
    )
  }
})
