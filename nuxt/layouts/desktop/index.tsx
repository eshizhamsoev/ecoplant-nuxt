import {
  computed,
  defineComponent,
  onMounted,
  ref
} from '@nuxtjs/composition-api'
import DefaultLayout from '~/components/layouts/default'
import { BlockDesktopComponents } from '~/components/blocks/block-components'
import { injectStyles } from '~/support/utils/injectStyle'
import { useClassNames } from '~/support/utils/bem-classnames'
import { BaseScrollToTop } from '~/components/base/base-scroll-to-top/base-scroll-to-top'
import style from './desktop.scss?module'

const cn = useClassNames('desktop', style)

export default injectStyles(
  style,
  defineComponent({
    name: 'DesktopLayout',
    setup: (props, { root }) => {
      const components = computed(() => {
        if (root.$route.path === '/') {
          return root.$accessor.components.homeLayoutComponents
        }
        return root.$accessor.components.layoutComponents
      })
      const layoutComponents = computed(() =>
        components.value.filter((v) => v.isDesktop)
      )

      const buttonShowHeight = ref(0)

      const scrollY = ref(0)

      onMounted(() => {
        const maxLowerBound = document.body.scrollHeight - window.innerHeight

        if (maxLowerBound) {
          buttonShowHeight.value =
            maxLowerBound > 1000 ? maxLowerBound - 1000 : maxLowerBound
        }

        window.addEventListener(
          'scroll',
          () => {
            scrollY.value = window.scrollY
          },
          { passive: true }
        )
      })

      return () => (
        <DefaultLayout class={cn()}>
          <BlockDesktopComponents components={layoutComponents.value} />
          {buttonShowHeight.value && scrollY.value > buttonShowHeight.value && (
            <BaseScrollToTop class={cn('to-top-button')} />
          )}
        </DefaultLayout>
      )
    }
  })
)
