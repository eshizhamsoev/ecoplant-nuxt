import { defineComponent } from '@nuxtjs/composition-api'
import DefaultLayout from '~/components/layouts/default'

export default defineComponent({
  name: 'OnlyContent',
  setup: () => {
    return () => (
      <DefaultLayout>
        <nuxt />
      </DefaultLayout>
    )
  }
})
