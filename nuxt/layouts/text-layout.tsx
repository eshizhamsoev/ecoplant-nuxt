import { defineComponent } from '@nuxtjs/composition-api'
import { TextLayout } from '~/components/layouts/text/text-layout'

export default defineComponent({
  name: 'TextLayout',
  setup: () => {
    return () => (
      <TextLayout>
        <nuxt />
      </TextLayout>
    )
  }
})
