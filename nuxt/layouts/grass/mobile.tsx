import { defineComponent } from '@nuxtjs/composition-api'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import { MobileGrassBackButton } from '~/components/layouts/grass/mobile/mobile-grass-back-button'
import { BaseIcon, BaseRoundButton } from '~/components/base'
import { Color } from '~/components/theme/theme-button'
import { BaseBottomFixesLine } from '~/components/base/base-bottom-fixes-line/base-bottom-fixes-line'
import { MobileButtonLine } from '~/components/layouts/mobile/mobile-button-line'
import DefaultLayout from '~/components/layouts/default'
import style from './layout-grass-mobile.scss?module'

const cn = useClassNames('layout-grass-mobile', style)

const classes = {
  back: cn('back'),
  close: cn('close')
}

export default injectStyles(
  style,
  defineComponent({
    name: 'MobileGrassPage',
    props: {},
    setup: (props, { root }) => {
      const goBack = () => root.$router.push('/')
      return () => (
        <DefaultLayout>
          <MobileGrassBackButton class={classes.back} onClick={goBack} />
          <BaseRoundButton
            class={classes.close}
            size={30}
            color={Color.red}
            onClick={goBack}
          >
            <BaseIcon name="times" width={16} />
          </BaseRoundButton>
          <nuxt />
          <BaseBottomFixesLine>
            <MobileButtonLine />
          </BaseBottomFixesLine>
        </DefaultLayout>
      )
    }
  })
)
