import {
  computed,
  defineComponent,
  useContext,
  useRoute
} from '@nuxtjs/composition-api'
import DefaultLayout from '~/components/layouts/default'
import { BlockMobileComponents } from '~/components/blocks/block-components'
import { useClassNames } from '~/support/utils/bem-classnames'
import { injectStyles } from '~/support/utils/injectStyle'
import style from './mobile.scss?module'

const cn = useClassNames('mobile', style)

const classes = {
  main: cn()
}

export default injectStyles(
  style,
  defineComponent({
    name: 'MobileLayout',
    setup: () => {
      const route = useRoute()
      const { $accessor } = useContext()

      const components = computed(() => {
        if (route.value.path === '/') {
          return $accessor.components.homeLayoutComponents
        }
        return $accessor.components.layoutComponents
      })
      const layoutComponents = computed(() =>
        components.value.filter((v) => v.isMobile)
      )

      return () => (
        <DefaultLayout class={classes.main}>
          <BlockMobileComponents components={layoutComponents.value} />
        </DefaultLayout>
      )
    }
  })
)
