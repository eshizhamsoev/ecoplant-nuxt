import NodeCache from 'node-cache'

const cacheStorage = new NodeCache({ stdTTL: 600, checkperiod: 720 })

export async function getOrUpdate(
  key: string,
  callback: () => Promise<any>
): Promise<any> {
  let value = cacheStorage.get(key)
  if (value) {
    return value
  }
  value = await callback()
  cacheStorage.set(key, value)
  return value
}
