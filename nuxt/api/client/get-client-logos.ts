import axios from 'axios'
import { ServerClientLogosData } from '~/common-types/server/collections/client-logos'

export function getClientLogos(): Promise<ServerClientLogosData> {
  if (process.server) {
    return import('../server/commands/getClientLogos').then((command) =>
      command.getClientLogos()
    )
  } else {
    return axios.get('/api/client-logos').then((res) => res.data)
  }
}
