import axios from 'axios'
import { ServerSpecialsData } from '~/common-types/server/collections/specials'

export function getSpecials(): Promise<ServerSpecialsData> {
  if (process.server) {
    return Promise.resolve({ images: [] })
  } else {
    return axios.get('/api/specials').then((res) => res.data)
  }
}
