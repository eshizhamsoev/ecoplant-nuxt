import { request } from '../request'
import { getOrUpdate } from '../../cache'

const CLIENT_LOGOS_URL = 'index.php?route=landing_api/client_logo'

const requestClientLogos = () => {
  return request.get(CLIENT_LOGOS_URL).then((res) => res.data)
}

export const getClientLogos = () => {
  return getOrUpdate('client-logos', requestClientLogos)
}
