import { createRegionRequest } from '../request'
import { getOrUpdate } from '../../cache'
import { ServerLandingConfig } from '~/common-types/server/landing-config'

const SPECIALS_URL = 'index.php?route=extension/feed/landing/specials'

const requestSpecials = (group: string) => {
  return createRegionRequest(group)
    .get(SPECIALS_URL)
    .then((res) => res.data)
}

export const getSpecials = (data: ServerLandingConfig) => {
  return getOrUpdate(`special-banners-${data.group}`, () =>
    requestSpecials(data.group)
  )
}
