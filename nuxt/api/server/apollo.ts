import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  IntrospectionFragmentMatcher
} from 'apollo-boost'
import fetch from 'node-fetch'

// @ts-ignore
import schemaIntrospection from '../../graphql.schema.json'

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: schemaIntrospection
})

const cache = new InMemoryCache({ fragmentMatcher })

export const createApolloClient = (host: string) =>
  new ApolloClient({
    link: new HttpLink({
      // @ts-ignore
      fetch,
      uri: 'http://localhost:4000',
      headers: {
        host
      }
    }),

    cache
  })
