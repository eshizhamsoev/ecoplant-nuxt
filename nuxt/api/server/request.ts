import axios from 'axios'
import { API_DOMAIN, REGION_API_DOMAINS } from './constants/API_DOMAIN'

export const request = axios.create({
  baseURL: API_DOMAIN
})

export function createRegionRequest(group: string) {
  return axios.create({
    baseURL: REGION_API_DOMAINS[group] || API_DOMAIN
  })
}
