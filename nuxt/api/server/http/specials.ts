import { ServerMiddleware } from '@nuxt/types'

import { getSpecials } from '../commands/getSpecials'

const specialHandler: ServerMiddleware = async (req, res) => {
  try {
    const specials = await getSpecials(req.data)
    res.write(JSON.stringify(specials))
  } catch (e) {
    res.statusCode = 400
  }
  res.end()
}
export default specialHandler
