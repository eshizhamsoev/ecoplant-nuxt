import { ServerMiddleware } from '@nuxt/types'

import { generateYandexXml } from '../../xml/generators/yandex'
import { convertForSmartBanners } from '../../xml/converters/smart-banners'
import { getFullTree } from '../../xml/queries/get-full-tree'
import { createApolloClient } from '../../apollo'

const smartBannersHandler: ServerMiddleware = async (req, res) => {
  try {
    const categoryIds = req.data.categoryIds
    const { data } = await getFullTree(
      createApolloClient(req?.headers.host || ''),
      categoryIds
    )
    res.setHeader('Content-Type', 'application/xml; charset=UTF-8')
    res.write(
      generateYandexXml(
        convertForSmartBanners(req.headers.host || '', data)
      ).toString()
    )
  } catch (e) {
    res.statusCode = 500
    res.end()
    throw e
  }
  res.end()
}
export default smartBannersHandler
