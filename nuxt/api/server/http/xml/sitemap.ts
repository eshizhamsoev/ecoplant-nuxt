import { ServerMiddleware } from '@nuxt/types'
import { gql } from 'graphql-tag'
import { SitemapStream } from 'sitemap'
import { IncomingMessage } from 'connect'
import { createApolloClient } from '../../apollo'

function getHostname(req: IncomingMessage) {
  const host = req.headers.host?.trim()
  return `https://${host}`
}

const staticPages = [
  {
    url: '/',
    priority: 1,
    changefreq: 'daily'
  },
  {
    url: '/privacy-policy',
    priority: 0.3,
    changefreq: 'monthly'
  },
  {
    url: '/checkout',
    priority: 0.3,
    changefreq: 'monthly'
  },
  {
    url: '/info/delivery',
    priority: 0.3,
    changefreq: 'monthly'
  },
  {
    url: '/info/payment',
    priority: 0.3,
    changefreq: 'monthly'
  },
  {
    url: '/info/return',
    priority: 0.3,
    changefreq: 'monthly'
  },
  {
    url: '/info/legal',
    priority: 0.3,
    changefreq: 'monthly'
  }
]

const sitemapHandler: ServerMiddleware = async (req, res) => {
  try {
    const sitemap = new SitemapStream({ hostname: getHostname(req) })
    sitemap.pipe(res).on('error', (e) => {
      throw e
    })
    res.setHeader('Content-Type', 'application/xml')
    const { data } = await createApolloClient(req?.headers.host || '').query({
      query: gql`
        query Catalog {
          categories {
            id
            name
            ... on ServiceCategory {
              services {
                id
                name
              }
            }
            ... on ProductCategory {
              products {
                id
                name
              }
            }
          }
        }
      `
    })
    const { categories } = data
    staticPages.forEach((staticPage) => {
      sitemap.write(staticPage)
    })
    for (const category of categories) {
      sitemap.write({
        url: `/categories/${category.id}`,
        priority: 0.7,
        changefreq: 'daily'
      })
      if (category.products) {
        for (const product of category.products) {
          sitemap.write({
            url: `/products/${product.id}`,
            priority: 0.5,
            changefreq: 'weekly'
          })
        }
      }
      if (category.services) {
        for (const service of category.services) {
          sitemap.write({
            url: `/products/${service.id}`,
            priority: 0.5,
            changefreq: 'weekly'
          })
        }
      }
    }
    sitemap.end()
  } catch (e) {
    res.statusCode = 500
    res.end()
    throw e
  }
}
export default sitemapHandler
