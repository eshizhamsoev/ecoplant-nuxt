import { ServerMiddleware } from '@nuxt/types'
import { generateYandexXml } from '../../xml/generators/yandex'
import {
  convertForSmartBanners,
  ProductRoutePrefixes
} from '../../xml/converters/smart-banners'
import { getFullTree } from '../../xml/queries/get-full-tree'
import { createApolloClient } from '../../apollo'

const mainProductsHandler: ServerMiddleware = async (req, res) => {
  try {
    const { data } = await getFullTree(
      createApolloClient(req?.headers.host || ''),
      [req.data.mainCategoryId]
    )

    res.setHeader('Content-Type', 'application/xml; charset=UTF-8')
    res.write(
      generateYandexXml(
        convertForSmartBanners(
          req.headers.host || '',
          data,
          ProductRoutePrefixes.CATEGORY_PRODUCT
        )
      ).toString()
    )
  } catch (e) {
    res.statusCode = 500
    res.end()
    throw e
  }
  res.end()
}
export default mainProductsHandler
