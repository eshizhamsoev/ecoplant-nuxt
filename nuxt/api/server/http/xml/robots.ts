import { ServerMiddleware } from '@nuxt/types'

const robotsHandler: ServerMiddleware = (req, res) => {
  try {
    res.write(
      `User-agent: *\nAllow: /\nSitemap: https://${req.headers.host?.trim()}/sitemap.xml`
    )
  } catch (e) {
    res.statusCode = 500
    res.end()
    throw e
  }
  res.end()
}
export default robotsHandler
