import { ServerMiddleware } from '@nuxt/types'

import { generateYandexOXml } from '../../xml/generators/yandex-o'
import { convertForYandexO } from '../../xml/converters/yandex-o'
import { getFullTree } from '../../xml/queries/get-full-tree'
import { createApolloClient } from '../../apollo'

const yandexOHandler: ServerMiddleware = async (req, res) => {
  try {
    const categoryIds = req.data.categoryIds
    const { data: catalog } = await getFullTree(
      createApolloClient(req?.headers.host || ''),
      categoryIds
    )
    res.setHeader('Content-Type', 'application/xml; charset=UTF-8')
    res.write(
      generateYandexOXml(convertForYandexO(catalog, req.data)).toString()
    )
  } catch (e) {
    res.statusCode = 500
    res.end()
    throw e
  }
  res.end()
}

export default yandexOHandler
