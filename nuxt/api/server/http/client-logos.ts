import { ServerMiddleware } from '@nuxt/types'

import { getClientLogos } from '../commands/getClientLogos'

const clientLogosHandler: ServerMiddleware = async (req, res) => {
  try {
    const clientLogos = await getClientLogos()
    res.write(JSON.stringify(clientLogos))
  } catch (e) {
    res.statusCode = 400
  }
  res.end()
}
export default clientLogosHandler
