import { ServerMiddleware } from '@nuxt/types'
import { request } from '../request'

const formHandler: ServerMiddleware = (req, res) => {
  const body: string[] = []
  // @ts-ignore
  if (req.method !== 'POST' || !req.data.configId) {
    res.statusCode = 405
    res.end()
    return
  }
  req.on('data', (chunk) => {
    body.push(chunk)
  })
  req.on('end', async () => {
    try {
      const data = JSON.parse(body.join(''))
      if (data.phone) {
        data.phone = data.phone.replace(/[_()\s-]/gi, '')
      }
      const ip =
        req.headers['client-ip'] ||
        req.headers['x-forwarded-for'] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress
      const remoteResponse = await request.post(
        // @ts-ignore
        `index.php?route=tool/form&site_id=${req.data.configId}`,
        data,
        {
          headers: {
            'Client-IP': ip,
            'Content-Type': 'application/json'
          }
        }
      )
      res.write(JSON.stringify(remoteResponse.data))
    } catch (e) {
      res.statusCode = 400
    }
    res.end()
  })
}
export default formHandler
