import { ServerMiddleware } from '@nuxt/types'

import axios from 'axios'

const attributeHandler: ServerMiddleware = async (req, res) => {
  try {
    const id = Number(req.url?.substring(1))
    // @ts-ignore
    if (isNaN(id) || !req.data.configId) {
      res.statusCode = 400
      res.end()
      return
    }
    const remoteResponse = await axios.post(
      'https://ecoplant-pitomnik.ru/index.php?route=extension/feed/landing/product_attributes',
      {
        id,
        // @ts-ignore
        config_id: req.data.configId
      },
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    )
    res.write(JSON.stringify(remoteResponse.data))
  } catch (e) {
    res.statusCode = 400
  }
  res.end()
}
export default attributeHandler
