let domain = 'https://ecoplant-pitomnik.ru/'

if (process.env.NODE_ENV === 'development') {
  if (process.env.TEST_API_DOMAIN) {
    domain = process.env.TEST_API_DOMAIN
  }
}

export const API_DOMAIN = domain

export const REGION_API_DOMAINS: { [key: string]: string } = {
  spb: 'https://spb.ecoplant-pitomnik.ru/'
}
