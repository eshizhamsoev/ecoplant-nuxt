import { Price } from '~/_generated/types'
import {
  Currency,
  Product as YandexProduct,
  YandexXmlData
} from '~/api/server/xml/types/yandex'
import {
  DataProduct,
  FullTreeResult
} from '~/api/server/xml/types/full-tree-result'

const defaultCurrency: Currency = {
  id: 'RUB',
  rate: 1,
  plus: 0
}

export enum ProductRoutePrefixes {
  PRODUCT = 'products',
  CATEGORY_PRODUCT = 'category-products',
  LANDING_PRODUCT = 'lp-products'
}

const defaultAvailability = true

function getImage(product: DataProduct): string {
  return product.images.length ? product.images[0].large : product.image
}

function clearText(text: string) {
  return text.replace('&nbsp;', ' ')
}

function getName(product: DataProduct, price: Price): string {
  let str = product.name
  if (price.size) {
    str += ' ' + price.size
  }
  if (price.price) {
    const priceText = price.price
      .toString()
      .replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, `$1 `)
    str += ` по ${priceText} рублей`
  }
  return str
}

type NotNullPrice = Price & {
  price: number
}

function filterPrice(price: Price): price is NotNullPrice {
  return price.price !== undefined && price.price !== null && price.price < 7000
}

export function convertForSmartBanners(
  hostname: string,
  config: FullTreeResult,
  linkType = ProductRoutePrefixes.PRODUCT
): YandexXmlData {
  const categories = config.productCategories.map(({ id, name }) => ({
    id,
    name
  }))
  const products: YandexProduct[] = []
  config.productCategories.forEach((category) => {
    category.products.forEach((product) => {
      const picture = getImage(product)
      if (!picture) {
        return
      }
      const yandexProducts = product.prices.filter(filterPrice).map((price) => {
        const name = clearText(getName(product, price))
        return {
          id: product.id + '-' + price.id,
          price: price.price,
          url: `https://${hostname}/${linkType}/${product.id}`,
          categoryId: category.id,
          picture,
          name,
          available: defaultAvailability,
          currencyId: defaultCurrency.id
        }
      })
      products.push(...yandexProducts)
    })
  })
  const currencies = [defaultCurrency]
  return {
    categories,
    products,
    currencies,
    shop: {
      name: 'Экоплант',
      url: hostname,
      company: 'Экоплант'
    }
  }
}
