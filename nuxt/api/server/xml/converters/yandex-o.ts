import { Condition, ContactsMethod, Offer } from '../types/yandex-o'
import { ServerLandingConfig } from '~/common-types/server/landing-config'
import {
  DataProduct,
  FullTreeResult
} from '~/api/server/xml/types/full-tree-result'
import { Price } from '~/_generated/types'

const CATEGORY = 'Рассада, саженцы, кустарники, деревья'

const CONDITION = Condition.INAPPLICABLE

const CONTACTS_METHOD = ContactsMethod.ONLY_PHONE

const MSK_NUMBER = '+74991106854'
const SPB_NUMBER = '+78123176076'

function checkPrice(price: Price) {
  return price.price && price.price > 0
}

function filterProduct(product: DataProduct): boolean {
  return product.images.length > 0 && product.prices.some(checkPrice)
}

function mapProduct(product: DataProduct): Omit<Offer, 'seller'> {
  const price = product.prices.find(checkPrice)
  return {
    id: product.id,
    title: `${product.name} ${price!.size}`,
    images: product.images.map(({ large }) => large),
    category: CATEGORY,
    condition: CONDITION,
    price: price!.price!
  }
}

export function convertForYandexO(
  catalog: FullTreeResult,
  { contacts: { address }, group }: ServerLandingConfig
): Offer[] {
  const seller = {
    contacts: {
      phone: group === 'spb' ? SPB_NUMBER : MSK_NUMBER,
      contactMethod: CONTACTS_METHOD
    },
    locations: [address]
  }
  return catalog.productCategories
    .reduce((result, category) => {
      return result.concat(
        category.products.filter(filterProduct).map(mapProduct)
      )
    }, [] as Omit<Offer, 'seller'>[])
    .map((product) => ({
      ...product,
      seller
    }))
}
