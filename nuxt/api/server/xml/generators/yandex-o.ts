import { create } from 'xmlbuilder'
import { Offer, Seller } from '../../xml/types/yandex-o'

export function generateYandexOXml(offers: Offer[]) {
  const root = create('feed', {
    version: '1.0',
    encoding: 'UTF-8',
    headless: false
  })
  root.attribute('version', 1)
  root
    .element('offers')
    .children.push(...offers.map((offer) => getProductXml(offer)))

  root.end()
  return root.parent
}

function getProductXml(offer: Offer) {
  const offerNode = create('offer')
  offerNode.element('id', {}, offer.id)
  offerNode.element('title', {}, offer.title)
  if (offer.description) {
    offerNode.element('description', {}, offer.description)
  }
  offerNode.element('condition', {}, offer.condition)
  offerNode.element('category', {}, offer.category)
  const imagesNode = offerNode.element('images')
  offer.images.forEach((image) => {
    imagesNode.element('image', {}, image)
  })
  if (offer.price) {
    offerNode.element('price', {}, offer.price)
  }
  if (offer.video) {
    offerNode.element('video', {}, offer.video)
  }
  offerNode.children.push(getSellerXml(offer.seller))
  return offerNode
}

function getSellerXml(seller: Seller) {
  const sellerNode = create('seller')
  const contactsNode = sellerNode.element('contacts')
  contactsNode.element('contact-method', {}, seller.contacts.contactMethod)
  contactsNode.element('phone', {}, seller.contacts.phone)
  const locations = sellerNode.element('locations')
  seller.locations.forEach((location) => {
    locations.element('location', {}, location)
  })
  return sellerNode
}
