import builder, { XMLElement } from 'xmlbuilder'
import { Category, Product, YandexXmlData } from '~/api/server/xml/types/yandex'

export function generateYandexXml(data: YandexXmlData) {
  const now = new Date()
  const date = now
    .toISOString()
    .replace(/T/, ' ')
    .replace(/:\d\d\..+/, '')
  const root = builder.create('yml_catalog', {
    version: '1.0',
    encoding: 'UTF-8',
    headless: false
  })
  root.attribute('date', date)
  const shop = root.element('shop')
  shop.element('name', data.shop.name)
  shop.element('url', data.shop.url)
  shop.element('company', data.shop.company)
  const categories = shop.element('categories')
  data.categories.forEach((category) => writeCategory(categories, category))

  const products = shop.element('offers')
  data.products.forEach((product) => writeProduct(products, product))
  root.end()
  return root.parent
}

function writeCategory(parentNode: XMLElement, category: Category) {
  parentNode.element('category', { id: category.id }, category.name)
}

function writeProduct(parentNode: XMLElement, product: Product) {
  const offer = parentNode.element('offer', {
    id: product.id,
    available: product.available
  })
  offer.element('name', {}, product.name)
  offer.element('url', {}, product.url)

  offer.element('picture', {}, product.picture)
  offer.element('price', {}, product.price)
  offer.element('currencyId', {}, product.currencyId)
  offer.element('categoryId', {}, product.categoryId)
}
