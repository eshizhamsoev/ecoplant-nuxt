import { gql, ApolloClient } from 'apollo-boost'
import { CategoryId } from '~/common-types/catalog'
import { FullTreeResult } from '~/api/server/xml/types/full-tree-result'

export function getFullTree(
  client: ApolloClient<any>,
  categoryIds: CategoryId[]
) {
  return client.query<FullTreeResult>({
    query: gql`
      query Catalog($ids: [ID!]!) {
        productCategories(ids: $ids) {
          id
          name
          products {
            id
            name
            image
            images {
              large
            }
            prices {
              id
              size
              price
            }
          }
        }
      }
    `,
    variables: {
      ids: categoryIds
    }
  })
}
