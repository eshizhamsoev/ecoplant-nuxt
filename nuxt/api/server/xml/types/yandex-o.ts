export enum ContactsMethod {
  ONLY_PHONE = 'only-phone',
  ONLY_CHAT = 'only-chat',
  ANY = 'any'
}

export type Seller = {
  contacts: {
    phone: string
    contactMethod: ContactsMethod
  }
  locations: string[]
}

export enum Condition {
  NEW = 'new',
  USED = 'used',
  INAPPLICABLE = 'inapplicable'
}

export type Offer = {
  id: string
  seller: Seller
  title: string
  description?: string
  video?: string
  condition: Condition
  category: string
  price: number
  images: string[]
}
