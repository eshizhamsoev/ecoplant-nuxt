import { Price, Product, ProductCategory } from '~/_generated/types'

export type DataProduct = Pick<Product, 'id' | 'name' | 'images' | 'image'> & {
  prices: Price[]
}

export type DataCategory = Pick<ProductCategory, 'id' | 'name'> & {
  products: DataProduct[]
}

export type FullTreeResult = {
  productCategories: DataCategory[]
}
