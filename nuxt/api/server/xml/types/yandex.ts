export type Category = {
  id: string
  name: string
}

export type Product = {
  id: string
  price: number
  url: string
  categoryId: string
  picture: string
  name: string
  available: boolean
  currencyId: string
}

export type Currency = {
  id: string
  rate: number
  plus: number
}

export type ShopData = {
  name: string
  url: string
  company: string
}

export type YandexXmlData = {
  categories: Category[]
  products: Product[]
  currencies: Currency[]
  shop: ShopData
}
