/* eslint-disable import/no-duplicates */
declare module '*.scss' {
  import { Style } from '~/utils/injectStyle'
  const content: Style
  export default content
}
declare module '*.css' {
  import { Style } from '~/utils/injectStyle'
  const content: Style
  export default content
}
declare module '*.scss?module' {
  import { StyleModule } from '~/utils/injectStyle'
  const content: StyleModule
  export default content
}

declare module '*.svg?inline'
declare module '*.png' {
  const value: any
  export = value
}
declare module '*.jpg' {
  const value: any
  export = value
}
declare module '*.svg' {
  const value: any
  export = value
}
