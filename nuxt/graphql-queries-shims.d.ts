
declare module '*/CartQuantity.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const CartQuantity: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/ClearCart.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const ClearCart: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/Awards.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const Awards: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/CareSeason.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const CareSeason: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/CareSeasons.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const CareSeasons: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/CartProducts.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const CartProducts: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/Categories.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const Categories: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/Category.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const Category: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/CategoryName.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const CategoryName: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/ClientPhotos.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const ClientPhotos: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/Clients.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const Clients: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/Documents.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const Documents: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/Exclusive.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const Exclusive: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/FeaturedProducts.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const FeaturedProducts: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/Gallery.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const Gallery: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/GalleryItem.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const GalleryItem: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/ProductAttributes.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const ProductAttributes: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/ProductInfo.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const ProductInfo: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/ProductInfoWithCategory.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const ProductInfoWithCategory: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/ProductsInCart.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const ProductsInCart: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/SearchProduct.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const SearchProduct: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/Service.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const Service: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/VarietyInfo.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const VarietyInfo: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/VideoReviews.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const VideoReviews: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/Villages.graphql' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const Villages: DocumentNode;

  export default defaultDocument;
}
    