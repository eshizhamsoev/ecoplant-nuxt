import { computed, Ref } from '@nuxtjs/composition-api'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { serviceQuery } from '~/graphql/queries'
import { ServiceId } from '~/common-types/catalog'

export function getServiceById(serviceId: Ref<ServiceId>) {
  const queryVars = computed(() => ({
    id: serviceId.value
  }))

  const { result, onResult, error, loading } = useGqlQuery(
    serviceQuery,
    queryVars
  )

  const service = computed(() => result.value && result.value.service)

  return { service, onResult, error, loading }
}
