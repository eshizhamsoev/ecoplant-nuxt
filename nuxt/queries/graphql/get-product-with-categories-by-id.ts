import { computed, Ref } from '@nuxtjs/composition-api'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { productInfoWithCategoryQuery } from '~/graphql/queries'
import { ProductId } from '~/common-types/catalog'

export function getProductWithCategoriesById(productId: Ref<ProductId>) {
  const queryVars = computed(() => ({
    id: productId.value
  }))

  const { result, onResult, error, loading } = useGqlQuery(
    productInfoWithCategoryQuery,
    queryVars
  )

  const product = computed(() => result.value && result.value.product)

  return { product, onResult, error, loading }
}
