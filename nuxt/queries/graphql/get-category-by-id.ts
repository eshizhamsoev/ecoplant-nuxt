import { computed, Ref } from '@nuxtjs/composition-api'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { categoryQuery } from '~/graphql/queries'
import { CategoryId } from '~/common-types/catalog'

export function getCategoryById(categoryId: Ref<CategoryId>, withMeta = false) {
  const queryVars = computed(() => ({
    id: categoryId.value,
    withMeta
  }))

  const { result, onResult, error, loading } = useGqlQuery(
    categoryQuery,
    queryVars
  )

  const category = computed(() => result.value && result.value.category)

  return { category, onResult, error, loading }
}
