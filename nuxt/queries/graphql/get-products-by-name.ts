import { computed, Ref } from '@nuxtjs/composition-api'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { searchProducts } from '~/graphql/queries'

export function getProductsByName(
  productName: Ref<string>,
  sendQuery: Ref<boolean>
) {
  const queryVars = computed(() => ({
    search: productName.value
  }))
  const queryOptions = computed(() => ({
    enabled: sendQuery.value
  }))

  const { result, onResult, error, loading } = useGqlQuery(
    searchProducts,
    queryVars,
    queryOptions
  )

  return { result, onResult, error, loading }
}
