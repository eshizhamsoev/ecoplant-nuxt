import { computed, Ref } from '@nuxtjs/composition-api'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { exclusiveQuery } from '~/graphql/queries'
import { ExclusiveId } from '~/common-types/catalog'

export function getExclusive(exclusiveId: Ref<ExclusiveId>) {
  const queryVars = computed(() => ({
    id: exclusiveId.value
  }))

  const { result, onResult, error, loading } = useGqlQuery(
    exclusiveQuery,
    queryVars
  )

  const exclusive = computed(() => result.value && result.value?.exclusive)

  return { exclusive, onResult, error, loading }
}
