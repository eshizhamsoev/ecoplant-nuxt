import { computed, Ref } from '@nuxtjs/composition-api'
import { useGqlQuery } from '~/support/hooks/useGqlQuery'
import { productQuery } from '~/graphql/queries'
import { ProductId } from '~/common-types/catalog'

export function getProductById(productId: Ref<ProductId>, withMeta = false) {
  const queryVars = computed(() => ({
    id: productId.value,
    withMeta
  }))

  const { result, onResult, error, loading } = useGqlQuery(
    productQuery,
    queryVars
  )

  const product = computed(() => result.value && result.value.product)

  return { product, onResult, error, loading }
}
