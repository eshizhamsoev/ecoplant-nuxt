import { DocumentNode } from 'graphql'
import { Categories } from './Categories.graphql'
import { Category } from './Category.graphql'
import { CategoryName } from './CategoryName.graphql'
import { SearchProduct } from './SearchProduct.graphql'
import { ProductInfo } from './ProductInfo.graphql'
import { ProductAttributes } from './ProductAttributes.graphql'
import { CartProducts } from './CartProducts.graphql'
import { ProductsInCart } from './ProductsInCart.graphql'
import { ProductInfoWithCategory } from './ProductInfoWithCategory.graphql'
import { Service } from './Service.graphql'
import { Exclusive } from './Exclusive.graphql'
import { VarietyInfo } from './VarietyInfo.graphql'
import { FeaturedProducts } from './FeaturedProducts.graphql'
import { CareSeason } from './CareSeason.graphql'
import { CareSeasons } from './CareSeasons.graphql'
import { VideoReviews } from './VideoReviews.graphql'
import { ClientPhotos } from './ClientPhotos.graphql'
import { Awards } from './Awards.graphql'
import { Villages } from './Villages.graphql'
import { Documents } from './Documents.graphql'
import { Clients } from './Clients.graphql'
import { Gallery } from './Gallery.graphql'
import { GalleryItem } from './GalleryItem.graphql'
import {
  CategoriesQuery,
  CategoriesQueryVariables,
  CategoryQuery,
  CategoryQueryVariables,
  ProductInfoQuery,
  ProductInfoQueryVariables,
  ProductAttributesQuery,
  ProductAttributesQueryVariables,
  CartProductsQuery,
  CartProductsQueryVariables,
  ProductsInCartQuery,
  ProductsInCartQueryVariables,
  CategoryNameQuery,
  CategoryNameQueryVariables,
  ProductInfoWithCategoryQuery,
  ProductInfoWithCategoryQueryVariables,
  ServiceQuery,
  ServiceQueryVariables,
  ExclusiveQueryVariables,
  ExclusiveQuery,
  VarietyInfoQuery,
  VarietyInfoQueryVariables,
  SearchProductQuery,
  SearchProductQueryVariables,
  FeaturedProductsQuery,
  FeaturedProductsQueryVariables,
  CareSeasonQuery,
  CareSeasonQueryVariables,
  CareSeasonsQuery,
  CareSeasonsQueryVariables,
  VideoReviewsQuery,
  VideoReviewsQueryVariables,
  ClientPhotosQuery,
  ClientPhotosQueryVariables,
  AwardsQuery,
  AwardsQueryVariables,
  VillagesQuery,
  VillagesQueryVariables,
  DocumentsQuery,
  DocumentsQueryVariables,
  ClientsQuery,
  ClientsQueryVariables,
  GalleryQuery,
  GalleryQueryVariables,
  GalleryItemQueryVariables,
  GalleryItemQuery
} from '~/_generated/types'

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export class Query<TResult, TVariables> {
  private readonly query: DocumentNode
  private readonly propsToKeys: (variables: TVariables) => string
  private readonly name: string

  constructor(
    query: DocumentNode,
    name: string,
    propsToKeys: (variables: TVariables) => string
  ) {
    this.query = query
    this.name = name
    this.propsToKeys = propsToKeys
  }

  public getQueryKey(variables: TVariables): string {
    return this.name + '-' + this.propsToKeys(variables)
  }

  public getQuery() {
    return this.query
  }
}

type QueryVars<T> = Omit<T, 'site'>

export const categoriesQuery = new Query<
  CategoriesQuery,
  QueryVars<CategoriesQueryVariables>
>(Categories, 'categories', (variables) => variables.ids?.toString() || '')

export const categoryQuery = new Query<
  CategoryQuery,
  QueryVars<CategoryQueryVariables>
>(Category, 'category', (variables) => variables.id?.toString())

export const categoryNameQuery = new Query<
  CategoryNameQuery,
  QueryVars<CategoryNameQueryVariables>
>(CategoryName, 'category-name', (variables) => variables.id?.toString())

export const productQuery = new Query<
  ProductInfoQuery,
  QueryVars<ProductInfoQueryVariables>
>(ProductInfo, 'product', (variables) => variables.id?.toString())

export const productAttributesQuery = new Query<
  ProductAttributesQuery,
  QueryVars<ProductAttributesQueryVariables>
>(ProductAttributes, 'product-attributes', (variables) =>
  variables.productId.toString()
)
export const varietyInfoQuery = new Query<
  VarietyInfoQuery,
  QueryVars<VarietyInfoQueryVariables>
>(VarietyInfo, 'variety-info', (variables) => variables.id?.toString())

export const cartProducts = new Query<
  CartProductsQuery,
  QueryVars<CartProductsQueryVariables>
>(CartProducts, 'cart-products', () => '')

export const productsInCart = new Query<
  ProductsInCartQuery,
  QueryVars<ProductsInCartQueryVariables>
>(ProductsInCart, 'products-in-cart', () => '')

export const searchProducts = new Query<
  SearchProductQuery,
  QueryVars<SearchProductQueryVariables>
>(SearchProduct, 'search-product', () => '')

export const productInfoWithCategoryQuery = new Query<
  ProductInfoWithCategoryQuery,
  QueryVars<ProductInfoWithCategoryQueryVariables>
>(ProductInfoWithCategory, 'product-info-with-category', (variables) =>
  variables.id?.toString()
)

export const serviceQuery = new Query<
  ServiceQuery,
  QueryVars<ServiceQueryVariables>
>(Service, 'service', (variables) => variables.id)

export const exclusiveQuery = new Query<
  ExclusiveQuery,
  QueryVars<ExclusiveQueryVariables>
>(Exclusive, 'service', (variables) => variables.id)

export const featuredProductsQuery = new Query<
  FeaturedProductsQuery,
  QueryVars<FeaturedProductsQueryVariables>
>(FeaturedProducts, 'featured-products', (variables) =>
  JSON.stringify(variables)
)

export const careSeasonQuery = new Query<
  CareSeasonQuery,
  QueryVars<CareSeasonQueryVariables>
>(CareSeason, 'care-season', (variables) => variables.id)

export const careSeasonsQuery = new Query<
  CareSeasonsQuery,
  QueryVars<CareSeasonsQueryVariables>
>(CareSeasons, 'care-seasons', () => '')

export const videoReviewsQuery = new Query<
  VideoReviewsQuery,
  QueryVars<VideoReviewsQueryVariables>
>(VideoReviews, 'video-reviews', () => '')

export const awards = new Query<AwardsQuery, QueryVars<AwardsQueryVariables>>(
  Awards,
  'awards',
  () => ''
)

export const clients = new Query<
  ClientsQuery,
  QueryVars<ClientsQueryVariables>
>(Clients, 'clients', () => '')

export const documents = new Query<
  DocumentsQuery,
  QueryVars<DocumentsQueryVariables>
>(Documents, 'document', () => '')

export const villages = new Query<
  VillagesQuery,
  QueryVars<VillagesQueryVariables>
>(Villages, 'villages', () => '')

export const clientPhotos = new Query<
  ClientPhotosQuery,
  QueryVars<ClientPhotosQueryVariables>
>(ClientPhotos, 'client-photos', () => '')

export const gallery = new Query<
  GalleryQuery,
  QueryVars<GalleryQueryVariables>
>(Gallery, 'gallery', (variables) => variables.id?.toString())

export const galleryItem = new Query<
  GalleryItemQuery,
  QueryVars<GalleryItemQueryVariables>
>(GalleryItem, 'galleryItem', (variables) => variables.id?.toString())
