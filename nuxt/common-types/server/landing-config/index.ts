import { CategoryId } from '~/common-types/catalog'
import { CategoryPageType } from '~/data/const/page-types/category'

type Meta = {
  title: string
  description: string
}

export type ServerLandingConfig = {
  categoryIds: CategoryId[]
  mainCategoryId: CategoryId
  configId: number
  type: string

  contacts: any
  layoutComponents: any
  homeLayoutComponents: any
  homeComponents: any
  grassComponents: any
  categoryComponent: CategoryPageType

  showExpandedInformation: boolean
  planting: {
    video: string
    poster: string
    buttonText: string
  }
  crmCode: string
  group: string
  grass: {
    availableProducts: any
    services: any
  }
  meta: {
    home: Meta
  }
}
