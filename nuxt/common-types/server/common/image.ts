export type ServerImage = string

export type ServerImageWithSize = {
  src: ServerImage
  width: number
  height: number
}
