import { ServerImageWithSize } from '~/common-types/server/common/image'

export type ClientLogo = {
  image: ServerImageWithSize
  title: string
  isState: boolean
}

export type ServerClientLogosData = {
  clientLogos: ClientLogo[]
}
