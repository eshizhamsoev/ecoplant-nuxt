import { ServerImageWithSize } from '~/common-types/server/common/image'

export type SpecialItem = {
  image: ServerImageWithSize
  title: string
}
export type ServerSpecialsData = {
  images: SpecialItem[]
}
