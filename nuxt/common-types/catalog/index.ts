import {
  Product,
  Category,
  Price,
  Service,
  Exclusive
} from '~/_generated/types'

export type CategoryId = Category['id']
export type ProductId = Product['id']
export type PriceId = Price['id']
export type ServiceId = Service['id']
export type ExclusiveId = Exclusive['id']

export const CategoryIdConstructor = String as () => CategoryId
export const ProductIdConstructor = String as () => ProductId
export const PriceIdConstructor = String as () => PriceId
