export type ExternalClientPhoto = {
  person: string
  role: string
  photo: {
    small: {
      src: string
      width: number
      height: number
    }
    large: string
    // eslint-disable-next-line camelcase
    sized_large: {
      src: string
      width: number
      height: number
    }
  }
}
export type ExternalClientPhotos = ExternalClientPhoto[]
