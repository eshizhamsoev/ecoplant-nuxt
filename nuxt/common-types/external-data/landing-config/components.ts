export type ServerComponent = {
  name: string
  properties: any
  key: string
}
