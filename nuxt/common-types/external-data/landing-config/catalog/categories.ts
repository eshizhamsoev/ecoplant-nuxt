export type ExternalCategoryId = number

export type ExternalCategory = {
  id: ExternalCategoryId
  parentId: ExternalCategoryId | 0
  name: string
  icon: string
  image: string
  items: number[]
  itemsType: 'products' | 'categories'
}

export type ExternalCategories = { [key: number]: ExternalCategory }
