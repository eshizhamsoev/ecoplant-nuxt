export type ExternalProductId = number

export type ExternalProduct = {
  id: ExternalProductId
  name: string
  variety: string
  image: string | null
  saleImage: string | null
  saleId: number | null
  images: string[]
  // eslint-disable-next-line camelcase
  large_images: string[]
  prices: number[]
  price: number
  label: string
  salePriceId: number
  showSalePrice: boolean
  isSameSize: boolean
  hasAttributes: boolean
  categoryId: number
}

export type ExternalProducts = { [key: number]: ExternalProduct }
