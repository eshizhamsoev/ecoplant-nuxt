export type ExternalProductPrice = {
  size: string | null
  price: number | string
  byRequest: boolean
  weekprice: boolean
}

export type ExternalProductPrices = { [key: number]: ExternalProductPrice }
