export type ExternalContacts = {
  phone: {
    number: string
    text: string
  }
  email: string
  legal: {
    info: string
    name: string
  }
  address: {
    text: string
    latitude: string
    longitude: string
  }
  whatsapp: string
}
