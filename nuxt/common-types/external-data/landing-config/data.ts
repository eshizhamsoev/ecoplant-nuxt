import { ServerComponent } from '~/common-types/external-data/landing-config/components'
import { ExternalProducts } from '~/common-types/external-data/landing-config/catalog/products'
import { ExternalCategories } from '~/common-types/external-data/landing-config/catalog/categories'
import { ExternalProductPrices } from '~/common-types/external-data/landing-config/catalog/prices'
import { ExternalContacts } from '~/common-types/external-data/landing-config/contacts'
import { ExternalClientPhotos } from '~/common-types/external-data/landing-config/client-photos'
import { LandingGroup } from '~/common-types/common/LandingGroup'

type HeadingMenuItem = {
  link: string
  name: string
}

type Heading = {
  image: string
  mobileImage: {
    src: string
  }
  type: string
  menuItems: HeadingMenuItem[]
}

type ExternalCatalog = {
  categories: ExternalCategories
  products: ExternalProducts
  prices: ExternalProductPrices
}

export type ExternalLandingConfig = {
  configId: number | string
  type: string
  group: LandingGroup
  heading: Heading
  senderUrl: string
  isNew: string
  pages: {
    home: {
      components: ServerComponent[]
    }
    category: {
      components: ServerComponent[]
    }
  }
  store: {
    componentData: {
      header: {
        logo: string
        heading: string
        leading: string
        siteName: string
      }
      footer: {
        logo: string
      }
      'client-photos'?: ExternalClientPhotos
      'review-block': any[]
    }
    modalData: {
      award: any[]
    }
    catalog: ExternalCatalog
  }
  contacts: ExternalContacts
  /**
   * @deprecated
   */
  yandexMetrika: any
  /**
   * @deprecated
   */
  mangoId: any
  /**
   * @deprecated
   */
  calltouch: any
  /**
   * @deprecated
   */
  googleTagCode: any
  /**
   * @deprecated
   */
  alphaTrackGtm: any
  /**
   * @deprecated
   */
  crmCode: any
  /**
   * @deprecated
   */
  urls: any
}
