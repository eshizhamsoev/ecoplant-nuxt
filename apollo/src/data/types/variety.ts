export type VarietyAttribute = {
  name: string;
  value: string;
};

export type Variety = {
  id: string;
  name: string;
  description: VarietyAttribute[];
};

export type Varieties = Map<string, Variety>;
export type VarietiesByProducts = Map<string, Variety[]>;
