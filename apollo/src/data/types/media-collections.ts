import {Award, ClientPhoto, VideoReview, Document, Client, Village, GalleryItem} from "~/generated/graphql";

export type MediaCollections = {
  videoReviews: VideoReview[];
  awards: Award[];
  clientPhotos: ClientPhoto[];
  documents: Document[];
  clients: Client[];
  villages: Village[],
  galleries: Map<string, GalleryItem[]>
  galleryItems: Map<string, GalleryItem>
};
