import type { Index } from "lunr";
import { OpencartCategory } from "~/data/sources/opencart/types/preparedData/opencartCategories";
import { OpencartProduct } from "~/data/sources/opencart/types/preparedData/opencartProducts";
import { PreparedPrices } from "~/data/sources/opencart/types/preparedData/preparedPrices";
import { Meta } from "~/generated/graphql";
import { Varieties, VarietiesByProducts } from "~/data/types/variety";

export type Product = OpencartProduct & {
  meta: Meta;
  description: string;
};
export type Category = OpencartCategory & {
  meta: Meta;
  description: string;
};

export type Catalog = {
  categories: Map<string, Category>;
  products: Map<string, Product>;
  featuredProducts: OpencartProduct["id"][];
  prices: PreparedPrices;
  varieties: Varieties;
  varietiesByProducts: VarietiesByProducts;
  searchIndex: Index;
};
