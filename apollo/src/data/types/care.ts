import { CareSeason } from "~/generated/graphql";

export type Care = {
  seasons: CareSeason[];
};
