import { ProductDescription } from "~/data/sources/database/products/models/ProductDescription";
import {
  OpencartProducts,
  OpencartProduct,
} from "~/data/sources/opencart/types/preparedData/opencartProducts";
import { Product } from "~/data/types/catalog";
import { decodeHtmlEntities } from "~/utils/decode-html-entities";

function joinProductWithDescription(
  product: OpencartProduct,
  productDescription: ProductDescription
): Product {
  return {
    ...product,
    description:
      productDescription && decodeHtmlEntities(productDescription.description),
    meta: {
      title:
        (productDescription && productDescription.metaTitle) || product.name,
      h1: (productDescription && productDescription.h1) || product.name,
      description:
        (productDescription && productDescription.metaDescription) || null,
    },
  };
}

export function joinProductsWithDescription(
  products: OpencartProducts,
  productDescriptions: ProductDescription[]
) {
  const productDescriptionsDictionary = productDescriptions.reduce(
    (dict, product) => {
      dict.set(product.productId, product);
      return dict;
    },
    new Map()
  );
  return Object.keys(products).reduce((map, id) => {
    const product = products[id];
    const productDescription = productDescriptionsDictionary.get(+id);
    map.set(id, joinProductWithDescription(product, productDescription));
    return map;
  }, new Map());
}
