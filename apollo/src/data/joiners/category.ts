import { decodeHtmlEntities } from "~/utils/decode-html-entities";
import { CategoryDescription } from "~/data/sources/database/categories/models/CategoryDescription";
import {
  OpencartCategories,
  OpencartCategory,
} from "~/data/sources/opencart/types/preparedData/opencartCategories";
import { Category } from "~/data/types/catalog";

function joinCategoryWithDescription(
  category: OpencartCategory,
  categoryDescription: CategoryDescription
): Category {
  return {
    ...category,
    description:
      categoryDescription &&
      decodeHtmlEntities(categoryDescription.description),
    meta: {
      title:
        (categoryDescription && categoryDescription.metaTitle) || category.name,
      h1: (categoryDescription && categoryDescription.h1) || category.name,
      description:
        (categoryDescription && categoryDescription.metaDescription) || null,
    },
  };
}

export function joinCategoriesWithDescription(
  categories: OpencartCategories,
  categoryDescriptions: CategoryDescription[]
) {
  const categoryDescriptionsDictionary = categoryDescriptions.reduce(
    (dict, category) => {
      dict.set(category.landingCatalogCategoryId, category);
      return dict;
    },
    new Map()
  );
  return Object.keys(categories).reduce((map, id) => {
    const category = categories[id];
    const categoryDescription = categoryDescriptionsDictionary.get(+id);
    map.set(id, joinCategoryWithDescription(category, categoryDescription));
    return map;
  }, new Map());
}
