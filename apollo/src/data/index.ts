import lunr from "lunr";
// @ts-ignore
import support from "lunr-languages/lunr.stemmer.support";
// @ts-ignore
import ru from "lunr-languages/lunr.ru";
// @ts-ignore
import multi from "lunr-languages/lunr.multi";
import {getOpencartCatalog} from "./sources/opencart/getOpencartCatalog";
import {
    getProductDescriptions,
    getCategoryDescriptions,
    getProductVarieties,
} from "~/data/sources/database";
import {CatalogType} from "~/data/const/catalog-type";
import {joinProductsWithDescription} from "~/data/joiners/product";
import {Catalog} from "~/data/types/catalog";
import {joinCategoriesWithDescription} from "~/data/joiners/category";
// import { AutoUpdatableStorage } from "~/storage/auto-updatable-storage";
import {SimpleStorage} from "~/storage/simple-storage";
import {
    createVarietiesByProductDictionary,
    createVarietiesDictionary,
    prepareVarieties,
} from "~/data/sources/database/preparers/varieties";
import {
    fetchAwards,
    fetchCare,
    fetchClientPhotos, fetchClients, fetchDocuments, fetchGallery,
    fetchReviews, fetchVillages,
} from "~/data/sources/opencart/fetch/fetch-config";
import {Care} from "~/data/types/care";
import {careConverter} from "~/data/sources/opencart/preparers/careConverter";
import {MediaCollections} from "~/data/types/media-collections";
import {GalleryItem} from "~/generated/graphql";

support(lunr);
ru(lunr);
multi(lunr);

async function fetchCatalog(type: CatalogType) {
    const {
        products: opencartProducts,
        categories: opencartCategories,
        prices,
        featuredProducts,
    } = await getOpencartCatalog(type);
    const categoryDescriptions = await getCategoryDescriptions(
        2,
        Object.keys(opencartCategories)
    );
    const categories = joinCategoriesWithDescription(
        opencartCategories,
        categoryDescriptions
    );
    const productIds = Object.keys(opencartProducts);
    const productDescriptions = await getProductDescriptions(2, productIds);
    const products = joinProductsWithDescription(
        opencartProducts,
        productDescriptions
    );
    const productVarieties = await getProductVarieties(productIds);
    const preparedVarieties = prepareVarieties(productVarieties);
    const varieties = createVarietiesDictionary(preparedVarieties);
    const varietiesByProducts =
        createVarietiesByProductDictionary(preparedVarieties);
    return {
        featuredProducts,
        products,
        categories,
        prices,
        varieties,
        varietiesByProducts,
        searchIndex: lunr(function () {
            // @ts-ignore
            this.use(lunr.multiLanguage("en", "ru"));
            this.ref("id");
            this.field("name");

            Array.from(products.values())
                .filter(({categoryId}) =>
                    ["1", "2", "3", "4", "5"].includes(categoryId)
                )
                .forEach((product) => {
                    this.add({id: product.id, name: product.name});
                });
        }),
    };
}

async function fetchMediaCollections(): Promise<MediaCollections> {
    const galleryIds = [11]
    const galleryPromises = galleryIds.map(id => {
        return fetchGallery(id).then(data => ({id: id.toString(), data}))
    })
    const [reviews, awards, clientPhotos, documents, clients, villages, galleries] = await Promise.all([
        fetchReviews(),
        fetchAwards(),
        fetchClientPhotos(),
        fetchDocuments(),
        fetchClients(),
        fetchVillages(),
        Promise.all(galleryPromises)
    ]);
    return {
        videoReviews: reviews,
        awards,
        clientPhotos,
        documents,
        clients,
        villages,
        galleries: new Map(galleries.map(item => [item.id, item.data])),
        galleryItems: new Map(
            galleries.reduce(
                (result, gallery) => result.concat(
                    gallery.data.map(item => [item.id, item])
                ),
                [] as [string, GalleryItem][]
            )
        )
    };
}

async function fetchCareSeasons(): Promise<Care> {
    const config = await fetchCare();
    return careConverter(config);
}

const catalogStorage = new SimpleStorage(fetchCatalog, 300);

export function getCatalog(type: CatalogType): Promise<Catalog> {
    return catalogStorage.get<Catalog>(type);
}

const careStorage = new SimpleStorage(fetchCareSeasons, 300);

export function getCare(): Promise<Care> {
    return careStorage.get<Care>(null);
}

const mediaCollectionsStorage = new SimpleStorage(fetchMediaCollections, 300);

export function getMediaCollections(): Promise<MediaCollections> {
    return mediaCollectionsStorage.get<MediaCollections>(null);
}
