export const SERVICE_CATEGORY_ID = "services";
export const servicesCategory = {
  id: SERVICE_CATEGORY_ID,
  name: "Ландшафтный дизайн",
  image: "https://ecoplant-pitomnik.ru/image/catalog/categories/land.jpg",
};
