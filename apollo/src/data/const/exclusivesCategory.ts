export const EXCLUSIVE_CATEGORY_ID = "exclusives";
export const exclusivesCategory = {
  id: EXCLUSIVE_CATEGORY_ID,
  name: "Эксклюзив",
  image: "https://ecoplant-pitomnik.ru/image/catalog/categories/exclusive.jpg",
};
