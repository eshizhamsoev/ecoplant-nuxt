type Sizes = Record<"small" | "large" | "original", string>;

const mainSizes: Sizes = {
  small: "small",
  large: "large",
  original: "original",
};

const MAIN_PATH =
  "https://ecoplant-pitomnik.ru/image/catalog/blocks/exclusives";

function range(q: number, start = 0) {
  return Array.from(Array(q).keys()).map((i) => i + start);
}

function makeImagePath(
  path: string,
  group: string,
  size: string,
  number: number
): string {
  return `${path}/${size}/${group}/${number}.jpg`;
}

type Image = {
  small: string;
  large: string;
  original: string;
};

function makeImage(
  path: string,
  group: string,
  number: number,
  sizes: Sizes
): Image {
  return Object.fromEntries<"small" | "large" | "original", string>(
    // @ts-ignore
    Object.entries(sizes).map(([sizeName, sizeWidth]) => [
      sizeName,
      makeImagePath(path, group, sizeWidth, number),
    ])
  );
}

function makeImages(
  path: string,
  group: string,
  quantity: number,
  start: number = 1,
  sizes: Sizes
): Image[] {
  return range(quantity, start).map((index) => {
    return makeImage(path, group, index, sizes);
  });
}

function makeMainImages(group: string, quantity: number, start: number = 1) {
  return makeImages(MAIN_PATH, group, quantity, start, mainSizes);
}

const items = [
  {
    id: "arches",
    name: "Арки",
  },
  {
    id: "blocks",
    name: "Блоки",
  },
  {
    id: "leafy-umbrellas",
    name: "Зонты лиственные",
  },
  {
    id: "coniferous-umbrellas",
    name: "Зонты хвойные",
  },
  {
    id: "living-stones",
    name: "Живые камни",
  },
  {
    id: "mountain-pines",
    name: "Сосны горные",
  },
  {
    id: "cubes",
    name: "Кубы",
  },
  {
    id: "trellises",
    name: "Шпалеры",
  },
  {
    id: "deciduous-balls",
    name: "Шары из лиственных",
  },
  {
    id: "coniferous-balls",
    name: "Шары из хвойных",
  },
  {
    id: "balloon-stem",
    name: "Шар на штамбе",
  },
  {
    id: "roofs",
    name: "Крыши",
  },
  {
    id: "mushroom",
    name: "Грибы",
  },
  {
    id: "pyramids",
    name: "Пирамиды",
  },
  {
    id: "cones",
    name: "Конусы",
  },
  {
    id: "gazebos",
    name: "Беседки",
  },
].sort((a, b) => (a.name > b.name ? 1 : -1));

export const exclusives = items.map((item) => ({
  ...item,
  images: makeMainImages(item.id, 5, 1),
}));
