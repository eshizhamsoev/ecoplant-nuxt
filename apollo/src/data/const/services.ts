import { CatalogType } from "~/data/const/catalog-type";

type Sizes = Record<"small" | "large" | "original", string>;

const mainSizes: Sizes = {
  small: "160",
  large: "496",
  original: "1200",
};

const additionalSizes: Sizes = {
  small: "160",
  large: "496",
  original: "960",
};

const MAIN_PATH =
  "https://ecoplant-pitomnik.ru/image/catalog/blocks/services/main/";

const ADDITIONAL_PATH =
  "https://ecoplant-pitomnik.ru/image/catalog/blocks/services/additional/";

function range(q: number, start = 0) {
  return Array.from(Array(q).keys()).map((i) => i + start);
}

function makeImagePath(
  path: string,
  group: string,
  size: string,
  number: number
): string {
  return `${path}/${size}/${group}/${number}.jpg`;
}

type Image = {
  small: string;
  large: string;
  original: string;
};

function makeImage(
  path: string,
  group: string,
  number: number,
  sizes: Sizes
): Image {
  return Object.fromEntries<"small" | "large" | "original", string>(
    // @ts-ignore
    Object.entries(sizes).map(([sizeName, sizeWidth]) => [
      sizeName,
      makeImagePath(path, group, sizeWidth, number),
    ])
  );
}

function makeImages(
  path: string,
  group: string,
  quantity: number,
  start: number = 1,
  sizes: Sizes
): Image[] {
  return range(quantity, start).map((index) => {
    return makeImage(path, group, index, sizes);
  });
}

function makeMainImages(group: string, quantity: number, start: number = 1) {
  return makeImages(MAIN_PATH, group, quantity, start, mainSizes);
}

function makeAdditionalImages(
  group: string,
  quantity: number,
  start: number = 1
) {
  return makeImages(ADDITIONAL_PATH, group, quantity, start, additionalSizes);
}

export function getServices(type: CatalogType) {
  return [
    {
      id: "1",
      name: "Ландшафтное проектирование",
      images: makeMainImages("landscape-design", 5, 1),
      content: [
        {
          text:
            "Каждый сад должен начинаться с проектных работ. Создание любого сада, парка, зеленой зоны придомовой территории или промышленного предприятия начинается с ландшафтного проектирования.\nЛандшафтное проектирование – это комплекс архитектурно-планировочных и объемно-пространственных решений для разработки методов художественного оформления открытого пространства. В работе над ландшафтными проектами наши специалисты руководствуются такими принципами как: функциональность (удобство, польза), конструктивность (прочность, экономичность), эстетичность и долговечность.\nПроект организации ландшафта является совокупностью проектов от дендроплана до проектов различных инженерных сетей и расчета объема и стоимости работ на участке. Основная проектная документация базируется на утвержденном заказчиком эскизе. Она представляет собой генеральный план, в состав которого входят следующая документация: план покрытий со схемами дорожных одежд и балансом территории, проект вертикальной планировки в красных отметках, разбивочный чертеж, схема освещения, посадочный чертеж, совмещенный с дендропланом, подробные планы цветников, рокариев, сметная стоимость ландшафтных работ и пояснительная записка, включающая в себя схемы строения конструктивных элементов ландшафта (дорожки, площадки, подпорные стенки, рокарии, цветники), флористическую легенду к дендроплану с иллюстрациями, габитусом и описанием высаживаемых сортов, рекомендации по посадке и уходу за насаждениями. Это серьезная и слаженная работа отдела сервиса и ландшафтного дизайна, в которой принимают участие как архитекторы и ландшафтные дизайнеры так и инженеры озеленения и дендрологи, итогом которой станет прекрасный сад, парк или любой другой городской объект.\nЛандшафтный проект дает возможность заказчику иметь полный контроль над всем комплексом работ по организации озеленения и благоустройства территории и избежать нежелательных финансовых затрат, возникающих при спонтанном и необдуманном проведении посадочных и иных работ на своем участке.",
        },
        {
          images: makeMainImages("landscape-design", 4, 2),
        },
      ],
      price:
        type === CatalogType.MSK
          ? "Бесплатно"
          : "15 соток - 10 000 р\n" +
            "15-25 соток - 15 000 р\n" +
            "25-35 соток 20 000 р\n" +
            "Более 35 соток по запросу",
    },
    {
      id: "4",
      name: "Газоны",
      images: makeMainImages("grass", 5, 1),
      content: [
        {
          text:
            "В пейзажных картинах газон является фоновой основой, выгодно подчеркивая красоту сада. Его устройство – это трудная, но приятная работа. Если сделать его по всем правилам, то газон будет радовать глаз и приносить пользу не один десяток лет. Газон объединяет и оттеняет все посадки, подчеркивая красоту деревьев, яркие краски и фактуру многолетних и однолетних растений.\nТакже газон имеет и огромное практическое значение: поддерживает оптимальный уровень влажности, создает благотворный микроклимат, закрепляет почву, задерживает шум и пыль, выделяет кислород.\nВсе газоны в зависимости от их функционального использования можно разделить на несколько типов – это универсальный газон, спортивно игровой, спортивный, партерный и газон для гольф полей.\nПо физиологическим особенностям травостоя газонные покрытия делят на универсальный, теневыносливый и засухоустойчивый газоны. Подкатегорией универсального можно назвать газон придорожный. В этом типе газона используются злаки исключительно выносливые и способные быстро регенерировать, чтоб выдерживать высокую техногенную нагрузку. Правда стоит отметить, что столь неординарные качества злаков этого типа газона достигаются с некоторой потерей декоративности.\nТакже отдельной категорией можно выделить мавританский газон, который принципиально отличается от всех газонов тем, что в его составе кроме злаков присутствуют цветущие многолетние и однолетние культуры, которые имитируют различные уголки ландшафта: лесную поляну, луговое разнотравье и др. Такие газоны не требуют регулярной стрижки и используются чаще всего в пейзажных садах. Технологии создания газонов также различны – это посевной газон, рулонный и газон устроенный гидро-посевом.\nПри выборе способа устройства специалист скорее всего посоветует вам рулонный газон, который сэкономит массу времени и средств, предотвратит в большой степени влияние возможных негативных погодных факторов. При соблюдении всех правил технологии укладки, такой газон вступит в эксплуатацию уже через три-четыре недели ( спортивный - несколько позже, что обусловлено высокими нагрузками этого газона ), в отличие от сеяного газона, который достигает требуемой декоративности только через несколько месяцев и порой требует проведение дополнительных работ по уходу за травостоем.\nТехнология гидропосева притеняется чаще всего в городском озеленении, при создании газонов в аэропортах, больших парках и на обочинах дорог. Даже если территория имеет очень сложный ландшафт (например, необходимо озеленить склон холма) то эта технология все равно подойдет.\nГазон является незаменимым компонентом ландшафта и от правильного выбора типа газона и технологии создания зависит успех создания всей ландшафтной композиции. Доверьте эту работу профессионалам и через короткий промежуток времени зеленый газон будет радовать глаз, принесет ощущение гармонии и комфорта на ваш участок.",
        },
        {
          images: makeAdditionalImages("grass", 3, 1),
        },
      ],
      price: "от 300 за м2",
    },
    {
      id: "5",
      name: "Автополив",
      images: makeMainImages("autowatering", 5, 1),
      content: [
        {
          text:
            "Любой профессиональный садовник скажет, что полив самая трудоемкая и ответственная работа в саду. И потому грамотно спроектированная и профессионально установленная автоматическая система полива способна сэкономить массу времени и сил, а зачастую и финансов владельцам загородных домов.\nВ зависимости от способа подачи воды растениям различаются дождевальные и капельные системы полива. Способ дождевания хорошо подходит для газона и клумб, так как дождеватели со специально подобранными форсунками равномерно распыляют воду над растениями. Радиус их действия зависит от конструкции самих дождевателей и может при необходимости изменяться вручную. Хотя такие случаи редки. Как правило во время наладки регулируются все форсунки в соответствии проекту и внешнее вмешательство может вывести из строя сам дождеватель и нанести вред растениям, которые либо получат излишек влаги, либо будут страдать от недостатка воды. Такая система состоит из источника воды, центральной поливочной магистрали, системы трубопроводов с электромагнитными клапанами и дождевателями, автоматического блока управления – контроллера и при необходимости дополняется датчиками дождя и влажности почвы. Все трубы, клапаны и дождеватели располагаются под землей и в момент начала работы под напором воды дождеватели приподнимаются над поверхностью.\nВ плотных растительных группах оптимально использовать капельный полив, т.е. каждое дерево или кустарник индивидуально поливается по специальным капиллярным трубкам. Такой способ полностью исключает ожоги растений и гарантирует необходимый объем воды растению. Также капельный полив рекомендуется для выращивания огородных продуктов, снижая риск заболеваний овощей различными болезнями, спровоцированными неправильным поливом, и облегчает процесс внесения удобрений.\nГораздо надежнее, когда организацией автоматической системы полива занимаются профессионалы: они учтут рельеф участка, характер и расположение «объектов» полива, источник воды и ее напор, и многие другие факторы.",
        },
        {
          images: makeAdditionalImages("autowatering", 4, 1),
        },
      ],
      price: "от 28 000 за 100 м2",
    },
    {
      id: "2",
      name: "Планировка участка",
      images: makeMainImages("site-layout", 5, 1),
      price: "от 125 за м2",
      content: [
        {
          text:
            "Вертикальной планировкой называется процесс создания нового рельефа путем преобразования старого. В первую очередь стоит сказать, что вертикальную планировку участка необходимо сначала качественно спроектировать. Проект должен обеспечить удобное передвижение по дорожкам, организовать правильный сток атмосферных вод и усилить перспективный эффект ландшафта объекта. Не спишите вызывать технику, перевозить существующий грунт с места на место особенно после выкопанного котлована или вообще увозить его с участка, вдруг он еще понадобится? Обратитесь к нам, и мы подскажем как сэкономить и организовать рельеф правильно.\n",
        },
        {
          images: makeMainImages("site-layout", 4, 2),
        },
      ],
    },
    {
      id: "3",
      name: "Мощение дорожек",
      images: makeMainImages("paving-paths", 5, 1),
      content: [
        {
          text:
            "Дорожки в саду являются полифункциональным объектом . Помимо коммуникативной и разделительной функций дорожки должны нести декоративную и эстетическую нагрузку в ландшафте. Из-за своих конструктивных особенностей садовые дорожки способны выполнять и еще одну важнейшую функцию – водоотведение. Для этого их поверхности придают небольшой уклон, обеспечивающий стекание дождевой и талой воды в дренажный слой основания дорожки.\nВ соответствии с функциональными потребностями мощение дорожек можно разделить на три типа: мягкий, твердый и комбинированный типы мощения.\nТ.н. мягкое мощение используется в местах не интенсивного движения – в индивидуальных садах, для маршрутов здоровья медицинских учреждениях, базах отдыха, санаториях и пр. Это один из наиболее экологически чистых типов садовых покрытий, не препятствующий водо- и воздухообмену почвы. Выполняется такое мощение из различных видов галька, гравия, гранитного отсева, крупнозернистого песка, коры или щепы. Не смотря на то, что тип мощения называют мягким, дорожка устраивается на хорошо затрамбованное основание, устанавливается бордюр и сама засыпка проводится слоями, используя разную фракцию выбранного для покрытия материала.",
        },
        {
          images: makeAdditionalImages("paving-paths/1", 3, 1),
        },
        {
          text:
            "Твердый тип мощения используется в местах, больших нагрузок, где большой поток пешеходов, на парковках на отмостке вокруг дома и т.д. Этот тип долговечен и практичен. Такие дорожки выполняются из натурального камня, тротуарной плитки и в последнее время очень популярны в строительстве дорожек деревянные настилы – деки. Существует несколько технологий мощения дорожек такого типа: на бетонное основание, на сухую строительную смесь и на песок. Наиболее популярно сейчас делать дорожки на сухую строительную смесь. Бетонное основание под мощение делается в местах проезда автотранспорта, игровых площадках и местах массового отдыха.",
        },
        {
          images: makeAdditionalImages("paving-paths/2", 3, 1),
        },
        {
          text:
            "Комбинированные дорожки предполагаю сочетание твердых и мягких поверхностей. Технология строительства такая же как и при устройстве дорожки на сухую строительную смесь.\nТакже можно выделить особый тип мощения с использованием газонной решетки. Экологичный, удобный и не дорогой, в последнее время такой тип мощения дорожек и площадок набирает популярность.\nТакое покрытие выдерживает нагрузки даже малотоннажных грузовых автомобилей и в то же время мягкая природная текстура придает этим дорожкам и площадкам особую привлекательность.\n\nПравильно спланированные дорожки и площадки на участке – это залог комфортного отдыха в саду владельцев усадьбы и их гостей. Стильное покрытие - это элемент авторского сада. Безусловно, некоторые виды покрытия не дешевы, но совсем не обязательно делать всю сеть дорожек из дорогостоящих материалов, возможно присутствие на развилках, отдельных площадках определенных выделяющихся из общего фона дорожного покрытия, элементов, сделает ваш сад оригинальным и запоминающимся. Выполненные по всем правилам технологии строительства, они на долгие годы станут самыми нетребовательными в уходе элементами сада.",
        },
        {
          images: makeAdditionalImages("paving-paths/3", 3, 1),
        },
      ],
      price: "от 750 за м2",
    },
    {
      id: "7",
      name: "Сервисный уход",
      images: makeMainImages("service-care", 5, 1),
      content: [
        {
          text:
            "Обслуживание зеленых насаждений служит целям поддержания растительности на участке в эстетически привлекательном и здоровом виде. Все элементы, составляющие ландшафт, будь то деревья, кустарники, цветущие растения и газон, либо садовые дорожки и площадки, пруды и водоемы – все они требуют ухода для поддержания качественного внешнего вида. Кроме этих работ необходимо выполнять профилактический осмотр и техническое обслуживание систем полива и освещения, систематически проводить чистку фильтров в прудах и фонтанах.\nСервисные работы по уходу за зелеными насаждениями включают в себя стрижку газона, внесение удобрений, аэрацию газона и его вычесывание, профилактические опрыскивания от вредителей и болезней, прополку и рыхление цветников, обрезку больных и погибших ветвей, удаление отцветших соцветий, сезонную обрезку в плодовых садах, виноградниках и ягодниках, формовочную и ремонтную стрижку живых изгородей, ремонт газона и цветников, уборку растительных остатков с участка, полив, где есть необходимость в дополнительном поливе, ране-весеннее притенение и подготовка растений к зиме.\nВ комплекс работ по сервисному обслуживанию также входят очистка прудов и бассейнов от донных отложений, замена фильтрующих элементов в системах очистки воды и техническое обслуживание системы уличного освещения. Также наши специалисты выполнят мелкий ремонт дорожек, площадок и подпорных стенок.",
        },
        {
          images: makeAdditionalImages("service-care", 4, 1),
        },
        {
          text:
            "Мы заключаем договора на сервисные работы на любой период времени, как единоразовое, таки и круглогодичное обслуживание.\nВсе строительные работы по устройству дорожек, площадок, водоемов, фонтанов, строительству беседок и барбекю, монтажным работам систем полива и освещения, выполненные нашими специалистами, имеют гарантийный срок качества от 3-х лет и более. Гарантийный срок при устройстве зеленых насаждений – один год. В течении этого периода бригада сервисного обслуживания будет регулярно проводить комплекс мер по уходу за гарантийным объектом. Все нюансы сервисного обслуживания в гарантийный срок обговариваются и фиксируются в договорах подряда.",
        },
        {
          images: makeMainImages("service-care", 4, 2),
        },
      ],
      price: "от 450 за 100 м2",
    },
    {
      id: "6",
      name: "Освещение",
      images: makeMainImages("lighting", 5, 1),
      content: [
        {
          text:
            "Расширение представлений о возможностях современного дизайна в значительной степени связано с такой областью как дизайн света. За последнее время он изменил облик многих городов и ландшафтных объектов в вечерние часы и во многом способствовал увеличению времени использования таких объектов в темное время суток. Не отставая от подсветки архитектурной , подсветка вечернего пейзажа стала демонстрировать свои подходы, требующие альтернативного дизайна ввиду отличия самого объекта освещения.\nЛандшафтное освещение имеет несколько направлений - это: обозначение с помощью света доминирующих направлений пешеходного движения, цветовое акцентирование отдельных фрагментов ландшафта, разграничение с помощью света пространств пешеходного и транспортного назначения, подсветка ландшафтных доминант – деревьев, водоемов, малых архитектурных форм и т.д., устройство световых инсталяций.\nВесь набор средств ландшафтной подсветки можно условно разделить на несколько световых ярусов:",
        },
        {
          images: makeMainImages("lighting", 4, 2),
        },
        {
          text:
            "Следование логике гармонии в создании световых композиций с грамотной расстановкой смысловых акцентов и с сдержанным использованием динамических эффектов составляет наиболее современную версию реализации новых технологических возможностей в области светового дизайна.",
        },
      ],
      price: "от 500 за м п",
    },
    {
      id: "8",
      name: "Дренаж",
      images: makeMainImages("drainage", 5, 1),
      content: [
        {
          text:
            "Дренажом называется система подземных трубопроводов или каналов (дрен), которые используются для отведения излишков влаги. Собранная вода выводится за пределы осушаемой территории или аккумулируется в накопителях. Таким образом, дренажная система участка – это искусственно созданный водоток в почве, необходимый для обустройства большинства участков. Использование дренажа позволяет предотвратить от разрушения фундамент зданий, а также создает нормальные условия для произрастания растений. Ливневая система дополнительно собирает воду с поверхности кровель, площадок и мощений и также уходит в дренажные колодцы.\n" +
            "Существует понимание пристенного дренажа. Служащего для сбора и отведения воды от фундамента здания и исключения подтопления подвалов.\n" +
            "Сложность всех технических приемов обусловлена грамотно сделанной вертикальной планировкой обьекта. Различным уровнем земли по всей территории, для исключения остаточных мест замокания и застоя воды.\n" +
            "Настоятельно рекомендуем перед началом работ подготовить грамотную рабочую документацию, для исключения неожиданных расходов и переделок.",
        },
        {
          images: makeMainImages("drainage", 4, 2),
        },
      ],
      price: "от 350 за м п",
    },
  ];
}
