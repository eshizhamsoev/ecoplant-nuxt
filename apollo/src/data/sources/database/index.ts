import { Model, knexSnakeCaseMappers } from "objection";
import Knex from "knex";
import { config } from "~/knexfile";

export {
  getProductDescriptions,
  getProductVarieties,
} from "./products/queries";
export { getCategoryDescriptions } from "./categories/queries";
// Initialize knex.

const knex = Knex({ ...config, ...knexSnakeCaseMappers() });

Model.knex(knex);
