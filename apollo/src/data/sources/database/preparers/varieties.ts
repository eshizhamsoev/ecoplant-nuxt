import { ProductVariety } from "~/data/sources/database/products/models/ProductVariety";
import { Variety, Varieties, VarietiesByProducts } from "~/data/types/variety";

type PreparedVariety = Variety & {
  productId: string;
};

export function prepareVariety(variety: ProductVariety): PreparedVariety {
  return {
    id: variety.productVarietyId.toString(),
    productId: variety.productId.toString(),
    name: variety.name,
    description: JSON.parse(variety.description),
  };
}

export function prepareVarieties(
  varieties: ProductVariety[]
): PreparedVariety[] {
  return varieties.map(prepareVariety);
}

export function createVarietiesDictionary(
  varieties: PreparedVariety[]
): Varieties {
  return new Map(varieties.map((variety) => [variety.id, variety]));
}

export function createVarietiesByProductDictionary(
  varieties: PreparedVariety[]
): VarietiesByProducts {
  return varieties.reduce((map, variety) => {
    const key = variety.productId;
    const productVarieties: Variety[] = map.get(key) || [];
    productVarieties.push(variety);
    map.set(key, productVarieties);
    return map;
  }, new Map());
}
