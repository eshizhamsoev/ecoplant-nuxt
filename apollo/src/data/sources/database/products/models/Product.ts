import { Model } from "objection";
import { ProductDescription } from "~/data/sources/database/products/models/ProductDescription";

export class Product extends Model {
  productId!: number;

  // Table name is the only required property.
  static tableName = "oc_product";

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.

  // This object defines the relations to other models. The relationMappings
  // property can be a thunk to prevent circular dependencies.
  static relationMappings = () => ({
    descriptions: {
      relation: Model.HasManyRelation,
      // The related model.
      modelClass: ProductDescription,

      join: {
        from: "oc_product.product_id",
        to: "oc_product_description.product_id",
      },
    },
  });
}
