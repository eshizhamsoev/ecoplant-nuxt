import { Model } from "objection";

export class ProductVariety extends Model {
  productVarietyId!: number;
  productId!: number;
  name!: string;
  description!: string;

  // Table name is the only required property.
  static tableName = "oc_product_variety";
}
