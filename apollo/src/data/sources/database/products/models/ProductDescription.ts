import { Model } from "objection";

export class ProductDescription extends Model {
  productId!: number;
  languageId!: number;
  name!: string;
  description!: string;
  metaTitle!: string;
  metaDescription!: string;
  h1!: string;

  // Table name is the only required property.
  static tableName = "oc_product_description";
}
