import { ProductDescription } from "~/data/sources/database/products/models/ProductDescription";
import { ProductVariety } from "~/data/sources/database/products/models/ProductVariety";

export function getProductDescriptions(
  languageId: number,
  productIds: string[]
): Promise<ProductDescription[]> {
  return ProductDescription.query()
    .alias("descriptions")
    .where("descriptions.language_id", "=", languageId)
    .whereIn("descriptions.product_id", productIds);
}

export function getProductVarieties(
  productIds: string[]
): Promise<ProductVariety[]> {
  return ProductVariety.query()
    .alias("variety")
    .whereIn("variety.product_id", productIds)
    .orderBy("priority", "desc")
    .orderBy("name")
    .orderBy("productVarietyId");
}
