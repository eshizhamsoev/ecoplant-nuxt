import { CategoryDescription } from "~/data/sources/database/categories/models/CategoryDescription";

export function getCategoryDescriptions(
  languageId: number,
  categoryIds: string[]
): Promise<CategoryDescription[]> {
  return CategoryDescription.query()
    .alias("descriptions")
    .where("descriptions.language_id", "=", languageId)
    .whereIn("descriptions.landing_catalog_category_id", categoryIds);
}
