import { Model } from "objection";

export class CategoryDescription extends Model {
  landingCatalogCategoryId!: number;
  languageId!: number;
  description!: string;
  metaTitle!: string;
  metaDescription!: string;
  h1!: string;

  // Table name is the only required property.
  static tableName = "oc_landing_catalog_category_description";
}
