import { Model } from "objection";
import { CategoryDescription } from "~/data/sources/database/categories/models/CategoryDescription";

export class Category extends Model {
  productId!: number;

  // Table name is the only required property.
  static tableName = "oc_landing_catalog_category";

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.

  // This object defines the relations to other models. The relationMappings
  // property can be a thunk to prevent circular dependencies.
  static relationMappings = () => ({
    descriptions: {
      relation: Model.HasManyRelation,
      // The related model.
      modelClass: CategoryDescription,

      join: {
        from: "oc_product.product_id",
        to: "oc_product_description.product_id",
      },
    },
  });
}
