import Landing from "~/data/sources/database/landings/models/Landing";

export function getLandingConfig(domain: string) {
  return Landing.query()
    .select(["landing_id", "use_spb_price"])
    .where(function () {
      this.where("full_url", "like", "%" + domain + "%").orWhereIn(
        "landing_id",
        function () {
          this.select("landing_id")
            .from("oc_landing_alias")
            .where("domain", "like", "%" + domain + "%");
        }
      );
    })
    .orderBy("landing_id")
    .first();
}
