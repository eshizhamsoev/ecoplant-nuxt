import { Model } from "objection";
import Landing from "./Landing";

export default class LandingAlias extends Model {
  landing_alias_id!: number;
  domain!: string;
  landing!: Landing;

  // Table name is the only required property.
  static tableName = "oc_landing_alias";

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.

  // This object defines the relations to other models. The relationMappings
  // property can be a thunk to prevent circular dependencies.
  static relationMappings = () => ({
    owner: {
      relation: Model.BelongsToOneRelation,
      // The related model.
      modelClass: Landing,

      join: {
        from: "oc_landing_alias.landing_id",
        to: "oc_landing.landing_id",
      },
    },
  });
}
