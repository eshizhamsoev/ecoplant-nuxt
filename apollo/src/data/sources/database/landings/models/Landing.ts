import { Model, snakeCaseMappers } from "objection";
import LandingAlias from "./LandingAlias";

export default class Landing extends Model {
  landingId!: number;
  fullUrl!: string;
  useSpbPrice!: boolean;

  aliases!: LandingAlias[];

  // Table name is the only required property.
  static tableName = "oc_landing";

  // Optional JSON schema. This is not the database schema! Nothing is generated
  // based on this. This is only used for validation. Whenever a model instance
  // is created it is checked against this schema. http://json-schema.org/.

  // This object defines the relations to other models. The relationMappings
  // property can be a thunk to prevent circular dependencies.
  static relationMappings = () => ({
    aliases: {
      relation: Model.HasManyRelation,
      // The related model.
      modelClass: LandingAlias,

      join: {
        from: "oc_landing.landing_id",
        to: "oc_landing_alias.landing_id",
      },
    },
  });

  static get columnNameMappers() {
    return snakeCaseMappers();
  }
}
