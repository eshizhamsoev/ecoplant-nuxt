import { OpencartCategories } from "~/data/sources/opencart/types/preparedData/opencartCategories";
import {
  OpencartProduct,
  OpencartProducts,
} from "~/data/sources/opencart/types/preparedData/opencartProducts";
import { PreparedPrices } from "~/data/sources/opencart/types/preparedData/preparedPrices";

export type Catalog = {
  categories: OpencartCategories;
  products: OpencartProducts;
  prices: PreparedPrices;
  featuredProducts: OpencartProduct["id"][];
};
