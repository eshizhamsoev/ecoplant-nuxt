export interface PreparedProductImage {
  small: string;
  large: string;
}

export interface SquaredImage {
  thumb: string;
  original: string;
}

export interface OpencartProduct {
  id: string;
  name: string;
  image: string;
  squaredImage: SquaredImage;
  saleId: number | null;
  images: PreparedProductImage[];
  prices: string[];
  label: string;
  isSameSize: boolean;
  hasAttributes: boolean;
  categoryId: string;
  salePriceId: number;
  showSalePrice: boolean;
}

export type OpencartProducts = { [key: string]: OpencartProduct };
