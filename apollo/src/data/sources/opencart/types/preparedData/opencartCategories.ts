export interface OpencartCategory {
  id: string;
  parentId: number;
  name: string;
  image: string;
  icon: string | null;
  items: string[];
  limit: number;
}

export type OpencartCategories = { [key: string]: OpencartCategory };
