export interface PreparedPrice {
  id: string;
  size: string | null;
  price: number | null;
  oldPrice: number | null;
  weekPrice: boolean;
}

export type PreparedPrices = { [key: string]: PreparedPrice };
