export type RemoteCategoryId = number;

export type RemoteProductId = number;

type SquaredImage = {
  thumb: string;
  original: string;
};

export type RemoteProduct = {
  id: RemoteProductId;
  name: string;
  variety: string;
  image: string | null;
  squaredImage: SquaredImage | null;
  saleImage: string | null;
  saleId: number | null;
  images: string[];
  // eslint-disable-next-line camelcase
  large_images: string[];
  prices: number[];
  price: number;
  label: string;
  salePriceId: number;
  showSalePrice: boolean;
  isSameSize: boolean;
  hasAttributes: boolean;
  categoryId: number;
};

export type RemoteProducts = { [key: number]: RemoteProduct };

export type RemoteProductPrice = {
  size: string | null;
  price: number | string;
  byRequest: boolean;
  weekprice: boolean;
};

export type RemoteProductPrices = { [key: number]: RemoteProductPrice };

export type RemoteCategory = {
  id: RemoteCategoryId;
  parentId: RemoteCategoryId | 0;
  name: string;
  icon: string;
  image: string;
  items: number[];
  itemsType: "products" | "categories";
};

export type RemoteCategories = { [key: number]: RemoteCategory };

type RemoteCatalog = {
  categories: RemoteCategories;
  products: RemoteProducts;
  prices: RemoteProductPrices;
  featuredProducts: number[];
};

export type RemoteLandingConfig = {
  store: {
    catalog: RemoteCatalog;
  };
};

export type RemoteCareSeasonItem = {
  title: string;
  text: string;
  images: string[];
};

export type RemoteCareSeason = {
  id: string;
  title: string;
  items: RemoteCareSeasonItem[];
};

export type RemoteCareConfig = {
  seasons: RemoteCareSeason[];
};

export type YoutubeVideo = {
  type: "youtube";
  code: string;
};

export type FileVideo = {
  type: "video";
  source: string;
};

export type ImageWithSize = {
  src: string;
  width: number;
  height: number;
};

export type ThumbnailImage = {
  thumb: ImageWithSize;
  large: ImageWithSize;
};

export type VideoReview = {
  person: string;
  role: string;
  video: YoutubeVideo | FileVideo;
  poster: {
    mobile: ImageWithSize;
    desktop: ImageWithSize;
  };
};

export type VideoReviews = VideoReview[];

export type ClientPhoto = {
  image: ThumbnailImage;
  name: string;
};

export type Award = {
  image: ThumbnailImage;
  name?: string | null;
};
