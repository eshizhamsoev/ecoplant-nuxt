import {
  convertCategories,
  filterCategories,
} from "./preparers/categoryConverter";
import {
  convertProducts,
  filterProductsByCategories,
} from "./preparers/productConverter";
import {
  convertPrices,
  filterPricesByProducts,
} from "./preparers/priceConverter";
import { CatalogType } from "~/data/const/catalog-type";
import { availableCategories } from "~/data/const/available-categories";
import { fetchConfig } from "~/data/sources/opencart/fetch/fetch-config";
import { Catalog } from "~/data/sources/opencart/types/Catalog";

// export function getCatalog(type: CatalogType): Promise<Catalog> {
//   return getFromCacheOrUpdate(
//     getCatalogCacheName(type),
//     async () => {
//       const config = await fetchConfig(type);
//       const categories = convertCategories(
//         filterCategories(config.store.catalog.categories, availableCategories)
//       );
//       const products = convertProducts(
//         filterProductsByCategories(config.store.catalog.products, categories)
//       );
//
//       const prices = convertPrices(
//         filterPricesByProducts(config.store.catalog.prices, products)
//       );
//       return { categories, prices, products };
//     },
//     180
//   );
// }

export async function getOpencartCatalog(type: CatalogType): Promise<Catalog> {
  const config = await fetchConfig(type);
  const categories = convertCategories(
    filterCategories(config.store.catalog.categories, availableCategories)
  );

  const products = convertProducts(
    filterProductsByCategories(config.store.catalog.products, categories)
  );

  const prices = convertPrices(
    filterPricesByProducts(config.store.catalog.prices, products)
  );
  return {
    categories,
    prices,
    products,
    featuredProducts:
      config.store.catalog.featuredProducts?.map((id) => id.toString()) || [],
  };
}
