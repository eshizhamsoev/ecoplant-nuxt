import { OpencartCategories } from "../types/preparedData/opencartCategories";
import { RemoteCategories } from "../types/remoteData";
import { getImageUrl } from "~/data/sources/opencart/utils/getImageUrl";

export function convertCategories(data: RemoteCategories): OpencartCategories {
  const categories = {} as OpencartCategories;
  Object.keys(data).forEach((categoryId) => {
    const id = categoryId;
    const category = data[+id];
    categories[id] = {
      id,
      parentId: category.parentId,
      name: category.name,
      image: getImageUrl(category.image),
      icon: null,
      limit: 17,
      items: category.items.map((id) => id.toString()),
    };
  });
  return categories;
}

export function filterCategories(
  data: RemoteCategories,
  availableCategories: number[]
): RemoteCategories {
  return Object.fromEntries(
    Object.entries(data).filter(([categoryId]) =>
      availableCategories.includes(+categoryId)
    )
  );
}
