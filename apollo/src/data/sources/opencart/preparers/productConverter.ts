import { decodeHtmlEntities } from "~/utils/decode-html-entities";
import {
  RemoteProduct,
  RemoteProducts,
} from "~/data/sources/opencart/types/remoteData";
import {
  PreparedProductImage,
  OpencartProducts,
  SquaredImage,
} from "~/data/sources/opencart/types/preparedData/opencartProducts";
import { OpencartCategories } from "~/data/sources/opencart/types/preparedData/opencartCategories";
import { getImageUrl } from "~/data/sources/opencart/utils/getImageUrl";

function createSquaredImage(thumb: string, original: string): SquaredImage {
  return {
    thumb: getImageUrl(thumb),
    original: getImageUrl(original),
  };
}

function getSquaredImage(product: RemoteProduct): SquaredImage {
  if (product.squaredImage) {
    return createSquaredImage(
      product.squaredImage.thumb,
      product.squaredImage.original
    );
  }
  const image = product.saleImage || product.images[0];
  return createSquaredImage(image, image);
}

function getImages(product: RemoteProduct): PreparedProductImage[] {
  return product.images
    .filter((image) => Boolean(image))
    .map((image, i) => {
      return {
        small: getImageUrl(image),
        large: getImageUrl(product.large_images[i]),
      };
    });
}

export function convertProducts(data: RemoteProducts): OpencartProducts {
  return Object.fromEntries(
    Object.entries(data).map(([productId, product]) => {
      const id = productId.toString();
      return [
        id,
        {
          id,
          name: decodeHtmlEntities(product.name.replace(/<br\/?>/, "\n")),
          image: getImageUrl(product.saleImage || product.images[0]),
          squaredImage: getSquaredImage(product),
          saleId: 5,
          images: getImages(product),
          prices: product.prices.map((id) => id.toString()),
          salePriceId: product.prices.includes(product.salePriceId)
            ? product.salePriceId
            : 0,
          showSalePrice: product.showSalePrice,
          label: "",
          isSameSize: product.isSameSize,
          hasAttributes: product.hasAttributes,
          categoryId: product.categoryId.toString(),
        },
      ];
    })
  );
}

export function filterProductsByCategories<T extends { [key: number]: any }>(
  data: T,
  categories: OpencartCategories
): T {
  const result = {} as T;
  Object.values(categories).forEach((category) =>
    category.items.forEach((productId) => {
      result[+productId] = data[+productId];
    })
  );
  return result;
}
