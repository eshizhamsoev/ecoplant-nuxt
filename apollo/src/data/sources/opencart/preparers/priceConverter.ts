import { RemoteProductPrices } from "../types/remoteData";
import { PreparedPrices } from "../types/preparedData/preparedPrices";
import { OpencartProducts } from "../types/preparedData/opencartProducts";

export function convertPrices(data: RemoteProductPrices): PreparedPrices {
  const prices = {} as PreparedPrices;
  Object.keys(data).forEach((priceId) => {
    const id = priceId;
    const price = data[+id];
    prices[id] = {
      id,
      size: price.size,
      price: price.byRequest ? null : +price.price,
      oldPrice: price.price ? +price.price * 2 : null,
      weekPrice: price.weekprice,
    };
  });
  return prices;
}

export function filterPricesByProducts(
  data: RemoteProductPrices,
  products: OpencartProducts
): RemoteProductPrices {
  const result: RemoteProductPrices = {};
  Object.values(products).forEach((product) =>
    product.prices.forEach((priceId) => {
      result[+priceId] = data[+priceId];
    })
  );
  return result;
}
