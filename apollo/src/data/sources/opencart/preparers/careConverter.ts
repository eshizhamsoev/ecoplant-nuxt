import { RemoteCareConfig } from "~/data/sources/opencart/types/remoteData";
import { Care } from "~/data/types/care";

export function careConverter(config: RemoteCareConfig): Care {
  return {
    seasons: config.seasons.map((season) => ({
      id: season.id,
      title: season.title,
      blocks: season.items.map((block, index) => ({
        id: `${season.id}-${index}`,
        title: block.title,
        text: block.text,
        images: block.images.map((image) => ({
          src: image,
          width: 112,
          height: 112,
        })),
      })),
    })),
  };
}
