const dotenv = require("dotenv");
dotenv.config();

let domain = "https://ecoplant-pitomnik.ru/";

if (process.env.NODE_ENV === "development") {
  if (process.env.TEST_API_DOMAIN) {
    domain = process.env.TEST_API_DOMAIN;
  }
}

export const MSK_URL = `${domain}index.php?route=extension/feed/landing&config_id=18`;
export const SPB_URL = `${domain}index.php?route=extension/feed/landing&config_id=19`;

export const CARE_URL = `${domain}index.php?route=extension/feed/landing/care`;
export const VIDEO_REVIEWS_URL = `${domain}index.php?route=extension/feed/landing/video_reviews`;
export const CLIENT_PHOTOS_URL = `${domain}index.php?route=extension/feed/landing/client_photos`;
export const AWARDS_URL = `${domain}index.php?route=extension/feed/landing/awards`;
export const VILLAGES_URL = `${domain}index.php?route=extension/feed/landing/villages`;
export const DOCUMENTS_URL = `${domain}index.php?route=extension/feed/landing/documents`;
export const CLIENTS_URL = `${domain}index.php?route=extension/feed/landing/clients`;
export const GALLERY_URL = `${domain}index.php?route=extension/feed/landing/gallery`;
