import { IMAGE_PREFIX } from "~/data/sources/opencart/const/image-prefix";

export function getImageUrl(url: string): string {
  return IMAGE_PREFIX + url;
}
