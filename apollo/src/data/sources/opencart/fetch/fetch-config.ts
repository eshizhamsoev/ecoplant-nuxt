import axios from "axios";
import {CatalogType} from "../../../const/catalog-type";
import {
    CARE_URL,
    MSK_URL,
    SPB_URL,
    VIDEO_REVIEWS_URL,
    CLIENT_PHOTOS_URL,
    AWARDS_URL, VILLAGES_URL, DOCUMENTS_URL, CLIENTS_URL, GALLERY_URL,
} from "../const/config-url";
import {
    ClientPhoto,
    RemoteCareConfig,
    RemoteLandingConfig,
    VideoReviews,
} from "../types/remoteData";
import {Award, Client, Document, GalleryItem, Village} from "~/generated/graphql";

function getUrl(type: CatalogType) {
    if (type === CatalogType.SPB) {
        return SPB_URL;
    }
    return MSK_URL;
}

export function fetchConfig(type: CatalogType): Promise<RemoteLandingConfig> {
    return axios.get(getUrl(type)).then((res) => res.data);
}

export function fetchCare(): Promise<RemoteCareConfig> {
    return axios.get(CARE_URL).then((res) => res.data);
}

export function fetchReviews(): Promise<VideoReviews> {
    return axios.get(VIDEO_REVIEWS_URL).then((res) => res.data);
}

export function fetchClientPhotos(): Promise<ClientPhoto[]> {
    return axios.get(CLIENT_PHOTOS_URL).then((res) => res.data);
}

export function fetchAwards(): Promise<Award[]> {
    return axios.get(AWARDS_URL).then((res) => res.data);
}

export function fetchVillages(): Promise<Village[]> {
    return axios.get(VILLAGES_URL).then((res) => res.data);
}

export function fetchDocuments(): Promise<Document[]> {
    return axios.get(DOCUMENTS_URL).then((res) => res.data);
}

export function fetchClients(): Promise<Client[]> {
    return axios.get(CLIENTS_URL).then((res) => res.data);
}

export function fetchGallery(id: Number): Promise<GalleryItem[]> {
    return axios.get(`${GALLERY_URL}&gallery_id=${id}`).then((res) => res.data);
}
