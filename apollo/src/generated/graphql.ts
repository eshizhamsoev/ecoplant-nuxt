import { GraphQLResolveInfo } from 'graphql';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Meta = {
  __typename?: 'Meta';
  title: Scalars['String'];
  h1: Scalars['String'];
  description?: Maybe<Scalars['String']>;
};

export type Category = {
  id: Scalars['ID'];
  name: Scalars['String'];
  image: Scalars['String'];
};

export type ProductCategory = Category & {
  __typename?: 'ProductCategory';
  id: Scalars['ID'];
  name: Scalars['String'];
  image: Scalars['String'];
  products: Array<Product>;
  meta: Meta;
  description?: Maybe<Scalars['String']>;
};

export type ServiceCategory = Category & {
  __typename?: 'ServiceCategory';
  id: Scalars['ID'];
  name: Scalars['String'];
  image: Scalars['String'];
  services: Array<Service>;
};

export type ExclusiveCategory = Category & {
  __typename?: 'ExclusiveCategory';
  id: Scalars['ID'];
  name: Scalars['String'];
  image: Scalars['String'];
  exclusives: Array<Exclusive>;
};

export type ProductAttribute = {
  __typename?: 'ProductAttribute';
  name: Scalars['String'];
  text: Scalars['String'];
};

export type Product = {
  __typename?: 'Product';
  id: Scalars['ID'];
  categoryId: Scalars['ID'];
  category: ProductCategory;
  name: Scalars['String'];
  image: Scalars['String'];
  squaredImage: SquaredProductImage;
  hasAttributes: Scalars['Boolean'];
  isSameSize: Scalars['Boolean'];
  prices: Array<Price>;
  images: Array<ProductImage>;
  showSalePrice: Scalars['Boolean'];
  salePrice?: Maybe<Price>;
  meta: Meta;
  description?: Maybe<Scalars['String']>;
  varieties: Array<Variety>;
};


export type ProductPricesArgs = {
  ids?: Maybe<Array<Scalars['ID']>>;
};

export type VarietyAttribute = {
  __typename?: 'VarietyAttribute';
  name: Scalars['String'];
  value: Scalars['String'];
};

export type Variety = {
  __typename?: 'Variety';
  id: Scalars['ID'];
  name: Scalars['String'];
  description: Array<VarietyAttribute>;
};

export type SquaredProductImage = {
  __typename?: 'SquaredProductImage';
  thumb: Scalars['String'];
  original: Scalars['String'];
};

export type ProductImage = {
  __typename?: 'ProductImage';
  small: Scalars['String'];
  large: Scalars['String'];
};

export type Price = {
  __typename?: 'Price';
  id: Scalars['ID'];
  size?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  oldPrice?: Maybe<Scalars['Float']>;
  weekPrice: Scalars['Boolean'];
};

export type ServiceGallery = {
  __typename?: 'ServiceGallery';
  images: Array<ServiceImage>;
};

export type ServiceText = {
  __typename?: 'ServiceText';
  text: Scalars['String'];
};

export type ServiceContent = ServiceText | ServiceGallery;

export type AbstractService = {
  id: Scalars['ID'];
  name: Scalars['String'];
  images: Array<ServiceImage>;
  price?: Maybe<Scalars['String']>;
};

export type Service = AbstractService & {
  __typename?: 'Service';
  id: Scalars['ID'];
  name: Scalars['String'];
  images: Array<ServiceImage>;
  content: Array<ServiceContent>;
  price?: Maybe<Scalars['String']>;
};

export type ServiceImage = {
  __typename?: 'ServiceImage';
  small: Scalars['String'];
  large: Scalars['String'];
  original: Scalars['String'];
};

export type Exclusive = AbstractService & {
  __typename?: 'Exclusive';
  id: Scalars['ID'];
  name: Scalars['String'];
  images: Array<ServiceImage>;
  price?: Maybe<Scalars['String']>;
};

export type CareSeasonBlock = {
  __typename?: 'CareSeasonBlock';
  id: Scalars['ID'];
  title: Scalars['String'];
  text: Scalars['String'];
  images: Array<ImageWithSize>;
};

export type CareSeason = {
  __typename?: 'CareSeason';
  id: Scalars['ID'];
  title: Scalars['String'];
  blocks: Array<CareSeasonBlock>;
};

export type ImageWithSize = {
  __typename?: 'ImageWithSize';
  src: Scalars['String'];
  width: Scalars['Int'];
  height: Scalars['Int'];
};

export type ResponsiveImageVariants = {
  __typename?: 'ResponsiveImageVariants';
  desktop?: Maybe<ImageWithSize>;
  mobile?: Maybe<ImageWithSize>;
};

export type YoutubeVideo = {
  __typename?: 'YoutubeVideo';
  code: Scalars['String'];
};

export type FileVideo = {
  __typename?: 'FileVideo';
  source: Scalars['String'];
};

export type Video = YoutubeVideo | FileVideo;

export type VideoReview = {
  __typename?: 'VideoReview';
  person?: Maybe<Scalars['String']>;
  role?: Maybe<Scalars['String']>;
  video: Video;
  poster: ResponsiveImageVariants;
};

export type ThumbnailImage = {
  __typename?: 'ThumbnailImage';
  thumb: ImageWithSize;
  large: ImageWithSize;
};

export type Award = {
  __typename?: 'Award';
  image: ThumbnailImage;
};

export type Village = {
  __typename?: 'Village';
  image: ThumbnailImage;
  title: Scalars['String'];
};

export type GalleryItem = {
  __typename?: 'GalleryItem';
  image: ThumbnailImage;
  title: Scalars['String'];
  id: Scalars['ID'];
  content?: Maybe<Scalars['String']>;
};

export type Document = {
  __typename?: 'Document';
  image: ThumbnailImage;
};

export type Client = {
  __typename?: 'Client';
  image: ThumbnailImage;
  title?: Maybe<Scalars['String']>;
  isState: Scalars['Boolean'];
};

export type ClientPhoto = {
  __typename?: 'ClientPhoto';
  image: ThumbnailImage;
  name: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  categories: Array<Category>;
  productCategories: Array<ProductCategory>;
  category?: Maybe<Category>;
  products: Array<Product>;
  product?: Maybe<Product>;
  productAttributes: Array<ProductAttribute>;
  service?: Maybe<Service>;
  exclusive?: Maybe<Exclusive>;
  variety?: Maybe<Variety>;
  search: Array<Product>;
  featuredProducts: Array<Product>;
  careSeasons: Array<CareSeason>;
  careSeason?: Maybe<CareSeason>;
  videoReviews: Array<VideoReview>;
  awards: Array<Award>;
  documents: Array<Document>;
  clients: Array<Client>;
  villages: Array<Village>;
  clientPhotos: Array<ClientPhoto>;
  gallery: Array<GalleryItem>;
  galleryItem?: Maybe<GalleryItem>;
};


export type QueryCategoriesArgs = {
  ids?: Maybe<Array<Scalars['ID']>>;
};


export type QueryProductCategoriesArgs = {
  ids?: Maybe<Array<Scalars['ID']>>;
};


export type QueryCategoryArgs = {
  id: Scalars['ID'];
};


export type QueryProductsArgs = {
  ids: Array<Scalars['ID']>;
};


export type QueryProductArgs = {
  id: Scalars['ID'];
};


export type QueryProductAttributesArgs = {
  productId: Scalars['ID'];
};


export type QueryServiceArgs = {
  id: Scalars['ID'];
};


export type QueryExclusiveArgs = {
  id: Scalars['ID'];
};


export type QueryVarietyArgs = {
  id: Scalars['ID'];
};


export type QuerySearchArgs = {
  query: Scalars['String'];
};


export type QueryFeaturedProductsArgs = {
  offset?: Maybe<Scalars['Int']>;
  limit?: Maybe<Scalars['Int']>;
};


export type QueryCareSeasonArgs = {
  id: Scalars['ID'];
};


export type QueryGalleryArgs = {
  id: Scalars['ID'];
};


export type QueryGalleryItemArgs = {
  id: Scalars['ID'];
};

export type WithIndex<TObject> = TObject & Record<string, any>;
export type ResolversObject<TObject> = WithIndex<TObject>;

export type ResolverTypeWrapper<T> = Promise<T> | T;


export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> = LegacyStitchingResolver<TResult, TParent, TContext, TArgs> | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}> = (obj: T, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = ResolversObject<{
  Meta: ResolverTypeWrapper<Meta>;
  String: ResolverTypeWrapper<Scalars['String']>;
  Category: ResolversTypes['ProductCategory'] | ResolversTypes['ServiceCategory'] | ResolversTypes['ExclusiveCategory'];
  ID: ResolverTypeWrapper<Scalars['ID']>;
  ProductCategory: ResolverTypeWrapper<Omit<ProductCategory, 'products'>>;
  ServiceCategory: ResolverTypeWrapper<Omit<ServiceCategory, 'services'>>;
  ExclusiveCategory: ResolverTypeWrapper<ExclusiveCategory>;
  ProductAttribute: ResolverTypeWrapper<ProductAttribute>;
  Product: ResolverTypeWrapper<Omit<Product, 'prices' | 'category' | 'varieties'>>;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  VarietyAttribute: ResolverTypeWrapper<VarietyAttribute>;
  Variety: ResolverTypeWrapper<Variety>;
  SquaredProductImage: ResolverTypeWrapper<SquaredProductImage>;
  ProductImage: ResolverTypeWrapper<ProductImage>;
  Price: ResolverTypeWrapper<Price>;
  Float: ResolverTypeWrapper<Scalars['Float']>;
  ServiceGallery: ResolverTypeWrapper<ServiceGallery>;
  ServiceText: ResolverTypeWrapper<ServiceText>;
  ServiceContent: ResolversTypes['ServiceText'] | ResolversTypes['ServiceGallery'];
  AbstractService: ResolversTypes['Service'] | ResolversTypes['Exclusive'];
  Service: ResolverTypeWrapper<Omit<Service, 'content'> & { content: Array<ResolversTypes['ServiceContent']> }>;
  ServiceImage: ResolverTypeWrapper<ServiceImage>;
  Exclusive: ResolverTypeWrapper<Exclusive>;
  CareSeasonBlock: ResolverTypeWrapper<CareSeasonBlock>;
  CareSeason: ResolverTypeWrapper<CareSeason>;
  ImageWithSize: ResolverTypeWrapper<ImageWithSize>;
  Int: ResolverTypeWrapper<Scalars['Int']>;
  ResponsiveImageVariants: ResolverTypeWrapper<ResponsiveImageVariants>;
  YoutubeVideo: ResolverTypeWrapper<YoutubeVideo>;
  FileVideo: ResolverTypeWrapper<FileVideo>;
  Video: ResolversTypes['YoutubeVideo'] | ResolversTypes['FileVideo'];
  VideoReview: ResolverTypeWrapper<Omit<VideoReview, 'video'> & { video: ResolversTypes['Video'] }>;
  ThumbnailImage: ResolverTypeWrapper<ThumbnailImage>;
  Award: ResolverTypeWrapper<Award>;
  Village: ResolverTypeWrapper<Village>;
  GalleryItem: ResolverTypeWrapper<GalleryItem>;
  Document: ResolverTypeWrapper<Document>;
  Client: ResolverTypeWrapper<Client>;
  ClientPhoto: ResolverTypeWrapper<ClientPhoto>;
  Query: ResolverTypeWrapper<{}>;
}>;

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = ResolversObject<{
  Meta: Meta;
  String: Scalars['String'];
  Category: ResolversParentTypes['ProductCategory'] | ResolversParentTypes['ServiceCategory'] | ResolversParentTypes['ExclusiveCategory'];
  ID: Scalars['ID'];
  ProductCategory: Omit<ProductCategory, 'products'>;
  ServiceCategory: Omit<ServiceCategory, 'services'>;
  ExclusiveCategory: ExclusiveCategory;
  ProductAttribute: ProductAttribute;
  Product: Omit<Product, 'prices' | 'category' | 'varieties'>;
  Boolean: Scalars['Boolean'];
  VarietyAttribute: VarietyAttribute;
  Variety: Variety;
  SquaredProductImage: SquaredProductImage;
  ProductImage: ProductImage;
  Price: Price;
  Float: Scalars['Float'];
  ServiceGallery: ServiceGallery;
  ServiceText: ServiceText;
  ServiceContent: ResolversParentTypes['ServiceText'] | ResolversParentTypes['ServiceGallery'];
  AbstractService: ResolversParentTypes['Service'] | ResolversParentTypes['Exclusive'];
  Service: Omit<Service, 'content'> & { content: Array<ResolversParentTypes['ServiceContent']> };
  ServiceImage: ServiceImage;
  Exclusive: Exclusive;
  CareSeasonBlock: CareSeasonBlock;
  CareSeason: CareSeason;
  ImageWithSize: ImageWithSize;
  Int: Scalars['Int'];
  ResponsiveImageVariants: ResponsiveImageVariants;
  YoutubeVideo: YoutubeVideo;
  FileVideo: FileVideo;
  Video: ResolversParentTypes['YoutubeVideo'] | ResolversParentTypes['FileVideo'];
  VideoReview: Omit<VideoReview, 'video'> & { video: ResolversParentTypes['Video'] };
  ThumbnailImage: ThumbnailImage;
  Award: Award;
  Village: Village;
  GalleryItem: GalleryItem;
  Document: Document;
  Client: Client;
  ClientPhoto: ClientPhoto;
  Query: {};
}>;

export type MetaResolvers<ContextType = any, ParentType extends ResolversParentTypes['Meta'] = ResolversParentTypes['Meta']> = ResolversObject<{
  title?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  h1?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type CategoryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Category'] = ResolversParentTypes['Category']> = ResolversObject<{
  __resolveType: TypeResolveFn<'ProductCategory' | 'ServiceCategory' | 'ExclusiveCategory', ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  image?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
}>;

export type ProductCategoryResolvers<ContextType = any, ParentType extends ResolversParentTypes['ProductCategory'] = ResolversParentTypes['ProductCategory']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  image?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  products?: Resolver<Array<ResolversTypes['Product']>, ParentType, ContextType>;
  meta?: Resolver<ResolversTypes['Meta'], ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ServiceCategoryResolvers<ContextType = any, ParentType extends ResolversParentTypes['ServiceCategory'] = ResolversParentTypes['ServiceCategory']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  image?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  services?: Resolver<Array<ResolversTypes['Service']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ExclusiveCategoryResolvers<ContextType = any, ParentType extends ResolversParentTypes['ExclusiveCategory'] = ResolversParentTypes['ExclusiveCategory']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  image?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  exclusives?: Resolver<Array<ResolversTypes['Exclusive']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ProductAttributeResolvers<ContextType = any, ParentType extends ResolversParentTypes['ProductAttribute'] = ResolversParentTypes['ProductAttribute']> = ResolversObject<{
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  text?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ProductResolvers<ContextType = any, ParentType extends ResolversParentTypes['Product'] = ResolversParentTypes['Product']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  categoryId?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  category?: Resolver<ResolversTypes['ProductCategory'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  image?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  squaredImage?: Resolver<ResolversTypes['SquaredProductImage'], ParentType, ContextType>;
  hasAttributes?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  isSameSize?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  prices?: Resolver<Array<ResolversTypes['Price']>, ParentType, ContextType, RequireFields<ProductPricesArgs, never>>;
  images?: Resolver<Array<ResolversTypes['ProductImage']>, ParentType, ContextType>;
  showSalePrice?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  salePrice?: Resolver<Maybe<ResolversTypes['Price']>, ParentType, ContextType>;
  meta?: Resolver<ResolversTypes['Meta'], ParentType, ContextType>;
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  varieties?: Resolver<Array<ResolversTypes['Variety']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type VarietyAttributeResolvers<ContextType = any, ParentType extends ResolversParentTypes['VarietyAttribute'] = ResolversParentTypes['VarietyAttribute']> = ResolversObject<{
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  value?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type VarietyResolvers<ContextType = any, ParentType extends ResolversParentTypes['Variety'] = ResolversParentTypes['Variety']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  description?: Resolver<Array<ResolversTypes['VarietyAttribute']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type SquaredProductImageResolvers<ContextType = any, ParentType extends ResolversParentTypes['SquaredProductImage'] = ResolversParentTypes['SquaredProductImage']> = ResolversObject<{
  thumb?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  original?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ProductImageResolvers<ContextType = any, ParentType extends ResolversParentTypes['ProductImage'] = ResolversParentTypes['ProductImage']> = ResolversObject<{
  small?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  large?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type PriceResolvers<ContextType = any, ParentType extends ResolversParentTypes['Price'] = ResolversParentTypes['Price']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  size?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  price?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  oldPrice?: Resolver<Maybe<ResolversTypes['Float']>, ParentType, ContextType>;
  weekPrice?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ServiceGalleryResolvers<ContextType = any, ParentType extends ResolversParentTypes['ServiceGallery'] = ResolversParentTypes['ServiceGallery']> = ResolversObject<{
  images?: Resolver<Array<ResolversTypes['ServiceImage']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ServiceTextResolvers<ContextType = any, ParentType extends ResolversParentTypes['ServiceText'] = ResolversParentTypes['ServiceText']> = ResolversObject<{
  text?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ServiceContentResolvers<ContextType = any, ParentType extends ResolversParentTypes['ServiceContent'] = ResolversParentTypes['ServiceContent']> = ResolversObject<{
  __resolveType: TypeResolveFn<'ServiceText' | 'ServiceGallery', ParentType, ContextType>;
}>;

export type AbstractServiceResolvers<ContextType = any, ParentType extends ResolversParentTypes['AbstractService'] = ResolversParentTypes['AbstractService']> = ResolversObject<{
  __resolveType: TypeResolveFn<'Service' | 'Exclusive', ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  images?: Resolver<Array<ResolversTypes['ServiceImage']>, ParentType, ContextType>;
  price?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
}>;

export type ServiceResolvers<ContextType = any, ParentType extends ResolversParentTypes['Service'] = ResolversParentTypes['Service']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  images?: Resolver<Array<ResolversTypes['ServiceImage']>, ParentType, ContextType>;
  content?: Resolver<Array<ResolversTypes['ServiceContent']>, ParentType, ContextType>;
  price?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ServiceImageResolvers<ContextType = any, ParentType extends ResolversParentTypes['ServiceImage'] = ResolversParentTypes['ServiceImage']> = ResolversObject<{
  small?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  large?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  original?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ExclusiveResolvers<ContextType = any, ParentType extends ResolversParentTypes['Exclusive'] = ResolversParentTypes['Exclusive']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  images?: Resolver<Array<ResolversTypes['ServiceImage']>, ParentType, ContextType>;
  price?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type CareSeasonBlockResolvers<ContextType = any, ParentType extends ResolversParentTypes['CareSeasonBlock'] = ResolversParentTypes['CareSeasonBlock']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  title?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  text?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  images?: Resolver<Array<ResolversTypes['ImageWithSize']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type CareSeasonResolvers<ContextType = any, ParentType extends ResolversParentTypes['CareSeason'] = ResolversParentTypes['CareSeason']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  title?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  blocks?: Resolver<Array<ResolversTypes['CareSeasonBlock']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ImageWithSizeResolvers<ContextType = any, ParentType extends ResolversParentTypes['ImageWithSize'] = ResolversParentTypes['ImageWithSize']> = ResolversObject<{
  src?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  width?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  height?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ResponsiveImageVariantsResolvers<ContextType = any, ParentType extends ResolversParentTypes['ResponsiveImageVariants'] = ResolversParentTypes['ResponsiveImageVariants']> = ResolversObject<{
  desktop?: Resolver<Maybe<ResolversTypes['ImageWithSize']>, ParentType, ContextType>;
  mobile?: Resolver<Maybe<ResolversTypes['ImageWithSize']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type YoutubeVideoResolvers<ContextType = any, ParentType extends ResolversParentTypes['YoutubeVideo'] = ResolversParentTypes['YoutubeVideo']> = ResolversObject<{
  code?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type FileVideoResolvers<ContextType = any, ParentType extends ResolversParentTypes['FileVideo'] = ResolversParentTypes['FileVideo']> = ResolversObject<{
  source?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type VideoResolvers<ContextType = any, ParentType extends ResolversParentTypes['Video'] = ResolversParentTypes['Video']> = ResolversObject<{
  __resolveType: TypeResolveFn<'YoutubeVideo' | 'FileVideo', ParentType, ContextType>;
}>;

export type VideoReviewResolvers<ContextType = any, ParentType extends ResolversParentTypes['VideoReview'] = ResolversParentTypes['VideoReview']> = ResolversObject<{
  person?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  role?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  video?: Resolver<ResolversTypes['Video'], ParentType, ContextType>;
  poster?: Resolver<ResolversTypes['ResponsiveImageVariants'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ThumbnailImageResolvers<ContextType = any, ParentType extends ResolversParentTypes['ThumbnailImage'] = ResolversParentTypes['ThumbnailImage']> = ResolversObject<{
  thumb?: Resolver<ResolversTypes['ImageWithSize'], ParentType, ContextType>;
  large?: Resolver<ResolversTypes['ImageWithSize'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type AwardResolvers<ContextType = any, ParentType extends ResolversParentTypes['Award'] = ResolversParentTypes['Award']> = ResolversObject<{
  image?: Resolver<ResolversTypes['ThumbnailImage'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type VillageResolvers<ContextType = any, ParentType extends ResolversParentTypes['Village'] = ResolversParentTypes['Village']> = ResolversObject<{
  image?: Resolver<ResolversTypes['ThumbnailImage'], ParentType, ContextType>;
  title?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type GalleryItemResolvers<ContextType = any, ParentType extends ResolversParentTypes['GalleryItem'] = ResolversParentTypes['GalleryItem']> = ResolversObject<{
  image?: Resolver<ResolversTypes['ThumbnailImage'], ParentType, ContextType>;
  title?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  content?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type DocumentResolvers<ContextType = any, ParentType extends ResolversParentTypes['Document'] = ResolversParentTypes['Document']> = ResolversObject<{
  image?: Resolver<ResolversTypes['ThumbnailImage'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ClientResolvers<ContextType = any, ParentType extends ResolversParentTypes['Client'] = ResolversParentTypes['Client']> = ResolversObject<{
  image?: Resolver<ResolversTypes['ThumbnailImage'], ParentType, ContextType>;
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  isState?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type ClientPhotoResolvers<ContextType = any, ParentType extends ResolversParentTypes['ClientPhoto'] = ResolversParentTypes['ClientPhoto']> = ResolversObject<{
  image?: Resolver<ResolversTypes['ThumbnailImage'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = ResolversObject<{
  categories?: Resolver<Array<ResolversTypes['Category']>, ParentType, ContextType, RequireFields<QueryCategoriesArgs, never>>;
  productCategories?: Resolver<Array<ResolversTypes['ProductCategory']>, ParentType, ContextType, RequireFields<QueryProductCategoriesArgs, never>>;
  category?: Resolver<Maybe<ResolversTypes['Category']>, ParentType, ContextType, RequireFields<QueryCategoryArgs, 'id'>>;
  products?: Resolver<Array<ResolversTypes['Product']>, ParentType, ContextType, RequireFields<QueryProductsArgs, 'ids'>>;
  product?: Resolver<Maybe<ResolversTypes['Product']>, ParentType, ContextType, RequireFields<QueryProductArgs, 'id'>>;
  productAttributes?: Resolver<Array<ResolversTypes['ProductAttribute']>, ParentType, ContextType, RequireFields<QueryProductAttributesArgs, 'productId'>>;
  service?: Resolver<Maybe<ResolversTypes['Service']>, ParentType, ContextType, RequireFields<QueryServiceArgs, 'id'>>;
  exclusive?: Resolver<Maybe<ResolversTypes['Exclusive']>, ParentType, ContextType, RequireFields<QueryExclusiveArgs, 'id'>>;
  variety?: Resolver<Maybe<ResolversTypes['Variety']>, ParentType, ContextType, RequireFields<QueryVarietyArgs, 'id'>>;
  search?: Resolver<Array<ResolversTypes['Product']>, ParentType, ContextType, RequireFields<QuerySearchArgs, 'query'>>;
  featuredProducts?: Resolver<Array<ResolversTypes['Product']>, ParentType, ContextType, RequireFields<QueryFeaturedProductsArgs, never>>;
  careSeasons?: Resolver<Array<ResolversTypes['CareSeason']>, ParentType, ContextType>;
  careSeason?: Resolver<Maybe<ResolversTypes['CareSeason']>, ParentType, ContextType, RequireFields<QueryCareSeasonArgs, 'id'>>;
  videoReviews?: Resolver<Array<ResolversTypes['VideoReview']>, ParentType, ContextType>;
  awards?: Resolver<Array<ResolversTypes['Award']>, ParentType, ContextType>;
  documents?: Resolver<Array<ResolversTypes['Document']>, ParentType, ContextType>;
  clients?: Resolver<Array<ResolversTypes['Client']>, ParentType, ContextType>;
  villages?: Resolver<Array<ResolversTypes['Village']>, ParentType, ContextType>;
  clientPhotos?: Resolver<Array<ResolversTypes['ClientPhoto']>, ParentType, ContextType>;
  gallery?: Resolver<Array<ResolversTypes['GalleryItem']>, ParentType, ContextType, RequireFields<QueryGalleryArgs, 'id'>>;
  galleryItem?: Resolver<Maybe<ResolversTypes['GalleryItem']>, ParentType, ContextType, RequireFields<QueryGalleryItemArgs, 'id'>>;
}>;

export type Resolvers<ContextType = any> = ResolversObject<{
  Meta?: MetaResolvers<ContextType>;
  Category?: CategoryResolvers<ContextType>;
  ProductCategory?: ProductCategoryResolvers<ContextType>;
  ServiceCategory?: ServiceCategoryResolvers<ContextType>;
  ExclusiveCategory?: ExclusiveCategoryResolvers<ContextType>;
  ProductAttribute?: ProductAttributeResolvers<ContextType>;
  Product?: ProductResolvers<ContextType>;
  VarietyAttribute?: VarietyAttributeResolvers<ContextType>;
  Variety?: VarietyResolvers<ContextType>;
  SquaredProductImage?: SquaredProductImageResolvers<ContextType>;
  ProductImage?: ProductImageResolvers<ContextType>;
  Price?: PriceResolvers<ContextType>;
  ServiceGallery?: ServiceGalleryResolvers<ContextType>;
  ServiceText?: ServiceTextResolvers<ContextType>;
  ServiceContent?: ServiceContentResolvers<ContextType>;
  AbstractService?: AbstractServiceResolvers<ContextType>;
  Service?: ServiceResolvers<ContextType>;
  ServiceImage?: ServiceImageResolvers<ContextType>;
  Exclusive?: ExclusiveResolvers<ContextType>;
  CareSeasonBlock?: CareSeasonBlockResolvers<ContextType>;
  CareSeason?: CareSeasonResolvers<ContextType>;
  ImageWithSize?: ImageWithSizeResolvers<ContextType>;
  ResponsiveImageVariants?: ResponsiveImageVariantsResolvers<ContextType>;
  YoutubeVideo?: YoutubeVideoResolvers<ContextType>;
  FileVideo?: FileVideoResolvers<ContextType>;
  Video?: VideoResolvers<ContextType>;
  VideoReview?: VideoReviewResolvers<ContextType>;
  ThumbnailImage?: ThumbnailImageResolvers<ContextType>;
  Award?: AwardResolvers<ContextType>;
  Village?: VillageResolvers<ContextType>;
  GalleryItem?: GalleryItemResolvers<ContextType>;
  Document?: DocumentResolvers<ContextType>;
  Client?: ClientResolvers<ContextType>;
  ClientPhoto?: ClientPhotoResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
}>;


/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
 */
export type IResolvers<ContextType = any> = Resolvers<ContextType>;
