import { ApolloServer } from "apollo-server";
import axios from "axios";
import { getCare, getCatalog, getMediaCollections } from "./data";
import { ResolverFn, Resolvers } from "~/generated/graphql";
import { CatalogType } from "~/data/const/catalog-type";
import {
  Catalog,
  Category as OpencartCategory,
  Product,
} from "~/data/types/catalog";
import { getServices } from "~/data/const/services";
import {
  SERVICE_CATEGORY_ID,
  servicesCategory,
} from "~/data/const/servicesCategory";
import { getLandingConfig } from "~/data/sources/database/landings/queries";
import { SimpleStorage } from "~/storage/simple-storage";
import { exclusives } from "~/data/const/exclusives";
import {
  EXCLUSIVE_CATEGORY_ID,
  exclusivesCategory,
} from "~/data/const/exclusivesCategory";
import { Care } from "~/data/types/care";
import { MediaCollections } from "~/data/types/media-collections";

type ApolloServerContext = {
  host: string;
};

const hostTypeStorage = new SimpleStorage((host: string) => {
  return getLandingConfig(host).then((data) => {
    return data && data.useSpbPrice ? CatalogType.SPB : CatalogType.MSK;
  });
});

function getType(host: string): Promise<CatalogType> {
  return hostTypeStorage.get(host);
}

const typeDefs = require("./graphql/schema.graphql");

function injectCatalog<TResult, TParent, TArgs>(
  callback: ResolverFn<
    TResult,
    TParent,
    { catalog: Catalog } & ApolloServerContext,
    TArgs
  >
): ResolverFn<TResult, TParent, ApolloServerContext, TArgs> {
  return async function (parent, args, context, info) {
    const { host } = context;
    const type = await getType(host);
    const catalog = await getCatalog(type);
    return callback(parent, args, { ...context, catalog }, info);
  };
}

function injectCare<TResult, TParent, TArgs>(
  callback: ResolverFn<
    TResult,
    TParent,
    { care: Care } & ApolloServerContext,
    TArgs
  >
): ResolverFn<TResult, TParent, ApolloServerContext, TArgs> {
  return async function (parent, args, context, info) {
    const care = await getCare();
    return callback(parent, args, { ...context, care }, info);
  };
}

function injectCommonMediaCollections<TResult, TParent, TArgs>(
  callback: ResolverFn<
    TResult,
    TParent,
    { mediaCollections: MediaCollections } & ApolloServerContext,
    TArgs
  >
): ResolverFn<TResult, TParent, ApolloServerContext, TArgs> {
  return async function (parent, args, context, info) {
    const mediaCollections = await getMediaCollections();
    return callback(parent, args, { ...context, mediaCollections }, info);
  };
}

function injectType<TResult, TParent, TArgs>(
  callback: ResolverFn<
    TResult,
    TParent,
    { type: CatalogType } & ApolloServerContext,
    TArgs
  >
): ResolverFn<TResult, TParent, ApolloServerContext, TArgs> {
  return async function (parent, args, context, info) {
    const { host } = context;
    const type = await getType(host);
    return callback(parent, args, { ...context, type }, info);
  };
}

const resolvers: Resolvers<ApolloServerContext> = {
  Query: {
    categories: injectCatalog((parent, args, context) => {
      const { categories } = context.catalog;
      let categoryIds = [
        "2",
        "3",
        "1",
        "4",
        "5",
        "6",
        "7",
        EXCLUSIVE_CATEGORY_ID,
        SERVICE_CATEGORY_ID,
      ];
      if (Array.isArray(args.ids)) {
        const ids = args.ids;
        categoryIds = categoryIds.filter((categoryId) =>
          ids.includes(categoryId)
        );
      }

      return categoryIds.map((id) =>
        id === SERVICE_CATEGORY_ID
          ? servicesCategory
          : id === EXCLUSIVE_CATEGORY_ID
          ? exclusivesCategory
          : (categories.get(id) as OpencartCategory)
      );
    }),
    productCategories: injectCatalog((parent, args, context) => {
      const { categories } = context.catalog;
      let categoryIds = ["2", "3", "1", "4", "5", "6", "7"];
      if (Array.isArray(args.ids)) {
        const ids = args.ids;
        categoryIds = categoryIds.filter((categoryId) =>
          ids.includes(categoryId)
        );
      }

      return categoryIds.map((id) => categories.get(id) as OpencartCategory);
    }),
    category: injectCatalog((parent, args, context) => {
      if (args.id === SERVICE_CATEGORY_ID) {
        return servicesCategory;
      }
      if (args.id === EXCLUSIVE_CATEGORY_ID) {
        return exclusivesCategory;
      }
      const { categories } = context.catalog;
      return categories.get(args.id) || null;
    }),
    product: injectCatalog((parent, args, context) => {
      return context.catalog.products.get(args.id) || null;
    }),
    products: injectCatalog((parent, { ids }, context) => {
      const { products } = context.catalog;
      return ids.reduce((arr, id) => {
        const product = products.get(id);
        if (product) {
          arr.push(product);
        }
        return arr;
      }, [] as Product[]);
    }),
    search: injectCatalog((parent, { query }, context) => {
      const { products, searchIndex } = context.catalog;
      const searchQuery = query
        .split(" ")
        .filter((s) => s.length > 0)
        .map((item) => `${item}^4 ${item}*`)
        .join(" ");
      return searchIndex
        .search(searchQuery)
        .map((id) => products.get(id.ref))
        .filter<Product>((item): item is Product => item !== undefined);
    }),
    featuredProducts: injectCatalog((parent, params, context) => {
      const { products, featuredProducts } = context.catalog;
      const offset = params.offset || 0;
      const limit = params.limit || featuredProducts.length;
      return featuredProducts
        .slice(offset, limit + offset)
        .map((id) => products.get(id))
        .filter((item): item is Product => !!item);
    }),
    productAttributes(parent, { productId }) {
      return axios
        .post(
          "https://ecoplant-pitomnik.ru/index.php?route=extension/feed/landing/product_attributes",
          {
            id: productId,
            // config_id: 16
          },
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        )
        .then((res) => {
          return res.data;
        });
    },
    service: injectType((_, { id }, { type }) => {
      return getServices(type).find((service) => service.id === id) || null;
    }),
    exclusive(_, { id }) {
      return exclusives.find((exclusive) => exclusive.id === id) || null;
    },
    variety: injectCatalog((parent, args, { catalog: { varieties } }) => {
      return varieties.get(args.id) || null;
    }),
    careSeasons: injectCare((parent, args, { care }) => {
      return care.seasons;
    }),
    careSeason: injectCare((parent, { id }, { care }) => {
      return care.seasons.find((item) => item.id === id) || null;
    }),
    videoReviews: injectCommonMediaCollections(
      (parent, _, { mediaCollections }) => {
        return mediaCollections.videoReviews || [];
      }
    ),
    clientPhotos: injectCommonMediaCollections(
      (parent, _, { mediaCollections }) => {
        return mediaCollections.clientPhotos || [];
      }
    ),
    gallery: injectCommonMediaCollections((parent, args, {mediaCollections}) => {
      return mediaCollections.galleries.get(args.id) || []
    }),
    galleryItem: injectCommonMediaCollections((parent, args, {mediaCollections}) => {
      return mediaCollections.galleryItems.get(args.id) || null
    }),
    awards: injectCommonMediaCollections((parent, _, { mediaCollections }) => {
      return mediaCollections.awards || [];
    }),
    documents: injectCommonMediaCollections((parent, _, { mediaCollections }) => {
      return mediaCollections.documents || [];
    }),
    clients: injectCommonMediaCollections((parent, _, { mediaCollections }) => {
      return mediaCollections.clients || [];
    }),
    villages: injectCommonMediaCollections((parent, _, { mediaCollections }) => {
      return mediaCollections.villages || [];
    }),
  },
  ProductCategory: {
    products: injectCatalog((parent, args, context) => {
      const { categories, products } = context.catalog;
      const category = categories.get(parent.id);
      if (!category) {
        return [];
      }
      return category.items.reduce((arr, id) => {
        const product = products.get(id);
        if (product) {
          arr.push(product);
        }
        return arr;
      }, [] as Product[]);
    }),
  },
  Product: {
    salePrice: injectCatalog((parent, args, { catalog }) => {
      const { products, prices } = catalog;
      const product = products.get(parent.id);
      if (!product || product.prices.length === 0) {
        return null;
      }
      return prices[product.salePriceId || product.prices[0]] || null;
    }),
    prices: injectCatalog((parent, { ids }, { catalog }) => {
      const { products, prices } = catalog;
      const product = products.get(parent.id);
      if (!product) {
        return [];
      }
      if (ids) {
        return product.prices
          .filter((id) => ids.includes(id))
          .map((id) => prices[id]);
      }
      return product.prices.map((id) => prices[id]);
    }),
    category: injectCatalog((parent, args, { catalog }) => {
      const { products, categories } = catalog;
      const product = products.get(parent.id) as Product;
      return categories.get(product.categoryId) as OpencartCategory;
    }),
    varieties: injectCatalog(
      ({ id: productId }, args, { catalog: { varietiesByProducts } }) => {
        return varietiesByProducts.get(productId) || [];
      }
    ),
  },
  ServiceCategory: {
    services: injectType((_, __, { type }) => {
      return getServices(type);
    }),
  },
  ExclusiveCategory: {
    exclusives() {
      return exclusives;
    },
  },
  ServiceContent: {
    __resolveType(obj) {
      if ("text" in obj) {
        return "ServiceText";
      }
      if ("images" in obj) {
        return "ServiceGallery";
      }
      return null;
    },
  },
  Video: {
    __resolveType(obj) {
      if ("code" in obj) {
        return "YoutubeVideo";
      }
      if ("source" in obj) {
        return "FileVideo";
      }
      return null;
    },
  },
  Category: {
    __resolveType(obj) {
      if (obj.id === SERVICE_CATEGORY_ID) {
        return "ServiceCategory";
      }
      if (obj.id === EXCLUSIVE_CATEGORY_ID) {
        return "ExclusiveCategory";
      }
      // if ("products" in obj) {
      return "ProductCategory";
      // }
      // return null;
    },
  },
};
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }) => {
    return { host: req.headers.host };
  },
});

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});

// Hot Module Replacement
if (module.hot) {
  module.hot.accept();
  module.hot.dispose(() => {
    server.stop();
  });
}
