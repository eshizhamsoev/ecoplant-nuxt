import NodeCache from "node-cache";

const stdTTL = 600;

const cacheStorage = new NodeCache({ stdTTL, checkperiod: 720 });

export async function getFromCacheOrUpdate<T>(
  key: string,
  callback: () => Promise<T>,
  keyTTL?: number
): Promise<T> {
  const ttl = keyTTL || stdTTL;
  let value = cacheStorage.get<T>(key);
  if (value) {
    const restTime = (cacheStorage.getTtl(key) || Infinity) - +Date.now();
    if (restTime * 5 < ttl * 1000) {
      cacheStorage.ttl(key, ttl); // Prolong old cache
      callback().then((value) => {
        // and run async operation
        cacheStorage.set(key, value, ttl);
      });
    }
    return value;
  }
  value = await callback();
  cacheStorage.set(key, value, ttl);

  return value;
}
