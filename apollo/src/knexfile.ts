const dotenv = require("dotenv");
dotenv.config();

export const config = {
  client: "mysql2",
  connection: {
    host: process.env.DB_HOST,
    port: 3306,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    charset: "utf8",
  },
  pool: {
    min: 2,
    max: 10,
  },
};
