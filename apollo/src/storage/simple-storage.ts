type FetchFunction = (key: any) => Promise<any>;

export class SimpleStorage {
  private storage: Map<any, any>;
  private fetchFunction: FetchFunction;
  private autoFetchSeconds: number;

  constructor(fetchFunction: FetchFunction, autoFetchSeconds = 3600) {
    this.fetchFunction = fetchFunction;
    this.autoFetchSeconds = autoFetchSeconds;
    this.storage = new Map<any, any>();
  }

  get<T>(key: any): Promise<T> {
    if (this.storage.has(key)) {
      return Promise.resolve(this.storage.get(key));
    }
    return this.fetchFunction(key).then((res) => {
      this.storage.set(key, res);
      this.registerFetcher(key);
      return res;
    });
  }

  registerFetcher(key: any) {
    const f = () =>
      this.fetchFunction(key).then((res) => {
        this.storage.set(key, res);

        return res;
      });
    setInterval(f, this.autoFetchSeconds * 1000);
  }
}
